<?php
include("./admin/config/class.web.config.php");
$con = new Config();


if ($con->checkUserLogin() == FALSE) {
    $con->redirect('signin-signup.php?param=checkout');
}

$userID = 0;
$billing_id = 0;
$billing_fname = '';
$billing_lname = '';
$billing_phone = '';
$billing_address = '';
$billing_city = '';
$billing_zip = '';
$shipping_id = 0;
$shipping_fname = '';
$shipping_lname = '';
$shipping_address = '';
$shipping_phone = '';
$shipping_city = '';
$shipping_zip = '';
$eventID = 0;
$scheduleID = 0;
$ticktQunty = 0;
$programID = 0;
$batchID = 0;

if ($con->checkUserLogin() == true) {
    $userID = $_SESSION['customer_id'];
    $billing_fname = $_SESSION['first_name'];
    $billing_lname = $_SESSION['last_name'];
    $shipping_fname = $_SESSION['first_name'];
    $shipping_lname = $_SESSION['last_name'];
}

//get shipping address details from database
if ($userID > 0) {
    $arrayShipping = array();
    $sqlShipping = "SELECT * FROM shipping_address WHERE customer_id=$userID";
    $resultShipping = mysqli_query($con->open(), $sqlShipping);
    if ($resultShipping) {
        while ($resultShippingObj = mysqli_fetch_array($resultShipping)) {
            $shipping_id = $resultShippingObj['shipping_address_id'];
            $shipping_address = $resultShippingObj['shipping_address'];
            $shipping_phone = $resultShippingObj['shipping_phone'];
            $shipping_city = $resultShippingObj['shipping_city'];
            $shipping_zip = $resultShippingObj['shipping_country'];
        }
    } else {
        echo "resultShipping query failed.";
    }

    $arrayBilling = array();
    $sqlBilling = "SELECT * FROM billing_address WHERE customer_id=$userID";
    $resultBilling = mysqli_query($con->open(), $sqlBilling);
    if ($resultBilling) {
        while ($resultBillingObj = mysqli_fetch_array($resultBilling)) {
            $billing_id = $resultBillingObj['billing_address_id'];
            $billing_phone = $resultBillingObj['billing_phone'];
            $billing_address = $resultBillingObj['billing_address'];
            $billing_city = $resultBillingObj['billing_city'];
            $billing_zip = $resultBillingObj['billing_post_code'];
        }
    } else {
        echo "resultBilling query failed.";
    }
}



$sessionID = session_id();
$arrayWholeCheckout = array();
$sqlWholeCart = "SELECT * "
        . "FROM temp_carts_events "
        . "LEFT JOIN event ON event.event_id=temp_carts_events.TC_product_id "
        . "LEFT JOIN event_schedule ON event_schedule.event_schedule_id=temp_carts_events.TC_schedule_id "
        . "LEFT JOIN event_venue ON event_venue.event_id=temp_carts_events.TC_product_id "
        . "LEFT JOIN venue ON venue.venue_id=event_venue.venue_id "
        . "WHERE temp_carts_events.TC_session_id='$sessionID' "
        . "AND event_venue.is_active='true' "
        . "ORDER BY `temp_carts_events`.`TC_updated` DESC";
$resultWholeCart = mysqli_query($con->open(), $sqlWholeCart);
if ($resultWholeCart) {
    while ($resultWholeCartObj = mysqli_fetch_array($resultWholeCart)) {

        $TempCartID = $resultWholeCartObj['TC_id'];
        $eventID = $TmpCartEventID = $resultWholeCartObj['TC_product_id'];
        $TmpCartType = $resultWholeCartObj['TC_product_type'];
        $scheduleID = $TmpCartScheduleID = $resultWholeCartObj['TC_schedule_id'];
        if ($TmpCartType == "event") {
            $arrayWholeCheckout[]['event']['event_details'] = $resultWholeCartObj;
        } elseif ($TmpCartType == "subscribe") {
            $arrayWholeCheckout[]['subscribe']['subscribe_details'] = $resultWholeCartObj;
        }



        $sqlSelectCustAdd = "SELECT * FROM customer_addition WHERE CA_session_id='$sessionID'";
        $resultSelectCustAdd = mysqli_query($con->open(), $sqlSelectCustAdd);
        if ($resultSelectCustAdd) {
            $resultSelectCustAddObj = mysqli_fetch_array($resultSelectCustAdd);
            $programID = $resultSelectCustAddObj['CA_program_id'];
            $batchID = $resultSelectCustAddObj['CA_batch_id'];
        }

        //getting ticket type information
        $sqlGetTicktType = "SELECT * "
                . "FROM temp_cart_addition "
                . "LEFT JOIN batch ON batch.batch_id=$batchID "
                . "WHERE temp_cart_addition.TCA_session_id='$sessionID' "
                . "AND temp_cart_addition.TCA_item_id=$batchID "
                . "AND temp_cart_addition.TCA_TC_id=$TempCartID ";
        if ($TmpCartType == "event") {
            $sqlGetTicktType .= "AND temp_cart_addition.TCA_item_type='type' ";
        } else {
            $sqlGetTicktType .= "AND temp_cart_addition.TCA_item_type='subscribe' ";
        }

        $sqlGetTicktType .= "ORDER BY `temp_cart_addition`.`TCA_item_updated` DESC";

        $resultGetTicktType = mysqli_query($con->open(), $sqlGetTicktType);

        if ($resultGetTicktType) {
            while ($resultGetTicktTypeObj = mysqli_fetch_array($resultGetTicktType)) {
                if ($TmpCartType == "event") {
                    $arrayWholeCheckout[(count($arrayWholeCheckout) - 1)]['event']['event_addition'][$resultGetTicktTypeObj['TCA_item_type']][] = $resultGetTicktTypeObj;
                } elseif ($TmpCartType == "subscribe") {
                    $arrayWholeCheckout[(count($arrayWholeCheckout) - 1)]['subscribe']['subscribe_addition'][$resultGetTicktTypeObj['TCA_item_type']][] = $resultGetTicktTypeObj;
                }
            }
        } else {
            echo "resultGetTicktType query failed." . mysqli_error($con->open());
        }
    }
} else {
    echo "resultWholeCart query failed.";
}



if (count($arrayWholeCheckout) == 0) {
    $con->redirect('cart.php');
}

//echo "<pre>";
//print_r($arrayWholeCheckout);
//echo "</pre>";
//getting state information from database
$arrayState = array();
$sqlState = "SELECT * FROM delivery_cost "
        . "LEFT JOIN district_state ON district_state.dis_state_id=delivery_cost.dis_state_id "
        . "WHERE delivery_cost.is_active='true'";
$resultState = mysqli_query($con->open(), $sqlState);
if ($resultState) {
    while ($resultStateObj = mysqli_fetch_array($resultState)) {
        $arrayState[] = $resultStateObj;
    }
} else {
    echo "resultState query failed.";
}


//getting city information from database
$arrayCity = array();
$sqlCity = "SELECT * FROM city WHERE is_active='true'";
$resultCity = mysqli_query($con->open(), $sqlCity);
if ($resultCity) {
    while ($resultCityObj = mysqli_fetch_array($resultCity)) {
        $arrayCity[] = $resultCityObj;
    }
} else {
    echo "resultCity query failed.";
}

//echo "<pre>";
//print_r($arrayState);
//echo "</pre>";
?>
<?php include './header_script.php'; ?>
<body class="header-fixed">

    <div class="wrapper">
        <div class="header-v5 header-static">
            <?php include './menu_top.php'; ?>
            <?php include './header.php'; ?>
        </div>
        <!--=== Breadcrumbs v4 ===-->
        <!--        <div class="breadcrumbs-v4">
                    <div class="container">
                        <span class="page-name">Check Out</span>
                        <h1>Maecenas <span class="shop-green">enim</span> sapien</h1>
                        <ul class="breadcrumb-v4-in">
                            <li><a href="index.html">Home</a></li>
                            <li><a href="">Product</a></li>
                            <li class="active">Shopping Cart</li>
                        </ul>
                    </div>/end container
                </div> -->
        <!--=== End Breadcrumbs v4 ===-->

        <!--=== Content Medium Part ===-->
        <div class="content-md margin-bottom-30">
            <div class="container">
                <form class="shopping-cart">
                    <div>
                        <div class="header-tags">            
                            <div class="overflow-h">
                                <h2>Shopping Cart</h2>
                                <p>Review &amp; edit your product</p>
                                <i class="rounded-x fa fa-check"></i>
                            </div>    
                        </div>
                        <section>
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Product</th>
                                            <th>Details</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $subTotalPrice = 0; ?> 
                                        <?php $totalPrice = 0; ?>    
                                        <?php $subscribtionAmount = 0; ?>    
                                        <?php if (count($arrayWholeCheckout) > 0): ?>
                                            <?php foreach ($arrayWholeCheckout AS $CartInfo): ?>

                                                <?php if (isset($CartInfo['subscribe'])): ?>
                                                    <?php $subscribtionAmount = 10000; ?>

                                                    <tr>
                                                        <td class="product-in-table" style="margin-top: 10px !important;width: 500px;">
                                                            <img class="img-responsive" src="admin/uploads/event_logo_image/<?php echo $CartInfo['subscribe']['subscribe_details']['event_full_size_image']; ?>" alt="<?php echo $CartInfo['subscribe']['subscribe_details']['event_title']; ?>">
                                                            <div class="product-it-in">
                                                                <h3><strong>Lifetime Membership</strong> for DUDSAA</h3>

                                                            </div>    
                                                        </td>
                                                        <td>

                                                            <?php if (isset($CartInfo['subscribe']['subscribe_addition']['subscribe']) AND count($CartInfo['subscribe']['subscribe_addition']['subscribe'] > 0)): ?>
                                                                <table style="font-size: x-small; color: black;"  width="100%">
                                                                    <tr style="border-bottom: 2px #dedede solid; font-weight: bold;">
                                                                        <td width="50%">Type</td>
                                                                        <!--<td width="30%">Qty.</td>-->
                                                                        <td width="20%">Price</td>
                                                                    </tr>
                                                                    <?php foreach ($CartInfo['subscribe']['subscribe_addition']['subscribe'] AS $CartAdditionTypeInfo): ?>
                                                                        <tr>
                                                                            <td width="50%">Lifetime Subscription</td>
                                                                            <!--<td width="30%">1</td>-->
                                                                            <td width="20%">TK. <?php echo number_format($CartAdditionTypeInfo['TCA_item_total_price'], 2); ?></td>
                                                                        </tr>
                                                                    <?php endforeach; ?>    
                                                                </table>
                                                            <?php endif; ?>      
                                                        </td>
            <!--                                            <td>
                                                            <button type='button' class="quantity-button" name='subtract' onclick='javascript: subtractQty1();' value='-'>-</button>
                                                            <input type='text' class="quantity-field" name='qty1' value="5" id='qty1'/>
                                                            <button type='button' class="quantity-button" name='add' onclick='javascript: document.getElementById("qty1").value++;' value='+'>+</button>
                                                        </td>-->
                                                        <td class="shop-red">TK. <?php echo number_format($subscribtionAmount, 2); ?></td>
                                                        <td>
                                                            <!--<button type="button" class="close"><span style="font-size: 25px; color: rgb(208, 0, 0);">&times;</span><span class="sr-only">Close</span></button>-->
                                                        </td>
                                                    </tr>


                                                <?php else: ?>


                                                    <tr>
                                                        <td class="product-in-table" style="margin-top: 10px !important;width: 500px;">
                                                            <img class="img-responsive" src="admin/uploads/event_logo_image/<?php echo $CartInfo['event']['event_details']['event_full_size_image']; ?>" alt="<?php echo $CartInfo['event']['event_details']['event_title']; ?>">
                                                            <div class="product-it-in">
                                                                <h3><?php echo $CartInfo['event']['event_details']['event_title']; ?></h3>
                                                                <span style="font-size: x-small; color: #79b92d;">
                                                                    Date: <strong><?php echo date("d M, Y", strtotime($CartInfo['event']['event_details']['event_date'])); ?></strong><br/> 
                                                                    Venue: <strong><?php echo $CartInfo['event']['event_details']['venue_address']; ?></strong><br/>
                                                                    Time: <strong><?php echo $CartInfo['event']['event_details']['event_schedule_start_time']; ?> - <?php echo $CartInfo['event']['event_details']['event_schedule_end_time']; ?></strong>
                                                                </span>
                                                            </div>    
                                                        </td>
                                                        <td>

                                                            <?php if (isset($CartInfo['event']['event_addition']['type']) AND count($CartInfo['event']['event_addition']['type'] > 0)): ?>
                                                                <table style="font-size: x-small; color: black;"  width="100%">
                                                                    <tr style="border-bottom: 2px #dedede solid; font-weight: bold;">
                                                                        <td width="50%">Ticket Type</td>
                                                                        <td width="30%">Qty.</td>
                                                                        <td width="20%">Price</td>
                                                                    </tr>
                                                                    <?php foreach ($CartInfo['event']['event_addition']['type'] AS $CartAdditionTypeInfo): ?>
                                                                        <?php $totalPrice += $CartAdditionTypeInfo['ticket_price'] + $CartAdditionTypeInfo['anual_subscription'] + $CartAdditionTypeInfo['enrollment_fee']; ?>
                                                                        <tr>
                                                                            <td width="50%">Ticket Price</td>
                                                                            <td width="30%">1</td>
                                                                            <td width="20%">TK. <?php echo $ticket_unit_price = $CartAdditionTypeInfo['ticket_price']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="50%">Annual Subscription</td>
                                                                            <td width="30%">1</td>
                                                                            <td width="20%">TK. <?php echo $anual_subscription = $CartAdditionTypeInfo['anual_subscription']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="50%">Enrollment Fee</td>
                                                                            <td width="30%">1</td>
                                                                            <td width="20%">TK. <?php echo $enrollment_fee = $CartAdditionTypeInfo['enrollment_fee']; ?></td>
                                                                        </tr>
                                                                    <?php endforeach; ?>    
                                                                </table>
                                                            <?php endif; ?>    

                                                            <hr>

                                                            <?php // if(isset($CartInfo['event_addition']['include']) AND count($CartInfo['event_addition']['include'] > 0)):  ?>    
                        <!--                                                    <table style="font-size: x-small; color: black;"  width="100%">
                                                                                <tr style="border-bottom: 2px #dedede solid; font-weight: bold;">
                                                                                    <td width="50%">Includes</td>
                                                                                    <td width="30%">Qty.</td>
                                                                                    <td width="20%">Price</td>
                                                                                </tr>-->
                                                            <?php // foreach ($CartInfo['event_addition']['include'] AS $CartAdditionIncldInfo):  ?>
                                                            <?php // $totalPrice += $CartAdditionIncldInfo['TCA_item_total_price'];  ?>
                        <!--                                                        <tr>
                                                                                    <td width="50%"><?php echo $CartAdditionIncldInfo['event_includes_name']; ?></td>
                                                                                    <td width="30%"><?php echo $CartAdditionIncldInfo['TCA_item_quantity']; ?></td>
                                                                                    <td width="20%">TK. <?php echo $CartAdditionIncldInfo['TCA_item_total_price']; ?></td>
                                                                                </tr>-->
                                                            <?php // endforeach;  ?>    
                                                            <!--</table>-->
                                                            <?php // endif;  ?>    
                                                        </td>
            <!--                                            <td>
                                                            <button type='button' class="quantity-button" name='subtract' onclick='javascript: subtractQty1();' value='-'>-</button>
                                                            <input type='text' class="quantity-field" name='qty1' value="5" id='qty1'/>
                                                            <button type='button' class="quantity-button" name='add' onclick='javascript: document.getElementById("qty1").value++;' value='+'>+</button>
                                                        </td>-->
                                                        <td class="shop-red">TK. <?php echo number_format($totalPrice, 2); ?></td>
                                                        <td>
                                                            <!--<button type="button" class="close"><span style="font-size: 25px; color: rgb(208, 0, 0);">&times;</span><span class="sr-only">Close</span></button>-->
                                                        </td>
                                                    </tr>


                                                <?php endif; ?>
                                                <?php $subTotalPrice = $subscribtionAmount + $totalPrice; ?>
                                                <?php // $subTotalPrice += $totalPrice;  ?>
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                            <tr>
                                                <td colspan="5" class="text-center">
                                                    <h3>No item added to cart yet.</h3>
                                                </td>
                                            </tr>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </section>

                        <div class="header-tags">
                            <div class="overflow-h">
                                <h2>Billing & Shipping info</h2>
                                <p>Shipping and address infot</p>
                                <i class="rounded-x fa fa-home"></i>
                            </div>    
                        </div>
                        <section class="billing-info">
                            <?php if ($con->checkUserLogin() == FALSE): ?>
                                <div class="alert alert-warning fade in">
                                    <strong>Oopss!</strong> It seems you are not logged in. Please fill below form to proceed further.
                                </div>


                                <div class="row" style="margin-bottom: 60px;">
                                    <div class="col-md-5 md-margin-bottom-40">
                                        <h2 class="title-type"><strong>Sign in</strong></h2>
                                        <div class="billing-info-inputs checkbox-list">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <input id="loginEmail" type="text" placeholder="Email" name="email" class="form-control">
                                                </div>
                                                <div class="col-sm-12">
                                                    <input id="loginPass" type="password" placeholder="Password" name="password" class="form-control">
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-2 md-margin-bottom-40 text-center" style="margin-top: 80px;"> <h1>OR</h1></div>

                                    <div class="col-md-5 md-margin-bottom-40">
                                        <h2 class="title-type"><strong>Sign up</strong></h2>
                                        <div class="billing-info-inputs checkbox-list">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <input id="signupFname" type="text" placeholder="First Name" name="firstname" class="form-control">
                                                    <input id="signupEmail" type="text" placeholder="Email" name="email" class="form-control">
                                                </div>
                                                <div class="col-sm-6">
                                                    <input id="signupLname" type="text" placeholder="Last Name" name="lastname" class="form-control">
                                                    <input id="signupPassword" type="password" placeholder="Password" name="password" class="form-control">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            <?php endif; ?>



                            <div class="row">
                                <div class="col-md-12 md-margin-bottom-40">
                                    <h2 class="title-type">Select Delivery Method</h2>
                                    <div class="billing-info-inputs checkbox-list">
                                        <div class="row">
                                            <table class="col-lg-12" cellpadding="10">
                                                <tr style="border-bottom: 2px solid #79b92d;">
                                                    <td width="30%">Delivery Type</td>
                                                    <td width="45%">Details</td>
                                                    <td width="25%">Cost</td>
                                                </tr>
                                                <tr style="border-bottom: 1px solid #dedede;">
                                                    <td width="30%"><input checked="checked" onchange="javascript:checkMethod(this.value,<?php echo $subTotalPrice; ?>);" type="radio" name="method"  value="home" style="vertical-align: middle"><strong> Home Delivery</strong></td>
                                                    <td width="45%">Our courier service provider will delivery your tickets at your given delivery address</td>
                                                    <td width="25%">Area Dependent</td>
                                                </tr>
                                                <tr style="border-bottom: 1px solid #dedede;">
                                                    <td width="30%"><input onchange="javascript:checkMethod(this.value,<?php echo $subTotalPrice; ?>);" type="radio" name="method" value="collect" style="vertical-align: middle"><strong> Pick from DU</strong></td>
                                                    <td width="45%">Pick ticket from event venue or pre-defined point.</td>
                                                    <td width="25%">Free</td>
                                                </tr>
                                                <tr style="border-bottom: 1px solid #dedede;">
                                                    <td width="30%"><input onchange="javascript:checkMethod(this.value,<?php echo $subTotalPrice; ?>);" type="radio" name="method" value="pick" style="vertical-align: middle"><strong> Pick from office</strong></td>
                                                    <td width="45%">Pick from Ticketchai's office.</td>
                                                    <td width="25%">Free</td>
                                                </tr>
<!--                                                <tr style="border-bottom: 1px solid #dedede;">
                                                    <td width="30%"><input onchange="javascript:checkMethod(this.value);" type="radio" name="method" value="eticket" style="vertical-align: middle"><strong> e-Ticket</strong></td>
                                                    <td width="45%">You will get e-Ticket in email.</td>
                                                    <td width="25%">Free</td>
                                                </tr>-->
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="row">
                                <div class="col-md-6 md-margin-bottom-40">
                                    <h2 class="title-type">Billing Address</h2>
                                    <div class="billing-info-inputs checkbox-list">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <input value="<?php echo $billing_fname; ?>" id="billingFname" type="text" placeholder="First Name" name="billing_fname" class="form-control">
                                            </div>
                                            <div class="col-sm-6">
                                                <input value="<?php echo $billing_lname; ?>" id="billingLname" type="text" placeholder="Last Name" name="billing_lname" class="form-control">
                                            </div>
                                            <div class="col-sm-12">
                                                <input value="<?php echo $billing_phone; ?>" id="billingPhone" type="text" placeholder="Phone Number" name="billing_phone" class="form-control">
                                            </div>
                                        </div>
                                        <input value="<?php echo $billing_address; ?>" id="billingAddress" type="text" placeholder="Address Line 1" name="billing_address" class="form-control">
                                        <div class="row">
                                            <div class="col-sm-6">

                                                <input list="citylist" value="<?php echo $billing_city; ?>" id="billingCity" type="text" placeholder="City" name="billing_city" class="form-control">
                                                <datalist id="citylist">
                                                    <?php if (count($arrayCity) > 0): ?>
                                                        <?php foreach ($arrayCity AS $City): ?>
                                                            <?php if ($City['city_name'] != ""): ?>
                                                                <option value="<?php echo $City['city_name']; ?>">
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>    
                                                </datalist>
                                            </div>
                                            <div class="col-sm-6">
                                                <input value="<?php echo $billing_zip; ?>" id="billingZip" type="text" placeholder="Zip/Postal Code" name="billing_zip" class="form-control required">
                                            </div>

                                        </div>
                                    </div>
                                    <h5 style="color: red;"><strong>***Additional 4% will be added on total price while paying online.</strong></h5>
                                </div>

                                <div class="col-md-6">

                                    <!-- div show against delivery method -->
                                    <span id="divDelivery">
                                        <h2 class="title-type"><strong>Shipping Address</strong></h2>
                                        <div class="billing-info-inputs checkbox-list">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <input value="<?php echo $shipping_fname; ?>" id="shippingFname" type="text" placeholder="First Name" name="shipping_fname" class="form-control">
                                                </div>
                                                <div class="col-sm-6">
                                                    <input value="<?php echo $shipping_lname; ?>" id="shippingLname" type="text" placeholder="Last Name" name="shipping_lname" class="form-control">
                                                </div>
                                            </div>
                                            <input value="<?php echo $shipping_address; ?>" id="shippingAddress" type="text" placeholder="Address Line 1" name="shipping_address" class="form-control">
                                            <input value="<?php echo $shipping_phone; ?>" id="shippingPhone" type="text" placeholder="Phone No" name="shipping_phone" class="form-control">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <select name="shippingCity" id="shippingCity" class="form-control" onchange="javascript:getDelCost(this.value,<?php echo $subTotalPrice; ?>);">
                                                        <option value="">Select Area</option>
                                                        <?php if (count($arrayState) > 0): ?>
                                                            <?php foreach ($arrayState AS $state): ?>
                                                                <?php if ($state['dis_state_title'] != ""): ?>
                                                                    <option value="<?php echo $state['dis_state_id']; ?>"><?php echo $state['dis_state_title']; ?></option>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>    
                                                    </select>
                                                    <!--<input value="<?php echo $shipping_city; ?>" id="shippingCity" type="text" placeholder="City" name="shipping_city" class="form-control">-->
                                                </div>
                                                <div class="col-sm-6">
                                                    <input value="<?php echo $shipping_zip; ?>" id="shippingZip" type="text" placeholder="Zip/Postal Code" name="shipping_zip" class="form-control">
                                                </div>
                                                <input type="hidden" id="shippingID" value="<?php echo $shipping_id; ?>">
                                            </div>
                                        </div>
                                    </span>
                                    <!-- div show against delivery method -->

                                    <!-- div show against pick method -->
                                    <span style="display: none;" id="divPick">
                                        <h2 class="title-type"><strong>Pick Address</strong></h2>
                                        <div class="billing-info-inputs checkbox-list">
                                            <div class="row" style="padding: 15px;">
                                                <h4><strong>TicketChai</strong></h4>
                                                <address class="md-margin-bottom-40">
                                                    <i class="fa fa-home"></i>&nbsp;&nbsp;Razzak Plaza (8th Floor),1 New Eskaton <br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Road,Moghbazar Circle, Dhaka-1217 <br><br>
                                                    <i class="fa fa-phone"></i>&nbsp;&nbsp;Phone: +8801971842538, +8804478009569 <br><br>
                                                    <i class="fa fa-globe"></i>&nbsp;&nbsp;Website: <a href="http://www.ticketchai.org">www.ticketchai.org</a> <br><br>
                                                    <i class="fa fa-envelope"></i>&nbsp;&nbsp;Email: <a href="mailto:support@ticketchai.org">support@ticketchai.org</a> 
                                                </address>
                                            </div>
                                        </div>
                                    </span>
                                    <!-- div show against pick method -->


                                    <!-- div show against pick method -->
                                    <span style="display: none;" id="divCollect">
                                        <h2 class="title-type"><strong>Collect Address</strong></h2>
                                        <div class="billing-info-inputs checkbox-list">
                                            <div class="row" style="padding: 15px;">
                                                <?php if (count($arrayWholeCheckout) > 0): ?>
                                                    <?php foreach ($arrayWholeCheckout AS $CartItem): ?>
                                                        <?php if (isset($CartItem['subscribe'])): ?>                                        
                                                            <h4>Collect membership confirmation of <strong><?php echo $CartItem['subscribe']['subscribe_details']['event_title']; ?></strong></h4>
                                                            <address class="md-margin-bottom-40">
                                                                <i class="fa fa-home"></i>&nbsp;&nbsp;<?php //echo $CartItem['event_details']['venue_address'];  ?>Department of Development Studies University of Dhaka. 
                                                                <p style="color: red;"><b>Md.Billal Hossain, 01911203651</b></p>
                                                            </address>
                                                        <?php endif; ?>                                                
                                                        <?php if (isset($CartItem['event'])): ?>                                        
                                                            <h4>Collect ticket of <strong><?php echo $CartItem['event']['event_details']['event_title']; ?></strong></h4>
                                                            <address class="md-margin-bottom-40">
                                                                <i class="fa fa-home"></i>&nbsp;&nbsp;<?php //echo $CartItem['event_details']['venue_address'];   ?>Department of Development Studies University of Dhaka.
                                                                <p style="color: red;"><b>Md.Billal Hossain, 01911203651</b></p>
                                                            </address>
                                                        <?php endif; ?>                                                
                                                    <?php endforeach; ?>
                                                <?php endif; ?>        
                                            </div>
                                        </div>
                                    </span>
                                    <!-- div show against pick method -->



                                    <!-- div show against e-ticket method -->
                                    <span style="display: none;" id="divEtickt">
                                        <h2 class="title-type"><strong>e-Ticket</strong></h2>
                                        <div class="billing-info-inputs checkbox-list">
                                            <div class="row" style="padding: 15px;">
                                                <h4><strong>A PDF version of the ticket will be send to your email upon payment confirmation.</strong></h4>

                                            </div>
                                        </div>
                                    </span>
                                    <!-- div show against e-ticket method -->

                                </div>
                            </div>       
                        </section>

                        <div class="header-tags">
                            <div class="overflow-h">
                                <h2>Payment</h2>
                                <p>Select Payment method</p>
                                <i class="rounded-x fa fa-credit-card"></i>
                            </div>    
                        </div>
                        <section>
                            <div class="row">
                                <div class="col-md-6 md-margin-bottom-50">
                                    <h2 class="title-type">Choose a payment method</h2>
                                    <!-- Accordion -->
                                    <div class="accordion-v2">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default" style="border: 1px #79b92d solid;">
                                                <div class="panel-heading" style="padding: 15px;">
                                                    <h4 class="panel-title">
                                                        <input type="radio" name="payment" value="card">
                                                        <i class="fa fa-credit-card"></i>
                                                        Online Payment

                                                    </h4>
                                                </div>
                                                <div>
                                                    <div class="panel-body cus-form-horizontal">
                                                        You have to make the payment by your credit or debit card through a secure and reliable payment gateway service.
                                                        <h6 style="color: red;"><strong>***Additional 4% will be added on total price while paying online.</strong></h6>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="border: 1px #79b92d solid;">
                                                <div class="panel-heading" style="padding: 15px;">
                                                    <h4 class="panel-title">
                                                        <input type="radio" name="payment" value="cash">
                                                        <i class="fa fa-money"></i>
                                                        Cash on Delivery
                                                    </h4>
                                                </div>
                                                <div>
                                                    <div class="panel-body cus-form-horizontal">
                                                        One of our representative will contact with you and will collect money from your given address within 48 hours of purchase.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Accordion -->    
                                </div>

                                <div class="col-md-6">
                                    <h2 class="title-type">Frequently asked questions</h2>
                                    <!-- Accordion -->
                                    <div class="accordion-v2 plus-toggle">
                                        <div class="panel-group" id="accordion-v2">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion-v2" href="#collapseOne-v2">
                                                            What payments methods can I use?
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne-v2" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        If you have valid Debit Card or Credit, you can use <strong>"Online Payment"</strong> method. Otherwise you can use <strong>"Cash on Delivery"</strong> method.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion-v2" href="#collapseThree-v2">
                                                            Will I be charged when I place my order?
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThree-v2" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        Yes, after completing payment form provided by <strong>"SSLCommerz"</strong> on next step, you will be charged immediately;
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion-v2" href="#collapseFive-v2">
                                                            How long will it take to get my order?
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseFive-v2" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <ul>
                                                            <li>** If you choose <strong>"Home Delivery"</strong> method from previous step, our delivery service will contact with you via your phone no. and he will deliver your ticket at your given delivery address.</li>
                                                            <li>** If you choose <strong>"Pick from DU"</strong> method from previous step, you have to pick the ticket from pre-defined pickup point shown on Previous step.</li>
                                                            <li>** If you choose <strong>"Pick from office"</strong> method from previous step, you have to pick the ticket from TicketChai's office shown on Previous step.</li>
                                                            <li>** If you choose <strong>"e-Ticket"</strong> method from previous step, upon payment confirmation, you will get your ticket via email.</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>    
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion-v2" href="#collapseFour-v2">
                                                            Where can i get help from?
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseFour-v2" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        If you need any help or assistance, please call to below numbers:<br><br>
                                                        <strong>TicketChai Hotline:</strong> +8801971842538, +8804478009569<br>
                                                        <strong>DUDSAA Hotline:</strong> +8801716020445, +8801624690104<br>
                                                    </div>
                                                </div>
                                            </div>    
                                        </div>
                                    </div>
                                    <!-- End Accordion -->    
                                </div>
                            </div>
                        </section>

                        <div class="coupon-code">
                            <div class="row">
                                <div class="col-sm-4 sm-margin-bottom-30">
                                </div>
                                <div class="col-sm-3 col-sm-offset-5">
                                    <ul class="list-inline total-result">
                                        <li>
                                            <h4>Subtotal:</h4>
                                            <div class="total-result-in">
                                                <span>TK. <?php echo number_format($subTotalPrice, 2); ?></span>
                                            </div>    
                                        </li>

                                        <li class="divider"></li>
                                        <li id="shipping_cost_div" style="display: none;" >
                                            <h4>Shipping Cost</h4>
                                            <div class="shipping_cost" style="margin-top: 6px;text-align: right;font-size: 20px;color:graytext;"></div>
                                        </li>
                                        <li class="divider"></li>
                                        <li class="total-price">
                                            <h4>Total:</h4>
                                            <div class="total-result-in" id="grand_total_value_price">
                                                <span>TK. <?php echo number_format($subTotalPrice, 2); ?></span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="button-actions clearfix text-right" style="padding-top: 50px !important;">
                                <button id="btnPrev" disabled="disabled" type="button" style="padding: 15px; font-size: medium;" class="btn-u btn-u-sea-shop btn-u-lg text-right"><i style="color: white;" class="glyphicon glyphicon-arrow-left"></i>&nbsp;&nbsp;Previous Step</button>
                                <button id="btnNext" onclick="javascript:goSecondStep();" type="button" style="padding: 15px; font-size: medium;" class="btn-u btn-u-sea-shop btn-u-lg text-right">Next Step&nbsp;&nbsp;<i style="color: white;" class="glyphicon glyphicon-arrow-right"></i></button>

                            </div>



                        </div>    
                    </div>
                </form>
            </div><!--/end container-->

        </div>

        <form action="https://www.sslcommerz.com.bd/process/index.php" method="post" name="form1" id="sslform">
            <input type="hidden" name="store_id" value="ticketchailive001"> 
            <?php
//ticketchailive001
//systechunimaxtest001
            ?>
            <input type="hidden" id="total_amount_ssl" name="total_amount" value="">
            <input type="hidden" id="trans_id_ssl" name="tran_id" value="">
            <input id="notify_url" type="hidden" name="success_url" value="">
            <input type="hidden" id="fail_url" name="fail_url" value = "">
            <input type="hidden" id="cancle_url" name="cancel_url" value = "">
            <!--<input class="pull-right" id="ssl_main" type="submit" value="Pay Online"  style="display: none;"  name="pay">-->
        </form>


        <input type="hidden" id="billingID" value="<?php echo $billing_id; ?>">
        <input type="hidden" id="shippingID" value="<?php echo $shipping_id; ?>">
        <input type="hidden" id="eventID" value="<?php echo $eventID; ?>">
        <input type="hidden" id="userID" value="<?php echo $userID; ?>">
        <input type="hidden" id="scheduleID" value="<?php echo $scheduleID; ?>">
        <input type="hidden" id="subTotalPrice" value="<?php echo $subTotalPrice; ?>">
        <input type="hidden" id="BatchID" value="<?php echo $batchID; ?>">
        <input type="hidden" id="ProgramID" value="<?php echo $programID; ?>">
        <input type="hidden" id="enrollment_fee" value="<?php echo $enrollment_fee; ?>">
        <input type="hidden" id="anual_subscription" value="<?php echo $anual_subscription; ?>">
        <input type="hidden" id="ticket_unit_price" value="<?php echo $ticket_unit_price; ?>">
        <input type="hidden" id="lifetimeSubscription" value="<?php echo $subscribtionAmount; ?>">
        <input type="hidden" id="delivery_cost_for_ticket" value="">
        <input type="hidden" id="deli_cost" value="">

        <!--=== End Content Medium Part ===-->     

        <?php include './newsletter.php'; ?>

        <?php include './footer.php'; ?>
    </div><!--/wrapper-->

    <script>
        function checkMethod(id, price) {

            if (id == "home") {

                var delCost = parseFloat(0);
                var delFrmHiddenVal = $("#deli_cost").val();
                if (delFrmHiddenVal != "") {
                    delCost = delFrmHiddenVal;
                }

                var total_price = parseFloat(delCost) + parseFloat(price);
                var totalPrice = total_price.toFixed(2);

                $("#delivery_cost_for_ticket").val(delCost);
                $("#shipping_cost_div").show();
                $(".shipping_cost").show();
                $(".shipping_cost").html(delCost);
                $("#grand_total_value_price").html('<span>TK.' + totalPrice + '</span>');
                $("#subTotalPrice").val(price);


                $('#divDelivery').fadeIn('slow');
                $('#divPick').fadeOut();
                $('#divCollect').fadeOut();
                $('#divEtickt').fadeOut();
            } else if (id == "collect") {

                $("#shipping_cost_div").hide();
                $(".shipping_cost").hide('');
////                $(".shipping_cost").html(shipping_cost.toFixed(2));
//                $("#grand_total_value_price").html('');
                $("#grand_total_value_price").html('<span>TK.' + price.toFixed(2) + '</span>')
                $("#subTotalPrice").val(price);
                $("#delivery_cost_for_ticket").val(0);

                $('#divDelivery').fadeOut();
                $('#divPick').fadeOut();
                $('#divEtickt').fadeOut();
                $('#divCollect').fadeIn('slow');
            } else if (id == "pick") {

                $("#shipping_cost_div").hide();
                $(".shipping_cost").hide('');
////                $(".shipping_cost").html(shipping_cost.toFixed(2));
//                $("#grand_total_value_price").html('');
                $("#grand_total_value_price").html('<span>TK.' + price.toFixed(2) + '</span>')
                $("#subTotalPrice").val(price);
                $("#delivery_cost_for_ticket").val(0);


                $('#divDelivery').fadeOut();
                $('#divPick').fadeIn('slow');
                $('#divCollect').fadeOut();
                $('#divEtickt').fadeOut();
            } else if (id == "eticket") {

                $("#shipping_cost_div").hide();
                $(".shipping_cost").hide('');
////                $(".shipping_cost").html(shipping_cost.toFixed(2));
//                $("#grand_total_value_price").html('');
                $("#grand_total_value_price").html('<span>TK.' + price.toFixed(2) + '</span>')
                $("#subTotalPrice").val(price);
                $("#delivery_cost_for_ticket").val(0);


                $('#divDelivery').fadeOut();
                $('#divPick').fadeOut();
                $('#divCollect').fadeOut();
                $('#divEtickt').fadeIn('slow');
            }
        }

        jQuery(document).ready(function () {
            $('.actions').remove();
        });
    </script>

    <script>
        function goSecondStep() {

            $('#btnPrev').removeAttr('disabled');
            $('#btnNext').removeAttr('onclick');
            $('#btnNext').attr("onclick", "javascript:goThirdStep();");
            $('#steps-uid-0-p-0').hide();
            $('#steps-uid-0-p-1').show('slow');
            $('.first').addClass("done").removeClass("first");
            $(".done").removeClass("current");
            $('.disabled').addClass("current").removeClass("disabled");
            $(".last").addClass("disabled").removeClass("current");
        }


        function goThirdStep() {

            var userID = <?php echo ($con->checkUserLogin() == "true") ? $_SESSION['customer_id'] : 0; ?>;
//        var loginEmail = $("#loginEmail").val();
//        var loginPass = $("#loginPass").val();
//        var signupFname = $("#signupFname").val();
//        var signupLname = $("#signupLname").val();
//        var signupEmail = $("#signupEmail").val();
//        var signupPass = $("#signupPassword").val();
            var firstName = $("#billingFname").val();
            var lastName = $("#billingLname").val();
            var phoneNo = $("#billingPhone").val();
            var City = $("#billingCity").val();
            var Zip = $("#billingZip").val();
            var Address = $("#billingAddress").val();




            //checking customer billing information

            if (firstName == "") {
                $("#billingFname").addClass("input-error");
            } else {
                $("#billingFname").removeClass("input-error");
            }

            if (lastName == "") {
                $("#billingLname").addClass("input-error");
            } else {
                $("#billingLname").removeClass("input-error");
            }

            if (phoneNo == "") {
                $("#billingPhone").addClass("input-error");
            } else {
                $("#billingPhone").removeClass("input-error");
            }

            if (City == "") {
                $("#billingCity").addClass("input-error");
            } else {
                $("#billingCity").removeClass("input-error");
            }

            if (Zip == "") {
                $("#billingZip").addClass("input-error");
            } else {
                $("#billingZip").removeClass("input-error");
            }

            if (Address == "") {
                $("#billingAddress").addClass("input-error");
            } else {
                $("#billingAddress").removeClass("input-error");
            }

            //checking customer billing information


            //checking customer shipping information
            if (firstName != "" && lastName != "" && phoneNo != "" && City != "" && Zip != "" && Address != "") {

                //checking if home delivery enabled
                var checkMethodVar = $("input:radio[name='method']:checked").val();
                if (checkMethodVar == "home") {
                    var shippingFirstName = $("#shippingFname").val();
                    var shippingLastName = $("#shippingLname").val();
                    var shippingPhoneNo = $("#shippingPhone").val();
                    var shippingCity = $("#shippingCity").val();
                    var shippingZip = $("#shippingZip").val();
                    var shippingAddress = $("#shippingAddress").val();

                    if (shippingFirstName == "") {
                        $("#shippingFname").addClass("input-error");
                    } else {
                        $("#shippingFname").removeClass("input-error");
                    }

                    if (shippingLastName == "") {
                        $("#shippingLname").addClass("input-error");
                    } else {
                        $("#shippingLname").removeClass("input-error");
                    }

                    if (shippingPhoneNo == "") {
                        $("#shippingPhone").addClass("input-error");
                    } else {
                        $("#shippingPhone").removeClass("input-error");
                    }

                    if (shippingCity == "") {
                        $("#shippingCity").addClass("input-error");
                    } else {
                        $("#shippingCity").removeClass("input-error");
                    }

                    if (shippingZip == "") {
                        $("#shippingZip").addClass("input-error");
                    } else {
                        $("#shippingZip").removeClass("input-error");
                    }

                    if (shippingAddress == "") {
                        $("#shippingAddress").addClass("input-error");
                    } else {
                        $("#shippingAddress").removeClass("input-error");
                    }
                }


                if (shippingFirstName != "" && shippingLastName != "" && shippingPhoneNo != "" && shippingCity != "" && shippingZip != "" && shippingAddress != "") {
                    $('#btnNext').removeAttr('onclick');
                    $('#btnNext').attr("onclick", "javascript:saveInformation();");
                    $('#steps-uid-0-p-1').hide();
                    $('#steps-uid-0-p-2').show('slow');
                    $('.current').addClass("done").removeClass("current");
                    $(".last ").addClass("current").removeClass("disabled");
                    $('.current').addClass("last");
                }
            }
        }


        function saveInformation() {
            var checkPayment = $("input:radio[name='payment']:checked").val();

            if (checkPayment == "" || typeof checkPayment == "undefined") {
                dropDownBox("error", "Please select a payment method before proceed.");
            } else {
                if (checkPayment == "card") {
                    dropDownBox("success", "Thank you!! You will be redirect to payment page within a while.");
                } else {
                    dropDownBox("success", "Thank you!! You will be redirect to confirmation page within a while.");
                }

                var customerID = $('#userID').val();
                var eventID = $('#eventID').val();
                var shippingID = $('#shippingID').val();
                var billingID = $('#billingID').val();
                console.log(billingID);
                console.log(shippingID);
                var scheduleID = $('#scheduleID').val();
                var subTotalPrice = $('#subTotalPrice').val();
                var delivery_method = $('input[name=method]:checked').val();
                var delMethod = $("input[name=payment]:checked").val();
                var delivery_cost_for_ticket = $("#delivery_cost_for_ticket").val();

                var shippingFirstName = $("#shippingFname").val();
                var shippingLastName = $("#shippingLname").val();

                var shippingCity = $("#shippingCity").val();
                var shippingZip = $("#shippingZip").val();
                var shippingAddress = $("#shippingAddress").val();
                var enrollment_fee = $("#enrollment_fee").val();
                var anual_subscription = $("#anual_subscription").val();
                var ticket_unit_price = $("#ticket_unit_price").val();

                var firstName = $("#billingFname").val();
                var lastName = $("#billingLname").val();
                var shippingPhoneNo = $("#shippingPhone").val();
                var lifetimeSubscription = $("#lifetimeSubscription").val();

                if (shippingPhoneNo === "" || shippingPhoneNo === null) {
                    shippingPhoneNo = $("#billingPhone").val();
                }
                var phoneNo = shippingPhoneNo;
                var City = $("#billingCity").val();
                var Zip = $("#billingZip").val();
                var Address = $("#billingAddress").val();
                console.log(delivery_method);
//                return false;

                $.ajax({
                    type: "POST",
                    url: "ajax/ajaxSaveOrder.php",
                    dataType: "json",
                    data: {
                        customerID: customerID,
                        eventID: eventID,
                        shippingID: shippingID,
                        scheduleID: scheduleID,
                        billingID: billingID,
                        checkPayment: checkPayment,
                        subTotalPrice: subTotalPrice,
                        enrollment_fee: enrollment_fee,
                        anual_subscription: anual_subscription,
                        ticket_unit_price: ticket_unit_price,
                        delivery_method: delivery_method,
                        delMethod: delMethod,
                        shippingFirstName: shippingFirstName,
                        shippingLastName: shippingLastName,
                        shippingPhoneNo: shippingPhoneNo,
                        shippingCity: shippingCity,
                        shippingZip: shippingZip,
                        delivery_cost_for_ticket: delivery_cost_for_ticket,
                        lifetimeSubscription: lifetimeSubscription,
                        shippingAddress: shippingAddress,
                        firstName: firstName,
                        lastName: lastName,
                        phoneNo: phoneNo,
                        City: City,
                        Zip: Zip,
                        Address: Address
                    },
                    success: function (response) {
                        var obj = response;

//                         {"output":"success","delivery_method":"online_payment","booking_id":"5","total_amount":"270","customer_id":"6"}
                        // dropDownBox(obj.output, obj.msg);
                        if (obj.output === "success") {
                            var booking_id = obj.booking_id;
                            var total_amount = obj.total_amount;
                            var customer_id = obj.customer_id;

                            if (obj.delivery_method === "online_payment") {

                                $("#total_amount_ssl").val(total_amount);
                                $("#trans_id_ssl").val(booking_id);
                                var notify_url = "http://www.ticketchai.org/notify.php?itemid=" + booking_id + "&customer_id=" + customer_id + "&total_amount=" + total_amount;
                                $("#notify_url").val(notify_url);
                                var fail_url = "http://www.ticketchai.org/fail.php?itemid=" + booking_id + "&customer_id=" + customer_id + "&total_amount=" + total_amount;
                                $("#fail_url").val(fail_url);
                                var cancle_url = "http://www.ticketchai.org/cancel.php?itemid=" + booking_id + "&customer_id=" + customer_id + "&total_amount=" + total_amount;
                                $("#cancle_url").val(cancle_url);
                                if (total_amount !== "" && booking_id !== "" && customer_id !== "") {
                                    $("form#sslform").submit();
                                }

                            } else if (obj.delivery_method === "home_delivery") {

                                window.location.href = "thankyou.php?itemid=" + booking_id + "&customer_id=" + customer_id + "&total_amount=" + total_amount;

                            }

//                        if()
                            $('.k-window').fadeOut();
                            $('.k-overlay').fadeOut();
                            $('#register_form').replaceWith('<a href="javascript:void();" onclick="javascript:userLogout();">Logout</a>');
                            $('#login_form').replaceWith('<a href="customer_dashboard.php">' + obj.first_name + '</a>');
                            dropDownBox(obj.output, obj.msg);
//                        $("#login_form").text(obj.first_name);
                        } else {
                            dropDownBox(obj.output, obj.msg);
                        }

                    }
                });
            }
        }
    </script>




    <script>
        function getDelCost(id, price) {
            var subTotalVal = parseFloat(price);
            $.ajax({
                url: "ajax/ajaxGetDeliveryCost.php",
                type: "POST",
                data: {id: id},
                dataType: "json",
                success: function (result) {
                    var obj = result;
                    console.log(obj);
                    if (obj.output === "success") {
                        var shipping_cost = parseFloat(obj.delCost);

                        var totalVal = subTotalVal + shipping_cost;
                        var total_price = totalVal.toFixed(2);
                        $("#shipping_cost_div").show();
                        $(".shipping_cost").html('');
                        $(".shipping_cost").html(shipping_cost.toFixed(2));
                        $("#grand_total_value_price").html('');
                        $("#grand_total_value_price").html('<span>TK.' + total_price + '</span>')
                        $("#subTotalPrice").val(totalVal);
                        $("#delivery_cost_for_ticket").val(shipping_cost);
                        $("#deli_cost").val(shipping_cost.toFixed(2));
                    }
                }
            });
        }
    </script>
</body>
</html> 