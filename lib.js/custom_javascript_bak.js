//dropdown message/error/warning message function
function dropDownBox(type, message) {
    if (type == "success") {

        $("#message-suc").text(message);
        $("#success-msg").slideDown("slow");
        setTimeout(function () {
            $("#success-msg").slideUp("slow");
        }, 1600);

    } else if (type == "error") {

        $("#message-err").text(message);
        $("#error-msg").slideDown("slow");
        setTimeout(function () {
            $("#error-msg").slideUp("slow");
        }, 1600);

    } else if (type == "warning") {

        $("#message-war").text(message);
        $("#warning-msg").slideDown("slow");
        setTimeout(function () {
            $("#warning-msg").slideUp("slow");
        }, 1600);

    } else {

        $("#message-err").text("Message types are 'success'/'error'/'warning'. Please define type.");
        $("#error-msg").slideDown("slow");
        setTimeout(function () {
            $("#error-msg").slideUp("slow");
        }, 5000);

    }
}
//dropdown message/error/warning message function

function getCurrentPage() {
    var path = window.location.pathname;
    var page = path.split("/").pop();
    return page;
}


//Registration complete Function Start Here
function KendoWindowFunction(msg, status) {

    var window = jQuery("#kWindow");
    if (!window.data("kendoWindow")) {
        window.kendoWindow({
            title: "Message Window",
            modal: true,
            height: 100,
            width: 400
        });
    }
    window.data("kendoWindow").center().open();
    var okHtml = '';
    if (status === "error") {
        okHtml = '<br/><p style="color:red;font-weight:bold;text-align:center;">' + msg + '</p>';
    } else {
        okHtml = '<div id="message_div"><br/><p id="msg_display" style="color:green;font-weight:bold;text-align:center;">' + msg + '</p>';
        okHtml += '<div class="clearfix"></div>';
        okHtml += '<a href="javascript:void(0);" id="btnReload" class="k-button pull-right">OK</a>';
        okHtml += '<script type="text/javascript">';
        //okHtml += ' jQuery(document).ready(function(){';
        //okHtml += '  jQuery("#btnReload").click(function(){';
        //okHtml += '    window.location.reload();';
        //okHtml += '  });';
        //okHtml += '    });';
        okHtml += '</script>';
        okHtml += '</div>';
    }
    window.html(okHtml);
    this.cancelChanges();
}

//Registration complete Function End Here

//Sign In complete Function Start Here
function KendoWindowFunctionLogin(msg, status) {

    var window = jQuery("#kWindow");
    if (!window.data("kendoWindow")) {
        window.kendoWindow({
            title: "Message Window",
            modal: true,
            height: 100,
            width: 400
        });
    }
    window.data("kendoWindow").center().open();

    var okHtml = '';
    if (status === "error") {
        okHtml = '<br/><p style="color:green;font-weight:bold;text-align:center;">' + msg + '</p>';
    } else {
        okHtml = '<div id="message_div"><br/><p id="msg_display" style="color:green;font-weight:bold;text-align:center;">' + msg + '</p>';
        okHtml += '<div class="clearfix"></div>';
        okHtml += '<a href="javascript:void(0);" id="btnReload" class="k-button pull-right">OK</a>';
        okHtml += '<script type="text/javascript">';
        okHtml += ' jQuery(document).ready(function(){';
        okHtml += '  jQuery("#btnReload").click(function(){';
        okHtml += '    window.location.reload();';
        okHtml += '  });';
        okHtml += '    });';
        okHtml += '</scr' + 'ipt>';
        okHtml += '</div>';
    }
    window.html(okHtml);
    this.cancelChanges();
}

//Sign In complete Function End Here


//Create Account Ajax Code Start Here
jQuery(document).ready(function () {
    jQuery("#create_account").click(function () {
        var first_name = jQuery("#first_name").val();
        var last_name = jQuery("#last_name").val();
        //var gender = jQuery("#gender").val();
        //var month = jQuery("#month").val();
        //var day = jQuery("#day").val();
        //var year = jQuery("#year").val();
        // var dob = year + "-" + month + "-" + day;
        var email = jQuery("#email").val();
        var password = jQuery("#password").val();
        //var retype_password = jQuery("#retype_password").val();
        var terms_and_conditions = jQuery("#terms_and_conditions")
        if (first_name === "") {
            jQuery("#first_invalid").html('');
            jQuery("#first_name").css("background-color", "#FFF0F0");
            jQuery("#first_name").after('<em style="color:red;margin-left:9px;" id="first_invalid" for="first_name">Please enter first name</em>');
        }
        if (last_name === "") {
            jQuery("#last_invalid").html('');
            jQuery("#last_name").css("background-color", "#FFF0F0");
            jQuery("#last_name").after('<em style="color:red;margin-left:15px;" id="last_invalid" for="last_name">Please enter last name</em>');
        }
        if (email === "") {
            jQuery("#email_invalid").html('');
            jQuery("#email").css("background-color", "#FFF0F0");

            jQuery("#email").after('<em style="color:red;margin-left:1px;" id="email_invalid" for="email">Please enter email address</em>');

        }
        if (email !== "") {

            jQuery("#email").css("background-color", "white");
        }
        if (password === "") {
            jQuery("#password_invalid").html('');
            jQuery("#password").css("background-color", "#FFF0F0");
            jQuery("#password").after('<em style="color:red;margin-left:1px;" id="password_invalid" for="password">Please enter password</em>');
        }
//        if (retype_password === "") {
//            jQuery("#retype_password_invalid").html('');
//            jQuery("#retype_password").css("background-color", "#FFF0F0");
//            jQuery("#retype_password").after('<em style="color:red;margin-left:1px;" id="retype_password_invalid" for="retype_password">Please confirm password</em>');
//        }
//        if (password !== retype_password) {
//            jQuery("#retype_password_invalid").html('');
//            jQuery("#retype_password").css("background-color", "#FFF0F0");
//            jQuery("#retype_password").after('<em style="color:red;margin-left:1px;" id="retype_password_invalid" for="retype_password">Please mismatched</em>');
//        }
        if (first_name !== "" && last_name !== "" && email !== "" && password !== "") {

            $.ajax({
                type: "POST",
                url: "ajax/save_registration_data.php",
                dataType: "json",
                data: {
                    first_name: first_name,
                    last_name: last_name,
                    email: email,
                    password: password
                            //gender: gender,
                            //dob: dob
                },
                success: function (response) {
                    var obj = response;
                    console.log(response.msg);
                    if (obj.output === "success") {
                        $('.k-window').fadeOut();
                        $('.k-overlay').fadeOut();
                        $('#register_form').replaceWith('<a href="javascript:void();" onclick="javascript:userLogout();">Logout</a>');
                        $('#login_form').replaceWith('<a href="customer_dashboard.php">' + obj.first_name + '</a>');
                        dropDownBox(obj.output, obj.msg);
//                        $("#login_form").text(obj.first_name);
                    } else {
                        dropDownBox(obj.output, obj.msg);
                    }
                }
            });
        }
    });
    jQuery("#first_name").keyup(function () {
        var first_name = jQuery("#first_name").val();
        if (first_name !== "") {
            jQuery("#first_name").css("background-color", "white");
            jQuery("#first_invalid").html('');
        } else {
            jQuery("#first_invalid").html('');
            jQuery("#first_name").css("background-color", "#FFF0F0");
            jQuery("#first_name").after('<em style="color:red;margin-left:6px;" id="first_invalid" for="first_name">Please enter first name</em>');
        }
    });
    jQuery("#last_name").keyup(function () {
        var last_name = jQuery("#last_name").val();
        if (last_name !== "") {
            jQuery("#last_name").css("background-color", "white");
            jQuery("#last_invalid").html('');
        } else {
            jQuery("#last_invalid").html('');
            jQuery("#last_name").css("background-color", "#FFF0F0");
            jQuery("#last_name").after('<em style="color:red;margin-left:12px;" id="last_invalid" for="last_name">Please enter last name</em>');
        }
    });
    jQuery("#email").keyup(function () {
        var email = jQuery("#email").val();
        if (email !== "") {
            jQuery("#email").css("background-color", "white");
            jQuery("#email_invalid").html('');
        } else {
            jQuery("#email_invalid").html('');
            jQuery("#email").css("background-color", "#FFF0F0");
            jQuery("#email").after('<em style="color:red;margin-left:1px;" id="email_invalid" for="email">Please enter last name</em>');
        }
    });
    jQuery("#email").keyup(function () {
        var email = jQuery("#email").val();
        if (email !== "") {
            jQuery("#email").css("background-color", "white");
            if (validateEmail(email)) {
                jQuery.post("ajax/check_email_address.php", {email: email},
                function (result) {
                    if (result == 1) {
                        jQuery("#email_invalid").html('');
                        jQuery("#email").after('<em style="color:green;margin-left:1px;" id="email_invalid" for="email">Email Address Available</em>');
                    }
                    else {
                        jQuery("#email_invalid").html('');
                        jQuery("#email").after('<em style="color:green;margin-left:1px;" id="email_invalid" for="email">Email Address Already Exists</em>');
                    }
                });
            } else {
                jQuery("#email_invalid").html('');
                jQuery("#email").after('<em style="color:red;margin-left:1px;" id="email_invalid" for="email">Invalid Email Address</em>');
            }

        } else {
            jQuery("#email_invalid").html('');
            jQuery("#email").css("background-color", "#FFF0F0");
            jQuery("#email").after('<em style="color:red;margin-left:1px;" id="email_invalid" for="email">Please enter email address</em>');
        }
    });
    jQuery("#password").keyup(function () {
        var password = jQuery("#password").val();
        if (password !== "") {
            jQuery("#password").css("background-color", "white");
            jQuery("#password_invalid").html('');
        } else {
            jQuery("#password_invalid").html('');
            jQuery("#password").css("background-color", "#FFF0F0");
            jQuery("#password").after('<em style="color:red;margin-left:6px;" id="password_invalid" for="password">Please enter password</em>');
        }
    });
//    jQuery("#retype_password").keyup(function () {
//        var retype_password = jQuery("#retype_password").val();
//        if (retype_password !== "") {
//            jQuery("#retype_password").css("background-color", "white");
//            jQuery("#retype_password_invalid").html('');
//        } else {
//            jQuery("#retype_password_invalid").html('');
//            jQuery("#retype_password").css("background-color", "#FFF0F0");
//            jQuery("#retype_password").after('<em style="color:red;margin-left:1px;" id="retype_password_invalid" for="retype_password">Please confirm password</em>');
//        }
//    });
});
//Create Account Ajax Code End Here





// Email Address Validation Function Start
function validateEmail(email) {
    var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    var valid = emailReg.test(email);
    if (!valid) {
        return false;
    } else {
        return true;
    }
}

// Email Address Validation Function End




//Add to Wishlist code start
function addToWishlist(id) {
    var event_id = id;

    $.ajax({
        type: "POST",
        url: baseUrl + "/ajax/ajaxAddToWishlist.php",
        dataType: "json",
        data: {
            WL_product_id: event_id,
            WL_product_type: "event"
        },
        success: function (response) {
            var obj = response;
            if (obj.output == "login" && obj.msg == "login") {
                var window = jQuery("#login_window").kendoWindow({
                    width: "505px",
                    height: "auto",
                    visible: false,
                    modal: true,
                    title: "Login To Your Account"
                });
                window.data("kendoWindow").center().open();
                dropDownBox('error', "You need to login first.");
            } else {
                dropDownBox(obj.output, obj.msg);
            }
        }
    });
}
//Add to Wishlist code end

// function for save user review data start

function saveUserReview(id, customer_id) {
    var event_id = id;
    var customer_id = customer_id;
    var ratings = $('.radio1:checked').val();
    var review = jQuery("#review").val();
    if (typeof ratings == "undefined") {
        ratings = 0;
    }

    if (review == "") {
        jQuery("#review_error").html('');
        jQuery("#review").css("background-color", "#FFF0F0");
        jQuery("#review").after('<em style="color:red;margin-left:9px;" id="review_error" for="review">Please enter your review </em>');
    } else {
        $.ajax({
            type: "POST",
            url: baseUrl + "/ajax/ajax_SaveUserReview.php",
            dataType: "json",
            data: {
                event_id: event_id,
                user_id: customer_id,
                ratings: ratings,
                review: review

            },
            success: function (response) {
                var obj = response;
                if (obj.output == "error" && obj.msg == "login") {
                    var window = jQuery("#login_window").kendoWindow({
                        width: "505px",
                        height: "auto",
                        visible: false,
                        modal: true,
                        title: "Login To Your Account"
                    });
                    window.data("kendoWindow").center().open();
                    dropDownBox(obj.output, "You need to login first");
                } else {
                    dropDownBox(obj.output, obj.msg);
                }
            }
        });
    }

    jQuery("#review").keyup(function () {
        var review = jQuery("#review").val();
        if (review !== "") {
            jQuery("#review").css("background-color", "white");
            jQuery("#review_error").html('');
        } else {
            jQuery("#review_error").html('');
            jQuery("#review").css("background-color", "#FFF0F0");
            jQuery("#review").after('<em style="color:red;margin-left:6px;" id="review_error" for="review">Please enter your review</em>');
        }
    });
}

// function for save user review data end


// Normal Customer Login Script Start Here //
jQuery(document).ready(function () {
    jQuery("#click_login").click(function () {

        var login_email = jQuery("#login_email").val();
        var login_password = jQuery("#login_password").val();

        if (login_email === "") {
            jQuery("#login_email_invalid").html('<em style="color:red;margin-left:1px;" id="login_email_invalid" for="login_email">Please enter email address</em>');
            jQuery("#login_email").css("background-color", "#FFF0F0");
        }
        if (login_password === "") {
            jQuery("#login_password_invalid").html('<em style="color:red;margin-left:1px;" id="login_password_invalid" for="login_password">Please enter password</em>');
            jQuery("#login_password").css("background-color", "#FFF0F0");
        }

        if (login_email != "" && login_password != "") {
            $.ajax({
                type: "POST",
                url: "ajax/customer_login_check.php",
                dataType: "json",
                data: {
                    email: login_email,
                    password: login_password
                },
                success: function (response) {
                    var obj = response;
                    // dropDownBox(obj.output, obj.msg);
                    if (obj.output === "success") {
                        $('.k-window').fadeOut();
                        $('.k-overlay').fadeOut();
                        $('#register_form').replaceWith('<a href="javascript:void();" onclick="javascript:userLogout();">Logout</a>');
                        $('#login_form').replaceWith('<a href="customer_dashboard.php">' + obj.first_name + '</a>');
                        dropDownBox(obj.output, obj.msg);
//                        $("#login_form").text(obj.first_name);
                    } else {
                        dropDownBox(obj.output, obj.msg);
                    }

                }
            });
        }
    });

    jQuery("#login_email").keyup(function () {
        var login_email = jQuery("#login_email").val();
        if (login_email !== "") {
            jQuery("#login_email").css("background-color", "white");
            if (validateEmail(login_email)) {
                jQuery("#login_email_invalid").html('<em style="color:green;margin-left:1px;" id="login_email_invalid" for="login_email">Valid Email Address</em>');
//                jQuery("#login_email").after('');

            } else {
                jQuery("#login_email_invalid").html('<em style="color:red;margin-left:1px;" id="login_email_invalid" for="login_email">Invalid Email Address</em>');
//                jQuery("#login_email").after('');
            }

        } else {
            jQuery("#login_email_invalid").html('<em style="color:red;margin-left:12px;" id="login_email_invalid" for="login_email">Please enter email address</em>');
            jQuery("#login_email").css("background-color", "#FFF0F0");
//            jQuery("#login_email").after('');
        }
    });
    jQuery("#login_password").keyup(function () {
        var login_password = jQuery("#login_password").val();
        if (login_password !== "") {
            jQuery("#login_password").css("background-color", "white");
            jQuery("#login_password_invalid").html('');
        } else {
            jQuery("#login_password_invalid").html('<em style="color:red;margin-left:6px;" id="login_password_invalid" for="login_password">Please enter password</em>');
            jQuery("#login_password").css("background-color", "#FFF0F0");
            // jQuery("#login_password").after('<');
        }
    });
});




// Normal Customer Login Script End Here //


// Login Email Address Validation Function Start
function validateEmail(login_email) {
    var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    var valid = emailReg.test(login_email);

    if (!valid) {
        return false;
    } else {
        return true;
    }
}



//logout function for front end user


function userLogout() {
    //    alert("Here");
    jQuery.ajax({
        type: "POST",
        url: "ajax/ajaxLogout.php",
        dataType: "json",
        data: {
        },
        success: function (response) {
            var obj = response;
            //            console.log(obj);
            dropDownBox(obj.output, obj.msg);
            setTimeout(function () {
                window.location.reload();
            }, 1500);

        }
    });
}

//logout function for front end user






//ADD TO CART SCRIPT
function addToCart(type, eventID, scheduleID, additionID) {
    var quantity = 0;
    if (type == "type") {
        quantity = $("#txt_type_quantity_" + additionID).val();
    } else if (type == "include") {
        quantity = $("#txt_include_quantity_" + additionID).val();
    }

    $.ajax({
        type: "POST",
        url: "ajax/ajaxAddToCart.php",
        dataType: "json",
        data: {
            additionID: additionID,
            type: type,
            quantity: quantity,
            eventID: eventID,
            scheduleID: scheduleID
        },
        success: function (response) {
            var obj = response;
            // dropDownBox(obj.output, obj.msg);
            if (obj.output === "success") {
                var countCart = obj.CartCount;
                var cartObj = obj.WholeCart;
                generateMiniCart(cartObj);
                $(".cart-count").text(countCart);
                dropDownBox(obj.output, obj.msg);
            } else {
                dropDownBox(obj.output, obj.msg);
            }

        }
    });
}
//ADD TO CART SCRIPT



//Generate Mini Cart Script

function generateMiniCart(obj) {
    var cartHtml = '';
    var countItem = obj.length;

    if (countItem > 0) {
        var totalCartPrice = 0;
        for (var i = 0; i < obj.length; i++) {
            //converting event date format
            var mydate = new Date(obj[i].event_details.event_date);
            var eventDate = mydate.toString("dd MMM yyyy");

            cartHtml += '<li>';
            cartHtml += '<button type="button" class="close" onclick="javascript:deleteFromCart(' + obj[i].event_details.TC_id + ');">×</button>';
            cartHtml += '<div class="overflow-h">';
            cartHtml += '<span class="text-left" style="display: inline-block; width: 75% !important;">';
            cartHtml += '<p style="color: black !important;"><strong>' + obj[i].event_details.event_title + '</strong></p>';
            cartHtml += '<span style="font-size: x-small; color: #79b92d;">';
            cartHtml += 'Date: <strong>' + eventDate + '</strong><br> ';
            cartHtml += 'Venue: <strong>' + obj[i].event_details.venue_address + '</strong><br>';
            cartHtml += 'Time: <strong>' + obj[i].event_details.event_schedule_start_time + ' - ' + obj[i].event_details.event_schedule_end_time + '</strong><br>';
            cartHtml += '</span>';
            var itemWisePrice = 0;
            if (typeof obj[i].event_addition.type !== 'undefined' && obj[i].event_addition.type.length > 0) {
                cartHtml += '<table style="font-size: x-small; color: black;" width="100%">';
                cartHtml += '<tr style="border-bottom: 2px #dedede solid; font-weight: bold;">';
                cartHtml += '<td width="50%">Ticket Type</td>';
                cartHtml += '<td width="30%">Qty.</td>';
                cartHtml += '<td width="20%">Price</td>';
                cartHtml += '</tr>';
                for (var x = 0; x < obj[i].event_addition.type.length; x++) {
                    cartHtml += '<tr>';
                    cartHtml += '<td width="50%">' + obj[i].event_addition.type[x].ticket_name + '</td>';
                    cartHtml += '<td width="30%">' + obj[i].event_addition.type[x].TCA_item_quantity + '</td>';
                    cartHtml += '<td width="20%">TK. ' + obj[i].event_addition.type[x].TCA_item_total_price + '</td>';
                    cartHtml += '</tr>';
                    if (typeof obj[i].event_addition.type[x].TCA_item_total_price !== 'undefined') {
                        var itemPrice = obj[i].event_addition.type[x].TCA_item_total_price;
                        itemWisePrice += parseFloat(itemPrice);
                    }
                }
                cartHtml += '</table>';
            }

            cartHtml += '<hr>';
            if (typeof obj[i].event_addition.include !== 'undefined' && obj[i].event_addition.include.length > 0) {
                cartHtml += '<table style="font-size: x-small; color: black;" width="100%">';
                cartHtml += '<tr style="border-bottom: 2px #dedede solid; font-weight: bold;">';
                cartHtml += '<td width="50%">Includes</td>';
                cartHtml += '<td width="30%">Qty.</td>';
                cartHtml += '<td width="20%">Price</td>';
                cartHtml += '</tr>';
                for (var y = 0; y < obj[i].event_addition.include.length; y++) {
                    cartHtml += '<tr>';
                    cartHtml += '<td width="50%">' + obj[i].event_addition.include[y].event_includes_name + '</td>';
                    cartHtml += '<td width="30%">' + obj[i].event_addition.include[y].TCA_item_quantity + '</td>';
                    cartHtml += '<td width="20%">TK. ' + obj[i].event_addition.include[y].TCA_item_total_price + '</td>';
                    cartHtml += '</tr>';
                    if (typeof obj[i].event_addition.include[y].TCA_item_total_price !== 'undefined') {
                        var itemPrice = obj[i].event_addition.include[y].TCA_item_total_price;
                        itemWisePrice += parseFloat(itemPrice);
                    }
                }
                cartHtml += '</table>';
            }

            cartHtml += '</span>';
            cartHtml += '<span class="text-right" style="display: inline-block; float: right; width: 25% !important; color: black !important;">';
            cartHtml += '<strong>TK. ' + itemWisePrice.toFixed(2) + ' &nbsp;&nbsp;</strong>';
            cartHtml += '</span>';
            cartHtml += '</div>';
            cartHtml += '</li>';
            totalCartPrice += itemWisePrice;
        }
        cartHtml += '<li class="subtotal">';
        cartHtml += '<div class="overflow-h margin-bottom-10">';
        cartHtml += '<span>Subtotal</span>';
        cartHtml += '<span class="pull-right subtotal-cost">TK. ' + totalCartPrice.toFixed(2) + '</span>';
        cartHtml += '</div>';
        cartHtml += '<div class="row">';
        cartHtml += '<div class="col-xs-6">';
        cartHtml += '<a href="cart.php" class="btn-u btn-brd btn-brd-hover btn-u-sea-shop btn-block">View Cart</a>';
        cartHtml += '</div>';
        cartHtml += '<div class="col-xs-6">';
        cartHtml += '<a href="checkout.php" class="btn-u btn-u-sea-shop btn-block">Checkout</a>';
        cartHtml += '</div>';
        cartHtml += '</div>';
        cartHtml += '</li>';
    } else {
        cartHtml += '<li class="subtotal">';
        cartHtml += '<div class="overflow-h margin-bottom-10">';
        cartHtml += '<span>Subtotal</span>';
        cartHtml += '<span class="pull-right subtotal-cost">TK. 0.00</span>';
        cartHtml += '</div>';
        cartHtml += '<div class="row">';
        cartHtml += '<div class="col-xs-6">';
        cartHtml += '<a href="cart.php" class="btn-u btn-brd btn-brd-hover btn-u-sea-shop btn-block">View Cart</a>';
        cartHtml += '</div>';
        cartHtml += '<div class="col-xs-6">';
        cartHtml += '<a href="checkout.php" class="btn-u btn-u-sea-shop btn-block">Checkout</a>';
        cartHtml += '</div>';
        cartHtml += '</div>';
        cartHtml += '</li>';
    }

    $('#scrollbar').html(cartHtml);
}

//Generate Mini Cart Script




//Delete from cart

function deleteFromCart(id) {
    $.ajax({
        type: "POST",
        url: "ajax/ajaxDeleteFromCart.php",
        dataType: "json",
        data: {
            TempCartID: id
        },
        success: function (response) {
            var obj = response;
            // dropDownBox(obj.output, obj.msg);
            if (obj.output === "success") {
                var countCart = obj.CartCount;
                var cartObj = obj.WholeCart;
                generateMiniCart(cartObj);
                $(".cart-count").text(countCart);
                dropDownBox(obj.output, obj.msg);

                if (getCurrentPage() == "checkout.php") {
                    setTimeout(function () {
                        location.href = 'cart.php';
                    }, 1200);
                }
            } else {
                dropDownBox(obj.output, obj.msg);
            }

        }
    });
}
//Delete from cart






// Save Customer Account settings Data

jQuery(document).ready(function () {
    jQuery("#save_account_settings_data").click(function () {

        var customer_id = jQuery("#customer_id").val();
        var first_name = jQuery("#first_name_edit").val();
        var last_name = jQuery("#last_name_edit").val();
        var phone = jQuery("#phone").val();
        var gender = jQuery("#gender_edit").val();
        var dob = jQuery("#dob_edit").val();

        if (first_name === "") {
            jQuery("#error").html('<em style="color:red;margin-left:15px;" id="error" for="first_name">Enter First Name</em>');
            jQuery("#first_name").css("background-color", "#FFF0F0");
        }
        else if (last_name === "") {
            jQuery("#error").html('<em style="color:red;margin-left:15px;" id="error" for="last_name">Enter Last Name</em>');
            jQuery("#last_name").css("background-color", "#FFF0F0");
        } else if (phone === "") {
            jQuery("#error").html('<em style="color:red;margin-left:15px;" id="error" for="phone">Enter Phone Number</em>');
            jQuery("#phone").css("background-color", "#FFF0F0");
        } else {
            $.ajax({
                type: "POST",
                url: "ajax/save_account_settings_data.php",
                dataType: "json",
                data: {
                    customer_id: customer_id,
                    first_name: first_name,
                    last_name: last_name,
                    //phone: phone,
                    dob: dob,
                    gender: gender
                },
                success: function (response) {
                    var obj = response;
                    if (obj.output === "success") {
                        $('#myaccount').modal('hide');
                        dropDownBox(obj.output, obj.msg);
                    } else {
                        dropDownBox(obj.output, obj.msg);
                    }
                }
            });
        }

    });
});





// Save Customer Billing Address Data

jQuery(document).ready(function () {
    jQuery("#change_billing_address").click(function () {

        var billing_address_id = jQuery("#billing_address_id").val();
        var customer_id = jQuery("#customer_id").val();
        var billing_address = jQuery("#billing_address").val();
        var billing_post_code = jQuery("#billing_post_code").val();
        var billing_city = jQuery("#billing_city").val();
        var billing_country = jQuery("#billing_country").val();
        
       

        if (billing_address === "") {
            jQuery("#error_billing").html('<em style="color:red;margin-left:15px;" id="error_billing" for="billing_address">Enter Billing Address</em>');
            jQuery("#billing_address").css("background-color", "#FFF0F0");
        } else if (billing_post_code === "") {
            jQuery("#error_billing").html('<em style="color:red;margin-left:15px;" id="error_billing" for="billing_post_code">Enter Billing Post Code</em>');
            jQuery("#billing_post_code").css("background-color", "#FFF0F0");
        } else if (billing_city === "") {
            jQuery("#error_billing").html('<em style="color:red;margin-left:15px;" id="error_billing" for="billing_city">Enter Billing City</em>');
            jQuery("#billing_city").css("background-color", "#FFF0F0");
        } else if (billing_country === "") {
            jQuery("#error_billing").html('<em style="color:red;margin-left:15px;" id="error_billing" for="billing_country">Enter Billing Country</em>');
            jQuery("#billing_country").css("background-color", "#FFF0F0");
        } else {
            $.ajax({
                type: "POST",
                url: "ajax/save_billing_address.php",
                dataType: "json",
                data: {
                    billing_address_id: billing_address_id,
                    customer_id: customer_id,
                    billing_address: billing_address,
                    billing_post_code: billing_post_code,
                    billing_city: billing_city,
                    billing_country: billing_country
                },
                success: function (response) {
                    var obj = response;
                    if (obj.output === "success") {
                       $('#bill').modal('hide');
                        $('#bill_add').replaceWith('<a href="customer_dashboard.php">' + obj.first_name + '</a>');
                       
                       // $('.k-overlay').fadeOut();
                        dropDownBox(obj.output, obj.msg);
                    } else {
                        dropDownBox(obj.output, obj.msg);
                    }
                }
            });
        }


    });
});



// Save Customer Shipping Address Data

jQuery(document).ready(function () {
    jQuery("#change_shipping_address").click(function () {

        var shipping_address_id = jQuery("#shipping_address_id").val();
        var customer_id = jQuery("#customer_id").val();
        var shipping_address = jQuery("#shipping_address").val();
        var shipping_post_code = jQuery("#shipping_post_code").val();
        var shipping_city = jQuery("#shipping_city").val();
        var shipping_country = jQuery("#shipping_country").val();

        if (shipping_address === "") {
            jQuery("#error_shipping").html('<em style="color:red;margin-left:15px;" id="error_shipping" for="shipping_address">Enter Shipping Address</em>');
            jQuery("#shipping_address").css("background-color", "#FFF0F0");
        } else if (shipping_post_code === "") {
            jQuery("#error_shipping").html('<em style="color:red;margin-left:15px;" id="error_shipping" for="shipping_post_code">Enter Shipping Post Code</em>');
            jQuery("#shipping_post_code").css("background-color", "#FFF0F0");
        } else if (shipping_city === "") {
            jQuery("#error_shipping").html('<em style="color:red;margin-left:15px;" id="error_shipping" for="shipping_city">Enter Shipping City</em>');
            jQuery("#shipping_city").css("background-color", "#FFF0F0");
        } else if (shipping_country === "") {
            jQuery("#error_shipping").html('<em style="color:red;margin-left:15px;" id="error_shipping" for="shipping_country">Enter Shipping Coountry</em>');
            jQuery("#shipping_country").css("background-color", "#FFF0F0");
        } else {
            $.ajax({
                type: "POST",
                url: "ajax/save_shipping_address.php",
                dataType: "json",
                data: {
                    shipping_address_id: shipping_address_id,
                    customer_id: customer_id,
                    shipping_address: shipping_address,
                    shipping_post_code: shipping_post_code,
                    shipping_city: shipping_city,
                    shipping_country: shipping_country
                },
                success: function (response) {
                    var obj = response;
                    if (obj.output === "success") {
                        $('#myModal').modal('hide');
                        dropDownBox(obj.output, obj.msg);
                    } else {
                        dropDownBox(obj.output, obj.msg);
                    }
                }
            });
        }


    });
});



// Delete product from wishlist tab 

function removeWishlist(id) {
    var wishlist_id = id;
    console.log(wishlist_id);
    $.ajax({
        type: "POST",
        url: baseUrl + "/ajax/ajaxRemoveWishlist.php",
        dataType: "json",
        data: {
            WL_id: wishlist_id
        },
        success: function (response) {
            var obj = response;
            if (obj.output === "success") {
                dropDownBox(obj.output, obj.msg);
                $("#wishlist_event_" + id).fadeOut("slow");
            } else {
                dropDownBox(obj.output, obj.msg);
            }
        }
    });
}

// Function For click View in My Ticket Tab Start
function hideshow(which) {
    if (!document.getElementById)
        return;
    if (which.style.display === "block")
        which.style.display = "none";
    else
        which.style.display = "block";
}
// Function For click View in My Ticket Tab End



// Donation Amount Save Start //

jQuery(document).ready(function () {
    jQuery("#ssl_payment_donation").click(function () {
        var program_id = jQuery("#program_id").val();
        var batch_id = jQuery("#batch_id").val();
        var name = jQuery("#name").val();
        var amount = jQuery("#amount").val();
        console.log(program_id);

        if (program_id == 0) {
            jQuery("#program_id_error").html('');
            jQuery("#program_id").css("background-color", "#FFF0F0");
            jQuery("#program_id").after('<em style="color:red;margin-left:9px;" id="program_id_error" for="program_id">Please select one program </em>');

        }
        if (batch_id == 0) {
            jQuery("#batch_id_error").html('');
            jQuery("#batch_id").css("background-color", "#FFF0F0");
            jQuery("#batch_id").after('<em style="color:red;margin-left:9px;" id="batch_id_error" for="batch_id">Please select one batch </em>');

        }
        if (name == "") {
            jQuery("#name_error").html('');
            jQuery("#name").css("background-color", "#FFF0F0");
            jQuery("#name").after('<em style="color:red;margin-left:9px;" id="name_error" for="name">Please enter your name </em>');

        }
        if (amount == 0) {
            jQuery("#amount_error").html('');
            jQuery("#amount").css("background-color", "#FFF0F0");
            jQuery("#amount").after('<em style="color:red;margin-left:9px;" id="amount_error" for="amount">Please enter donation amount </em>');

        }
        if(amount < 500){
            jQuery("#amount_error").html('');
            jQuery("#amount").css("background-color", "#FFF0F0");
            jQuery("#amount").after('<em style="color:red;margin-left:9px;" id="amount_error" for="amount">Minimum amount 500 BDT. </em>');
        }
        if(program_id != "" && batch_id != "" && name != "" && amount > 499) {
            $.ajax({
                type: "POST",
                url: baseUrl + "/ajax/ajaxDonateOnlinePayment.php",
                dataType: "json",
                data: {
                    program_id: program_id,
                    batch_id: batch_id,
                    name: name,
                    amount: amount
                },
                success: function (response) {
                    var obj = response;
                    if (obj.output === "success") {
                        dropDownBox(obj.output, obj.msg);
                    } else {
                        dropDownBox(obj.output, obj.msg);
                    }
                }
            });
        }

        jQuery("#program_id").click(function () {
            var program_id = jQuery("#program_id").val();
            if (program_id !== "") {
                jQuery("#program_id").css("background-color", "white");
                jQuery("#program_id_error").html('');
            } else {
                jQuery("#program_id_error").html('');
                jQuery("#program_id").css("background-color", "#FFF0F0");
                jQuery("#program_id").after('<em style="color:red;margin-left:6px;" id="program_id_error" for="program_id">Please select one program</em>');
            }
        });

        jQuery("#batch_id").click(function () {
            var batch_id = jQuery("#batch_id").val();
            if (batch_id !== "") {
                jQuery("#batch_id").css("background-color", "white");
                jQuery("#batch_id_error").html('');
            } else {
                jQuery("#batch_id_error").html('');
                jQuery("#batch_id").css("background-color", "#FFF0F0");
                jQuery("#batch_id").after('<em style="color:red;margin-left:6px;" id="batch_id_error" for="batch_id">Please select one batch</em>');
            }
        });
        jQuery("#name").keyup(function () {
            var name = jQuery("#name").val();
            if (name !== "") {
                jQuery("#name").css("background-color", "white");
                jQuery("#name_error").html('');
            } else {
                jQuery("#name_error").html('');
                jQuery("#name").css("background-color", "#FFF0F0");
                jQuery("#name").after('<em style="color:red;margin-left:6px;" id="name_error" for="name">Please enter your name</em>');
            }
        });
        
        jQuery("#amount").keyup(function () {
            var amount = jQuery("#amount").val();
            if (amount !== "") {
                jQuery("#amount").css("background-color", "white");
                jQuery("#amount_error").html('');
            } else {
                jQuery("#amount_error").html('');
                jQuery("#amount").css("background-color", "#FFF0F0");
                jQuery("#amount").after('<em style="color:red;margin-left:6px;" id="amount_error" for="amount">Please enter donation amount</em>');
            }
        });

    });
});

// Donation Amount Save End //