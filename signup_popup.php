<div id="register_window" style="display: none;">
    <div class="col-md-10">
        <form style="width: 480px;"  id="sky-form4" class="log-reg-block sky-form">

            <div class="login-input reg-input" style="padding-top: 15px;">
                <div class="row">
                    <div class="col-sm-5">
                        <section>
                            <label class="input">
                                <input style="margin-left: 10px;" type="text" id="first_name" name="firstname" placeholder="First name" class="form-control">
                            </label>
                        </section>
                    </div>
                    <div class="col-sm-5">
                        <section>
                            <label class="input">
                                <input style="margin-left: 15px;" type="text" id="last_name" name="lastname" placeholder="Last name" class="form-control">
                            </label>
                        </section>        
                    </div>
                </div>
                <!--                <label class="select margin-bottom-15" style="width: 460px;margin-left: 10px">
                                    <select name="gender" id="gender" class="form-control">
                                        <option value="0" selected disabled>Gender</option>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                        <option value="other">Other</option>
                                    </select>
                                </label>-->
                <!-- <div class="row margin-bottom-10">
                <!--                    <div class="col-xs-5" style="margin-left: 10px;">
                                        <label class="select">
                                            <select name="month" id="month" class="form-control">
                                                <option disabled="" selected="" value="0">Month</option>
                <?php //$month_array = array("01" => "January", "02" => "February", "03" => "March", "04" => "April", "05" => "May", "06" => "June", "07" => "July", "08" => "August", "09" => "September", "10" => "October", "11" => "November", "12" => "December"); ?>
                <?php //foreach ($month_array as $key => $value): ?>
                                                    <option value="<?php //echo $key;  ?>"><?php // echo $value;  ?></option>
                <?php //endforeach; ?>
                
                                            </select>
                                        </label>    
                                    </div>-->
                <!--                    <div class="col-xs-2">
                                        <select style="margin-left: -15px; width: 80px; height: 20px;" name="day" id="day" class="form-control">
                                            <option disabled="" selected="" value="0">Day</option>
                <?php //$day_array = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"); ?>
                <?php //foreach ($day_array as $d): ?>
                                                <option value="<?php //echo $d;  ?>"><?php //echo $d;  ?></option>
                <?php // endforeach; ?>
                                        </select>
                                    </div>-->
                <!--                    <div class="col-xs-2">
                                        <select style="margin-left: -5px; width: 80px;height: 20px;" name="year" id="year" class="form-control">
                                            <option disabled="" selected="" value="0">Year</option>
                <?php
                //$today = date("Y-m-d");
                // $c_today = date_create($today);
                // $f_today = date_format("Y", $c_today);
                //for ($j = 2015; $j >= 1950; $j--) {
                //    echo '<option value="' . $j . '"> ' . $j . ' </option>';
                // }
                ?>
                                        </select>
                                    </div>-->
                <!--</div>-->

                <section style="margin-top: 10px;">
                    <label class="input" style="margin-left: 10px;width: 459px;">
                        <input type="email" id="email" name="email" placeholder="Email address" class="form-control">
                    </label>
                </section>                                
                <section style="margin-top: 10px;">
                    <label class="input" style="margin-left: 10px;width: 459px;">
                        <input type="password" name="password" placeholder="Password" id="password" class="form-control">
                    </label>
                </section>                                
<!--                <section>
                    <label class="input" style="margin-left: 10px;width: 459px;">
                        <input type="password" id="retype_password" name="passwordConfirm" placeholder="Confirm password" class="form-control">
                    </label>
                </section>                                -->
            </div>

            <label class="checkbox margin-bottom-20" style="margin-left: 10px;">
                <input id="terms_and_conditions" type="checkbox" name="checkbox"/>
                <i></i>
                I have read agreed with the <a href="#">terms &amp; conditions</a>
            </label>
            <button onclick="return false;" style="width: 434px;margin-top: 10px; margin-left: 10px;"   id="create_account" class="btn-u btn-u-sea-shop btn-block margin-bottom-20">Create Account</button>
            <div class="row columns-space-removes" style="width: 496px; padding-top: 12px;margin-left: -5px;">
                <div class="col-lg-5 margin-bottom-20">
                    <button onclick="fb_login();" type="button" class="btn-u btn-u-md btn-u-fb btn-block"><i class="fa fa-facebook"></i>Facebook Login</button>
                </div>
               
                <div class="col-lg-5">
                    <button onclick="googleLogin();" type="button" style="background-color: #dd4b39;margin-left: -10px;" class="btn-u btn-u-md btn-u-tw btn-block"><i class="fa fa-google-plus"></i>Google Login</button>
                </div>
            </div>

        </form>

        <div class="margin-bottom-20"></div>
        <p class="text-center">Already you have an account? <a href="javascript:void(0)" id="login_form_from_register">Sign In</a></p>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery("#login_form_from_register").click(function () {
                    jQuery(".k-i-close").click();
                    jQuery("#login_window").kendoWindow({
                        width: "530px",
                        height: "auto",
                        visible: false,
                        modal: true,
                        title: "Login To Your Account"
                    }).data("kendoWindow").center().open();
                });
            });
        </script>
    </div>
</div>

