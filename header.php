<?php
$sessionID = session_id();
$arrayWholeCartHeader = array();
$sqlWholeCart = "SELECT * "
 . "FROM temp_carts_events "
 . "LEFT JOIN event ON event.event_id=temp_carts_events.TC_product_id "
 . "LEFT JOIN event_schedule ON event_schedule.event_schedule_id=temp_carts_events.TC_schedule_id "
 . "LEFT JOIN event_venue ON event_venue.event_id=temp_carts_events.TC_product_id "
 . "LEFT JOIN venue ON venue.venue_id=event_venue.venue_id "
 . "WHERE temp_carts_events.TC_session_id='$sessionID' "
 . "AND event_venue.is_active='true' "
 . "ORDER BY `temp_carts_events`.`TC_updated` DESC";
$resultWholeCart = mysqli_query($con->open(), $sqlWholeCart);
if ($resultWholeCart) {
    while ($resultWholeCartObj = mysqli_fetch_array($resultWholeCart)) {
        $arrayWholeCartHeader[]['event_details'] = $resultWholeCartObj;   
    }
} else {
    echo "resultWholeCart query failed.";
}
?>
<!-- Navbar -->
<div class="navbar navbar-default mega-menu" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">
                <img  style="height: 60px;width: 150px; margin-top: -20px;" id="logo-header" src="assets_new/img/ticketchai_logo.png" alt="Logo">
            </a>
        </div>
        <div class="navbar-header col-sm-12 col-xs-12 hidden-lg hidden-md">
            <ul class="list-inline hidden-lg col-xs-12 col-sm-12">
                <li class="pull-left" aria-disabled="false">
                    <a href="checkout.php" style="padding: 6px; font-size: medium;" class="btn-u btn-u-sea-shop btn-u-lg">Checkout&nbsp;&nbsp;<i style="color: white;" class="fa  fa-check-circle-o"></i></a>
                </li>
<!--                <li class="pull-left" aria-disabled="false">
                    <a href="donate.php" style="padding: 6px; font-size: medium;" class="btn-u btn-u-sea-shop btn-u-lg">Donate&nbsp;&nbsp;<i style="color: white;" class="fa fa-money"></i></a>
                </li>-->
                <li class="pull-right" aria-disabled="false">
                    <a href="cart.php" style="font-size: 24px">
                        <i class="fa fa-shopping-cart"></i>
                        <span class="badge badge-sea rounded-x cart-count"><?php echo count($arrayWholeCartHeader); ?></span>
                    </a>
                </li>
            </ul>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-responsive-collapse">
            
            <?php include './mini_cart.php'; ?>

            <ul class="nav navbar-nav" style="float: left !important; margin-left: 30px;">
                <!-- Home -->
                <li class="dropdown active">
                    <a href="index.php">
                        HOME
                    </a>
                </li>
                <!-- End Home -->

                <!-- Event -->
                <li class="dropdown">
                    <a href="index.php" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">
                        EVENTS
                    </a>
                </li>
                <!-- End Events -->

                <!-- MOVIES -->
                <li class="dropdown mega-menu-fullwidth">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">
                        MOVIES
                    </a>
                </li>
                <!-- End Movies -->

                <!-- ABOUT US -->
                <li class="dropdown">
                    <a href="about_us.php">
                        ABOUT US
                    </a>

                </li>
                <!-- End ABOUT US -->

                <!-- HOW TO BUY -->
                <li class="dropdown">
                    <a href="contact_us.php">
                        CONTACT US
                    </a>

                </li>
                <!-- End HOW TO BUY-->

            </ul>
        </div><!--/navbar-collapse-->
    </div>    
</div>            
<!-- End Navbar -->