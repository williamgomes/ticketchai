<?php
error_reporting(0);
include("admin/config/class.web.config.php");
$con = new Config();

if (isset($_GET["booking_id"])) {
    $booking_id = $_GET["booking_id"];
}

$query_for_booking_history = "SELECT b.schedule_id, b.online_pay_method, ev.event_id,ev.event_title,b.booking_id,b.customer_id,b.ticket_price,b.ticket_quantity,
    b.total_amount,b.delivery_cost,b.delivery_method,b.delivery_status,b.booking_time, b.payment_status,b.life_time_subsciption_cost,
    (SELECT event_date from event_schedule WHERE event_schedule.event_schedule_id = b.schedule_id) as schedule_date,
    (SELECT event_schedule_start_time from event_schedule WHERE event_schedule.event_schedule_id= b.schedule_id) as schedule_start_time,
    (SELECT event_schedule_end_time from event_schedule WHERE event_schedule.event_schedule_id= b.schedule_id) as schedule_end_time,
    b.delivery_status,b.tocken
    FROM booking as b, event as ev
    WHERE b.event_id = ev.event_id AND b.booking_id = '$booking_id'";
$booking_historys = $con->ReturnObjectByQuery($query_for_booking_history, "array");
?>



<!DOCTYPE html>

<html>

    <head>

        <script type="text/javascript" src="admin/assets/js/jquery-1.9.1.min.js"></script>

        <script type="text/javascript" src="jquery-barcode.js"></script>

        <link href="style.css" rel="stylesheet">

    </head>
    <body>
        <div class="top_text">
            Please print and bring this ticket with you.
        </div>
        <br />
        <?php foreach ($booking_historys as $booking): ?>

            <?php
            $customer_id = $booking->customer_id;
        
            $query_for_view = "SELECT booking.*, customer.*, `event`.*
                                                        FROM booking 
                                                        INNER JOIN customer on customer.customer_id = booking.customer_id
                                                        INNER JOIN `event` ON `event`.event_id = booking.event_id
                                                        WHERE booking.booking_id = '$booking->booking_id'";
            $bookings = $con->ReturnObjectByQuery($query_for_view, "array");
            $event_schedules = $con->SelectAll("event_schedule", "event_id='$booking->event_id'");
            $customer_addition_query_string = "SELECT  customer_addition.*, batch.*, program.* FROM customer_addition
                                                                        INNER JOIN batch on batch.batch_id = customer_addition.CA_batch_id
                                                                        INNER JOIN program on program.program_id = customer_addition.CA_program_id
                                                                        WHERE customer_addition.CA_customer_id = '$customer_id' LIMIT 0,1";
            $customer_addition_query = $con->ReturnObjectByQuery($customer_addition_query_string);
            $event_terms_and_conditions = $con->SelectAll("event_terms_and_conditions", "event_id='$booking->event_id' LIMIT 0,1");
            $event_category = $con->SelectAll("event_category", "event_id='$booking->event_id' LIMIT 0,1");
            ?>


            <table class="main" id="tiecket_recent_view_<?php echo $booking->booking_id; ?>" style="display:none; width: 100%; border-style: none;">
                <tr>
                    <td width="139" style="height:50px;" bgcolor="#FFFFFF" rowspan="2">
                        <img src="images/DUDSAA.jpg" class="event_logo" alt=""/>
                    </td>
                    <td colspan="6" bgcolor="#FFFFFF"><h3><strong><?php echo $bookings{0}->event_title; ?></strong></h1></td>
                    <td style="width: 20%;" rowspan="2" align="center" valign="middle" bgcolor="#FFFFFF">
                        <img src="images/DU.jpg" class="event_logo" alt=""/>
                    </td>
                </tr>
                <tr>

                    <td colspan="6" bgcolor="#FFFFFF">
                        <h3>
                            Shawkat Osman Auditorium, Central Public Library, Dhaka
                        </h3>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#FFFFFF"><h6>DATE</h6></td>
                    <td colspan="5" bgcolor="#FFFFFF"><?php echo $event_schedules{0}->event_date; ?></td>
                    <td  bgcolor="#FFFFFF"><h6>TIME</h6></td>
                    <td colspan="3" bgcolor="#FFFFFF">
                        <h6>
                            Time Start: <?php echo $event_schedules{0}->event_schedule_start_time; ?> &nbsp;&nbsp;
                            End Time: <?php echo $event_schedules{0}->event_schedule_end_time; ?>
                        </h6>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#FFFFFF"><h6>Delivery Method:</h6></td>
                    <td colspan="4" bgcolor="#FFFFFF">
                        <h4>
                            <?php
                            if ($bookings{0}->online_pay_method == "home") {
                                echo "Home Delivery";
                            } else if ($bookings{0}->online_pay_method == "collect") {
                                echo "Pick from Department of Development Studies, University of Dhaka.";
                            } else if ($bookings{0}->online_pay_method == "pick") {
                                echo "Pick From Ticketchai Office.";
                            }
                            ?> 
                        </h4>

                    </td>
                    <td bgcolor="#FFFFFF"><h6>CATAGORY</h6></td>
                    <td colspan="4" bgcolor="#FFFFFF">
                        <h6>
                            <?php
                            $category_id = $event_category{0}->category_id;
                            $categories = $con->SelectAll("category", "category_id='$category_id'");
                            echo $categories{0}->category_name;
                            ?>
                        </h6>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" valign="top" bgcolor="#FFFFFF" style="font-size: 11px;">
                        <p style="text-align: center; font-size: 14px; font-weight: bold; margin-top: 4px;">Owner's Info</p>
                        <p> Name: <?php echo $bookings{0}->first_name; ?>&nbsp;<?php echo $bookings{0}->last_name; ?></p>
                        <p> Address: <?php
                            echo $bookings{0}->delivery_address;
                            ?></p>
                        <p> Email: <?php echo $bookings{0}->email; ?></p>

                        <p> Phone Number:<?php echo $bookings{0}->delivery_phone_number; ?></p>
                        <p> Batch: <?php echo $customer_addition_query{0}->batch_name; ?></p>

                        <p> Profession: <?php echo $customer_addition_query{0}->CA_profession; ?></p>
                        <p> Designation: <?php echo $customer_addition_query{0}->CA_designation; ?></p>
                        <p> Organization: <?php echo $customer_addition_query{0}->CA_organization; ?></p>
                        <p> Blood Group: <?php echo $customer_addition_query{0}->CA_blood_grp; ?></p>
                        <p> Program Studied: <?php echo $customer_addition_query{0}->program_name; ?></p>
                    </td>
                    <td colspan="5">
                        <table style="width: 100%; margin-left: -8px; font-size: 11px;">
                            <tr>
                                <td colspan="2" style="text-align: center; font-size: 14px; font-weight: bold; border-style: none;">Payment Details (BDT)</td>
                            </tr>
                             
                            <?php if ($bookings{0}->ticket_price > 0):?>
                            <tr>
                                <!--Check this, self value is not coming-->
                                <td style="border-style: none;">Ticket Owner:</td>
                                <td style="border-style: none; text-align: right">
                                    <?php echo $bookings{0}->ticket_price; ?>                                                                   
                                </td>
                            </tr>
                            <?php endif; ?>
                            <tr>
                                <!--Check this, self value is not coming-->
                                <td style="border-style: none;">
                                    <?php
                                    if ($bookings{0}->enrollment_fee > 0 OR $bookings{0}->anual_subsciption_cost > 0 OR $bookings{0}->life_time_subsciption_cost > 0) {
                                        echo "(";
                                    }
                                    if ($bookings{0}->enrollment_fee > 0) {
                                        echo "Enroll Fee";
                                        echo ", ";
                                        $total_fee = $bookings{0}->enrollment_fee;
                                    }
                                    if ($bookings{0}->anual_subsciption_cost > 0) {
                                        
                                        echo "Annual Subscription";
                                         echo ", ";
                                        $total_fee += $bookings{0}->anual_subsciption_cost;
                                    }
                                    if ($bookings{0}->life_time_subsciption_cost > 0) {
                                       
                                        echo "Lifetime Subscription";
                                        $total_fee += $bookings{0}->life_time_subsciption_cost;
                                    }
                                    if ($bookings{0}->enrollment_fee > 0 OR $bookings{0}->anual_subsciption_cost > 0 OR $bookings{0}->life_time_subsciption_cost > 0) {
                                        echo ")";
                                    }
                                    ?>
                                </td>
                                <td style="border-style: none; text-align: right">
                                    <?php echo $total_fee; ?>                                                                   
                                </td>
                            </tr>


                            <tr>
                                <td colspan="2" style="border-style: none;"><hr style="border-style: dotted;" /></td>
                            </tr>
                            <?php if ($bookings{0}->delivery_method == "home_delivery"): ?>
                                <tr>
                                    <td style="border-style: none; font-weight: bold;">Home Delivery:</td>
                                    <td style="border-style: none; font-weight: bold; font-size: 16px; text-align: right">
                                        <?php echo $bookings{0}->delivery_cost; ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                            <tr>
                                <td style="border-style: none; font-weight: bold;">Grand Total:</td>
                                <td style="border-style: none; font-weight: bold; font-size: 16px; text-align: right">
                                    <?php echo $bookings{0}->total_amount; ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>        
                <tr>
                    <td height="74" bgcolor="#FFFFFF">EVENT DETAILS</td>
                    <td colspan="9" align="left" bgcolor="#FFFFFF" style="text-align: justify; font-size: 8px;">

                        <?php
                        $event_id = $bookings{0}->event_id;
                        $event_description = $con->SelectAll("event_details", "event_id='$event_id'");
                        echo html_entity_decode(html_entity_decode($event_description{0}->event_details_description));
                        ?>

                    </td>
                </tr>
                <tr>
                    <td height="43" colspan="10" align="center" bgcolor="#FFFFFF" style="padding-bottom: 0px; padding-top: 0px;">
                        E-Ticket NO:
                        <?php if ($bookings{0}->tocken != ''): ?>

                            <div class="barcodecell"><barcode code="<?php echo $bookings{0}->tocken; ?>" type="C39" height="1.5" text="1"/></div>

                            <div style="text-align:center; font-size: 10px;">

                                <?php echo $bookings{0}->tocken; ?>

                            </div> 

                        <?php endif; ?>


                    </td>
                </tr>
                <tr>
                    <td colspan="10" align="center" bgcolor="#FFFFFF" style="border-bottom-color: white;">TERMS &amp; CONDITIONS</td>
                </tr>
                <tr>
                    <td colspan="10" bgcolor="#FFFFFF" style=" text-align: justify; font-size: 8px;">   
                        <?php echo html_entity_decode(html_entity_decode($event_terms_and_conditions{0}->event_terms_and_conditions_description)); ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="10" align="center" bgcolor="#FFFFFF" style="border-bottom-style: none;">TICKETING PARTNER</td>
                </tr>
                <tr>
                    <td style="border-top-style: none;" colspan="10" bgcolor="#FFFFFF">
                        <table style="width: 100%; margin-left: -8px;">
                            <tr>
                                <td style="border-style: none; width: 46%">
                                    <h5>Hot Line Number:  +8801971842538,+8804478009569 </h5>
                                    <h5> <font style="color:white;">Hot Line Number:</font> +8801716020445,+8801624690104 </h5>
                                </td>
                                <td style="border-style: none;"><img src="assets_new/ticketchai_logo.png" width="100" height="50" alt=""/></td>
                                <td style="border-style: none; text-align: right"></td>
                            </tr>              
                        </table>
                    </td>
                </tr>
            </table> 
        <?php endforeach; ?>
</html>
