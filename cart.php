<?php
include("./admin/config/class.web.config.php");
$con = new Config();
?>
<?php
$sessionID = session_id();
$arrayWholeCart = array();
$sqlWholeCart = "SELECT * "
 . "FROM temp_carts_events "
 . "LEFT JOIN event ON event.event_id=temp_carts_events.TC_product_id "
 . "LEFT JOIN event_schedule ON event_schedule.event_schedule_id=temp_carts_events.TC_schedule_id "
 . "LEFT JOIN event_venue ON event_venue.event_id=temp_carts_events.TC_product_id "
 . "LEFT JOIN venue ON venue.venue_id=event_venue.venue_id "
 . "WHERE temp_carts_events.TC_session_id='$sessionID' "
 . "AND event_venue.is_active='true' "
 . "ORDER BY `temp_carts_events`.`TC_updated` DESC";
$resultWholeCart = mysqli_query($con->open(), $sqlWholeCart);
if ($resultWholeCart) {
    while ($resultWholeCartObj = mysqli_fetch_array($resultWholeCart)) {
        
        $TempCartID = $resultWholeCartObj['TC_id'];
        $TmpCartEventID = $resultWholeCartObj['TC_product_id'];
        $TmpCartType = $resultWholeCartObj['TC_product_type'];
        $TmpCartScheduleID = $resultWholeCartObj['TC_schedule_id'];
        if($TmpCartType == "event"){
            $arrayWholeCart[]['event']['event_details'] = $resultWholeCartObj;
        } elseif ($TmpCartType == "subscribe") {
            $arrayWholeCart[]['subscribe']['subscribe_details'] = $resultWholeCartObj;
        }
        


        $sqlSelectCustAdd = "SELECT * FROM customer_addition WHERE CA_session_id='$sessionID'";
        $resultSelectCustAdd = mysqli_query($con->open(), $sqlSelectCustAdd);
        if($resultSelectCustAdd){
            $resultSelectCustAddObj = mysqli_fetch_array($resultSelectCustAdd);
            $programID = $resultSelectCustAddObj['CA_program_id'];
            $batchID = $resultSelectCustAddObj['CA_batch_id'];
        }

        //getting ticket type information
        $sqlGetTicktType = "SELECT * "
                . "FROM temp_cart_addition "
                . "LEFT JOIN batch ON batch.batch_id=$batchID "
                . "WHERE temp_cart_addition.TCA_session_id='$sessionID' "
                . "AND temp_cart_addition.TCA_item_id=$batchID "
                . "AND temp_cart_addition.TCA_TC_id=$TempCartID ";
        if($TmpCartType == "event"){
            $sqlGetTicktType .= "AND temp_cart_addition.TCA_item_type='type' ";
        } else {
            $sqlGetTicktType .= "AND temp_cart_addition.TCA_item_type='subscribe' ";
        }
        
        $sqlGetTicktType .=  "ORDER BY `temp_cart_addition`.`TCA_item_updated` DESC";
        
        $resultGetTicktType = mysqli_query($con->open(), $sqlGetTicktType);

        if ($resultGetTicktType) {
            while ($resultGetTicktTypeObj = mysqli_fetch_array($resultGetTicktType)) {
                if($TmpCartType == "event"){
                    $arrayWholeCart[(count($arrayWholeCart) - 1)]['event']['event_addition'][$resultGetTicktTypeObj['TCA_item_type']][] = $resultGetTicktTypeObj;
                } elseif ($TmpCartType == "subscribe") {
                    $arrayWholeCart[(count($arrayWholeCart) - 1)]['subscribe']['subscribe_addition'][$resultGetTicktTypeObj['TCA_item_type']][] = $resultGetTicktTypeObj;
                }
            }
        } else {
            echo "resultGetTicktType query failed." . mysqli_error($con->open());
        }  
        
    }
} else {
    echo "resultWholeCart query failed.";
}

//echo "<pre>";
//print_r($arrayWholeCart);
//echo "</pre>";


?>
<?php include './header_script.php'; ?>
<body class="header-fixed">

    <div class="wrapper">
        <div class="header-v5 header-static">
            <?php include './menu_top.php'; ?>
            <?php include './header.php'; ?>
        </div>
        

        <!--=== Content Medium Part ===-->
        <div class="content-md margin-bottom-30">
            <div class="container">
                <div class="heading heading-v1 margin-bottom-20" style="margin-bottom: 100px;">
                    <h2><strong>Shopping Cart</strong></h2>
                </div>
                <?php if(count($arrayWholeCart) == 0): ?>
                    <div class="alert alert-danger fade in">
                        <strong>Oh noo!</strong> It seems your cart is empty. Buy some tickets and have fun.
                    </div>
                <?php endif; ?>
                <form class="shopping-cart" action="#">
                    <div>
                        <section>
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Product</th>
                                            <th>Details</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $subTotalPrice = 0; ?> 
                                        <?php $totalPrice = 0; ?>    
                                        <?php if(count($arrayWholeCart) > 0): ?>
                                            <?php foreach($arrayWholeCart AS $CartInfo): ?>
                                        
                                        <?php if(isset($CartInfo['subscribe'])): ?>
                                        <?php $subscribtionAmount = 10000;
                                        $subTotalPrice += $subscribtionAmount; ?>
                                        <tr>
                                            <td class="product-in-table" style="margin-top: 10px !important;width: 500px;">
                                                <img class="img-responsive" src="admin/uploads/event_logo_image/<?php echo $CartInfo['subscribe']['subscribe_details']['event_full_size_image']; ?>" alt="<?php echo $CartInfo['subscribe']['subscribe_details']['event_title']; ?>">
                                                <div class="product-it-in">
                                                    <h3><strong>Lifetime Membership</strong> for DUDSAA</h3>
                                                    
                                                </div>    
                                            </td>
                                            <td>
                                                
                                                <?php if(isset($CartInfo['subscribe']['subscribe_addition']['subscribe']) AND count($CartInfo['subscribe']['subscribe_addition']['subscribe'] > 0)): ?>
                                                    <table style="font-size: x-small; color: black;"  width="100%">
                                                        <tr style="border-bottom: 2px #dedede solid; font-weight: bold;">
                                                            <td width="50%">Type</td>
                                                            <!--<td width="30%">Qty.</td>-->
                                                            <td width="20%">Price</td>
                                                        </tr>
                                                    <?php foreach ($CartInfo['subscribe']['subscribe_addition']['subscribe'] AS $CartAdditionTypeInfo): ?>
                                                        <tr>
                                                            <td width="50%">Lifetime Subscription</td>
                                                            <!--<td width="30%">1</td>-->
                                                            <td width="20%">TK. <?php echo number_format($CartAdditionTypeInfo['TCA_item_total_price'],2); ?></td>
                                                        </tr>
                                                    <?php endforeach; ?>    
                                                    </table>
                                                <?php endif; ?>      
                                            </td>
<!--                                            <td>
                                                <button type='button' class="quantity-button" name='subtract' onclick='javascript: subtractQty1();' value='-'>-</button>
                                                <input type='text' class="quantity-field" name='qty1' value="5" id='qty1'/>
                                                <button type='button' class="quantity-button" name='add' onclick='javascript: document.getElementById("qty1").value++;' value='+'>+</button>
                                            </td>-->
                                            <td class="shop-red">TK. <?php echo number_format($subscribtionAmount,2); ?></td>
                                            <td>
                                                <!--<button type="button" class="close"><span style="font-size: 25px; color: rgb(208, 0, 0);">&times;</span><span class="sr-only">Close</span></button>-->
                                            </td>
                                        </tr>
                                        
                                        
                                        <?php else: ?>
                                        
                                        
                                        <tr>
                                            <td class="product-in-table" style="margin-top: 10px !important;width: 500px;">
                                                <img class="img-responsive" src="admin/uploads/event_logo_image/<?php echo $CartInfo['event']['event_details']['event_full_size_image']; ?>" alt="<?php echo $CartInfo['event']['event_details']['event_title']; ?>">
                                                <div class="product-it-in">
                                                    <h3><?php echo $CartInfo['event']['event_details']['event_title']; ?></h3>
                                                    <span style="font-size: x-small; color: #79b92d;">
                                                        Date: <strong><?php echo date("d M, Y" , strtotime($CartInfo['event']['event_details']['event_date'])); ?></strong><br/> 
                                                        Venue: <strong><?php echo $CartInfo['event']['event_details']['venue_address']; ?></strong><br/>
                                                        Time: <strong><?php echo $CartInfo['event']['event_details']['event_schedule_start_time']; ?> - <?php echo $CartInfo['event']['event_details']['event_schedule_end_time']; ?></strong>
                                                    </span>
                                                </div>    
                                            </td>
                                            <td>
                                                
                                                <?php if(isset($CartInfo['event']['event_addition']['type']) AND count($CartInfo['event']['event_addition']['type'] > 0)): ?>
                                                    <table style="font-size: x-small; color: black;"  width="100%">
                                                        <tr style="border-bottom: 2px #dedede solid; font-weight: bold;">
                                                            <td width="50%">Ticket Type</td>
                                                            <td width="30%">Qty.</td>
                                                            <td width="20%">Price</td>
                                                        </tr>
                                                    <?php foreach ($CartInfo['event']['event_addition']['type'] AS $CartAdditionTypeInfo): ?>
                                                        <?php $totalPrice += $CartAdditionTypeInfo['ticket_price'] + $CartAdditionTypeInfo['anual_subscription'] + $CartAdditionTypeInfo['enrollment_fee']; ?>
                                                        <tr>
                                                            <td width="50%">Ticket Price</td>
                                                            <td width="30%">1</td>
                                                            <td width="20%">TK. <?php echo $CartAdditionTypeInfo['ticket_price']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="50%">Annual Subscription</td>
                                                            <td width="30%">1</td>
                                                            <td width="20%">TK. <?php echo $CartAdditionTypeInfo['anual_subscription']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="50%">Enrollment Fee</td>
                                                            <td width="30%">1</td>
                                                            <td width="20%">TK. <?php echo $CartAdditionTypeInfo['enrollment_fee']; ?></td>
                                                        </tr>
                                                    <?php endforeach; ?>    
                                                    </table>
                                                <?php endif; ?>    

                                                    <hr>

                                                <?php // if(isset($CartInfo['event_addition']['include']) AND count($CartInfo['event_addition']['include'] > 0)): ?>    
<!--                                                    <table style="font-size: x-small; color: black;"  width="100%">
                                                        <tr style="border-bottom: 2px #dedede solid; font-weight: bold;">
                                                            <td width="50%">Includes</td>
                                                            <td width="30%">Qty.</td>
                                                            <td width="20%">Price</td>
                                                        </tr>-->
                                                    <?php // foreach ($CartInfo['event_addition']['include'] AS $CartAdditionIncldInfo): ?>
                                                        <?php // $totalPrice += $CartAdditionIncldInfo['TCA_item_total_price']; ?>
<!--                                                        <tr>
                                                            <td width="50%"><?php echo $CartAdditionIncldInfo['event_includes_name']; ?></td>
                                                            <td width="30%"><?php echo $CartAdditionIncldInfo['TCA_item_quantity']; ?></td>
                                                            <td width="20%">TK. <?php echo $CartAdditionIncldInfo['TCA_item_total_price']; ?></td>
                                                        </tr>-->
                                                    <?php // endforeach; ?>    
                                                    <!--</table>-->
                                                <?php // endif; ?>    
                                            </td>
<!--                                            <td>
                                                <button type='button' class="quantity-button" name='subtract' onclick='javascript: subtractQty1();' value='-'>-</button>
                                                <input type='text' class="quantity-field" name='qty1' value="5" id='qty1'/>
                                                <button type='button' class="quantity-button" name='add' onclick='javascript: document.getElementById("qty1").value++;' value='+'>+</button>
                                            </td>-->
                                            <td class="shop-red">TK. <?php echo number_format($totalPrice,2); ?></td>
                                            <td>
                                                <!--<button type="button" class="close"><span style="font-size: 25px; color: rgb(208, 0, 0);">&times;</span><span class="sr-only">Close</span></button>-->
                                            </td>
                                        </tr>
                                        
                                        
                                        <?php endif; ?>
                                            <?php $subTotalPrice += $totalPrice; ?>
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                        <tr>
                                            <td colspan="5" class="text-center">
                                                <h3>No item added to cart yet.</h3>
                                            </td>
                                        </tr>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                            
                            
                            <div class="coupon-code">
                            <div class="row">
                                <div class="col-sm-4 sm-margin-bottom-30">
                                    
                                </div>
                                <div class="col-sm-3 col-sm-offset-5">
                                    <ul class="list-inline total-result">
                                        <li>
                                            <h4>Subtotal:</h4>
                                            <div class="total-result-in">
                                                <span>TK. <?php echo number_format($subTotalPrice,2); ?></span>
                                            </div>    
                                        </li>    
<!--                                        <li>
                                            <h4>Delivery:</h4>
                                            <div class="total-result-in">
                                                <span class="text-right">TK. 0.00</span>
                                            </div>
                                        </li>-->
                                        <li class="divider"></li>
                                        <li class="total-price">
                                            <h4>Total:</h4>
                                            <div class="total-result-in">
                                                <span>TK. <?php echo number_format($subTotalPrice,2); ?></span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                                <div class="row" style="margin-top: 30px;">
                                <ul role="menu" aria-label="Pagination" class="text-right">
                                    <a href="checkout.php" style="padding: 10px; font-size: medium;" class="btn-u btn-u-sea-shop btn-u-lg">Checkout&nbsp;&nbsp;<i style="color: white;" class="glyphicon glyphicon-arrow-right"></i></a>
                                </ul>
                            </div>
                                
                        </div> 
                            
                        </section>
                    </div>
                </form>
            </div><!--/end container-->
        </div>
        <!--=== End Content Medium Part ===-->     

        <?php include './newsletter.php'; ?>

        <?php include './footer.php'; ?>
    </div><!--/wrapper-->

    <script>
    function checkDelivery(){
        if($('#showDelivery').is(":checked")){
            $('#divDelivery').fadeIn('slow');
        } else {
            $('#divDelivery').fadeOut('slow');
        }
    }
    

    </script>

</body>
</html> 