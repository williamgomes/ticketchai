<?php
include './admin/config/class.web.config.php';

$con = new Config();

$page_id = 3;

$err = '';

$msg = '';

$contact_us_name = '';

$contact_us_email = '';

$contact_us_desc = '';

//$insert_array='';

if (isset($_POST['btnSendMessage'])) {

    extract($_POST);

    //$con->debug($_POST);
//    if($_POST["contact_us_desc"]==""){
//        $_POST['contact_us_desc']="NULL";
//    }

    if ($_POST["contact_us_name"] == "") {

        $err = "Name Required";
    } elseif ($_POST["contact_us_email"] == "") {

        $err = "Email Address Required";
    } else {

        $insert_array = array(
            "contact_us_name" => $contact_us_name,
            "contact_us_email" => $contact_us_email,
            "contact_us_desc" => $contact_us_desc
        );

        $result = $con->Insert("contact_us", $insert_array, "", "", "array");

        // $result=$con->Insert("contact_us", $insert_array);

        if ($result["output"] == "success") {

            $msg = "Message Send Successfully.";
        }
    }
}
?>

<?php include './header_script.php'; ?>

<body class="header-fixed"> 



    <script>

        function initialize() {

            var myLatlng = new google.maps.LatLng(23.748556, 90.403792);

            var mapOptions = {
                zoom: 14,
                center: myLatlng

            }

            var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);



            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: 'Hello World!'

            });

        }



        google.maps.event.addDomListener(window, 'load', initialize);



    </script>



    <div class="wrapper">
        <div class="header-v5 header-static">
<?php include './menu_top.php'; ?>
<?php include './header.php'; ?>
        </div>




        <!--=== Breadcrumbs ===-->

        <div class="breadcrumbs">

            <div class="container">

                <h1 class="pull-left">Our Contacts</h1>

                <!--                    <ul class="pull-right breadcrumb">

                                        <li><a href="index.html">Home</a></li>

                                        <li><a href="">Pages</a></li>

                                        <li class="active">Contacts</li>

                                    </ul>-->

            </div>

        </div><!--/breadcrumbs-->

        <!--=== End Breadcrumbs ===-->



        <!--=== Content Part ===-->

        <div class="container content">    

            <div class="row">

                <div class="col-md-12" style="text-align:center;">

<?php if (!empty($err)): ?>

                        <div class="alert alert-danger fade in alert-dismissable">

                            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                            <strong>Sorry!</strong>

    <?php echo $err; ?>

                        </div>

<?php endif; ?>

                </div>

            </div>

            <div class="row">

                <div class="col-md-12" style="text-align:center;">

<?php if (!empty($msg)): ?>

                        <div class="alert alert-success">

                            <button class="close" aria-hidden="true" type = "button" data-dismiss = "alert">×</button>

                            <strong>Well done! </strong>

                        <?php echo $msg; ?>

                        </div>

<?php endif; ?>

                </div>

            </div>

            <div class="row margin-bottom-30">

                <div class="col-md-9 mb-margin-bottom-30">

                    <!-- Google Map -->

                    <div id="map-canvas" style="height: 300px;width: 100%"></div><br/>

                    <!--                        <div id="map" class="map map-box map-box-space margin-bottom-40"></div>-->

                    <!-- End Google Map -->



                    <p>There's nothing better than working with good people. If you have a specific project that you'd like to talk about  simply fill in the form under the map and we will get back to you as soon as we can. Also  if you've got some feedback on the site or just want to say hi  drop us an email or have a coffee with us.</p><br />

                    <form method="post">

                        <label >Name</label>

                        <div class="row margin-bottom-20">

                            <div class="col-md-7 col-md-offset-0">

                                <input type="text"  id="contact_us_name" name="contact_us_name" class="form-control">

                            </div>                

                        </div>



                        <label >Email <span class="color-red">*</span></label>

                        <div class="row margin-bottom-20">

                            <div class="col-md-7 col-md-offset-0">

                                <input type="text" id="contact_us_email" name="contact_us_email" class="form-control">

                            </div>                

                        </div>



                        <label >Message</label>

                        <div class="row margin-bottom-20">

                            <div class="col-md-11 col-md-offset-0">

                                <textarea rows="8" class="form-control" id="contact_us_desc" name="contact_us_desc"></textarea>

                            </div>                

                        </div>



                        <p><button type="submit" name="btnSendMessage" class="btn-u">Send Message</button></p>

                    </form>

                </div><!--/col-md-9-->



                <div class="col-md-3">

                    <!-- Contacts -->

                    <div class="headline"><h2>BANGLADESH OFFICE</h2></div>

                    <ul class="list-unstyled who margin-bottom-30">

                        <li><i class="fa fa-home"></i>Razzak Plaza (8th Floor),1 New Eskaton &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Road,Moghbazar Circle, Dhaka-1217</li>

                        <li><a href="mailto:info@ticketchai.com"><i class="fa fa-envelope"></i>info@ticketchai.com</a></li>

                        <li><i class="fa fa-phone"></i> +8801971842538, +8804478009569</li>

                        <li><a href="http://www.ticketchai.com"><i class="fa fa-globe"></i>http://www.ticketchai.com</a></li>

                    </ul>





                    <!-- Why we are? -->

                    <div class="headline"><h2>Why we are?</h2></div>

                    <!--<p>To provide different types of tickets at your door.</p>-->

                    <ul class="list-unstyled">

                        <li><i class="fa fa-check color-green"></i> Buy tickets sitting at home</li>

                        <li><i class="fa fa-check color-green"></i> Pay by credit card, mobile banking or &nbsp;&nbsp;&nbsp;&nbsp;Cash</li>

                        <li><i class="fa fa-check color-green"></i> Get tickets delivered to your doorstep</li>

                        <li><i class="fa fa-check color-green"></i> Buy tickets anytime from anywhere</li>

                    </ul>

                </div><!--/col-md-3-->

            </div><!--/row-->        

        </div><!--/container-->     

        <!--=== End Content Part ===-->
<?php include './newsletter.php'; ?>
<?php include './footer.php'; ?>

    </div><!--/wrapper-->


</body>

</html> 