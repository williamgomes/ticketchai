<?php
include("./admin/config/class.web.config.php");
include("./admin/lib/mpdf/mpdf.php");
$con = new Config();
if (!($con->checkUserLogin())) {
    $con->redirect("index.php");
}
$page_id = 6;
$err = '';
$msg = '';

if ($con->checkUserLogin()) {
    $email = $_SESSION['email'];
    $customer_id = $_SESSION['customer_id'];

    $query = "select * from customer where email='$email' AND customer_id=$customer_id";

    $query_result = mysqli_query($con->open(), $query);
//    $con->debug($query_result);
    if ($query_result) {
        $customer_data = mysqli_fetch_object($query_result);
//$con->debug($customer_data);
    }
}
$shipping_sql = "SELECT shipping_address.*, country.country_name FROM shipping_address INNER JOIN country ON shipping_address.shipping_country = country.country_id WHERE shipping_address.customer_id = ( SELECT customer_id FROM customer WHERE email = '$email' AND customer_id=$customer_id)";
$shipping_sql_result = mysqli_query($con->open(), $shipping_sql);
if ($shipping_sql_result) {
    $shipping_info = mysqli_fetch_object($shipping_sql_result);
//$con->debug($shipping_info);
}


$billing_sql = "SELECT billing_address.*, country.country_name FROM billing_address INNER JOIN country ON billing_address.billing_country = country.country_id WHERE billing_address.customer_id = ( SELECT customer_id FROM customer WHERE email = '$email' AND customer_id=$customer_id)";
$billing_sql_result = mysqli_query($con->open(), $billing_sql);
if ($billing_sql_result) {
    $billing_info = mysqli_fetch_object($billing_sql_result);
// $con->debug($billing_info);
}

$countries = $con->SelectAll("country");

$wishlist_data = $con->ReturnObjectByQuery("SELECT wishlists.*, `event`.*, customer.* FROM wishlists LEFT JOIN `event` ON wishlists.WL_product_id = `event`.event_id LEFT JOIN customer ON wishlists.WL_user_id = customer.customer_id WHERE customer.email = '$email'", "array");
//$con->debug($wishlist_data);
$user_review_data = $con->ReturnObjectByQuery("SELECT user_review.*, `event`.* FROM user_review LEFT JOIN `event` ON user_review.event_id = `event`.event_id WHERE user_id = $customer_id", "array");
//$con->debug($user_review_data);
//donation

$donation_info = "select * from donation where customer_id=$customer_id";
$donation_history = $con->ReturnObjectByQuery($donation_info, "array");
//$con->debug($donation_history);
if (count($donation_history) >= 1) {
    foreach ($donation_history as $don) {
        $key = "btnPrintDonation_" . $don->donation_id;
        if (isset($_POST["$key"])) {
            extract($_POST);
            $donation_key = "donation_id_" . $don->donation_id;
            $donation_id = $_POST["$donation_key"];

            $mpdf = new mPDF('c', 'A4', '', '', 15, 15, 15, 15, 16, 13);
            $mpdf->SetDisplayMode('fullpage');
            $stylesheet = file_get_contents('style.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
            $url = "localhost/ticketchai/donation_print.php?donation_id=" . $donation_id;
            $html = file_get_contents($url);
            $mpdf->WriteHTML($html, 2);
            $mpdf->Output('e_ticket.pdf', 'D');
        }
    }
}

/*
 * Fetch all the  booking information
 * Based on customer ID from session
 * More than one table will be connected
 */

$query_for_booking_history = "SELECT b.schedule_id, b.online_pay_method, ev.event_id,ev.event_title,b.booking_id,b.ticket_price,b.ticket_quantity,
    b.total_amount,b.delivery_cost,b.delivery_method,b.delivery_status,b.life_time_subsciption_cost,b.enrollment_fee,b.anual_subsciption_cost,b.booking_time, b.payment_status,
    (SELECT event_date from event_schedule WHERE event_schedule.event_schedule_id = b.schedule_id) as schedule_date,
    (SELECT event_schedule_start_time from event_schedule WHERE event_schedule.event_schedule_id= b.schedule_id) as schedule_start_time,
    (SELECT event_schedule_end_time from event_schedule WHERE event_schedule.event_schedule_id= b.schedule_id) as schedule_end_time,
    b.delivery_status,b.tocken
    FROM booking as b, event as ev
    WHERE b.event_id = ev.event_id AND b.customer_id =(SELECT customer_id FROM customer WHERE customer.customer_id='$customer_id') ORDER BY b.booking_id DESC";
$booking_historys = $con->ReturnObjectByQuery($query_for_booking_history, "array");
//$con->debug($booking_historys);
?>
<?php include './header_script.php'; ?>

<body class="header-fixed"> 

    <div class="wrapper">
        <div class="header-v5 header-static">
            <?php include './menu_top.php'; ?>
            <?php include './header.php'; ?>
        </div>
        <div class="profile">
            <div class="container content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-body margin-bottom-20">
                            <div class="tab-v1">
                                <ul class="nav nav-justified nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#my_tickets">My Transactions</a></li>
                                    <li class=""><a data-toggle="tab" href="#my_wish_list">My Wish List</a></li>
                                    <li><a data-toggle="tab" href="#my_review_and_ratings">My Review and Ratings</a></li>
                                    <li><a data-toggle="tab" href="#account_settings">Account Settings</a></li>
                                    <!--<li><a data-toggle="tab" href="#donation">Donation</a></li>-->
                                    <li><a data-toggle="tab" href="#contact_support_desk">Contact Support Desk</a></li>

                                </ul> 
                                <div class="tab-content">
                                    <!-- My Ticket Tab Start Here -->
                                    <div id="my_tickets" class="profile-edit tab-pane fade in active" >
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th style="width: 10%">Ticket ID</th>
                                                    <th>Description</th>
                                                    <th>Quantity</th>
                                                    <th>Ticket Price</th>
                                                    <th>Total Amount</th>
                                                    <th>Payment Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                

                                                <?php if (count($booking_historys) > 0) : ?>
                                                    <?php foreach ($booking_historys as $booking): ?>
                                                        <?php 
                                                        $t_price =  $booking->ticket_price;
                                                        $l_price =  $booking->enrollment_fee;
                                                        $a_price = $booking->anual_subsciption_cost;
                                                        $ticket_price = $t_price + $l_price + $a_price;
                                                        
                                                        ?>
                                                        <tr>

                                                            <td><?php echo $booking->tocken; ?></td>
                                                            <td><?php echo $booking->event_title; ?></td>
                                                            <td><?php echo $booking->ticket_quantity; ?></td>
                                                            <td><?php echo $ticket_price; ?></td>
                                                            <td><?php echo $booking->total_amount; ?></td>
                                                            <td><?php echo ucfirst(str_replace('_', ' ', $booking->payment_status)); ?></td>
                                                            <td>
                                                                <span class="label">
                                                                    <?php if ($booking->payment_status == "paid"): ?>
                                                                        <a class="btn-u" href="download_eticket.php?booking_id=<?php echo $booking->booking_id; ?>">Download</a>
                                                                        <a class="btn-u" href="javascript:hideshow(document.getElementById('tiecket_recent_view_<?php echo $booking->booking_id; ?>'))">View</a>
                                                                    <?php else: ?>
                                                                        <a class="btn-u" style="background-color: silver;" disabled="disabled">Download</a>
                                                                        <a class="btn-u" style="background-color: silver;" disabled="disabled">View</a>
                                                                    <?php endif; ?>
                                                                </span>
                                                            </td>
                                                        </tr>

                                                        <!--Ticket Internal View Starts Here-->
                                                        <?php
                                                        $query_for_view = "SELECT booking.*, customer.*, `event`.*
                                                        FROM booking 
                                                        INNER JOIN customer on customer.customer_id = booking.customer_id
                                                        INNER JOIN `event` ON `event`.event_id = booking.event_id 
                                                        WHERE booking.booking_id = '$booking->booking_id'";
                                                        $bookings = $con->ReturnObjectByQuery($query_for_view, "array");
                                                        $event_schedules = $con->SelectAll("event_schedule", "event_id='$booking->event_id'");
                                                        $customer_addition_query_string = "SELECT  customer_addition.*, batch.*, program.* FROM customer_addition
                                                                        INNER JOIN batch on batch.batch_id = customer_addition.CA_batch_id
                                                                        INNER JOIN program on program.program_id = customer_addition.CA_program_id
                                                                        WHERE customer_addition.CA_customer_id = '$customer_id' LIMIT 0,1";
                                                        $customer_addition_query = $con->ReturnObjectByQuery($customer_addition_query_string);
                                                        $event_terms_and_conditions = $con->SelectAll("event_terms_and_conditions", "event_id='$booking->event_id' LIMIT 0,1");
                                                        $event_category = $con->SelectAll("event_category", "event_id='$booking->event_id' LIMIT 0,1");
                                                        ?>
                                                        <tr>
                                                            <td colspan="6">

                                                                <table class="main" id="tiecket_recent_view_<?php echo $booking->booking_id; ?>" style="display:none; width: 100%; border-style: none;">
                                                                    <tr>
                                                                        <td width="139" style="height:50px;" bgcolor="#FFFFFF" rowspan="2">
                                                                            <img src="images/DUDSAA.jpg" class="event_logo" alt=""/>
                                                                        </td>
                                                                        <td colspan="6" bgcolor="#FFFFFF"><h3><strong><?php echo $bookings{0}->event_title; ?></strong></h1></td>
                                                                        <td style="width: 20%;" rowspan="2" align="center" valign="middle" bgcolor="#FFFFFF">
                                                                            <img src="images/DU.jpg" class="event_logo" alt=""/>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>

                                                                        <td colspan="6" bgcolor="#FFFFFF">
                                                                            <h3>
                                                                                Shawkat Osman Auditorium, Central Public Library, Dhaka
                                                                            </h3>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td bgcolor="#FFFFFF"><h6>DATE</h6></td>
                                                                        <td colspan="5" bgcolor="#FFFFFF"><?php echo $event_schedules{0}->event_date; ?></td>
                                                                        <td  bgcolor="#FFFFFF"><h6>TIME</h6></td>
                                                                        <td colspan="3" bgcolor="#FFFFFF">
                                                                            <h6>
                                                                                Time Start: <?php echo $event_schedules{0}->event_schedule_start_time; ?> &nbsp;&nbsp;
                                                                                End Time: <?php echo $event_schedules{0}->event_schedule_end_time; ?>
                                                                            </h6>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td bgcolor="#FFFFFF"><h6>Delivery Method:</h6></td>
                                                                        <td colspan="4" bgcolor="#FFFFFF">
                                                                            <h4>
                                                                                <?php
                                                                                if ($bookings{0}->online_pay_method == "home") {
                                                                                    echo "Home Delivery";
                                                                                } else if ($bookings{0}->online_pay_method == "collect") {
                                                                                    echo "Pick From Ticketchai Office.";
                                                                                } else if ($bookings{0}->online_pay_method == "pick") {
                                                                                    echo "Pick from  Shawkat Osman Memorial Auditorium, Central Public Library";
                                                                                }
                                                                                ?> 
                                                                            </h4>
                                                                        </td>
                                                                        <td bgcolor="#FFFFFF"><h6>CATAGORY</h6></td>
                                                                        <td colspan="4" bgcolor="#FFFFFF">
                                                                            <h6>
                                                                                <?php
                                                                                $category_id = $event_category{0}->category_id;
                                                                                $categories = $con->SelectAll("category", "category_id='$category_id'");
                                                                                echo $categories{0}->category_name;
                                                                                ?>
                                                                            </h6>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="5" valign="top" bgcolor="#FFFFFF" style="font-size: 11px;">
                                                                            <p style="text-align: center; font-size: 14px; font-weight: bold; margin-top: 4px;">Owner's Info</p>
                                                                            <p> Name: <?php echo $bookings{0}->first_name; ?>&nbsp;<?php echo $bookings{0}->last_name; ?></p>
                                                                            <p> Address: <?php
                                                                                echo $bookings{0}->delivery_address;
                                                                                ?></p>
                                                                            <p> Email: <?php echo $bookings{0}->email; ?></p>

                                                                            <p> Phone Number:<?php echo $bookings{0}->delivery_phone_number; ?></p>
                                                                            <p> Batch: <?php echo $customer_addition_query{0}->batch_name; ?></p>

                                                                            <p> Profession: <?php echo $customer_addition_query{0}->CA_profession; ?></p>
                                                                            <p> Designation: <?php echo $customer_addition_query{0}->CA_designation; ?></p>
                                                                            <p> Organization: <?php echo $customer_addition_query{0}->CA_organization; ?></p>
                                                                            <p> Blood Group: <?php echo $customer_addition_query{0}->CA_blood_grp; ?></p>
                                                                            <p> Program Studied: <?php echo $customer_addition_query{0}->program_name; ?></p>
                                                                        </td>
                                                                        <td colspan="6">
                                                                            <table style="width: 100%; margin-left: -8px; font-size: 11px;">
                                                                                <tr>
                                                                                    <td colspan="2" style="text-align: center; font-size: 14px; font-weight: bold; border-style: none;">Payment Details (BDT)</td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <!--Check this, self value is not coming-->
                                                                                    <td style="border-style: none;">Ticket Owner:</td>
                                                                                    <td style="border-style: none; text-align: right">
                                                                                        <?php echo $bookings{0}->ticket_price; ?>                                                                   
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <!--Check this, self value is not coming-->
                                                                                    <td style="border-style: none;">
                                                                                        <?php
                                                                                        if ($bookings{0}->enrollment_fee > 0 OR $bookings{0}->anual_subsciption_cost > 0 OR $bookings{0}->life_time_subsciption_cost > 0) {
                                                                                            echo "(";
                                                                                        }

                                                                                        if ($bookings{0}->enrollment_fee > 0) {

                                                                                            echo "Enroll Fee";
                                                                                            $total_fee = $bookings{0}->enrollment_fee;
                                                                                        }
                                                                                        if ($bookings{0}->anual_subsciption_cost > 0) {
                                                                                            echo ", ";
                                                                                            echo "Annual Subscription";
                                                                                            $total_fee += $bookings{0}->anual_subsciption_cost;
                                                                                        }
                                                                                        
                                                                                        if ($bookings{0}->life_time_subsciption_cost > 0) {
                                                                                            echo ", ";
                                                                                            echo "Lifetime Subscription";
                                                                                            $total_fee += $bookings{0}->life_time_subsciption_cost;
                                                                                        }

                                                                                        if ($bookings{0}->enrollment_fee > 0 OR $bookings{0}->anual_subsciption_cost > 0 OR $bookings{0}->life_time_subsciption_cost > 0) {
                                                                                            echo ")";
                                                                                        }
                                                                                        ?>
                                                                                    </td>
                                                                                    <td style="border-style: none; text-align: right">
                                                                                        <?php echo $total_fee; ?>                                                                   
                                                                                    </td>
                                                                                </tr>



                                                                                <tr>
                                                                                    <td colspan="2" style="border-style: none;"><hr style="border-style: dotted;" /></td>
                                                                                </tr>
                                                                                <?php if ($bookings{0}->delivery_method == "home_delivery"): ?>
                                                                                    <tr>
                                                                                        <td style="border-style: none; font-weight: bold;">Home Delivery:</td>
                                                                                        <td style="border-style: none; font-weight: bold; font-size: 16px; text-align: right">
                                                                                            <?php echo $bookings{0}->delivery_cost; ?>
                                                                                        </td>
                                                                                    </tr>
                                                                                <?php endif; ?>
                                                                                <tr>
                                                                                    <td style="border-style: none; font-weight: bold;">Grand Total:</td>
                                                                                    <td style="border-style: none; font-weight: bold; font-size: 16px; text-align: right">
                                                                                        <?php echo $bookings{0}->total_amount; ?>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>        
                                                                    <tr>
                                                                        <td height="74" bgcolor="#FFFFFF">EVENT DETAILS</td>
                                                                        <td colspan="9" align="left" bgcolor="#FFFFFF" style="text-align: justify;">
                                                                            <?php
                                                                            $event_id = $bookings{0}->event_id;
                                                                            $event_description = $con->SelectAll("event_details", "event_id='$event_id'");
                                                                            echo html_entity_decode(html_entity_decode($event_description{0}->event_details_description));
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="43" colspan="10" align="center" bgcolor="#FFFFFF" style="padding-bottom: 0px; padding-top: 0px;">
                                                                            E-Ticket NO:
                                                                            <?php if ($bookings{0}->tocken != ''): ?>
                                                                                <script type="text/javascript">
                                                                                    $(document).ready(function () {
                                                                                        $("#tocken_recent_<?php echo $bookings{0}->tocken; ?>").barcode(
                                                                                                "<?php echo $bookings{0}->tocken; ?>", // Value barcode (dependent on the type of barcode)
                                                                                                "code39" // type (string)
                                                                                                );
                                                                                    });
                                                                                </script>
                                                                                <div id="tocken_recent_<?php echo $bookings{0}->tocken; ?>"></div>
                                                                            <?php endif; ?>


                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="10" align="center" bgcolor="#FFFFFF" style="border-bottom-color: white;">TERMS &amp; CONDITIONS</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="10" bgcolor="#FFFFFF" style=" text-align: justify;">   
                                                                            <?php echo html_entity_decode(html_entity_decode($event_terms_and_conditions{0}->event_terms_and_conditions_description)); ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="10" align="center" bgcolor="#FFFFFF" style="border-bottom-style: none;">TICKETING PARTNER</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="border-top-style: none;" colspan="10" bgcolor="#FFFFFF">
                                                                            <table style="width: 100%; margin-left: -8px;">
                                                                                <tr>
                                                                                    <td style="border-style: none; width: 46%">
                                                                                        <h5>Hot Line Number:  +8801971842538,+8804478009569 </h5>
                                                                                        <h5> <font style="color:white;">Hot Line Number:</font> +8801716020445,+8801624690104 </h5>
                                                                                    </td>
                                                                                    <td style="border-style: none;"><img src="assets_new/ticketchai_logo.png" width="100" height="50" alt=""/></td>
                                                                                    <td style="border-style: none; text-align: right"></td>
                                                                                </tr>              
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table> 
                                                            </td>

                                                        </tr>
                                                        <!--Ticket Internal View Ends here-->
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </tbody>
                                        </table>

                                        <script type="text/javascript">
                                            function hideshow(which) {
                                                if (!document.getElementById)
                                                    return;
                                                if (which.style.display === "block")
                                                    which.style.display = "none";
                                                else
                                                    which.style.display = "block";
                                            }
                                            ;
                                        </script>
                                        <div style="height: 150px;"></div>
                                    </div>
                                    <!-- My Ticket Tab End Here -->
                                    
                                    

                                    <!-- My Wishlist Tab Start Here style="overflow-x: scroll;" -->

                                    <div id="my_wish_list" class="profile-edit tab-pane fade">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th style="width: 10%">Thumbnail</th>
                                                    <th style="width: 70%">Details</th>
                                                    <th style="width: 20%">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if (count($wishlist_data) > 0): ?>
                                                    <?php foreach ($wishlist_data as $ws): ?>
                                                        <tr id="wishlist_event_<?php echo $ws->WL_id; ?>">
                                                            <td>
                                                                <img src="<?php echo "admin/uploads/event_logo_image/" . $ws->event_logo_image; ?>" style="height: auto;width: 100px;" class="event_logo" alt=""/>
                                                            </td>
                                                            <td><?php echo $ws->event_title; ?></td>
                                                            <td> 
                                                                <span class="label">
                                                                    <a style="margin-top: 15px;" class="btn-u" href="event_details.php?event_id=<?php echo base64_encode($ws->event_id); ?>"  name="add_to_cart">Add To Cart</a>
        <!--                                                                    <input type="submit" class="btn-u"   name="share_it" value="Share It"/>-->
                                                                    <input type="submit" class="btn-u" onclick="javascript:removeWishlist(<?php echo $ws->WL_id; ?>)" value="Remove"/>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </tbody>
                                        </table>
                                         <div style="height: 150px;"></div>
                                    </div>
                                    <!-- My Wishlist Tab End Here -->
                                    <!-- My Review And Ratings Tab Start Here -->
                                    <div id="my_review_and_ratings" class="profile-edit tab-pane fade">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th style="width: 10%">Thumbnail</th>
                                                    <th style="width: 40%">Details</th>
                                                    <th style="width: 20%">Ratings</th>
                                                    <th style="width: 30%">Review</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if (count($user_review_data) > 0): ?>
                                                    <?php foreach ($user_review_data as $ur): ?>
                                                        <tr id="user_review_event_<?php echo $ur->user_review_id; ?>">
                                                            <td>
                                                                <img src="<?php echo "admin/uploads/event_logo_image/" . $ur->event_logo_image; ?>" style="height: auto;width: 100px;" class="event_logo" alt=""/>
                                                            </td>
                                                            <td><?php echo $ur->event_title; ?></td>
                                                            <td>
                                                                <ul class="list-inline product-ratings">
                                                                    <?php
                                                                    $rat = $ur->ratings;
                                                                    for ($i = 0; $i < $rat; $i++) {
                                                                        echo '<li><i class = "rating-selected fa fa-star"></i></li>';
                                                                    }

                                                                    for ($j = $rat; $j < 5; $j++) {
                                                                        echo '<li><i class = "rating fa fa-star"></i></li>';
                                                                    }
                                                                    ?>
                                                                </ul>

                                                            </td>
                                                            <td><?php echo $ur->review; ?></td>

                                                        </tr>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </tbody>
                                        </table>
                                         <div style="height: 150px;"></div>
                                    </div>
                                    <!-- My Review And Ratings Tab End Here -->


                                    <!-- Account settings Tab Start Here -->
                                    <div id="account_settings" class="profile-edit tab-pane fade">
                                        <?php if ($customer_data->first_name == "" || $customer_data->last_name == "" || $customer_data->dob == "" || $customer_data->gender == ""): ?>
                                            <div class="alert alert-warning fade in">
                                                <strong>Warning!</strong> You must complete your profile. Otherwise you can not buy tickets.
                                            </div>
                                        <?php endif; ?>
                                        <h2 class="heading-md"><u>My Profile</u></h2>
                                        </br>
                                        <dl class="dl-horizontal">
                                            <dt><strong>Account Type </strong></dt>
                                            <dd>
                                                User
                                                <span>
                                                    <a class="pull-right" href="#"></a>
                                                </span>
                                            </dd>
                                            <dt><strong>Account ID </strong></dt>
                                            <dd>
                                                41124
                                                <span>
                                                    <a class="pull-right" href="#"></a>
                                                </span>
                                            </dd>
                                            <dt><strong>Name </strong></dt>
                                            <dd>
                                                <?php echo '<span id="first_name_main">' . $customer_data->first_name . '</span>'; ?>&nbsp;<?php echo '<span id="last_name_main">' . $customer_data->last_name . '</span>'; ?>
                                            </dd>
                                            <dt><strong>DOB </strong></dt>
                                            <dd>
                                                <?php
                                                if ($customer_data->dob == "") {
                                                    echo "Not Given";
                                                } else {
                                                    echo '<span id="dob_main">' . date("d M, Y", strtotime($customer_data->dob)) . '</span>';
                                                }
                                                ?>
                                            </dd>
                                            <dt><strong>Gender </strong></dt>
                                            <dd>
                                                <?php
                                                if ($customer_data->gender == "") {
                                                    echo "Not given";
                                                } else {
                                                    echo '<span id="gender_main">' . $customer_data->gender . '</span>';
                                                }
                                                ?>
                                            </dd>
                                            <dt><strong>Login Email</strong></dt>
                                            <dd>
                                                <?php echo $customer_data->email; ?>

                                            </dd>

                                            <dd>
                                                <p><a href="" data-toggle="modal" data-target=".bs-example-modal-lg">[Edit]</a></p>
                                            </dd>
                                            <div style="padding-top: 50px;" id="myaccount" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg" style="width:300px;">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                            <h4 id="myLargeModalLabel" class="modal-title">Change User Profile</h4>
                                                        </div>
                                                        <div id="error"></div>
                                                        <form >
                                                            <div class="modal-body">
                                                                <input type="hidden" class="form-control" id="customer_id" name="customer_id" value="<?php echo $customer_data->customer_id; ?>">  
                                                                <label>First Name</label>
                                                                <input class="form-control" type="text" id="first_name_edit" name="first_name" value="<?php echo $customer_data->first_name; ?>">

                                                                <label>Last Name</label>
                                                                <input class="form-control" type="text" id="last_name_edit" name="last_name" value="<?php echo $customer_data->last_name; ?>">
                                                                <!--<label>Phone Number</label>-->
                                                                <!--<input class="form-control" type="text" id="phone" name="phone" value="<?php //echo $customer_data->phone;                                       ?>">-->

                                                                <label>Gender</label>
                                                                <label class="select margin-bottom-15" style="width: 460px;" >
                                                                    <select name="gender" id="gender_edit" class="form-control" value="<?php echo $customer_data->gender; ?>">
                                                                        <option value="0" <?php
                                                                        if ($customer_data->gender == "") {
                                                                            echo "selected";
                                                                        }
                                                                        ?>>Select Gender</option>
                                                                        <option value="male" <?php
                                                                        if ($customer_data->gender == "male") {
                                                                            echo "selected";
                                                                        }
                                                                        ?>>Male</option>
                                                                        <option value="female" <?php
                                                                        if ($customer_data->gender == "female") {
                                                                            echo "selected";
                                                                        }
                                                                        ?>>Female</option>
                                                                        <option value="other" <?php
                                                                        if ($customer_data->gender == "other") {
                                                                            echo "selected";
                                                                        }
                                                                        ?>>Other</option>
                                                                    </select>
                                                                </label>
                                                                <label>Date Of Birth</label>

                                                                <label>
                                                                    <input style="width: 267px !important;" id="dob_edit" name="dob" value="<?php echo date("Y/m/d", strtotime($customer_data->dob)); ?>" />
                                                                </label>
                                                                <script type="text/javascript">
                                                                    jQuery(document).ready(function () {
                                                                        jQuery("#dob_edit").kendoDatePicker();
                                                                    });
                                                                </script>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="submit" onclick="return false;" id="save_account_settings_data" name="btnSaveUser" class="btn-u btn-u-primary">Save changes</button>
                                                            </div>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </dl>
                                        <hr/>
                                        <h3><u>My Address Book</u></h3>
                                        <div class="col-md-12" style="border: 1px solid;">
                                            <div class="col-md-6" >
                                                <h5><b><i>Billing Address</i></b></h5>
                                                <?php
                                                if (count($billing_info) == "") {
                                                    echo "Not Given";
                                                } else {
                                                    echo 'Address:&nbsp;<span id="billing_address_main">' . $billing_info->billing_address . '</span>';
                                                    echo '<br/>';
                                                    echo 'City:&nbsp;<span id="billing_city_main">' . $billing_info->billing_city . '</span>';
                                                    echo '<br/>';
                                                    echo 'Post Code: &nbsp;<span id="billing_post_code_main">' . $billing_info->billing_post_code . '</span>';
                                                    echo '<br/>';
                                                    echo 'Country:&nbsp;<span id="billing_country_main">' . $billing_info->country_name . '</span>';
                                                }
                                                ?>

                                                <?php //if (count($billing_info) == ""):   ?>
                                                <!--<p><a  disabled="disabled"> [Change]</a></p>-->
                                                <?php // else:    ?>
                                                    <!--<p><a href="" data-toggle="modal" data-target=".bs-example-modal-sm">[Change]</a></p>-->

                                                <?php //endif;   ?>
                                                <p><a href="" data-toggle="modal" data-target=".bs-example-modal-sm">[Change]</a></p>
                                                <div style="padding-top: 50px;" id="bill" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-sm">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                                <h4 id="myLargeModalLabel" class="modal-title">Change Billing Address</h4>
                                                            </div>
                                                            <div id="error_billing"></div>
                                                            <form>
                                                                <div class="modal-body">
                                                                    <input type="hidden" id="billing_address_id" name="billing_address_id" value="<?php echo $billing_info->billing_address_id; ?>"/>
                                                                    <input type="hidden" id="customer_id" name="customer_id" value="<?php echo $billing_info->customer_id; ?>"/>

                                                                    <label>Address</label>
                                                                    <input type="text" class="form-control margin-bottom-20" id="billing_address" name="billing_address" value="<?php echo $billing_info->billing_address; ?>">

                                                                    <label>Post Code</label>
                                                                    <input type="text" class="form-control margin-bottom-20" id="billing_post_code" name="billing_post_code" value="<?php echo $billing_info->billing_post_code; ?>">
                                                                    <label>City</label>
                                                                    <input type="text" class="form-control margin-bottom-20" id="billing_city" name="billing_city" value="<?php echo $billing_info->billing_city; ?>">
                                                                    <label>Country</label>
                                                                    <select style="width: 100%" id="billing_country" name="billing_country" class="form-control">
                                                                        <option value="0">Select a Country</option>
                                                                        <?php if (count($countries) >= 1): ?>
                                                                            <?php foreach ($countries as $country): ?>
                                                                                <option value="<?php echo $country->country_id; ?>"  
                                                                                <?php
                                                                                if ($country->country_id == $billing_info->billing_country) {
                                                                                    echo ' selected="selected"';
                                                                                }
                                                                                ?>><?php echo $country->country_name; ?></option>
                                                                                    <?php endforeach; ?>
                                                                                <?php endif; ?>
                                                                    </select>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button onclick="return false;" id="change_billing_address" type="submit" name="btnSaveBillAddress" class="btn-u btn-u-primary">Save changes</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <h5><b><i>Shipping Address</i></b></h5>

                                                <?php
                                                if (count($shipping_info) == "") {
                                                    echo "Not Given";
                                                } else {
                                                    echo 'Address:&nbsp;<span id="shipping_address_main">' . $shipping_info->shipping_address . '</span>';
                                                    echo '<br/>';
                                                    echo 'City:&nbsp;<span id="shipping_city_main">' . $shipping_info->shipping_city . '</span>';
                                                    echo '<br/>';
                                                    echo 'Post Code:&nbsp;<span id="shipping_post_code_main">' . $shipping_info->shipping_post_code . '</span>';
                                                    echo '<br/>';
                                                    echo 'Country:&nbsp;<span id="shipping_country_main">' . $shipping_info->country_name . '</span>';
                                                }
                                                ?>


                                                <p><a href="" data-toggle="modal" data-target="#myModal">[Change]</a></p>



                                                <div  style="padding-top: 50px;" class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" style="width: 300px !important;">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                                <h4 id="myModalLabel" class="modal-title">Change Shipping Address</h4>
                                                            </div>
                                                            <div id="error_shipping"></div>
                                                            <form>
                                                                <div class="modal-body">
                                                                    <input type="hidden" id="customer_id" name="customer_id" value="<?php echo $shipping_info->customer_id; ?>"/>
                                                                    <input type="hidden" id="shipping_address_id" name="shipping_address_id" value="<?php echo $shipping_info->shipping_address_id; ?>"/>
                                                                    <label>Address</label>
                                                                    <input type="text" id="shipping_address" class="form-control margin-bottom-20" name="shipping_address" value="<?php echo $shipping_info->shipping_address; ?>">
                                                                    <label>Post Code</label>
                                                                    <input type="text" class="form-control margin-bottom-20" id="shipping_post_code" name="shipping_post_code" value="<?php echo $shipping_info->shipping_post_code; ?>">

                                                                    <label>City</label>
                                                                    <input type="text" class="form-control margin-bottom-20" id="shipping_city" name="shipping_city" value="<?php echo $shipping_info->shipping_city; ?>">
                                                                    <label>Country</label>
                                                                    <select id="shipping_country" style="width: 100%" name="shipping_country" class="form-control">
                                                                        <option value="0">Select a Country</option>
                                                                        <?php if (count($countries) >= 1): ?>
                                                                            <?php foreach ($countries as $country): ?>
                                                                                <option value="<?php echo $country->country_id; ?>"  
                                                                                <?php
                                                                                if ($country->country_id == $shipping_info->shipping_country) {
                                                                                    echo ' selected="selected"';
                                                                                }
                                                                                ?>><?php echo $country->country_name; ?></option>
                                                                                    <?php endforeach; ?>
                                                                                <?php endif; ?>
                                                                    </select>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button onclick="return false;" id="change_shipping_address" type="submit" name="btnSaveShipAddress" class="btn-u btn-u-primary">Save changes</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- Account settings Tab End Here -->



                                    <!-- Donation Tab Start Here-->
                                    <div id="donation" class="profile-edit tab-pane fade">
                                        <form method="post">
                                            <table class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 15%">E-Donation No</th>
                                                        <th style="width: 15%">Name</th>
                                                        <th style="width: 10%">Batch No</th>
                                                        <th style="width: 20%">Donation Amount</th>
                                                        <th style="width: 20%">Status</th>
                                                        <th style="width: 15%">Payment Method</th>
                                                        <th style="width: 20%">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($donation_history as $don): ?>
                                                        <tr>
                                                            <td>
                                                                <?php echo $don->token_no; ?>
                                                            </td>
                                                            <td><?php echo $don->name; ?></td>
                                                            <td><?php echo $don->batch_id; ?></td>
                                                            <td><?php echo $don->amount; ?></td>
                                                            <td><?php echo ucfirst(str_replace('_', ' ', $don->payment_status)); ?></td>
                                                            <td>

                                                                <span class="label label-warning"><?php echo ucfirst(str_replace('_', ' ', $don->delivery_method)); ?></span>
                                                                <input type="hidden" value="<?php echo $don->donation_id; ?>" name="donation_id_<?php echo $don->donation_id; ?>">
                                                            </td> 
                                                            <td>
                                                                <!--Action buttons based on payment status-->
                                                                <span class="label">
                                                                    <?php if ($don->payment_status == "paid"): ?>
                                                                        <input type="submit" class="btn-u"   name="btnPrintDonation_<?php echo $don->donation_id; ?>" value="Download"/>
                                                                        <a class="btn-u" href="javascript:hideshow(document.getElementById('tiecket_view_<?php echo $don->donation_id; ?>'))">View</a>
                                                                    <?php else: ?>
                                                                        <input type="submit" class="btn-u" value="Download" style="background-color: silver;" disabled="disabled"/>
                                                                        <a class="btn-u" style="background-color: silver;" disabled="disabled"> View</a>
                                                                    <?php endif; ?>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <?php $customers = $con->SelectAll("customer", "customer_id='$don->customer_id'"); ?>
                                                        <?php $events = $con->SelectAll("event", "event_id='$don->event_id'"); ?>
                                                        <tr>
                                                            <td colspan="6">
                                                                <div id="tiecket_view_<?php echo $don->donation_id; ?>" style="display:none; width: 100%;">
                                                                    <div class="top_text">
                                                                        Please print and bring this ticket with you, if further necessary.
                                                                    </div>
                                                                    <br />

                                                                    <table class="main">
                                                                        <tr>
                                                                            <td width="100" rowspan="2" style="height:50px;" bgcolor="#FFFFFF">
                                                                                <img src="images/DUDSAA.jpg" class="event_logo" alt=""/>
                                                                            </td>
                                                                            <td colspan="5" bgcolor="#FFFFFF"><h3><strong>Donation Reciept</strong></h1></td>
                                                                            <td colspan="4" rowspan="2" align="center" valign="middle" bgcolor="#FFFFFF">
                                                                                <img src="images/DU.jpg" class="event_logo" alt=""/>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td colspan="5" bgcolor="#FFFFFF"><h3>Thank You For Your Contribution!</h3></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="10" bgcolor="#FFFFFF">
                                                                                <h3>
                                                                                    Launching Ceremony of Dhaka University Development Studies Alumni Association
                                                                                </h3>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td bgcolor="#FFFFFF"><h6>DATE</h6></td>
                                                                            <td colspan="4" bgcolor="#FFFFFF"> 31-01-2015</td>
                                                                            <td  bgcolor="#FFFFFF"><h6>TIME</h6>  </td>
                                                                            <td colspan="4" bgcolor="#FFFFFF">
                                                                                <h6> 
                                                                                    09:00 AM to 02:00 PM
                                                                                </h6>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td bgcolor="#FFFFFF"><h6>Delivery Method:</h6></td>
                                                                            <td colspan="4" bgcolor="#FFFFFF">
                                                                                <h4>
                                                                                    Online Payment
                                                                                </h4>
                                                                            </td>
                                                                            <td bgcolor="#FFFFFF"><h6>CATAGORY</h6></td>
                                                                            <td colspan="4" bgcolor="#FFFFFF"><h6> Launching Ceremony </h6></td>
                                                                        </tr>


                                                                        <tr>
                                                                            <?php foreach ($customers as $customer): ?>
                                                                                <td colspan="5" valign="top" bgcolor="#FFFFFF" style="font-size: 11px;">
                                                                                    <p style="text-align: center; font-size: 14px; font-weight: bold; margin-top: 4px;">Owner's Info</p>
                                                                                    <p> Name: <?php echo $customer->first_name; ?>&nbsp;<?php echo $customer->last_name; ?></p>
                                                                                    <p> Email: <?php echo $customer->email; ?></p>

                                                                                <?php endforeach; ?>
                                                                            </td>
                                                                            <td colspan="6">
                                                                                <table style="width:100%;">
                                                                                    <tr>
                                                                                        <td style="border-style: none; width: 50%;">Donated Amount:</td>
                                                                                        <td style="border-style: none; text-align: right; width: 50%;">
                                                                                            <?php echo $don->amount; ?>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2" style="border-style: none;"><hr style="border-style: dotted;" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="border-style: none; font-weight: bold;">Grand Total:</td>
                                                                                        <td style="border-style: none; font-weight: bold; font-size: 16px; text-align: right">
                                                                                            <?php echo $don->amount; ?>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="43" colspan="10" align="center" bgcolor="#FFFFFF" style="border-bottom-color: white; padding-bottom: 0px; padding-top: 0px;">Donation Token NO:
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td colspan="10" align="center" bgcolor="#FFFFFF" style="padding-top: 0px; border-top-color: white;" >


                                                                                <script type="text/javascript" src="jquery-barcode.js"></script>

                                                                                <script type = "text/javascript" >
                                                                        $(document).ready(function () {
                                                                            $("#tocken_recent_3<?php echo $don->token_no; ?>").barcode(
                                                                                    "<?php echo $don->token_no; ?>", // Value barcode (dependent on the type of barcode)
                                                                                    "code39" // type (string)
                                                                                    );
                                                                        });
                                                                                </script>

                                                                                <div id="tocken_recent_3<?php echo $don->token_no; ?>">
                                                                                    <?php echo $don->token_no; ?>
                                                                                </div>


                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="10" align="center" bgcolor="#FFFFFF" style="border-bottom-style: none;">TICKETING PARTNER</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="border-top-style: none;" colspan="10" bgcolor="#FFFFFF">
                                                                                <table style="width: 100%; margin-left: -8px;">
                                                                                    <tr>
                                                                                        <td style="border-style: none; width: 46%">
                                                                                            <h5>Hot Line Number:  +8801971842538,+8804478009569 </h5>
                                                                                            <h5> <font style="color:white;">Hot Line Number:</font> +8801716020445,+8801624690104 </h5>
                                                                                        </td>
                                                                                        <td style="border-style: none;  width: 33%"><img src="ticketchai_logo.png" width="100" height="50" alt=""/></td>
                                                                                        <td style="border-style: none; width: 33%; text-align: right;"></td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>

                                                                    </table>

                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>

                                            </table>
                                        </form>
                                    </div>
                                    <div class="clearfix"></div>
                                    <!--Donation Tab End Here -->

                                    <!-- Contact Support Desk Tab Start Here-->
                                    <div id="contact_support_desk" class="profile-edit tab-pane fade" style="height: 200px;">
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="col-md-6">
                                                    <h4>Contact Information</h4>
                                                    <ul>
                                                        <li>Razzak Plaza (8th Floor),1 New Eskaton Road,Moghbazar Circle, Dhaka-1217 </li>
                                                        <li>Phone: +8801971842538,+8804478009569</li>
                                                        <li>Website:  www.ticketchai.com</li>
                                                        <li>Email: support@ticketchai.com</li>
                                                    </ul>

                                                </div>
                                                <div class="col-md-4" style="margin-left: 703px;margin-top: -82px;">
                                                    <img src="admin/assets/images/img_1.png"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Contact Support Desk Tab End Here-->



                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include './newsletter.php'; ?>
        <?php include './footer.php'; ?>

    </div>
</body>
</html>