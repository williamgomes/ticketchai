<!-- JS Global Compulsory -->
<script src="assets_new/plugins/jquery/jquery.min.js"></script>
<script src="assets_new/plugins/jquery/jquery-migrate.min.js"></script>
<script src="assets_new/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- JS Implementing Plugins -->
<script src="assets_new/plugins/back-to-top.js"></script>
<script src="assets_new/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script src="assets_new/plugins/scrollbar/src/jquery.mousewheel.js"></script>
<script src="assets_new/plugins/scrollbar/src/perfect-scrollbar.js"></script>
<script src="assets_new/plugins/jquery.parallax.js"></script>
<script src="assets_new/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="assets_new/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<!-- Master Slider -->
<script src="assets_new/plugins/master-slider/quick-start/masterslider/masterslider.min.js"></script>
<script src="assets_new/plugins/master-slider/quick-start/masterslider/jquery.easing.min.js"></script>
<!-- Scrollbar -->
<script src="assets_new/plugins/scrollbar/src/jquery.mousewheel.js"></script>
<script src="assets_new/plugins/scrollbar/src/perfect-scrollbar.js"></script>
<script src="assets_new/js/plugins/master-slider.js"></script>
<script src="assets_new/js/forms/product-quantity.js"></script>
<!-- JS Customization -->
<script src="assets_new/js/custom.js"></script>
<!-- JS Page Level -->
<script src="assets_new/js/shop.app.js"></script>
<script src="assets_new/js/plugins/owl-carousel.js"></script>
<script src="assets_new/js/plugins/revolution-slider.js"></script>
<script src="assets_new/js/shop.app.js"></script>
<script src="assets_new/js/forms/page_login.js"></script>
<script src="assets_new/js/forms/page_contact_form.js"></script>
<!-- Login Form -->
<script src="assets_new/plugins/sky-forms/version-2.0.1/js/jquery.form.min.js"></script>
<!-- Validation Form -->
<script src="assets_new/plugins/sky-forms/version-2.0.1/js/jquery.validate.min.js"></script>
<!-- Registration Form -->
<script src="assets_new/js/forms/page_registration.js"></script>
<script>
    jQuery(document).ready(function() {
        App.init();
        Registration.initRegistration();      
    });
</script>
<!-- Kendo UI Script Linked Here -->
<script type="text/javascript" src="assets_new/kendo/js/kendo.web.min.js"></script>

<script>
    jQuery(document).ready(function () {
        App.init();
        App.initParallaxBg();
        OwlCarousel.initOwlCarousel();
        RevolutionSlider.initRSfullWidth();
    });
</script>
<script>
    jQuery(document).ready(function () {
        App.init();
        OwlCarousel.initOwlCarousel();
        MasterSliderShowcase2.initMasterSliderShowcase2();
    });
</script>
<script>
    jQuery(document).ready(function () {
        App.init();
        Login.initLogin();
        PageContactForm.initPageContactForm();
    });
</script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery("#login_form").click(function () {
            jQuery("#login_window").kendoWindow({
                width: "505px",
                height: "390px",
                visible: false,
                modal: true,
                title: "Login To Your Account"
            }).data("kendoWindow").center().open();;
        });
    });
</script>

<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery("#register_form").click(function () {
            jQuery("#register_window").kendoWindow({
                width: "530px",
                height: "700px",
                visible: false,
                modal: true,
                title: "Create New Account"
            }).data("kendoWindow").center().open();;
        });
    });
</script>
