<?php
include './admin/config/class.web.config.php';
$con = new Config();

if ($con->checkUserLogin() == FALSE) {
    $con->redirect('signin-signup.php?param=donate');
}
$batch_id = '';
$program_id = '';
$batches = $con->SelectAll("batch", "", "", "array");
$program = $con->SelectAll("program", "", "", "array");
//$con->debug($program);
?>

<?php include './header_script.php'; ?>

<body class="header-fixed">
    <div class="wrapper">
        <div class="header-v5 header-static">
            <?php include './menu_top.php'; ?>
            <?php include './header.php'; ?>
        </div>
        <div class="container content">


            <div class="panel panel-grey margin-bottom-40">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-globe"></i> Please Donate Here</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                             <label>Program Studied</label>
                            <select class="form-control" id="program_id"  name="program_id" style="width: 400px;">
                                <option value="0">Select Program</option>
                                <?php if (count($program) >= 1): ?>
                                    <?php foreach ($program as $p): ?>
                                        <option value="<?php echo $p->program_id; ?>" 
                                        <?php
                                        if ($p->program_id == $program_id) {
                                            echo " selected='selected'";
                                        }
                                        ?>
                                                ><?php echo $p->program_name; ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                            </select>
                            
                        </div>
                        <div class="col-md-6">
                             <label>Batch No.</label>
                            <select class="form-control" id="batch_id"  name="batch_id" style="width: 400px;">
                                <option value="0">Select Batch</option>
                                <?php if (count($batches) >= 1): ?>
                                    <?php foreach ($batches as $bc): ?>
                                        <option value="<?php echo $bc->batch_id; ?>" 
                                        <?php
                                        if ($bc->batch_id == $batch_id) {
                                            echo " selected='selected'";
                                        }
                                        ?>
                                                ><?php echo $bc->batch_name; ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                            </select>
                            
                        </div>
               
                        
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-6">
                             <label class="input">
                                Your Name:
                            </label>
                            <input style="width: 400px;" id="name" type="text" name="name" placeholder="Your name" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label class="input">
                                Enter Amount (BDT.):
                            </label>
                            <input style="width: 400px;" type="number" min="500" id="amount" name="amount" placeholder="Amount" onkeypress="return isNumberKey(event)" value="500" class="form-control">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <button onclick="return false;" type="submit" style="color:white; margin-top: 10px;margin-left:889px;" class="pull-left btn btn-success" id="ssl_payment_donation" >Pay Online</button> 
                        
                    </div>
               

                    <form action="https://www.sslcommerz.com.bd/process/index.php" method="post" name="form1">
                        <input type="hidden" name="store_id" value="ticketchailive001"> 
                        <?php
                        //ticketchailive001
                        //systechunimaxtest001
                        ?>
                        <input type="hidden" id="total_amount_ssl" name="total_amount" value="">
                        <input type="hidden" id="trans_id_ssl" name="tran_id" value="">
                        <input id="notify_url" type="hidden" name="success_url"
                               value="">
                        <input type="hidden" id="fail_url" name="fail_url" value = "">
                        <input type="hidden" id="cancle_url" name="cancel_url" value = "">
                        <input class="pull-right" id="ssl_main" type="submit" value="Pay Online"  style="display: none;"  name="pay">
                    </form>
                </div>                      
            </div>
        </div>
        <?php include './newsletter.php'; ?>
        <?php include './footer.php'; ?>
    </div>

</body>
</html> 

