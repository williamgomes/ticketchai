<?php include './login_popup.php'; ?>
<?php include './signup_popup.php'; ?>
<?php  include './ajax/googleLogin.php'; ?>
<?php  include './ajax/facebookLogin.php'; ?>
<?php // $con = new Config(); ?>

<div class="topbar-v3">
    <div class="search-open">
        <div class="container">
            <input type="text" class="form-control" placeholder="Search">
            <div class="search-close"><i class="icon-close"></i></div>
        </div>    
    </div>

    <div class="container" style="font-size: medium;">
        <div class="row">
            <div class="col-sm-6">
                <li><i class="search fa fa-search search-button"> <span style="font-family: 'Open Sans', sans-serif; font-weight: bolder;">Search</span></i></li>
            </div>
           
            <div class="col-sm-6">
                <ul class="list-inline right-topbar pull-right" style="font-family: 'Open Sans', sans-serif; font-weight: bolder;">
                    <?php if($con->checkUserLogin()): ?>
                        <li><a href="customer_dashboard.php"><?php echo $_SESSION['first_name']; ?></a> | <a href="javascript:void();" onclick="javascript:userLogout();">Logout</a></li>
                    <?php else: ?>
                        <li><a href="javascript:void(0);" id="login_form">Login</a> | <a href="javascript: void(0);" id="register_form"> Register</a></li>
                    <?php endif; ?>  
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" id="success-msg">
    <h1 id="message-suc" style="color: white; font-weight: bold;"></h1>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" id="error-msg">
    <h2 id="message-err" style="color: white; font-weight: bold;"></h2>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" id="warning-msg">
    <h2 id="message-war" style="color: white; font-weight: bold;"></h2>
</div>
<div id="kWindow"></div>
