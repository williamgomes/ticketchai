<?php
include './admin/config/class.web.config.php';
$con = new Config();
require 'admin/lib/PHPMailer/PHPMailerAutoload.php';
$mail = new PHPMailer();
$mail->setFrom('support@ticketchai.com', 'Ticket Chai');
$mail->addReplyTo('support@ticketchai.com', 'Ticket Chai');

$page_id = 25;
$valid_payment = '';
//$val_id = $_POST['val_id'];
if (isset($_GET["itemid"])) {
    $update_array = array("donation_id" => $_GET["itemid"], "is_online" => "cancel", "payment_status" => "unpaid");
    $result = $con->Update("donation", $update_array, "", "", "array");
    //$con->debug($result);
    if ($result["output"] == "success") {
        $valid_payment = 1;
        //fetch customer info
        $donation_info = $con->SelectAll("donation", "", array("donation_id" => $_GET["itemid"]), "array");
        $cus_id = $donation_info{0}->customer_id;
        $customers = $con->ReturnObjectByQuery("SELECT * FROM customer where customer_id=$cus_id", "array");
        //$coll_id = $donation_info{0}->college_validation_id;
        $customer_email = $customers{0}->email;
        $customer_name = $customers{0}->first_name . " " . $customers{0}->last_name;
        //Send mail
        $mail->Subject = 'Donation Transaction Canceled!';
        $htmlString = '<table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tbody><tr>
            <td valign="top" align="center">
                <table cellspacing="0" cellpadding="10" width="650" border="0" bgcolor="#FFFFFF" style="border:1px solid #E0E0E0;">

                    <tbody><tr>
                            <td valign="top" class="ecxfirst" style="">
                                <a target="_blank" href="http://www.ticketchai.org/" style="font-size:20px;color:#383838;text-decoration:none;" class=""><img border="0" style="" alt="" src="http://www.ticketchai.org/ticketchai_logo.png"></a>
                                <span style="font-family:Arial_Rounded_MT_Bold;">
                                     Ticket Chai Office Hotline : +8801971-842538 or +88 04478009569<br/>
                                     DUDSAA Hotline: +88 01716020445 or  +88 01624690104
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td valign="top">
                                <h1 style="font-size:22px;font-weight:normal;line-height:22px;">Hello, ' . $customers{0}->first_name . ' ' . $customers{0}->last_name . ' </h1>
                                <p style="font-size:12px;line-height:16px;">
                                    You have canceled transaction request for your donation.
                                    </a>.
                                    If you have further requests, please contact us at <a style="color:#1E7EC8;" href="mailto:support@ticketchai.com">support@ticketchai.com</a> <br>or call us at <span class="nobr">+8801971842538,+8804478009569 </span> Everyday, 10am - 8pm BDT.
                                </p><br>
                                
                            </td></tr>
                        
                       
                        <tr>
                            <td bgcolor="#FFFFFF" align="center" class="ecxlast"><center><p style="font-size:12px;">&copy; 2014 ticketchai.com Ltd. All Rights Reserved</p></center></td>
                </tr>
            </tbody></table>
        </td>
        </tr>
        </tbody></table>';
        $mail->addAddress("$customer_email", "$customer_name");
        $mail->msgHTML($htmlString);
        $mail->AltBody = 'This is a plain-text message body';
        if (!$mail->send()) {
            
        } else {
            
        }
    }
}

//$donation_info = array();
//$donation_info = $con->SelectAll("donation", "", array("donation_id" => $_GET["itemid"]), "array");
//if (isset($_SESSION["purchase_info"])) {
//    unset($_SESSION["purchase_info"]);
//}
?>
<?php include './header_script.php'; ?>
<body class="header-fixed">

    <div class="wrapper">
        <div class="header-v5 header-static">
            <?php include './menu_top.php'; ?>
            <?php include './header.php'; ?>
        </div>
        <div class="clearfix"></div>
        <div class="container content">
            <div class="col-md-12">

                <div class="servive-block rounded-2x" style="background-color: #A9CA4F !important;">            
                    <div class="row" style="text-align: center; padding-top: 10px;">
                        <i class="icon-2x color-light fa fa-warning" ></i>
                    </div>
                    <div class="clearfix"></div>
                    <h2 class="heading-md" style="color: white;margin-left: 10px;">Your have canceled your requested transaction for donation.Please contact with HOT LINE NO.(+8801971842538)</h2>
                    <br/>
                </div>

            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">


            </div>

            <div class="clearfix"></div>
            <div style="height: 10px;"></div>
            <div class="clearfix"></div>
            <div class="col-md-12 ">
                <a class="btn btn-success pull-right" href="customer_dashboard.php">Go To Dashboard</a>
            </div>
        </div>
        <?php include './newsletter.php'; ?>
        <?php include './footer.php'; ?>
    </div>

</body>