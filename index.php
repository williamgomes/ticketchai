<?php
include './admin/config/class.web.config.php';
$con = new Config();
$slider = $con->ReturnObjectByQuery("SELECT * FROM event WHERE featured='true' ORDER BY f_priority", "array");

$featured_event = $con->ReturnObjectByQuery("SELECT `event`.*, city.* FROM EVENT LEFT JOIN city ON `event`.city_id = city.city_id WHERE featured = 'true' ORDER BY f_priority", "array");
$upcomming_event = $con->ReturnObjectByQuery("SELECT * FROM `event` WHERE upcomming='true' ORDER BY u_priority", "array");
//$con->debug($featured_event);
$f_category = $con->ReturnObjectByQuery("SELECT `event`.event_id,( SELECT GROUP_CONCAT(category_name) FROM category AS ca INNER JOIN event_category AS ec ON ca.category_id = ec.category_id WHERE ec.event_id = `event`.event_id ) AS category_name FROM `event` LEFT JOIN event_category ON `event`.event_id = event_category.event_id LEFT JOIN category ON event_category.category_id = category.category_id LEFT JOIN event_ticket_type ON EVENT .event_id = event_ticket_type.event_id WHERE `event`.featured = 'true' GROUP BY `event`.event_id DESC", "array");
//$con->debug($f_category);
$u_category = $con->ReturnObjectByQuery("SELECT `event`.event_id,( SELECT GROUP_CONCAT(category_name) FROM category AS ca INNER JOIN event_category AS ec ON ca.category_id = ec.category_id WHERE ec.event_id = `event`.event_id ) AS ucategory_name FROM `event` LEFT JOIN event_category ON `event`.event_id = event_category.event_id LEFT JOIN category ON event_category.category_id = category.category_id LEFT JOIN event_ticket_type ON EVENT .event_id = event_ticket_type.event_id WHERE `event`.upcomming = 'true' GROUP BY `event`.event_id DESC", "array");
?>
<?php include './header_script.php'; ?>	
<body class="header-fixed">
    <div class="wrapper">
        <div class="header-v5 header-static">
            <?php include './menu_top.php'; ?>
            <?php include './header.php'; ?>
        </div>
        <!--=== SLIDER START HERE ===-->
        <div class="tp-banner-container">
            <div class="tp-banner">
                <ul>
                    <?php if (count($slider) >= 1): ?>
                        <?php foreach ($slider as $s): ?>
                            <li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="View Slider">
                                <img src="admin/uploads/event_full_size_image/<?php echo $s->event_full_size_image; ?>"  alt="darkblurbg"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                                <div class="tp-caption revolution-ch1 sft start"
                                     data-x="center"
                                     data-hoffset="0"
                                     data-y="100"
                                     data-speed="1500"
                                     data-start="500"
                                     data-easing="Back.easeInOut"
                                     data-endeasing="Power1.easeIn"                        
                                     data-endspeed="300">
                                    <strong style="font-size: 25px;"><?php echo $s->event_title; ?></strong>
                                </div>
                                <div class="tp-caption sft"
                                     data-x="center"
                                     data-hoffset="0"
                                     data-y="380"
                                     data-speed="1600"
                                     data-start="1800"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="300"
                                     data-endeasing="Power1.easeIn"
                                     data-captionhidden="off"
                                     style="z-index: 6">
                                    <a href="event_details.php?event_id=<?php echo base64_encode($s->event_id); ?>" class="btn-u btn-brd btn-brd-hover btn-u-light">View More</a>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
                <div class="tp-bannertimer tp-bottom"></div>            
            </div>
        </div>
        <!--=== SLIDER END HERE ===-->

        <!--=== MAIN CONTENT START HERE ===-->
        <div class="container content-md">
            <!--=== ADVERTISEMENT START HERE ===-->
<!--            <div class="heading heading-v1 margin-bottom-20">
                <h2><strong>Search Events</strong></h2>

            </div>
            <div class="row" style="margin-bottom: 120px;">
                <div class="col-md-6  md-margin-bottom-40" style="background-color: #F0F0F0; padding: 40px;">
                    <h2 class="title-type">By Name</h2>
                    <div class="billing-info-inputs checkbox-list">
                        <div class="row">
                            <div class="col-sm-12">
                                <input id="name" type="text" placeholder="First Name" name="firstname" class="form-control required">
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="col-md-6">
                    <h2 class="title-type">By Date</h2>
                    <div class="billing-info-inputs checkbox-list">
                        <div class="row">
                            <div class="col-sm-12">
                                <input style="width: 100% !important;" id="event_ticket_expair_date" name="event_ticket_expair_date" value="" />
                            </div>
                        </div>
                        
                    </div> 
                </div>
            </div>-->
            <!--=== ADVERTISEMENT END HERE ===-->

            <!--=== FEATURED EVENT START HERE ===-->
            <div class="heading heading-v1 margin-bottom-20">
                <h2><strong>Featured Events</strong></h2>

            </div>
            <div class="illustration-v2 margin-bottom-60">

                <ul class="list-inline owl-slider">
                    <?php if (count($featured_event) >= 1): ?>
                        <?php foreach ($featured_event as $fev): ?>
                            <li class="item">
                                <div class="product-img">
                                    <a id="<?php echo base64_encode($fev->event_id); ?>" href="event_details.php?event_id=<?php echo base64_encode($fev->event_id); ?>">
                                        <img class="full-width img-responsive" src="admin/uploads/event_logo_image/<?php echo $fev->event_logo_image; ?>" alt="">
                                    </a>
                                    <a class="product-review" href="event_details.php?event_id=<?php echo base64_encode($fev->event_id); ?>">Quick review</a>
                                    <a class="add-to-cart" href="event_details.php?event_id=<?php echo base64_encode($fev->event_id); ?>"><i class="fa fa-shopping-cart"></i>Buy Ticket</a>
                                </div>
                                <div class="product-description product-description-brd">
                                    <div class="overflow-h margin-bottom-5">
                                        <div class="pull-left">
                                            <h4 class="title-price"><a href="event_details.php?event_id=<?php echo base64_encode($fev->event_id); ?>"><?php echo $fev->event_title; ?></a></h4>
                                        </div>

                                        <div class="shop-rgba-dark-green rgba-banner">New</div>
                                        <span>Category&nbsp;:&nbsp;</span><small class="shop-rgba-dark-green"><?php echo $f_category{0}->category_name; ?></small>
                                    </div>   
                                    <div style="height: 35px;"></div>
                                    <ul class="list-inline product-ratings">
                                        <li><i class="rating-selected fa fa-star"></i></li>
                                        <li><i class="rating-selected fa fa-star"></i></li>
                                        <li><i class="rating-selected fa fa-star"></i></li>
                                        <li><i class="rating fa fa-star"></i></li>
                                        <li><i class="rating fa fa-star"></i></li>
                                        <li class="like-icon"><a onclick="javascript:addToWishlist(<?php echo $fev->event_id; ?>);" data-original-title="Add to wishlist" data-toggle="tooltip" data-placement="left" class="tooltips" href="javascript:void(0);"><i class="fa fa-heart"></i></a></li>

                                    </ul>    
                                </div>
                            </li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>

<!--                <div class="customNavigation margin-bottom-25">
                    <a class="owl-btn prev rounded-x"><i class="fa fa-angle-left"></i></a>
                    <a class="owl-btn next rounded-x"><i class="fa fa-angle-right"></i></a>
                </div>-->
            </div> 


            <!--=== FEATURED EVENT END HERE ===-->


<!--            === UPCOMING EVENT START HERE ===
            <div class="heading heading-v1 margin-bottom-40">
                <h2><strong>Upcoming Events</strong></h2>
            </div>
            <div class="row illustration-v2">
                <?php //if (count($upcomming_event) >= 1) : ?>
                    <?php// foreach ($upcomming_event as $up) : ?>
                        <div class="col-md-3 col-sm-6 md-margin-bottom-30">
                            <div class="product-img">
                                <a href="event_details.php?event_id=<?php// echo base64_encode($up->event_id); ?>">
                                    <img class="full-width img-responsive" src="admin/uploads/event_logo_image/<?php //echo $up->event_logo_image; ?>"  alt=""></a>
                                <a class="product-review" href="event_details.php?event_id=<?php //echo base64_encode($up->event_id); ?>">Quick review</a>
                                <a class="add-to-cart" href="event_details.php?event_id=<?php //echo base64_encode($up->event_id); ?>"><i class="fa fa-shopping-cart"></i>Buy Ticket</a>
                            </div> 
                            <div class="product-description product-description-brd">
                                <div class="overflow-h margin-bottom-5">
                                    <div class="pull-left">
                                        <h4 class="title-price"><a href="event_details.php?event_id=<?php //echo base64_encode($up->event_id); ?>" ><?php //echo $up->event_title; ?></a></h4>
                                    </div>
                                </div> 

                                <div style="height: 35px;"></div>
                                <ul class="list-inline product-ratings">
                                    <li><i class="rating-selected fa fa-star"></i></li>
                                    <li><i class="rating-selected fa fa-star"></i></li>
                                    <li><i class="rating-selected fa fa-star"></i></li>
                                    <li><i class="rating fa fa-star"></i></li>
                                    <li><i class="rating fa fa-star"></i></li>
                                    <li class="like-icon"><a onclick="javascript:addToWishlist(<?php //echo $up->event_id; ?>);" data-original-title="Add to wishlist" data-toggle="tooltip" data-placement="left" class="tooltips" href="javascript:void(0);"><i class="fa fa-heart"></i></a></li>
                                </ul>    
                            </div>
                        </div>
                    <?php //endforeach; ?>
                <?php //endif; ?>
            </div> 
            === UPCOMING EVENT END HERE ===-->
        </div>
        <!--=== MAIN CONTENT END HERE ===-->

        <!--=== Sponsors ===-->
        <div class="container content">
            <div class="heading heading-v1 margin-bottom-40">
                <h2>Our Sponsors</h2>

            </div>

            <ul class="list-inline owl-slider-v2">
                <li class="item first-child">
                    <img src="assets_new/img/clients/07.png" alt="">
                </li>
                <li class="item">
                    <img src="assets_new/img/clients/08.png" alt="">
                </li>
                <li class="item">
                    <img src="assets_new/img/clients/10.png" alt="">
                </li>
                <li class="item">
                    <img src="assets_new/img/clients/11.png" alt="">
                </li>
                <li class="item">
                    <img src="assets_new/img/clients/09.png" alt="">
                </li>
                <li class="item">
                    <img src="assets_new/img/clients/12.png" alt="">
                </li>
                <li class="item">
                    <img src="assets_new/img/clients/07.png" alt="">
                </li>
                <li class="item">
                    <img src="assets_new/img/clients/08.png" alt="">
                </li>
                <li class="item">
                    <img src="assets_new/img/clients/09.png" alt="">
                </li>
                <li class="item">
                    <img src="assets_new/img/clients/10.png" alt="">
                </li>
                <li class="item">
                    <img src="assets_new/img/clients/11.png" alt="">
                </li>
                <li class="item">
                    <img src="assets_new/img/clients/12.png" alt="">
                </li>
            </ul><!--/end owl-carousel-->
        </div>
        <!--=== End Sponsors ===-->

        <?php include './newsletter.php'; ?>
        <?php include './footer.php'; ?>
    </div><!--/wrapper-->

    <?php //include './footer_script.php'; ?>
<script>
$(document).ready(function () {
    $("#event_ticket_expair_date").kendoDatePicker();
});
</script>

</body>
</html> 