<?php

$sessionID = session_id();
$arrayWholeCartMiniCart = array();
$sqlWholeCart = "SELECT * "
 . "FROM temp_carts_events "
 . "LEFT JOIN event ON event.event_id=temp_carts_events.TC_product_id "
 . "LEFT JOIN event_schedule ON event_schedule.event_schedule_id=temp_carts_events.TC_schedule_id "
 . "LEFT JOIN event_venue ON event_venue.event_id=temp_carts_events.TC_product_id "
 . "LEFT JOIN venue ON venue.venue_id=event_venue.venue_id "
 . "WHERE temp_carts_events.TC_session_id='$sessionID' "
 . "AND event_venue.is_active='true' "
 . "ORDER BY `temp_carts_events`.`TC_updated` DESC";
$resultWholeCart = mysqli_query($con->open(), $sqlWholeCart);
if ($resultWholeCart) {
    while ($resultWholeCartObj = mysqli_fetch_array($resultWholeCart)) {
        
        $TempCartID = $resultWholeCartObj['TC_id'];
        $TmpCartEventID = $resultWholeCartObj['TC_product_id'];
        $TmpCartType = $resultWholeCartObj['TC_product_type'];
        $TmpCartScheduleID = $resultWholeCartObj['TC_schedule_id'];
        if($TmpCartType == "event"){
            $arrayWholeCartMiniCart[]['event']['event_details'] = $resultWholeCartObj;
        } elseif ($TmpCartType == "subscribe") {
            $arrayWholeCartMiniCart[]['subscribe']['subscribe_details'] = $resultWholeCartObj;
        }
        


        $sqlSelectCustAdd = "SELECT * FROM customer_addition WHERE CA_session_id='$sessionID'";
        $resultSelectCustAdd = mysqli_query($con->open(), $sqlSelectCustAdd);
        if($resultSelectCustAdd){
            $resultSelectCustAddObj = mysqli_fetch_array($resultSelectCustAdd);
            $programID = $resultSelectCustAddObj['CA_program_id'];
            $batchID = $resultSelectCustAddObj['CA_batch_id'];
        }

        //getting ticket type information
        $sqlGetTicktType = "SELECT * "
                . "FROM temp_cart_addition "
                . "LEFT JOIN batch ON batch.batch_id=$batchID "
                . "WHERE temp_cart_addition.TCA_session_id='$sessionID' "
                . "AND temp_cart_addition.TCA_item_id=$batchID "
                . "AND temp_cart_addition.TCA_TC_id=$TempCartID ";
        if($TmpCartType == "event"){
            $sqlGetTicktType .= "AND temp_cart_addition.TCA_item_type='type' ";
        } else {
            $sqlGetTicktType .= "AND temp_cart_addition.TCA_item_type='subscribe' ";
        }
        
        $sqlGetTicktType .=  "ORDER BY `temp_cart_addition`.`TCA_item_updated` DESC";
        
        $resultGetTicktType = mysqli_query($con->open(), $sqlGetTicktType);

        if ($resultGetTicktType) {
            while ($resultGetTicktTypeObj = mysqli_fetch_array($resultGetTicktType)) {
                if($TmpCartType == "event"){
                    $arrayWholeCartMiniCart[(count($arrayWholeCartMiniCart) - 1)]['event']['event_addition'][$resultGetTicktTypeObj['TCA_item_type']][] = $resultGetTicktTypeObj;
                } elseif ($TmpCartType == "subscribe") {
                    $arrayWholeCartMiniCart[(count($arrayWholeCartMiniCart) - 1)]['subscribe']['subscribe_addition'][$resultGetTicktTypeObj['TCA_item_type']][] = $resultGetTicktTypeObj;
                }
            }
        } else {
            echo "resultGetTicktType query failed." . mysqli_error($con->open());
        }  
        
    }
} else {
    echo "resultWholeCart query failed.";
}


?>

<!-- Mini Cart Start -->
<ul class="list-inline shop-badge badge-lists badge-icons pull-right">
    <li>
        <a href="javascript:void(0);"><i class="fa fa-shopping-cart"></i></a>
        <span class="badge badge-sea rounded-x cart-count"><?php echo count($arrayWholeCartMiniCart); ?></span>
        <ul id="scrollbar" class="list-unstyled badge-open contentHolder">
            <?php $totalValueCart = 0; ?>
            <?php if (count($arrayWholeCartMiniCart) > 0): ?>
                <?php foreach ($arrayWholeCartMiniCart AS $CartInfo): ?>
                    <?php if(isset($CartInfo['subscribe'])): ?>
            
                    <li id="cart-item-<?php echo $CartInfo['subscribe']['subscribe_details']['TC_id']; ?>">
                        <button type="button" class="close" onclick="javascript:deleteFromCart(<?php echo $CartInfo['subscribe']['subscribe_details']['TC_id']; ?>);">×</button>
                        <div class="overflow-h">
                            <span class="text-left" style="display: inline-block; width: 75% !important;">
                                <p style="color: black !important;"><strong>Lifetime Membership</strong> for DUDSAA</p>
                                
                            <?php $totalPrice = 0; ?>    
                            <?php if(isset($CartInfo['subscribe']['subscribe_addition']['subscribe']) AND count($CartInfo['subscribe']['subscribe_addition']['subscribe'] > 0)): ?>
                                <table style="font-size: x-small; color: black;"  width="100%">
                                    <tr style="border-bottom: 2px #dedede solid; font-weight: bold;">
                                        <td width="50%">Ticket Type</td>
                                        <td width="30%">Qty.</td>
                                        <td width="20%">Price</td>
                                    </tr>
                                <?php foreach ($CartInfo['subscribe']['subscribe_addition']['subscribe'] AS $CartAdditionTypeInfo): ?>
                                    <?php $totalPrice += $CartAdditionTypeInfo['TCA_item_total_price']; ?>
                                    <tr>
                                        <td width="50%">Lifetime Subscription</td>
                                        <!--<td width="30%"><?php // echo $CartAdditionTypeInfo['TCA_item_quantity']; ?></td>-->
                                        <td width="20%">TK. <?php echo $CartAdditionTypeInfo['TCA_item_total_price']; ?></td>
                                    </tr>
                                    
                                <?php endforeach; ?>    
                                </table>
                            <?php endif; ?>    
                                
                                <hr>
                                
                            <?php if(isset($CartInfo['subscribe']['event_addition']['include']) AND count($CartInfo['subscribe']['event_addition']['include'] > 0)): ?>    
<!--                                <table style="font-size: x-small; color: black;"  width="100%">
                                    <tr style="border-bottom: 2px #dedede solid; font-weight: bold;">
                                        <!--<td width="50%">Includes</td>-->
                                        <!--<td width="30%">Qty.</td>-->
                                        <!--<td width="20%">Price</td>
                                    </tr>-->
                                <?php foreach ($CartInfo['subscribe']['event_addition']['include'] AS $CartAdditionIncldInfo): ?>
                                    <?php $totalPrice += $CartAdditionIncldInfo['TCA_item_total_price']; ?>
                                    <!--<tr>-->
                                        <!--<td width="50%"><?php // echo $CartAdditionIncldInfo['event_includes_name']; ?></td>-->
                                        <!--<td width="30%"><?php // echo $CartAdditionIncldInfo['TCA_item_quantity']; ?></td>-->
                                        <!--<td width="20%">TK. <?php // echo $CartAdditionIncldInfo['TCA_item_total_price']; ?></td>-->
                                    <!--</tr>-->
                                <?php endforeach; ?>    
                                <!--</table>-->
                            <?php endif; ?>    
                            </span>

                            <span class="text-right" style="display: inline-block; float: right; width: 25% !important; color: black !important;">
                                <strong>TK. <?php echo number_format($totalPrice,2); ?> &nbsp;&nbsp;</strong>
                            </span>

                        </div>    
                    </li>
                    
                    <?php else: ?>
                    
                    
                    <li id="cart-item-<?php echo $CartInfo['event']['event_details']['TC_id']; ?>">
                        <button type="button" class="close" onclick="javascript:deleteFromCart(<?php echo $CartInfo['event']['event_details']['TC_id']; ?>);">×</button>
                        <div class="overflow-h">
                            <span class="text-left" style="display: inline-block; width: 75% !important;">
                                <p style="color: black !important;"><strong><?php echo $CartInfo['event']['event_details']['event_title']; ?></strong></p>
                                <span style="font-size: x-small; color: #79b92d;">
                                    Date: <strong><?php echo date("d M, Y" , strtotime($CartInfo['event']['event_details']['event_date'])); ?></strong><br/> 
                                    Venue: <strong><?php echo $CartInfo['event']['event_details']['venue_address']; ?></strong><br/>
                                    Time: <strong><?php echo $CartInfo['event']['event_details']['event_schedule_start_time']; ?> - <?php echo $CartInfo['event']['event_details']['event_schedule_end_time']; ?></strong><br/>
                                </span>
                            <?php $totalPrice = 0; ?>    
                            <?php if(isset($CartInfo['event']['event_addition']['type']) AND count($CartInfo['event']['event_addition']['type'] > 0)): ?>
                                <table style="font-size: x-small; color: black;"  width="100%">
                                    <tr style="border-bottom: 2px #dedede solid; font-weight: bold;">
                                        <td width="50%">Ticket Type</td>
                                        <td width="30%">Qty.</td>
                                        <td width="20%">Price</td>
                                    </tr>
                                <?php foreach ($CartInfo['event']['event_addition']['type'] AS $CartAdditionTypeInfo): ?>
                                    <?php $totalPrice += $CartAdditionTypeInfo['TCA_item_total_price']; ?>
                                    <tr>
                                        <td width="50%">Ticket Price</td>
                                        <td width="30%"><?php echo $CartAdditionTypeInfo['TCA_item_quantity']; ?></td>
                                        <td width="20%">TK. <?php echo $CartAdditionTypeInfo['ticket_price']; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50%">Annual Subscription</td>
                                        <td width="30%"><?php echo $CartAdditionTypeInfo['TCA_item_quantity']; ?></td>
                                        <td width="20%">TK. <?php echo $CartAdditionTypeInfo['anual_subscription']; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="50%">Enrollment Fee</td>
                                        <td width="30%"><?php echo $CartAdditionTypeInfo['TCA_item_quantity']; ?></td>
                                        <td width="20%">TK. <?php echo $CartAdditionTypeInfo['enrollment_fee']; ?></td>
                                    </tr>
                                <?php endforeach; ?>    
                                </table>
                            <?php endif; ?>    
                                
                                <hr>
                                
                            <?php if(isset($CartInfo['event_addition']['include']) AND count($CartInfo['event_addition']['include'] > 0)): ?>    
<!--                                <table style="font-size: x-small; color: black;"  width="100%">
                                    <tr style="border-bottom: 2px #dedede solid; font-weight: bold;">
                                        <!--<td width="50%">Includes</td>-->
                                        <!--<td width="30%">Qty.</td>-->
                                        <!--<td width="20%">Price</td>
                                    </tr>-->
                                <?php foreach ($CartInfo['event_addition']['include'] AS $CartAdditionIncldInfo): ?>
                                    <?php $totalPrice += $CartAdditionIncldInfo['TCA_item_total_price']; ?>
                                    <!--<tr>-->
                                        <!--<td width="50%"><?php // echo $CartAdditionIncldInfo['event_includes_name']; ?></td>-->
                                        <!--<td width="30%"><?php // echo $CartAdditionIncldInfo['TCA_item_quantity']; ?></td>-->
                                        <!--<td width="20%">TK. <?php // echo $CartAdditionIncldInfo['TCA_item_total_price']; ?></td>-->
                                    <!--</tr>-->
                                <?php endforeach; ?>    
                                <!--</table>-->
                            <?php endif; ?>    
                            </span>

                            <span class="text-right" style="display: inline-block; float: right; width: 25% !important; color: black !important;">
                                <strong>TK. <?php echo number_format($totalPrice,2); ?> &nbsp;&nbsp;</strong>
                            </span>

                        </div>    
                    </li>
                    
                    
                    <?php endif; ?>
                    
                    <?php $totalValueCart += $totalPrice; ?>
                <?php endforeach; ?>
            <?php endif; ?>
            <li class="subtotal">
                <div class="overflow-h margin-bottom-10">
                    <span>Subtotal</span>
                    <span class="pull-right subtotal-cost">TK. <?php echo number_format($totalValueCart,2); ?></span>
                </div>
                <div class="row">    
                    <div class="col-xs-6">
<!--                        <a href="cart.php" class="btn-u btn-brd btn-brd-hover btn-u-sea-shop btn-block">View Cart</a>-->
                    </div>
                    <div class="col-xs-6">
                        <a href="checkout.php" class="btn-u btn-u-sea-shop btn-block">Checkout</a>
                    </div>
                </div>        
            </li>    
        </ul>
    </li>
    <li class="" aria-disabled="false" style="padding: 0px !important;">
        <a href="checkout.php" style="padding: 6px; font-size: medium;" class="btn-u btn-u-sea-shop btn-u-lg">Checkout&nbsp;&nbsp;<i style="color: white;" class="fa  fa-check-circle-o"></i></a>
    </li>
<!--    <li class="" aria-disabled="false"  style="padding: 0px !important;">
        <a href="donate.php" style="padding: 6px; font-size: medium; height: 35px;" class="btn-u btn-u-sea-shop btn-u-lg">Donate&nbsp;&nbsp;<i style="color: white;" class="fa fa-money"></i></a>
    </li>-->
</ul>

<!-- Mini Cart End -->