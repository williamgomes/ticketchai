<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
    <head>
        <title>Ticket Chai | Buy Online Tickets....</title>

        <!-- Meta -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">


        <script>
            var baseUrl = '<?php echo $con->siteUrl(); ?>';
        </script>
        <!-- Favicon -->
        <link rel="shortcut icon" href="favicon_2.ico">

        <!-- CSS Global Compulsory -->
        <link rel="stylesheet" href="assets_new/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets_new/css/shop.style.css">
        <link rel="stylesheet" href="style.css">

        <!-- CSS Implementing Plugins -->
        <link rel="stylesheet" href="assets_new/plugins/line-icons/line-icons.css">
        <link rel="stylesheet" href="assets_new/plugins/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets_new/plugins/scrollbar/src/perfect-scrollbar.css">
        <link rel="stylesheet" href="assets_new/plugins/owl-carousel/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="assets_new/plugins/revolution-slider/rs-plugin/css/settings.css">
        <link rel="stylesheet" href="assets_new/plugins/sky-forms/version-2.0.1/css/custom-sky-forms.css">
        <link rel="stylesheet" href="assets_new/plugins/master-slider/quick-start/masterslider/style/masterslider.css">
        <link rel='stylesheet' href="assets_new/plugins/master-slider/quick-start/masterslider/skins/default/style.css">
        <!-- CSS Theme -->
        <link rel="stylesheet" href="assets_new/css/theme-colors/default.css">

        <!-- CSS Customization -->
        <link rel="stylesheet" href="assets_new/css/custom.css">

        <!-- CSS Page Style -->
        <link rel="stylesheet" href="assets_new/css/pages/log-reg-v3.css">

        <!-- Kendo UI Style Linked Here -->
        <link rel="stylesheet" href="assets_new/kendo/css/kendo.common.min.css">
        <link rel="stylesheet" href="assets_new/kendo/css/kendo.metro.min.css">


        <!-- Cart Page Style -->
        <link rel="stylesheet" href="assets_new/plugins/jquery-steps/css/custom-jquery.steps.css">



        <!-- Google Map Script -->
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>

        <!-- All Scripts Start Here -->

        <!-- JS Global Compulsory -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="assets_new/plugins/jquery/jquery-migrate.min.js"></script>
        <script src="assets_new/plugins/bootstrap/js/bootstrap.min.js"></script>
        <!-- JS Implementing Plugins -->
        <script src="assets_new/plugins/back-to-top.js"></script>
        <script src="assets_new/plugins/sky-forms/version-2.0.1/js/jquery.validate.min.js"></script>
        <script src="assets_new/plugins/jquery-steps/build/jquery.steps.js"></script>
        <script src="assets_new/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
        <script src="assets_new/plugins/scrollbar/src/jquery.mousewheel.js"></script>
        <script src="assets_new/plugins/scrollbar/src/perfect-scrollbar.js"></script>
        <script src="assets_new/plugins/jquery.parallax.js"></script>
        <script src="assets_new/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
        <script src="assets_new/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
        <!-- Master Slider -->
        <script src="assets_new/plugins/master-slider/quick-start/masterslider/masterslider.min.js"></script>
        <script src="assets_new/plugins/master-slider/quick-start/masterslider/jquery.easing.min.js"></script>
        <!-- Scrollbar -->
        <script src="assets_new/plugins/scrollbar/src/jquery.mousewheel.js"></script>
        <script src="assets_new/plugins/scrollbar/src/perfect-scrollbar.js"></script>
        <script src="assets_new/js/plugins/master-slider.js"></script>
        <script src="assets_new/js/forms/product-quantity.js"></script>
        <!-- JS Customization -->
        <script src="assets_new/js/custom.js"></script>
        <!-- JS Page Level -->
        <script src="assets_new/js/shop.app.js"></script>
        <script src="assets_new/js/plugins/owl-carousel.js"></script>
        <script src="assets_new/js/plugins/revolution-slider.js"></script>
        <script src="assets_new/js/shop.app.js"></script>
        <script src="assets_new/js/forms/page_login.js"></script>
        <script src="assets_new/js/plugins/stepWizard.js"></script>
        <script src="assets_new/js/forms/product-quantity.js"></script>
        <script src="assets_new/js/forms/page_contact_form.js"></script>

        <script type="text/javascript" src="assets_new/kendo/js/kendo.web.min.js"></script>

        <script src="http://www.datejs.com/build/date.js" type="text/javascript"></script>



        <script type="text/javascript" src="lib.js/custom_javascript.js"></script>
        <script src="assets_new/js/forms/page_registration.js"></script>
        <script>
            jQuery(document).ready(function () {
                App.init();
                Registration.initRegistration();
            });
        </script>

        <script>
            jQuery(document).ready(function () {
                App.init();
                Login.initLogin();
                StepWizard.initStepWizard();
            });
        </script>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                App.init();
                App.initParallaxBg();
                OwlCarousel.initOwlCarousel();
                RevolutionSlider.initRSfullWidth();
            });</script>
        <script>
            jQuery(document).ready(function () {
                App.init();
                OwlCarousel.initOwlCarousel();
                MasterSliderShowcase2.initMasterSliderShowcase2();
            });</script>

        <!-- Script For Login Form -->
        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery("#login_form").click(function () {
                    jQuery("#login_window").kendoWindow({width: "505px",
                        height: "auto",
                        visible: false,
                        modal: true,
                        title: "Login To Your Account"
                    }).data("kendoWindow").center().open();
                });
            });</script>

        <!-- Script For Registration -->
        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery("#register_form").click(function () {
                    jQuery("#register_window").kendoWindow({width: "530px",
                        height: "auto",
                        visible: false,
                        modal: true,
                        title: "Create New Account"
                    }).data("kendoWindow").center().open();
                });
            });
        </script>




        <style>
            #success-msg{
                height: auto; 
                width: 100%; 
                z-index: 999999999; 
                position: absolute; 
                background-color: #6CDA6C;
                -webkit-box-shadow: 1px 13px 10px -1px rgba(0,0,0,0.68);
                -moz-box-shadow: 1px 13px 10px -1px rgba(0,0,0,0.68);
                display: none;
            }

            #error-msg{
                height: auto; 
                width: 100%; 
                z-index: 999999999; 
                position: absolute; 
                background-color: #FF3030;
                -webkit-box-shadow: 1px 13px 10px -1px rgba(0,0,0,0.68);
                -moz-box-shadow: 1px 13px 10px -1px rgba(0,0,0,0.68);
                display: none;
            }

            #warning-msg{
                height: auto; 
                width: 100%; 
                z-index: 99999999; 
                position: absolute; 
                background-color: #D8C183;
                -webkit-box-shadow: 1px 13px 10px -1px rgba(0,0,0,0.68);
                -moz-box-shadow: 1px 13px 10px -1px rgba(0,0,0,0.68);
                display: none;
            }

        </style>


        

    </head>