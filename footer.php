<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=784075561649470&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!--=== Footer Version 2 ===-->
<div style="height: 10px;"></div>
<div class="footer-v3">
    <div class="footer">
        <div class="container">
            <div class="row">
                <!-- About -->
                <!--                <div class="col-md-3 md-margin-bottom-40">
                                    <a href="index.php"><img class="footer-logo" height="40" width="100" style="margin-top:-3px;" src="assets/img/ticketchai_logo.png" alt=""></a>
                                    <div style="height: 15px;"></div>
                                    <ul class="list-unstyled link-list">
                                        <li><i class="fa fa-chevron-circle-right"></i>&nbsp;&nbsp;<a href="about_us.php">Hot Line Number: (+8801971842538) </a></li>
                                        <li><i class="fa fa-chevron-circle-right"></i>&nbsp;&nbsp;<a href="terms_of_service.php">Terms of service</a></li>
                                        <li><i class="fa fa-chevron-circle-right"></i>&nbsp;&nbsp;<a href="index.php">Latest Events</a></i></li>
                                        <li><i class="fa fa-chevron-circle-right"></i>&nbsp;&nbsp;<a href="privacy_policy.php">Privacy Policy</a></li>
                                        <li><i class="fa fa-chevron-circle-right"></i>&nbsp;&nbsp;<a href="contact_us.php">Contact us</a></li>
                                    </ul>
                                    <form class="footer-subsribe">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Email Address">                            
                                            <span class="input-group-btn">
                                                <button class="btn-u" type="button">Go</button>
                                            </span>
                                        </div>                  
                                    </form>                         
                                </div>/col-md-3-->
                <!-- End About -->
                <div class="col-md-3 md-margin-bottom-40">
                    <div class="headline"><h2>Stay In Touch</h2></div>
                    <a href="index.php"><img class="footer-logo" height="40" width="100" style="margin-top:-3px;" src="assets/img/ticketchai_logo.png" alt=""></a>
                    <div style="height: 10px;"></div>
                    <ul class="list-unstyled link-list">
                        <li><i class="fa fa-chevron-circle-right"></i>&nbsp;&nbsp;<a href="javascript:void(0);">HOT LINE: <b><span style="color:#EB3D03">(+8801971842538)</span></b> </a></li>
                        <li><i class="fa fa-chevron-circle-right"></i>&nbsp;&nbsp;<a href="javascript:void(0);">OFFICE HOURS: 10 AM TO 06 PM</a></li>
                        <li><i class="fa fa-chevron-circle-right"></i>&nbsp;&nbsp;<a href="javascript:void(0);">OFFICE DAY: SATURDAY TO THURSDAY</a></li>

                    </ul>
                </div>

                <!-- Link List -->
                <div class="col-md-3 md-margin-bottom-40">
                    <div class="headline"><h2>Useful Links</h2></div>
                    <ul class="list-unstyled link-list">
                        <li><i class="fa fa-chevron-circle-right"></i>&nbsp;&nbsp;<a href="about_us.php">About us</a></li>
                        <li><i class="fa fa-chevron-circle-right"></i>&nbsp;&nbsp;<a href="terms_of_service.php">Terms of service</a></li>
                        <li><i class="fa fa-chevron-circle-right"></i>&nbsp;&nbsp;<a href="index.php">Latest Events</a></i></li>
                        <li><i class="fa fa-chevron-circle-right"></i>&nbsp;&nbsp;<a href="privacy_policy.php">Privacy Policy</a></li>
                        <li><i class="fa fa-chevron-circle-right"></i>&nbsp;&nbsp;<a href="contact_us.php">Contact us</a></li>
                    </ul>
                </div><!--/col-md-3-->
                <!-- End Link List -->                   

                <!-- Latest Tweets -->
                <div class="col-md-3 md-margin-bottom-40">
                    <div class="latest-tweets">
                        <div class="headline"><h2 class="heading-sm">Social Feeds</h2></div>
                        <div class="latest-tweets-inner">
                            <div class="fb-like-box" data-href="https://www.facebook.com/ticketchaibd" data-width="200px" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="false"></div>
                        </div>

                    </div>
                </div>
                <!-- End Latest Tweets -->    

                <!-- Address -->
                <div class="col-md-3 md-margin-bottom-40">
                    <div class="headline"><h2 class="heading-sm">Contact Us</h2></div>                         
                    <address class="md-margin-bottom-40">
                        <i class="fa fa-home"></i>&nbsp;&nbsp;Razzak Plaza (8th Floor),1 New <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Eskaton Road,Moghbazar <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Circle,Dhaka-1217 <br />
                        <i class="fa fa-phone"></i>&nbsp;&nbsp;Phone: +8801971842538, &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+8804478009569 <br />
                        <i class="fa fa-globe"></i>&nbsp;&nbsp;Website: <a href="http://www.ticketchai.com">www.ticketchai.com</a> <br />
                        <i class="fa fa-envelope"></i>&nbsp;&nbsp;Email: <a href="mailto:support@ticketchai.com">support@ticketchai.com</a> 
                    </address>

                    <!-- Social Links -->
                    <!--                    <ul class="social-icons">
                                            <li><a href="https://www.facebook.com/ticketchaibd" data-original-title="Facebook" class="fa fa-facebook-square"></a></li>
                                            <li><a href="https://www.twitter.com/ticketchaibd" data-original-title="Twitter" class="rounded-x social_twitter"></a></li>
                                            <li><a href="https://plus.google.com/ticketchaibd" data-original-title="Goole Plus" class="rounded-x social_googleplus"></a></li>
                                            <li><a href="https://www.linkedin.com/ticketchaibd" data-original-title="Linkedin" class="rounded-x social_linkedin"></a></li>
                                        </ul>-->
                    <!--                     <ul class="list-inline shop-social">
                                            <li><a href="#"><i class="fb rounded-md fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="tw rounded-md fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="gp rounded-md fa fa-google-plus"></i></a></li>
                                            <li><a href="#"><i class="yt rounded-md fa fa-youtube"></i></a></li>
                                        </ul>-->

                    <ul class="list-inline team-social">
                        <li>
                            <a target="_blank" data-placement="top" data-toggle="tooltip" class="fb tooltips" data-original-title="Facebook" href="https://www.facebook.com/ticketchaibd">
                                <img src="assets_new/img/facebook.png"  alt="">
                            </a>
                        </li>
                        <li>
                            <a target="_blank" data-placement="top" data-toggle="tooltip" class="tw tooltips" data-original-title="Twitter" href="https://www.twitter.com/ticketchaibd">
                                <img src="assets_new/img/twitter.png"  alt="">
                            </a>
                        </li>
                        <li>
                            <a target="_blank" data-placement="top" data-toggle="tooltip" class="gp tooltips" data-original-title="Google plus" href="https://plus.google.com/ticketchaibd">
                                <img src="assets_new/img/google.png"  alt="">
                            </a>
                        </li>
                        <li>
                            <a target="_blank" data-placement="top" data-toggle="tooltip" class="gp tooltips" data-original-title="Linkedin" href="https://www.linkedin.com/ticketchaibd">
                                <img src="assets_new/img/linkedin.png"  alt="">
                            </a>
                        </li>
                    </ul>
                    <!-- End Social Links -->
                </div><!--/col-md-3-->
                <!-- End Address -->
            </div>
        </div> 
    </div><!--/footer-->

    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">                     
                    <p style="margin-top: 16px;">
                        2014 &copy; ticketchai.com . ALL Rights Reserved. 
                        <a href="privacy_policy.php">Privacy Policy</a> | <a href="terms_of_service.php">Terms of Service</a>
                    </p>
                </div>
                <div class="col-md-6">  
                    <ul class="list-inline sponsors-icons pull-right">

                        <img src="assets_new/img/paywith.png" style="width: 80%;margin-top: 14px;margin-left: 90px;"  alt="">
<!--                        <img src="assets_new/img/creditcard_mastercard.png"  alt="">
                        <img src="assets_new/img/visa_curved.png"  alt="">-->


                    </ul>
                </div>
            </div>
        </div> 
    </div>
</div>   
<!--=== End Footer Version 2 ===-->