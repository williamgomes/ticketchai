<?php
include '../admin/config/class.web.config.php';
$con = new Config();
extract($_POST);

if(isset($delMethod)){
    //$temp_tocken = "DUDSAA"."-".$customerID."-".$."-".$customerID
    if($delMethod=="cash"){
      
        $insert_array =array(
            "event_id"=>$eventID,
            "schedule_id"=>$scheduleID,
            "shipping_id"=>$shippingID,
            "billing_id"=>$billingID,
            "payment_status"=>"unpaid",
            "customer_id"=>$customerID,
            "ticket_quantity"=>"1",
            "ticket_price"=>$ticket_unit_price,
            "enrollment_fee"=>$enrollment_fee,
            "anual_subsciption_cost"=>$anual_subscription,
            "total_amount"=>$subTotalPrice,
            "delivery_method"=>"home_delivery",
            "delivery_address"=>$shippingAddress,
            "delivery_post_code"=>$shippingZip,
            "delivery_status"=>"undelivered",
            "delivery_city"=>$shippingCity,
            "delivery_first_name"=>$shippingFirstName,
            "delivery_last_name"=>$shippingLastName,
            "delivery_city"=>$shippingCity,
            "booking_time"=> date('Y-m-d H:i:s'),
            "delivery_phone_number"=>$shippingPhoneNo,
            "delivery_cost"=>$delivery_cost_for_ticket,
            "online_pay_method"=>$delivery_method,
            "life_time_subsciption_cost"=>$lifetimeSubscription
          
        );
        
         $insert_book = $con->InsertClear("booking",$insert_array , "", "", "array");
         //$con->debug($insert_book);
         $token = mysqli_real_escape_string($con->open(),"DUDSAA-" . $insert_book["id"] . "-" . $eventID . "-" ."-".$customerID. rand(1, 99));
         
         $update_tocken_array = array("booking_id" => $insert_book["id"], "tocken" => $token);

         $update_tocken = $con->Update("booking", $update_tocken_array, "", "", "array");
         //$con->debug($update_tocken);
         
         $booking_object = $insert_book["object"];
         $return_array = array("output" => "success", "delivery_method"=>$booking_object{0}->delivery_method, "booking_id" => $booking_object{0}->booking_id, "total_amount" => $booking_object{0}->total_amount,"customer_id"=>$booking_object{0}->customer_id);
         echo json_encode($return_array);
        
    }else if($delMethod =="card"){
    
        $total_payment_amount_ssl = round((floatval($subTotalPrice) + (floatval($subTotalPrice) * 4) / 100),2);
         $insert_array =array(
            "event_id"=>$eventID,
            "schedule_id"=>$scheduleID,
            "shipping_id"=>$shippingID,
            "billing_id"=>$billingID,
            "payment_status"=>"unpaid",
            "customer_id"=>$customerID,
            "ticket_quantity"=>"1",
            "ticket_price"=>$ticket_unit_price,
            "enrollment_fee"=>$enrollment_fee,
            "anual_subsciption_cost"=>$anual_subscription,
            "total_amount"=>$total_payment_amount_ssl,
            "delivery_method"=>"online_payment",
            "delivery_address"=>$shippingAddress,
            "delivery_post_code"=>$shippingZip,
            "delivery_status"=>"undelivered",
            "delivery_city"=>$shippingCity,
            "delivery_first_name"=>$shippingFirstName,
            "delivery_last_name"=>$shippingLastName,
            "delivery_city"=>$shippingCity,
            "booking_time"=> date('Y-m-d H:i:s'),
            "delivery_phone_number"=>$shippingPhoneNo,
            "delivery_cost"=>$delivery_cost_for_ticket,
            "online_payment_status"=>"false",
            "online_pay_method"=>$delivery_method,
            "life_time_subsciption_cost"=>$lifetimeSubscription
        
        );
         $insert_book = $con->InsertClear("booking",$insert_array , "", "", "array");
         //$con->debug($insert_book);
         
         $token = mysqli_real_escape_string($con->open(),"DUDSAA-" . $insert_book["id"] . "-" . $eventID . "-" ."-".$customerID. rand(1, 99));
         
         $update_tocken_array = array("booking_id" => $insert_book["id"], "tocken" => $token);

         $update_tocken = $con->Update("booking", $update_tocken_array, "", "", "array");
         //$con->debug($update_tocken);
     
         $booking_object = $insert_book["object"];
         $return_array = array("output" => "success", "delivery_method"=>$booking_object{0}->delivery_method, "booking_id" => $booking_object{0}->booking_id, "total_amount" => $booking_object{0}->total_amount,"customer_id"=>$booking_object{0}->customer_id);
         echo json_encode($return_array);
    }else{
         $return_array = array("output" => "error", "msg"=>"Booking Payment method is not selected");
         echo json_encode($return_array);
    }
}
