<?php

include '../admin/config/class.web.config.php';
$con = new Config();

$event_id = "";
$user_id = "";
$session_id = session_id();
//$con->debug($session_id);
$ratings = "";
$review = "";
$status = "";

extract($_POST);


if ($con->checkUserLogin()) {
    $checkReviewSessionId = $con->CheckExistsWithCondition("user_review", "session_id='$session_id' AND user_id='$user_id'");

    if ($ratings = '') {
        $ratings = 0;
    }

    if ($checkReviewSessionId == 0) {
        $review_array = '';
        $review_array .=' event_id = "' . intval($event_id) . '"';
        $review_array .=', user_id ="' . intval($user_id) . '"';
        $review_array .=', ratings ="' . intval($ratings) . '"';
        $review_array .=', review ="' . mysqli_real_escape_string($con->open(), $review) . '"';
        $review_array .=', session_id ="' . mysqli_real_escape_string($con->open(), $session_id) . '"';
        $review_array .=', status ="' . mysqli_real_escape_string($con->open(), 'draft') . '"';

        $insert_review = "INSERT INTO user_review SET $review_array";
        $result_insert_review = mysqli_query($con->open(), $insert_review);
        //$con->debug($result_insert_review);

        if ($result_insert_review) {
            $return_array = array("output" => "success", "msg" => "Your review saved successfully");
            echo json_encode($return_array);
            exit();
        } else {
            $return_array = array("output" => "error", "msg" => "insert_review_array failed.");
            echo json_encode($return_array);
            exit();
        }
    } else {
        $return_array = array("output" => "warning", "msg" => "Your review already saved");
        echo json_encode($return_array);
        exit();
    }
} else {
    $return_array = array("output" => "error", "msg" => "login");
    echo json_encode($return_array);
    exit();
}
?>
