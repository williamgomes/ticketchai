<?php
include "../admin/config/class.web.config.php";
$con = new Config();

$id = 0;
$delCost = 0;

extract($_POST);

if($id > 0){
    
    $arrayState = array();
    $sqlState = "SELECT * FROM delivery_cost WHERE dis_state_id=$id";
    $resultState = mysqli_query($con->open(), $sqlState);
    if ($resultState) {
        $resultStateObj = mysqli_fetch_array($resultState);
        if(isset($resultStateObj['dis_state_id'])){
            $delCost = $resultStateObj['cost'];
            $return_array = array("output" => "success", "delCost" => $delCost);
            echo json_encode($return_array);
            exit();
        }
    } else {
        $return_array = array("output" => "error", "msg" => "resultState query failed.", "delCost"=> 0);
        echo json_encode($return_array);
        exit();
    }
}


?>