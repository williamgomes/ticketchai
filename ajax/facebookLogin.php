<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: '784075561649470',
            oauth: true,
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true // parse XFBML
        });

    };

    function fb_login() {
        FB.login(function (response) {

            if (response.authResponse) {

                FB.api('/me', function (response) {

                    var data = new Object();
                    data.first_name = response.first_name;
                    data.last_name = response.last_name;
                    data.email = response.email;
                    data.social_id = response.id;
                    data.gender = response.gender;
                    data.social_type = 'facebook';
                    data.is_social = 'yes';

                    var url = 'ajax/save_social_login_data.php';
                    $.ajax({
                        type: 'post',
                        url: url,
                        data: data,
                        dataType: "json",
                        success: function (response) {
                            var obj = response;
                            // dropDownBox(obj.output, obj.msg);
                            if (obj.output === "success") {
                                $('.k-window').fadeOut();
                                $('.k-overlay').fadeOut();
                                $('#register_form').replaceWith('<a href="javascript:void();" onclick="javascript:userLogout();">Logout</a>');
                                $('#login_form').replaceWith('<a href="customer_dashboard.php">' + obj.first_name + '</a>');
                                dropDownBox(obj.output, obj.msg);
                            } else {
                                dropDownBox(obj.output, obj.msg);
                            }

                        },
                        error: function (output) {
                            alert("Process Working Stopped");
                        }

                    });

                });
                access_token = response.authResponse.accessToken; //get access token
                user_id = response.authResponse.userID; //get FB UID

            } else {
                //user hit cancel button
                console.log('User cancelled login or did not fully authorize.');

            }
        }, {
            scope: 'publish_stream,email'
        });
    }
    (function () {
        var e = document.createElement('script');
        e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
        e.async = true;
        document.getElementById('fb-root').appendChild(e);
    }());
</script>