<?php

include "../admin/config/class.web.config.php";
$con = new Config();

$id = 0;
$arrBatch = array();

extract($_POST);

if($id > 0){
    $sqlGetBatch = "SELECT * FROM batch WHERE program_id=$id";
    $resultGetBatch = mysqli_query($con->open(), $sqlGetBatch);
    if($resultGetBatch){
        while($resultGetBatchArr = mysqli_fetch_array($resultGetBatch)){
            $arrBatch[] = $resultGetBatchArr;
        }
        
        $return_array = array("output" => "success", "batchObj" => $arrBatch);
        echo json_encode($return_array);
        exit();
    } else {
        $return_array = array("output" => "error", "msg" => "resultGetBatch query failed.");
        echo json_encode($return_array);
        exit();
    }
}


?>