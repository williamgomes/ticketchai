<?php

include '../admin/config/class.web.config.php';
$con = new Config();
extract($_POST);
//echo $billing_address_id;
if ($shipping_address_id == "") {
    $shipping_address_array = array(
        "shipping_address" => $shipping_address,
        "shipping_city" => $shipping_city,
        "shipping_country" => $shipping_country,
        "shipping_post_code" => $shipping_post_code,
        "customer_id" => $customer_id
    );

    $insert_shipping_address_array = $con->Insert("shipping_address", $shipping_address_array, "", "", "array");
    if ($insert_shipping_address_array["output"] == "success") {

        $return_array = array("output" => "success", "msg" => "Shipping address saved successfully");
        echo json_encode($return_array);
        exit();
    } else {
        $return_array = array("output" => "error", "msg" => "Shipping address not saved");
        echo json_encode($return_array);
        exit();
    }
} else{

    if ($shipping_address_id != "" && $shipping_address != "" && $shipping_post_code != "" && $shipping_city != "" && $shipping_country != "") {
        $shipping_address_update = $con->Update("shipping_address", $_POST, "", "shipping_address_id=$shipping_address_id", "array");
        if ($shipping_address_update["output"] == "success") {

            $return_array = array("output" => "success", "msg" => "Shipping address changed successfully");
            echo json_encode($return_array);
            exit();
        } else {
            $return_array = array("output" => "error", "msg" => "Billing address aot updated");
            echo json_encode($return_array);
            exit();
        }
    } else {

        $return_array = array("output" => "error", "msg" => "Billing address not updated");
        echo json_encode($return_array);
        exit();
    }
} 
?>
