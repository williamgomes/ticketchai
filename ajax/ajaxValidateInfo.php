<?php

include "../admin/config/class.web.config.php";
$con = new Config();


$userID = 0;

if ($con->checkUserLogin() == TRUE) {
    $userID = $_SESSION['customer_id'];
}
//echo $userID;
$CA_program_id = 0;
$CA_batch_id = 0;
$CA_profession = '';
$CA_designation = 'NULL';
$CA_organization = '';
$CA_blood_grp = '';
$CA_Photo = '';
$arrTickt = array();
$sessionID = session_id();


extract($_POST);

if ($CA_program_id > 0 AND $CA_batch_id > 0 AND $CA_profession != "" AND $CA_organization != "" AND $CA_blood_grp != "") {

    if ($CA_Photo == "") {
        $CA_Photo= "no_image.jpg";
    }
    $sqlCheckInfo = "SELECT * FROM customer_addition WHERE CA_session_id='$sessionID' AND CA_program_id=$CA_program_id AND CA_batch_id=$CA_batch_id";
    $resultCheckInfo = mysqli_query($con->open(), $sqlCheckInfo);
    if ($resultCheckInfo) {
        $resultCount = mysqli_num_rows($resultCheckInfo);

        if ($resultCount > 0) {
            $return_array = array("output" => "warning", "msg" => "Your information saved already.");
            echo json_encode($return_array);
            exit();
        } else {

            $insertCustInfo = '';
            $insertCustInfo .='  CA_session_id = "' . mysqli_real_escape_string($con->open(), $sessionID) . '"';
            $insertCustInfo .=', CA_customer_id ="' . intval($userID) . '"';
            $insertCustInfo .=', CA_program_id ="' . intval($CA_program_id) . '"';
            $insertCustInfo .=', CA_batch_id ="' . intval($CA_batch_id) . '"';
            $insertCustInfo .=', CA_profession ="' . mysqli_real_escape_string($con->open(), $CA_profession) . '"';
            $insertCustInfo .=', CA_designation ="' . mysqli_real_escape_string($con->open(), $CA_organization) . '"';
            $insertCustInfo .=', CA_organization ="' . mysqli_real_escape_string($con->open(), $CA_organization) . '"';
            $insertCustInfo .=', CA_Photo ="' . mysqli_real_escape_string($con->open(), $CA_Photo) . '"';
            $insertCustInfo .=', CA_blood_grp ="' . mysqli_real_escape_string($con->open(), $CA_blood_grp) . '"';

            $sqlInsert = "INSERT INTO customer_addition SET $insertCustInfo";
            $resultInsert = mysqli_query($con->open(), $sqlInsert);
            if ($resultInsert) {
                $sqlGetTicktInfo = mysqli_query($con->open(), "SELECT * FROM batch WHERE batch_id=$CA_batch_id");
                if ($sqlGetTicktInfo) {
                    $resultGetTicktInfo = mysqli_fetch_array($sqlGetTicktInfo);
                    if (isset($resultGetTicktInfo['batch_id'])) {
                        $arrTickt['batch_id'] = $resultGetTicktInfo['batch_id'];
                        $arrTickt['batch_name'] = $resultGetTicktInfo['batch_name'];
                        $arrTickt['ticket_price'] = $resultGetTicktInfo['ticket_price'];
                        $arrTickt['anual_subscription'] = $resultGetTicktInfo['anual_subscription'];
                        $arrTickt['enrollment_fee'] = $resultGetTicktInfo['enrollment_fee'];
                        $arrTickt['is_active'] = $resultGetTicktInfo['is_active'];
                    }
                    $_SESSION['is_verified'] = true;
                    $return_array = array("output" => "success", "msg" => "Your information saved successfully.", "objectTickt" => $arrTickt);
                    echo json_encode($return_array);
                    exit();
                } else {
                    $return_array = array("output" => "error", "msg" => "sqlGetTicktInfo query failed.");
                    echo json_encode($return_array);
                    exit();
                }
            } else {
                echo mysqli_error($con->open());
                $return_array = array("output" => "error", "msg" => "resultInsert query failed.");
                echo json_encode($return_array);
                exit();
            }
        }
    } else {
        $return_array = array("output" => "error", "msg" => "sqlCheckInfo query failed.");
        echo json_encode($return_array);
        exit();
    }
}