<?php

include "../admin/config/class.web.config.php";
$con = new Config();
$WL_id = '';
extract($_POST);

if ($con->checkUserLogin()) {
    if (isset($_POST['WL_id'])) {
        $WL_id = $_POST['WL_id'];
        $object_array = array("WL_id" => $WL_id);

        $delete_wishlist = $con->Delete("wishlists", $object_array);

        if ($delete_wishlist["output"] == "success") {
            $return_array = array("output" => "success", "msg" => "Wishlist deleted successfully.");
            echo json_encode($return_array);
            exit();
        } else {
            $return_array = array("output" => "error", "msg" => "Error wishlist delete");
            echo json_encode($return_array);
            exit();
        }
    } else {
        $return_array = array("output" => "error", "msg" => "Error wishlist delete");
        echo json_encode($return_array);
        exit();
    }
} else {
    $return_array = array("output" => "error", "msg" => "You have to login first");
    echo json_encode($return_array);
    exit();
}
?>
