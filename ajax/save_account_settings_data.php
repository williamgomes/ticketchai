<?php

include '../admin/config/class.web.config.php';
$con = new Config();
extract($_POST);

if ($customer_id != "" && $first_name != "" && $last_name != "") {
    $update_customer = $con->Update("customer", $_POST, "", "", "array");
    if ($update_customer["output"] == "success") {

        $return_array = array("output" => "success", "msg" => "Account Changed Successfully");
        echo json_encode($return_array);
        exit();
    } else {
        $return_array = array("output" => "error", "msg" => "Your Account Not Changed");
        echo json_encode($return_array);
        exit();
    }
} else {
    $return_array = array("output" => "error", "msg" => "Your Account Not Changed");
    echo json_encode($return_array);
    exit();
}
?>