<?php

include '../admin/config/class.web.config.php';
$con = new Config();
$sessionID = session_id();
$TempCartID = 0;
extract($_POST);

if ($TempCartID > 0) {
    $sqlDeleteTempCartEvent = "DELETE FROM temp_carts_events WHERE TC_id=$TempCartID AND TC_session_id='$sessionID'";
    $resultDeleteTempCartEvent = mysqli_query($con->open(), $sqlDeleteTempCartEvent);
    if ($resultDeleteTempCartEvent) {
        $sqlDeleteTempCartAdd = "DELETE FROM temp_cart_addition WHERE TCA_TC_id=$TempCartID AND TCA_session_id='$sessionID'";
        $resultDeleteTempCartAdd = mysqli_query($con->open(), $sqlDeleteTempCartAdd);


        /*         * ************************ Generate Whole Cart **************************************** */
        $sessionID = session_id();
        $arrayWholeCart = array();
        $sqlWholeCart = "SELECT * "
                . "FROM temp_carts_events "
                . "LEFT JOIN event ON event.event_id=temp_carts_events.TC_product_id "
                . "LEFT JOIN event_schedule ON event_schedule.event_schedule_id=temp_carts_events.TC_schedule_id "
                . "LEFT JOIN event_venue ON event_venue.event_id=temp_carts_events.TC_product_id "
                . "LEFT JOIN venue ON venue.venue_id=event_venue.venue_id "
                . "WHERE temp_carts_events.TC_session_id='$sessionID' "
                . "AND event_venue.is_active='true' "
                . "ORDER BY `temp_carts_events`.`TC_updated` DESC";
        $resultWholeCart = mysqli_query($con->open(), $sqlWholeCart);
        if ($resultWholeCart) {
            while ($resultWholeCartObj = mysqli_fetch_array($resultWholeCart)) {
                $arrayWholeCart[]['event_details'] = $resultWholeCartObj;
                $TempCartEventID = $resultWholeCartObj['TC_id'];

                //getting ticket type information
                $sqlGetTicktType = "SELECT * "
                        . "FROM temp_cart_addition "
                        . "LEFT JOIN event_ticket_type ON event_ticket_type.event_ticket_type_id=temp_cart_addition.TCA_item_id "
                        . "WHERE temp_cart_addition.TCA_session_id='$sessionID' "
                        . "AND temp_cart_addition.TCA_TC_id=$TempCartEventID "
                        . "AND temp_cart_addition.TCA_item_type='type' "
                        . "ORDER BY `temp_cart_addition`.`TCA_item_updated` DESC";
                $resultGetTicktType = mysqli_query($con->open(), $sqlGetTicktType);

                if ($resultGetTicktType) {
                    while ($resultGetTicktTypeObj = mysqli_fetch_array($resultGetTicktType)) {
                        $arrayWholeCart[(count($arrayWholeCart) - 1)]['event_addition'][$resultGetTicktTypeObj['TCA_item_type']][] = $resultGetTicktTypeObj;
                    }
                } else {
                    echo "resultGetTicktType query failed." . mysqli_error($con->open());
                }


                //getting ticket include information
                $sqlGetInclude = "SELECT * "
                        . "FROM temp_cart_addition "
                        . "LEFT JOIN event_includes ON event_includes.event_includes_id=temp_cart_addition.TCA_item_id "
                        . "WHERE temp_cart_addition.TCA_session_id='$sessionID' "
                        . "AND temp_cart_addition.TCA_TC_id=$TempCartEventID "
                        . "AND temp_cart_addition.TCA_item_type='include' "
                        . "ORDER BY `temp_cart_addition`.`TCA_item_updated` DESC";
                $resultGetInclude = mysqli_query($con->open(), $sqlGetInclude);

                if ($resultGetInclude) {
                    while ($resultGetIncludeObj = mysqli_fetch_array($resultGetInclude)) {
                        $arrayWholeCart[(count($arrayWholeCart) - 1)]['event_addition'][$resultGetIncludeObj['TCA_item_type']][] = $resultGetIncludeObj;
                    }
                } else {
                    echo "resultGetInclude query failed." . mysqli_error($con->open());
                }
            }
        } else {
            echo "resultWholeCart query failed.";
        }
        /*************************************** Generate Whole Cart ************************************************ */

        if ($resultDeleteTempCartAdd) {
            $return_array = array("output" => "success", "msg" => "Item deleted from cart.", "CartCount" => count($arrayWholeCart), "WholeCart" => $arrayWholeCart);
            echo json_encode($return_array);
            exit();
        } else {
            $return_array = array("output" => "error", "msg" => "resultDeleteTempCartAdd query failed.");
            echo json_encode($return_array);
            exit();
        }
    } else {
        $return_array = array("output" => "error", "msg" => "resultDeleteTempCartEvent query failed.");
        echo json_encode($return_array);
        exit();
    }
}

