<?php

include '../admin/config/class.web.config.php';
$con = new Config();
extract($_POST);
//echo $billing_address_id;
if ($billing_address_id == "") {
    $billing_address_array = array(
        "billing_address" => $billing_address,
        "billing_city" => $billing_city,
        "billing_country" => $billing_country,
        "billing_post_code" => $billing_post_code,
        "customer_id" => $customer_id
    );

    $insert_billing_address_array = $con->Insert("billing_address", $billing_address_array, "", "", "array");
    if ($insert_billing_address_array["output"] == "success") {

        $return_array = array("output" => "success", "msg" => "Billing address saved successfully");
        echo json_encode($return_array);
        exit();
    } else {
        $return_array = array("output" => "error", "msg" => "Billing address not saved");
        echo json_encode($return_array);
        exit();
    }
} else{

    if ($billing_address_id != "" && $billing_address != "" && $billing_post_code != "" && $billing_city != "" && $billing_country != "") {
        $billing_address_update = $con->Update("billing_address", $_POST, "", "billing_address_id=$billing_address_id", "array");
        if ($billing_address_update["output"] == "success") {

            $return_array = array("output" => "success", "msg" => "Billing Address Changed Successfully");
            echo json_encode($return_array);
            exit();
        } else {
            $return_array = array("output" => "error", "msg" => "Billing Address Not Updated");
            echo json_encode($return_array);
            exit();
        }
    } else {

        $return_array = array("output" => "error", "msg" => "Billing Address Not Updated");
        echo json_encode($return_array);
        exit();
    }
} 
?>
