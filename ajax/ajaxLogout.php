<?php

include "../admin/config/class.web.config.php";
$con = new Config();
$sessionID = session_id();
$con->logout();


if(!($con->checkUserLogin())){
    
    $sqlTmpCart = "DELETE FROM temp_carts_events WHERE TC_session_id='$sessionID'";
    $resultDelTmpCart = mysqli_query($con->open(),$sqlTmpCart);
    
    $sqlTmpCartAdd = "DELETE FROM temp_cart_addition WHERE TCA_session_id='$sessionID'";
    $resultTmpCartAdd = mysqli_query($con->open(), $sqlTmpCartAdd);
    
    if($resultDelTmpCart && $resultTmpCartAdd){
        $_SESSION['is_verified'] = false;
        session_regenerate_id();
        $return_array = array("output" => "success", "msg" => "Logged out successfully.");
        echo json_encode($return_array);
        exit();
    }
} else {
    $return_array = array("output" => "error", "msg" => "Login first.");
    echo json_encode($return_array);
    exit();
}