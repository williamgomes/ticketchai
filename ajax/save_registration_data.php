<?php

include '../admin/config/class.web.config.php';
$con = new Config();
$sessionID = session_id();
extract($_POST);

if ($email) {

    $CheckExists = $con->CheckExistsWithCondition("customer", "email='$email'");
    if ($CheckExists == 0) {
        $insert_customer = $con->Insert("customer", $_POST, "", "", "array");
        if ($insert_customer["output"] == "success") {

            //setting customer session
            $_SESSION['first_name'] = $first_name;
            $_SESSION['last_name'] = $last_name;
            $_SESSION['email'] = $email;
            $_SESSION['customer_id'] = $insert_customer["id"];

            $arrCusAdd = array(
                "CA_session_id" => $sessionID,
                "CA_customer_id" => $_SESSION['customer_id']
            );

            $updateCustomerAdd = $con->Update('customer_addition', $arrCusAdd, '', '', 'array');

            $arrTmpCart = array(
                "TC_session_id" => $sessionID,
                "TC_user_id" => $_SESSION['customer_id']
            );

            $updateTempCart = $con->Update('temp_carts_events', $arrTmpCart, '', '', 'array');

            if ($updateCustomerAdd['output'] == "success" AND $updateTempCart['output'] == "success") {

                $return_array = array("output" => "success", "first_name" => $first_name, "msg" => $insert_customer["msg"]);
                echo json_encode($return_array);
                exit();
            }
        } else {
            $return_array = array("output" => "error", "msg" => "Customer is not inserted");
            echo json_encode($return_array);
            exit();
        }
    } else {
        $return_array = array("output" => "warning", "msg" => "Email Address Already Exists");
        echo json_encode($return_array);
    }
}
?>