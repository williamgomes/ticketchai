<?php

include '../admin/config/class.web.config.php';
$con = new Config();

$additionID = 0;
$type = '';
$quantity = 0;
$eventID = 0;
$scheduleID = 0;
$countRecord = 0;
$sessionID = session_id();
$userID = 0;
$error_return = array();
$return_array = array();
if (isset($_SESSION['customer_id'])) {
    $userID = $_SESSION['customer_id'];
}

extract($_POST);

if ($additionID > 0 AND $type != "" AND $quantity > 0 AND $eventID > 0 AND $scheduleID > 0) {

    $isSuccess = TRUE;
    $isUpdate = FALSE;
    $TempCartEventID = 0;

    $sqlCheckRecord = "SELECT * FROM temp_carts_events "
            . "WHERE TC_session_id='$sessionID' "
            . "AND TC_product_id=$eventID "
            . "AND TC_product_type='event' "
            . "AND TC_schedule_id=$scheduleID";
    $resultCheckRecord = mysqli_query($con->open(), $sqlCheckRecord);
    if ($resultCheckRecord) {
        $countRecord = mysqli_num_rows($resultCheckRecord);

        if ($countRecord == 0) { //record does not exist, need to create new record
            $insertEventInfo = '';
            $insertEventInfo .=' TC_session_id = "' . mysqli_real_escape_string($con->open(), $sessionID) . '"';
            $insertEventInfo .=', TC_user_id ="' . intval($userID) . '"';
            $insertEventInfo .=', TC_product_id ="' . intval($eventID) . '"';
            $insertEventInfo .=', TC_product_type ="' . mysqli_real_escape_string($con->open(), 'event') . '"';
            $insertEventInfo .=', TC_schedule_id ="' . intval($scheduleID) . '"';

            $sqlInsertEvent = "INSERT INTO temp_carts_events SET $insertEventInfo";
            $resultInsertEventID = $con->InsertObjectByQuery($sqlInsertEvent);
            if ($resultInsertEventID) {
                $TempCartEventID = $resultInsertEventID;
            } else {
                $error_return = array("output" => "error", "msg" => "resultInsertEvent " . mysqli_error($con->open()) . "");
                $isSuccess = FALSE;
            }
        } else { //record exist, need to fetch the information
            $resultRecord = mysqli_fetch_array($resultCheckRecord);
            $TempCartEventID = $resultRecord['TC_id'];
        }
    } else {
        $error_return = array("output" => "error", "msg" => "resultCheckRecord " . mysqli_error($con->open()) . "");
        $isSuccess = FALSE;
    }

    
    if($sessionID != ""){
        
        $sqlSelectCustAdd = "SELECT * FROM customer_addition WHERE CA_session_id='$sessionID'";
        $resultSelectCustAdd = mysqli_query($con->open(), $sqlSelectCustAdd);
        if($resultSelectCustAdd){
            $resultSelectCustAddObj = mysqli_fetch_array($resultSelectCustAdd);
            $programID = $resultSelectCustAddObj['CA_program_id'];
            $batchID = $resultSelectCustAddObj['CA_batch_id'];
            
            if($programID > 0 AND $batchID > 0){
                $sqlBatchPrice = "SELECT * FROM batch WHERE program_id=$programID AND batch_id=$batchID";
                $resultBatchPrice = mysqli_query($con->open(), $sqlBatchPrice);
                
                if($resultBatchPrice){
                    $resultBatchPriceObj = mysqli_fetch_array($resultBatchPrice);
                    
                    if(isset($resultBatchPriceObj['batch_id'])){
                        $totalPrice = $resultBatchPriceObj['ticket_price'] + $resultBatchPriceObj['anual_subscription'] + $resultBatchPriceObj['enrollment_fee'];
                        
                        if($totalPrice > 0){
                            $sqlCheckEventTicktType = "SELECT * FROM temp_cart_addition "
                                    . "WHERE TCA_session_id='$sessionID' "
                                    . "AND TCA_item_id=$programID "
                                    . "AND TCA_item_type='type' "
                                    . "AND TCA_TC_id=$batchID";
                            $resultCheckEventTicktType = mysqli_query($con->open(), $sqlCheckEventTicktType);
                            if ($resultCheckEventTicktType) {
                                $countCheckTicktType = mysqli_num_rows($resultCheckEventTicktType);

                                if ($countCheckTicktType > 0) { //ticket type already exist in temp table, need to update quantity
                                    $resultCheckTicktType = mysqli_fetch_array($resultCheckEventTicktType);
                                    //calculating new total price
                                    $itemPerPrice = $totalPrice;
                                    $newTotalPrice = $totalPrice * $quantity;
                                    $tmpCartAdditionID = $resultCheckTicktType['TCA_id'];

                                    //updating table with new quantity and price
                                    $updateEventTypeInfo = '';
                                    $updateEventTypeInfo .=' TCA_item_total_price = "' . floatval($newTotalPrice) . '"';
                                    $updateEventTypeInfo .=', TCA_item_quantity = "' . intval($quantity) . '"';

                                    $sqlUpdateEventTypeInfo = "UPDATE temp_cart_addition SET $updateEventTypeInfo WHERE TCA_id=$tmpCartAdditionID";
                                    $resultUpdateEventTypeInfo = mysqli_query($con->open(), $sqlUpdateEventTypeInfo);

                                    if (!$resultUpdateEventTypeInfo) {
                                        $error_return = array("output" => "error", "msg" => "resultUpdateEventTypeInfo " . mysqli_error($con->open()) . "");
                                        $isSuccess = FALSE;
                                    } else {
                                        $isUpdate = TRUE;
                                    }
                                } else { //ticket type does not exist in temp table, need to insert
                                    //getting necessary information from type table
    //                                $eventTypePerPrice = 0;
    //                                $sqlCheckEventTypeInfo = "SELECT * FROM event_ticket_type WHERE event_ticket_type_id=$additionID";
    //                                $resultCheckEventTypeInfo = mysqli_query($con->open(), $sqlCheckEventTypeInfo);
    //                                if ($resultCheckEventTypeInfo) {
    //                                    $arrCheckEventTypeInfo = mysqli_fetch_array($resultCheckEventTypeInfo);
    //                                    $eventTypePerPrice = $arrCheckEventTypeInfo['ticket_price'];
    //                                }

                                    //inserting ticket type information into table
                                    $insertEventTypeInfo = '';
                                    $insertEventTypeInfo .=' TCA_TC_id = "' . intval($batchID) . '"';
                                    $insertEventTypeInfo .=', TCA_session_id ="' . mysqli_real_escape_string($con->open(), $sessionID) . '"';
                                    $insertEventTypeInfo .=', TCA_item_id ="' . intval($programID) . '"';
                                    $insertEventTypeInfo .=', TCA_item_type ="' . mysqli_real_escape_string($con->open(), 'type') . '"';
                                    $insertEventTypeInfo .=', TCA_item_per_price ="' . floatval($totalPrice) . '"';
                                    $insertEventTypeInfo .=', TCA_item_total_price ="' . floatval(($totalPrice * $quantity)) . '"';
                                    $insertEventTypeInfo .=', TCA_item_quantity ="' . intval($quantity) . '"';

                                    $sqlInsertEventTypeInfo = "INSERT INTO temp_cart_addition SET $insertEventTypeInfo";
                                    $resultInsertEventTypeInfo = mysqli_query($con->open(), $sqlInsertEventTypeInfo);

                                    if (!$resultInsertEventTypeInfo) {
                                        $error_return = array("output" => "error", "msg" => "resultInsertEventTypeInfo " . mysqli_error($con->open()) . "");
                                        $isSuccess = FALSE;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            $error_return = array("output" => "error", "msg" => "resultInsertEventTypeInfo " . mysqli_error($con->open()) . "");
            $isSuccess = FALSE;
        }
        
    }
    

    /*     * ****************************************Temp Cart Generation********************************************* */

    $arrayWholeCart = array();
    $sqlWholeCart = "SELECT * "
            . "FROM temp_carts_events "
            . "LEFT JOIN event ON event.event_id=temp_carts_events.TC_product_id "
            . "LEFT JOIN event_schedule ON event_schedule.event_schedule_id=temp_carts_events.TC_schedule_id "
            . "LEFT JOIN event_venue ON event_venue.event_id=temp_carts_events.TC_product_id "
            . "LEFT JOIN venue ON venue.venue_id=event_venue.venue_id "
            . "WHERE temp_carts_events.TC_session_id='$sessionID' "
            . "AND event_venue.is_active='true' "
            . "ORDER BY `temp_carts_events`.`TC_updated` DESC";
    $resultWholeCart = mysqli_query($con->open(), $sqlWholeCart);
    if ($resultWholeCart) {
        while ($resultWholeCartObj = mysqli_fetch_array($resultWholeCart)) {
            $arrayWholeCart[]['event_details'] = $resultWholeCartObj;
            $TempCartEventID = $resultWholeCartObj['TC_id'];
            
            $sqlSelectCustAdd = "SELECT * FROM customer_addition WHERE CA_session_id='$sessionID'";
            $resultSelectCustAdd = mysqli_query($con->open(), $sqlSelectCustAdd);
            if($resultSelectCustAdd){
                $resultSelectCustAddObj = mysqli_fetch_array($resultSelectCustAdd);
                $programID = $resultSelectCustAddObj['CA_program_id'];
                $batchID = $resultSelectCustAddObj['CA_batch_id'];
            }
            
            //getting ticket type information
            $sqlGetTicktType = "SELECT * "
                    . "FROM temp_cart_addition "
                    . "LEFT JOIN batch ON batch.batch_id=$batchID "
                    . "WHERE temp_cart_addition.TCA_session_id='$sessionID' "
                    . "AND temp_cart_addition.TCA_TC_id=$batchID "
                    . "AND temp_cart_addition.TCA_item_id=$programID "
                    . "AND temp_cart_addition.TCA_item_type='type' "
                    . "ORDER BY `temp_cart_addition`.`TCA_item_updated` DESC";
            $resultGetTicktType = mysqli_query($con->open(), $sqlGetTicktType);

            if ($resultGetTicktType) {
                while ($resultGetTicktTypeObj = mysqli_fetch_array($resultGetTicktType)) {
                    $arrayWholeCart[(count($arrayWholeCart) - 1)]['event_addition'][$resultGetTicktTypeObj['TCA_item_type']][] = $resultGetTicktTypeObj;
                }
            } else {
                echo "resultGetTicktType query failed." . mysqli_error($con->open());
            }


            //getting ticket include information
//            $sqlGetInclude = "SELECT * "
//                    . "FROM temp_cart_addition "
//                    . "LEFT JOIN event_includes ON event_includes.event_includes_id=temp_cart_addition.TCA_item_id "
//                    . "WHERE temp_cart_addition.TCA_session_id='$sessionID' "
//                    . "AND temp_cart_addition.TCA_TC_id=$TempCartEventID "
//                    . "AND temp_cart_addition.TCA_item_type='include' "
//                    . "ORDER BY `temp_cart_addition`.`TCA_item_updated` DESC";
//            $resultGetInclude = mysqli_query($con->open(), $sqlGetInclude);
//
//            if ($resultGetInclude) {
//                while ($resultGetIncludeObj = mysqli_fetch_array($resultGetInclude)) {
//                    $arrayWholeCart[(count($arrayWholeCart) - 1)]['event_addition'][$resultGetIncludeObj['TCA_item_type']][] = $resultGetIncludeObj;
//                }
//            } else {
//                echo "resultGetInclude query failed." . mysqli_error($con->open());
//            }
        }
    } else {
        echo "resultWholeCart query failed.";
    }
    /*     * ****************************************Temp Cart Generation********************************************* */

    if ($isSuccess) {
        if ($isUpdate) {
            $return_array = array("output" => "success", "msg" => "Product updated to cart successfully.", "CartCount" => count($arrayWholeCart), "WholeCart" => $arrayWholeCart);
        } else {
            $return_array = array("output" => "success", "msg" => "Product added to cart successfully.", "CartCount" => count($arrayWholeCart), "WholeCart" => $arrayWholeCart);
        }
    }

    echo json_encode($return_array);
}