<?php

include "../admin/config/class.web.config.php";
$con = new Config();

$WL_product_id = "";
$WL_product_type = "";

$userID = 0;
if($con->checkUserLogin() == true){
    $userID = $_SESSION['customer_id'];
}

extract($_POST);

if ($con->checkUserLogin()) {
    $postData = $_POST;
    $postData['WL_user_id'] = $userID;
    if ($WL_product_id > 0 AND $WL_product_type != "") {

        // check availability for wishlist
        $check_wishlist_available = $con->CheckExistsWithCondition("wishlists", "WL_product_id='$WL_product_id' AND WL_user_id='$userID' AND WL_product_type='$WL_product_type'");
       // $con->debug($check_wishlist_available);
        if ($check_wishlist_available == 0) {
            $insert_wishlist = $con->Insert("wishlists", $postData, "", "", "array");
            if ($insert_wishlist["output"] == "success") {
                $return_array = array("output" => "success", "msg" => "Added to wishlist successfully.");
                echo json_encode($return_array);
                exit();
            }
        } else {
            $return_array = array("output" => "warning", "msg" => "Already added to wishlist.");
            echo json_encode($return_array);
            exit();
        }
    }
} else {
    $return_array = array("output" => "login", "msg" => "login");
    echo json_encode($return_array);
    exit();
}