<?php
$files = $_FILES['profile_photo'];
$targetfolder = '../images/users/';

if (empty($_FILES)) {
    $_POST["label_ref_image"] = "no_image.jpg";
    $array = array("output" => "uploaded", "u_image" => "no_image.jpg");
    echo json_encode($array);
} else {
    $filename = "user" . '_' . date("Y_m_d") . '_' . time() . basename($_FILES['profile_photo']['name']);
    $full_file_name = $targetfolder . $filename;

    if (!file_exists($full_file_name)) {
        $_POST["profile_photo"] = $filename;
        $targetfolder = $targetfolder . $filename;
        move_uploaded_file($_FILES['profile_photo']['tmp_name'], $targetfolder);
        $array = array("output" => "uploaded", "u_image" => $filename);
        echo json_encode($array);
    } else {
        $_POST["profile_photo"] = "no_image.jpg";
        $array = array("output" => "not_uploaded", "u_image" => "no_image.jpg");
        echo json_encode($array);
    }
}