<?php
include("./admin/config/class.web.config.php");
$con = new Config();
$event_id = "";
$batch_id = '';
$program_id = '';
$event = array();
if (isset($_GET["event_id"])) {
    $event_id = base64_decode($_GET["event_id"]);
    //$con->debug($event_id);
    $event = $con->ReturnObjectByQuery("select * from event where event_id='$event_id'", "array");
}

$schedule = $con->ReturnObjectByQuery("SELECT * FROM event_schedule WHERE event_id='$event_id'", "array");
//$con->debug($schedule);
$photo = $con->ReturnObjectByQuery("select * from photo_gallery where event_id='$event_id'", "array");
//$con->debug($photo);
$video = $con->ReturnObjectByQuery("select * from video_gallery where event_id='$event_id'", "array");
//$con->debug($video);
$venue = $con->ReturnObjectByQuery("SELECT event_venue.*,venue.* FROM event_venue LEFT JOIN venue ON event_venue.venue_id = venue.venue_id WHERE event_venue.event_id = '$event_id'", "array");
//$con->debug($venue);
$attraction = $con->ReturnObjectByQuery("SELECT * FROM event_key_attraction WHERE event_id='$event_id'", "array");

$faq = $con->ReturnObjectByQuery("SELECT * FROM event_faq WHERE event_id = '$event_id'", "array");
//$con->debug($faq);
$event_details = $con->ReturnObjectByQuery("SELECT * FROM event_details WHERE event_id = '$event_id'", "array");
//$con->debug($event_details);
$event_terms_and_conditions = $con->ReturnObjectByQuery("SELECT * FROM event_terms_and_conditions WHERE event_id = '$event_id'", "array");
//$con->debug($event_terms_and_conditions);
$activity = $con->ReturnObjectByQuery("SELECT * FROM event_activity WHERE event_id = '$event_id'", "array");
//$con->debug($activity);

$user_review = $con->ReturnObjectByQuery("SELECT user_review.*, customer.first_name, customer.last_name FROM user_review LEFT JOIN customer ON user_review.user_id = customer.customer_id WHERE event_id ='$event_id' AND status='published' ORDER BY user_review.updated_date DESC", "array");
//$con->debug($user_review);


$batches = $con->SelectAll("batch", "", "", "array");
$program = $con->SelectAll("program", "", "", "array");
?>
<?php include './header_script.php'; ?>
<body class="header-fixed">
    <script>
        function initialize() {
            var myLatlng = new google.maps.LatLng(<?php echo $venue{0}->latitude; ?>, <?php echo $venue{0}->longitude; ?>);
            var mapOptions = {
                zoom: 15,
                center: myLatlng
            }
            var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: 'Hello World!'
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize);

    </script>
    <div class="wrapper"> 
        <div class="header-v5 header-static">
            <?php include './menu_top.php'; ?>
            <?php include './header.php'; ?>
        </div>
        <div class="shop-product">
            <div class="breadcrumbs">
                <div class="container">
                    <ul class="breadcrumb-v5">
                        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
                        <li><a href="index.php">Home</a></li>
                        <li class="active">Event Details</li>
                    </ul> 
                </div>
            </div>
            <div class="container content" style="padding-top: 0px;">
                <!-- About Sldier Start Here-->
                <div class="shadow-wrapper margin-bottom-50">
                    <div class="carousel slide carousel-v1 box-shadow shadow-effect-2" id="myCarousel">
                        <div class="carousel-inner">
                            <div class="item active">
                                <img class="img-responsive" src="<?php echo "admin/uploads/event_full_size_image/"; ?><?php echo $event{0}->event_full_size_image; ?>" style="height:300px; width:1150px;" alt="">
                            </div>                  
                        </div>            
                    </div>
                    <!-- End About Sldier -->
                </div> 
                <!-- Tab Part Start Here -->
                <div class="tab-v1">
                    <ul class="nav nav-tabs">
                        <?php if (count($schedule) >= 1): ?>
                            <li class="active"><a data-toggle="tab" href="#buy-ticket">Buy Ticket</a></li>
                        <?php endif; ?>
                        <?php if (count($venue) >= 1): ?>
                            <li><a data-toggle="tab" href="#venue">Venue Details</a></li>
                        <?php endif; ?>
                        <?php if (count($event_details) >= 1) : ?>
                            <li id="event_details_tab"><a data-toggle="tab" href="#event-details">Event Details</a></li>
                        <?php endif; ?>
                        <?php if (count($photo) >= 1 || (count($video) >= 1)): ?>
                            <li><a data-toggle="tab" href="#gallery">Gallery</a></li>
                        <?php endif; ?>
                        <?php if (count($activity) >= 1): ?>
                            <li><a data-toggle="tab" href="#activity">Activity</a></li>
                        <?php endif; ?>
                        <?php if (count($attraction) >= 1): ?>
                            <li><a data-toggle="tab" href="#attraction">Attraction</a></li>
                        <?php endif; ?>
                        <?php if (count($faq) >= 1): ?>
                            <li><a data-toggle="tab" href="#faq">FAQ's</a></li>
                        <?php endif; ?>
                        <!-- Event Ratings -->
                        <li><a data-toggle="tab" href="#user_ratings">Review & Ratings</a></li>
                        <?php if (count($event_terms_and_conditions) >= 1): ?>
                            <li><a data-toggle="tab" href="#terms-and-condition">Terms and Condition</a></li>
                        <?php endif; ?>
                    </ul> 
                    <div class="tab-content">

                        <!-- Event Buy Ticket Tab Start Here -->
                        <div class="tab-pane fade in active" id="buy-ticket">
                            <div class="product-comment margin-bottom-40">
                                <div class="product-comment-in">
                                    <div class="tab-v1">
                                        <ul class="nav nav-tabs">
                                            <?php if (count($schedule) >= 1): ?>
                                                <?php $count = 0; ?>
                                                <?php foreach ($schedule as $sc): ?>                                                
                                                    <li <?php
                                                    if ($count == 0) {
                                                        echo "class='active'";
                                                    }
                                                    ?>><a data-toggle="tab" href="#scehdule_<?php echo $sc->event_schedule_id; ?>"><?php echo date("jS F, Y", strtotime($sc->event_date)); ?></a></li>
                                                        <?php $count++; ?>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                        </ul>
                                        <div class="tab-content">
                                            <?php if (count($schedule) >= 1): ?>
                                                <?php $count = 0; ?>
                                                <?php foreach ($schedule as $sc): ?>
                                                    <div class="tab-pane fade in <?php
                                                    if ($count == 0) {
                                                        echo 'active';
                                                    }
                                                    ?>" id="scehdule_<?php echo $sc->event_schedule_id; ?>">
                                                        <div class="clearfix margin-bottom-30"></div>
                                                        <div class="shadow-wrapper">
                                                            <?php
                                                            $query_for_ticket_type = $con->SelectAll("event_ticket_type", "", array("event_schedule_id" => $sc->event_schedule_id), "array");
                                                            $query_for_event_includes = $con->SelectAll("event_includes", "", array("event_schedule_id" => $sc->event_schedule_id), "array");
                                                            ?>


                                                            <div class="col-sm-12 col-lg-12">

                                                                <div class="col-sm-12 col-lg-12" id="divValidation">
                                                                    <?php if (!isset($_SESSION['is_verified']) OR $_SESSION['is_verified'] != true): ?>
                                                                        <div class="alert alert-warning fade in">
                                                                            <strong>Sorry!</strong> We need to validate your information before you can purchase event ticket.
                                                                        </div>
                                                                        <div class="row col-lg-6">
                                                                            <section>
                                                                                <label>Program Studied: *</label>
                                                                                <select onchange="javascript:generateBatch(this.value);" class="form-control" id="program_id"  name="program_id">
                                                                                    <option value="0">Select Program</option>
                                                                                    <?php if (count($program) >= 1): ?>
                                                                                        <?php foreach ($program as $p): ?>
                                                                                            <option value="<?php echo $p->program_id; ?>" 
                                                                                            <?php
                                                                                            if ($p->program_id == $program_id) {
                                                                                                echo " selected='selected'";
                                                                                            }
                                                                                            ?>
                                                                                                    ><?php echo $p->program_name; ?></option>
                                                                                                <?php endforeach; ?>
                                                                                            <?php endif; ?>
                                                                                </select>
                                                                            </section>

                                                                            <section>
                                                                                <label>Batch No.: *</label>
                                                                                <select class="form-control" id="batch_id"  name="batch_id">
                                                                                    <option value="0">Select a Program first</option>
                                                                                </select>
                                                                            </section>
                                                                            <section>
                                                                                <label class="input">
                                                                                    Profession: *
                                                                                </label>
                                                                                <input id="profession" type="text" name="name" placeholder="Profession" class="form-control">
                                                                            </section>  
                                                                            <section>
                                                                                <label class="input">
                                                                                    Designation: 
                                                                                </label>
                                                                                <input type="text" id="designation" name="amount" placeholder="Designation" class="form-control">
                                                                            </section> 
                                                                            <section>
                                                                                <label class="input">
                                                                                    Organization: *
                                                                                </label>
                                                                                <input type="text" id="organization" name="amount" placeholder="Organization" class="form-control">
                                                                            </section> 
                                                                            <section>
                                                                                <label class="input">
                                                                                    Blood Group: *
                                                                                </label>
                                                                                <input type="text" id="blood" name="amount" placeholder="Blood Group" class="form-control">
                                                                            </section> 
                                                                            <div class="clearfix"></div>
                             <div class="row">



                            <label class="col-md-6 control-label" for="size">Photo</label>
                            <div class="clearfix"></div>
                            <div class="col-md-6">  <div class="fileupload fileupload-new margin-none" data-provides="fileupload">
                                    <span style="border-color: #799D37;" class="btn btn-default btn-file"><span class="fileupload-new"></span><span class="fileupload-exists"></span><input type="file" id="profile_photo" name="profile_photo"/></span>
                                    <span class="fileupload-preview"></span>

                                </div></div>
                            <script type="text/javascript">
                                $(document).ready(function () {

                                    var remove_url = '';
                                    var remove_image_name = $("#profile_photo_text").val();
                                    if (remove_image_name === "") {
                                        remove_url = "ajax/ajaxRemoveProfileImage.php";
                                    }

                                    $("#profile_photo").kendoUpload({
                                        multiple: false,
                                        async: {
                                            saveUrl: "ajax/ajaxProfileImageSave.php",
                                            removeUrl: "ajax/ajaxRemoveProfileImage.php",
                                            autoUpload: true
                                        },
                                        upload: function (e) {
                                            console.log(e);
                                        },
                                        success: function (e) {
                                            console.log(e.response);
                                            $("#profile_photo_text").val(e.response.u_image);
                                            remove_url = "ajax/ajaxRemoveProfileImage.php?remove_image_name=" + e.response.u_image;
                                            //options.model.set("photo_name", e.response.image);
                                        },
                                        remove: function (e) {
                                            $.ajax({
                                                url: remove_url,
                                                dataType: "json",
                                                type: "POST",
                                                success: function (data) {
                                                    console.log(data);
                                                }

                                            });
                                            $("#profile_photo_text").val('');
                                        }
                                    });
                                });
                            </script>
                            <input type="hidden" name="profile_photo_text" id="profile_photo_text" />
                        </div>
                                                                            <div class="row"></div>                                               
         
                                                                            <input type="hidden" value="<?php echo $event_id; ?>">
                                                                            <input type="hidden" value="<?php echo $sc->event_schedule_id; ?>">
                                                                            <input type="hidden" value="<?php echo $sc->event_schedule_id; ?>">
                                                                            <div class="button-actions clearfix text-right" style="padding-top: 50px !important;">
                                                                                <button onclick="javascript:submitInfo();" type="button" style="padding: 15px; font-size: medium;" class="btn-u btn-u-sea-shop btn-u-lg text-right">Submit&nbsp;&nbsp;<i style="color: white;" class="fa  fa-check-square-o"></i></button>
                                                                            </div>
                                                                            
                                                                        </div>
                                                                    <?php endif; ?>


                                                                    <?php if (isset($_SESSION['is_verified']) AND $_SESSION['is_verified'] == true): ?>
                                                                        <div class="col-md-6 col-sm-6">
                                                                            <div class="service-block service-block-u">
                                                                                <i class="icon-custom icon-color-light rounded-x fa fa-thumbs-o-up"></i>
                                                                                <p>Launching Ceremony Ticket of DUDSAA</p>
                                                                                <a style="margin: 10px 0px;" href="javascript:void(0);" onclick="javascript:showCategoryDiv();" class="btn-u btn-u-orange">Buy Ticket</a>
                                                                                <p style="color: black"><strong><em>** Not Applicable for Life Members</em></strong></p>
                                                                                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6 col-sm-6">
                                                                            <div class="service-block service-block-u">
                                                                                <i class="icon-custom icon-color-light rounded-x fa fa-thumbs-o-up"></i>
                                                                                <p>Be an honored Life Member</p>
                                                                                <a style="margin: 10px 0px;" onclick="javascript:showSubscribeDiv();" href="javascript:void(0);" class="btn-u btn-u-orange">Subscribe for Lifetime</a>
                                                                                <p style="color: black; font-size: small;"><strong><em>**Discounted subscription fee is 10,000 BDT (Only for event)</em></strong></p>
                                                                                <p style="color: black; font-size: small;"><strong><em>**Regular subscription fee is 25,000 BDT</em></strong></p>
                                                                            </div>
                                                                        </div>    

                                                                    <?php endif; ?>

                                                                </div>

                                                            </div>


                                                            <!-- Event Ticket Type Block Start -->
                                                            <?php if (count($query_for_ticket_type) >= 1) : ?>
                                                                <?php foreach ($query_for_ticket_type as $t): ?>
                                                                    <div class="col-lg-12 col-sm-12" id="divCategory" style="display: none;">
                                                                        <div class="panel-heading">
                                                                            <h3 class="panel-title"><i class="fa fa-globe"></i> Event Schedule Tickets</h3>
                                                                        </div>
                                                                        <div class="panel-body">
                                                                            <table class="table table-striped">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th style="width: 45%;">Ticket Type</th>
                <!--                                                                                        <th style="width: 15%;">Price</th>-->
                                                                                        <th style="width: 40%;">Capacity</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="width: 45%;"><?php echo $t->ticket_name; ?></td>
                                                                                        <!--<td style="width: 15%;"><a style="height: 40px;width: 100%;padding-top: 10px;" class="btn btn-danger btn-xs">Tk. <?php echo $t->ticket_price; ?></a></td>--> 
                                                                                        <td style="width: 40%;">
                                                                                            <button style="height: 40px;" type='button' class="quantity-button" name='subtract' onclick='javascript:subtractQty(<?php echo $t->event_ticket_type_id; ?>,<?php echo $t->per_user_limit; ?>);' value='-'>-</button>
                                                                                            <input id="txt_type_quantity_<?php echo $t->event_ticket_type_id; ?>" style="height: 40px;" type='text' class="quantity-field" name='qty' value="1" />
                                                                                            <button type='button' style="height: 40px;" class="quantity-button" name='add' onclick='javascript:addtionQty(<?php echo $t->event_ticket_type_id; ?>,<?php echo $t->per_user_limit; ?>);' value='+'>+</button>
                                                                                            <button onclick="javascript:addToCart('event',<?php echo $event_id; ?>,<?php echo $sc->event_schedule_id; ?>,<?php echo $t->event_ticket_type_id; ?>);" type="button" style="height: 40px; width: 166px;padding-top: 7px;" class="btn-u btn-u-sea-shop btn-u-lg">Buy Ticket</button>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>                      
                                                                    </div>
                                                                    <?php $count++; ?>
                                                                <?php endforeach; ?>
                                                            <?php endif; ?>
                                                            <!--Event Ticket Type Block End-->
                                                            
                                                            
                                                            
                                                            <div class="col-lg-12 col-sm-12" id="divSubscribe" style="display: none;">
                                                            <div class="panel-heading">
                                                                <h3 class="panel-title"><i class="fa fa-globe"></i> Lifetime Subscription</h3>
                                                            </div>
                                                            <div class="panel-body">
                                                                <table class="table table-striped">
                                                                    <thead>
                                                                        <tr>
                                                                            <th style="width: 45%;">Subscription Type</th>
    <!--                                                                                        <th style="width: 15%;">Price</th>-->
                                                                            <th style="width: 40%;">Amount</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="width: 45%;">Lifetime Subscription to DUDSAA<br/>Discount Amount: 10,000 BDT.</td>
                                                                            <!--<td style="width: 15%;"><a style="height: 40px;width: 100%;padding-top: 10px;" class="btn btn-danger btn-xs">Tk. <?php echo $t->ticket_price; ?></a></td>--> 
                                                                            <td style="width: 40%;">
                                                                                <!--<button style="height: 40px;" type='button' class="quantity-button" name='subtract' onclick='javascript:subtractQty(<?php echo $t->event_ticket_type_id; ?>,<?php echo $t->per_user_limit; ?>);' value='-'>-</button>-->
                                                                                BDT. &nbsp;&nbsp;<input id="txt_type_quantity_<?php echo $t->event_ticket_type_id; ?>" style="height: 40px; width: 80px; margin-right: 50px;" type='text' class="quantity-field" name='qty' value="10000" disabled="disabled" />
                                                                                <!--<button type='button' style="height: 40px;" class="quantity-button" name='add' onclick='javascript:addtionQty(<?php echo $t->event_ticket_type_id; ?>,<?php echo $t->per_user_limit; ?>);' value='+'>+</button>-->
                                                                                <button onclick="javascript:addToCart('subscribe',<?php echo $event_id; ?>,<?php echo $sc->event_schedule_id; ?>,0);" type="button" style="height: 40px; width: 166px;padding-top: 7px;" class="btn-u btn-u-sea-shop btn-u-lg">Subscribe Here</button>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>                      
                                                        </div>
                                                            

                                                            <!-- Event Includes Block -->
                                                            <?php if (count($query_for_event_includes) >= 1) : ?>
                                                                <?php foreach ($query_for_event_includes as $evi): ?>
                                                                    <!--                                                                    <div class="panel panel-grey">
                                                                                                                                            <div class="panel-heading">
                                                                                                                                                <h3 class="panel-title"><i class="fa fa-globe"></i> Event Includes</h3>
                                                                                                                                            </div>
                                                                                                                                            <div class="panel-body">
                                                                                                                                                <table class="table table-striped">
                                                                                                                                                    <thead>
                                                                                                                                                        <tr>
                                                                                                                                                            <th style="width: 45%;">Includes Type</th>
                                                                    
                                                                                                                                                            <th style="width: 15%;">Price</th>
                                                                                                                                                            <th style="width: 40%;">Capacity</th>
                                                                                                                                                        </tr>
                                                                                                                                                    </thead>
                                                                                                                                                    <tbody>
                                                                                                                                                        <tr>
                                                                                                                                                            <td style="width: 45%;"><?php echo $evi->event_includes_name; ?></td>
                                                                                                                                                            <td style="width: 15%;"><a style="height: 40px;width: 100%;padding-top: 10px;" class="btn btn-danger btn-xs">Tk. <?php echo $evi->event_includes_price; ?></a></td> 
                                                                                                                                                            <td style="width: 40%;">
                                                                                                                                                                <button style="height: 40px;" id="btn_decrease_<?php echo $evi->event_includes_id; ?>" type='button' class="quantity-button" name='subtract' onclick='javascript:subtractIncludesQty(<?php echo $evi->event_includes_id; ?>,<?php echo $evi->event_includes_per_user_limit; ?>);' value='-'>-</button>
                                                                                                                                                                <input  style="height: 40px;" id="txt_include_quantity_<?php echo $evi->event_includes_id; ?>" type='text' class="quantity-field" name='qty_includes' value="1" />
                                                                                                                                                                <button style="height: 40px;" id="btn_increase_<?php echo $evi->event_includes_id; ?>" type='button' class="quantity-button" name='add' onclick='javascript:addtionIncludesQty(<?php echo $evi->event_includes_id; ?>,<?php echo $evi->event_includes_per_user_limit; ?>);' value='+'>+</button>
                                                                                                                                                                <button onclick="javascript:addToCart('include',<?php echo $event_id; ?>,<?php echo $sc->event_schedule_id; ?>,<?php echo $evi->event_includes_id; ?>);" type="button" style="height: 40px; width: 166px; padding-top: 7px;" class="btn-u btn-u-sea-shop btn-u-lg">Buy Includes</button>
                                                                                                                                                            </td>
                                                                                                                                                        </tr>
                                                                                                                                                    </tbody>
                                                                                                                                                </table>
                                                                                                                                            </div>                      
                                                                                                                                        </div>-->
                                                                <?php endforeach; ?>
                                                            <?php endif; ?>
                                                            <!-- Event Includes Block End -->
                                                        </div>
                                                    </div>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Event Buy Ticket Tab End Here  -->

                        <!-- Event Venue Details Tab Start Here -->

                        <div class="tab-pane fade in" id="venue">
                            <div class="row">
                                <div class="col-md-12">

                                    <h4>Venue Location</h4>
                                    <p><b>Venue Name&nbsp;:&nbsp;</b><?php echo $venue{0}->venue_name; ?>&comma;&nbsp;&nbsp;&nbsp;<b>Venue Address&nbsp;:&nbsp;</b><?php echo $venue{0}->venue_address; ?></p>
                                </div>
                            </div>
                            <div class="row ">

                                <div class="col-md-1"></div>

                                <div class="col-md-1"></div>
                            </div>
                        </div>
                        <!-- Event Venue Details Tab End Here -->

                        <!-- Event Details Tab Start Here -->
                        <?php if (count($event_details) >= 1): ?>
                            <div class="tab-pane fade in" id="event-details">
                                <div class="shadow-wrapper margin-bottom-60">
                                    <div class="tag-box tag-box-v1 box-shadow shadow-effect-2">
                                        <p><?php echo html_entity_decode(html_entity_decode($event_details{0}->event_details_description, ENT_COMPAT, 'UTF-8')); ?></p>

                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <!-- Event Details Tab End Here -->

                        <!-- Event Photo And video Gallery Tab Start Here -->
                        <div class="tab-pane fade in" id="gallery">
                            <div class="product-comment margin-bottom-40">
                                <div class="product-comment-in">
                                    <div class="tab-v1">
                                        <ul class="nav nav-tabs">
                                            <?php if (count($photo) >= 1): ?>
                                                <li class="active"><a data-toggle="tab" href="#photo">Photo Gallery</a></li>
                                            <?php endif; ?>
                                            <?php if (count($video) >= 1): ?>
                                                <li><a data-toggle="tab" href="#video">Video Gallery</a></li>
                                            <?php endif; ?>
                                        </ul>
                                        <div class="tab-content">
                                            <?php if (count($photo) >= 1): ?>
                                                <div class="tab-pane fade in active" id="photo">
                                                    <div class="container">
                                                        <div class="col-md-11">
                                                            <div class="illustration-v2 margin-bottom-60">
                                                                <div class="customNavigation margin-bottom-25">
                                                                    <a class="owl-btn prev rounded-x"><i class="fa fa-angle-left"></i></a>
                                                                    <a class="owl-btn next rounded-x"><i class="fa fa-angle-right"></i></a>
                                                                </div>
                                                                <ul class="list-inline owl-slider-v4">
                                                                    <?php if (count($photo) >= 1) : ?>
                                                                        <?php foreach ($photo as $ph) : ?>
                                                                            <li class="item">
                                                                                <a href="#"><img style="height: 150px;" class="img-responsive img-bordered rounded-2x" src="<?php echo "admin/uploads/event_photo_gallery/"; ?><?php echo $ph->photo_name; ?>" alt=""></a>
                                                                            </li>
                                                                        <?php endforeach; ?>
                                                                    <?php endif; ?>
                                                                </ul>
                                                            </div> 
                                                        </div>
                                                    </div> 
                                                </div>
                                            <?php endif; ?>

                                            <div class="tab-pane fade in" id="video">
                                                <?php if (count($video) >= 1): ?>
                                                    <?php foreach ($video as $vi) : ?>
                                                        <div class="col-md-6" style="margin-top: 20px;">
                                                            <div class="responsive-video">
                                                                <iframe src="<?php echo $vi->link; ?>" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
                                                            </div>
                                                        </div> 
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Event Photo And video Gallery Tab End Here -->

                        <!-- Event Activity Tab Start Here -->
                        <?php if (count($activity) >= 1): ?>
                            <div class="tab-pane fade in" id="activity">
                                OK Activity
                            </div>
                        <?php endif; ?>
                        <!-- Event Activity Tab End Here -->

                        <!-- Event Attraction Tab Start Here -->
                        <div class="tab-pane fade in" id="attraction">
                            <?php if (count($attraction) >= 1): ?>
                                <?php foreach ($attraction as $attr) : ?>
                                    <!-- Bordered Funny Boxes -->
                                    <div class="funny-boxes funny-boxes-right-u">
                                        <div class="col-md-12">
                                            <div class="col-md-4 funny-boxes-img">
                                                <img class="img-responsive" src="<?php echo "admin/uploads/event_key_attraction/"; ?><?php echo $attr->image; ?>" alt="" style="height:250px;width: 240px;margin-top: 13px;">
                                                <ul class="list-inline team-social">
                                                    <li>
                                                        <a target="_blank" data-placement="top" data-toggle="tooltip" class="fb tooltips" data-original-title="Facebook" href="<?php echo $attr->facebook; ?>">
                                                            <img src="assets_new/img/facebook.png"  alt="">
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a data-placement="top" data-toggle="tooltip" class="tw tooltips" data-original-title="Twitter" href="<?php echo $attr->twitter; ?>">
                                                            <img src="assets_new/img/twitter.png"  alt="">
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a data-placement="top" data-toggle="tooltip" class="gp tooltips" data-original-title="Google plus" href="<?php echo $attr->google_plus; ?>">
                                                            <img src="assets_new/img/google.png"  alt="">
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-8" style="margin-left: -100px;">
                                                <h3><a href="#"><?php echo $attr->attraction_name; ?></a></h3>
                                                <ul class="list-unstyled funny-boxes-rating">
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                </ul>
                                                <p style="text-align: justify;"><?php echo html_entity_decode(html_entity_decode($attr->details)); ?></p>
                                            </div>
                                        </div>                            
                                    </div>
                                    <!-- End Bordered Funny Boxes -->
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                        <!-- Event Attraction Tab End Here -->
                        <!-- Event FAQ'S Tab Start Here -->
                        <div class="tab-pane fade in" id="faq">
                            <div class="col-md-12">
                                <?php if (count($faq) >= 1) : ?>
                                    <?php foreach ($faq as $fa): ?>
                                        <div class="accordion-v2 plus-toggle">
                                            <div class="panel-group" id="accordion-v2_<?php echo $fa->faq_id; ?>">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion-v2_<?php echo $fa->faq_id; ?>" href="#collapseTwo-v2_<?php echo $fa->faq_id; ?>">
                                                                <?php echo $fa->question; ?>
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseTwo-v2_<?php echo $fa->faq_id; ?>" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <?php echo $fa->answer; ?>                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endif; ?>


                            </div>
                        </div>

                        <!-- Event FAQ'S Tab End Here -->

                        <!-- Event Review And Ratings Tab Start Here -->


                        <div class="tab-pane fade in" id="user_ratings">
                            <div class="col-md-12">
                                <div>
                                    <h4>User Reviews</h4>
                                    <?php
                                    if (count($user_review) == 0) {
                                        echo "There are no reviews for this event.";
                                    } else {
                                        ?>
                                        <?php if (count($user_review) > 0): ?>
                                            <?php foreach ($user_review as $ur): ?>
                                                <p>
                                                    <span class="item" style="padding: 15px;">
                                                        <h5>
                                                            <i class="icon-user"></i>&nbsp;&nbsp;<strong><?php echo $ur->first_name; ?>&nbsp;&nbsp;<?php echo $ur->last_name; ?></strong>
                                                            <small>on <em><?php echo $ur->updated_date; ?></em></small>
                                                            <ul class="list-inline product-ratings">
                                                                <?php
                                                                $rat = $ur->ratings;
                                                                for ($i = 0; $i < $rat; $i++) {
                                                                    echo '<li><i class = "rating-selected fa fa-star"></i></li>';
                                                                }

                                                                for ($j = $rat; $j < 5; $j++) {
                                                                    echo '<li><i class = "rating fa fa-star"></i></li>';
                                                                }
                                                                ?>
                                                            </ul>
                                                        </h5>
                                                    </span>
                                                </p>
                                                <p>
                                                    <span class="item">
                                                        <i class="fa fa-bars"></i>&nbsp;&nbsp;<?php echo $ur->review; ?>
                                                    </span>
                                                </p>
                                            <?php endforeach; ?>
                                            <?php
                                        endif;
                                    }
                                    ?>

                                </div>
                                <hr/>
                                <div>
                                    <h4>Write a review</h4>
                                    <hr>
                                    <form>
                                        <div class="col-md-10">
                                            <div>
                                                Your Review:
                                            </div>
                                            <br>
                                            <div>
                                                <textarea  id="review" class="form-control" rows="4" cols="100"></textarea>
                                            </div>

                                            <p><span><font color="red">Note:</font></span> HTML is not translated! </p>
                                        </div>

                                        <div class="col-md-10">
                                            <div>
                                                <h5>Rating:&nbsp; Bad &nbsp;<input class="radio1" name="rating" value="1" type="radio"/>&nbsp;&nbsp;<input class="radio1"  name="rating" value="2" type="radio"/>&nbsp;&nbsp;<input name="rating" class="radio1" value="3" type="radio"/>&nbsp;&nbsp;<input name="rating" class="radio1" value="4" type="radio"/>&nbsp;&nbsp;<input class="radio1" name="rating" value="5" type="radio"/>&nbsp; Good</h5>
                                            </div>
                                        </div>
                                        <div class="col-md-10">
                                            <a href="javascript:void(0);" style="height: 40px; width: 106px; padding-top: 7px;" onclick="javascript:saveUserReview(<?php echo $event_id ?>,<?php echo ((isset($_SESSION["customer_id"]) AND $_SESSION["customer_id"] != 0) ? $_SESSION["customer_id"] : 'null'); ?>);" id="save_review_data"  class="btn-u btn-u-sea-shop btn-u-lg">Submit</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- Event Review And Ratings Tab End Here -->

                        <!-- Event Terms And Conditions Tab Start Here -->
                        <?php if (count($event_terms_and_conditions) >= 1): ?>
                            <div class="tab-pane fade in" id="terms-and-condition">
                                <div class="shadow-wrapper margin-bottom-60">
                                    <div class="tag-box tag-box-v1 box-shadow shadow-effect-2">
                                        <p><?php echo html_entity_decode(html_entity_decode($event_terms_and_conditions{0}->event_terms_and_conditions_description, ENT_COMPAT, 'UTF-8')); ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <!-- Event Terms And Conditions Tab End Here -->
                    </div>
                </div>
                <!-- Tab Part End Here -->
                <!-- Venue Google Map -->
                <div class="clearfix"></div>
                <div style="height: 10px;"></div>
                <div class="row">
                    <div class="col-md-10">
                        <div id="map-canvas" style="height: 300px;width: 1150px;"></div>
                    </div>
                </div>

            </div>
            <!--=== Content Medium ===-->
            <div class="content-md container" style="padding-top: 0px;">
                <!--=== Product Service ===-->
                <div class="row margin-bottom-60">
                    <div class="col-md-4 product-service md-margin-bottom-30" >
                        <div class="product-service-heading">
                            <i class="fa fa-truck"></i>
                        </div>
                        <div class="product-service-in" style="height: 145px;">
                            <h3>Home Delivery</h3>
                            <p>Additional BDT 50 (within Dhaka City)/ BDT 70 (Outside Dhaka City) as home delivery fee.</p>
                            <!--                            <a href="#">+Read More</a>-->
                        </div>
                    </div>
                    <div class="col-md-4 product-service md-margin-bottom-30">
                        <div class="product-service-heading">
                            <i class="icon-earphones-alt"></i>
                        </div>
                        <div class="product-service-in" style="height: 145px;">
                            <h3>Customer Service</h3>
                            <p> HOT LINE: (+8801971842538). OFFICE HOURS: 10 AM TO 06 PM.OFFICE DAY: SATURDAY TO THURSDAY </p>

                            <!--                            <a href="#">+Read More</a>-->
                        </div>
                    </div>
                    <div class="col-md-4 product-service">
                        <div class="product-service-heading">
                            <i class="icon-refresh"></i>
                        </div>
                        <div class="product-service-in" style="height: 145px;">
                            <h3>Online Payment</h3>
                            <p>4% amount for the online payment as service and handling charge will be charged</p>
                            <!--                            <a href="#">+Read More</a>-->
                        </div>
                    </div>
                </div><!--/end row-->
                <!--=== End Product Service ===-->


            </div><!--/end container-->    
            <!--=== End Content Medium ===-->

        </div>


        <?php include './newsletter.php'; ?>
        <?php include './footer.php'; ?>
    </div>

    <script>

        function showCategoryDiv() {
            $('#divCategory').show();
            $('#divSubscribe').hide();
        }
        
        
        function showSubscribeDiv() {
            $('#divCategory').hide();
            $('#divSubscribe').show();
        }



        function submitInfo() {

            var program = $('#program_id').val();
            var batch = $('#batch_id').val();
            var profession = $('#profession').val();
            var designation = $('#designation').val();
            var organization = $('#organization').val();
            var CA_Photo =$("#profile_photo_text").val();
            var blood = $('#blood').val();

            if (blood == "") {
                $("#blood").addClass("input-error");
            } else {
                $("#blood").removeClass("input-error");
            }
            
            if(CA_Photo ==="" || CA_Photo ===  null){
                CA_Photo= "no_image.jpg";
            }

            if (profession == "") {
                $("#profession").addClass("input-error");
            } else {
                $("#profession").removeClass("input-error");
            }

            if (program == 0) {
                $("#program_id").addClass("input-error");
            } else {
                $("#program_id").removeClass("input-error");
            }

            if (batch == 0) {
                $("#batch_id").addClass("input-error");
            } else {
                $("#batch_id").removeClass("input-error");
            }


            if (organization == "") {
                $("#organization").addClass("input-error");
            } else {
                $("#organization").removeClass("input-error");
            }


            if (organization !== "" && batch > 0 && program > 0 && profession !== "" && blood !== "") {
                $.ajax({
                    type: "POST",
                    url: "ajax/ajaxValidateInfo.php",
                    dataType: "json",
                    data: {
                        CA_program_id: program,
                        CA_batch_id: batch,
                        CA_profession: profession,
                        CA_designation: designation,
                        CA_organization: organization,
                        CA_blood_grp: blood,
                        CA_Photo:CA_Photo
                    },
                    success: function (response) {
                        var obj = response;
                        // dropDownBox(obj.output, obj.msg);
                        if (obj.output === "success") {

                            dropDownBox(obj.output, obj.msg)
                            var ticktHtml = '';

                            if (obj.objectTickt) {

                                ticktHtml += '<div class="col-md-6 col-sm-6">';
                                ticktHtml += '<div class="service-block service-block-u">';
                                ticktHtml += '<i class="icon-custom icon-color-light rounded-x fa fa-thumbs-o-up"></i>';
                                ticktHtml += '<p>Launching Ceremony Ticket of DUDSAA</p>';
                                ticktHtml += '<a style="margin: 10px 0px;" href="javascript:void(0);" onclick="javascript:showCategoryDiv();" class="btn-u btn-u-orange">Buy Ticket</a>';
                                ticktHtml += '<p style="color: black"><strong><em>** Not Applicable for Life Members</em></strong></p>';
                                ticktHtml += '<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>';
                                ticktHtml += '</div>';
                                ticktHtml += '</div>';
                                ticktHtml += '<div class="col-md-6 col-sm-6">';
                                ticktHtml += '<div class="service-block service-block-u">';
                                ticktHtml += '<i class="icon-custom icon-color-light rounded-x fa fa-thumbs-o-up"></i>';
                                ticktHtml += '<p>Be an honored Life Member</p>';
                                ticktHtml += '<a style="margin: 10px 0px;" onclick="javascript:showSubscribeDiv();" href="javascript:void(0);" class="btn-u btn-u-orange">Subscribe for Lifetime</a>';
                                ticktHtml += '<p style="color: black; font-size: small;"><strong><em>**Discounted subscription fee is 10,000 BDT (Only for event)</em></strong></p>';
                                ticktHtml += '<p style="color: black; font-size: small;"><strong><em>**Regular subscription fee is 25,000 BDT</em></strong></p>';
                                ticktHtml += '</div>';
                                ticktHtml += '</div>';
                            } else {
                                ticktHtml += '<div class="col-md-6 col-sm-6">';
                                ticktHtml += '<div class="service-block service-block-u">';
                                ticktHtml += '<i class="icon-custom icon-color-light rounded-x fa fa-thumbs-o-up"></i>';
                                ticktHtml += '<h2 class="heading-md">Lunching Ceremony Ticket</h2>';
                                ticktHtml += '<p>Annual Subscription: BDT. </p>';
                                ticktHtml += '</div>';
                                ticktHtml += '</div>';
                            }
                            $('#divValidation').html(ticktHtml);
                        } else {
                            dropDownBox(obj.output, obj.msg);
                        }

                    }
                });
            }
        }

    </script>



    <script>
        function generateBatch(id) {
            $.ajax({
                type: "POST",
                url: "ajax/ajaxGetBatch.php",
                dataType: "json",
                data: {
                    id: id
                },
                success: function (response) {
                    var obj = response;
                    // dropDownBox(obj.output, obj.msg);
                    if (obj.output === "success") {
                        var batchHtml = '';

                        if (obj.batchObj.length > 0) {
                            batchHtml += '<select class="form-control" id="batch_id"  name="batch_id">';
                            batchHtml += '<option value="0">Select Batch</option>';
                            for (var i = 0; i < obj.batchObj.length; i++) {
                                batchHtml += '<option value="' + obj.batchObj[i].batch_id + '">' + obj.batchObj[i].batch_name + '</option>';
                            }
                            batchHtml += '</select>';
                        } else {
                            batchHtml += '<select class="form-control" id="batch_id"  name="batch_id" style="width: 400px;">';
                            batchHtml += '<option value="0">Select Program First</option>';
                            batchHtml += '</select>';
                        }
                        $('#batch_id').replaceWith(batchHtml);
                    } else {
                        dropDownBox(obj.output, obj.msg);
                    }

                }
            });
        }

    </script>



    <?php //include './footer_script.php';          ?>

    <!-- Event Type Quantity Script Start -->
    <script type="text/javascript">
        function subtractQty(eventID, limit) {
            var quantity = parseInt($("#txt_type_quantity_" + eventID).val());
            var newQuantity = quantity - 1;
            if (newQuantity != 0) {
                $("#txt_type_quantity_" + eventID).val(newQuantity);
            }
        }

        function addtionQty(eventID, limit) {
            var quantity = parseInt($("#txt_type_quantity_" + eventID).val());
            var newQuantity = quantity + 1;
            if (quantity < limit) {
                $("#txt_type_quantity_" + eventID).val(newQuantity);
            }
        }

    </script>
    <!-- Event Type Quantity Script End -->


    <!-- Event Includes Quantity Script Start -->
    <script type="text/javascript">
        function subtractIncludesQty(eventID, limit) {
            var quantity = parseInt($("#txt_include_quantity_" + eventID).val());
            var newQuantity = quantity - 1;
            if (newQuantity != 0) {
                $("#txt_include_quantity_" + eventID).val(newQuantity);
            }
        }

        function addtionIncludesQty(eventID, limit) {
            var quantity = parseInt($("#txt_include_quantity_" + eventID).val());
            var newQuantity = quantity + 1;
            if (quantity < limit) {
                $("#txt_include_quantity_" + eventID).val(newQuantity);
            }
        }

    </script>
    <!-- Event Includes Quantity Script End -->

</body>
</html> 
