<div id="login_window" style="display: none;" >
    <div class="col-md-10">
        <form style="width: 460px;" id="sky-form1" class="log-reg-block sky-form">
            <section>
                <label class="input login-input" style="margin-left: 6px;margin-right: 6px;padding-top: 10px;">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input id="login_email"  type="email" placeholder="Email Address" name="login_email" class="form-control">
                    </div>
                    <span id="login_email_invalid"></span>
                </label>
            </section>        
            <section>
                <label class="input login-input no-border-top" style="margin-left: 6px;margin-right: 6px;">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input id="login_password" type="password" placeholder="Password" name="login_password" class="form-control">
                    </div>
                    <span id="login_password_invalid"></span>
                </label>
            </section>
            <div class="row margin-bottom-5" style="margin-left: -9px;">
                <div class="col-xs-5">
                    <label class="checkbox">
                        <input type="checkbox" name="checkbox"/>
                        <i></i>
                        Remember me
                    </label>
                </div>
                <div class="col-xs-5 text-right">
                    <a href="#">Forget your Password?</a>
                </div>
            </div>
            <button onclick="return false;" id="click_login" style="width: 422px;margin-top: 10px;margin-left: 6px;margin-right: 6px;" class="btn-u btn-u-sea-shop btn-block margin-bottom-20">Log in</button>

            <div class="text-center bold_brown_text">
                <h3>OR</h3>
            </div>

            <div class="row columns-space-removes" style="width: 483px; padding-top: 12px;margin-left: -9px;">
                <div class="col-lg-5">
                    <button onclick="fb_login();" type="button" class="btn-u btn-u-md btn-u-fb btn-block"><i class="fa fa-facebook"></i>Facebook Login</button>
                </div>
<!--                <div class="col-lg-3">
                    <button type="button" class="btn-u btn-u-md btn-u-tw btn-block"><i class="fa fa-twitter"></i>Twitter Sign</button>
                </div>-->
                <div class="col-lg-5">
                    <button onclick="googleLogin();" type="button" style="background-color: #dd4b39;margin-left: -10px;" class="btn-u btn-u-md btn-u-tw btn-block"><i class="fa fa-google-plus"></i>Google Login</button>
                </div>
            </div>
        </form>

        <div class="margin-bottom-20"></div>
        <p class="text-center">Don't have account yet? Learn more and <a href="javascript:void(0);" id="register_form_from_login">Sign Up</a></p>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery("#register_form_from_login").click(function () {
                    jQuery(".k-i-close").click();
                    jQuery("#register_window").kendoWindow({
                        width: "530px",
                        height: "auto",
                        visible: false,
                        modal: true,
                        title: "Create New Account"
                    }).data("kendoWindow").center().open();
                });
            });
        </script>
    </div>
</div>