<?php
include("./admin/config/class.web.config.php");
$con = new Config();

if ($con->checkUserLogin() == TRUE) {
    $con->redirect('customer_dashboard.php');
}

$login_email = '';
$signup_email = '';
$login_password = '';
$signup_password = '';
$fname = '';
$lname = '';
$param = FALSE;


if (isset($_GET['param']) AND $_GET['param'] == "true") {
    $param = TRUE;
}

if (isset($_POST['signin'])) {
    extract($_POST);
}
if (isset($_POST['signup'])) {
    echo "signup";
}
?>
<?php include './header_script.php'; ?>
<body class="header-fixed">

    <div class="wrapper">
        <div class="header-v5 header-static">
            <?php include './menu_top.php'; ?>
            <?php include './header.php'; ?>
        </div>
        <!--=== Breadcrumbs v4 ===-->

        <!--=== End Breadcrumbs v4 ===-->

        <!--=== Content Medium Part ===-->
        <div class="content-md margin-bottom-30">
            <div class="container">
                <form class="shopping-cart">
                    <div>

                        <section class="billing-info">
                            <div class="alert alert-warning fade in">
                                <strong>Oopss!</strong> It seems you are not logged in. Please fill below form to proceed further.
                            </div>


                            <div class="row" style="margin-bottom: 60px;">
                                <div class="col-md-5 md-margin-bottom-40">
                                    <h2 class="title-type"><strong>Sign in</strong></h2>
                                    <div class="billing-info-inputs checkbox-list">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input id="loginEmail" type="text" placeholder="Email" name="login_email" class="form-control">
                                                <span id="login_email_invalid"></span>
                                            </div>
                                            <div class="col-sm-12">
                                                <input id="loginPass" type="password" placeholder="Password" name="login_password" class="form-control">
                                            </div>
                                        </div>
                                        <div class="button-actions clearfix text-right" style="padding-top: 50px !important;">
                                            <button onclick="javascript:btnSignin();" type="button" style="padding: 15px; font-size: medium;" class="btn-u btn-u-sea-shop btn-u-lg text-right">Sign In&nbsp;&nbsp;<i style="color: white;" class="fa  fa-check-square-o"></i></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2 md-margin-bottom-40 text-center" style="margin-top: 80px;"> <h1>OR</h1></div>

                                <div class="col-md-5 md-margin-bottom-40">
                                    <h2 class="title-type"><strong>Sign up</strong></h2>
                                    <div class="billing-info-inputs checkbox-list">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <input id="signupFname" type="text" placeholder="First Name" name="signup_fname" class="form-control">
                                                <input id="signupEmail" type="text" placeholder="Email" name="signup_email" class="form-control">
                                                <span id="signup_email_invalid"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <input id="signupLname" type="text" placeholder="Last Name" name="signup_lname" class="form-control">
                                                <input id="signupPassword" type="password" placeholder="Password" name="signup_password" class="form-control">
                                            </div>
                                        </div>
                                        <div class="button-actions clearfix text-right" style="padding-top: 50px !important;">
                                            <button onclick="javascript:btnSignup();" type="button" style="padding: 15px; font-size: medium;" class="btn-u btn-u-sea-shop btn-u-lg text-right">Sign Up&nbsp;&nbsp;<i style="color: white;" class="fa  fa-check-square-o"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                    </div>
                </form>
            </div><!--/end container-->

        </div>

        <!--=== End Content Medium Part ===-->     

        <!--=== Shop Suvbscribe ===-->
        <div class="shop-subscribe">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 md-margin-bottom-20">
                        <h2>subscribe to our weekly <strong>newsletter</strong></h2>
                    </div>  
                    <div class="col-md-4">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Email your email...">
                            <span class="input-group-btn">
                                <button class="btn" type="button"><i class="fa fa-envelope-o"></i></button>
                            </span>
                        </div>    
                    </div>
                </div>
            </div><!--/end container-->
        </div>
        <!--=== End Shop Suvbscribe ===-->

        <?php include './footer.php'; ?>
    </div><!--/wrapper-->

    <script>

        function btnSignin() {
            
            var email = $('#loginEmail').val();
            var password = $('#loginPass').val();
            var param = '<?php echo (isset($_GET['param'])) ? $_GET['param'] : "false"; ?>';
            
            if (email == "") {
                $("#loginEmail").addClass("input-error");
            } else {
                $("#loginEmail").removeClass("input-error");
            }

            if (email != "" && validateEmail(email) != true) {
                $("#loginEmail").addClass("input-error");
                $("#login_email_invalid").html('<em style="color:red;margin-left:1px;" id="email_invalid" for="email">Incorrect Email format.</em>');
            } else {
                $("#login_email_invalid").html('');
            }


            if (password == "") {
                $("#loginPass").addClass("input-error");
            } else {
                $("#loginPass").removeClass("input-error");
            }
            
            $.ajax({
                type: "POST",
                url: "ajax/customer_login_check.php",
                dataType: "json",
                data: {
                    email: email,
                    password: password
                },
                success: function (response) {
                    var obj = response;
                    // dropDownBox(obj.output, obj.msg);
                    if (obj.output === "success") {
                        $('#register_form').replaceWith('<a href="javascript:void();" onclick="javascript:userLogout();">Logout</a>');
                        $('#login_form').replaceWith('<a href="customer_dashboard.php">' + obj.first_name + '</a>');
                        dropDownBox(obj.output, obj.msg);
//                        $("#login_form").text(obj.first_name);
                            if (param == 'checkout') {
                                setTimeout(function () {
                                    location.href = "checkout.php";
                                }, 1200);
                            } else if(param == 'donate'){
                                setTimeout(function () {
                                    location.href = "donate.php";
                                }, 1200);
                            } else {
                                setTimeout(function () {
                                    location.href = "customer_dashboard.php";
                                }, 1200);
                            }
                    } else {
                        dropDownBox(obj.output, obj.msg);
                    }

                }
            });
        }



        function btnSignup() {

            var fname = $('#signupFname').val();
            var lname = $('#signupLname').val();
            var email = $('#signupEmail').val();
            var password = $('#signupPassword').val();
            var param = '<?php echo (isset($_GET['param'])) ? $_GET['param'] : "false"; ?>';

            if (fname == "") {
                $("#signupFname").addClass("input-error");
            } else {
                $("#signupFname").removeClass("input-error");
            }


            if (lname == "") {
                $("#signupLname").addClass("input-error");
            } else {
                $("#signupLname").removeClass("input-error");
            }


            if (email == "") {
                $("#signupEmail").addClass("input-error");
            } else {
                $("#signupEmail").removeClass("input-error");
            }

            if (email != "" && validateEmail(email) != true) {
                $("#signupEmail").addClass("input-error");
                $("#signup_email_invalid").html('<em style="color:red;margin-left:1px;" id="email_invalid" for="email">Incorrect Email format.</em>');
            } else {
                $("#signup_email_invalid").html('');
            }


            if (password == "") {
                $("#signupPassword").addClass("input-error");
            } else {
                $("#signupPassword").removeClass("input-error");
            }


            if (fname != "" && lname != "" && email != "" && password != "" && validateEmail(email) == true) {
                $.ajax({
                    type: "POST",
                    url: "ajax/save_registration_data.php",
                    dataType: "json",
                    data: {
                        first_name: fname,
                        last_name: lname,
                        email: email,
                        password: password
                                //gender: gender,
                                //dob: dob
                    },
                    success: function (response) {
                        var obj = response;
                        console.log(obj);
                        if (obj.output === "success") {
                            $('#register_form').replaceWith('<a href="javascript:void();" onclick="javascript:userLogout();">Logout</a>');
                            $('#login_form').replaceWith('<a href="customer_dashboard.php">' + obj.first_name + '</a>');
                            dropDownBox(obj.output, obj.msg);
                            if (param == 'checkout') {
                                setTimeout(function () {
                                    location.href = "checkout.php";
                                }, 1200);
                            } else if(param == 'donate'){
                                setTimeout(function () {
                                    location.href = "donate.php";
                                }, 1200);
                            } else {
                                setTimeout(function () {
                                    location.href = "customer_dashboard.php";
                                }, 1200);
                            }
                            //                        $("#login_form").text(obj.first_name);
                        } else {
                            dropDownBox(obj.output, obj.msg);
                        }
                    }
                });
            }
        }

    </script>
</body>
</html> 