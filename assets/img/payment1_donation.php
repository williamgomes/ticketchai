<?php
session_start();
include("admin/config/class.web.config.php");
require 'admin/lib/PHPMailer/PHPMailerAutoload.php';
//** smtp configuration **/
//Create a new PHPMailer instance
$mail = new PHPMailer();
//Tell PHPMailer to use SMTP
//$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
//$mail->SMTPDebug = 0;
//Ask for HTML-friendly debug output
//$mail->Debugoutput = 'html';
//Set the hostname of the mail server
//$mail->Host = "smtpout.asia.secureserver.net"; //"mail.example.com";
//Set the SMTP port number - likely to be 25, 465 or 587
//$mail->Port = 25;
//Whether to use SMTP authentication
//$mail->SMTPAuth = true;
//Username to use for SMTP authentication
//$mail->Username = "support@ticketchai.com";
//Password to use for SMTP authentication
//$mail->Password = "Support@2014";
//Set who the message is to be sent from
$mail->setFrom('support@ticketchai.com', 'Ticket Chai');
//Set an alternative reply-to address
$mail->addReplyTo('support@ticketchai.com', 'Ticket Chai');
//** smtp configuration **/
$con = new Config();
$page_id = 10;
if (!isset($_SESSION["customer_email_address"])) {
    $con->redirect("login.php");
}
$event_id = '';
$college_id = '';
$year_id = '';
$err = '';
$buyDetails = array();
$ticket_price = '';
$ticket_limit = '';
$first_name = '';
$last_name = '';
$email_address = '';
$schedule_id = '';
$ticket_id = '';
$ticket_total = "";
$ticket_discount = '';
$customer_address = '';
$mobile_number = '';
$post_code = '';
$ticket_type_id = '';
$student_number = '';
$year_name = '';
$city = '';
$country = '';
$dis_state_id = '';
$zip_code = '';
$country_id = '';
$delivery_cost_id = '';
$delivery_cost = '';
$city_id = '';
$batch_id = '';
$donation_amount = '';
$customer_id = '';

$batches = array();

$pick_points = $con->SelectAll("pick_point", "", "", "array");
$pick_point_id = '';
$collegess = $con->SelectAll("college", "", "", "array");
$years = $con->SelectAll("year", "", "", "array");
$ticket_categories = array();
$district_name = $con->SelectAll("district_state", "", "", "array");
$countryName = $con->SelectAll("country", "", "", "array");

//$con->debug($years);
if (isset($_GET["event_id"]) && isset($_GET["schedule_id"]) && isset($_SESSION["customer_email_address"])) {

    //$con->debug($_SESSION["purchase_info"]);
    $customer_infos = $con->SelectAll("customer", "", array("email_address" => $_SESSION["customer_email_address"]), "array");
    $customer_id = $customer_infos{0}->customer_id;
    //$con->debug($customer_infos);
    $first_name = $customer_infos{0}->first_name;
    $last_name = $customer_infos{0}->last_name;
    $email_address = $customer_infos{0}->email_address;
    $dis_state_id = $customer_infos{0}->district;
    $city = $customer_infos{0}->city;
    $zip_code = $customer_infos{0}->zip_postal_code;
    $country_id = $customer_infos{0}->country;
    $customer_address = $customer_infos{0}->address;
    $mobile_number = $customer_infos{0}->phone;
//    $delivery_costs = $con->SelectAll("delivery_cost", "", array("dis_state_id" => $dis_state_id), "array");
//    //$con->debug($delivery_cost);
//    $delivery_cost = $delivery_costs{0}->cost;
    $batches = $con->SelectAll("batch", "", "", "array");
    //$con->debug($customer_infos);
//    $con->debug($_SESSION["purchase_info"]);
//    $buyDetails = $_SESSION["purchase_info"];
    //$con->debug($buyDetails);
    //-------------- get ticket info -------------------------//
//    $ticket_id = $buyDetails["ticket_id"];
    $schedule_id = base64_decode($_GET["schedule_id"]);
    $event_id = base64_decode($_GET["event_id"]);
//    $ticket_type_id = $buyDetails["ticket_type_id"];
//    $ticket_categories = $con->SelectAll("ticket_type", "", array("event_id" => $event_id), "array");
//
//    $ticket_price = $tickets{0}->price;
//    $ticket_limit = $buyDetails["ticket_limit"];
//    $ticket_total = ($ticket_limit * $ticket_price);
    //--------------- end get ticket info -------------------//
    $events = $con->SelectAll("event", "", array("event_id" => $event_id), "array");
    $query_string_venue = "SELECT v.* FROM venue as v, event e where v.venue_id = e.venue_id AND e.event_id='$event_id'";
    $venues = $con->ReturnObjectByQuery($query_string_venue, "array");
    $schedules = $con->SelectAll("event_schedule", "", array("event_schedule_id" => $schedule_id), "array");
}
//if(isset($_POST["btnThankyou"])){
//    $con->redirect("thankyou.php");
//}
?>
<?php include './header_script.php'; ?>

<body  class="boxed-layout container">


    <div class="wrapper">
        <?php include './header.php'; ?>

        <!--=== Breadcrumbs ===-->
        <div class="breadcrumbs">
            <div class="container">
                <!--                    <h1 class="pull-left">Advanced Layout Forms</h1>-->
                <ul class="pull-left breadcrumb">
                    <li><a href="index.php">Event</a></li>
                    <li><a href="event_details.php">Event Details</a></li>
                    <li class="active">Donation</li>
                </ul>
            </div>
        </div><!--/breadcrumbs-->
        <!--=== End Breadcrumbs ===-->
        <div class="container content">

            <div class="row">
                <?php if (!empty($err)): ?>
                    <div class="alert alert-danger fade in alert-dismissable">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">&times;</button>
                        <strong>Sorry</strong>
                        <?php echo $err; ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <form method="post" id="sky-form" class="sky-form">
                        <p style="margin-left: 30px;margin-top: 15px; font-size: 16px;"><b>Contact Details</b></p>
                        <fieldset style="margin-top: -15px;">
                            <div class="row">
                                <section class="col col-6" style="pointer-events: none;">
                                    <input type="hidden" id="customer_id" value="<?php echo $customer_id; ?>" />
                                    <label class="input">
                                        <i class="icon-prepend fa fa-user"></i>
                                        <input type="text" value="<?php echo $first_name; ?>" name="first_name" placeholder="First name" >
                                    </label>
                                </section>
                                <section class="col col-6" style="pointer-events: none;">
                                    <label class="input">
                                        <i class="icon-prepend fa fa-user"></i>
                                        <input type="text" value="<?php echo $last_name; ?>" name="last_name" placeholder="Last name" >
                                    </label>
                                </section>
                            </div>

                            <div class="row">
                                <section class="col col-6" style="pointer-events: none;">
                                    <label class="input">
                                        <i class="icon-prepend fa fa-envelope"></i>
                                        <input type="email" value="<?php echo $email_address; ?>" id="email_address" name="email_address" placeholder="E-mail" >
                                        <input type="hidden" id="dis_state_id_add" name="dis_state_id" value="<?php echo $dis_state_id ?>" />
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="input">
                                        <i class="icon-prepend fa fa-phone"></i>
                                        <input type="tel" id="mobile_number" name="mobile_number" value="<?php echo $mobile_number; ?>" placeholder="Phone">
                                    </label>
                                </section>
                            </div>

                        </fieldset>
                        <fieldset>
                            <div class="row">
                                <div class="col-md-12" style="font-size: 16px;"><b>Cadet Details</b></div>
                            </div>
                            <br/>
                            <div class="clearfix"></div>
                            <div class="row">

                                <section class="col col-3">
                                    <label>Cadet No.</label>

                                    <input class="form-control" id="student_number" type="text" name="student_number" value="<?php echo $student_number; ?>" placeholder="Cadet No">

                                </section>
                                <section class="col col-3">
                                    <label>Batch No.</label>
                                    <select class="form-control" id="batch_id"  name="batch_id">
                                        <option value="0">Select Batch</option>
                                        <?php if (count($batches) >= 1): ?>
                                            <?php foreach ($batches as $bc): ?>
                                                <option value="<?php echo $bc->batch_id; ?>" 

                                                        <?php
                                                        if ($bc->batch_id == $batch_id) {
                                                            echo " selected='selected'";
                                                        }
                                                        ?>

                                                        ><?php echo $bc->batch_name; ?></option>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                    </select>

<!--                                    <select class="form-control"  name="college">
                                        <option value="0">Select College</option>
                                    <?php //if (count($collegess) >= 1):  ?>
                                    <?php //foreach ($collegess as $col): ?>
                                                <option value="<?php //echo $col->college_id;                           ?>" 

                                    <?php
//                                                        if ($col->college_id == $college_id) {
//                                                            echo " selected='selected'";
//                                                        }
                                    ?>

                                                        ><?php //echo $col->college_name;                            ?></option>
                                    <?php //endforeach;  ?>
                                    <?php //endif; ?>
                                    </select>-->

                                </section>

                                <!-- Donation Amount Start -->
                                <section class="col col-3">
                                    <label>Donation Amount</label>

                                    <input style="display: none;" class="form-control" id="donation_amount" type="text" name="donation_amount" value="<?php echo $donation_amount; ?>" placeholder="Donation Amount">

                                </section>
                                <!-- Donation Amount End -->
<!--                                <section class="col col-3">-->

<!--                                    <select class="form-control" name="year_name">
                                        <option value="0" selected disabled>Passing Out Year</option>-->
                                <?php //if (count($years) >= 1):  ?>
                                <?php //foreach ($years as $y): ?>
                                    <!--<option value="<?php //echo $y->year_name;                           ?>"-->

                                <?php
//                                                        if ($y->year_name == $year_name) {
//                                                            echo " selected='selected'";
//                                                        }
                                ?>

<!--                                            ><?php //echo $y->year_name;                            ?></option>-->
                                <?php //endforeach;  ?>
                                <?php //endif; ?>
                                <!--                                    </select>-->

                                <!--</section>-->


                            </div>
                            <div>

                                <div class="servive-block servive-block-red" id="divSubscribe" style="color: white;font-weight: bold;font-size: 16px;display: none;">


                                </div>

                            </div>
                        </fieldset>

                        <fieldset>
                            <p style="margin-left: 2px;margin-top: -15px; font-size: 16px;"><b>Select Delivery Method</b></p>
                            <!-- Tab v3 -->                
                            <div class="row tab-v3">
                                <div class="col-sm-3">
                                    <ul class="nav nav-pills nav-stacked"> 
                                        <li><a href="#event-1" id="pick_on_spot" data-toggle="tab">Pick From Office</a></li>
                                        <li><a href="#home-1" id="home_delivery" data-toggle="tab">Cash On Delivery</a></li>
                                        <li><a href="#ssl-payment" id="sslpayment" data-toggle="tab">Online Payment</a></li>
                                    </ul>                    
                                </div>

                                <div class="col-sm-9">
                                    <div class="tab-content">
                                        <div class="row" id="selection_div">
                                            <div class="servive-block servive-block-red">
                                                <div><b style="color: white;font-size: 16px;">Select Delivery method</b></div>
                                            </div>
                                        </div>
                                        <!--                                        <div class="tab-pane fade in" id="office-1">
                                                                                    <section>
                                                                                        <label class="checkbox"><input type="checkbox" value="pick_from_office" name="pick_from_office"><i></i>To collect your ticket you must <strong>agree Terms and Conditions.</strong></label>
                                                                                    </section>
                                        
                                                                                </div> -->

                                        <div class="tab-pane fade in" id="event-1">
                                            <div class="row">
                                                <div class="col-md-12">  You can collect your e-ticket from <b>Ticket Chai office.</b> </div>

                                            </div>
                                            <div class="clearfix"></div>
                                            <br>
                                            <div class="row">
                                                <section class="col col-12">
                                                    <p><b>Ticket Chai Office Address: </b>Razzak Plaza (8th Floor),1 New Eskaton Road,Moghbazar Circle, Dhaka-1217.</p>

                                                    <?php // if (count($pick_points) >= 1): ?>
                                                    <?php //foreach ($pick_points as $pp): ?>


                                                    <?php // echo $pp->pick_point_name . " ," ?>
                                                    <?php // endforeach; ?>
                                                    <?php //endif; ?>
                                                    <!--                                                    <label class="select">
                                                                                                            <select style="width: 100%" name="pick_point_id">
                                                                                                                <option value="0">Select Point Name</option>
                                                    <?php //if (count($pick_points) >= 1):         ?>
                                                    <?php //foreach ($pick_points as $pp):       ?>
                                                                                                                        <option value="<?php //echo $pp->pick_point_id;                                                             ?>" 
                                                    <?php
//if ($pp->pick_point_id == $pick_point_id) {
//echo "selected='selected'";
//}
                                                    ?>
                                                                                                                                ><?php //echo $pp->pick_point_name;                                                        ?></option>
                                                    <?php //endforeach;     ?>
                                                    <?php //endif;   ?>
                                                                                                            </select>
                                                                                                        </label>-->
                                                </section>
                                            </div>
                                            <div class="row">


                                                <div class="col-md-10">
                                                    <div style="border:1px solid gainsboro;height: 100px;width: 100%;overflow: scroll;">
                                                        <?php echo html_entity_decode(html_entity_decode($events{0}->event_terms_and_conditions)); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-1"></div>
                                            </div>
                                            <section>
                                                <label class="checkbox"><input value="pick_from_venue" type="checkbox" id="chkPrickTC" name="pick_from_venue"><i></i>To complete purchase you must agree <strong> Terms and Conditions.</strong></label>
                                            </section>

                                        </div>


                                        <div class="tab-pane fade in" id="home-1">
                                            <div class="row">
                                                <div class="servive-block servive-block-red">
                                                    <div><b style="color: white;font-size: 16px;">Cash On Delivery is available only for below locations:</b>  </div>
                                                    <p style="font-size: 14px;"> Dhaka, Savar, Narayanganj, Gazipur,Chittagong, Sylhet, Khulna, Rajshahi, Comilla</p>

                                                </div>
                                            </div>


                                            <div class="row">

                                                <div id="dcalc">
                                                    <div class="col-md-12">
                                                        <div class="col-md-7">
                                                            Shipping and handling charge: BDT <input type="hidden" id="delivery_cost_1" name="delivery_cost" value="<?php echo $delivery_cost; ?>" /><span id="delivery_cost"> <?php echo $delivery_cost; ?> </span>
                                                        </div>

                                                        <div class="clearfix"></div>
                                                        <br/>

                                                        <section class="col-md-12">
                                                            <!--                                                        <div>Address</div>-->
                                                            <label for="file" class="input">
                                                                <input type="text" id="customer_address" name="customer_address" value="<?php echo $customer_address; ?>" placeholder="Customer Address" style="width:100%;">
                                                            </label>
                                                        </section>
    <!--                                                    <section class="col col-3">
                                                             <label class="input">
                                                                <input type="text" name="post_code" value="<?php //echo $post_code;                                                                 ?>" placeholder="Post code" >
                                                            </label>
                                                        </section>  -->
                                                    </div>

                                                    <div class="col-md-12">
                                                        <section class="col-md-6">
                                                            <!--                                                        <div>Address</div>-->
                                                            <label for="file" class="input">
                                                                <input type="text" id="city" name="city" value="<?php echo $city; ?>" placeholder="City">
                                                            </label>
                                                        </section>

                                                        <section class="col-md-6">

                                                            <select class="form-control" style="width: 100%"  id="dis_state_id" name="dis_state_id">
                                                                <option value="0">Select District</option>
                                                                <?php if (count($district_name) >= 1): ?>
                                                                    <?php foreach ($district_name as $dt): ?>
                                                                        <option value="<?php echo $dt->dis_state_id; ?>" 

                                                                                ><?php echo $dt->dis_state_title; ?></option>
                                                                            <?php endforeach; ?>
                                                                        <?php endif; ?>
                                                            </select>


                                                        </section>

                                                    </div>

                                                    <div class="col-md-12">

                                                        <section class="col-md-6">
                                                            <!-- <div>Address</div>-->
                                                            <label for="file" class="input">
                                                                <input type="text" id="zip_code" name="zip_code" value="<?php echo $zip_code; ?>" placeholder="Zip Code">
                                                            </label>
                                                        </section>

                                                        <section class="col-md-6">

                                                            <select class="form-control" style="width: 100%" id="country_type" name="country_id">
                                                                <option value="0">Select Country</option>
                                                                <?php if (count($countryName) >= 1): ?>
                                                                    <?php foreach ($countryName as $dt): ?>
                                                                        <option value="<?php echo $dt->country_id; ?>" 
                                                                        <?php
                                                                        if ($dt->country_id == $country_id) {
                                                                            echo "selected='selected'";
                                                                        }
                                                                        ?>
                                                                                ><?php echo $dt->country_name; ?></option>
                                                                            <?php endforeach; ?>
                                                                        <?php endif; ?>
                                                            </select>


                                                        </section>

                                                    </div>

                                                </div>



                                                <div style="float: right;" class="col-md-3">
<!--                                                    <input type="submit" class="btn-u" name="btnEdit" value="Edit">-->

                                                </div>



                                                <div class="col-md-12" style="margin-left:15px;">
                                                    <div style="border:1px solid gainsboro; height: 100px; width: 94%; overflow: scroll; padding-top: 5px; padding-left: 5px; padding-right: 5px;">
                                                        <?php echo html_entity_decode(html_entity_decode($events{0}->event_terms_and_conditions)); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-1"></div>
                                                <div class="clearfix"></div>
                                                <br />
                                                <section class="col-md-12" style="margin-left: 15px;">
                                                    <label class="checkbox"><input type="checkbox" value="home_delivery"  id="chkkHomeDeliveryTC" name="home_delivery"><i></i>To complete purchase you must agree <strong>Terms and Conditions.</strong></label>
                                                </section>
                                            </div>
                                        </div>
                                        <footer style="text-align: right;display: none;" id="divProceed" >
                                            <a href="javascript:void(0);" class="btn-u" id="btnPickFromPoint" >Proceed</a>
                                            <a href="javascript:void(0);" class="btn-u" id="btnHomeDelivery">Proceed</a>

                                            <button type="submit" class="btn-u" id="btnThankyou" name="btnThankyou" style="display:none;">Proceed</button>
                                        </footer>

                                        </form>
                                        <div id="ssl-payment" class="tab-pane fade in">
                                            <div class="col-md-12"><img src="assets/img/paywith.png" style="width: 100%"></div>
                                            <div class="clearfix"></div>
                                            <br/>
<!--                                            <input type="radio" id="pick_on_online_spot" name="delivery_online_method[]" />Pick Point &nbsp;
                                            <input type="radio" id="online_home_delivery" name="delivery_online_method[]"/>Home Delivery &nbsp;-->
                                            <input type="hidden" id="online_delivery_method" name="online_delivery_method"/>
                                            <select class="form-control" style="width: 40%;display: none;"  id="dis_state_online_id" name="dis_state_id">
                                                <option value="0">Select District</option>
                                                <?php if (count($district_name) >= 1): ?>
                                                    <?php foreach ($district_name as $dt): ?>
                                                        <option value="<?php echo $dt->dis_state_id; ?>" 
                                                                ><?php echo $dt->dis_state_title; ?></option>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                            </select>
                                            <a style="color:white;" class="pull-right btn btn-success" href="javascript:void(0)" id="ssl_payment_x" >Pay Online</a> 

                                            <br/>
                                            <form action="https://www.sslcommerz.com.bd/process/index.php" method="post" name="form1">
                                                <input type="hidden" name="store_id" value="ticketchailive001"> 
                                                              <input type="hidden" id="total_amount_ssl" name="total_amount" value="">        
                                                              <input type="hidden" id="trans_id_ssl" name="tran_id" value="">          
                                                <input id="notify_url" type="hidden" name="success_url"
                                                       value="">  
                                                 <input type="hidden" id="fail_url" name="fail_url" value = "">
                                                 <input type="hidden" id="cancle_url" name="cancel_url" value = "">                      
                                                 <input class="pull-right" id="ssl_main" type="submit" value="Pay Online"  style="display: none;"  name="pay">
                                            </form>

                                            <!--<a href="javascript:void(0)" id="ssl_payment_x" >X Online Payment</a>--> 
                                        </div>
                                    </div>                                    
                                </div>
                            </div>            
                            <!-- Tab v3 --> 

                        </fieldset>

                </div>
                <div class="col-md-3">
                    <style>
                        table.table table-striped table-bordered td.custom_info_left{
                            width: 40%;
                        }
                        table.table table-striped table-bordered th.custom_info_left{
                            width: 40%;
                        }
                    </style>
                    <table class="table table-striped table-bordered">
                        <thead>
                        <th colspan="2" style="text-align: center;" class="custom_info_left">Ticket Summary</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="custom_info_left"><p> Event:</p></td>
                                <td><p> <?php echo $events{0}->event_title; ?></p></td>
                            </tr>
                            <tr>
                                <td class="custom_info_left"><p> Venue:</p></td>
                                <td><p> <?php echo $venues{0}->venue_name; ?></p></td>
                            </tr>
                            <tr>
                                <td class="custom_info_left"><p> Date : </p></td>
                                <td><p> <?php echo $schedules{0}->event_date; ?></p></td>
                            </tr>
                            <tr>
                                <td class="custom_info_left"><p> <b>Total</b> Tk. :</p></td>
                                <td><p> <span id="ticket_total_span"></span></p></td>
                            </tr>
                        </tbody>
                    </table>


                </div>
            </div>
        </div> <!--=== End Container ===-->

        <?php include './footer.php'; ?>
    </div><!--/End Wrapepr-->

    <!-- JS Global Compulsory -->           
    <script type="text/javascript" src="assets/plugins/jquery-1.10.2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {



            //start student number change function //
            $("#student_number").keyup(function() {
                var batch_id = $("#batch_id option:selected").val();
                var student_number = $("#student_number").val();
                var college_id = 7;

                $.ajax({
                    type: "POST",
                    url: "check_cadet_batch_ajax.php",
                    dataType: "json",
                    data: {student_number: student_number, batch_id: batch_id, college_id: college_id},
                    success: function(response) {
                        console.log(response);

                        var obj = eval(response);
                        if (obj.output === "error") {
                            $("#divSubscribe").fadeIn();
                            $("#divSubscribe").html(obj.msg);
                            $("#donation_amount").fadeOut();
                            $("#donation_amount").val("");
                        } else {
                            $("#divSubscribe").fadeIn();
                            $("#divSubscribe").html("You Can Donate For This Event");
                            $("#donation_amount").fadeIn();
                        }
                    },
                    error: function() {

                    }
                });
            });
            //end student number change function //

            // Start Batch Change Event //
            $("#batch_id").change(function() {
                var batch_id = $("#batch_id option:selected").val();
                var student_number = $("#student_number").val();
                var college_id = 7;

                $.ajax({
                    type: "POST",
                    url: "check_cadet_batch_ajax.php",
                    dataType: "json",
                    data: {student_number: student_number, batch_id: batch_id, college_id: college_id},
                    success: function(response) {

                        console.log(response);
                        var obj = eval(response);
                        if (obj.output === "error") {
                            $("#divSubscribe").fadeIn();
                            $("#divSubscribe").html(obj.msg);
                            $("#donation_amount").fadeOut();
                            $("#donation_amount").val("");

                        } else {
                            $("#divSubscribe").fadeIn();
                            $("#divSubscribe").html("You Can Donate For This Event");
                            $("#donation_amount").fadeIn();
                        }

                    },
                    error: function() {

                    }
                });

            });

            // End Batch Change Event //


            // start pick from point //
            $("#btnPickFromPoint").click(function() {

                var college_id = 7;
                var donation_amount = $("#donation_amount").val();
                var event_id = <?php echo $event_id; ?>;
                var schedule_id = <?php echo $schedule_id; ?>;
                var customer_id = $("#customer_id").val();
                var student_number = $("#student_number").val();
                var batch_id = $("#batch_id option:selected").val();
                var delivery_method = "pick_from_point";
                //var email_address = $("#email_address").val();

                var checkTC = "";

                if ($("#chkPrickTC").is(":checked")) {
                    checkTC = "1";
                }


                //$("#ticket_total_span").html(total_Price_For_ticket);



                if (student_number === "") {
                    alert("Please Give Cadet No");
                } else if (batch_id === "0") {
                    alert("Please Select Batch");
                } else if (donation_amount === "")
                {
                    alert("Enter Donation Amount");
                } else if (checkTC === "") {
                    alert("Select Terms and Conditions");
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: "save_donation_price_ajax.php",
                        dataType: "json",
                        data: {student_number: student_number, batch_id: batch_id, college_id: college_id, event_id: event_id, schedule_id: schedule_id, delivery_method: delivery_method, customer_id: customer_id, donation_amount: donation_amount},
                        success: function(response) {
                            console.log(response);

                            var obj = eval(response);
                            //console.log(obj.error_msg);

                            if (obj.output === "success") {

                                window.location = "thankyou_donate.php";

                            } else {
                                alert(obj.msg);
                            }

                            //;
                            //$("#btnThankyou").trigger('click');

                        },
                        error: function(a, b, c) {

                        }
                    });
                    //$("#btnThankyou").click();
                }
            });
            // end pick from point //

//start home delivery //

            $("#btnHomeDelivery").click(function() {

                var college_id = 7;
                var donation_amount = $("#donation_amount").val();
                var event_id = <?php echo $event_id; ?>;
                var schedule_id = <?php echo $schedule_id; ?>;
                var customer_id = $("#customer_id").val();
                var student_number = $("#student_number").val();
                var batch_id = $("#batch_id option:selected").val();
                var delivery_method = "pick_from_point";

                var email_address = $("#email_address").val();
                var subscription_type = $("#subscription_type").val();
                //console.log(email_address);
                var customer_address = $("#customer_address").val();
                var city = $("#city").val();

                var discrict_id = $("#dis_state_id option:selected").val();

                var zip_code = $("#zip_code").val();
                 var TCchecked = "";
                if ($("#chkkHomeDeliveryTC").is(":checked")) {
                    TCchecked = "1";
                }

                if (student_number === "") {
                    alert("Please Give Cadet No");
                } else if (batch_id === "0") {
                    alert("Please Select Batch");
                }
                else if (customer_address === "") {
                    alert("Customer Address Empty");
                }
                else if (discrict_id === "0") {
                    alert("Please Select District");
                }
                 else if (TCchecked === "") {
                    alert("Terms and condition is not selected");
                } else {
                    $.ajax({
                        type: "POST",
                        url: "home_delivery_save_donation_ajax.php",
                        dataType: "json",
                        data: {student_number: student_number, batch_id: batch_id, college_id: college_id, event_id: event_id, schedule_id: schedule_id, delivery_method: delivery_method,
                            customer_id: customer_id, donation_amount: donation_amount, customer_address: customer_address, city: city, discrict_id: discrict_id, zip_code: zip_code},
                        success: function(response) {

                            console.log(response);
                            var obj = eval(response);
                            //console.log(obj.error_msg);

                            if (obj.output !== "success") {
                                alert(obj.msg);
                            } else {
                                window.location = "thankyou_donate.php"
                            }

                        },
                        error: function(a, b, c) {

                        }
                    });
                }
            });

            // end home delivery //

            //SSL Payment Start Here //



            $("#ssl_payment_x").click(function() {

                var student_number = $("#student_number").val();
                var batch_id = $("#batch_id option:selected").val();
                var college_id = 7;
                var donation_amount = $("#donation_amount").val();
                var customer_id = $("#customer_id").val();
                var delivery_method = "online_payment";

                //$("#ticket_total_span").html(total_Price_For_ticket);

                var event_id = <?php echo $event_id; ?>;

                var schedule_id = <?php echo $schedule_id; ?>;

                if (student_number === "") {
                    alert("Please Give Cadet No");
                } else if (batch_id === "0") {
                    alert("Please Select Batch No");
                } else if (donation_amount === "0") {
                    alert("Please Enter Donation Amount");
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: "ssl_payment_donation_ajax.php",
                        dataType: "json",
                        data: {student_number: student_number, batch_id: batch_id, college_id: college_id, event_id: event_id, schedule_id: schedule_id, delivery_method: delivery_method, customer_id: customer_id, donation_amount: donation_amount},
                        success: function(response) {
                            console.log(response);

                            var obj = eval(response);
                            console.log(obj.output);

                            if (obj.output === "success") {
                                //window.location = "thankyou.php"
                                var total_amount = obj.donation_amount;
                                var booking_id = obj.donation_id;

                                $("#total_amount_ssl").val(total_amount);
                                $("#trans_id_ssl").val(booking_id);
                                var notify_url = "http://www.ticketchai.org/apec/notify_donate.php?itemid=" + booking_id;
                                $("#notify_url").val(notify_url);
                                var  fail_url = "http://www.ticketchai.org/apec/fail.php?itemid=" + booking_id;
                                $("#fail_url").val(fail_url);
                                var cancle_url = "http://www.ticketchai.org/apec/cancel.php?itemid=" + booking_id;

                                if (total_amount !== "" && booking_id !== "") {
                                    document.getElementById("ssl_main").click();
                                }

                            } else {
                                alert(obj.msg);
                            }

                        },
                        error: function(a, b, c) {

                        }
                    });

                }

            });


            //SSL Payment Start Here //




            // Start Batch Change Event //
            $("#batch_id").change(function() {
                var batch_id = $("#batch_id option:selected").val();
                var student_number = $("#student_number").val();
                var college_id = 7;

                $.ajax({
                    type: "POST",
                    url: "check_subscription_price_ajax.php",
                    dataType: "json",
                    data: {student_number: student_number, batch_id: batch_id, college_id: college_id},
                    success: function(response) {

                        var obj = eval(response);

                    },
                    error: function() {

                    }
                });

            });

            // End Batch Change Event //








            $("#pick_on_online_spot").click(function() {

                if ($('#pick_on_online_spot').is(':checked')) {
                    $("#dis_state_online_id").hide();
                    $("#shipping_cost").html('');

                    $("#online_delivery_method").val("pick_on_spot");
                }
            });

            $("#online_home_delivery").click(function() {
                if ($('#online_home_delivery').is(':checked')) {
                    $("#dis_state_online_id").show();

                    $("#online_delivery_method").val("pick_on_spot");

                }
            });

            $("#delivery_id_name").hide();

//            $("#dis_state_id").change(function() {
//
//                var dis_state_id = $("#dis_state_id option:selected").val();
//
//                $.ajax({
//                    type: "POST",
//                    url: "delivery_cost_ajax.php",
//                    dataType: "json",
//                    data: {dis_state_id: dis_state_id},
//                    success: function(response) {
//                        console.log(response);
//                        $("#delivery_cost").html(response);
//                        $("#dis_state_id_add").val(dis_state_id);
//
//
//                        $("#delivery_cost_1").val(response);
//                        $("#shipping_cost").html(response);
//
//                    },
//                    error: function(a, b, c) {
//
//                    }
//                });
//
//            });
            $("#pick_on_spot").click(function() {
                $("#delivery_id_name").hide();
                $("#divProceed").show();


                $("#btnHomeDelivery").hide();
                $("#btnPickFromPoint").show();


                $("#selection_div").hide();

                $("#shipping_cost").html('');

            });
            $("#home_delivery").click(function() {
                $("#delivery_id_name").show();
                $("#divProceed").show();

                $("#btnHomeDelivery").show();
                $("#btnPickFromPoint").hide();


                $("#shipping_cost").html('');
                $("#selection_div").hide();

            });

            $("#sslpayment").click(function() {
                $("#selection_div").hide();
                $("#divProceed").hide();
                $("#shipping_cost").html('');

            });

        });
    </script>
    <script type="text/javascript" src="assets/plugins/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
    <!-- JS Implementing Plugins -->
    <script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
    <!-- Checkout Form -->
    <script src="assets/plugins/sky-forms/version-2.0.1/js/jquery.validate.min.js"></script>
    <script src="assets/plugins/sky-forms/version-2.0.1/js/jquery.maskedinput.min.js"></script>
    <!-- Order Form -->
    <script src="assets/plugins/sky-forms/version-2.0.1/js/jquery-ui.min.js"></script>
    <script src="assets/plugins/sky-forms/version-2.0.1/js/jquery.form.min.js"></script>
    <!-- JS Page Level -->           
    <script type="text/javascript" src="assets/js/app.js"></script>
    <script type="text/javascript" src="assets/js/forms/order.js"></script>
    <script type="text/javascript" src="assets/js/forms/review.js"></script>
    <script type="text/javascript" src="assets/js/forms/checkout.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            App.init();
            OrderForm.initOrderForm();
            ReviewForm.initReviewForm();
            CheckoutForm.initCheckoutForm();
        });
    </script>
    <!--[if lt IE 9]>
        <script src="assets/plugins/respond.js"></script>
        <script src="assets/plugins/html5shiv.js"></script>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src="assets/plugins/sky-forms/version-2.0.1/js/sky-forms-ie8.js"></script>
    <![endif]-->

    <!--[if lt IE 10]>
        <script src="assets/plugins/sky-forms/version-2.0.1/js/jquery.placeholder.min.js"></script>
    <![endif]-->

</body>
</html>