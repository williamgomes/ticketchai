-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 15, 2015 at 08:36 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ticketchai`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE IF NOT EXISTS `activity` (
  `activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_name` varchar(255) DEFAULT NULL,
  `is_active` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `activity`
--

INSERT INTO `activity` (`activity_id`, `activity_name`, `is_active`) VALUES
(1, 'ADVENTURE', 'true'),
(2, 'WATER SPORTS', 'true'),
(3, 'F & B', 'true'),
(4, 'FLEA MARKET', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `admin_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `admin_title` varchar(10) DEFAULT NULL,
  `admin_first_name` varchar(100) DEFAULT NULL,
  `admin_last_name` varchar(100) DEFAULT NULL,
  `admin_mobile_number` varchar(20) DEFAULT NULL,
  `admin_email_address` varchar(100) DEFAULT NULL,
  `admin_password` varchar(255) DEFAULT NULL,
  `admin_image` varchar(255) DEFAULT NULL,
  `admin_address` text,
  `admin_is_member` varchar(10) DEFAULT NULL,
  `admin_updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `admin_updated_by` int(10) unsigned DEFAULT NULL,
  `gender_id` int(10) unsigned DEFAULT NULL,
  `is_active` int(10) unsigned DEFAULT NULL,
  `user_type_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_title`, `admin_first_name`, `admin_last_name`, `admin_mobile_number`, `admin_email_address`, `admin_password`, `admin_image`, `admin_address`, `admin_is_member`, `admin_updated_date`, `admin_updated_by`, `gender_id`, `is_active`, `user_type_id`) VALUES
(1, NULL, 'Khairul Islam Sujan', 'John Carter', '019-85632514', 'admin@gmail.com', 'admin', NULL, '4 / N - A - 1 Korobi Buliding, Lakecity Concord,Khilkhet, Dhaka, 1229', NULL, '2015-01-01 07:50:37', NULL, 1, 1, 1),
(2, NULL, 'Tariqul', 'Islam', '018-46929383', 'tariqul@systechunimax.com', 'super', NULL, 'Dhaka, Bangladesh', NULL, '2014-11-25 11:33:13', NULL, 1, 1, 1),
(4, NULL, 'jahangir', 'jony', '8801723152651', 'jahangirjony007@gmail.com', 'Jony12345', NULL, 'Mirpur-1, Dhaka', NULL, '2014-10-22 10:51:52', NULL, 1, 1, 2),
(5, NULL, 'johir', 'Ahmed', '019-36595351', 'jahirul@systechunimax.com', '123456789', NULL, 'Moghbazar Dhaka', NULL, '2014-10-22 10:59:05', NULL, 1, 1, 2),
(6, NULL, 'Abu ', 'Sayeed', '019-19699666', 'sayeed712@yahoo.com', 'apec123', NULL, 'Pabna', NULL, '2014-11-11 14:20:40', NULL, 1, 1, 4),
(7, NULL, 'Md Abdul', 'Matin', '018-16433362', 'matinpcc@yahoo.com', 'apec123', NULL, 'Pabna', NULL, '2014-11-11 14:23:35', NULL, 1, 1, 4),
(8, NULL, 'Khairul', 'Islam', '015-57232498', 'arkhairulislam@gmail.com', 'Pandora', NULL, 'Lakecity Concord, Khilkhet, Dhaka, 1229', NULL, '2014-12-23 04:34:54', NULL, 1, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `batch`
--

CREATE TABLE IF NOT EXISTS `batch` (
  `batch_id` int(11) NOT NULL AUTO_INCREMENT,
  `program_id` int(11) DEFAULT NULL,
  `batch_name` varchar(255) NOT NULL,
  `ticket_price` varchar(100) DEFAULT NULL,
  `anual_subscription` varchar(100) DEFAULT NULL,
  `is_active` varchar(255) NOT NULL,
  PRIMARY KEY (`batch_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `batch`
--

INSERT INTO `batch` (`batch_id`, `program_id`, `batch_name`, `ticket_price`, `anual_subscription`, `is_active`) VALUES
(1, NULL, '1st Batch', '3500', '2400', 'true'),
(2, NULL, '2nd Batch', '3500', '2400', 'true'),
(3, NULL, '3rd Batch', '3500', '2400', 'true'),
(4, NULL, '4th Batch', '3500', '2400', 'true'),
(5, NULL, '5th Batch', '3500', '2400', 'true'),
(6, NULL, '6th Batch', '3500', '2400', 'true'),
(7, NULL, '7th Batch', '3500', '2400', 'true'),
(8, NULL, '8th Batch', '3500', '2400', 'true'),
(9, NULL, '9th Batch', '3500', '2400', 'true'),
(10, NULL, '10th Batch', '3500', '2400', 'true'),
(11, NULL, '11th Batch', '3500', '2400', 'true'),
(12, NULL, '12th Batch', '3500', '2400', 'true'),
(13, NULL, '13th Batch', '3500', '2400', 'true'),
(14, NULL, '14th Batch', '3500', '2400', 'true'),
(15, NULL, '15th Batch', '3500', '2400', 'true'),
(16, NULL, '16th Batch', '2000', '2400', 'true'),
(17, NULL, '17th Batch', '2000', '2400', 'true'),
(18, NULL, '18th Batch', '2000', '2400', 'true'),
(19, NULL, '19th Batch', '2000', '2400', 'true'),
(20, NULL, '20th Batch', '2000', '2400', 'true'),
(21, NULL, '21th Batch', '2000', '2400', 'true'),
(22, NULL, '22th Batch', '2000', '2400', 'true'),
(23, NULL, '23th Batch', '2000', '2400', 'true'),
(24, NULL, '24th Batch', '1000', '1200', 'true'),
(25, NULL, '25th Batch', '1000', '1200', 'true'),
(26, NULL, '26th Batch', '1000', '1200', 'true'),
(27, NULL, '27th Batch', '1000', '1200', 'true'),
(28, NULL, '28th Batch', '1000', '1200', 'true'),
(29, NULL, '29th Batch', '1000', '0', 'true'),
(30, NULL, '30th Batch', '1000', '0', 'true'),
(31, NULL, '31th Batch', '1000', '0', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `billing_address`
--

CREATE TABLE IF NOT EXISTS `billing_address` (
  `billing_address_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `billing_city` varchar(45) DEFAULT NULL,
  `billing_post_code` varchar(45) DEFAULT NULL,
  `billing_country` varchar(45) DEFAULT NULL,
  `billing_address` text,
  PRIMARY KEY (`billing_address_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `billing_address`
--

INSERT INTO `billing_address` (`billing_address_id`, `customer_id`, `billing_city`, `billing_post_code`, `billing_country`, `billing_address`) VALUES
(1, 6, 'dhakaasa', '1414', '1', 'bfbfvb');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE IF NOT EXISTS `booking` (
  `booking_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(10) unsigned DEFAULT NULL,
  `schedule_id` int(10) unsigned DEFAULT NULL,
  `payment_status` varchar(255) DEFAULT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `ticket_quantity` varchar(255) DEFAULT NULL,
  `ticket_price` varchar(255) DEFAULT NULL,
  `discount_amount` varchar(255) DEFAULT NULL,
  `total_amount` varchar(255) DEFAULT NULL,
  `delivery_method` varchar(255) DEFAULT NULL,
  `delivery_address` text,
  `delivery_post_code` varchar(40) DEFAULT NULL,
  `delivery_status` varchar(45) DEFAULT NULL,
  `tocken` varchar(255) DEFAULT NULL,
  `booking_time` varchar(255) DEFAULT NULL,
  `pick_point_id` int(11) DEFAULT NULL,
  `delivery_city` varchar(45) DEFAULT NULL,
  `delivery_district` varchar(45) DEFAULT NULL,
  `delivery_cost` varchar(45) DEFAULT NULL,
  `delivery_vat` varchar(45) DEFAULT NULL,
  `cadet_id` int(11) DEFAULT NULL,
  `is_cancle` varchar(45) DEFAULT NULL,
  `delivery_country` int(11) DEFAULT NULL,
  `ticket_type_id` int(11) DEFAULT NULL,
  `spouse` varchar(45) DEFAULT NULL,
  `self` varchar(45) DEFAULT NULL,
  `kidesUn12` varchar(45) DEFAULT NULL,
  `kidesUp12` varchar(45) DEFAULT NULL,
  `guest` varchar(45) DEFAULT NULL,
  `maid` varchar(45) DEFAULT NULL,
  `driver` varchar(45) DEFAULT NULL,
  `is_online` varchar(45) DEFAULT NULL,
  `val_id` varchar(255) DEFAULT NULL,
  `online_payment_status` varchar(255) DEFAULT NULL,
  `online_pay_method` varchar(45) DEFAULT NULL,
  `card_number` varchar(255) DEFAULT NULL,
  `card_Holder_name` varchar(255) DEFAULT NULL,
  `Mobile_number` varchar(255) DEFAULT NULL,
  `selfQty` varchar(45) DEFAULT NULL,
  `kidesUn12Qty` varchar(45) DEFAULT NULL,
  `kidsUpQty` varchar(45) DEFAULT NULL,
  `guestQty` varchar(45) DEFAULT NULL,
  `driverQty` varchar(45) DEFAULT NULL,
  `spouseQty` varchar(45) DEFAULT NULL,
  `maidQty` varchar(45) DEFAULT NULL,
  `anual_subsciption_cost` varchar(45) DEFAULT NULL,
  `life_time_subsciption_cost` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `parent_category_id` int(10) unsigned DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `is_active` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `parent_category_id`, `updated_date`, `updated_by`, `is_active`) VALUES
(1, 'EVENT', NULL, '2014-12-30 05:27:53', NULL, 'false'),
(2, 'MUSIC', 1, '2014-12-30 05:27:59', NULL, 'true'),
(3, 'SPEECH', 1, '2014-12-30 05:28:05', NULL, 'true'),
(4, 'REUNION', 1, '2014-12-30 05:28:12', NULL, 'true'),
(5, 'CONCERT', 2, '2014-12-30 05:28:20', NULL, 'true'),
(6, 'DJ PARTY', NULL, '2014-12-30 05:28:39', NULL, 'true');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `city_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `city_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `country_id` int(10) DEFAULT NULL,
  `is_active` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `city_name`, `country_id`, `is_active`) VALUES
(1, 'Dhaka', 1, 'true'),
(2, 'Chittagong', 1, 'true'),
(3, 'Rajshahi', 1, 'true'),
(4, 'Khulna', 1, 'true'),
(5, 'Barisal', 1, 'true'),
(6, 'Sylhet', 1, 'true'),
(7, 'Comilla', 1, 'true'),
(8, 'Rangpur', 1, 'true');

-- --------------------------------------------------------

--
-- Table structure for table `college`
--

CREATE TABLE IF NOT EXISTS `college` (
  `college_id` int(11) NOT NULL AUTO_INCREMENT,
  `college_name` varchar(255) DEFAULT NULL,
  `is_active` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`college_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `college`
--

INSERT INTO `college` (`college_id`, `college_name`, `is_active`) VALUES
(1, 'FGCC', 'true'),
(2, 'JCC', 'true'),
(3, 'RCC', 'true'),
(4, 'SCC', 'true'),
(5, 'MCC', 'true'),
(6, 'BCC', 'true'),
(7, 'PCC', 'true'),
(8, 'CCR', 'true'),
(9, 'MGCC', 'true'),
(10, 'CCC', 'true'),
(11, 'JGCC', 'true'),
(12, 'FCC', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `college_validation`
--

CREATE TABLE IF NOT EXISTS `college_validation` (
  `college_validation_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_number` varchar(255) DEFAULT NULL,
  `college_id` int(11) DEFAULT NULL,
  `year_id` int(11) DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `booking_status` varchar(45) DEFAULT NULL,
  `is_anual_subcribe` varchar(45) DEFAULT NULL,
  `is_life_time_subscribe` varchar(45) DEFAULT NULL,
  `contact_number` varchar(100) DEFAULT NULL,
  `student_name` varchar(255) DEFAULT NULL,
  `is_spouse` varchar(45) DEFAULT NULL,
  `is_self` varchar(45) DEFAULT NULL,
  `anual_subsciption_cost` varchar(45) DEFAULT NULL,
  `life_time_subscription_cost` varchar(45) DEFAULT NULL,
  `token_no` varchar(45) DEFAULT NULL,
  `subcription_delivery_method` varchar(45) DEFAULT NULL,
  `customer_address` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `discrict_id` varchar(45) DEFAULT NULL,
  `zip_code` varchar(45) DEFAULT NULL,
  `val_id` varchar(45) DEFAULT NULL,
  `is_online` varchar(45) DEFAULT NULL,
  `payment_status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`college_validation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1605 ;

--
-- Dumping data for table `college_validation`
--

INSERT INTO `college_validation` (`college_validation_id`, `student_number`, `college_id`, `year_id`, `batch_id`, `booking_status`, `is_anual_subcribe`, `is_life_time_subscribe`, `contact_number`, `student_name`, `is_spouse`, `is_self`, `anual_subsciption_cost`, `life_time_subscription_cost`, `token_no`, `subcription_delivery_method`, `customer_address`, `city`, `discrict_id`, `zip_code`, `val_id`, `is_online`, `payment_status`) VALUES
(2, '1', 7, NULL, 1, 'NULL', '1', '1', '01715094582,', 'Md. Rizwanul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '2', 7, NULL, 1, 'NULL', '2', '1', '01712262896,', 'Md. Monjur Rahman', '1', '1', '2400', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '3', 7, NULL, 1, 'NULL', '1', '1', NULL, 'Md. Nurul Islam', NULL, NULL, NULL, 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '4', 7, NULL, 1, 'NULL', '1', '1', '01711191335,', 'Md. Rafiqul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '5', 7, NULL, 1, 'NULL', '1', '1', '01711444486,', 'Md. Rezaul Korim', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, '6', 7, NULL, 1, 'NULL', '1', '1', '01720302376,', 'Mollah Md. Mahbubur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, '7', 7, NULL, 1, 'NULL', '1', '1', '01716299653,', 'Mir Md. Nazrul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, '8', 7, NULL, 1, 'NULL', '1', '1', NULL, 'Md.Nadim Akther Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, '9', 7, NULL, 1, 'NULL', '1', '2', '01711937750,', 'K.M.Selim Reza', NULL, NULL, 'NULL', '20000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, '10', 7, NULL, 1, 'NULL', '1', '1', '01712213174,', 'Md.Ahsan Kabir', NULL, NULL, NULL, 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, '11', 7, NULL, 1, 'NULL', '1', '1', '01711664233,', 'Mollah Mahmud Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, '12', 7, NULL, 1, 'NULL', '1', '1', NULL, 'Md. Ibrahim Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, '13', 7, NULL, 1, 'NULL', '1', '1', NULL, 'Khondokar Shafiur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, '14', 7, NULL, 1, 'NULL', '1', '1', '01730343536,', 'S.M. Abul Kalam Azad', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, '15', 7, NULL, 1, 'NULL', '1', '1', '01912586351,', 'Md. Saiful Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, '16', 7, NULL, 1, 'NULL', '1', '1', NULL, 'Md. Monirul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, '17', 7, NULL, 1, 'NULL', '1', '1', '01711391065,', 'Md. Salahuddin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, '18', 7, NULL, 1, 'NULL', '1', '1', '01673015030, 01817035733,', 'Sk. Nazrul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, '19', 7, NULL, 1, 'NULL', '1', '1', NULL, 'Late Dr. A.K.M. Ziaul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, '20', 7, NULL, 1, 'NULL', '1', '1', '01711878730,', 'Md. Tanvir Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, '21', 7, NULL, 1, 'NULL', '1', '2', '01819258642, 8024093(res), 9568268(off)', 'Abu Sayeed Md. Mostaq', NULL, NULL, 'NULL', '20000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, '22', 7, NULL, 1, 'NULL', '1', '1', '01552375532,', 'Md. Saidul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, '23', 7, NULL, 1, 'NULL', '1', '1', NULL, 'Md. Anamul Hoque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, '24', 7, NULL, 1, 'NULL', '1', '1', '01733991964, ', 'A.B.M. Salahuddin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, '25', 7, NULL, 1, 'NULL', '1', '1', '01811410463,', 'Md. Mofazzal Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, '26', 7, NULL, 1, 'NULL', '1', '1', NULL, 'Md. Azmanul Haque Rabbani', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, '27', 7, NULL, 1, 'NULL', '1', '1', '01199198318,', 'Md. Mofidul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, '28', 7, NULL, 1, 'NULL', '1', '1', '01711372644, ', 'Mahmud Hossain Zia', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, '29', 7, NULL, 1, 'NULL', '1', '1', '01552540121,', 'Md. Abu Saqlayen', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, '30', 7, NULL, 1, 'NULL', '1', '1', NULL, 'Md. Abdul Mozid Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, '31', 7, NULL, 1, 'NULL', '1', '1', NULL, 'Md. Abul Hashem', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, '32', 7, NULL, 1, 'NULL', '1', '1', NULL, 'Sk Forid Uddin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, '33', 7, NULL, 1, 'NULL', '1', '1', '01720384662,', 'M. Kamran Hamid', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, '34', 7, NULL, 1, 'NULL', '1', '1', NULL, 'Pradit Kranti Chowdhury', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, '35', 7, NULL, 1, 'NULL', '1', '1', '1713425719', 'Wahid Hasan Kutubuddin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, '36', 7, NULL, 1, 'NULL', '1', '2', NULL, 'Salah Uddin Ahmed Morshed', NULL, NULL, 'NULL', '20000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, '37', 7, NULL, 1, 'NULL', '1', '2', '01711521500,', 'M.M Mostafa Jamal', NULL, NULL, 'NULL', '20000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, '38', 7, NULL, 1, 'NULL', '1', '2', '01816519613,', 'Md. Samsuzzaman Khan', NULL, NULL, 'NULL', '20000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, '39', 7, NULL, 1, 'NULL', '1', '1', NULL, 'Nur Mohammad Mostafa', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, '40', 7, NULL, 1, 'NULL', '1', '1', '01711902878,', 'Md. Ashraful Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, '41', 7, NULL, 1, 'NULL', '1', '1', '1711593794', 'Md. Abdullah al Obaidi', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, '42', 7, NULL, 1, 'NULL', '1', '1', NULL, 'Md. Abdul Baki', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, '43', 7, NULL, 1, 'NULL', '1', '1', NULL, 'Mohammad Ali Sohag', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, '44', 7, NULL, 1, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, '45', 7, NULL, 1, 'NULL', '1', '1', '01716308628,', 'Sohel Masud Aziz', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, '46', 7, NULL, 1, 'NULL', '1', '1', NULL, 'Md. Moazzem Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, '47', 7, NULL, 1, 'NULL', '1', '1', NULL, 'Md. Zakir Hossain Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, '48', 7, NULL, 1, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, '49', 7, NULL, 1, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, '50', 7, NULL, 1, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, '51', 7, NULL, 2, 'NULL', '1', '1', NULL, 'Fida Nur', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(53, '52', 7, NULL, 2, 'NULL', '1', '1', '1711168370', 'Md. Matinur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, '53', 7, NULL, 2, 'NULL', '1', '1', '1711538098', 'Abul Kalam Md. Nurul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, '54', 7, NULL, 2, 'NULL', '1', '1', '1714090463', 'Md. Abdul Latif', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, '55', 7, NULL, 2, 'NULL', '1', '1', '1711039319', 'Sardar Md. Ataur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, '56', 7, NULL, 2, 'NULL', '1', '1', '1711959591', 'Md. Abdul Quddus Mia', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, '57', 7, NULL, 2, 'NULL', '1', '1', '1712066336', 'Md. Halim Sarwar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, '58', 7, NULL, 2, 'NULL', '1', '1', NULL, 'Md. Rafiqul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, '59', 7, NULL, 2, 'NULL', '1', '1', '1714032244', 'Md. Nahid Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, '60', 7, NULL, 2, 'NULL', '1', '1', '1731928926', 'Md. Wazed Ali Mollah', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, '61', 7, NULL, 2, 'NULL', '1', '1', '1818509802', 'Md. Humayun Kobir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, '62', 7, NULL, 2, 'NULL', '1', '1', '1712269741', 'Md. Ahsan Kobir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, '63', 7, NULL, 2, 'NULL', '1', '1', NULL, 'Md. Monirul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, '64', 7, NULL, 2, 'NULL', '1', '1', NULL, 'Late Md. Musfiqur Kobir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, '65', 7, NULL, 2, 'NULL', '1', '1', NULL, 'Shahriar Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, '66', 7, NULL, 2, 'NULL', '1', '1', NULL, 'Late Md. Mostafizur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, '67', 7, NULL, 2, 'NULL', '1', '2', '1711824936', 'Md. Anamur Rahman Tang', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, '68', 7, NULL, 2, 'NULL', '1', '1', '1711226941', 'A.S.M. Kabir Rana', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(70, '69', 7, NULL, 2, 'NULL', '1', '1', NULL, 'Md. Johirul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(71, '70', 7, NULL, 2, 'NULL', '1', '1', '1716370490', 'Md. Yakub Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(72, '71', 7, NULL, 2, 'NULL', '1', '1', NULL, 'S.M. Alamgeer Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(73, '72', 7, NULL, 2, 'NULL', '1', '1', '1715434152', 'Ahmed Rafiq Badi', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(74, '73', 7, NULL, 2, 'NULL', '1', '1', '1711534981', 'Md. Omar Zahid', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(75, '74', 7, NULL, 2, 'NULL', '1', '1', NULL, 'Mahmud-Un-Nobi', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(76, '75', 7, NULL, 2, 'NULL', '1', '2', '1711211303', 'Md. Moynur Johur', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(77, '76', 7, NULL, 2, 'NULL', '1', '1', NULL, 'Md. Zahidur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(78, '77', 7, NULL, 2, 'NULL', '1', '1', NULL, 'Md. Aliuzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(79, '78', 7, NULL, 2, 'NULL', '1', '1', NULL, 'Md. Atiquzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(80, '79', 7, NULL, 2, 'NULL', '1', '1', '1712284462', 'Md. Raihanul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(81, '80', 7, NULL, 2, 'NULL', '1', '1', NULL, 'Asheq A Mahnud', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(82, '81', 7, NULL, 2, 'NULL', '1', '1', NULL, 'Md. Kamrul Hasan Sarkar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(83, '82', 7, NULL, 2, 'NULL', '1', '1', '1715909964', 'Md. Tareenul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(84, '83', 7, NULL, 2, 'NULL', '1', '1', '1713091330', 'T.M. Zubaer', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(85, '84', 7, NULL, 2, 'NULL', '1', '1', NULL, 'Shaheen Ahmed Chowdhury', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(86, '85', 7, NULL, 2, 'NULL', '1', '1', NULL, 'Sayeed Md. Tohidul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(87, '86', 7, NULL, 2, 'NULL', '1', '1', '1819274117', 'Md. Aminul Haque Pathan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(88, '87', 7, NULL, 2, 'NULL', '1', '1', '1914314175', 'Md. Julfikar haidar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(89, '88', 7, NULL, 2, 'NULL', '1', '1', '1713367620', 'Khandakar Md. Mozammel Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(90, '89', 7, NULL, 2, 'NULL', '1', '1', '1713098340', 'Mirza Enamul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(91, '90', 7, NULL, 2, 'NULL', '1', '1', NULL, 'Ali Reza Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(92, '91', 7, NULL, 2, 'NULL', '1', '1', '1713065589', 'Kazi Mazharul karim', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(93, '92', 7, NULL, 2, 'NULL', '1', '1', NULL, 'Md. Mohbubul Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(94, '93', 7, NULL, 2, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(95, '94', 7, NULL, 2, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(96, '95', 7, NULL, 2, 'NULL', '1', '1', '1556311969', 'S.M. Mahbubul Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(97, '96', 7, NULL, 2, 'NULL', '1', '1', NULL, 'Md. Mehedi Masud Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(98, '97', 7, NULL, 3, 'NULL', '1', '1', NULL, 'Mahmud akhtar khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(99, '98', 7, NULL, 3, 'NULL', '1', '1', '1722182904', 'K.M. Jahangir Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(100, '99', 7, NULL, 3, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(101, '100', 7, NULL, 3, 'NULL', '1', '2', NULL, 'T.M.A Mamun', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(102, '101', 7, NULL, 3, 'NULL', '1', '1', NULL, 'Md.khairul Alam Ansary', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(103, '102', 7, NULL, 3, 'NULL', '1', '1', NULL, 'Major Khandokar Farid ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(104, '103', 7, NULL, 3, 'NULL', '1', '1', NULL, 'Md.Moinul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(105, '104', 7, NULL, 3, 'NULL', '1', '1', NULL, 'K.M. Mahmud', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(106, '105', 7, NULL, 3, 'NULL', '1', '2', '1712812777', 'Md.Rafiul Islam', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(107, '106', 7, NULL, 3, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(108, '107', 7, NULL, 3, 'NULL', '1', '1', '1718091264', 'Md.Mojammel Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(109, '108', 7, NULL, 3, 'NULL', '1', '1', NULL, 'Md.Zafaruallah', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(110, '109', 7, NULL, 3, 'NULL', '1', '1', '1711451839', 'Md.Aminul Bari ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(111, '110', 7, NULL, 3, 'NULL', '1', '1', '1711954820', 'Md.Liaquat Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(112, '111', 7, NULL, 3, 'NULL', '1', '1', NULL, 'Late Md.Kanakuzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(113, '112', 7, NULL, 3, 'NULL', '1', '2', '1711118007', 'Dr.Md.Arifur Rahman', NULL, NULL, 'NULL', '20000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(114, '113', 7, NULL, 3, 'NULL', '1', '1', '1711301782', 'Md.Motarob Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(115, '114', 7, NULL, 3, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(116, '115', 7, NULL, 3, 'NULL', '1', '1', NULL, 'Md.Shofiul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(117, '116', 7, NULL, 3, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(118, '117', 7, NULL, 3, 'NULL', '1', '1', NULL, 'Md.Shofikul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(119, '118', 7, NULL, 3, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(120, '119', 7, NULL, 3, 'NULL', '1', '1', '1715760315', 'Md.Ziaur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(121, '120', 7, NULL, 3, 'NULL', '1', '2', '1715075740', 'Nurul Islam Sarkar', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(122, '121', 7, NULL, 3, 'NULL', '1', '1', '1711582715', 'Shah Mushfiqur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(123, '122', 7, NULL, 3, 'NULL', '1', '1', NULL, 'jameel Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(124, '123', 7, NULL, 3, 'NULL', '1', '1', '1818301202', 'Dr. A.K.M. Shahabuddin Lipton', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(125, '124', 7, NULL, 3, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(126, '125', 7, NULL, 3, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(127, '126', 7, NULL, 3, 'NULL', '1', '1', NULL, 'Md.Faisal Karim', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(128, '127', 7, NULL, 3, 'NULL', '1', '1', '1712813005', 'Abu Taher', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(129, '128', 7, NULL, 3, 'NULL', '1', '1', '1713095158', 'Md.Fakrul Ahsan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(130, '129', 7, NULL, 3, 'NULL', '1', '1', NULL, 'Md.Moazzem Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(131, '130', 7, NULL, 3, 'NULL', '1', '1', '1819251297', 'Md.Emran Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(132, '131', 7, NULL, 3, 'NULL', '1', '1', NULL, 'Shameem Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(133, '132', 7, NULL, 3, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(134, '133', 7, NULL, 3, 'NULL', '1', '1', NULL, 'Md.Zalal Uddin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(135, '134', 7, NULL, 3, 'NULL', '1', '1', NULL, 'Md. Maidul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(136, '135', 7, NULL, 3, 'NULL', '1', '1', NULL, 'Abu Abdulla Al Mamun', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(137, '136', 7, NULL, 3, 'NULL', '1', '2', '1711188617', 'Khosru Mohammad Salim', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(138, '137', 7, NULL, 3, 'NULL', '1', '2', '1711325665', 'Md.Rabiul Alam', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(139, '138', 7, NULL, 3, 'NULL', '1', '2', '1730089118', 'Khan Sharif Raihan', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(140, '139', 7, NULL, 3, 'NULL', '1', '1', NULL, 'Md.Zahir Shah Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(141, '140', 7, NULL, 3, 'NULL', '1', '2', '1711946200', 'Mesbaul Haq', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(142, '141', 7, NULL, 3, 'NULL', '1', '1', NULL, 'Md.Nayeem Golam Muktadir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(143, '142', 7, NULL, 3, 'NULL', '1', '1', '1713778371', 'Md. Faruk Al Mamun', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(144, '143', 7, NULL, 3, 'NULL', '1', '1', NULL, 'Major Shafee Ullah Bulbul', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(145, '144', 7, NULL, 3, 'NULL', '1', '2', '1720095621', 'Md.Waliullah', NULL, NULL, 'NULL', '20000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(146, '145', 7, NULL, 3, 'NULL', '1', '1', '1713494200', 'Lt. Col. Zahirul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(147, '146', 7, NULL, 3, 'NULL', '1', '2', '1670802303', 'Md.Zahidul Haque', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(148, '147', 7, NULL, 3, 'NULL', '1', '1', NULL, 'Aminul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(149, '148', 7, NULL, 4, 'NULL', '1', '1', '1716036927', 'A.B.M. Atiqul Ashfar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(150, '149', 7, NULL, 4, 'NULL', '1', '1', NULL, 'Mohammad Hasanur Rahaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(151, '150', 7, NULL, 4, 'NULL', '1', '2', '1713145487', 'Md. Tanvir Hossain', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(152, '151', 7, NULL, 4, 'NULL', '1', '1', NULL, 'Monjur Rahim', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(153, '152', 7, NULL, 4, 'NULL', '1', '1', NULL, 'Golam Moin Uddin Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(154, '153', 7, NULL, 4, 'NULL', '1', '1', '1718945018', 'Abdul Motalib', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(155, '154', 7, NULL, 4, 'NULL', '1', '1', NULL, 'Md. Anisul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(156, '155', 7, NULL, 4, 'NULL', '1', '1', '1712610807', 'Khondaker Abidur Rahaman ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(157, '156', 7, NULL, 4, 'NULL', '1', '1', '1711239563', 'Major Md. Mahaboob Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(158, '157', 7, NULL, 4, 'NULL', '1', '1', NULL, 'Mehedi Morshed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(159, '158', 7, NULL, 4, 'NULL', '1', '1', '1711730760', 'Md. Amirul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(160, '159', 7, NULL, 4, 'NULL', '1', '1', '1711964980', 'Major Md. Yousuf Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(161, '160', 7, NULL, 4, 'NULL', '1', '1', '1917336295', 'Md. Dulalur Rahaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(162, '161', 7, NULL, 4, 'NULL', '1', '1', NULL, 'Md. Selim Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(163, '162', 7, NULL, 4, 'NULL', '1', '1', '1753212121', 'Major Md. Sohel Rana', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(164, '163', 7, NULL, 4, 'NULL', '1', '1', '1711701133', 'Major Md. Abul Kalam ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(165, '164', 7, NULL, 4, 'NULL', '1', '1', NULL, 'Major Md. Fariqul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(166, '165', 7, NULL, 4, 'NULL', '1', '1', NULL, 'Major Golam Rasul Azad', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(167, '166', 7, NULL, 4, 'NULL', '1', '2', '1711362497', 'Farhad Jahan', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(168, '167', 7, NULL, 4, 'NULL', '1', '1', NULL, 'Md. Zia Mohiuddin ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(169, '168', 7, NULL, 4, 'NULL', '1', '1', NULL, 'Md. Rafiqul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(170, '169', 7, NULL, 4, 'NULL', '1', '1', '1730317733', 'Md. Zakiur Rahaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(171, '170', 7, NULL, 4, 'NULL', '1', '2', '1713094484', 'Syed Md. Imtiaz Akbar', '1', '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(172, '171', 7, NULL, 4, 'NULL', '1', '1', '1190744524', 'Md. Mostafa Kamal', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(173, '172', 7, NULL, 4, 'NULL', '1', '1', '1199010923', 'Major Mohsin Ali Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(174, '173', 7, NULL, 4, 'NULL', '1', '2', '1711734385', 'Md. Neamul Hasan ', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(175, '174', 7, NULL, 4, 'NULL', '1', '1', NULL, 'Md. Mahiul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(176, '175', 7, NULL, 4, 'NULL', '1', '1', '1819245797', 'Md. Mabian Miah', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(177, '176', 7, NULL, 4, 'NULL', '1', '1', NULL, 'Md. Nazimuddin  ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(178, '177', 7, NULL, 4, 'NULL', '1', '1', '1552323578', 'Md. Abdul Kader', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(179, '178', 7, NULL, 4, 'NULL', '1', '1', '1715062019', 'Md. Maksudur Rahaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(180, '179', 7, NULL, 4, 'NULL', '1', '1', NULL, 'Md. Abul Kalam Azad', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(181, '180', 7, NULL, 4, 'NULL', '1', '1', '1199071912', 'Md. Rokon Yamani  ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(182, '181', 7, NULL, 4, 'NULL', '1', '1', NULL, 'Mansurul Kabir Choudhary', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(183, '182', 7, NULL, 4, 'NULL', '1', '1', '1711014369', 'Mahabub Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(184, '183', 7, NULL, 4, 'NULL', '1', '1', '1713337082', 'Md. Rawshan Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(185, '184', 7, NULL, 4, 'NULL', '1', '1', '1711054389', 'Md. Zahidul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(186, '185', 7, NULL, 4, 'NULL', '1', '1', '1711908448', 'Major Shariar Rashid  ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(187, '186', 7, NULL, 4, 'NULL', '1', '1', '1716771953', 'Md. Rezanur Rahaman  ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(188, '187', 7, NULL, 4, 'NULL', '1', '1', '1712517895', 'Md. Murad Akter  Khan  ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(189, '188', 7, NULL, 4, 'NULL', '1', '1', '1715016495', 'Md. Yadul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(190, '189', 7, NULL, 4, 'NULL', '1', '1', '1711548887', 'Khalid Mahmood Khan', NULL, NULL, 'NULL', NULL, 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(191, '190', 7, NULL, 4, 'NULL', '1', '1', '1712185092', 'Major Md. Ashraf Ali', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(192, '191', 7, NULL, 4, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(193, '192', 7, NULL, 4, 'NULL', '1', '1', NULL, 'Md. Hamidur  Rahaman ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(194, '193', 7, NULL, 4, 'NULL', '1', '1', NULL, 'Md. Ali Immam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(195, '194', 7, NULL, 4, 'NULL', '1', '1', '1715005902', 'Md. Hasan Ifftekhar ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(196, '195', 7, NULL, 4, 'NULL', '1', '1', '1713095412', 'Major Md. Zakir Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(197, '196', 7, NULL, 4, 'NULL', '1', '1', NULL, 'Md. Saiful Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(198, '197', 7, NULL, 4, 'NULL', '1', '1', NULL, 'Md. Saifullah Mohammad Kahled', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(199, '198', 7, NULL, 4, 'NULL', '1', '1', '1716729836', 'Major Md. Sanuwar Uddin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(200, '199', 7, NULL, 4, 'NULL', '1', '1', NULL, 'Md. Anwar Khurshid', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(201, '200', 7, NULL, 4, 'NULL', '1', '1', NULL, 'Md. Hasibul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(202, '201', 7, NULL, 4, 'NULL', '1', '1', NULL, 'Muhammad Saifullah  ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(203, '202', 7, NULL, 5, 'NULL', '1', '1', NULL, 'Mostofa Kamal Pasha', NULL, NULL, 'NULL', NULL, 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, 'false', NULL),
(204, '203', 7, NULL, 5, 'NULL', '2', '1', '1713018297', 'Khandkar Nazmul Hassan', '1', '1', '2400', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(205, '204', 7, NULL, 5, 'NULL', '1', '1', '1710367850', 'Md Faizur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(206, '205', 7, NULL, 5, 'NULL', '1', '1', NULL, 'Md. Anwar Ashiq', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(207, '206', 7, NULL, 5, 'NULL', '1', '1', '1716298181', 'Zia- us â€“ Shams', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(208, '207', 7, NULL, 5, 'NULL', '1', '1', '1711467780', 'Md. Boni Yameen', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(209, '208', 7, NULL, 5, 'NULL', '1', '2', '1713423884', 'Md Khairul Bashar', '1', '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(210, '209', 7, NULL, 5, 'NULL', '1', '1', '1911398893', 'Md.Saifur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(211, '210', 7, NULL, 5, 'NULL', '1', '1', '1711318951', 'MD. Khairul Kobir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(212, '211', 7, NULL, 5, 'NULL', '1', '2', '1711194052', 'Md.Mahmudul Malek', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(213, '212', 7, NULL, 5, 'NULL', '1', '1', '1715882512', 'Humayun Kabir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(214, '213', 7, NULL, 5, 'NULL', '1', '1', '1711818671', 'ASM Afzal Hoque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(215, '214', 7, NULL, 5, 'NULL', '1', '1', NULL, 'Arman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(216, '215', 7, NULL, 5, 'NULL', '1', '1', '1736750951', 'Md. Shah Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(217, '216', 7, NULL, 5, 'NULL', '1', '1', NULL, 'Md Jahangir ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(218, '217', 7, NULL, 5, 'NULL', '1', '1', NULL, 'AKMM Sherafullah', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(219, '218', 7, NULL, 5, 'NULL', '1', '1', NULL, 'Abul Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(220, '219', 7, NULL, 5, 'NULL', '1', '1', NULL, 'Md. Moniruzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(221, '220', 7, NULL, 5, 'NULL', '1', '2', '1712613874', 'Md. Asaduzzaman', '1', '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(222, '221', 7, NULL, 5, 'NULL', '1', '1', '1714794400', 'Md. Abdul Alim Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(223, '222', 7, NULL, 5, 'NULL', '1', '2', NULL, 'Md.Miarul Haque', NULL, NULL, 'NULL', '20000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(224, '223', 7, NULL, 5, 'NULL', '1', '1', NULL, 'Md. Abdul Malek', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(225, '224', 7, NULL, 5, 'NULL', '1', '2', '1713332049', 'Md. Mamunur Rashid', '1', '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(226, '225', 7, NULL, 5, 'NULL', '1', '1', NULL, 'Md. Bashirul Hoque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(227, '226', 7, NULL, 5, 'NULL', '1', '2', NULL, 'Rafikuzzaman', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(228, '227', 7, NULL, 5, 'NULL', '1', '1', '1715026508', 'Khondokar Yusuf Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(229, '228', 7, NULL, 5, 'NULL', '1', '1', NULL, 'Kawser Imam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(230, '229', 7, NULL, 5, 'NULL', '1', '2', '1817562700', 'Md. Shahadat Hossain', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(231, '230', 7, NULL, 5, 'NULL', '1', '1', '1711789564', 'MD. Mubarak Hossain ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(232, '231', 7, NULL, 5, 'NULL', '1', '1', '1715202490', 'GM khorshed Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(233, '232', 7, NULL, 5, 'NULL', '1', '1', '1720418927', 'Arif Innas', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(234, '233', 7, NULL, 5, 'NULL', '1', '2', '1817042390', 'Khondokar Md. Arif', NULL, NULL, 'NULL', '20000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(235, '234', 7, NULL, 5, 'NULL', '1', '1', NULL, 'Mushfiqur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(236, '235', 7, NULL, 5, 'NULL', '1', '1', '01550400001,01741473005,', 'Md. Mizanur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(237, '236', 7, NULL, 5, 'NULL', '1', '1', NULL, 'Md. Shahidul Kobir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(238, '237', 7, NULL, 5, 'NULL', '1', '1', '1710960506', 'Md. Razu Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(239, '238', 7, NULL, 5, 'NULL', '1', '1', NULL, 'Muzzadid sarkar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(240, '239', 7, NULL, 5, 'NULL', '1', '1', NULL, 'Md. Lutfur Kobir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(241, '240', 7, NULL, 5, 'NULL', '1', '1', '1711102605', 'Md.Abdur Rashid', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(242, '241', 7, NULL, 5, 'NULL', '1', '1', '1713036684', 'Rabiul Alam Sarker ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(243, '242', 7, NULL, 5, 'NULL', '1', '1', '1715500799', 'Md.Osman Goni ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(244, '243', 7, NULL, 5, 'NULL', '1', '1', NULL, 'Md.Shahidul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(245, '244', 7, NULL, 5, 'NULL', '1', '1', NULL, 'Md. Abul Khayer', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(246, '245', 7, NULL, 5, 'NULL', '1', '1', '1713009986', 'Md. Bazlur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(247, '246', 7, NULL, 5, 'NULL', '1', '2', NULL, 'Arif Ahmed Belal', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(248, '247', 7, NULL, 5, 'NULL', '1', '1', '1715011602', 'A.M. Shamshir Ahmmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(249, '248', 7, NULL, 5, 'NULL', '1', '1', NULL, 'Late Md Aktheruzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(250, '249', 7, NULL, 5, 'NULL', '1', '1', NULL, 'Abdullah', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(251, '250', 7, NULL, 5, 'NULL', '1', '1', '1730013594', 'Nur Kuttubuk Alamgir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(252, '251', 7, NULL, 5, 'NULL', '1', '1', NULL, 'Md zahid Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(253, '252', 7, NULL, 5, 'NULL', '1', '2', '1711934458', 'Sawkat nur Abedi', NULL, NULL, 'NULL', '20000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(254, '253', 7, NULL, 5, 'NULL', '1', '1', '1841316162', 'Mahmud Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(255, '254', 7, NULL, 5, 'NULL', '1', '1', NULL, 'Late Md, Ahsanul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(256, '255', 7, NULL, 5, 'NULL', '1', '1', '1711817447', 'Kazi Md. Asif', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(257, '256', 7, NULL, 5, 'NULL', '1', '2', '1715490535', 'Md.Manjurul Karim', NULL, NULL, NULL, '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(258, '257', 7, NULL, 5, 'NULL', '1', '1', NULL, 'Md. Moinuddin ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(259, '258', 7, NULL, 5, 'NULL', '1', '1', '1713066632', 'Aminur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(260, '259', 7, NULL, 5, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(261, '260', 7, NULL, 6, 'NULL', '1', '1', '1712413569', 'Md. Latiful Bari', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(262, '261', 7, NULL, 6, 'NULL', '1', '1', '1730437305', 'Saleh Ahmed Monsur', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(263, '262', 7, NULL, 6, 'NULL', '1', '1', NULL, 'Khondokar Ibne Enam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(264, '263', 7, NULL, 6, 'NULL', '1', '2', '1712158678', 'Md. Mostafizur Rahman', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(265, '264', 7, NULL, 6, 'NULL', '1', '2', '1919765432', 'A.K.M. Afzal Hossain', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(266, '265', 7, NULL, 6, 'NULL', '1', '1', NULL, 'Md.Amirul Kdir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(267, '266', 7, NULL, 6, 'NULL', '1', '2', '1711136249', 'Shakil Bin Mahboob', '1', '1', NULL, '10000', 'NULL', 'home_delivery', NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(268, '267', 7, NULL, 6, 'NULL', '1', '1', NULL, 'Md. Reaz Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(269, '268', 7, NULL, 6, 'NULL', '1', '1', NULL, 'Md. Zahidul Hassan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(270, '269', 7, NULL, 6, 'NULL', '1', '1', NULL, 'Zubaer Siddique', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(271, '270', 7, NULL, 6, 'NULL', '1', '1', NULL, 'Md. Zakir Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(272, '271', 7, NULL, 6, 'NULL', '1', '1', NULL, 'AKM Ahsan Habib', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(273, '272', 7, NULL, 6, 'NULL', '1', '1', '1915695439', 'Md.Abu Zafar Siddiqui', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(274, '273', 7, NULL, 6, 'NULL', '1', '1', NULL, 'Md.Ahsanul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(275, '274', 7, NULL, 6, 'NULL', '1', '1', '1711578318', 'Md.Abdul Matin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(276, '275', 7, NULL, 6, 'NULL', '1', '1', '1713099708', 'Md.Obaidul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(277, '276', 7, NULL, 6, 'NULL', '1', '1', NULL, 'Md.Omar Faruk Azad', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(278, '277', 7, NULL, 6, 'NULL', '1', '1', '1912501187', 'Md.Niamul Bashir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(279, '278', 7, NULL, 6, 'NULL', '1', '1', '1711465975', 'Md.Zahurul Hyder', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(280, '279', 7, NULL, 6, 'NULL', '1', '2', '1711500032', 'Md.Abu Mamun Hashmi', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(281, '280', 7, NULL, 6, 'NULL', '1', '1', '1714094400', 'Md.Shariful Islam Talukder', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(282, '281', 7, NULL, 6, 'NULL', '1', '2', '1713043600', 'T.M.Shahidul Islam', '1', '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(283, '282', 7, NULL, 6, 'NULL', '1', '1', NULL, 'Md. Nazrul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(284, '283', 7, NULL, 6, 'NULL', '1', '1', '1617143580', 'Syeed Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(285, '284', 7, NULL, 6, 'NULL', '1', '1', '1750000518', 'Md.Rafiqur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(286, '285', 7, NULL, 6, 'NULL', '1', '1', NULL, 'Md. Akbar Ali', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(287, '286', 7, NULL, 6, 'NULL', '1', '2', '1713257423', 'Kazi Masum Rashed', '1', '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(288, '287', 7, NULL, 6, 'NULL', '1', '1', NULL, 'Ehsan Masud', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(289, '288', 7, NULL, 6, 'NULL', '1', '1', NULL, 'Md. Manzurul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(290, '289', 7, NULL, 6, 'NULL', '1', '1', '1711810403', 'Md. Mahbubul Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(291, '290', 7, NULL, 6, 'NULL', '1', '1', NULL, 'Md. Saiful Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(292, '291', 7, NULL, 6, 'NULL', '1', '1', '1552202725', 'Md. Rakbul Hassan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(293, '292', 7, NULL, 6, 'NULL', '1', '1', NULL, 'Rafiqul Hyder', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(294, '293', 7, NULL, 6, 'NULL', '1', '1', NULL, 'Md. Atiqul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(295, '294', 7, NULL, 6, 'NULL', '1', '1', NULL, 'AKM Quamruzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(296, '295', 7, NULL, 6, 'NULL', '1', '1', '1711327541', 'Tarek Abdullah', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(297, '296', 7, NULL, 6, 'NULL', '1', '2', '1711334053', 'Md. Azizur Rahman Talukder', '1', '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(298, '297', 7, NULL, 6, 'NULL', '1', '1', '1911011909', 'Md. Mahbubur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(299, '298', 7, NULL, 6, 'NULL', '1', '1', NULL, 'Md. Masroor Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(300, '299', 7, NULL, 6, 'NULL', '1', '1', '1915482935', 'Md. Shahadat Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(301, '300', 7, NULL, 6, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(302, '301', 7, NULL, 6, 'NULL', '1', '1', '1712932124', 'Md. Jahangir Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(303, '302', 7, NULL, 6, 'NULL', '1', '1', NULL, 'Md. Arshadul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(304, '303', 7, NULL, 6, 'NULL', '1', '1', NULL, 'Md. Mahbubar Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(305, '304', 7, NULL, 6, 'NULL', '1', '1', NULL, 'Mojurul Alam (Tony)', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(306, '305', 7, NULL, 6, 'NULL', '1', '1', NULL, 'Mostafa Mannan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(307, '306', 7, NULL, 6, 'NULL', '1', '1', NULL, 'Hussain Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(308, '307', 7, NULL, 6, 'NULL', '1', '1', '1818219365', 'Md. Rezaul Awal', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(309, '308', 7, NULL, 6, 'NULL', '1', '1', '1713095032', 'Shahriar Ahmed Amin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(310, '309', 7, NULL, 6, 'NULL', '1', '2', '1711532251', 'Abul Hashem Sarker', '1', '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(311, '310', 7, NULL, 6, 'NULL', '1', '1', '1715421195', 'Biplob Borua', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(312, '311', 7, NULL, 6, 'NULL', '1', '2', '1979226227', 'Khandaker Morshed Jahan', '1', '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(313, '312', 7, NULL, 6, 'NULL', '1', '1', '1729096555', 'J.M.H. Quamar Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(314, '313', 7, NULL, 6, 'NULL', '1', '1', '1912234606', 'Kamal Hyder', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(315, '314', 7, NULL, 6, 'NULL', '1', '1', '1711397819', 'Hasan Fakhrur Bari', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(316, '315', 7, NULL, 6, 'NULL', '1', '1', '1713034079', 'Shahjahan SHajedur', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `college_validation` (`college_validation_id`, `student_number`, `college_id`, `year_id`, `batch_id`, `booking_status`, `is_anual_subcribe`, `is_life_time_subscribe`, `contact_number`, `student_name`, `is_spouse`, `is_self`, `anual_subsciption_cost`, `life_time_subscription_cost`, `token_no`, `subcription_delivery_method`, `customer_address`, `city`, `discrict_id`, `zip_code`, `val_id`, `is_online`, `payment_status`) VALUES
(317, '316', 7, NULL, 7, 'NULL', '1', '2', '1711953823', 'Md. Jahangir Alam', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(318, '317', 7, NULL, 7, 'NULL', '1', '1', '1715177298', 'Fajlul Karim', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(319, '318', 7, NULL, 7, 'NULL', '1', '1', '1711174212', 'Doctor Md. Atiqur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(320, '319', 7, NULL, 7, 'NULL', '1', '1', NULL, 'Suvro', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(321, '320', 7, NULL, 7, 'NULL', '1', '1', '1713044361', 'Zia', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(322, '321', 7, NULL, 7, 'NULL', '1', '1', '1818295479', 'Quazi Md. Fazlul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(323, '322', 7, NULL, 7, 'NULL', '1', '1', NULL, 'Late Sajadul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(324, '323', 7, NULL, 7, 'NULL', '1', '1', '1920256608', 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(325, '324', 7, NULL, 7, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(326, '325', 7, NULL, 7, 'NULL', '1', '1', '1711400600', 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(327, '326', 7, NULL, 7, 'NULL', '1', '1', '1711737802', 'Md. Rafiqul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(328, '329', 7, NULL, 7, 'NULL', '1', '2', '1711520997', 'Murtaza Sibgatul Haq Sujon ', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(329, '330', 7, NULL, 7, 'NULL', '1', '2', '1913188426', 'Ashraf', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(330, '331', 7, NULL, 7, 'NULL', '1', '1', NULL, 'Mostafa', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(331, '332', 7, NULL, 7, 'NULL', '1', '1', '1731804751', 'Azad Ullah', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(332, '333', 7, NULL, 7, 'NULL', '1', '1', '1715862341', 'Major Anwar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(333, '334', 7, NULL, 7, 'NULL', '1', '2', '1763832704', 'Md. Zakir Hossain', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(334, '335', 7, NULL, 7, 'NULL', '1', '1', '1715167986', 'Md. Shahin Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(335, '336', 7, NULL, 7, 'NULL', '1', '1', NULL, 'Md. Moinul Islam ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(336, '337', 7, NULL, 7, 'NULL', '1', '1', NULL, 'Sk. Mohammad Anwar Hossain Kobir ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(337, '338', 7, NULL, 7, 'NULL', '1', '1', NULL, 'Md. Ahsan Ali Chowdhury', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(338, '339', 7, NULL, 7, 'NULL', '1', '1', '1913566506', 'Md. Monirul Haque Chowdhury ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(339, '340', 7, NULL, 7, 'NULL', '1', '1', NULL, 'Mohammad Shamim Akhtar ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(340, '341', 7, NULL, 7, 'NULL', '1', '1', NULL, 'Nazrul', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(341, '342', 7, NULL, 7, 'NULL', '1', '1', '1713332056', 'Md. Minarul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(342, '343', 7, NULL, 7, 'NULL', '1', '1', NULL, 'M. M. Asadullah Galib', NULL, NULL, NULL, NULL, 'NULL', 'online_payment', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(343, '344', 7, NULL, 7, 'NULL', '1', '1', NULL, 'Doctor Kamrul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(344, '345', 7, NULL, 7, 'NULL', '1', '2', '1769020302', 'Kamran Rashid', '1', '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(345, '346', 7, NULL, 7, 'NULL', '1', '1', NULL, 'Major Abdul Matin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(346, '347', 7, NULL, 7, 'NULL', '1', '2', '1712094560', 'Taimur Rahman', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(347, '348', 7, NULL, 7, 'NULL', '1', '1', '1718311479', 'Shakil Aziz', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(348, '349', 7, NULL, 7, 'NULL', '1', '1', '1738193571', 'Md. Ibrahim Hossain ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(349, '350', 7, NULL, 7, 'NULL', '1', '2', '1674796342', 'Hafijur Rahman', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(350, '351', 7, NULL, 7, 'NULL', '1', '1', NULL, 'Late Md. Tariqul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(351, '352', 7, NULL, 7, 'NULL', '1', '1', '1916100810', 'Rafiqul Islam ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(352, '353', 7, NULL, 7, 'NULL', '1', '2', '1711860374', 'Humayun Kabir', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(353, '354', 7, NULL, 7, 'NULL', '1', '1', NULL, 'Nazmul Ehsaan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(354, '355', 7, NULL, 7, 'NULL', '1', '1', '1711075464', 'Mozammel Haque Ripon ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(355, '356', 7, NULL, 7, 'NULL', '1', '2', '1775019535', 'Ehsaan Mahboob', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(356, '357', 7, NULL, 7, 'NULL', '1', '2', '1913631090', 'A. K. M. Shafiqul Alam', NULL, '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(357, '358', 7, NULL, 7, 'NULL', '1', '2', '1552314166', 'Sharifur Rahman', NULL, '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(358, '359', 7, NULL, 7, 'NULL', '1', '1', '1911666606', 'Mejbahul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(359, '360', 7, NULL, 7, 'NULL', '1', '2', '1715082538', 'Mahboob Alam ', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(360, '361', 7, NULL, 7, 'NULL', '1', '2', '1716302666', 'Atiqur Rahman ', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(361, '362', 7, NULL, 7, 'NULL', '1', '1', NULL, 'Mojibul Akbar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(362, '363', 7, NULL, 7, 'NULL', '1', '2', '1711322487', 'Kazi Md. Noor-ul Ferdous', NULL, '1', 'NULL', '20000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(363, '364', 7, NULL, 7, 'NULL', '1', '1', '1711204504', 'S. M. Mohmudul Hasan ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(364, '365', 7, NULL, 7, 'NULL', '1', '1', '1675431336', 'Mohiuddin Polash', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(365, '366', 7, NULL, 7, 'NULL', '1', '1', '1713044822', 'Md. Monowar hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(366, '367', 7, NULL, 7, 'NULL', '1', '1', '1680302432', 'Belal Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(367, '368', 7, NULL, 7, 'NULL', '1', '2', NULL, 'Azad Amin', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(368, '369', 7, NULL, 7, 'NULL', '1', '2', ',1819285976,1716282888', 'Aslam Md. Aurangjeb', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(369, '370', 7, NULL, 7, 'NULL', '1', '1', '1731316069', 'Mizanur Rahman Liton', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(370, '371', 7, NULL, 8, 'NULL', '1', '1', NULL, 'Late Md. Mahbubur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(371, '372', 7, NULL, 8, 'NULL', '1', '1', '1716676455', 'Riad Hasan Sinha', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(372, '373', 7, NULL, 8, 'NULL', '1', '2', '1712051159', 'Md. Sultan Rumi Khan ( Liton)', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(373, '374', 7, NULL, 8, 'NULL', '1', '1', '1713082406', 'S. Iftekhar Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(374, '375', 7, NULL, 8, 'NULL', '1', '1', '1716222453', 'Md. Sher Ali Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(375, '376', 7, NULL, 8, 'NULL', '1', '1', '1819873082', 'Abdus Sattar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(376, '377', 7, NULL, 8, 'NULL', '1', '1', '1711106819', 'Md. Zubayer Hasnat', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(377, '378', 7, NULL, 8, 'NULL', '1', '1', '1715026585', 'Noor Alam Siddique', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(378, '379', 7, NULL, 8, 'NULL', '1', '2', '1715468517', 'Mohammed Omer Faruq', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(379, '380', 7, NULL, 8, 'NULL', '1', '1', '1191759942', 'Fazal Reza', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(380, '381', 7, NULL, 8, 'NULL', '1', '1', '1711950464', 'Md. Shahidullah Humayun Kabir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(381, '382', 7, NULL, 8, 'NULL', '1', '1', '1778573500', 'Md. Emranul Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(382, '383', 7, NULL, 8, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(383, '384', 7, NULL, 8, 'NULL', '1', '1', NULL, 'Md. Nazmul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(384, '385', 7, NULL, 8, 'NULL', '1', '1', NULL, 'S.M Saiful Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(385, '386', 7, NULL, 8, 'NULL', '1', '2', '1711133695', 'Md. Sagir Ahmed', NULL, '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(386, '387', 7, NULL, 8, 'NULL', '1', '1', '1711963506', 'Md.Anwar Kabir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(387, '388', 7, NULL, 8, 'NULL', '1', '1', '1855596628', 'Md. Sohel Kudrat-E-Khuda', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(388, '389', 7, NULL, 8, 'NULL', '1', '2', '1711030905', 'Md. Asaduzzaman', '1', '1', 'NULL', '20000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(389, '390', 7, NULL, 8, 'NULL', '1', '2', '1711503802', 'Johirul Islam', '1', '1', 'NULL', '20000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(390, '391', 7, NULL, 8, 'NULL', '1', '1', NULL, 'Md.Sadrud-Doza Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(391, '392', 7, NULL, 8, 'NULL', '1', '1', '1716431865', 'Sohel Md.Samsuddin Firoz', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(392, '393', 7, NULL, 8, 'NULL', '1', '1', '1711277188', 'Shantanu Shaha', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(393, '394', 7, NULL, 8, 'NULL', '1', '1', '1199803542', 'Noor Alam ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(394, '395', 7, NULL, 8, 'NULL', '1', '1', '1713304885', 'Md.Babul Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(395, '396', 7, NULL, 8, 'NULL', '1', '1', '1711478897', 'Md. Rasedul Karim', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(396, '397', 7, NULL, 8, 'NULL', '1', '2', '1711898098', 'J.M.H Quameel Alam', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(397, '398', 7, NULL, 8, 'NULL', '1', '1', '1715333808', 'G.M. Shafiqul Kabir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(398, '399', 7, NULL, 8, 'NULL', '1', '1', NULL, 'Khondoker Abdullah Sayeed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(399, '400', 7, NULL, 8, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(400, '401', 7, NULL, 8, 'NULL', '1', '1', '1714473903, 1949754024', 'Saiful Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(401, '402', 7, NULL, 8, 'NULL', '1', '1', '1712532908', 'Md. Saiful Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(402, '403', 7, NULL, 8, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(403, '404', 7, NULL, 8, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(404, '405', 7, NULL, 8, 'NULL', '1', '2', NULL, 'NULL', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(405, '406', 7, NULL, 8, 'NULL', '1', '1', '1727550272', 'Md. Abul Kalam Azad', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(406, '407', 7, NULL, 8, 'NULL', '1', '1', '1911722803', 'Gazi Parvez Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(407, '408', 7, NULL, 8, 'NULL', '1', '1', '1716431865', 'Firoz', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(408, '409', 7, NULL, 8, 'NULL', '1', '1', NULL, 'Imtiaz Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(409, '410', 7, NULL, 8, 'NULL', '1', '1', '1713362812, 1769012572', 'Ashikur Rahim', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(410, '411', 7, NULL, 8, 'NULL', '1', '1', '1199086399', 'Md. Moinul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(411, '412', 7, NULL, 8, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(412, '413', 7, NULL, 8, 'NULL', '1', '1', '1715027614', 'A.H.M. Mahmudur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(413, '414', 7, NULL, 8, 'NULL', '1', '1', '1922113997', 'Major Md Sazzad Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(414, '415', 7, NULL, 8, 'NULL', '1', '1', NULL, 'Shahinur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(415, '416', 7, NULL, 8, 'NULL', '1', '1', '1711858603', 'Md. Masud Biswash', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(416, '417', 7, NULL, 8, 'NULL', '1', '1', '1769005099', 'Ahmad Tarik Kabir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(417, '418', 7, NULL, 8, 'NULL', '1', '1', NULL, 'Md, Anzam Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(418, '419', 7, NULL, 8, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(419, '421', 7, NULL, 8, 'NULL', '1', '2', NULL, 'Kamal Hossain', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(420, '422', 7, NULL, 9, 'NULL', '1', '1', NULL, 'Sharif Faysal Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(421, '423', 7, NULL, 9, 'NULL', '1', '1', '1730426798', 'Mirza Selim Arman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(422, '424', 7, NULL, 9, 'NULL', '1', '2', '1711538199', 'Md. Imrul Alam', NULL, NULL, 'NULL', '20000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(423, '425', 7, NULL, 9, 'NULL', '1', '2', '1717940090', 'Shah Alam', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(424, '426', 7, NULL, 9, 'NULL', '1', '1', '1713113672', 'Md. Sultan Hayat Khan ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(425, '427', 7, NULL, 9, 'NULL', '1', '1', '1713097727', 'Ahmad Murtaza Reza', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(426, '428', 7, NULL, 9, 'NULL', '1', '1', '1819726710', 'Abdullah Al Morshed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(427, '429', 7, NULL, 9, 'NULL', '1', '1', '1711383929', 'Md. Asaduzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(428, '430', 7, NULL, 9, 'NULL', '1', '1', '1711907292', 'Md. Tofayel Uddin Sikder', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(429, '431', 7, NULL, 9, 'NULL', '1', '1', '1713399715', 'Md. Saifulla Dostogir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(430, '432', 7, NULL, 9, 'NULL', '1', '1', NULL, 'Robiul Hasnat', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(431, '433', 7, NULL, 9, 'NULL', '1', '1', '1916466639', 'Md. Alamgir Hossain ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(432, '434', 7, NULL, 9, 'NULL', '1', '1', NULL, 'Md. Mujibul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(433, '435', 7, NULL, 9, 'NULL', '1', '1', '1712145008', 'Md. Arifuzzaman ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(434, '436', 7, NULL, 9, 'NULL', '1', '1', '1610001600', 'Md. Kamrul Hasan ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(435, '437', 7, NULL, 9, 'NULL', '1', '1', '1711349530', 'Md. Khalilullah Asraf', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(436, '438', 7, NULL, 9, 'NULL', '1', '1', NULL, 'Maruf ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(437, '439', 7, NULL, 9, 'NULL', '1', '2', '1730332500', 'Sheikh Shaer Hasan ', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(438, '440', 7, NULL, 9, 'NULL', '1', '1', NULL, 'Md. Aminul Haque  ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(439, '441', 7, NULL, 9, 'NULL', '1', '1', NULL, 'Md. Ul-Ul-Azam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(440, '442', 7, NULL, 9, 'NULL', '1', '1', '1714061424', 'Md.Imrul Mabud', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(441, '443', 7, NULL, 9, 'NULL', '1', '1', NULL, 'Md. Shafiul Azam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(442, '444', 7, NULL, 9, 'NULL', '1', '2', '1712871135', 'Shafiqul Islam', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(443, '445', 7, NULL, 9, 'NULL', '1', '1', NULL, 'Ahmed Bazlur Rahman hayati', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(444, '446', 7, NULL, 9, 'NULL', '1', '1', '1716597305', 'Golam Ahmad', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(445, '447', 7, NULL, 9, 'NULL', '1', '1', NULL, 'Masun Bin Mahbub', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(446, '448', 7, NULL, 9, 'NULL', '1', '1', '1713375420', 'Rawshanul Firoz', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(447, '449', 7, NULL, 9, 'NULL', '1', '1', NULL, 'Ahmed Ashfaque Noman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(448, '450', 7, NULL, 9, 'NULL', '1', '1', NULL, 'Abdur Rob', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(449, '451', 7, NULL, 9, 'NULL', '1', '2', '1720044792', 'Faiz Ahmed', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(450, '452', 7, NULL, 9, 'NULL', '1', '1', '1711110770', 'Md. Monir-uz-zaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(451, '453', 7, NULL, 9, 'NULL', '1', '2', '1715039372', 'Md. Hossain Reza', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(452, '454', 7, NULL, 9, 'NULL', '1', '1', NULL, 'Mostafiz', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(453, '455', 7, NULL, 9, 'NULL', '1', '1', NULL, 'Md. Firoz Murad Hassan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(454, '456', 7, NULL, 9, 'NULL', '1', '1', '1713044999', 'Ferdous Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(455, '457', 7, NULL, 9, 'NULL', '1', '1', NULL, 'Abu Bakar Siddique', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(456, '458', 7, NULL, 9, 'NULL', '1', '2', '1715764041', 'Zia Ahmed', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(457, '459', 7, NULL, 9, 'NULL', '1', '1', '1678034121', 'Anwar Hossain ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(458, '460', 7, NULL, 9, 'NULL', '1', '1', NULL, 'Md. Mizanur Rahaman Chowdhury', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(459, '461', 7, NULL, 9, 'NULL', '1', '1', NULL, 'A.T.M. shameem Ahsan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(460, '462', 7, NULL, 9, 'NULL', '1', '1', NULL, 'Md. Abu said naser', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(461, '463', 7, NULL, 9, 'NULL', '1', '1', '1713329693', 'Md. Masud-Uz-Zaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(462, '464', 7, NULL, 9, 'NULL', '1', '1', NULL, 'Md. Ahsan Sayeed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(463, '465', 7, NULL, 9, 'NULL', '1', '2', '1713612741', 'Mahmudur Rahman Bhuiyan', '1', '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(464, '466', 7, NULL, 9, 'NULL', '1', '1', NULL, 'Md. Humayun Kabir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(465, '467', 7, NULL, 9, 'NULL', '1', '2', '1977885512', 'Sattique Ahmed ', '1', '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(466, '468', 7, NULL, 9, 'NULL', '1', '1', NULL, 'Md. Kamal Uddin Komol ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(467, '469', 7, NULL, 9, 'NULL', '1', '1', '1610007702', 'Zahangir Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(468, '470', 7, NULL, 9, 'NULL', '1', '1', NULL, 'Shiekh Rahmat Ali', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(469, '521', 7, NULL, 9, 'NULL', '1', '1', NULL, 'Safat', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(470, '471', 7, NULL, 10, 'NULL', '1', '1', '1711066285', 'Khandakar Ali Kawser', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(471, '472', 7, NULL, 10, 'NULL', '1', '1', '1715077452', 'Md. Abdul Matin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(472, '473', 7, NULL, 10, 'NULL', '1', '1', '1710870151', 'Md. Tawfiq Al Mahmud', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(473, '474', 7, NULL, 10, 'NULL', '1', '1', '1914402022', 'Nur- Un- Nabi', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(474, '475', 7, NULL, 10, 'NULL', '1', '1', NULL, 'Md. Fakhruddin Khandakar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(475, '476', 7, NULL, 10, 'NULL', '1', '1', NULL, 'Saifur Rahman Milon', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(476, '477', 7, NULL, 10, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(477, '478', 7, NULL, 10, 'NULL', '1', '1', NULL, 'Sk. Wareshul Habib', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(478, '479', 7, NULL, 10, 'NULL', '1', '1', NULL, 'Mohammad Arif Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(479, '480', 7, NULL, 10, 'NULL', '1', '1', '1912348768', 'Raju Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(480, '481', 7, NULL, 10, 'NULL', '1', '1', NULL, 'Md. Mahfuzur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(481, '482', 7, NULL, 10, 'NULL', '1', '1', NULL, 'Khandakar Saiful Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(482, '483', 7, NULL, 10, 'NULL', '1', '1', NULL, 'Major Kudrat-E-Khuda', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(483, '484', 7, NULL, 10, 'NULL', '1', '1', '1913570893', 'Md. Asaduzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(484, '485', 7, NULL, 10, 'NULL', '1', '1', '1715003560', 'Abdullah-Al-Baqi', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(485, '486', 7, NULL, 10, 'NULL', '1', '1', '1610003838', 'Md. Ali Zakir', NULL, NULL, 'NULL', NULL, 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(486, '487', 7, NULL, 10, 'NULL', '1', '1', NULL, 'Md. Ismail', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(487, '488', 7, NULL, 10, 'NULL', '1', '1', NULL, 'Abdullah-Al-Mashuk', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(488, '489', 7, NULL, 10, 'NULL', '1', '1', NULL, 'Md. Rafiqul Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(489, '490', 7, NULL, 10, 'NULL', '1', '1', '1914498070', 'Md. Zakir Hasan Nur', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(490, '491', 7, NULL, 10, 'NULL', '1', '1', NULL, 'Md. Hamid Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(491, '492', 7, NULL, 10, 'NULL', '1', '1', '1716237163', 'Bishwajit Kumar Paul', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(492, '493', 7, NULL, 10, 'NULL', '1', '1', NULL, 'Shah Mahamudul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(493, '494', 7, NULL, 10, 'NULL', '1', '1', '1721493690', 'A.K.M Azad', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(494, '495', 7, NULL, 10, 'NULL', '1', '1', NULL, 'F.M.Nur-Un-Nabi', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(495, '496', 7, NULL, 10, 'NULL', '1', '1', '1713375885', 'Md. Shanzidul Bari', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(496, '497', 7, NULL, 10, 'NULL', '1', '1', '1714103674', 'S.M. Shaheedul Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(497, '498', 7, NULL, 10, 'NULL', '1', '1', NULL, 'A.T.M. Shahid Ahsan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(498, '499', 7, NULL, 10, 'NULL', '1', '2', '1711506852', 'S.M. Zahid Al Mamun', NULL, '1', 'NULL', '10000', 'NULL', 'home_delivery', NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(499, '500', 7, NULL, 10, 'NULL', '1', '1', NULL, 'Mazharul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(500, '501', 7, NULL, 10, 'NULL', '1', '1', '1195001650', 'Ashfaq Rahat Siddique', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(501, '502', 7, NULL, 10, 'NULL', '1', '1', NULL, 'Md. Arif Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(502, '503', 7, NULL, 10, 'NULL', '1', '1', NULL, 'Md. Abul Bashar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(503, '504', 7, NULL, 10, 'NULL', '1', '1', NULL, 'Ahmmed Jobaer', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(504, '505', 7, NULL, 10, 'NULL', '1', '1', NULL, 'Mahbub-Ul-Hakim', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(505, '506', 7, NULL, 10, 'NULL', '1', '2', '1713337422', 'Md. Abdul Alim', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(506, '507', 7, NULL, 10, 'NULL', '1', '1', '1713337422', 'Md. Ashraful Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(507, '508', 7, NULL, 10, 'NULL', '1', '1', NULL, 'Md. Maqdummul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(508, '509', 7, NULL, 10, 'NULL', '1', '2', '1729095686', 'Saif Mohammad Anwar', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(509, '510', 7, NULL, 10, 'NULL', '1', '1', NULL, 'Md. Momsad Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(510, '511', 7, NULL, 10, 'NULL', '1', '1', '1746929394', 'Kazi Mahmudunnabi(Sohel)', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(511, '512', 7, NULL, 10, 'NULL', '1', '2', '1714170055', 'Md. Abdul Matin Emon', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(512, '513', 7, NULL, 10, 'NULL', '1', '1', NULL, 'Md. Kamruzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(513, '514', 7, NULL, 10, 'NULL', '1', '1', '1713080065', 'Asif Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(514, '515', 7, NULL, 10, 'NULL', '1', '1', NULL, 'Khandakar Ershad Alahi', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(515, '516', 7, NULL, 10, 'NULL', '1', '1', '1711459792', 'Md. Romeo Nowreen Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(516, '517', 7, NULL, 10, 'NULL', '1', '2', '1712296999', 'Md. Fokhrul Hasan', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(517, '518', 7, NULL, 10, 'NULL', '1', '1', NULL, 'Shafiul Azam Rabbani', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(518, '519', 7, NULL, 10, 'NULL', '1', '1', '1730040346', 'Md. Wahidul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(519, '520', 7, NULL, 10, 'NULL', '1', '1', NULL, 'Md. Asaduzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(520, '522', 7, NULL, 11, 'NULL', '1', '1', NULL, 'Dewan Gazzali', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(521, '523', 7, NULL, 11, 'NULL', '1', '1', NULL, 'A.K.M. Ahsanul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(522, '524', 7, NULL, 11, 'NULL', '1', '2', NULL, 'Md. Mohidur Rahaman', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(523, '525', 7, NULL, 11, 'NULL', '1', '1', NULL, 'Md. Abu Nasir Mia', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(524, '526', 7, NULL, 11, 'NULL', '1', '1', '1720915225', 'Md. Kamrul Hassan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(525, '527', 7, NULL, 11, 'NULL', '1', '1', '1714484015', 'Asif Mahmud Faruk', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(526, '528', 7, NULL, 11, 'NULL', '1', '1', NULL, 'Md. Nadiruzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(527, '529', 7, NULL, 11, 'NULL', '1', '1', NULL, 'Mehedi Milon', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(528, '530', 7, NULL, 11, 'NULL', '1', '1', '1712784086', 'Md. Arifuzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(529, '531', 7, NULL, 11, 'NULL', '1', '2', '1713245367', 'Kazi Md. Mizan', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(530, '532', 7, NULL, 11, 'NULL', '1', '1', '1713426954', 'A S M Nazim,ul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(531, '533', 7, NULL, 11, 'NULL', '1', '1', NULL, 'Tanzim Faruk', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(532, '534', 7, NULL, 11, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(533, '535', 7, NULL, 11, 'NULL', '2', '1', '1745894344', 'Arif Ahmed Dipu', '1', '1', '2400', 'NULL', 'NULL', 'home_delivery', NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(534, '536', 7, NULL, 11, 'NULL', '1', '1', NULL, 'Jamal Pathan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(535, '537', 7, NULL, 11, 'NULL', '1', '2', '1712014722', 'Md. Iftekharul Mabud', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(536, '538', 7, NULL, 11, 'NULL', '1', '1', '1916763012', 'Mohammad Moqsudul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(537, '539', 7, NULL, 11, 'NULL', '1', '2', '1711505129', 'Md. Tanzirul Islam', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(538, '540', 7, NULL, 11, 'NULL', '1', '1', '1730187534', 'Sheikh Mohammad Ali', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(539, '541', 7, NULL, 11, 'NULL', '1', '1', '1720915225', 'Md. Kamrul Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(540, '542', 7, NULL, 11, 'NULL', '1', '1', '1712830079', 'Faruque Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(541, '543', 7, NULL, 11, 'NULL', '1', '1', NULL, 'Mehedi Al-Masud', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(542, '544', 7, NULL, 11, 'NULL', '1', '1', NULL, 'Talukder AGM Zaki ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(543, '545', 7, NULL, 11, 'NULL', '1', '2', NULL, 'Md. Nazmul Hasan', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(544, '546', 7, NULL, 11, 'NULL', '1', '1', NULL, 'Md. Ashraful Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(545, '547', 7, NULL, 11, 'NULL', '1', '1', '1711167286', 'Md. Sakhawat Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(546, '548', 7, NULL, 11, 'NULL', '1', '1', '1552456383', 'Md. Shameem Iftekhar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(547, '549', 7, NULL, 11, 'NULL', '1', '1', '1914421144', 'Khaled Mahmud', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(548, '550', 7, NULL, 11, 'NULL', '1', '1', '1914421144', 'Md. Humayun Kabir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(549, '551', 7, NULL, 11, 'NULL', '1', '1', NULL, 'Md. Tauhidul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(550, '552', 7, NULL, 11, 'NULL', '1', '1', NULL, 'Z.M. Sarwat Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(551, '553', 7, NULL, 11, 'NULL', '1', '1', '1714043370', 'Firoz kabir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(552, '554', 7, NULL, 11, 'NULL', '1', '2', NULL, 'Shafi-ul Islam', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(553, '555', 7, NULL, 11, 'NULL', '1', '1', '1712070191', 'Mehedi Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(554, '556', 7, NULL, 11, 'NULL', '1', '1', NULL, 'Md. Nurunnabi Mithu', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(555, '557', 7, NULL, 11, 'NULL', '1', '1', NULL, 'Sk. Md. Idrish Ali', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(556, '558', 7, NULL, 11, 'NULL', '1', '1', NULL, 'Komor', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(557, '559', 7, NULL, 11, 'NULL', '1', '1', NULL, 'Md. Mehedi Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(558, '560', 7, NULL, 11, 'NULL', '1', '1', NULL, 'Md. Khaled Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(559, '561', 7, NULL, 11, 'NULL', '1', '2', NULL, 'Shah Mostafa Sarwar', '1', '1', 'NULL', '10000', 'NULL', 'home_delivery', NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(560, '562', 7, NULL, 11, 'NULL', '1', '1', NULL, 'Md. Iqbal Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(561, '563', 7, NULL, 11, 'NULL', '1', '1', NULL, 'Md. Afjalur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(562, '564', 7, NULL, 11, 'NULL', '1', '1', NULL, 'Sated Imam Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(563, '565', 7, NULL, 11, 'NULL', '1', '1', '1911566839', 'Rokonuzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(564, '566', 7, NULL, 11, 'NULL', '1', '1', NULL, 'Omar Sharif', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(565, '567', 7, NULL, 11, 'NULL', '1', '1', NULL, 'Sajjad Hossain Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(566, '568', 7, NULL, 11, 'NULL', '1', '1', NULL, 'Khan Md. Ali Azam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(567, '569', 7, NULL, 11, 'NULL', '1', '1', NULL, 'S. Imtiaz Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(568, '570', 7, NULL, 11, 'NULL', '1', '1', '1715122266', 'Md. Rofiq-uz-zaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(569, '571', 7, NULL, 11, 'NULL', '2', '1', '1817518514', 'NULL', NULL, NULL, '2400', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(570, '572', 7, NULL, 12, 'NULL', '1', '1', NULL, 'Md. Imran Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(571, '573', 7, NULL, 12, 'NULL', '1', '1', '1911310704', 'Niloy Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(572, '574', 7, NULL, 12, 'NULL', '1', '2', '1711312926', 'Md. Shahriar Mizan', NULL, '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(573, '575', 7, NULL, 12, 'NULL', '2', '1', NULL, 'Shah Julfikar Ali', '1', '1', '2400', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(574, '576', 7, NULL, 12, 'NULL', '1', '1', NULL, 'Khandakar Masud Parvez', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(575, '577', 7, NULL, 12, 'NULL', '1', '1', '1711064300', 'Md. Jahangir Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(576, '578', 7, NULL, 12, 'NULL', '1', '1', '1711420512', 'Syed Anwar Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(577, '579', 7, NULL, 12, 'NULL', '1', '1', '1736872796', 'S.M. Razib Ameen Rana', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(578, '580', 7, NULL, 12, 'NULL', '1', '1', NULL, 'Md. Munim Shahriar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(579, '581', 7, NULL, 12, 'NULL', '1', '1', NULL, 'Md. Shihabul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(580, '582', 7, NULL, 12, 'NULL', '1', '1', NULL, 'Late Md. Shakwat Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(581, '583', 7, NULL, 12, 'NULL', '1', '1', NULL, 'A.T.M. Rasel Ahsan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(582, '584', 7, NULL, 12, 'NULL', '1', '1', NULL, 'Md. Muniruzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(583, '585', 7, NULL, 12, 'NULL', '1', '1', NULL, 'Ahmed Ibn Sayeed Khan Sujon', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(584, '586', 7, NULL, 12, 'NULL', '1', '1', NULL, 'Md. Mahbub Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(585, '587', 7, NULL, 12, 'NULL', '1', '1', NULL, 'Asaduzzaman Biswas', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(586, '588', 7, NULL, 12, 'NULL', '1', '1', '1712540468', 'Md. Zahid Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(587, '589', 7, NULL, 12, 'NULL', '1', '2', '1919261266', 'Md. Imtiaz Hossain', NULL, '1', NULL, '10000', 'NULL', 'home_delivery', NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(588, '590', 7, NULL, 12, 'NULL', '1', '1', NULL, 'Late Md. Razibul Kabir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(589, '591', 7, NULL, 12, 'NULL', '1', '1', NULL, 'Md. Mehedi Hossain Kabir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(590, '592', 7, NULL, 12, 'NULL', '1', '1', NULL, 'Sayeed Sazzad Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(591, '593', 7, NULL, 12, 'NULL', '1', '2', NULL, 'Md. Jahirul Islam', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(592, '594', 7, NULL, 12, 'NULL', '1', '1', NULL, 'S.M. Touhidul Hamid', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(593, '595', 7, NULL, 12, 'NULL', '1', '1', NULL, 'Md. Zakiur Rahaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(594, '596', 7, NULL, 12, 'NULL', '1', '1', NULL, 'S.M. Masud Rana', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(595, '597', 7, NULL, 12, 'NULL', '1', '2', NULL, 'Nur Hassn Khan Yusuf Ali', NULL, '1', 'NULL', '10000', 'NULL', 'home_delivery', NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(596, '598', 7, NULL, 12, 'NULL', '1', '2', NULL, 'Joynul Abedin Chistee', NULL, NULL, 'NULL', '20000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(597, '599', 7, NULL, 12, 'NULL', '1', '1', NULL, 'S.M. Ashiq Mahmud', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(598, '600', 7, NULL, 12, 'NULL', '1', '1', '1713061511', 'Md. Toufiq Arefin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(599, '601', 7, NULL, 12, 'NULL', '1', '1', NULL, 'Md. Azharul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(600, '602', 7, NULL, 12, 'NULL', '1', '1', '1715708802', 'Md. Mohibul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(601, '603', 7, NULL, 12, 'NULL', '1', '1', '1912076951', 'Mehedi Md. Iftikhar Uddin Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(602, '604', 7, NULL, 12, 'NULL', '1', '1', NULL, 'Rana Raihan Md. Faruk', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(603, '605', 7, NULL, 12, 'NULL', '1', '2', '1755553525', 'Sayed Sahed Ahsan Kabir', '1', '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(604, '606', 7, NULL, 12, 'NULL', '1', '1', NULL, 'Rayhan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(605, '607', 7, NULL, 12, 'NULL', '1', '1', '1733616033', 'Md. Rezaul Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(606, '608', 7, NULL, 12, 'NULL', '1', '1', '1718068987', 'Sahed Parvez', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(607, '609', 7, NULL, 12, 'NULL', '1', '1', NULL, 'Md. Khaledun Nabi', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(608, '610', 7, NULL, 12, 'NULL', '1', '1', NULL, 'Md. Zakir Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(609, '611', 7, NULL, 12, 'NULL', '1', '2', NULL, 'Mirza Golam Hafiz', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(610, '612', 7, NULL, 12, 'NULL', '1', '1', '1912311599', 'A.K.M Ehsanul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(611, '613', 7, NULL, 12, 'NULL', '1', '2', '1727224471', 'Md. Ariful Islam', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(612, '614', 7, NULL, 12, 'NULL', '1', '1', NULL, 'Md. Mahmud-Bin-Hasnat', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(613, '615', 7, NULL, 12, 'NULL', '1', '1', NULL, 'Md. Nayeemul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(614, '616', 7, NULL, 12, 'NULL', '1', '1', NULL, 'Md. Sayeed-Bin-Sharif', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(615, '617', 7, NULL, 12, 'NULL', '1', '2', '1713144287', 'Md. Ziaul Kabir', NULL, NULL, 'NULL', '20000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(616, '618', 7, NULL, 12, 'NULL', '1', '2', NULL, 'ATM Nazmul Huda', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(617, '619', 7, NULL, 12, 'NULL', '1', '1', NULL, 'Md. Shahriar Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(618, '620', 7, NULL, 12, 'NULL', '1', '1', NULL, 'Md. Ahsanul Karim', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(619, '621', 7, NULL, 12, 'NULL', '1', '1', '1711175422', 'Md. Zahid Parvez', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(620, '622', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Dewan Saleh Uddin Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(621, '623', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Md. Rasel Kabir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(622, '624', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Md. Faisal Afsar chowdhury', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(623, '625', 7, NULL, 13, 'NULL', '1', '1', '1715841908', 'Shamsul Karim Manik', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(624, '626', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Md. Shahidul Islam Bin Helal', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(625, '627', 7, NULL, 13, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(626, '628', 7, NULL, 13, 'NULL', '1', '2', '1712170766', 'Md. Mahbubur Rahman Khan', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(627, '629', 7, NULL, 13, 'NULL', '1', '1', '1712938433', 'Khandokar Riad Din Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(628, '630', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Saifuddin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(629, '631', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Md. Tanvir Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(630, '632', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Md. Shazedur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `college_validation` (`college_validation_id`, `student_number`, `college_id`, `year_id`, `batch_id`, `booking_status`, `is_anual_subcribe`, `is_life_time_subscribe`, `contact_number`, `student_name`, `is_spouse`, `is_self`, `anual_subsciption_cost`, `life_time_subscription_cost`, `token_no`, `subcription_delivery_method`, `customer_address`, `city`, `discrict_id`, `zip_code`, `val_id`, `is_online`, `payment_status`) VALUES
(631, '633', 7, NULL, 13, 'NULL', '1', '1', NULL, 'B. M Alim Hoossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(632, '634', 7, NULL, 13, 'NULL', '1', '1', '1711574975', 'S.A. Shahan Shah', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(633, '635', 7, NULL, 13, 'NULL', '1', '1', '1610006018', 'Saifullah-Hil-Monir Chowdhury', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(634, '636', 7, NULL, 13, 'NULL', '1', '1', '1725402093', 'Md. Asaduzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(635, '637', 7, NULL, 13, 'NULL', '1', '1', '1716638131', 'Md. Azim-ul-Ahshan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(636, '638', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Mahmudul Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(637, '639', 7, NULL, 13, 'NULL', '1', '1', '1713069997', 'Sanjid Newaz Laskar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(638, '640', 7, NULL, 13, 'NULL', '1', '1', '1199844850', 'Md. Mahbubul Kabir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(639, '641', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Md. Salam Mohammad Rasel', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(640, '642', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Md. Jabed Bin Yusuf', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(641, '643', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Kazi Alomgir Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(642, '644', 7, NULL, 13, 'NULL', '1', '1', '1818112263', 'Mamunur Rashid', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(643, '645', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Md. Jasim Uddin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(644, '646', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Md. Masum Ur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(645, '647', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Sahnaz Mithun', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(646, '648', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Md. Mehbub Islam ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(647, '649', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Md. Nazmul Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(648, '650', 7, NULL, 13, 'NULL', '1', '2', '1711506323', 'Md. Salauddin Shapan', NULL, '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'paid'),
(649, '651', 7, NULL, 13, 'NULL', '1', '2', '1713331774', 'Md. Shariful Islam', NULL, '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'paid'),
(650, '652', 7, NULL, 13, 'NULL', '1', '1', '1819210476', 'Md. Shafiullah', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(651, '653', 7, NULL, 13, 'NULL', '1', '1', '1191146700', 'Md. Shajjadur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(652, '654', 7, NULL, 13, 'NULL', '1', '1', '1818112263', 'Mahmudul Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(653, '655', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Md. Abbas Uddin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(654, '656', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Md. Sariful Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(655, '657', 7, NULL, 13, 'NULL', '1', '1', NULL, 'A.K. Rafi Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(656, '658', 7, NULL, 13, 'NULL', '1', '1', '1199844850', 'Mahmudul Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(657, '659', 7, NULL, 13, 'NULL', '1', '1', '1816899643', 'S M Ahsan Habib', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(658, '660', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Md. Muntasir Mamun Joarder', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(659, '661', 7, NULL, 13, 'NULL', '1', '1', '1818112263', 'MD. Mamunur Rashid', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(660, '662', 7, NULL, 13, 'NULL', '1', '1', '1713479820', 'Md.Sohel Rana', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(661, '663', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Mamunur Rasid', NULL, NULL, NULL, 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(662, '664', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Iqbal Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(663, '665', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Mirza Imtiaj Beg', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(664, '666', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Shahadat', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(665, '667', 7, NULL, 13, 'NULL', '1', '1', '1715938394', 'Khondaker Mohamed Mostafa', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(666, '668', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Md. Niaz Mahmud alim Haq', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(667, '669', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Ruhaniul-Adnan-Zayedi', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(668, '670', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Md. Khoshru Ahmed Jewel', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(669, '671', 7, NULL, 13, 'NULL', '1', '1', NULL, 'Minhaz', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(670, '672', 7, NULL, 14, 'NULL', '1', '2', '1819556017', 'Ferdous Ibne Bari', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(671, '673', 7, NULL, 14, 'NULL', '1', '1', '1732048777', 'AHM Mustafizur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(673, '675', 7, NULL, 14, 'NULL', '1', '2', '1819128471', 'Sayed Zaved Zakaria', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(674, '676', 7, NULL, 14, 'NULL', '1', '1', '16472616676', 'Nazmul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(675, '677', 7, NULL, 14, 'NULL', '1', '2', '1711373637', 'Rasel Ibna Shiraj', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(676, '678', 7, NULL, 14, 'NULL', '1', '1', '1552446541', 'Md. Kamrul Islam Bin Hellal', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(677, '679', 7, NULL, 14, 'NULL', '1', '1', '1911040171', 'Md. Atiqur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(678, '680', 7, NULL, 14, 'NULL', '1', '1', '1712077027', 'S M Aminul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(679, '681', 7, NULL, 14, 'NULL', '1', '2', '1713148980', 'Limon Mahmud', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(680, '682', 7, NULL, 14, 'NULL', '1', '1', '1713331948', 'G M Razeeb Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(681, '683', 7, NULL, 14, 'NULL', '1', '1', '1716584037', 'Abu Sayed Md. Gulam Rasul', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(682, '684', 7, NULL, 14, 'NULL', '1', '1', '1711701274', 'Md. Raihanur Rashid Siddiuee', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(683, '685', 7, NULL, 14, 'NULL', '1', '1', '1914128908', 'Md. Iblul Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(684, '686', 7, NULL, 14, 'NULL', '1', '1', '1196250323', 'Md. Omar Faruk', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(685, '687', 7, NULL, 14, 'NULL', '1', '1', '1711170109', 'Md. Imran Kader Chowdhury', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(686, '688', 7, NULL, 14, 'NULL', '1', '2', '1711381118', 'Md. Fazla Rabby Khan', '1', '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(687, '689', 7, NULL, 14, 'NULL', '1', '2', '1817184327', 'Md. Rafiqul Islam', '1', '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(688, '690', 7, NULL, 14, 'NULL', '1', '1', '1919042841', 'Md. Walid Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(689, '691', 7, NULL, 14, 'NULL', '1', '1', '1711506099', 'Farhadul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(690, '692', 7, NULL, 14, 'NULL', '1', '1', '1813835345', 'Alam Sharif Ahsan Siddiky', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(691, '693', 7, NULL, 14, 'NULL', '1', '1', '1819815959', 'Md.Sadat Al Hasan Talukder', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(692, '694', 7, NULL, 14, 'NULL', '1', '1', '1611507420', 'Md. Ahsanul Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(693, '695', 7, NULL, 14, 'NULL', '1', '2', '1715506030', 'Md. Lutful Arif', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(694, '696', 7, NULL, 14, 'NULL', '1', '1', '1914403936', 'Md. Emdadul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(695, '697', 7, NULL, 14, 'NULL', '1', '1', '61401173344', 'Sk. Rayhan Ibn Ismail', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(696, '698', 7, NULL, 14, 'NULL', '1', '1', '16477295539', 'Ahammod Ali', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(697, '699', 7, NULL, 14, 'NULL', '1', '1', '12147836607', 'Shahed Shahrier', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(698, '700', 7, NULL, 14, 'NULL', '1', '1', NULL, 'Late Md. Kawser Ahmed Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(699, '701', 7, NULL, 14, 'NULL', '1', '1', '61431634781', 'Tanvir Khan Mishu', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(700, '702', 7, NULL, 14, 'NULL', '1', '2', '1713041187', 'Moazzem Hossain Khan', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(701, '703', 7, NULL, 14, 'NULL', '1', '1', '61468476400', 'Md. Hafizur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(702, '704', 7, NULL, 14, 'NULL', '1', '1', '19172577751', 'Rizwan Mahbub Rasid', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(703, '705', 7, NULL, 14, 'NULL', '1', '1', '1778225283', 'Md. Mosfequr Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(704, '706', 7, NULL, 14, 'NULL', '1', '1', '4.48E+11', 'S M Ashraful Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(705, '707', 7, NULL, 14, 'NULL', '1', '2', '1790707711', 'Md. Abdus Shahid', NULL, NULL, 'NULL', '20000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(706, '708', 7, NULL, 14, 'NULL', '1', '1', '4.48E+11', 'Md. Arif Uddin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(707, '709', 7, NULL, 14, 'NULL', '1', '1', '1741405002', 'Sikdar nasimul Haque Jadu', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(708, '710', 7, NULL, 14, 'NULL', '1', '1', '1714092245', 'Md. Hasauzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(709, '711', 7, NULL, 14, 'NULL', '1', '1', 'Â +447447949396', 'Md. Shahnur Reza', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(710, '712', 7, NULL, 14, 'NULL', '1', '2', '1714020460', 'Abu Naser Md. Abu Sayeed', NULL, '1', 'NULL', '20000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(711, '713', 7, NULL, 14, 'NULL', '1', '2', '1819213729', 'Md. Ziaur Rahman', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(712, '714', 7, NULL, 14, 'NULL', '1', '1', '1922092562', 'Md. Shiohel Mahmud', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(713, '715', 7, NULL, 14, 'NULL', '1', '1', '172226991', 'Md. Shariful Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(714, '716', 7, NULL, 14, 'NULL', '1', '2', '1714225555', 'Nahid Tanvir Mustafa', NULL, NULL, 'NULL', '10000', 'NULL', 'pick_from_point', 'Babylon Hotel & Serviced Apartments, House - ', 'Dhaka', '14', '1206', NULL, NULL, NULL),
(715, '717', 7, NULL, 14, 'NULL', '1', '1', '61411789992', 'Md. Abdullah Al Hafiz', NULL, NULL, 'NULL', NULL, 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(716, '718', 7, NULL, 14, 'NULL', '1', '1', '1818730872', 'Md. Rafiqul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(717, '719', 7, NULL, 14, 'NULL', '1', '1', '1711408917', 'Ibn Monzurul Khalid', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(718, '720', 7, NULL, 14, 'NULL', '1', '1', '1769993102', 'A S M Mustafizul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(719, '721', 7, NULL, 14, 'NULL', '1', '2', '1723667859', 'Shibli Sadek Shakil', NULL, '1', 'NULL', '10450', 'NULL', 'online_payment', NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(720, '722', 7, NULL, 14, 'NULL', '1', '1', '61431332743', 'Khondoker Touhid Imam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(721, '723', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Harun- ur - Rashid', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(722, '724', 7, NULL, 15, 'NULL', '1', '1', '1914611904', 'Kazi Abdullah Sayeed ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(723, '725', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Md. Rashed Hossain Razib ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(724, '726', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Nurul Alam Rajib', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(725, '727', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Sardar Anisul Huq', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(726, '728', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Asif Ahmed Anik', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(727, '729', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Mostakhar Ali Shekh', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(728, '730', 7, NULL, 15, 'NULL', '1', '1', NULL, 'S. M Tanvir Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(729, '731', 7, NULL, 15, 'NULL', '1', '1', '1552313328', 'A F M Enamul Hassan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(730, '732', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Fahim Zibran', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(731, '733', 7, NULL, 15, 'NULL', '2', '1', '1914129817', 'Chowdhury Imtiaz Al Kayed', NULL, '1', '2400', 'NULL', 'NULL', 'home_delivery', NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(732, '734', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Tanvir Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(733, '735', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Md. Nasim-Al- Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(734, '736', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Huda', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(735, '737', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Md. Kamrul Islam ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(736, '738', 7, NULL, 15, 'NULL', '1', '1', '1919541561', 'Habib-un-Nabi Hillol', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(737, '739', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Md. Abdullah Al Faisal', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(738, '740', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Md. Miraz Uddin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(739, '741', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Aninda', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(740, '742', 7, NULL, 15, 'NULL', '1', '1', '1556317012', 'Mehdi Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(741, '743', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Md. Ezaz Masud', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(742, '744', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Asif kabir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(743, '745', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Zubayer Al Mahmud', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(744, '746', 7, NULL, 15, 'NULL', '1', '1', NULL, 'A.S.M.Arifuzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(745, '747', 7, NULL, 15, 'NULL', '1', '1', NULL, 'S.M. Kamrul Hasan', NULL, NULL, 'NULL', NULL, 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(746, '748', 7, NULL, 15, 'NULL', '1', '1', '1711116512', 'Ripon Abdus Salam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(747, '749', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Sondipon Chowdhury', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(748, '750', 7, NULL, 15, 'NULL', '1', '1', '1713332157', 'Ashik', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(749, '751', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Md. Shahinul Islam Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(750, '752', 7, NULL, 15, 'NULL', '2', '1', '1916100582', 'M. Shamarukh Alam Chowdhury', '1', '1', '2400', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(751, '753', 7, NULL, 15, 'NULL', '1', '2', '1711965134', 'Md. Atiar Rahman', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(752, '754', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Badrul', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(753, '755', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Khaled Iftekhar Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(754, '756', 7, NULL, 15, 'NULL', '1', '2', '1914820814', 'Md. Monirul Islam', '1', '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(755, '757', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Md. Mizanur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(756, '758', 7, NULL, 15, 'NULL', '2', '1', '1199330715', 'Abu Zafor Noor Ahmed', NULL, '1', '2400', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(757, '759', 7, NULL, 15, 'NULL', '1', '1', '1711138682', 'Md. Sarwar Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(758, '760', 7, NULL, 15, 'NULL', '2', '1', NULL, 'Mashnur Sakib', '1', '1', '2400', 'NULL', 'NULL', 'home_delivery', NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(759, '761', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Humayun', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(760, '762', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Md. Sams Ehsanul Haque Sojib', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(761, '763', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Hossain Sarif Rabbi', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(762, '764', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Arif Md, Anwar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(763, '765', 7, NULL, 15, 'NULL', '1', '1', NULL, 'A.K.M. Shariful Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(764, '766', 7, NULL, 15, 'NULL', '1', '1', '1815005427', 'Md. Nazmul Hasan Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(765, '767', 7, NULL, 15, 'NULL', '1', '1', NULL, 'A.S.M. Arif Anwar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(766, '768', 7, NULL, 15, 'NULL', '1', '2', '1711617438', 'Md. Jahidur Rashid', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(767, '769', 7, NULL, 15, 'NULL', '1', '1', '1714085843', 'Sheikh Md. Mahmudul Hassan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(768, '770', 7, NULL, 15, 'NULL', '1', '1', '1712063705', 'A.H.M. Khairul Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(769, '771', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Md. Mostafizur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(770, '772', 7, NULL, 15, 'NULL', '1', '1', '1711369462, 1716124539', 'Tanvir Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(771, '773', 7, NULL, 15, 'NULL', '1', '1', NULL, 'tamal', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(772, '774', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Nayeemur Rahman Khaled', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(773, '775', 7, NULL, 15, 'NULL', '1', '1', '1719362304', 'Md. Mokhlesur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(774, '776', 7, NULL, 15, 'NULL', '1', '1', NULL, 'Dewan Muhammad Al Amin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(775, '777', 7, NULL, 15, 'NULL', '1', '2', NULL, 'sabbir Ahmed', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(776, '778', 7, NULL, 16, 'NULL', '1', '1', NULL, 'Md. Abu Zafor ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(777, '779', 7, NULL, 16, 'NULL', '1', '1', NULL, 'Ad. Akhter Faruq', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(778, '780', 7, NULL, 16, 'NULL', '1', '1', NULL, 'Md. Sajjad Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(779, '781', 7, NULL, 16, 'NULL', '1', '1', '1678702656', 'Md. Humyun Kabir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(780, '782', 7, NULL, 16, 'NULL', '1', '1', NULL, 'Md. Al Amin Hasan Khan ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(781, '783', 7, NULL, 16, 'NULL', '2', '1', '1199535826', 'Md. Zamal Uddin ', NULL, '1', '2400', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(782, '784', 7, NULL, 16, 'NULL', '1', '1', '1714333484', 'ATM Shibly Noman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(783, '785', 7, NULL, 16, 'NULL', '1', '2', '1713456712', 'ABM Fattah', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(784, '786', 7, NULL, 16, 'NULL', '1', '2', NULL, 'ABM Mahmudul Haque', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(785, '787', 7, NULL, 16, 'NULL', '1', '1', NULL, 'Md. Mamun Muntasir Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(786, '788', 7, NULL, 16, 'NULL', '1', '1', NULL, 'Md. Tareq Jubayer', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(787, '789', 7, NULL, 16, 'NULL', '1', '1', '1916635178', 'Md. Abdul Aleem', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(788, '790', 7, NULL, 16, 'NULL', '1', '1', '1817181046', 'AKM Sajedur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(789, '791', 7, NULL, 16, 'NULL', '1', '1', '1553141049', 'Md. Soaliman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(790, '792', 7, NULL, 16, 'NULL', '1', '1', NULL, 'Zafor Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(791, '793', 7, NULL, 16, 'NULL', '1', '1', NULL, 'Abu Khaled Mohmmad Saifullah', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(792, '794', 7, NULL, 16, 'NULL', '1', '1', NULL, 'Md. Mahbubul Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(793, '795', 7, NULL, 16, 'NULL', '1', '1', NULL, 'Md. Zafor Iqbal', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(794, '796', 7, NULL, 16, 'NULL', '1', '1', NULL, 'Md. Ashraful Alam Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(795, '797', 7, NULL, 16, 'NULL', '1', '1', '1711387377', 'AHM Ahaduzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(796, '798', 7, NULL, 16, 'NULL', '1', '1', NULL, 'Md. Towhiduzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(797, '799', 7, NULL, 16, 'NULL', '1', '1', '1711345120', 'SM Shahriar Akhlaque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(798, '800', 7, NULL, 16, 'NULL', '1', '2', NULL, 'Md. Rasel Anawer', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(799, '801', 7, NULL, 16, 'NULL', '1', '1', NULL, 'Abdullah Mahfuj', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(800, '802', 7, NULL, 16, 'NULL', '1', '1', '1199335649', 'Md. Abdullah Al Amin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(801, '803', 7, NULL, 16, 'NULL', '1', '1', NULL, 'Late Md. Majedul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(802, '804', 7, NULL, 16, 'NULL', '1', '1', '1716593222', 'SM Masum', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(803, '805', 7, NULL, 16, 'NULL', '1', '1', NULL, 'Md. Shameem', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(804, '806', 7, NULL, 16, 'NULL', '1', '1', '1913583928', 'Imran Majid', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(805, '807', 7, NULL, 16, 'NULL', '1', '1', '1712013990', 'Md. Mehedi Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(806, '808', 7, NULL, 16, 'NULL', '1', '1', NULL, 'Late Ashraf Ibn Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(807, '809', 7, NULL, 16, 'NULL', '1', '2', '1712132517', 'Ahamed Hossain Sohel', '1', '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(808, '810', 7, NULL, 16, 'NULL', '1', '1', NULL, 'Adnan Wahid', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(809, '811', 7, NULL, 16, 'NULL', '1', '1', NULL, 'Md. AbDullah Mamun', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(810, '812', 7, NULL, 16, 'NULL', '1', '2', '1711507005', 'Md. Ramjan Hossain Khan', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(811, '813', 7, NULL, 16, 'NULL', '1', '1', NULL, 'A K Rezaul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(812, '814', 7, NULL, 16, 'NULL', '1', '1', NULL, 'KH Rifat Shahriar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(813, '815', 7, NULL, 16, 'NULL', '1', '1', '1716413883', 'Rajib Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(814, '816', 7, NULL, 16, 'NULL', '1', '1', NULL, 'Sayeed Shahin Ahammed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(815, '817', 7, NULL, 16, 'NULL', '1', '1', '1811413343', 'Rashedul Islam Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(816, '818', 7, NULL, 16, 'NULL', '1', '1', NULL, 'Mizanur Rahman Liton', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(817, '819', 7, NULL, 16, 'NULL', '1', '1', '1712696226', 'Md. Faisal Kobir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(818, '820', 7, NULL, 16, 'NULL', '1', '1', NULL, 'ATM Zakaria', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(819, '821', 7, NULL, 16, 'NULL', '1', '1', '1816634417', 'Md. Shahidul Isalm', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(820, '822', 7, NULL, 16, 'NULL', '1', '1', '1711504557', 'Gulam Sarwar', NULL, NULL, NULL, 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(821, '823', 7, NULL, 16, 'NULL', '1', '1', NULL, 'Khondokar Rashedul Arefin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(822, '824', 7, NULL, 16, 'NULL', '1', '1', NULL, 'Md. Ashikur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(823, '825', 7, NULL, 16, 'NULL', '1', '1', NULL, 'A Z M Sharif Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(824, '826', 7, NULL, 16, 'NULL', '1', '1', '1712207219', 'Md. Selim Reza', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(825, '827', 7, NULL, 16, 'NULL', '1', '2', '1714033140', 'Zabir Muhammad Taqui', '1', '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(826, '828', 7, NULL, 16, 'NULL', '1', '1', NULL, 'Iqbal Ahmed Naser', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(827, '829', 7, NULL, 16, 'NULL', '1', '1', '1552406652', 'Jewel Md. Tawhiduzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(828, '830', 7, NULL, 16, 'NULL', '1', '1', '1816468960', 'Md. Wahiduzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(829, '831', 7, NULL, 16, 'NULL', '1', '1', '1711705866', 'AM Mahbubul Alam Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(830, '832', 7, NULL, 16, 'NULL', '1', '1', '1716848070', 'Mohammad Foisol', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(831, '833', 7, NULL, 16, 'NULL', '1', '1', '1715301631', 'Abdullah Al Mamun', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(832, '834', 7, NULL, 17, 'NULL', '1', '1', '1712238753', 'Z.M. Raihan Kabir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(833, '835', 7, NULL, 17, 'NULL', '1', '2', '1713036722', 'Md. Maruf Hossain Khan', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(834, '836', 7, NULL, 17, 'NULL', '1', '2', '1711223112', 'Sheikh Nur-A-mobashshir Hasan', NULL, NULL, NULL, '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(835, '837', 7, NULL, 17, 'NULL', '1', '1', '1717432592', 'Md. Atiqul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(836, '838', 7, NULL, 17, 'NULL', '1', '1', '1730334789', 'Md. Mostafizur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(837, '839', 7, NULL, 17, 'NULL', '1', '1', NULL, 'Abu Sayed Mohammad Sayem', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(838, '840', 7, NULL, 17, 'NULL', '1', '1', '1714413062', 'Md. Saifur Rahman Paris', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(839, '841', 7, NULL, 17, 'NULL', '1', '1', '1716167294', 'Abu Hena Mahmud Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(840, '842', 7, NULL, 17, 'NULL', '1', '1', '1673639552', 'Asif Moinur Chowdhury', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(841, '843', 7, NULL, 17, 'NULL', '1', '1', NULL, 'Md. Khurshid Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(842, '844', 7, NULL, 17, 'NULL', '1', '1', NULL, 'Arif Bin Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(843, '845', 7, NULL, 17, 'NULL', '1', '1', NULL, 'Abdullah Al Mamun', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(844, '846', 7, NULL, 17, 'NULL', '1', '2', '1711081792', 'Md. Irfanul Hoque', NULL, '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(845, '847', 7, NULL, 17, 'NULL', '1', '1', '1755449900', 'Ashraful Kabir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(846, '848', 7, NULL, 17, 'NULL', '1', '1', '1750999259', 'Md Shafiul Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(847, '849', 7, NULL, 17, 'NULL', '1', '2', '1730311479', 'Sajedul Islam', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(848, '850', 7, NULL, 17, 'NULL', '1', '1', '1199013044', 'Md. Rabiul Awal Monir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(849, '851', 7, NULL, 17, 'NULL', '1', '1', '1678128576', 'Md. Ehsan-Ul-Haque Fahim', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(850, '852', 7, NULL, 17, 'NULL', '1', '1', '1817533953', 'A. B. M. Alim Al Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(851, '853', 7, NULL, 17, 'NULL', '1', '1', '1711507628', 'Monirul Islam Rana', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(852, '854', 7, NULL, 17, 'NULL', '1', '1', '1678017536', 'Md Hamidur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(853, '855', 7, NULL, 17, 'NULL', '1', '1', '1715107247', 'Md. Nurul Islam Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(854, '856', 7, NULL, 17, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(855, '857', 7, NULL, 17, 'NULL', '1', '1', '1769332118', 'Mohammad Tanvir Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(856, '858', 7, NULL, 17, 'NULL', '1', '1', NULL, 'Ariful Azad', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(857, '859', 7, NULL, 17, 'NULL', '1', '1', NULL, 'Muhammad Mahmudur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(858, '860', 7, NULL, 17, 'NULL', '1', '1', '1986450993', 'Aminur Rhaman Amin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(859, '861', 7, NULL, 17, 'NULL', '1', '1', NULL, 'Md. Moniruzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(860, '862', 7, NULL, 17, 'NULL', '1', '1', NULL, 'Md. Nurul Amin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(861, '863', 7, NULL, 17, 'NULL', '1', '1', '1730443655', 'Ezaz Imran', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(862, '864', 7, NULL, 17, 'NULL', '1', '1', '1917271413', 'Sheikh Md. Abdullah Bin Kalam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(863, '865', 7, NULL, 17, 'NULL', '1', '1', '1716158123', 'Abdullah All Mamun', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(864, '866', 7, NULL, 17, 'NULL', '1', '2', '1713095014', 'Md Razibul Haque', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(865, '867', 7, NULL, 17, 'NULL', '1', '2', '1713095867', 'Md Masum Akhter Zaman', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(866, '868', 7, NULL, 17, 'NULL', '1', '1', '1726884422', 'Shah Md. Masum Billah', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(867, '869', 7, NULL, 17, 'NULL', '1', '1', '1730164448', 'Md. Selim Reza', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(868, '870', 7, NULL, 17, 'NULL', '1', '1', '1833178162', 'Rakesh Saha', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(869, '871', 7, NULL, 17, 'NULL', '1', '2', '1730338571', 'Mirza Nure Alam', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(870, '872', 7, NULL, 17, 'NULL', '1', '1', '1819854789', 'Shah Abul Hasnat Md. Ferdous Wahid', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(871, '873', 7, NULL, 17, 'NULL', '1', '1', '1911314104', 'Hasan Jaman Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(872, '874', 7, NULL, 17, 'NULL', '1', '1', NULL, 'Kazi Khadem-UL-Islam Shahidullah', NULL, NULL, NULL, NULL, 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(873, '875', 7, NULL, 17, 'NULL', '1', '1', '1678052153', 'Md. Golam Morshed Rasel', NULL, NULL, 'NULL', NULL, 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(874, '876', 7, NULL, 17, 'NULL', '1', '1', '1912616099', 'Md Anwar Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(875, '877', 7, NULL, 17, 'NULL', '1', '1', '1711234280', 'Muhammod Abdul Awal Bin Sayeed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(876, '878', 7, NULL, 17, 'NULL', '1', '1', NULL, 'Shawkat Hasan shafin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(877, '879', 7, NULL, 17, 'NULL', '1', '2', '1747946383', 'Mohammad Salah Uddin', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(878, '880', 7, NULL, 17, 'NULL', '1', '1', '1714097842', 'Md Mahmudul Haque Sumon', NULL, NULL, NULL, 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(879, '881', 7, NULL, 17, 'NULL', '1', '1', '1711179860', 'Syed Azizul Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(880, '882', 7, NULL, 17, 'NULL', '1', '2', '1781141600', 'Md. Nazmul Islam Masud', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(881, '883', 7, NULL, 17, 'NULL', '1', '1', '1717179484', 'Mamun Mahbub Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(882, '884', 7, NULL, 17, 'NULL', '1', '1', '1733513785', 'Abu Bakar Siddique Saimum', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(883, '885', 7, NULL, 18, 'NULL', '1', '1', '1712532122', 'Faisal Bin Selim Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(884, '886', 7, NULL, 18, 'NULL', '2', '1', '1711858548', 'Mohammad Mahbubur Rahman', NULL, '1', '2400', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(885, '887', 7, NULL, 18, 'NULL', '1', '2', NULL, 'Mohammad Golam Mustafa Bhuyan', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(886, '888', 7, NULL, 18, 'NULL', '1', '1', '1711302904', 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(887, '889', 7, NULL, 18, 'NULL', '2', '1', '1712538602', 'Md. Abdul Matin', NULL, '1', '2400', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(888, '890', 7, NULL, 18, 'NULL', '1', '1', '1913949779', 'MD. FERDOUS ALOM', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(889, '891', 7, NULL, 18, 'NULL', '1', '1', '1711301456', 'Md. Monzur Hossain ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(890, '892', 7, NULL, 18, 'NULL', '1', '2', NULL, 'Reza', NULL, NULL, 'NULL', '20000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(891, '893', 7, NULL, 18, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(892, '894', 7, NULL, 18, 'NULL', '1', '1', '1753453173', 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(893, '895', 7, NULL, 18, 'NULL', '1', '2', '1712185895', 'Md. Abu Musa', NULL, '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(894, '896', 7, NULL, 18, 'NULL', '1', '1', NULL, 'Abdullah-Al-Imran', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(895, '897', 7, NULL, 18, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(896, '898', 7, NULL, 18, 'NULL', '1', '1', '1817518053', 'Md. Raquibul Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(897, '899', 7, NULL, 18, 'NULL', '1', '1', '1610007705', 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(898, '900', 7, NULL, 18, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(899, '901', 7, NULL, 18, 'NULL', '1', '1', '1673950496', 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(900, '902', 7, NULL, 18, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(901, '903', 7, NULL, 18, 'NULL', '1', '1', '1923846535', 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(902, '904', 7, NULL, 18, 'NULL', '1', '1', NULL, 'Saifullah Siddiqui', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(903, '905', 7, NULL, 18, 'NULL', '1', '1', '1911145380', 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(904, '906', 7, NULL, 18, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(905, '907', 7, NULL, 18, 'NULL', '1', '1', '1713114854', 'A.K.M. Apel Mahmud', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(906, '908', 7, NULL, 18, 'NULL', '1', '1', '1736091902', 'MD.KHALED NOMAN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(907, '909', 7, NULL, 18, 'NULL', '1', '1', NULL, 'Rokib Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(908, '910', 7, NULL, 18, 'NULL', '1', '1', '1710140132', 'HASAN MAHMUD', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(909, '911', 7, NULL, 18, 'NULL', '1', '1', NULL, 'MD AFZAL HOSSAIN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(910, '912', 7, NULL, 18, 'NULL', '1', '1', NULL, 'Golam Sarwar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(911, '913', 7, NULL, 18, 'NULL', '1', '1', '1713367743', 'Md. Mhabub Murshed Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(912, '914', 7, NULL, 18, 'NULL', '1', '1', NULL, 'Md. Mubashshirul Ibad Mishu', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(913, '915', 7, NULL, 18, 'NULL', '1', '1', '1678102015', 'Md. Rafiul Haque (Shahin)', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(914, '916', 7, NULL, 18, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(915, '918', 7, NULL, 18, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(916, '919', 7, NULL, 18, 'NULL', '1', '2', '1199482668', 'NULL', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(917, '920', 7, NULL, 18, 'NULL', '1', '1', NULL, 'Md. Abu Hena Mostofa Kamal', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(918, '921', 7, NULL, 18, 'NULL', '1', '1', NULL, 'Noman Ferdous', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(919, '922', 7, NULL, 18, 'NULL', '2', '1', NULL, 'T M Mojahidul Islam Richard', NULL, NULL, '2400', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(920, '923', 7, NULL, 18, 'NULL', '2', '1', NULL, 'Asif Iqbal', NULL, '1', '2400', 'NULL', 'NULL', 'online_payment', NULL, NULL, NULL, NULL, NULL, NULL, 'paid'),
(921, '924', 7, NULL, 18, 'NULL', '1', '1', '1716799652', 'Golam Mahfuz Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(922, '925', 7, NULL, 18, 'NULL', '1', '1', '1713479683', 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(923, '926', 7, NULL, 18, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(924, '927', 7, NULL, 18, 'NULL', '1', '2', '1912311613', 'Md. Iftekharul Islam', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(925, '928', 7, NULL, 18, 'NULL', '1', '1', '1819815245', 'Mir Abdul Baki Prince', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(926, '929', 7, NULL, 18, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(927, '930', 7, NULL, 18, 'NULL', '1', '1', '1718689788', 'Md. Armanul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(928, '931', 7, NULL, 18, 'NULL', '1', '1', '1715233947', 'TANVIR RASHID KHAN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(929, '932', 7, NULL, 18, 'NULL', '1', '1', '1717718950', 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(930, '933', 7, NULL, 18, 'NULL', '1', '1', '1617808034', 'Md.Zahiduzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(931, '934', 7, NULL, 18, 'NULL', '1', '2', NULL, 'Md. Moshiul Alam', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(932, '935', 7, NULL, 18, 'NULL', '2', '1', '1713084737', 'Didarul Islam', NULL, '1', '2400', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(933, '936', 7, NULL, 18, 'NULL', '1', '1', NULL, 'Md. Iktiaj Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(934, '937', 7, NULL, 18, 'NULL', '1', '1', NULL, 'Md. Masudur Rahman Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(935, '938', 7, NULL, 18, 'NULL', '1', '1', '1711080775', 'Shafiqul Islam Mallik', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(937, '940', 7, NULL, 18, 'NULL', '1', '1', NULL, 'S. M. Arifuzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(938, '941', 7, NULL, 19, 'NULL', '1', '1', '1911516917', 'Mohammad Asad Noor', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(939, '942', 7, NULL, 19, 'NULL', '1', '2', '1711502684', 'Md. Mohsin Ali Khan', NULL, NULL, 'NULL', '5000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(940, '943', 7, NULL, 19, 'NULL', '2', '1', '1710458352', 'Md. Mahbubur Rahman', '1', '1', '2400', NULL, 'NULL', 'home_delivery', NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(941, '944', 7, NULL, 19, 'NULL', '1', '1', NULL, 'Md. Saddam Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(942, '945', 7, NULL, 19, 'NULL', '1', '1', '1717890431', 'Md. Asif Bulbul', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(943, '946', 7, NULL, 19, 'NULL', '1', '1', NULL, 'Kabir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `college_validation` (`college_validation_id`, `student_number`, `college_id`, `year_id`, `batch_id`, `booking_status`, `is_anual_subcribe`, `is_life_time_subscribe`, `contact_number`, `student_name`, `is_spouse`, `is_self`, `anual_subsciption_cost`, `life_time_subscription_cost`, `token_no`, `subcription_delivery_method`, `customer_address`, `city`, `discrict_id`, `zip_code`, `val_id`, `is_online`, `payment_status`) VALUES
(944, '947', 7, NULL, 19, 'NULL', '1', '1', '1717507220', 'Md.Atiqur Rahman ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(945, '948', 7, NULL, 19, 'NULL', '1', '1', NULL, 'Omar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(946, '949', 7, NULL, 19, 'NULL', '1', '1', NULL, 'MD ARIFUR RAHMAN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(947, '950', 7, NULL, 19, 'NULL', '1', '1', '1617445127', 'MD.SHAHOL AHMED NOBEL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(948, '951', 7, NULL, 19, 'NULL', '1', '1', '1818473866', 'Md.Noora Mortuja', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(949, '952', 7, NULL, 19, 'NULL', '1', '1', '1678008104', 'JUBAIR SHAFIQUE', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(950, '953', 7, NULL, 19, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(951, '954', 7, NULL, 19, 'NULL', '1', '1', NULL, 'Asif Kamal Uddin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(952, '955', 7, NULL, 19, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(953, '956', 7, NULL, 19, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(954, '957', 7, NULL, 19, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(955, '958', 7, NULL, 19, 'NULL', '1', '1', NULL, 'Asif Alam Sazzad', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(956, '959', 7, NULL, 19, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(957, '960', 7, NULL, 19, 'NULL', '1', '1', '1713097794', 'AGM RASHEDUZZAMAN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(958, '961', 7, NULL, 19, 'NULL', '1', '1', NULL, 'Md Tameem Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(959, '962', 7, NULL, 19, 'NULL', '2', '1', NULL, 'Mahamudzzaman', NULL, '1', '2400', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(960, '963', 7, NULL, 19, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(961, '964', 7, NULL, 19, 'NULL', '1', '2', '1711170513', 'K. M. Tariqul Islam', NULL, '1', 'NULL', '10000', 'NULL', 'online_payment', NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(962, '965', 7, NULL, 19, 'NULL', '1', '1', NULL, 'Md. Hosne Jamil Salman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(963, '966', 7, NULL, 19, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(964, '967', 7, NULL, 19, 'NULL', '1', '1', '1678071495', 'MD. MUSTASINUL ISLAM ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(965, '968', 7, NULL, 19, 'NULL', '1', '1', '1716456515', 'Md. Kamal Hossain Ripon', NULL, NULL, NULL, 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(966, '969', 7, NULL, 19, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(967, '970', 7, NULL, 19, 'NULL', '1', '2', '1711081546', 'Md. Rashidul Hasan', NULL, '1', 'NULL', '10000', 'NULL', 'home_delivery', 'Uday Tower(14th Floor),Gulshan-1', 'Dhaka', '14', '1212', NULL, NULL, 'unpaid'),
(968, '971', 7, NULL, 19, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(969, '972', 7, NULL, 19, 'NULL', '2', '1', '1717432613', 'A.K.M. RAZIBUR RAHMAN', '1', '1', '2400', 'NULL', 'NULL', 'home_delivery', NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(970, '973', 7, NULL, 19, 'NULL', '1', '2', '1732944952', 'Jahangir Alam Khan', NULL, NULL, 'NULL', '5000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(971, '974', 7, NULL, 19, 'NULL', '1', '1', '1711822791', 'Raju Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(972, '975', 7, NULL, 19, 'NULL', '1', '1', '1716481276', 'Md. Rasel Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(973, '976', 7, NULL, 19, 'NULL', '1', '1', NULL, 'A.B.M. Musa', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(974, '977', 7, NULL, 19, 'NULL', '1', '2', '1712186722', 'MD. ASIKUZZAMAN', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(975, '978', 7, NULL, 19, 'NULL', '1', '1', '1711006239', 'Md Shafayet Jamil', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(976, '979', 7, NULL, 19, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(977, '980', 7, NULL, 19, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(978, '981', 7, NULL, 19, 'NULL', '2', '1', '1717665106', 'Khandaker Rashed Mahtab', NULL, '1', '2400', 'NULL', 'NULL', 'online_payment', NULL, NULL, NULL, NULL, NULL, NULL, 'paid'),
(979, '982', 7, NULL, 20, 'NULL', '1', '1', '1717934542', 'S.M.Shahinur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(980, '983', 7, NULL, 20, 'NULL', '1', '1', '1190769996', 'Md. Towhidur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(981, '984', 7, NULL, 20, 'NULL', '1', '2', '1717499632', 'Md. Fakhrul Hassan', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(982, '985', 7, NULL, 20, 'NULL', '2', '1', '1717548799', 'Md. Mushfiq Rahman', NULL, '1', '2400', 'NULL', 'NULL', 'home_delivery', NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(983, '986', 7, NULL, 20, 'NULL', '1', '1', '1611313152', 'A. S. M. FARHAD', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(984, '987', 7, NULL, 20, 'NULL', '1', '1', '1673939866', 'Salem Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(985, '988', 7, NULL, 20, 'NULL', '1', '1', '1915556029', 'Mia Mohammad Ashish Bin Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(986, '989', 7, NULL, 20, 'NULL', '1', '1', '1675671672', 'Md. Sujauddowla', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(987, '990', 7, NULL, 20, 'NULL', '1', '1', '1719069473', 'Sk. Rejanur Islam ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(988, '991', 7, NULL, 20, 'NULL', '2', '1', '1717719731', 'S. M. Nayeemul Bari', NULL, '1', '2400', 'NULL', 'NULL', 'online_payment', NULL, NULL, NULL, NULL, NULL, NULL, 'paid'),
(989, '992', 7, NULL, 20, 'NULL', '1', '1', '1553443929', 'Md. Mahbub Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(990, '993', 7, NULL, 20, 'NULL', '1', '1', '1718784269', 'A.S.M Al Imran', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(991, '994', 7, NULL, 20, 'NULL', '1', '1', NULL, 'Md. Mizanur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(992, '995', 7, NULL, 20, 'NULL', '1', '1', '1190156484', 'Md. Nabinauaze Sheikh', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(993, '996', 7, NULL, 20, 'NULL', '1', '1', NULL, 'Sanowar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(994, '997', 7, NULL, 20, 'NULL', '1', '1', NULL, 'A K M Mostafizur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(995, '998', 7, NULL, 20, 'NULL', '1', '1', '1678071316', 'A.M. Zahid Parvez', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(996, '999', 7, NULL, 20, 'NULL', '1', '1', NULL, 'Monir  ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(997, '1000', 7, NULL, 20, 'NULL', '1', '2', '1913782303', 'KHANDOKER TARIQUL ISLAM', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(998, '1001', 7, NULL, 20, 'NULL', '1', '1', '1712600871', 'Tazul Islam Sanchoy', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(999, '1002', 7, NULL, 20, 'NULL', '1', '1', '1919421726', 'Md. Ahad-uz-Zaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1000, '1003', 7, NULL, 20, 'NULL', '1', '1', NULL, 'Md. Delwar Rahman Raju', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1001, '1004', 7, NULL, 20, 'NULL', '1', '2', NULL, 'A G M Islam', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1002, '1005', 7, NULL, 20, 'NULL', '1', '1', '1711230005', 'MD RUHUL AMIN SAJIB', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1003, '1006', 7, NULL, 20, 'NULL', '1', '1', '1732114007', 'Md. Mahbubul Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1004, '1007', 7, NULL, 20, 'NULL', '1', '1', '1713865810', 'Md. Abdul Mazed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1005, '1008', 7, NULL, 20, 'NULL', '1', '1', '1912560394', 'Md Tawfiq Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1006, '1009', 7, NULL, 20, 'NULL', '1', '1', '1715028623', 'Shawkat  ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1007, '1010', 7, NULL, 20, 'NULL', '1', '1', '1713367719', 'SHAHRIAR MOHAMMAD ASIF ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1008, '1011', 7, NULL, 20, 'NULL', '1', '1', '1689354025', 'Redwanur Rahman Chowdhury', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1009, '1012', 7, NULL, 20, 'NULL', '1', '1', '1713332036', 'Md. Towfiqul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1010, '1013', 7, NULL, 20, 'NULL', '1', '1', '1712886876', 'Md. Sirajul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1011, '1014', 7, NULL, 20, 'NULL', '1', '1', '1675247427', 'Sk. Maruful Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1012, '1015', 7, NULL, 20, 'NULL', '1', '1', '1715609710', 'B.M.SALIM REJA SHOFIQ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1013, '1016', 7, NULL, 20, 'NULL', '1', '1', NULL, 'Golam Kibria', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1014, '1017', 7, NULL, 20, 'NULL', '1', '1', '1712507280', 'Md. Mohaiminur Rahman Shaheen', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1015, '1018', 7, NULL, 20, 'NULL', '1', '1', '1716457298', 'Md. Yeakub Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1016, '1019', 7, NULL, 20, 'NULL', '1', '2', '1730318018', 'Mohammad Shoyebur Rahman', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1017, '1020', 7, NULL, 20, 'NULL', '1', '1', '1717205365', 'M.Shahadat Hossan Chowdhury', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1018, '1021', 7, NULL, 20, 'NULL', '1', '1', '1558406009', 'MD FARUQ HOSSAIN KHAN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1019, '1022', 7, NULL, 20, 'NULL', '1', '2', '1717449219', 'MD. MOFIJUL ISLAM BULBUL', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1020, '1023', 7, NULL, 20, 'NULL', '1', '1', '1839569319', 'Ahsan Habib', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1021, '1024', 7, NULL, 20, 'NULL', '1', '1', '1816469164', 'Moinul Islam ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1022, '1025', 7, NULL, 20, 'NULL', '1', '1', '1723032360', 'Md. Mostafizur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1023, '1026', 7, NULL, 20, 'NULL', '1', '1', '1674079327', 'Md. Riaz Pervez', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1024, '1027', 7, NULL, 20, 'NULL', '1', '1', '1733007506', 'Mir Masud Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1025, '1028', 7, NULL, 20, 'NULL', '1', '1', '1720021612', 'Md. Mostaq Ahmed', NULL, NULL, NULL, 'NULL', 'NULL', 'online_payment', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1026, '1029', 7, NULL, 20, 'NULL', '1', '1', '1723467399', 'Md Abdur Rahman Tawfiq', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1027, '1030', 7, NULL, 20, 'NULL', '1', '1', NULL, 'Ahmed Shams Ibne Shaukat', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1028, '1031', 7, NULL, 20, 'NULL', '1', '1', NULL, 'A S M Shamsul Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1029, '1032', 7, NULL, 20, 'NULL', '1', '1', '1717400721', 'Ehsanul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1030, '1033', 7, NULL, 21, 'NULL', '2', '1', '1716532195', 'Shah Saifur Rahman Sadi ', NULL, NULL, '2400', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1031, '1034', 7, NULL, 21, 'NULL', '1', '1', '1670732083', 'Md. Nazmul Hossain ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1032, '1035', 7, NULL, 21, 'NULL', '1', '1', '1716215757', 'Ali Haider Uttosh', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1033, '1036', 7, NULL, 21, 'NULL', '1', '1', '1717553562', 'Samsuzzaman Konok', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1034, '1037', 7, NULL, 21, 'NULL', '2', '1', '1717803103', 'Nuhun Naziullah', NULL, '1', '2400', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1035, '1038', 7, NULL, 21, 'NULL', '1', '1', '1676159474', 'Md. Mashiour Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1036, '1039', 7, NULL, 21, 'NULL', '1', '1', '1617372700', 'Md. Rashedul Islam ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1037, '1040', 7, NULL, 21, 'NULL', '1', '1', '1819961765', 'Md. Arafat Islam Shomik', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1038, '1041', 7, NULL, 21, 'NULL', '2', '1', '1731044550', 'Mukaddim Sarwar', NULL, '1', '2400', 'NULL', 'NULL', 'online_payment', NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(1039, '1042', 7, NULL, 21, 'NULL', '1', '1', '1713097512', 'Abdul Al Mueed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1040, '1043', 7, NULL, 21, 'NULL', '2', '1', '1711081930', 'Safiul Islam ', NULL, '1', '2400', 'NULL', 'NULL', 'home_delivery', NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(1041, '1044', 7, NULL, 21, 'NULL', '1', '1', NULL, 'Fazle Rabby', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1042, '1045', 7, NULL, 21, 'NULL', '1', '1', '1722100589', 'Md. Zakir Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1043, '1046', 7, NULL, 21, 'NULL', '1', '2', '1712699306', 'Habib Mohammqad Sajjad', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1044, '1047', 7, NULL, 21, 'NULL', '1', '1', '1723818552', 'Ahmad farukh', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1045, '1048', 7, NULL, 21, 'NULL', '1', '1', '1713396818', 'Sadman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1046, '1049', 7, NULL, 21, 'NULL', '1', '1', NULL, 'sabbir Ahmed', NULL, NULL, NULL, NULL, 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1047, '1050', 7, NULL, 21, 'NULL', '1', '1', '1913972696', 'Rayhan ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1048, '1051', 7, NULL, 21, 'NULL', '1', '1', NULL, 'FM Rahmat Ullah', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1049, '1052', 7, NULL, 21, 'NULL', '1', '1', '1675213821', 'Mirza Adwit Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1050, '1053', 7, NULL, 21, 'NULL', '1', '1', '1611614369', 'Kamrul Kibria Ayon ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1051, '1054', 7, NULL, 21, 'NULL', '1', '1', '1710274271', 'Mehedi Hasan Siqque mithu ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1052, '1055', 7, NULL, 21, 'NULL', '1', '1', '1715393484', 'Md Shahriar Mahmud ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1053, '1056', 7, NULL, 21, 'NULL', '1', '1', '1913872932', 'Md. Tanvir Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1054, '1057', 7, NULL, 21, 'NULL', '1', '1', '1678710937', 'kawser Bari', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1055, '1058', 7, NULL, 21, 'NULL', '1', '1', '1612563049', 'Joshidul ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1056, '1059', 7, NULL, 21, 'NULL', '1', '1', '1734560597', 'Enayet ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1057, '1060', 7, NULL, 21, 'NULL', '1', '1', '1716419467', 'Farhad hasan Mamnun', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1058, '1061', 7, NULL, 21, 'NULL', '1', '1', '1710472377', 'Md. Sirajul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1059, '1062', 7, NULL, 21, 'NULL', '1', '1', '1710474350', 'Jewel Parvez', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1060, '1063', 7, NULL, 21, 'NULL', '1', '1', '1671072263', 'Hasan ', NULL, NULL, 'NULL', NULL, 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1061, '1064', 7, NULL, 21, 'NULL', '1', '2', '1678367070', 'Mofizur Rahman', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1062, '1065', 7, NULL, 21, 'NULL', '1', '1', '1719459039', 'Salahuddin ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1063, '1066', 7, NULL, 21, 'NULL', '1', '2', '1911046898', 'Sarfaraz', NULL, '1', 'NULL', '20000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(1064, '1067', 7, NULL, 21, 'NULL', '1', '1', NULL, 'Monjur', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1065, '1068', 7, NULL, 21, 'NULL', '1', '1', '1716070330', 'Raju', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1066, '1069', 7, NULL, 21, 'NULL', '1', '1', '1611332581', 'Mushfique', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1067, '1070', 7, NULL, 21, 'NULL', '1', '1', NULL, 'Himel', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1068, '1071', 7, NULL, 21, 'NULL', '2', '1', '1711139273', 'Rajib', NULL, '1', '2400', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1069, '1072', 7, NULL, 21, 'NULL', '1', '1', '1716851730', 'Morshed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1070, '1073', 7, NULL, 21, 'NULL', '1', '1', '1911202729', 'Tanzil', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1071, '1074', 7, NULL, 21, 'NULL', '1', '2', '1717697171', 'Ali reza', '1', '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1072, '1075', 7, NULL, 21, 'NULL', '1', '1', '1712256641', 'Sultan Mahmud Chomon', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1073, '1076', 7, NULL, 21, 'NULL', '1', '2', '1678028641', 'Lutor Rahman', '1', '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1074, '1077', 7, NULL, 21, 'NULL', '1', '1', '1673032402', 'Shihab', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1075, '1078', 7, NULL, 21, 'NULL', '1', '1', '1713396837', 'Avishekh', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1076, '1079', 7, NULL, 21, 'NULL', '1', '1', '1722085568', 'Tanvir hossain Rabby', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1077, '1080', 7, NULL, 21, 'NULL', '1', '1', '1766662998', 'Rashedul islam Rony', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1078, '1081', 7, NULL, 21, 'NULL', '2', '1', '1717381406', 'Shakil Ahmed Immtiaz', NULL, NULL, '2400', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1079, '1082', 7, NULL, 21, 'NULL', '1', '1', NULL, 'Rezwanur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1080, '1083', 7, NULL, 21, 'NULL', '1', '1', NULL, 'Nayeem', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1081, '1084', 7, NULL, 22, 'NULL', '1', '1', '1916123454', 'Abdullah-al- Mamun', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1082, '1085', 7, NULL, 22, 'NULL', '1', '1', '1714453425', 'Md Ishad Sadeque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1083, '1086', 7, NULL, 22, 'NULL', '1', '1', '1619991086', 'Abdul Awal MD Saiful Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1084, '1087', 7, NULL, 22, 'NULL', '1', '2', '1675698800', 'Md.Akhteruzaaman', NULL, '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1085, '1088', 7, NULL, 22, 'NULL', '1', '1', '1716625017', 'Md. Rafiur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1086, '1089', 7, NULL, 22, 'NULL', '1', '1', '1556304210', 'AL Montasir Billah', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1087, '1090', 7, NULL, 22, 'NULL', '1', '1', '1672568408', 'Saba Al Salem', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1088, '1091', 7, NULL, 22, 'NULL', '1', '1', '1911749724', 'Md. Arafat Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1089, '1092', 7, NULL, 22, 'NULL', '1', '1', '1811414208', 'Ismail Shams Azizi', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1090, '1093', 7, NULL, 22, 'NULL', '1', '1', '1558331037', 'Muhammad Abdullahil mamun', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1091, '1094', 7, NULL, 22, 'NULL', '1', '1', '1914481612', 'Md. Mahmudul Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1092, '1095', 7, NULL, 22, 'NULL', '1', '1', NULL, 'Late Golam Fauzul Kabir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1093, '1096', 7, NULL, 22, 'NULL', '1', '1', '1717135442', 'Al Imran Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1094, '1097', 7, NULL, 22, 'NULL', '2', '1', '1912094078', 'Khandakar Saleh Mohammad Tajuddin', NULL, '1', '2400', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1095, '1098', 7, NULL, 22, 'NULL', '1', '1', '1717552207', 'K.B.M Abdullah Chiste', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1096, '1099', 7, NULL, 22, 'NULL', '1', '1', '1674122376', 'Md. Hasibuzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1097, '1100', 7, NULL, 22, 'NULL', '2', '1', '1718695127', 'Md. Mehedi Hasan Chowdhury', NULL, '1', '2400', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1098, '1101', 7, NULL, 22, 'NULL', '2', '1', '1672496059', 'Md Zakaria Habib', NULL, NULL, '2400', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1099, '1102', 7, NULL, 22, 'NULL', '1', '1', '1715750309', 'Mahdy Rahman Chowdhury', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1100, '1103', 7, NULL, 22, 'NULL', '1', '1', '1816220760', 'Aquib Zaved', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1101, '1104', 7, NULL, 22, 'NULL', '1', '1', '1717767046', 'Md. Saleh Al Helal', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1102, '1105', 7, NULL, 22, 'NULL', '2', '1', NULL, 'Asif Arefin', '1', '1', '2400', 'NULL', 'NULL', 'home_delivery', NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(1103, '1106', 7, NULL, 22, 'NULL', '1', '1', '1737557381', 'Imtiaz Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1104, '1107', 7, NULL, 22, 'NULL', '1', '1', '1718161408', 'Tajwar Akram Sakapi Ibne Sazzad', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1105, '1108', 7, NULL, 22, 'NULL', '1', '1', '1553622500', 'Md Saimon Sikdar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1106, '1109', 7, NULL, 22, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1107, '1110', 7, NULL, 22, 'NULL', '1', '1', '1670244631', 'Reasad Amin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1108, '1111', 7, NULL, 22, 'NULL', '1', '1', '1711468909', 'Rehghir AL Shahid', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1109, '1112', 7, NULL, 22, 'NULL', '1', '1', '1556330529', 'Abdullah AL Mamun', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1110, '1113', 7, NULL, 22, 'NULL', '1', '1', '1712327094', 'Md Mehedi Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1111, '1114', 7, NULL, 22, 'NULL', '1', '1', '1720546917', 'Md Atiqur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1112, '1115', 7, NULL, 22, 'NULL', '1', '1', '1738090565', 'Md. Omar Faruk', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1113, '1116', 7, NULL, 22, 'NULL', '1', '1', '1722750369', 'Helal Morshed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1114, '1117', 7, NULL, 22, 'NULL', '1', '1', '1915118904', 'Md Sabbir Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1115, '1118', 7, NULL, 22, 'NULL', '1', '1', '1717565801', 'Md Miranuzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1116, '1119', 7, NULL, 22, 'NULL', '1', '1', '1558758410', 'Md Maksudul Alam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1117, '1120', 7, NULL, 22, 'NULL', '1', '1', '1717172433', 'Md Mosiur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1118, '1121', 7, NULL, 22, 'NULL', '1', '1', '1717873059', 'Md Salman Arefin Siddique', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1119, '1122', 7, NULL, 22, 'NULL', '1', '1', '1931321864', 'Sadat Mohammad Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1120, '1123', 7, NULL, 22, 'NULL', '1', '1', '1717137395', 'Md Rakib Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1121, '1124', 7, NULL, 22, 'NULL', '1', '1', NULL, 'Saeeduzzaman Chowdhury', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1122, '1125', 7, NULL, 22, 'NULL', '1', '1', '1717338261', 'Md Shahriar Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1123, '1126', 7, NULL, 22, 'NULL', '1', '1', '1730027419', 'Mamunur Rashid', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1124, '1127', 7, NULL, 22, 'NULL', '1', '1', '1717212519', 'Tarif Al Mustakim', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1125, '1128', 7, NULL, 22, 'NULL', '1', '1', '1917295449', 'Asaduzzaman Talukdar Ronok', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1126, '1129', 7, NULL, 22, 'NULL', '1', '1', '1710494752', 'K.M.Moniruzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1127, '1130', 7, NULL, 22, 'NULL', '1', '1', '1717624928', 'Md Sohel Rana(Sohel)', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1128, '1131', 7, NULL, 22, 'NULL', '1', '1', '1718645325', 'Md Salauddin Akbar(Shamrat)', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1129, '1132', 7, NULL, 22, 'NULL', '1', '1', '1670222323', 'Mehedi hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1130, '1133', 7, NULL, 22, 'NULL', '1', '1', '1717365366', 'Md Safiul Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1131, '1134', 7, NULL, 22, 'NULL', '1', '1', '1730027430', 'Khurrom jah Murad', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1132, '1135', 7, NULL, 22, 'NULL', '1', '1', '1675083448', 'Mohammad Masud   ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1133, '1136', 7, NULL, 22, 'NULL', '1', '1', '1716637350', 'A.R.N Shahinur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1134, '1137', 7, NULL, 23, 'NULL', '1', '1', '1716104535', 'Asif Shaikat', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1135, '1138', 7, NULL, 23, 'NULL', '2', '1', '1912935283', 'Md. Khairul Alam Khan (Rana)', NULL, NULL, '2400', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1136, '1139', 7, NULL, 23, 'NULL', '1', '1', '1717860997', 'Abu Bakar Siddique', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1137, '1140', 7, NULL, 23, 'NULL', '1', '1', '1717549801', 'Mehedi hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1138, '1141', 7, NULL, 23, 'NULL', '1', '1', '1674616054', 'Abdullah - Al- Mamun', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1139, '1142', 7, NULL, 23, 'NULL', '1', '1', NULL, 'Md. Dinar Ibn Raihan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1140, '1143', 7, NULL, 23, 'NULL', '1', '1', '1722059422', 'Md. Zahidur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1141, '1144', 7, NULL, 23, 'NULL', '1', '1', NULL, 'SHIBLY NOMAN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1142, '1145', 7, NULL, 23, 'NULL', '1', '1', '1710551749', 'Md. Mahbub Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1143, '1146', 7, NULL, 23, 'NULL', '1', '1', '1741494233', 'A.H.M Kamrul Hasan rony', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1144, '1147', 7, NULL, 23, 'NULL', '2', '1', '1717852090', 'S.M.Mahmudul Hasan ', NULL, '1', '2400', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1145, '1148', 7, NULL, 23, 'NULL', '1', '1', '1722066923', 'Ahmad Esha', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1146, '1149', 7, NULL, 23, 'NULL', '1', '1', NULL, 'Md Afrahim Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1147, '1150', 7, NULL, 23, 'NULL', '1', '2', '1722141751', 'Maisum Najib', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1148, '1151', 7, NULL, 23, 'NULL', '1', '1', '1811418825', 'Arif Md. Amirul Abrar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1149, '1152', 7, NULL, 23, 'NULL', '1', '1', '1717764649', 'NUR IMTIAZ MAHMUD', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1150, '1153', 7, NULL, 23, 'NULL', '1', '1', NULL, 'MD.SAZZADUL AHSAN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1151, '1154', 7, NULL, 23, 'NULL', '1', '1', '1717880570', 'zubair barkat', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1152, '1155', 7, NULL, 23, 'NULL', '1', '1', '1673652248', 'Kazi Tanvir Ahmed Siddiqui', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1153, '1156', 7, NULL, 23, 'NULL', '1', '2', '1717604838', 'Shafeen M Eunus Daud', NULL, '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1154, '1157', 7, NULL, 23, 'NULL', '1', '1', '1678037725', 'MD ZAKARIA MAHMUD ASIF', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1155, '1158', 7, NULL, 23, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1156, '1159', 7, NULL, 23, 'NULL', '1', '1', '1671611804', 'Abrarul haque Asif', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1157, '1160', 7, NULL, 23, 'NULL', '1', '1', '1717404997', 'Abdullah mohammad Roquibul Kabir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1158, '1161', 7, NULL, 23, 'NULL', '1', '1', '1722039111', 'Sakib Mahmud', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1159, '1162', 7, NULL, 23, 'NULL', '1', '1', '1710456950', 'ABM Golam sadik', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1160, '1163', 7, NULL, 23, 'NULL', '1', '1', '1733337815', 'Aleem Al Razee ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1161, '1164', 7, NULL, 23, 'NULL', '1', '1', '1911120390', 'Imran Iqbal Imon', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1162, '1165', 7, NULL, 23, 'NULL', '1', '1', '1675956517', 'Rahat Shams', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1163, '1166', 7, NULL, 23, 'NULL', '1', '2', '1717811104', 'A. T. M. Golam Muktadir', NULL, NULL, 'NULL', '10450', 'NULL', 'online_payment', NULL, NULL, NULL, NULL, NULL, 'false', 'unpaid'),
(1164, '1167', 7, NULL, 23, 'NULL', '1', '1', '1730327720', 'Md Abdullah Al Amin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1165, '1168', 7, NULL, 23, 'NULL', '1', '1', '1714663738', 'Md. Nasim Reza', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1166, '1169', 7, NULL, 23, 'NULL', '1', '1', '1717518620', 'S.M.Sahedul Abedin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1167, '1170', 7, NULL, 23, 'NULL', '1', '1', '1712544668', 'Mustafa Shahriar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1168, '1171', 7, NULL, 23, 'NULL', '1', '1', '1615626480', 'Mehdi hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1169, '1172', 7, NULL, 23, 'NULL', '1', '1', '1675627889', 'Mostafa Nahid Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1170, '1173', 7, NULL, 23, 'NULL', '1', '1', '1712367181', 'Ahsan Habib Nabil', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1171, '1174', 7, NULL, 23, 'NULL', '1', '1', '1678034290', 'Syed Md. Hasan ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1172, '1175', 7, NULL, 23, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1173, '1176', 7, NULL, 23, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1174, '1177', 7, NULL, 23, 'NULL', '1', '1', '1717564194', 'Rabby Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1175, '1178', 7, NULL, 23, 'NULL', '1', '1', '1720113191', 'Md. Hamidur Reza Sajal', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1176, '1179', 7, NULL, 23, 'NULL', '1', '1', '1725371740', 'Md. Shahed Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1177, '1180', 7, NULL, 23, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1178, '1181', 7, NULL, 23, 'NULL', '1', '1', NULL, 'Md.kamruzzaman shajal', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1179, '1182', 7, NULL, 23, 'NULL', '1', '1', '1710599390', 'Mohammad shafiqul ahsan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1180, '1183', 7, NULL, 23, 'NULL', '1', '1', '1722001823', 'B.M.samin monowar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1181, '1184', 7, NULL, 23, 'NULL', '1', '1', NULL, 'MD. KAYSAR HASAN SAURAV', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1182, '1185', 7, NULL, 23, 'NULL', '1', '1', '1737432110', 'Ashik Abdullah', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1183, '1186', 7, NULL, 23, 'NULL', '1', '1', NULL, 'Md.Asaduzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1184, '1187', 7, NULL, 23, 'NULL', '1', '2', '1673825774', 'Risalat Karim Arnob', '1', '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1185, '1188', 7, NULL, 23, 'NULL', '1', '1', '1717563252', 'Abu Saleh md. Nayeem', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1186, '1189', 7, NULL, 23, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1187, '1190', 7, NULL, 23, 'NULL', '2', '1', '1710456230', 'Md. Noor-E-Zaman', NULL, '1', '2400', 'NULL', 'NULL', 'online_payment', NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(1188, '1191', 7, NULL, 24, 'NULL', '1', '1', NULL, 'MD. MANZURUL ALAM', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1189, '1192', 7, NULL, 24, 'NULL', '1', '1', NULL, 'Touhidul Islam ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1190, '1193', 7, NULL, 24, 'NULL', '1', '1', NULL, 'Masud Parvez', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1191, '1194', 7, NULL, 24, 'NULL', '1', '1', NULL, 'Kawser Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1192, '1195', 7, NULL, 24, 'NULL', '1', '1', NULL, 'Hasibur Razzak', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1193, '1196', 7, NULL, 24, 'NULL', '1', '1', NULL, 'Ahmed Imtiaz', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1194, '1197', 7, NULL, 24, 'NULL', '2', '1', NULL, 'Rezwan Sarker', NULL, '1', '1200', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1195, '1198', 7, NULL, 24, 'NULL', '1', '1', NULL, 'Boni', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1196, '1199', 7, NULL, 24, 'NULL', '1', '1', NULL, 'Tanjilur Rahman Roman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1197, '1200', 7, NULL, 24, 'NULL', '1', '1', NULL, 'Momin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1198, '1201', 7, NULL, 24, 'NULL', '1', '1', NULL, 'Samiul Sani ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1199, '1202', 7, NULL, 24, 'NULL', '1', '1', NULL, 'Mustak Mahmud Imran', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1200, '1203', 7, NULL, 24, 'NULL', '2', '1', '1710487780', 'Md. Shihabul Karim', NULL, '1', '1200', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1201, '1204', 7, NULL, 24, 'NULL', '2', '1', '1720500853', 'Abu Jubaer', NULL, '1', '1200', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1202, '1205', 7, NULL, 24, 'NULL', '1', '1', '1734579197', 'Rifat Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1203, '1206', 7, NULL, 24, 'NULL', '2', '1', '1922031614', 'A M Al Amin', NULL, '1', '1200', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1204, '1207', 7, NULL, 24, 'NULL', '1', '1', '1734001122', 'Istiak Ahmed Sifat', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1205, '1208', 7, NULL, 24, 'NULL', '1', '1', '1673494341', 'Imrul', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1206, '1209', 7, NULL, 24, 'NULL', '1', '1', '1614617899', 'Arefin Kabir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1207, '1210', 7, NULL, 24, 'NULL', '1', '1', '1717557170', 'Shihan Muntasir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1208, '1211', 7, NULL, 24, 'NULL', '1', '1', '1914758077', 'Md. Azherul islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1209, '1212', 7, NULL, 24, 'NULL', '1', '1', '1718463666', 'Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1210, '1213', 7, NULL, 24, 'NULL', '1', '1', '1924234179', 'Shahid', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1211, '1214', 7, NULL, 24, 'NULL', '1', '1', '1675008228', 'Amir e Mohiuddin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1212, '1215', 7, NULL, 24, 'NULL', '2', '1', '1717215242', 'Sakib mahfuizul ', NULL, '1', '1200', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1213, '1216', 7, NULL, 24, 'NULL', '1', '1', '1720199351', 'Mehedi Hasan Sarker', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1214, '1217', 7, NULL, 24, 'NULL', '1', '1', '1711578500', 'Sameer Azad', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1215, '1218', 7, NULL, 24, 'NULL', '1', '1', NULL, 'Asif', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1216, '1219', 7, NULL, 24, 'NULL', '1', '1', NULL, 'Kh. Faisal Bari', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1217, '1220', 7, NULL, 24, 'NULL', '1', '1', '1671440412', 'Sifat e Rabby', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1218, '1221', 7, NULL, 24, 'NULL', '1', '1', '1674758989', 'Salah Uddin Ahmed Sohan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1219, '1222', 7, NULL, 24, 'NULL', '1', '1', '1914075185', 'Zahid', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1220, '1223', 7, NULL, 24, 'NULL', '1', '1', '1197230363', 'Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1221, '1224', 7, NULL, 24, 'NULL', '1', '1', '1722394122', 'Kaiser Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1222, '1225', 7, NULL, 24, 'NULL', '1', '1', '1715194104', 'Mamun', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1223, '1226', 7, NULL, 24, 'NULL', '1', '1', '1913884933', 'Sabirul Mostofa', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1224, '1227', 7, NULL, 24, 'NULL', '1', '1', '1729802798', 'Moin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1225, '1228', 7, NULL, 24, 'NULL', '1', '1', '1912367509', 'Hamza', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1226, '1229', 7, NULL, 24, 'NULL', '1', '1', '1717316173', 'Sohel', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1227, '1230', 7, NULL, 24, 'NULL', '1', '1', '1929186769', 'Nazmul Sakib', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1228, '1231', 7, NULL, 24, 'NULL', '1', '1', '1553117109', 'Md. Moshiur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1229, '1232', 7, NULL, 24, 'NULL', '1', '1', '1719096949', 'Rashid  ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1230, '1233', 7, NULL, 24, 'NULL', '1', '1', '1671407955', 'Shahriar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1231, '1234', 7, NULL, 24, 'NULL', '1', '1', '1614617969', 'Shibly  ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1232, '1235', 7, NULL, 24, 'NULL', '1', '1', '1916832500', 'Masud  ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1233, '1236', 7, NULL, 24, 'NULL', '1', '1', NULL, 'Abu Hena Mostofa Kamal', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1234, '1237', 7, NULL, 24, 'NULL', '1', '1', '1718747243', 'Md.Magfi reza siddique', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1235, '1238', 7, NULL, 24, 'NULL', '1', '1', '1670634995', 'Sarwar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1236, '1239', 7, NULL, 24, 'NULL', '1', '1', '1614617901', 'Shameem', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1237, '1240', 7, NULL, 24, 'NULL', '1', '1', '1918816708', 'Md Naimul Islam Khan ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1238, '1241', 7, NULL, 24, 'NULL', '1', '1', '1616466646', 'Nayeem', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1239, '1242', 7, NULL, 24, 'NULL', '2', '1', '1914758042', 'Ahsan', NULL, '1', '1200', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1240, '1243', 7, NULL, 24, 'NULL', '1', '1', '1674911309', 'Mahmud', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1241, '1244', 7, NULL, 24, 'NULL', '1', '1', '1614617906', 'Sadik', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1242, '1245', 7, NULL, 25, 'NULL', '1', '1', '1912671934', 'Shahaduzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1243, '1246', 7, NULL, 25, 'NULL', '1', '1', '1674086060', 'Raihan Mustafiz', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1244, '1247', 7, NULL, 25, 'NULL', '2', '1', NULL, 'Iftekhar Abedin', '1', '1', '1200', 'NULL', 'NULL', 'home_delivery', NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(1245, '1248', 7, NULL, 25, 'NULL', '1', '1', '1925270664', 'Isteak', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1246, '1249', 7, NULL, 25, 'NULL', '1', '1', '1673909424', 'Tanvir ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1247, '1250', 7, NULL, 25, 'NULL', '1', '1', '1928412062', 'Shovon', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1248, '1251', 7, NULL, 25, 'NULL', '1', '1', '1722358247', 'Sayeem', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1249, '1252', 7, NULL, 25, 'NULL', '1', '1', NULL, 'Sadat', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1250, '1253', 7, NULL, 25, 'NULL', '1', '1', NULL, 'Rafi Morshed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1251, '1254', 7, NULL, 25, 'NULL', '1', '1', NULL, 'Jesan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1252, '1255', 7, NULL, 25, 'NULL', '1', '1', NULL, 'Masud', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `college_validation` (`college_validation_id`, `student_number`, `college_id`, `year_id`, `batch_id`, `booking_status`, `is_anual_subcribe`, `is_life_time_subscribe`, `contact_number`, `student_name`, `is_spouse`, `is_self`, `anual_subsciption_cost`, `life_time_subscription_cost`, `token_no`, `subcription_delivery_method`, `customer_address`, `city`, `discrict_id`, `zip_code`, `val_id`, `is_online`, `payment_status`) VALUES
(1253, '1256', 7, NULL, 25, 'NULL', '1', '1', '1671532848', 'Kamruzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1254, '1257', 7, NULL, 25, 'NULL', '1', '1', '1710063555', 'Rakib', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1255, '1258', 7, NULL, 25, 'NULL', '1', '1', NULL, 'Hasib', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1256, '1259', 7, NULL, 25, 'NULL', '1', '1', '1937259849', 'Aiyub', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1257, '1260', 7, NULL, 25, 'NULL', '1', '1', '1717821706', 'Roman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1258, '1261', 7, NULL, 25, 'NULL', '1', '1', '1937121612', 'Shahriar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1259, '1262', 7, NULL, 25, 'NULL', '1', '1', NULL, 'Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1260, '1263', 7, NULL, 25, 'NULL', '1', '1', '1816111200', 'Noor', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1261, '1264', 7, NULL, 25, 'NULL', '1', '1', '1911253227', 'Iqbal', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1262, '1265', 7, NULL, 25, 'NULL', '1', '1', NULL, 'Tayef', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1263, '1266', 7, NULL, 25, 'NULL', '1', '1', '192004929', 'Shafique', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1264, '1267', 7, NULL, 25, 'NULL', '1', '1', NULL, 'Sayeed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1265, '1268', 7, NULL, 25, 'NULL', '1', '1', '1920300308', 'Muhib', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1266, '1269', 7, NULL, 25, 'NULL', '1', '1', '1719434156', 'Mehran', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1267, '1270', 7, NULL, 25, 'NULL', '1', '1', '1672642491', 'Zahir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1268, '1271', 7, NULL, 25, 'NULL', '1', '1', NULL, 'Anis', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1269, '1272', 7, NULL, 25, 'NULL', '1', '1', '1718817876', 'Mamun', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1270, '1273', 7, NULL, 25, 'NULL', '1', '1', NULL, 'Rezwan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1271, '1274', 7, NULL, 25, 'NULL', '1', '1', NULL, 'Imran', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1272, '1275', 7, NULL, 25, 'NULL', '1', '1', '1925247674', 'Nafees', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1273, '1276', 7, NULL, 25, 'NULL', '1', '1', '1913387553', 'Mohaimin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1274, '1277', 7, NULL, 25, 'NULL', '1', '2', '1722054553', 'Md. Saidul Islam', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1275, '1278', 7, NULL, 25, 'NULL', '1', '1', NULL, 'Rezwan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1276, '1279', 7, NULL, 25, 'NULL', '1', '1', '1718801616', 'Abid', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1277, '1280', 7, NULL, 25, 'NULL', '1', '1', NULL, 'Faruk', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1278, '1281', 7, NULL, 25, 'NULL', '1', '2', '1916194825', 'Mahmud', NULL, '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1279, '1282', 7, NULL, 25, 'NULL', '1', '1', '1730039916', 'Asif', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1280, '1283', 7, NULL, 25, 'NULL', '1', '1', '1719367176', 'Atiq', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1281, '1284', 7, NULL, 25, 'NULL', '2', '1', NULL, 'Murad', NULL, '1', '1200', 'NULL', 'NULL', 'online_payment', NULL, NULL, NULL, NULL, NULL, NULL, 'paid'),
(1282, '1285', 7, NULL, 25, 'NULL', '1', '1', NULL, 'Rashid', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1283, '1286', 7, NULL, 25, 'NULL', '1', '1', NULL, 'Amin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1284, '1287', 7, NULL, 25, 'NULL', '2', '1', '1712590756', 'Mehedi', NULL, '1', '1200', 'NULL', 'NULL', 'online_payment', NULL, NULL, NULL, NULL, NULL, NULL, 'paid'),
(1285, '1288', 7, NULL, 25, 'NULL', '1', '1', '1732931010', 'Sajib', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1286, '1289', 7, NULL, 25, 'NULL', '1', '1', NULL, 'Sohel', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1287, '1290', 7, NULL, 25, 'NULL', '1', '1', '1723779154', 'Rasel', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1288, '1291', 7, NULL, 25, 'NULL', '1', '1', NULL, 'Arafat', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1289, '1292', 7, NULL, 25, 'NULL', '1', '1', '1673125383', 'Abdullah', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1290, '1293', 7, NULL, 25, 'NULL', '1', '1', '1675963803', 'Monir ', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1291, '1294', 7, NULL, 25, 'NULL', '1', '1', '1719437838', 'Ashfak', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1292, '1295', 7, NULL, 26, 'NULL', '1', '1', '1914758034', 'Azfar Bin Anis  Nirjhor', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1293, '1296', 7, NULL, 26, 'NULL', '1', '1', NULL, 'Musfiqul Sayed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1294, '1297', 7, NULL, 26, 'NULL', '1', '1', '1677366114', 'Ashraful Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1295, '1298', 7, NULL, 26, 'NULL', '1', '1', NULL, 'Monirul Islam Shahin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1296, '1299', 7, NULL, 26, 'NULL', '1', '1', '1674869923', 'Md Yusuf Mahmud', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1297, '1300', 7, NULL, 26, 'NULL', '1', '1', '1616908070', 'S. M Omar Kaosar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1298, '1301', 7, NULL, 26, 'NULL', '1', '1', '1671532895', 'Mahmudul Hasan Faisal', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1299, '1302', 7, NULL, 26, 'NULL', '2', '1', '1671532894', 'Hasib Al Ahsan', NULL, '1', '1200', 'NULL', 'NULL', 'home_delivery', NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(1300, '1303', 7, NULL, 26, 'NULL', '1', '1', '1738191910', 'Ahaduzzaman Parvez', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1301, '1304', 7, NULL, 26, 'NULL', '1', '1', NULL, 'Shariful Islam Sozal', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1302, '1305', 7, NULL, 26, 'NULL', '1', '1', '1757117325', 'Omar Faruque Reza', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1303, '1306', 7, NULL, 26, 'NULL', '1', '1', NULL, 'Latiful bari', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1304, '1307', 7, NULL, 26, 'NULL', '1', '1', '1919407030', 'K. M. Mamunur Rashid', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1305, '1308', 7, NULL, 26, 'NULL', '1', '1', '1750059266', 'Musfiqur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1306, '1309', 7, NULL, 26, 'NULL', '1', '1', '1916535882', 'Mahedi Hasan Ibne Saif', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1307, '1310', 7, NULL, 26, 'NULL', '1', '1', '1915809488', 'Mostafa Kamal', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1308, '1311', 7, NULL, 26, 'NULL', '1', '1', '1914959383', 'Nazmul Hossain nahid', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1309, '1312', 7, NULL, 26, 'NULL', '1', '1', '1922889846', 'Rabbi Hasan Anik', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1310, '1313', 7, NULL, 26, 'NULL', '1', '2', '1671032180', 'Md Shakib Hasan', NULL, '1', 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1311, '1314', 7, NULL, 26, 'NULL', '1', '1', '1916745123', 'Shahriar parvez', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1312, '1315', 7, NULL, 26, 'NULL', '1', '1', '1674307096', 'Nafiz Amin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1313, '1316', 7, NULL, 26, 'NULL', '1', '1', '1676699474', 'Firoj Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1314, '1317', 7, NULL, 26, 'NULL', '1', '1', NULL, 'Tareq Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1315, '1318', 7, NULL, 26, 'NULL', '1', '1', '1672875960', 'Al Imran Bin Khodadad', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1316, '1319', 7, NULL, 26, 'NULL', '1', '1', NULL, 'Eshraqul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1317, '1320', 7, NULL, 26, 'NULL', '1', '1', NULL, 'Abdul Hannan', NULL, NULL, NULL, NULL, 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1318, '1321', 7, NULL, 26, 'NULL', '1', '1', '1710488548', 'Md Shohel Rana', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1319, '1322', 7, NULL, 26, 'NULL', '1', '1', '1671600597', 'Faisal Arefin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1320, '1323', 7, NULL, 26, 'NULL', '2', '1', '1741322232', 'Borhan Uddin Khan', NULL, '1', '1200', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1321, '1324', 7, NULL, 26, 'NULL', '1', '1', '1917371429', 'Md Anisur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1322, '1325', 7, NULL, 26, 'NULL', '1', '1', '1672320484', 'Rehanur Zaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1323, '1326', 7, NULL, 26, 'NULL', '1', '1', NULL, 'Md Hossain Sheikh Ripon', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1324, '1327', 7, NULL, 26, 'NULL', '1', '1', NULL, 'Sabbir  Ahmed Sabuj', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1325, '1328', 7, NULL, 26, 'NULL', '1', '1', '1674906728', 'Tauhid- Uz- Zaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1326, '1329', 7, NULL, 26, 'NULL', '1', '1', '1673251998', 'Faisal Imam Sourav', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1327, '1330', 7, NULL, 26, 'NULL', '1', '1', NULL, 'Abhijeet', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1328, '1331', 7, NULL, 26, 'NULL', '1', '1', NULL, 'Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1329, '1332', 7, NULL, 26, 'NULL', '1', '1', '1671344844', 'Hasib Al Mahbub', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1330, '1333', 7, NULL, 26, 'NULL', '1', '1', '1722486969', 'Nasimuzzaman Limon', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1331, '1334', 7, NULL, 26, 'NULL', '1', '1', '1921954647', 'Rafiqul Islam Razu', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1332, '1335', 7, NULL, 26, 'NULL', '1', '1', '1676763470', 'Azmot Ullah Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1333, '1336', 7, NULL, 26, 'NULL', '1', '1', '1674899419', 'Mahmudul Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1334, '1337', 7, NULL, 26, 'NULL', '1', '1', '1937027020', 'Abu Shahiran', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1335, '1338', 7, NULL, 26, 'NULL', '1', '1', NULL, 'Sazzad Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1336, '1339', 7, NULL, 26, 'NULL', '1', '1', '1923296014', 'Md Mahmudul Hasan', NULL, NULL, NULL, 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1337, '1340', 7, NULL, 26, 'NULL', '1', '1', '1675966915', 'Mujahidul Hakim Maruf', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1338, '1341', 7, NULL, 26, 'NULL', '1', '1', NULL, 'Late Mahbub Al Mehedi Jisan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1339, '1342', 7, NULL, 26, 'NULL', '1', '1', '1675867761', 'Ahsanul Kabir Ferdous', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1340, '1343', 7, NULL, 26, 'NULL', '1', '1', '1722655370', 'Fazle Elahi Dani', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1341, '1344', 7, NULL, 26, 'NULL', '1', '1', '1914758146', 'Md Tariq aziz', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1342, '1345', 7, NULL, 27, 'NULL', '1', '1', '1712835642', 'Istiak Ahmed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1343, '1346', 7, NULL, 27, 'NULL', '1', '1', '1920140001', 'Md.Samiul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1344, '1347', 7, NULL, 27, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1345, '1348', 7, NULL, 27, 'NULL', '1', '2', '1712534993', 'Shourav Kumar Paul', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1346, '1349', 7, NULL, 27, 'NULL', '1', '1', '1718578184', 'Mohammad Sadequl Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1347, '1350', 7, NULL, 27, 'NULL', '1', '1', '1673054762', 'Shahriar Iqbal', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1348, '1351', 7, NULL, 27, 'NULL', '1', '1', '1676436945', 'Iftekhar Siddique', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1349, '1352', 7, NULL, 27, 'NULL', '1', '1', '1914758151', 'Imran Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1350, '1353', 7, NULL, 27, 'NULL', '1', '1', '1675186135', 'Rashid Shahriar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1351, '1354', 7, NULL, 27, 'NULL', '1', '1', '1937004033', 'Srabon Mahmud', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1352, '1355', 7, NULL, 27, 'NULL', '1', '1', '1734727760', 'Aftarul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1353, '1356', 7, NULL, 27, 'NULL', '1', '1', '1912209216', 'Islamul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1354, '1357', 7, NULL, 27, 'NULL', '2', '1', '1746323864', 'Mostafa Mashnoon Kamal', NULL, '1', '1200', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1355, '1358', 7, NULL, 27, 'NULL', '1', '1', '1717824420', 'Sadequr Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1356, '1359', 7, NULL, 27, 'NULL', '1', '1', '1930975928', 'Hasib Jubayer Himel', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1357, '1360', 7, NULL, 27, 'NULL', '1', '1', '1912486533', 'Md.Abdullah Al Saleh', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1358, '1361', 7, NULL, 27, 'NULL', '1', '1', '1742523100', 'Sadman Sakib', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1359, '1362', 7, NULL, 27, 'NULL', '1', '1', '1916009479', 'Nayeemur Rahman Khaled', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1360, '1363', 7, NULL, 27, 'NULL', '1', '1', '1737521982', 'Shahnawaz Bin Sayeed', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1361, '1364', 7, NULL, 27, 'NULL', '1', '1', '1752134424', 'Imran Ahmed Uzzal', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1362, '1365', 7, NULL, 27, 'NULL', '1', '1', '1676036510', 'Atikur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1363, '1366', 7, NULL, 27, 'NULL', '1', '1', '1710500208', 'Asif Iqbal', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1364, '1367', 7, NULL, 27, 'NULL', '2', '1', '1675912115', 'Shahriar Bulbul Tonmoy', NULL, '1', '1200', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1365, '1368', 7, NULL, 27, 'NULL', '1', '1', '1914758038', 'Md.Sabbir Ahmed Sajib', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1366, '1369', 7, NULL, 27, 'NULL', '1', '1', '1675983280', 'Rezwanur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1367, '1370', 7, NULL, 27, 'NULL', '1', '1', '1914219903', 'Rifat Bin Asad', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1368, '1371', 7, NULL, 27, 'NULL', '1', '1', '1923625092', 'Shahtab Ahmed Khan Abir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1369, '1372', 7, NULL, 27, 'NULL', '1', '1', '1673565259', 'Arman Ibn Rashid', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1370, '1373', 7, NULL, 27, 'NULL', '1', '1', '1711391546', 'Sabbir Ibn Monzur', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1371, '1374', 7, NULL, 27, 'NULL', '1', '1', '1738151261', 'Rakesh Basak', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1372, '1375', 7, NULL, 27, 'NULL', '1', '1', '1674970775', 'Md.Riadus Salehin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1373, '1376', 7, NULL, 27, 'NULL', '2', '1', '1711458638', 'Sohan Bin Anwar Siddique', NULL, '1', '1200', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1374, '1377', 7, NULL, 27, 'NULL', '1', '1', '1674538439', 'Alimul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1375, '1378', 7, NULL, 27, 'NULL', '1', '1', '1724676970', 'Md. Nahidul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1376, '1379', 7, NULL, 27, 'NULL', '1', '1', '1751468797', 'Ruhul Quddus Rabbi', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1377, '1380', 7, NULL, 27, 'NULL', '1', '1', '1931167820', 'Hasan Imam Faisal', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1378, '1381', 7, NULL, 27, 'NULL', '1', '1', '1673135652', 'Mohammad Saifuddin Rony', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1379, '1382', 7, NULL, 27, 'NULL', '1', '1', '1675289848', 'Shuvo Ahmed Majumder', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1380, '1383', 7, NULL, 27, 'NULL', '1', '1', '1611750760', 'Mufti Mahmud Joy', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1381, '1384', 7, NULL, 27, 'NULL', '1', '1', '1975436361', 'S.M.Naimul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1382, '1385', 7, NULL, 27, 'NULL', '1', '1', '1916567890', 'Md.Shahriar Sagor', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1383, '1386', 7, NULL, 27, 'NULL', '1', '1', NULL, 'S.M.Hasan Israfil Ananda', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1384, '1387', 7, NULL, 27, 'NULL', '1', '1', '1930516757', 'Md.Riyad Mehadi', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1385, '1388', 7, NULL, 27, 'NULL', '1', '1', '1911834079', 'Nayreet Islam Rupok', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1386, '1389', 7, NULL, 27, 'NULL', '1', '1', NULL, 'Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1387, '1390', 7, NULL, 27, 'NULL', '1', '1', '1676017679', 'Monsurul Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1388, '1391', 7, NULL, 27, 'NULL', '1', '1', '1741783557', 'Rezwanul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1389, '1392', 7, NULL, 27, 'NULL', '1', '1', '1917800966', 'Rafiul Bari', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1390, '1393', 7, NULL, 27, 'NULL', '1', '1', '1673340823', 'Md.Ahsan Habib Tapu', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1391, '1394', 7, NULL, 27, 'NULL', '1', '1', '1920751144', 'Mustansir Billah', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1392, '1395', 7, NULL, 28, 'NULL', '1', '1', NULL, 'Masud', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1393, '1396', 7, NULL, 28, 'NULL', '1', '1', NULL, 'Salim', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1394, '1397', 7, NULL, 28, 'NULL', '1', '1', NULL, 'Mohammad Hasibul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1395, '1398', 7, NULL, 28, 'NULL', '1', '1', '1733331398', 'Mostafizur rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1396, '1399', 7, NULL, 28, 'NULL', '1', '1', '1740102274', 'Md. Nahid Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1397, '1400', 7, NULL, 28, 'NULL', '1', '1', '1733331400', 'Mahbub-Ul-Haque Rizon', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1398, '1401', 7, NULL, 28, 'NULL', '1', '1', '1733331401', 'Mehedi Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1399, '1402', 7, NULL, 28, 'NULL', '1', '1', '1717872703', 'MD.AZADUL ISLAM', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1400, '1403', 7, NULL, 28, 'NULL', '1', '1', '16836893692', 'Md Rakibul Hasan Roni', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1401, '1404', 7, NULL, 28, 'NULL', '1', '1', '1733331404', 'SHOHANUL ISLAM', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1402, '1405', 7, NULL, 28, 'NULL', '1', '1', '1733331405', 'Mithun Sarkar', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1403, '1406', 7, NULL, 28, 'NULL', '1', '1', '1733331406', 'Arif', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1404, '1407', 7, NULL, 28, 'NULL', '1', '1', '1684531407', 'Md. Rezwanul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1405, '1408', 7, NULL, 28, 'NULL', '1', '1', '1733331408', 'Md Jobayer Mahmud', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1406, '1409', 7, NULL, 28, 'NULL', '1', '1', '1733331409', 'Md.Ashfakur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1407, '1410', 7, NULL, 28, 'NULL', '1', '1', '1682793167', 'ASHIKUZZAMAN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1408, '1411', 7, NULL, 28, 'NULL', '1', '1', '1989783149', 'Nur - A - Taskin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1409, '1412', 7, NULL, 28, 'NULL', '1', '1', '1726285554', 'Rizwan Tareq', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1410, '1413', 7, NULL, 28, 'NULL', '1', '1', '1711083870', 'Md RAkibul Islam Protik', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1411, '1414', 7, NULL, 28, 'NULL', '1', '1', '1677635960', 'Mahbub Al hasib', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1412, '1415', 7, NULL, 28, 'NULL', '1', '1', '1918925524', 'Asif Shahriar Shakil', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1413, '1416', 7, NULL, 28, 'NULL', '1', '2', '61420295883', 'NABIL BIN FARUK', NULL, NULL, 'NULL', '10450', 'NULL', 'pick_from_point', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1414, '1417', 7, NULL, 28, 'NULL', '1', '1', '1620861526', 'Redwan Siddik', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1415, '1418', 7, NULL, 28, 'NULL', '1', '1', '8.80E+12', 'Ashhab Anwar Awny', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1416, '1419', 7, NULL, 28, 'NULL', '1', '1', NULL, 'Shambin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1417, '1420', 7, NULL, 28, 'NULL', '1', '1', '8.80E+12', 'Md. Asifur Rahman Rumon', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1418, '1421', 7, NULL, 28, 'NULL', '1', '1', '1748421352', 'setu basak', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1419, '1422', 7, NULL, 28, 'NULL', '1', '1', '1733331422', 'Md Tasikuzzaman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1420, '1423', 7, NULL, 28, 'NULL', '1', '1', '1946771423', 'Md. Mamunur Rashid', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1421, '1424', 7, NULL, 28, 'NULL', '1', '1', '1688648656', 'Md. Shakil Uddin', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1422, '1425', 7, NULL, 28, 'NULL', '1', '1', '1847015451', 'Hasan Al Mahmood', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1423, '1426', 7, NULL, 28, 'NULL', '1', '1', '1680799910', 'MIR ISTIAK AHMED RINKUE', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1424, '1427', 7, NULL, 28, 'NULL', '1', '1', '1843161672', 'MD. SAKHAWAT BIN AZAD', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1425, '1428', 7, NULL, 28, 'NULL', '1', '1', '1680027333', 'Sadiqul Hasan Talha', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1426, '1429', 7, NULL, 28, 'NULL', '1', '1', '1623220660', 'Hasan Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1427, '1430', 7, NULL, 28, 'NULL', '1', '1', '1733331430', 'Ishraq khan anonno', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1428, '1431', 7, NULL, 28, 'NULL', '1', '1', NULL, 'Anik', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1429, '1432', 7, NULL, 28, 'NULL', '1', '1', '1784470550', 'NUR-E-ALAM', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1430, '1433', 7, NULL, 28, 'NULL', '1', '1', '1733331433', 'Nurnobi Ferdous', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1431, '1434', 7, NULL, 28, 'NULL', '1', '1', '1754527272', 'Waliullah Khan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1432, '1435', 7, NULL, 28, 'NULL', '1', '1', NULL, 'Kawser', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1433, '1436', 7, NULL, 28, 'NULL', '1', '1', '1685600467', 'Aftab Maheer Ibne Amir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1434, '1437', 7, NULL, 28, 'NULL', '1', '1', '1774497196', 'KH MD MOHAIMINUL ISLAM', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1435, '1438', 7, NULL, 28, 'NULL', '1', '1', '1733331438', 'Nazmul Alam Siddique', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1436, '1439', 7, NULL, 28, 'NULL', '1', '1', NULL, 'Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1437, '1440', 7, NULL, 28, 'NULL', '1', '1', '1733331440', 'Sadat asif', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1438, '1441', 7, NULL, 28, 'NULL', '1', '1', '1674519888', 'A.N.M. ZAKARIA MASUD', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1439, '1442', 7, NULL, 28, 'NULL', '1', '1', '1913546989', 'S.M. Shahriar Kabir Labon', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1440, '1443', 7, NULL, 28, 'NULL', '1', '1', '1733331443', 'Arnab Kumar Paul', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1441, '1444', 7, NULL, 28, 'NULL', '1', '1', '1683292374', 'Sakibul Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1442, '1445', 7, NULL, 29, 'NULL', '2', '1', '1735870906', 'Tanzir Ahmed Khan', NULL, '1', 'NULL', 'NULL', 'NULL', 'home_delivery', NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(1443, '1446', 7, NULL, 29, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1444, '1447', 7, NULL, 29, 'NULL', '1', '1', '7133331447', 'MD.TANZIM HASAN RAHAT', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1445, '1448', 7, NULL, 29, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1446, '1449', 7, NULL, 29, 'NULL', '1', '1', '1733331449', 'MD.ABDUL MALEK', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1447, '1450', 7, NULL, 29, 'NULL', '1', '1', '1733331450', 'MD.EHSANUL HOQUE SHIPON', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1448, '1451', 7, NULL, 29, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1449, '1452', 7, NULL, 29, 'NULL', '1', '1', '1676486602', 'sudipta saha', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1450, '1453', 7, NULL, 29, 'NULL', '1', '1', '1733331453', 'Abdullah Al Mamun', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1451, '1454', 7, NULL, 29, 'NULL', '1', '1', '1733331454', 'Md. Imrul Hussain Sakib', NULL, '1', 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1452, '1455', 7, NULL, 29, 'NULL', '1', '1', '1834351952', 'Md. Redwan Karim Sony', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1453, '1456', 7, NULL, 29, 'NULL', '1', '1', '1733331456', 'MD RASHED KHAN JONY', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1454, '1457', 7, NULL, 29, 'NULL', '1', '1', '1733331457', 'Mohammad Ashikur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1455, '1458', 7, NULL, 29, 'NULL', '1', '1', '1676547160', 'MD.SHAMIM HASAN PIAS', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1456, '1459', 7, NULL, 29, 'NULL', '1', '1', '1934470914', 'Rabiul Al Mahmud', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1457, '1460', 7, NULL, 29, 'NULL', '1', '1', '1683380991', 'MD.SAMIUL HAQUE TANZIL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1458, '1461', 7, NULL, 29, 'NULL', '1', '1', '1677756287', 'SADMAN SOUMIK', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1459, '1462', 7, NULL, 29, 'NULL', '1', '1', '1733331462', 'Md. Moinuddin Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1460, '1463', 7, NULL, 29, 'NULL', '1', '1', '1795357500', 'MAHBUBUL ISLAM NAYIM', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1461, '1464', 7, NULL, 29, 'NULL', '1', '1', '0+8801733-331464', 'RAJENDRA KUMAR ROY', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1462, '1465', 7, NULL, 29, 'NULL', '1', '1', '1733331465', 'MD.MINHAZUL ISLAM', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1463, '1466', 7, NULL, 29, 'NULL', '1', '1', '1733331466', 'A.H.M ATIQUL HAQUE', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1464, '1467', 7, NULL, 29, 'NULL', '1', '1', '1733331467', 'SHAWON ANOGH', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1465, '1468', 7, NULL, 29, 'NULL', '1', '1', '1733331468', 'Md. Arifur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1466, '1469', 7, NULL, 29, 'NULL', '1', '1', '1733331469', 'Araf-Uz-Zaman Saikat', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1467, '1470', 7, NULL, 29, 'NULL', '1', '1', '1733331470', 'MD. SAIFUL ISLAM', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1468, '1471', 7, NULL, 29, 'NULL', '1', '1', '8075088', 'AHMED MANZIM RIDWAN SOUMIK', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1469, '1472', 7, NULL, 29, 'NULL', '1', '1', '1733331472', 'M Mubin Rahman Tapos', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1470, '1473', 7, NULL, 29, 'NULL', '1', '1', '1682400458', 'MD.MAHMUD HASAN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1471, '1474', 7, NULL, 29, 'NULL', '1', '1', '1733331474', 'Sabab hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1472, '1475', 7, NULL, 29, 'NULL', '1', '1', '1799277732', 'MD.ILYAS HOSSAIN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1473, '1476', 7, NULL, 29, 'NULL', '1', '1', '1733331476', 'Iftekhar Sarowar Dhrubo', NULL, '1', 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1474, '1477', 7, NULL, 29, 'NULL', '1', '1', '1733331477', 'Shakir Ar Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1475, '1478', 7, NULL, 29, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1476, '1479', 7, NULL, 29, 'NULL', '1', '1', '1733331479', 'A.F Ataur Rafee', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1477, '1480', 7, NULL, 29, 'NULL', '1', '1', '8.80E+12', 'MD. ATIQUL ISLAM SHUVO', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1478, '1481', 7, NULL, 29, 'NULL', '1', '1', '1733331481', 'MD.KAWSER RASHED SHOFOL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1479, '1482', 7, NULL, 29, 'NULL', '1', '1', '1733331482', 'MD.RUHUL AMIN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1480, '1483', 7, NULL, 29, 'NULL', '1', '1', '1733331483', 'ISMAIL HOSSAIN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1481, '1484', 7, NULL, 29, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1482, '1485', 7, NULL, 29, 'NULL', '1', '1', '1733331485', 'Tawhid Bin Rahmatullah', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1483, '1486', 7, NULL, 29, 'NULL', '1', '1', '1733331486', 'NOOR MOHAMMAD NIRU', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1484, '1487', 7, NULL, 29, 'NULL', '1', '1', '1685580187', 'Muzahidur Rahman Tanim', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1485, '1488', 7, NULL, 29, 'NULL', '1', '1', '1733331488', 'Sayafur Rahman Turzo', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1486, '1489', 7, NULL, 29, 'NULL', '1', '1', '1733331489', 'MD.TASKIN RAHMAN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1487, '1490', 7, NULL, 29, 'NULL', '2', '1', '8.80E+12', 'M RAKIN SARDER', NULL, '1', 'NULL', 'NULL', 'NULL', 'home_delivery', NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(1488, '1491', 7, NULL, 29, 'NULL', '1', '1', '1733331491', 'masud rana', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1489, '1492', 7, NULL, 29, 'NULL', '1', '1', '1733331492', 'Shahid Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1490, '1493', 7, NULL, 29, 'NULL', '1', '1', '1689575550', 'MD.YEASIR ARAFAT', NULL, '1', 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1491, '1494', 7, NULL, 29, 'NULL', '1', '1', '1733331494', 'Ziaur Rahman Rasel', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1492, '1495', 7, NULL, 29, 'NULL', '1', '1', '1733331485', 'Rakib Muntasir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1493, '1496', 7, NULL, 29, 'NULL', '1', '1', '1783037348', 'Shahriar Shams Uday', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1494, '1497', 7, NULL, 29, 'NULL', '1', '1', '1864240737', 'MD.AMINUL ISLAM', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1495, '1498', 7, NULL, 30, 'NULL', '1', '1', NULL, 'Tamal', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1496, '1499', 7, NULL, 30, 'NULL', '1', '1', NULL, 'Mohaimeen', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1497, '1500', 7, NULL, 30, 'NULL', '1', '1', NULL, 'Rashik', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1498, '1501', 7, NULL, 30, 'NULL', '1', '1', '1733331501', 'MD. MAHMUDUL HASAN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1499, '1502', 7, NULL, 30, 'NULL', '1', '1', '1733331502', 'MD. SAKIB SADAT RABBI', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1500, '1503', 7, NULL, 30, 'NULL', '1', '1', '1786434296', 'Md.Ali Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1501, '1504', 7, NULL, 30, 'NULL', '1', '1', '1733331504', 'ATIK MAHMUD', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1502, '1505', 7, NULL, 30, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1503, '1506', 7, NULL, 30, 'NULL', '1', '1', '1986457763', 'Md. Rifat Mehedi', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1504, '1507', 7, NULL, 30, 'NULL', '1', '2', '61413862017', 'Md Yeasir Arafat', NULL, NULL, 'NULL', '10450', 'NULL', 'online_payment', NULL, NULL, NULL, NULL, NULL, NULL, 'paid'),
(1505, '1508', 7, NULL, 30, 'NULL', '2', '1', '1733331508', 'MD. Mohiminul Islam', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1506, '1509', 7, NULL, 30, 'NULL', '1', '1', '1718219929', 'SUBORNO SOMVAR', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1507, '1510', 7, NULL, 30, 'NULL', '1', '1', '1915529120', 'Md. Ashikur Rahman', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1508, '1511', 7, NULL, 30, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1509, '1512', 7, NULL, 30, 'NULL', '1', '1', '1733331512', 'FARUQ TAHMED', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1510, '1513', 7, NULL, 30, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1511, '1514', 7, NULL, 30, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1512, '1515', 7, NULL, 30, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1513, '1516', 7, NULL, 30, 'NULL', '1', '1', '1681640559', 'Muhammad Nazmus Sadat Sujoy', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1514, '1517', 7, NULL, 30, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1515, '1518', 7, NULL, 30, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1516, '1519', 7, NULL, 30, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1517, '1520', 7, NULL, 30, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1518, '1521', 7, NULL, 30, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1519, '1522', 7, NULL, 30, 'NULL', '1', '1', '60166873213', 'MD MOTAHER HOSSAIN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1520, '1523', 7, NULL, 30, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1521, '1524', 7, NULL, 30, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1522, '1525', 7, NULL, 30, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1523, '1526', 7, NULL, 30, 'NULL', '1', '1', '1733331526', 'A.M. Ehsanul Haque', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1524, '1527', 7, NULL, 30, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1525, '1528', 7, NULL, 30, 'NULL', '1', '1', '1845260491', 'Tamhid Eshad', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1526, '1529', 7, NULL, 30, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1527, '1530', 7, NULL, 30, 'NULL', '1', '1', '1733331530', 'U M MUKRAMINUL ISLAM', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1528, '1531', 7, NULL, 30, 'NULL', '1', '1', '1955707491', 'Fazle Mahdi Pranto', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1529, '1532', 7, NULL, 30, 'NULL', '1', '1', '1733331532', 'Md. Samiun-Raji Sifat', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1530, '1533', 7, NULL, 30, 'NULL', '1', '1', '1733331533', 'MIR AYON ELAHI', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1531, '1534', 7, NULL, 30, 'NULL', '1', '1', '1733331534', 'MD. ABU YUSUF', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1532, '1535', 7, NULL, 30, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1533, '1536', 7, NULL, 30, 'NULL', '1', '1', '1733331536', 'ATANU SAHA', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1534, '1537', 7, NULL, 30, 'NULL', '1', '1', '1733331537', 'SAKIB BIN HASAN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1535, '1538', 7, NULL, 30, 'NULL', '1', '1', '1733331538', 'MD. SHAHEDUR RAHMAN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1536, '1539', 7, NULL, 30, 'NULL', '1', '1', '1733331539', 'Tofael ahmed pasha', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1537, '1540', 7, NULL, 30, 'NULL', '1', '1', '1733331547', 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1538, '1541', 7, NULL, 30, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1539, '1542', 7, NULL, 30, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1540, '1543', 7, NULL, 30, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1541, '1544', 7, NULL, 30, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1542, '1545', 7, NULL, 30, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1543, '1546', 7, NULL, 30, 'NULL', '1', '1', NULL, 'NULL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1544, '1547', 7, NULL, 30, 'NULL', '1', '1', '1733331547', 'Md.Anowarul Azim LIton', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1545, '1548', 7, NULL, 30, 'NULL', '1', '1', NULL, 'Sakib', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1546, '1549', 7, NULL, 30, 'NULL', '1', '1', NULL, 'Monir', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1547, '1550', 7, NULL, 31, 'NULL', '1', '1', '1957135848', 'NOOR E EMRAN KAWSER', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1548, '1551', 7, NULL, 31, 'NULL', '1', '1', '1733331551', 'MD. SAZZAD HOSSAIN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1549, '1552', 7, NULL, 31, 'NULL', '1', '1', '1733331552', 'Juhayar Sadman Shuprove', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1550, '1553', 7, NULL, 31, 'NULL', '1', '1', '1733331553', 'MD. NAHIDUL ISLAM', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1551, '1554', 7, NULL, 31, 'NULL', '1', '1', '1943378404', 'Sunny Rabby', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1552, '1555', 7, NULL, 31, 'NULL', '1', '1', '8.80E+12', 'Md. Hasnain Jalalee Saymoon', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1553, '1556', 7, NULL, 31, 'NULL', '1', '1', '1733331556', 'Shoumic Shahid Chowdhury', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1554, '1557', 7, NULL, 31, 'NULL', '1', '1', '1776064728', 'S.M. Mahmud Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(1555, '1558', 7, NULL, 31, 'NULL', '1', '1', '1733331558', 'Jobayer shajal', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1556, '1559', 7, NULL, 31, 'NULL', '2', '1', '1733331559', 'MD. Shafi Mustafa', NULL, '1', 'NULL', 'NULL', 'NULL', 'home_delivery', NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(1557, '1560', 7, NULL, 31, 'NULL', '1', '1', '1733331560', 'MD SHAKIL RAHMAN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1558, '1561', 7, NULL, 31, 'NULL', '1', '1', '1795865417', 'MD ROKIBUL HASAN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1559, '1562', 7, NULL, 31, 'NULL', '1', '1', '1733331562', 'ZIYAD KABIR JISAN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1560, '1563', 7, NULL, 31, 'NULL', '1', '1', '1733331563', 'MD. ABDULLAH AL ARIF', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1561, '1564', 7, NULL, 31, 'NULL', '1', '1', '1790182894', 'Moinul Islam Tusher', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1562, '1565', 7, NULL, 31, 'NULL', '1', '1', '1733331565', 'ZAGGROTHO NOOR SHOUMO', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `college_validation` (`college_validation_id`, `student_number`, `college_id`, `year_id`, `batch_id`, `booking_status`, `is_anual_subcribe`, `is_life_time_subscribe`, `contact_number`, `student_name`, `is_spouse`, `is_self`, `anual_subsciption_cost`, `life_time_subscription_cost`, `token_no`, `subcription_delivery_method`, `customer_address`, `city`, `discrict_id`, `zip_code`, `val_id`, `is_online`, `payment_status`) VALUES
(1563, '1566', 7, NULL, 31, 'NULL', '1', '1', '1733331566', 'FARHAD HOSSAIN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1564, '1567', 7, NULL, 31, 'NULL', '1', '1', '1733331567', 'NASIF AL MOHIMIN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1565, '1568', 7, NULL, 31, 'NULL', '1', '1', '1733331568', 'Hasib Ul Sakib', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1566, '1569', 7, NULL, 31, 'NULL', '1', '1', '1733331569', 'MD. NAHID HASAN', NULL, '1', 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1567, '1570', 7, NULL, 31, 'NULL', '1', '1', '1733331570', 'MAMUN HOSSAIN REDOY', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1568, '1571', 7, NULL, 31, 'NULL', '1', '1', '1733331571', 'MD. FAZLA ALAM RAHAT', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1569, '1572', 7, NULL, 31, 'NULL', '1', '1', '1733331572', 'Md Ishraq Hossain', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1570, '1573', 7, NULL, 31, 'NULL', '1', '1', '1733331573', 'Md Rakibul Hasan', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1571, '1574', 7, NULL, 31, 'NULL', '1', '1', '1733331574', 'Azmain Ikteder', NULL, '1', 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1572, '1575', 7, NULL, 31, 'NULL', '1', '1', '1733331575', 'Redwan Ratu', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1573, '1576', 7, NULL, 31, 'NULL', '1', '1', '1733331576', 'RAHFIN AHMED', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1574, '1577', 7, NULL, 31, 'NULL', '1', '1', '1733331577', 'MD IMRAN HASAN ABDULLAH', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1575, '1578', 7, NULL, 31, 'NULL', '1', '1', '1733331578', 'ZAHIDUL ISLAM JONY', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1576, '1579', 7, NULL, 31, 'NULL', '1', '1', '1914952860', 'AURNAB DEY AKASH', NULL, '1', 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1577, '1580', 7, NULL, 31, 'NULL', '1', '1', '1733331580', 'MONIR SHAYAN', NULL, '1', 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1578, '1581', 7, NULL, 31, 'NULL', '1', '1', '1733331581', 'MASUD RANA', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1579, '1582', 7, NULL, 31, 'NULL', '1', '1', '1764011577', 'Md Abu Saleh', NULL, '1', 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1580, '1583', 7, NULL, 31, 'NULL', '1', '1', '1951742947', 'Emon Chowdhury', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1581, '1584', 7, NULL, 31, 'NULL', '1', '1', '1733331584', 'SIFAT RAYHAN', NULL, '1', 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1582, '1585', 7, NULL, 31, 'NULL', '1', '1', '1733331585', 'ZAHID HASAN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1583, '1586', 7, NULL, 31, 'NULL', '1', '1', '1733331586', 'MOHAMMAD NAYAN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1584, '1587', 7, NULL, 31, 'NULL', '1', '1', '1733331587', 'KAUSHIK CHAKROBORTY', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1585, '1588', 7, NULL, 31, 'NULL', '1', '1', '1733331588', 'SABUJ AHMED ELITE', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1586, '1589', 7, NULL, 31, 'NULL', '1', '1', '1733331589', 'KIBRIA AHMED', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1587, '1590', 7, NULL, 31, 'NULL', '1', '1', '1733331590', 'MD. SOURAV AMIN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1588, '1591', 7, NULL, 31, 'NULL', '1', '1', '1733331591', 'SAZEDUL ISLAM SHAKIL', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1589, '1592', 7, NULL, 31, 'NULL', '1', '1', '1733331592', 'K. M. RAHAT MUNIR', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1590, '1593', 7, NULL, 31, 'NULL', '1', '1', '1733331593', 'MD. Abdullahil Mahmud', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1591, '1594', 7, NULL, 31, 'NULL', '1', '1', '1733331594', 'MD. ROMMAN SIDDIQUE ESHAN.', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1592, '1595', 7, NULL, 31, 'NULL', '1', '1', '1969167170', 'Dewan Salman Sunny', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1593, '1596', 7, NULL, 31, 'NULL', '1', '1', '1733331596', 'Nur Mohammad', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1594, '1597', 7, NULL, 31, 'NULL', '1', '1', '1733331597', 'SADMAN SAKIB', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1595, '1598', 7, NULL, 31, 'NULL', '2', '1', '1733331598', 'FATIR ABDULLAH', NULL, '1', 'NULL', 'NULL', 'NULL', 'home_delivery', NULL, NULL, NULL, NULL, NULL, NULL, 'unpaid'),
(1596, '1599', 7, NULL, 31, 'NULL', '1', '1', '1733331599', 'MD MUSHFIQUR RAHMAN', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1597, '1600', 7, NULL, 31, 'NULL', '1', '1', '1733331600', 'MD ROKIBUL ALAM', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1598, '1601', 7, NULL, 31, 'NULL', '1', '1', '1733331601', 'Md. Mehedi Hasan Heaven', NULL, NULL, 'NULL', 'NULL', 'NULL', 'online_payment', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1599, '1602', 7, NULL, 31, 'NULL', '1', '1', '1733331602', 'Md. Koushik Khan Pritom', NULL, '1', 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1600, '1603', 7, NULL, 31, 'NULL', '1', '1', '1733331603', 'MEHEDI HASAN SOURAV', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1601, '1604', 7, NULL, 31, 'NULL', '1', '1', '1733331604', 'SAEM AHMED SARKER', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1602, '1605', 7, NULL, 31, 'NULL', '1', '1', '1733826338', 'Rabbi Mushad', NULL, NULL, 'NULL', 'NULL', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1603, '674', 7, NULL, 14, 'NULL', '1', '1', '1553065555', 'S M Shamsur Rahman', NULL, NULL, 'NULL', NULL, 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1604, '939', 7, NULL, 18, 'NULL', '1', '2', '1611483461', 'Rashedul Hasan', NULL, NULL, 'NULL', '10000', 'NULL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE IF NOT EXISTS `contact_us` (
  `contact_us_id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_us_name` varchar(255) DEFAULT NULL,
  `contact_us_email` varchar(255) DEFAULT NULL,
  `contact_us_desc` text,
  PRIMARY KEY (`contact_us_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`contact_us_id`, `contact_us_name`, `contact_us_email`, `contact_us_desc`) VALUES
(1, 'fdffdf', 'admin@gmail.com', 'fdf'),
(2, 'Shamiul Sarkar', 'shishir2180@gmail.com', 'How the payment works in case of home delivery');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `country_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_name` varchar(255) DEFAULT NULL,
  `c_nationality` varchar(255) DEFAULT NULL,
  `language` varchar(255) NOT NULL,
  `is_active` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=240 ;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `country_name`, `c_nationality`, `language`, `is_active`) VALUES
(1, 'Bangladesh', 'Bangladeshi', 'Bangla', 'true'),
(2, 'Australia', 'Australian', 'English', 'true'),
(3, 'Canada', 'Canadian', 'English', 'true'),
(4, 'India', 'Indian', 'Hindi', 'true'),
(5, 'Malaysia', 'Malaysian', 'Malay', 'true'),
(6, 'Singapore', 'Singaporian', 'English', 'true'),
(7, 'Sweden', 'Swedish', 'Swedish', 'true'),
(8, 'Thailand', 'Thai', 'Thai', 'true'),
(9, 'UK', 'British', 'English', 'true'),
(10, 'USA', 'American', 'English', 'true'),
(11, 'Afghanistan', NULL, '', NULL),
(12, 'Albania', NULL, '', NULL),
(13, 'Algeria', NULL, '', NULL),
(14, 'American Samoa', NULL, '', NULL),
(15, 'Andorra', NULL, '', NULL),
(16, 'Angola', NULL, '', NULL),
(17, 'Anguilla', NULL, '', NULL),
(18, 'Antarctica', NULL, '', NULL),
(19, 'Antigua and/or Barbuda', NULL, '', NULL),
(20, 'Argentina', NULL, '', NULL),
(21, 'Armenia', NULL, '', NULL),
(22, 'Aruba', NULL, '', NULL),
(23, 'Austria', NULL, '', NULL),
(24, 'Azerbaijan', NULL, '', NULL),
(25, 'Bahamas', NULL, '', NULL),
(26, 'Bahrain', NULL, '', NULL),
(27, 'Barbados', NULL, '', NULL),
(28, 'Belarus', NULL, '', NULL),
(29, 'Belgium', NULL, '', NULL),
(30, 'Belize', NULL, '', NULL),
(31, 'Benin', NULL, '', NULL),
(32, 'Bermuda', NULL, '', NULL),
(33, 'Bhutan', NULL, '', NULL),
(34, 'Bolivia', NULL, '', NULL),
(35, 'Bosnia and Herzegovina', NULL, '', NULL),
(36, 'Botswana', NULL, '', NULL),
(37, 'Bouvet Island', NULL, '', NULL),
(38, 'Brazil', NULL, '', NULL),
(39, 'British lndian Ocean Territory', NULL, '', NULL),
(40, 'Brunei Darussalam', NULL, '', NULL),
(41, 'Bulgaria', NULL, '', NULL),
(42, 'Burkina Faso', NULL, '', NULL),
(43, 'Burundi', NULL, '', NULL),
(44, 'Cambodia', NULL, '', NULL),
(45, 'Cameroon', NULL, '', NULL),
(46, 'Cape Verde', NULL, '', NULL),
(47, 'Cayman Islands', NULL, '', NULL),
(48, 'Central African Republic', NULL, '', NULL),
(49, 'Chad', NULL, '', NULL),
(50, 'Chile', NULL, '', NULL),
(51, 'China', NULL, '', NULL),
(52, 'Christmas Island', NULL, '', NULL),
(53, 'Cocos (Keeling) Islands', NULL, '', NULL),
(54, 'Colombia', NULL, '', NULL),
(55, 'Comoros', NULL, '', NULL),
(56, 'Congo', NULL, '', NULL),
(57, 'Cook Islands', NULL, '', NULL),
(58, 'Costa Rica', NULL, '', NULL),
(59, 'Croatia (Hrvatska)', NULL, '', NULL),
(60, 'Cuba', NULL, '', NULL),
(61, 'Cyprus', NULL, '', NULL),
(62, 'Czech Republic', NULL, '', NULL),
(63, 'Denmark', NULL, '', NULL),
(64, 'Djibouti', NULL, '', NULL),
(65, 'Dominica', NULL, '', NULL),
(66, 'Dominican Republic', NULL, '', NULL),
(67, 'East Timor', NULL, '', NULL),
(68, 'Ecudaor', NULL, '', NULL),
(69, 'Egypt', NULL, '', NULL),
(70, 'El Salvador', NULL, '', NULL),
(71, 'Equatorial Guinea', NULL, '', NULL),
(72, 'Eritrea', NULL, '', NULL),
(73, 'Estonia', NULL, '', NULL),
(74, 'Ethiopia', NULL, '', NULL),
(75, 'Falkland Islands (Malvinas)', NULL, '', NULL),
(76, 'Faroe Islands', NULL, '', NULL),
(77, 'Fiji', NULL, '', NULL),
(78, 'Finland', NULL, '', NULL),
(79, 'France', NULL, '', NULL),
(80, 'France, Metropolitan', NULL, '', NULL),
(81, 'French Guiana', NULL, '', NULL),
(82, 'French Polynesia', NULL, '', NULL),
(83, 'French Southern Territories', NULL, '', NULL),
(84, 'Gabon', NULL, '', NULL),
(85, 'Gambia', NULL, '', NULL),
(86, 'Georgia', NULL, '', NULL),
(87, 'Germany', NULL, '', NULL),
(88, 'Ghana', NULL, '', NULL),
(89, 'Gibraltar', NULL, '', NULL),
(90, 'Greece', NULL, '', NULL),
(91, 'Greenland', NULL, '', NULL),
(92, 'Grenada', NULL, '', NULL),
(93, 'Guadeloupe', NULL, '', NULL),
(94, 'Guam', NULL, '', NULL),
(95, 'Guatemala', NULL, '', NULL),
(96, 'Guinea', NULL, '', NULL),
(97, 'Guinea-Bissau', NULL, '', NULL),
(98, 'Guyana', NULL, '', NULL),
(99, 'Haiti', NULL, '', NULL),
(100, 'Heard and Mc Donald Islands', NULL, '', NULL),
(101, 'Honduras', NULL, '', NULL),
(102, 'Hong Kong', NULL, '', NULL),
(103, 'Hungary', NULL, '', NULL),
(104, 'Iceland', NULL, '', NULL),
(105, 'Indonesia', NULL, '', NULL),
(106, 'Iran (Islamic Republic of)', NULL, '', NULL),
(107, 'Iraq', NULL, '', NULL),
(108, 'Ireland', NULL, '', NULL),
(109, 'Israel', NULL, '', NULL),
(110, 'Italy', NULL, '', NULL),
(111, 'Ivory Coast', NULL, '', NULL),
(112, 'Jamaica', NULL, '', NULL),
(113, 'Japan', NULL, '', NULL),
(114, 'Jordan', NULL, '', NULL),
(115, 'Kazakhstan', NULL, '', NULL),
(116, 'Kenya', NULL, '', NULL),
(117, 'Kiribati', NULL, '', NULL),
(118, 'Korea, Democratic People''s Republic of', NULL, '', NULL),
(119, 'Korea, Republic of', NULL, '', NULL),
(120, 'Kuwait', NULL, '', NULL),
(121, 'Kyrgyzstan', NULL, '', NULL),
(122, 'Lao People''s Democratic Republic', NULL, '', NULL),
(123, 'Latvia', NULL, '', NULL),
(124, 'Lebanon', NULL, '', NULL),
(125, 'Lesotho', NULL, '', NULL),
(126, 'Liberia', NULL, '', NULL),
(127, 'Libyan Arab Jamahiriya', NULL, '', NULL),
(128, 'Liechtenstein', NULL, '', NULL),
(129, 'Lithuania', NULL, '', NULL),
(130, 'Luxembourg', NULL, '', NULL),
(131, 'Macau', NULL, '', NULL),
(132, 'Macedonia', NULL, '', NULL),
(133, 'Madagascar', NULL, '', NULL),
(134, 'Malawi', NULL, '', NULL),
(135, 'Maldives', NULL, '', NULL),
(136, 'Mali', NULL, '', NULL),
(137, 'Malta', NULL, '', NULL),
(138, 'Marshall Islands', NULL, '', NULL),
(139, 'Martinique', NULL, '', NULL),
(140, 'Mauritania', NULL, '', NULL),
(141, 'Mauritius', NULL, '', NULL),
(142, 'Mayotte', NULL, '', NULL),
(143, 'Mexico', NULL, '', NULL),
(144, 'Micronesia, Federated States of', NULL, '', NULL),
(145, 'Moldova, Republic of', NULL, '', NULL),
(146, 'Monaco', NULL, '', NULL),
(147, 'Mongolia', NULL, '', NULL),
(148, 'Montserrat', NULL, '', NULL),
(149, 'Morocco', NULL, '', NULL),
(150, 'Mozambique', NULL, '', NULL),
(151, 'Myanmar', NULL, '', NULL),
(152, 'Namibia', NULL, '', NULL),
(153, 'Nauru', NULL, '', NULL),
(154, 'Nepal', NULL, '', NULL),
(155, 'Netherlands', NULL, '', NULL),
(156, 'Netherlands Antilles', NULL, '', NULL),
(157, 'New Caledonia', NULL, '', NULL),
(158, 'New Zealand', NULL, '', NULL),
(159, 'Nicaragua', NULL, '', NULL),
(160, 'Niger', NULL, '', NULL),
(161, 'Nigeria', NULL, '', NULL),
(162, 'Niue', NULL, '', NULL),
(163, 'Norfork Island', NULL, '', NULL),
(164, 'Northern Mariana Islands', NULL, '', NULL),
(165, 'Norway', NULL, '', NULL),
(166, 'Oman', NULL, '', NULL),
(167, 'Pakistan', NULL, '', NULL),
(168, 'Palau', NULL, '', NULL),
(169, 'Panama', NULL, '', NULL),
(170, 'Papua New Guinea', NULL, '', NULL),
(171, 'Paraguay', NULL, '', NULL),
(172, 'Peru', NULL, '', NULL),
(173, 'Philippines', NULL, '', NULL),
(174, 'Pitcairn', NULL, '', NULL),
(175, 'Poland', NULL, '', NULL),
(176, 'Portugal', NULL, '', NULL),
(177, 'Puerto Rico', NULL, '', NULL),
(178, 'Qatar', NULL, '', NULL),
(179, 'Reunion', NULL, '', NULL),
(180, 'Romania', NULL, '', NULL),
(181, 'Russian Federation', NULL, '', NULL),
(182, 'Rwanda', NULL, '', NULL),
(183, 'Saint Kitts and Nevis', NULL, '', NULL),
(184, 'Saint Lucia', NULL, '', NULL),
(185, 'Saint Vincent and the Grenadines', NULL, '', NULL),
(186, 'Samoa', NULL, '', NULL),
(187, 'San Marino', NULL, '', NULL),
(188, 'Sao Tome and Principe', NULL, '', NULL),
(189, 'Saudi Arabia', NULL, '', NULL),
(190, 'Senegal', NULL, '', NULL),
(191, 'Seychelles', NULL, '', NULL),
(192, 'Sierra Leone', NULL, '', NULL),
(193, 'Slovakia', NULL, '', NULL),
(194, 'Slovenia', NULL, '', NULL),
(195, 'Solomon Islands', NULL, '', NULL),
(196, 'Somalia', NULL, '', NULL),
(197, 'South Africa', NULL, '', NULL),
(198, 'South Georgia South Sandwich Islands', NULL, '', NULL),
(199, 'Spain', NULL, '', NULL),
(200, 'Sri Lanka', NULL, '', NULL),
(201, 'St. Helena', NULL, '', NULL),
(202, 'St. Pierre and Miquelon', NULL, '', NULL),
(203, 'Sudan', NULL, '', NULL),
(204, 'Suriname', NULL, '', NULL),
(205, 'Svalbarn and Jan Mayen Islands', NULL, '', NULL),
(206, 'Swaziland', NULL, '', NULL),
(207, 'Switzerland', NULL, '', NULL),
(208, 'Syrian Arab Republic', NULL, '', NULL),
(209, 'Taiwan', NULL, '', NULL),
(210, 'Tajikistan', NULL, '', NULL),
(211, 'Tanzania, United Republic of', NULL, '', NULL),
(212, 'Togo', NULL, '', NULL),
(213, 'Tokelau', NULL, '', NULL),
(214, 'Tonga', NULL, '', NULL),
(215, 'Trinidad and Tobago', NULL, '', NULL),
(216, 'Tunisia', NULL, '', NULL),
(217, 'Turkey', NULL, '', NULL),
(218, 'Turkmenistan', NULL, '', NULL),
(219, 'Turks and Caicos Islands', NULL, '', NULL),
(220, 'Tuvalu', NULL, '', NULL),
(221, 'Uganda', NULL, '', NULL),
(222, 'Ukraine', NULL, '', NULL),
(223, 'United Arab Emirates', NULL, '', NULL),
(224, 'United States minor outlying islands', NULL, '', NULL),
(225, 'Uruguay', NULL, '', NULL),
(226, 'Uzbekistan', NULL, '', NULL),
(227, 'Vanuatu', NULL, '', NULL),
(228, 'Vatican City State', NULL, '', NULL),
(229, 'Venezuela', NULL, '', NULL),
(230, 'Vietnam', NULL, '', NULL),
(231, 'Virigan Islands (British)', NULL, '', NULL),
(232, 'Virgin Islands (U.S.)', NULL, '', NULL),
(233, 'Wallis and Futuna Islands', NULL, '', NULL),
(234, 'Western Sahara', NULL, '', NULL),
(235, 'Yemen', NULL, '', NULL),
(236, 'Yugoslavia', NULL, '', NULL),
(237, 'Zaire', NULL, '', NULL),
(238, 'Zambia', NULL, '', NULL),
(239, 'Zimbabwe', NULL, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `dob` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `is_social` varchar(255) DEFAULT NULL,
  `social_id` varchar(255) DEFAULT NULL,
  `social_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `first_name`, `last_name`, `gender`, `dob`, `email`, `password`, `is_social`, `social_id`, `social_type`) VALUES
(1, 'Khairul', 'Islam', 'male', NULL, 'arkhairulislam@gmail.com', NULL, 'yes', '105378951389348740221', 'google'),
(2, 'Md', 'Islam', 'male', NULL, 'arkhairulislam@gmail.com', NULL, 'yes', '390891871088813', 'facebook'),
(3, 'Test Sujan ssds', 'Name', 'female', '1970/01/01', 'test@gmail.com', 'test', NULL, NULL, NULL),
(4, 'retete', 'tefdfdf', 'male', '1/15/2015', 'af@fgmad.com', 'dfdfd', NULL, NULL, NULL),
(5, 'Jakia', 'Tamanna', 'female', NULL, 'blueroseislam13@gmail.com', NULL, 'yes', '340326022821014', 'facebook');

-- --------------------------------------------------------

--
-- Table structure for table `dates`
--

CREATE TABLE IF NOT EXISTS `dates` (
  `dates_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `day_type_id` int(11) DEFAULT NULL,
  `holiday_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`dates_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=366 ;

--
-- Dumping data for table `dates`
--

INSERT INTO `dates` (`dates_id`, `date`, `day_type_id`, `holiday_id`) VALUES
(1, '2014-01-01', 1, NULL),
(2, '2014-01-02', 1, NULL),
(3, '2014-01-03', 2, NULL),
(4, '2014-01-04', 2, NULL),
(5, '2014-01-05', 1, NULL),
(6, '2014-01-06', 1, NULL),
(7, '2014-01-07', 1, NULL),
(8, '2014-01-08', 1, NULL),
(9, '2014-01-09', 3, 3),
(10, '2014-01-10', 2, NULL),
(11, '2014-01-11', 2, NULL),
(12, '2014-01-12', 1, NULL),
(13, '2014-01-13', 1, NULL),
(14, '2014-01-14', 1, NULL),
(15, '2014-01-15', 1, NULL),
(16, '2014-01-16', 1, NULL),
(17, '2014-01-17', 2, NULL),
(18, '2014-01-18', 2, NULL),
(19, '2014-01-19', 1, NULL),
(20, '2014-01-20', 1, NULL),
(21, '2014-01-21', 1, NULL),
(22, '2014-01-22', 4, 4),
(23, '2014-01-23', 1, NULL),
(24, '2014-01-24', 2, NULL),
(25, '2014-01-25', 2, NULL),
(26, '2014-01-26', 1, NULL),
(27, '2014-01-27', 1, NULL),
(28, '2014-01-28', 1, NULL),
(29, '2014-01-29', 1, NULL),
(30, '2014-01-30', 1, NULL),
(31, '2014-01-31', 2, NULL),
(32, '2014-02-01', 2, 0),
(33, '2014-02-02', 1, NULL),
(34, '2014-02-03', 1, NULL),
(35, '2014-02-04', 1, NULL),
(36, '2014-02-05', 1, NULL),
(37, '2014-02-06', 1, NULL),
(38, '2014-02-07', 2, NULL),
(39, '2014-02-08', 2, NULL),
(40, '2014-02-09', 1, NULL),
(41, '2014-02-10', 1, NULL),
(42, '2014-02-11', 1, NULL),
(43, '2014-02-12', 1, NULL),
(44, '2014-02-13', 1, NULL),
(45, '2014-02-14', 2, NULL),
(46, '2014-02-15', 2, NULL),
(47, '2014-02-16', 1, NULL),
(48, '2014-02-17', 1, NULL),
(49, '2014-02-18', 1, NULL),
(50, '2014-02-19', 1, NULL),
(51, '2014-02-20', 1, NULL),
(52, '2014-02-21', 2, NULL),
(53, '2014-02-22', 2, NULL),
(54, '2014-02-23', 1, NULL),
(55, '2014-02-24', 1, NULL),
(56, '2014-02-25', 1, NULL),
(57, '2014-02-26', 1, NULL),
(58, '2014-02-27', 1, NULL),
(59, '2014-02-28', 2, NULL),
(60, '2014-03-01', 3, 1),
(61, '2014-03-02', 1, 0),
(62, '2014-03-03', 3, 3),
(63, '2014-03-04', 1, NULL),
(64, '2014-03-05', 1, NULL),
(65, '2014-03-06', 1, NULL),
(66, '2014-03-07', 2, NULL),
(67, '2014-03-08', 2, NULL),
(68, '2014-03-09', 1, NULL),
(69, '2014-03-10', 1, NULL),
(70, '2014-03-11', 1, NULL),
(71, '2014-03-12', 1, NULL),
(72, '2014-03-13', 1, NULL),
(73, '2014-03-14', 2, NULL),
(74, '2014-03-15', 2, NULL),
(75, '2014-03-16', 1, NULL),
(76, '2014-03-17', 1, NULL),
(77, '2014-03-18', 1, NULL),
(78, '2014-03-19', 1, NULL),
(79, '2014-03-20', 1, NULL),
(80, '2014-03-21', 2, NULL),
(81, '2014-03-22', 2, NULL),
(82, '2014-03-23', 1, NULL),
(83, '2014-03-24', 1, NULL),
(84, '2014-03-25', 1, NULL),
(85, '2014-03-26', 1, NULL),
(86, '2014-03-27', 1, NULL),
(87, '2014-03-28', 2, NULL),
(88, '2014-03-29', 2, NULL),
(89, '2014-03-30', 1, NULL),
(90, '2014-03-31', 1, NULL),
(91, '2014-04-01', 1, NULL),
(92, '2014-04-02', 3, 3),
(93, '2014-04-03', 1, NULL),
(94, '2014-04-04', 3, 1),
(95, '2014-04-05', 2, 0),
(96, '2014-04-06', 1, NULL),
(97, '2014-04-07', 2, 2),
(98, '2014-04-08', 1, NULL),
(99, '2014-04-09', 3, 3),
(100, '2014-04-10', 3, 4),
(101, '2014-04-11', 2, NULL),
(102, '2014-04-12', 2, NULL),
(103, '2014-04-13', 3, 0),
(104, '2014-04-14', 1, NULL),
(105, '2014-04-15', 1, NULL),
(106, '2014-04-16', 1, NULL),
(107, '2014-04-17', 1, NULL),
(108, '2014-04-18', 2, NULL),
(109, '2014-04-19', 2, NULL),
(110, '2014-04-20', 1, NULL),
(111, '2014-04-21', 1, NULL),
(112, '2014-04-22', 1, NULL),
(113, '2014-04-23', 1, NULL),
(114, '2014-04-24', 1, NULL),
(115, '2014-04-25', 2, NULL),
(116, '2014-04-26', 2, NULL),
(117, '2014-04-27', 1, NULL),
(118, '2014-04-28', 1, NULL),
(119, '2014-04-29', 1, NULL),
(120, '2014-04-30', 1, NULL),
(121, '2014-05-01', 1, 0),
(122, '2014-05-02', 2, NULL),
(123, '2014-05-03', 2, NULL),
(124, '2014-05-04', 1, NULL),
(125, '2014-05-05', 1, NULL),
(126, '2014-05-06', 1, NULL),
(127, '2014-05-07', 1, NULL),
(128, '2014-05-08', 1, NULL),
(129, '2014-05-09', 2, NULL),
(130, '2014-05-10', 3, 3),
(131, '2014-05-11', 1, NULL),
(132, '2014-05-12', 1, NULL),
(133, '2014-05-13', 1, NULL),
(134, '2014-05-14', 1, NULL),
(135, '2014-05-15', 1, NULL),
(136, '2014-05-16', 2, NULL),
(137, '2014-05-17', 2, NULL),
(138, '2014-05-18', 1, NULL),
(139, '2014-05-19', 1, NULL),
(140, '2014-05-20', 1, NULL),
(141, '2014-05-21', 1, NULL),
(142, '2014-05-22', 1, NULL),
(143, '2014-05-23', 2, NULL),
(144, '2014-05-24', 2, NULL),
(145, '2014-05-25', 1, NULL),
(146, '2014-05-26', 1, NULL),
(147, '2014-05-27', 1, NULL),
(148, '2014-05-28', 1, NULL),
(149, '2014-05-29', 1, NULL),
(150, '2014-05-30', 2, NULL),
(151, '2014-05-31', 2, NULL),
(152, '2014-06-01', 1, NULL),
(153, '2014-06-02', 1, NULL),
(154, '2014-06-03', 1, NULL),
(155, '2014-06-04', 1, NULL),
(156, '2014-06-05', 1, NULL),
(157, '2014-06-06', 2, NULL),
(158, '2014-06-07', 2, NULL),
(159, '2014-06-08', 1, NULL),
(160, '2014-06-09', 1, NULL),
(161, '2014-06-10', 1, NULL),
(162, '2014-06-11', 1, NULL),
(163, '2014-06-12', 1, NULL),
(164, '2014-06-13', 2, NULL),
(165, '2014-06-14', 2, NULL),
(166, '2014-06-15', 1, NULL),
(167, '2014-06-16', 1, NULL),
(168, '2014-06-17', 1, NULL),
(169, '2014-06-18', 1, NULL),
(170, '2014-06-19', 1, NULL),
(171, '2014-06-20', 2, NULL),
(172, '2014-06-21', 2, NULL),
(173, '2014-06-22', 1, NULL),
(174, '2014-06-23', 3, 4),
(175, '2014-06-24', 1, NULL),
(176, '2014-06-25', 1, NULL),
(177, '2014-06-26', 1, NULL),
(178, '2014-06-27', 2, NULL),
(179, '2014-06-28', 2, NULL),
(180, '2014-06-29', 1, NULL),
(181, '2014-06-30', 1, NULL),
(182, '2014-07-01', 1, NULL),
(183, '2014-07-02', 1, NULL),
(184, '2014-07-03', 1, 4),
(185, '2014-07-04', 2, NULL),
(186, '2014-07-05', 2, NULL),
(187, '2014-07-06', 1, NULL),
(188, '2014-07-07', 3, 3),
(189, '2014-07-08', 1, NULL),
(190, '2014-07-09', 1, NULL),
(191, '2014-07-10', 1, NULL),
(192, '2014-07-11', 2, NULL),
(193, '2014-07-12', 2, NULL),
(194, '2014-07-13', 1, NULL),
(195, '2014-07-14', 1, NULL),
(196, '2014-07-15', 1, NULL),
(197, '2014-07-16', 1, NULL),
(198, '2014-07-17', 1, NULL),
(199, '2014-07-18', 2, NULL),
(200, '2014-07-19', 2, NULL),
(201, '2014-07-20', 1, NULL),
(202, '2014-07-21', 1, NULL),
(203, '2014-07-22', 1, NULL),
(204, '2014-07-23', 1, NULL),
(205, '2014-07-24', 1, NULL),
(206, '2014-07-25', 2, NULL),
(207, '2014-07-26', 2, NULL),
(208, '2014-07-27', 1, NULL),
(209, '2014-07-28', 1, NULL),
(210, '2014-07-29', 1, NULL),
(211, '2014-07-30', 1, NULL),
(212, '2014-07-31', 1, NULL),
(213, '2014-08-01', 2, NULL),
(214, '2014-08-02', 2, NULL),
(215, '2014-08-03', 1, NULL),
(216, '2014-08-04', 1, NULL),
(217, '2014-08-05', 1, NULL),
(218, '2014-08-06', 1, NULL),
(219, '2014-08-07', 1, NULL),
(220, '2014-08-08', 2, NULL),
(221, '2014-08-09', 2, NULL),
(222, '2014-08-10', 1, NULL),
(223, '2014-08-11', 1, NULL),
(224, '2014-08-12', 1, NULL),
(225, '2014-08-13', 1, NULL),
(226, '2014-08-14', 1, NULL),
(227, '2014-08-15', 2, NULL),
(228, '2014-08-16', 2, NULL),
(229, '2014-08-17', 1, NULL),
(230, '2014-08-18', 1, NULL),
(231, '2014-08-19', 1, NULL),
(232, '2014-08-20', 1, NULL),
(233, '2014-08-21', 1, NULL),
(234, '2014-08-22', 2, NULL),
(235, '2014-08-23', 2, NULL),
(236, '2014-08-24', 1, NULL),
(237, '2014-08-25', 1, NULL),
(238, '2014-08-26', 1, NULL),
(239, '2014-08-27', 1, NULL),
(240, '2014-08-28', 1, NULL),
(241, '2014-08-29', 2, NULL),
(242, '2014-08-30', 2, NULL),
(243, '2014-08-31', 1, NULL),
(244, '2014-09-01', 1, NULL),
(245, '2014-09-02', 1, NULL),
(246, '2014-09-03', 1, NULL),
(247, '2014-09-04', 1, NULL),
(248, '2014-09-05', 2, NULL),
(249, '2014-09-06', 2, NULL),
(250, '2014-09-07', 1, NULL),
(251, '2014-09-08', 1, NULL),
(252, '2014-09-09', 1, NULL),
(253, '2014-09-10', 1, NULL),
(254, '2014-09-11', 1, NULL),
(255, '2014-09-12', 2, NULL),
(256, '2014-09-13', 2, NULL),
(257, '2014-09-14', 1, NULL),
(258, '2014-09-15', 1, NULL),
(259, '2014-09-16', 1, NULL),
(260, '2014-09-17', 1, NULL),
(261, '2014-09-18', 1, NULL),
(262, '2014-09-19', 2, NULL),
(263, '2014-09-20', 2, NULL),
(264, '2014-09-21', 1, 0),
(265, '2014-09-22', 1, NULL),
(266, '2014-09-23', 1, NULL),
(267, '2014-09-24', 1, NULL),
(268, '2014-09-25', 1, NULL),
(269, '2014-09-26', 2, NULL),
(270, '2014-09-27', 2, NULL),
(271, '2014-09-28', 1, NULL),
(272, '2014-09-29', 1, NULL),
(273, '2014-09-30', 1, NULL),
(274, '2014-10-01', 1, NULL),
(275, '2014-10-02', 1, NULL),
(276, '2014-10-03', 2, NULL),
(277, '2014-10-04', 2, NULL),
(278, '2014-10-05', 1, NULL),
(279, '2014-10-06', 1, NULL),
(280, '2014-10-07', 1, NULL),
(281, '2014-10-08', 1, NULL),
(282, '2014-10-09', 1, NULL),
(283, '2014-10-10', 2, NULL),
(284, '2014-10-11', 2, NULL),
(285, '2014-10-12', 1, NULL),
(286, '2014-10-13', 1, NULL),
(287, '2014-10-14', 1, NULL),
(288, '2014-10-15', 1, NULL),
(289, '2014-10-16', 1, NULL),
(290, '2014-10-17', 2, NULL),
(291, '2014-10-18', 2, NULL),
(292, '2014-10-19', 1, NULL),
(293, '2014-10-20', 1, NULL),
(294, '2014-10-21', 1, NULL),
(295, '2014-10-22', 1, NULL),
(296, '2014-10-23', 1, NULL),
(297, '2014-10-24', 2, NULL),
(298, '2014-10-25', 2, NULL),
(299, '2014-10-26', 1, NULL),
(300, '2014-10-27', 1, NULL),
(301, '2014-10-28', 1, NULL),
(302, '2014-10-29', 1, NULL),
(303, '2014-10-30', 1, NULL),
(304, '2014-10-31', 2, NULL),
(305, '2014-11-01', 2, NULL),
(306, '2014-11-02', 1, NULL),
(307, '2014-11-03', 1, NULL),
(308, '2014-11-04', 1, NULL),
(309, '2014-11-05', 1, NULL),
(310, '2014-11-06', 1, NULL),
(311, '2014-11-07', 2, NULL),
(312, '2014-11-08', 2, NULL),
(313, '2014-11-09', 1, NULL),
(314, '2014-11-10', 1, NULL),
(315, '2014-11-11', 1, NULL),
(316, '2014-11-12', 1, NULL),
(317, '2014-11-13', 1, NULL),
(318, '2014-11-14', 2, NULL),
(319, '2014-11-15', 2, NULL),
(320, '2014-11-16', 1, NULL),
(321, '2014-11-17', 1, NULL),
(322, '2014-11-18', 1, NULL),
(323, '2014-11-19', 1, NULL),
(324, '2014-11-20', 1, NULL),
(325, '2014-11-21', 2, NULL),
(326, '2014-11-22', 2, NULL),
(327, '2014-11-23', 1, NULL),
(328, '2014-11-24', 1, NULL),
(329, '2014-11-25', 1, NULL),
(330, '2014-11-26', 1, NULL),
(331, '2014-11-27', 1, NULL),
(332, '2014-11-28', 2, NULL),
(333, '2014-11-29', 2, NULL),
(334, '2014-11-30', 1, NULL),
(335, '2014-12-01', 1, NULL),
(336, '2014-12-02', 1, NULL),
(337, '2014-12-03', 1, NULL),
(338, '2014-12-04', 1, NULL),
(339, '2014-12-05', 2, NULL),
(340, '2014-12-06', 2, NULL),
(341, '2014-12-07', 1, NULL),
(342, '2014-12-08', 1, NULL),
(343, '2014-12-09', 1, NULL),
(344, '2014-12-10', 1, NULL),
(345, '2014-12-11', 1, NULL),
(346, '2014-12-12', 2, NULL),
(347, '2014-12-13', 2, NULL),
(348, '2014-12-14', 1, NULL),
(349, '2014-12-15', 1, NULL),
(350, '2014-12-16', 1, NULL),
(351, '2014-12-17', 1, NULL),
(352, '2014-12-18', 1, NULL),
(353, '2014-12-19', 2, NULL),
(354, '2014-12-20', 2, NULL),
(355, '2014-12-21', 1, NULL),
(356, '2014-12-22', 1, NULL),
(357, '2014-12-23', 1, NULL),
(358, '2014-12-24', 1, NULL),
(359, '2014-12-25', 1, NULL),
(360, '2014-12-26', 2, NULL),
(361, '2014-12-27', 2, NULL),
(362, '2014-12-28', 1, NULL),
(363, '2014-12-29', 1, NULL),
(364, '2014-12-30', 1, NULL),
(365, '2014-12-31', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_cost`
--

CREATE TABLE IF NOT EXISTS `delivery_cost` (
  `delivery_cost_id` int(11) NOT NULL AUTO_INCREMENT,
  `dis_state_id` int(11) DEFAULT NULL,
  `cost` varchar(45) DEFAULT NULL,
  `is_active` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`delivery_cost_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `delivery_cost`
--

INSERT INTO `delivery_cost` (`delivery_cost_id`, `dis_state_id`, `cost`, `is_active`) VALUES
(2, 2, '70', NULL),
(3, 3, '70', NULL),
(4, 4, '70', NULL),
(5, 5, '70', NULL),
(6, 6, '70', NULL),
(7, 7, '70', NULL),
(8, 8, '70', NULL),
(9, 9, '70', NULL),
(10, 10, '70', NULL),
(11, 11, '70', NULL),
(12, 12, '70', NULL),
(13, 13, '70', NULL),
(14, 14, '50', NULL),
(15, 15, '70', NULL),
(16, 16, '70', NULL),
(17, 17, '70', NULL),
(18, 18, '70', NULL),
(19, 19, '70', NULL),
(20, 20, '70', NULL),
(21, 21, '70', NULL),
(22, 22, '70', NULL),
(23, 23, '70', NULL),
(24, 24, '70', NULL),
(25, 25, '70', NULL),
(26, 26, '70', NULL),
(27, 27, '70', NULL),
(28, 28, '70', NULL),
(29, 29, '70', NULL),
(30, 30, '70', NULL),
(31, 31, '70', NULL),
(32, 32, '70', NULL),
(33, 33, '70', NULL),
(34, 34, '70', NULL),
(35, 35, '70', NULL),
(36, 36, '70', NULL),
(37, 37, '70', NULL),
(38, 38, '70', NULL),
(39, 39, '70', NULL),
(40, 40, '70', NULL),
(41, 41, '70', NULL),
(42, 42, '70', NULL),
(43, 43, '70', NULL),
(44, 44, '70', NULL),
(45, 45, '70', NULL),
(46, 46, '70', NULL),
(47, 47, '70', NULL),
(48, 48, '70', NULL),
(49, 49, '70', NULL),
(50, 50, '70', NULL),
(51, 51, '70', NULL),
(52, 52, '70', NULL),
(53, 53, '70', NULL),
(54, 54, '70', NULL),
(55, 55, '70', NULL),
(56, 56, '70', NULL),
(57, 57, '70', NULL),
(58, 58, '70', NULL),
(59, 59, '70', NULL),
(60, 60, '70', NULL),
(61, 61, '70', NULL),
(62, 62, '70', NULL),
(63, 63, '70', NULL),
(64, 64, '70', NULL),
(65, 65, '70', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `discount_type`
--

CREATE TABLE IF NOT EXISTS `discount_type` (
  `discount_type_id` int(11) NOT NULL,
  `discount_type_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`discount_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `district_state`
--

CREATE TABLE IF NOT EXISTS `district_state` (
  `dis_state_id` int(11) NOT NULL AUTO_INCREMENT,
  `dis_state_title` varchar(255) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `is_active` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`dis_state_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=64 ;

--
-- Dumping data for table `district_state`
--

INSERT INTO `district_state` (`dis_state_id`, `dis_state_title`, `city_id`, `country_id`, `is_active`) VALUES
(10, 'Chittagong', NULL, NULL, NULL),
(12, 'Comilla', NULL, NULL, NULL),
(14, 'Dhaka', NULL, NULL, NULL),
(19, 'Gazipur', NULL, NULL, NULL),
(28, 'Khulna', NULL, NULL, NULL),
(42, 'Narayanganj', NULL, NULL, NULL),
(55, 'Rajshahi', NULL, NULL, NULL),
(63, 'Sylhet', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `donation`
--

CREATE TABLE IF NOT EXISTS `donation` (
  `donation_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `program_id` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `college_validation_id` int(11) DEFAULT NULL,
  `is_donate` varchar(45) DEFAULT NULL,
  `token_no` varchar(255) NOT NULL,
  `event_id` int(11) DEFAULT NULL,
  `schedule_id` int(11) DEFAULT NULL,
  `delivery_method` varchar(255) DEFAULT NULL,
  `is_online` varchar(255) DEFAULT NULL,
  `customer_address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `discrict_id` int(11) DEFAULT NULL,
  `zip_code` varchar(45) DEFAULT NULL,
  `val_id` varchar(255) DEFAULT NULL,
  `payment_status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`donation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `donation`
--

INSERT INTO `donation` (`donation_id`, `customer_id`, `name`, `batch_id`, `program_id`, `amount`, `college_validation_id`, `is_donate`, `token_no`, `event_id`, `schedule_id`, `delivery_method`, `is_online`, `customer_address`, `city`, `discrict_id`, `zip_code`, `val_id`, `payment_status`) VALUES
(1, 0, 'sujan', 3, 2, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 0, 'sujan', 2, 2, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 3, 'sujan', 8, 2, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 5, 'jakia', 3, 1, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 0, '', 0, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 0, '', 0, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 0, '', 0, 2, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 0, 'dassa', 0, 2, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 0, 'h', 2, 5, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 0, 'h', 2, 5, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 0, '', 0, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 0, 'sujan', 2, 3, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 0, 'sujanfg', 2, 3, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 0, 'qwqw', 3, 2, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 0, 'asa', 3, 2, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 0, 'asaaaaaaaaaaaaa', 3, 3, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 0, 'sas', 3, 2, 1222, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 3, 'asa', 1, 4, 500, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 0, 'ooo', 5, 2, 500, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `event_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_title` varchar(255) DEFAULT NULL,
  `event_logo_image` varchar(255) DEFAULT NULL,
  `event_full_size_image` varchar(255) DEFAULT NULL,
  `city_id` int(10) unsigned DEFAULT NULL,
  `featured` varchar(100) DEFAULT NULL,
  `f_priority` varchar(45) DEFAULT NULL,
  `event_ticket_expair_date` varchar(45) DEFAULT NULL,
  `upcomming` varchar(11) DEFAULT NULL,
  `u_priority` varchar(45) DEFAULT NULL,
  `event_created_by` varchar(25) DEFAULT NULL,
  `event_created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` int(11) DEFAULT NULL,
  `is_private` enum('yes','no') NOT NULL,
  `is_online_payable` enum('yes','no') NOT NULL,
  `is_home_delivery` enum('yes','no') NOT NULL,
  `payment_page_style` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`event_id`, `event_title`, `event_logo_image`, `event_full_size_image`, `city_id`, `featured`, `f_priority`, `event_ticket_expair_date`, `upcomming`, `u_priority`, `event_created_by`, `event_created_date`, `is_active`, `is_private`, `is_online_payable`, `is_home_delivery`, `payment_page_style`) VALUES
(1, ' Enchanted Valley Carnival (EVC) - 2014', 'p1.jpg', 'p1.jpg', 1, 'true', '1', '1/30/2015', 'false', NULL, '1', '2015-01-14 04:31:56', 1, 'no', 'yes', 'yes', NULL),
(2, 'DISCLAIMERS AND LIMITATION OF LIABILITY', 'p2.jpg', 'p7.jpg', 2, 'false', NULL, '1/30/2015', 'false', NULL, '1', '2015-01-14 10:43:58', 1, 'no', 'yes', 'yes', NULL),
(3, 'fgfgfg', 'a.jpg', 'c.jpg', 1, 'false', NULL, '1/30/2015', 'false', NULL, '1', '2015-01-14 11:37:53', 1, 'no', 'yes', 'yes', NULL),
(4, 'sas', 'e.jpg', 'p2.jpg', 1, 'true', '2', '1/30/2015', 'false', NULL, '1', '2015-01-14 12:59:07', 1, 'no', 'yes', 'yes', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `event_activity`
--

CREATE TABLE IF NOT EXISTS `event_activity` (
  `event_activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `event_activity_title` varchar(255) DEFAULT NULL,
  `event_activity_details` text,
  `image` varchar(255) DEFAULT NULL,
  `is_active` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`event_activity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `event_activity`
--

INSERT INTO `event_activity` (`event_activity_id`, `event_id`, `activity_id`, `event_activity_title`, `event_activity_details`, `image`, `is_active`) VALUES
(1, 4, 1, 'fdfd', 'dfdf', 'p7.jpg', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `event_category`
--

CREATE TABLE IF NOT EXISTS `event_category` (
  `event_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `is_active` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`event_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `event_category`
--

INSERT INTO `event_category` (`event_category_id`, `event_id`, `category_id`, `is_active`) VALUES
(1, 1, 1, 'true'),
(2, 2, 2, 'true'),
(3, 2, 4, 'true'),
(4, 3, 2, 'true'),
(5, 4, 2, 'true');

-- --------------------------------------------------------

--
-- Table structure for table `event_details`
--

CREATE TABLE IF NOT EXISTS `event_details` (
  `event_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `event_details_description` text,
  PRIMARY KEY (`event_details_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `event_details`
--

INSERT INTO `event_details` (`event_details_id`, `event_id`, `event_details_description`) VALUES
(1, 1, '&amp;lt;p&amp;gt;With our first successful year behind us, we have taken a step forward \r\nto initiate this experience found on a global scale, now brought closer \r\nto home.&amp;lt;/p&amp;gt;&amp;lt;div style=&quot;overflow:hidden;color:#000000;background-color:#ffffff;text-align:left;text-decoration:none;border:medium none;&quot;&amp;gt;&amp;lt;br /&amp;gt;&amp;lt;br /&amp;gt;&amp;lt;/div&amp;gt;'),
(2, 2, '&amp;lt;h5&amp;gt;&amp;lt;strong&amp;gt;DISCLAIMERS AND LIMITATION OF LIABILITY&amp;lt;/strong&amp;gt;&amp;lt;/h5&amp;gt;'),
(3, 3, 'fgfg'),
(4, 4, 'sad');

-- --------------------------------------------------------

--
-- Table structure for table `event_faq`
--

CREATE TABLE IF NOT EXISTS `event_faq` (
  `faq_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `question` varchar(255) DEFAULT NULL,
  `answer` text,
  `is_active` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`faq_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `event_faq`
--

INSERT INTO `event_faq` (`faq_id`, `event_id`, `question`, `answer`, `is_active`) VALUES
(1, 1, 'Shall we get free drink?', 'No you have to pay lots of money for this', 'true'),
(2, 1, 'Amra sobai ki jete parbo?', 'tmra sobai jete parbe jodi ticket kino.', 'true'),
(3, 4, 'asasasasewewewew', 'asasasasasewew', 'true'),
(4, 4, 'qqqqqqqqqqqqqqqqqqqqqqqqqqqqqwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww', 'qwqwqwqwqw1212wsqwqw', 'true'),
(5, 4, 'qwa', 'asdcxz', 'true'),
(6, 4, 'qwa6', 'asaasas', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `event_feature`
--

CREATE TABLE IF NOT EXISTS `event_feature` (
  `event_feature_id` int(11) NOT NULL AUTO_INCREMENT,
  `feature_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `is_active` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`event_feature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `event_includes`
--

CREATE TABLE IF NOT EXISTS `event_includes` (
  `event_includes_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `event_schedule_id` int(11) DEFAULT NULL,
  `event_includes_name` varchar(100) DEFAULT NULL,
  `event_includes_price` double(10,2) DEFAULT NULL,
  `event_includes_per_user_limit` int(11) DEFAULT NULL,
  `total_qty` int(11) DEFAULT NULL,
  `is_active` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`event_includes_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `event_includes`
--

INSERT INTO `event_includes` (`event_includes_id`, `event_id`, `event_schedule_id`, `event_includes_name`, `event_includes_price`, `event_includes_per_user_limit`, `total_qty`, `is_active`) VALUES
(1, 1, 1, 'Drinks', 10.00, 5, 500, 'true'),
(2, 4, 3, 'dfd', 33.00, 33, 33, 'true');

-- --------------------------------------------------------

--
-- Table structure for table `event_key_attraction`
--

CREATE TABLE IF NOT EXISTS `event_key_attraction` (
  `event_key_attraction_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `attraction_name` varchar(255) DEFAULT NULL,
  `details` text,
  `image` varchar(245) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `google_plus` varchar(255) DEFAULT NULL,
  `is_active` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`event_key_attraction_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `event_key_attraction`
--

INSERT INTO `event_key_attraction` (`event_key_attraction_id`, `event_id`, `attraction_name`, `details`, `image`, `facebook`, `twitter`, `google_plus`, `is_active`) VALUES
(1, 1, 'KHAIRUL ISLAM', 'With our first successful year behind us, we have taken a step forward \nto initiate this experience found on a global scale, now brought closer \nto home.', 'a.jpg', 'http://www.facebook.com/systechunimax', 'http://www.facebook.com/systechunimax', 'http://www.facebook.com/systechunimax', 'true'),
(2, 1, 'WILLIAM GOMES', 'With our first successful year behind us, we have taken a step forward \nto initiate this experience found on a global scale, now brought closer \nto home.', 'c.jpg', 'http://www.facebook.com/william', 'http:www.facebook.com/william', 'http:www.facebook.com/william', 'true'),
(3, 4, 'dfdf', 'dfdf', 'p7.jpg', 'df', 'dfdf', 'dfdf', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `event_schedule`
--

CREATE TABLE IF NOT EXISTS `event_schedule` (
  `event_schedule_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_schedule_start_time` varchar(10) DEFAULT NULL,
  `event_schedule_end_time` varchar(10) DEFAULT NULL,
  `event_id` int(10) unsigned DEFAULT NULL,
  `venue_id` int(11) DEFAULT NULL,
  `is_active` varchar(45) DEFAULT NULL,
  `event_date` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`event_schedule_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `event_schedule`
--

INSERT INTO `event_schedule` (`event_schedule_id`, `event_schedule_start_time`, `event_schedule_end_time`, `event_id`, `venue_id`, `is_active`, `event_date`) VALUES
(1, '12:00 AM', '03:00 AM', 1, 1, 'true', '2015-01-30'),
(2, '01:00 AM', '01:30 AM', 3, 2, 'true', '2015-01-14'),
(3, '12:30 AM', '03:30 AM', 4, 1, 'true', '2015-01-21');

-- --------------------------------------------------------

--
-- Table structure for table `event_terms_and_conditions`
--

CREATE TABLE IF NOT EXISTS `event_terms_and_conditions` (
  `event_terms_and_conditions_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `event_terms_and_conditions_description` text,
  PRIMARY KEY (`event_terms_and_conditions_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `event_terms_and_conditions`
--

INSERT INTO `event_terms_and_conditions` (`event_terms_and_conditions_id`, `event_id`, `event_terms_and_conditions_description`) VALUES
(1, 1, '&amp;lt;p&amp;gt;With our first successful year behind us, we have taken a step forward \r\nto initiate this experience found on a global scale, now brought closer \r\nto home.&amp;lt;/p&amp;gt;&amp;lt;div style=&quot;overflow:hidden;color:#000000;background-color:#ffffff;text-align:left;text-decoration:none;border:medium none;&quot;&amp;gt;&amp;lt;br /&amp;gt;&amp;lt;br /&amp;gt;&amp;lt;br /&amp;gt;&amp;lt;br /&amp;gt;&amp;lt;/div&amp;gt;'),
(2, 2, '&amp;lt;h5&amp;gt;&amp;lt;strong&amp;gt;DISCLAIMERS AND LIMITATION OF LIABILITY&amp;lt;/strong&amp;gt;&amp;lt;h5&amp;gt;&amp;lt;strong&amp;gt;DISCLAIMERS AND LIMITATION OF LIABILITY&amp;lt;/strong&amp;gt;&amp;lt;/h5&amp;gt;&amp;lt;strong&amp;gt;&amp;lt;br /&amp;gt;&amp;lt;/strong&amp;gt;&amp;lt;/h5&amp;gt;'),
(3, 3, 'gfgfg'),
(4, 4, 'sds');

-- --------------------------------------------------------

--
-- Table structure for table `event_ticket_type`
--

CREATE TABLE IF NOT EXISTS `event_ticket_type` (
  `event_ticket_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `event_schedule_id` int(11) DEFAULT NULL,
  `ticket_name` varchar(255) DEFAULT NULL,
  `ticket_description` varchar(255) DEFAULT NULL,
  `total_ticket` int(11) DEFAULT NULL,
  `ticket_qty` int(11) DEFAULT NULL,
  `ticket_price` decimal(10,2) DEFAULT NULL,
  `per_user_limit` int(11) DEFAULT NULL,
  `e_ticket` int(11) DEFAULT NULL,
  `p_ticket` int(11) DEFAULT NULL,
  `total_sold_ticket` int(11) DEFAULT NULL,
  `total_sold_eticket` int(11) DEFAULT NULL,
  `total_sold_pticket` int(11) DEFAULT NULL,
  `ticket_available` int(11) DEFAULT NULL,
  `is_active` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`event_ticket_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `event_ticket_type`
--

INSERT INTO `event_ticket_type` (`event_ticket_type_id`, `event_id`, `event_schedule_id`, `ticket_name`, `ticket_description`, `total_ticket`, `ticket_qty`, `ticket_price`, `per_user_limit`, `e_ticket`, `p_ticket`, `total_sold_ticket`, `total_sold_eticket`, `total_sold_pticket`, `ticket_available`, `is_active`) VALUES
(1, 1, 1, 'GOLD', NULL, NULL, 500, '500.00', 5, 500, NULL, NULL, NULL, NULL, NULL, 'true'),
(2, 3, 2, 'gfgf', NULL, NULL, 5, '5.00', 5, 3, 2, NULL, NULL, NULL, NULL, 'true'),
(3, 4, 3, 'dfdf', NULL, NULL, 3, '3.00', 3, 1, 2, NULL, NULL, NULL, NULL, 'true');

-- --------------------------------------------------------

--
-- Table structure for table `event_venue`
--

CREATE TABLE IF NOT EXISTS `event_venue` (
  `event_venue_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `venue_id` int(11) DEFAULT NULL,
  `is_active` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`event_venue_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `event_venue`
--

INSERT INTO `event_venue` (`event_venue_id`, `event_id`, `venue_id`, `is_active`) VALUES
(1, 1, 1, 'true'),
(2, 2, 1, 'true'),
(3, 2, 2, 'true'),
(4, 3, 2, 'true'),
(5, 4, 1, 'true');

-- --------------------------------------------------------

--
-- Table structure for table `feature`
--

CREATE TABLE IF NOT EXISTS `feature` (
  `feature_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `feature_details` text,
  `feature_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`feature_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `feature`
--

INSERT INTO `feature` (`feature_id`, `feature_details`, `feature_name`) VALUES
(1, 'Ticket', 'Ticket'),
(2, 'Venue', 'Venue'),
(3, 'Terms & Condition', 'Terms & Condition'),
(4, 'Artist', 'Artist'),
(5, 'FAQ', 'FAQ'),
(6, 'Trailor', 'Trailor'),
(7, 'Image Gallery', 'Image Gallery'),
(8, 'Video Gallery', 'Video Gallery');

-- --------------------------------------------------------

--
-- Table structure for table `graphical_representaion`
--

CREATE TABLE IF NOT EXISTS `graphical_representaion` (
  `graphical_representation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `row` int(10) unsigned DEFAULT NULL,
  `column` int(10) unsigned DEFAULT NULL,
  `event_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`graphical_representation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE IF NOT EXISTS `language` (
  `language_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_name` varchar(255) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `organization`
--

CREATE TABLE IF NOT EXISTS `organization` (
  `organization_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organization_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `organization_official_email` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `organization_official_contact` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `organization_official_website` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `organization_varified_by` int(10) unsigned DEFAULT NULL,
  `organization_varified_status` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `organization_admin_id` int(10) unsigned DEFAULT NULL,
  `organization_updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `organization_updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`organization_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `organization`
--

INSERT INTO `organization` (`organization_id`, `organization_name`, `organization_official_email`, `organization_official_contact`, `organization_official_website`, `organization_varified_by`, `organization_varified_status`, `organization_admin_id`, `organization_updated_date`, `organization_updated_by`) VALUES
(1, 'AIUB', 'aiub@gmail.com', '52-5232323', 'www.aiub.edu', NULL, '1', 3, '2014-10-08 08:32:34', NULL),
(2, 'Ticket Chai', 'jahangirjony007@gmail.com', '87-8273827', 'www.systechunimax.com', NULL, '1', 4, '2014-10-22 10:51:52', NULL),
(3, 'SUL', 'sul@gmail.com', '54-5454545', 'www.systechunimax.com', NULL, '1', 5, '2014-10-22 10:59:05', NULL),
(4, 'TaltuFaltu', 'taltufaltu@gmail.com', '25-8525852', 'taltufaltu.org', NULL, '1', 8, '2014-12-23 04:34:55', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_name` varchar(45) DEFAULT NULL,
  `page_title` varchar(45) DEFAULT NULL,
  `is_active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `page_name`, `page_title`, `is_active`) VALUES
(1, 'index.php', 'Home', 'true'),
(2, 'event.php', 'Event', 'true'),
(3, 'contact_us.php', 'Contact Us', 'ture'),
(4, 'privacy_policy.php', 'Privacy Policy', 'false'),
(5, 'terms_of_service.php', 'Terms of Service ', 'false'),
(6, 'customer_dashboard.php', 'Dashboard ', 'true'),
(7, 'about_us.php', 'About Us', 'true'),
(8, 'login.php', 'Login', 'false'),
(9, 'thankyou.php', 'Thank you', 'false'),
(10, 'payment1.php', 'Payment', 'false'),
(11, 'event_details.php', 'Event Details', 'false'),
(12, 'how_to_buy.php', 'How To Buy', 'true'),
(13, 'forget_password.php', 'Forget Password', 'false'),
(14, 'notify.php', 'SSL notify', 'false'),
(15, 'fail.php', 'SSL fail', 'false'),
(16, 'cancel', 'SSL Cancel', 'false');

-- --------------------------------------------------------

--
-- Table structure for table `payment_info`
--

CREATE TABLE IF NOT EXISTS `payment_info` (
  `payment_info_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `card_number` varchar(255) DEFAULT NULL,
  `expeir_date` varchar(55) DEFAULT NULL,
  `expeir_month` varchar(45) DEFAULT NULL,
  `cvv` varchar(45) DEFAULT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`payment_info_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `payment_info`
--

INSERT INTO `payment_info` (`payment_info_id`, `card_number`, `expeir_date`, `expeir_month`, `cvv`, `customer_id`) VALUES
(1, '123456789', '2024', 'January', '123', 1);

-- --------------------------------------------------------

--
-- Table structure for table `photo_gallery`
--

CREATE TABLE IF NOT EXISTS `photo_gallery` (
  `photo_gallery_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `photo_name` varchar(255) DEFAULT NULL,
  `is_active` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`photo_gallery_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `photo_gallery`
--

INSERT INTO `photo_gallery` (`photo_gallery_id`, `event_id`, `photo_name`, `is_active`) VALUES
(1, 1, 'a.jpg', 'true'),
(2, 1, 'c.jpg', 'true'),
(3, 1, 'p1.jpg', 'true'),
(4, 1, 'p3.jpg', 'true'),
(5, 1, 'p4.jpg', 'true'),
(6, 1, 'p7.jpg', 'true'),
(7, 1, 's.jpg', 'true'),
(8, 1, 'q.jpg', 'true'),
(9, 4, 'p3.jpg', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `pick_point`
--

CREATE TABLE IF NOT EXISTS `pick_point` (
  `pick_point_id` int(11) NOT NULL AUTO_INCREMENT,
  `pick_point_name` varchar(255) DEFAULT NULL,
  `is_active` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`pick_point_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `pick_point`
--

INSERT INTO `pick_point` (`pick_point_id`, `pick_point_name`, `is_active`) VALUES
(1, 'JU Shuvo FCC 2011 01611522671', 'true'),
(2, 'DU Sharna MGCC 2008 0177949652', 'true'),
(3, 'DMC Sonnet JGCC 2010 01689730122', 'true'),
(4, 'BUET Sharmin FGCC 2011 01679047152', 'true'),
(5, 'MIST Rifah MGCC 2010 01917997362', 'true'),
(6, 'IUT Emon CCC 2010 01678070817', 'true'),
(7, 'AUST Mostafiz SCC 2010 01611526552', 'true'),
(8, 'NSU Rafid MCC 01675937097', 'true'),
(9, 'Dhanmondi Turja MCC 2014 01751916417', 'true'),
(10, 'Mirpur Alvi CCR 2014 01733341647', 'true'),
(11, 'Farmgate Istiar RCC 2010 01735136609', 'true'),
(12, 'Moghbazar Tanvir JCC 2011 01780115511', 'true'),
(13, 'Motijheel Tanzida Anzum MGCC 2014 01973111650', 'true'),
(14, 'Uttara Rayed CCC 2008 01732747934', 'true'),
(15, 'SMCC Mansura Moriom Milky JGCC 2012 01623251631', 'true'),
(16, 'Chittagong Koli MGCC 2010 01719461888', 'true'),
(17, 'Banani Saddi  FCC 1997 01819217644', 'true'),
(18, 'CCCL Ataullah  SCC 01711593031', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `priority`
--

CREATE TABLE IF NOT EXISTS `priority` (
  `priority_id` int(11) NOT NULL AUTO_INCREMENT,
  `priority_name` varchar(45) NOT NULL,
  PRIMARY KEY (`priority_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `priority`
--

INSERT INTO `priority` (`priority_id`, `priority_name`) VALUES
(1, '1'),
(2, '2'),
(3, '3'),
(4, '4');

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE IF NOT EXISTS `program` (
  `program_id` int(11) NOT NULL AUTO_INCREMENT,
  `program_name` varchar(255) DEFAULT NULL,
  `is_active` enum('true','false') NOT NULL,
  PRIMARY KEY (`program_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`program_id`, `program_name`, `is_active`) VALUES
(1, 'BSS', 'true'),
(2, 'MSS', 'true'),
(3, 'MDS', 'true'),
(4, 'PGDDS', 'true'),
(5, 'GDDS', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_address`
--

CREATE TABLE IF NOT EXISTS `shipping_address` (
  `shipping_address_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `shipping_post_code` varchar(45) DEFAULT NULL,
  `shipping_city` varchar(45) DEFAULT NULL,
  `shipping_country` varchar(45) DEFAULT NULL,
  `shipping_address` text,
  PRIMARY KEY (`shipping_address_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `shipping_address`
--

INSERT INTO `shipping_address` (`shipping_address_id`, `customer_id`, `shipping_post_code`, `shipping_city`, `shipping_country`, `shipping_address`) VALUES
(3, 1, '1123', 'Dhaka', '1', 'Banani Khilkhet Dhaka');

-- --------------------------------------------------------

--
-- Table structure for table `sms_traking`
--

CREATE TABLE IF NOT EXISTS `sms_traking` (
  `sms_tarking_id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`sms_tarking_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=125 ;

--
-- Dumping data for table `sms_traking`
--

INSERT INTO `sms_traking` (`sms_tarking_id`, `booking_id`, `status`) VALUES
(1, 3, 'CONFIRMED'),
(2, 9, 'CONFIRMED'),
(3, 1, 'NOT CONFIRMED'),
(4, 2, 'CONFIRMED'),
(5, 4, 'CONFIRMED'),
(6, 5, 'CONFIRMED'),
(7, 6, 'CONFIRMED'),
(8, 7, 'CONFIRMED'),
(9, 32, 'CONFIRMED'),
(10, 10, 'CONFIRMED'),
(11, 11, 'CONFIRMED'),
(12, 12, 'CONFIRMED'),
(13, 13, 'CONFIRMED'),
(14, 15, 'CONFIRMED'),
(15, 48, 'CONFIRMED'),
(16, 63, 'CONFIRMED'),
(17, 62, 'CONFIRMED'),
(18, 61, 'CONFIRMED'),
(19, 60, 'CONFIRMED'),
(20, 17, 'CONFIRMED'),
(21, 21, 'CONFIRMED'),
(22, 22, 'CONFIRMED'),
(23, 23, 'CONFIRMED'),
(24, 24, 'CONFIRMED'),
(25, 26, 'CONFIRMED'),
(26, 14, 'CONFIRMED'),
(27, 16, 'CONFIRMED'),
(28, 18, 'CONFIRMED'),
(29, 19, 'CONFIRMED'),
(30, 20, 'CONFIRMED'),
(31, 25, 'CONFIRMED'),
(32, 27, 'CONFIRMED'),
(33, 28, 'CONFIRMED'),
(34, 29, 'CONFIRMED'),
(35, 30, 'CONFIRMED'),
(36, 31, 'CONFIRMED'),
(37, 33, 'CONFIRMED'),
(38, 34, 'CONFIRMED'),
(39, 35, 'CONFIRMED'),
(40, 36, 'CONFIRMED'),
(41, 37, 'CONFIRMED'),
(42, 38, 'CONFIRMED'),
(43, 39, 'CONFIRMED'),
(44, 41, 'CONFIRMED'),
(45, 40, 'CONFIRMED'),
(46, 42, 'CONFIRMED'),
(47, 43, 'CONFIRMED'),
(48, 45, 'CONFIRMED'),
(49, 44, 'CONFIRMED'),
(50, 46, 'CONFIRMED'),
(51, 47, 'CONFIRMED'),
(52, 49, 'CONFIRMED'),
(53, 50, 'CONFIRMED'),
(54, 51, 'CONFIRMED'),
(55, 52, 'CONFIRMED'),
(56, 53, 'CONFIRMED'),
(57, 54, 'CONFIRMED'),
(58, 55, 'CONFIRMED'),
(59, 57, 'CONFIRMED'),
(60, 56, 'CONFIRMED'),
(61, 58, 'CONFIRMED'),
(62, 59, 'CONFIRMED'),
(63, 64, 'CONFIRMED'),
(64, 65, 'CONFIRMED'),
(65, 66, 'CONFIRMED'),
(66, 67, 'CONFIRMED'),
(67, 68, 'CONFIRMED'),
(68, 69, 'CONFIRMED'),
(69, 70, 'CONFIRMED'),
(70, 71, 'CONFIRMED'),
(71, 72, 'CONFIRMED'),
(72, 73, 'CONFIRMED'),
(73, 74, 'CONFIRMED'),
(74, 75, 'CONFIRMED'),
(75, 76, 'CONFIRMED'),
(76, 77, 'CONFIRMED'),
(77, 78, 'CONFIRMED'),
(78, 79, 'CONFIRMED'),
(79, 80, 'CONFIRMED'),
(80, 81, 'CONFIRMED'),
(81, 82, 'CONFIRMED'),
(82, 83, 'CONFIRMED'),
(83, 84, 'CONFIRMED'),
(84, 85, 'CONFIRMED'),
(85, 87, 'CONFIRMED'),
(86, 88, 'CONFIRMED'),
(87, 92, 'CONFIRMED'),
(88, 93, 'CONFIRMED'),
(89, 95, 'CONFIRMED'),
(90, 97, 'CONFIRMED'),
(91, 98, 'CONFIRMED'),
(92, 99, 'CONFIRMED'),
(93, 100, 'CONFIRMED'),
(94, 101, 'CONFIRMED'),
(95, 106, 'CONFIRMED'),
(96, 107, 'CONFIRMED'),
(97, 109, 'CONFIRMED'),
(98, 110, 'CONFIRMED'),
(99, 111, 'CONFIRMED'),
(100, 116, 'CONFIRMED'),
(101, 118, 'CONFIRMED'),
(102, 119, 'CONFIRMED'),
(103, 120, 'CONFIRMED'),
(104, 121, 'CONFIRMED'),
(105, 122, 'CONFIRMED'),
(106, 124, 'CONFIRMED'),
(107, 126, 'CONFIRMED'),
(108, 127, 'CONFIRMED'),
(109, 129, 'CONFIRMED'),
(110, 131, 'CONFIRMED'),
(111, 133, 'CONFIRMED'),
(112, 135, 'CONFIRMED'),
(113, 137, 'CONFIRMED'),
(114, 138, 'CONFIRMED'),
(115, 139, 'CONFIRMED'),
(116, 140, 'CONFIRMED'),
(117, 141, 'CONFIRMED'),
(118, 142, 'CONFIRMED'),
(119, 90, 'CONFIRMED'),
(120, 536, 'CONFIRMED'),
(121, 539, 'CONFIRMED'),
(122, 542, 'CONFIRMED'),
(123, 541, 'NOT CONFIRMED'),
(124, 540, 'NOT CONFIRMED');

-- --------------------------------------------------------

--
-- Table structure for table `subscription`
--

CREATE TABLE IF NOT EXISTS `subscription` (
  `subscription_id` int(11) NOT NULL AUTO_INCREMENT,
  `college_validation_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `subscription_type` varchar(255) DEFAULT NULL,
  `subscription_amount` float DEFAULT NULL,
  `payment_status` varchar(45) DEFAULT NULL,
  `is_booking` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`subscription_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `synopsis`
--

CREATE TABLE IF NOT EXISTS `synopsis` (
  `synopsis_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` varchar(255) DEFAULT NULL,
  `synopsis_genre` varchar(255) DEFAULT NULL,
  `synopsis_details` text,
  `synopsis_release_date` varchar(255) DEFAULT NULL,
  `synopsis_writer_name` varchar(255) DEFAULT NULL,
  `synopsis_director_name` varchar(255) DEFAULT NULL,
  `synopsis_duration` varchar(255) DEFAULT NULL,
  `event_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`synopsis_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `temp_booking`
--

CREATE TABLE IF NOT EXISTS `temp_booking` (
  `temp_booking_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(10) unsigned DEFAULT NULL,
  `schedule_id` int(10) unsigned DEFAULT NULL,
  `payment_status` varchar(255) DEFAULT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `ticket_quantity` varchar(255) DEFAULT NULL,
  `ticket_price` varchar(255) DEFAULT NULL,
  `discount_amount` varchar(255) DEFAULT NULL,
  `total_amount` varchar(255) DEFAULT NULL,
  `delivery_method` varchar(255) DEFAULT NULL,
  `delivery_address` text,
  `delivery_post_code` varchar(40) DEFAULT NULL,
  `delivery_status` varchar(45) DEFAULT NULL,
  `tocken` varchar(255) DEFAULT NULL,
  `booking_time` varchar(255) DEFAULT NULL,
  `pick_point_id` int(11) DEFAULT NULL,
  `delivery_city` varchar(45) DEFAULT NULL,
  `delivery_district` varchar(45) DEFAULT NULL,
  `delivery_cost` varchar(45) DEFAULT NULL,
  `delivery_vat` varchar(45) DEFAULT NULL,
  `cadet_id` int(11) DEFAULT NULL,
  `is_cancle` varchar(45) DEFAULT NULL,
  `delivery_country` int(11) DEFAULT NULL,
  `ticket_type_id` int(11) DEFAULT NULL,
  `spouse` varchar(45) DEFAULT NULL,
  `self` varchar(45) DEFAULT NULL,
  `kidesUn12` varchar(45) DEFAULT NULL,
  `kidesUp12` varchar(45) DEFAULT NULL,
  `guest` varchar(45) DEFAULT NULL,
  `maid` varchar(45) DEFAULT NULL,
  `driver` varchar(45) DEFAULT NULL,
  `is_online` varchar(45) DEFAULT NULL,
  `val_id` varchar(255) DEFAULT NULL,
  `online_payment_status` varchar(255) DEFAULT NULL,
  `online_pay_method` varchar(45) DEFAULT NULL,
  `card_number` varchar(255) DEFAULT NULL,
  `card_Holder_name` varchar(255) DEFAULT NULL,
  `Mobile_number` varchar(255) DEFAULT NULL,
  `selfQty` varchar(45) DEFAULT NULL,
  `kidesUn12Qty` varchar(45) DEFAULT NULL,
  `kidsUpQty` varchar(45) DEFAULT NULL,
  `guestQty` varchar(45) DEFAULT NULL,
  `driverQty` varchar(45) DEFAULT NULL,
  `spouseQty` varchar(45) DEFAULT NULL,
  `maidQty` varchar(45) DEFAULT NULL,
  `anual_subsciption_cost` varchar(45) DEFAULT NULL,
  `life_time_subsciption_cost` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`temp_booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `temp_carts_events`
--

CREATE TABLE IF NOT EXISTS `temp_carts_events` (
  `TC_id` int(5) NOT NULL AUTO_INCREMENT,
  `TC_session_id` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `TC_user_id` int(11) NOT NULL,
  `TC_product_id` int(5) NOT NULL,
  `TC_product_type` enum('event','') COLLATE utf8_unicode_ci NOT NULL,
  `TC_schedule_id` int(11) NOT NULL,
  `TC_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`TC_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='empty regularly using cron, TC= temp_carts' AUTO_INCREMENT=19 ;

--
-- Dumping data for table `temp_carts_events`
--

INSERT INTO `temp_carts_events` (`TC_id`, `TC_session_id`, `TC_user_id`, `TC_product_id`, `TC_product_type`, `TC_schedule_id`, `TC_updated`) VALUES
(1, '98f6mfvtk7uei3jd195rpverg0', 0, 2, 'event', 2, '2015-01-11 11:06:55'),
(5, 'l2pm2969ofr7i5e3p5ktmr3tt1', 0, 2, 'event', 2, '2015-01-11 13:04:17'),
(6, 's57mhb00h8fbogtdvpqc6dtcc6', 0, 2, 'event', 2, '2015-01-12 04:35:03'),
(7, 'h09vostc5ttub2u2uvqikaho47', 0, 36, 'event', 4, '2015-01-13 03:45:59'),
(8, 'h09vostc5ttub2u2uvqikaho47', 0, 37, 'event', 6, '2015-01-13 08:24:29'),
(10, 'pis7gmuuqpq6rrcenb4lq6iaf6', 0, 38, 'event', 8, '2015-01-13 14:32:06'),
(12, 'r28npqa2p2p04cln1cuncfpeb6', 3, 1, 'event', 1, '2015-01-14 11:35:35'),
(15, 'gthiak4g9jc4ihr4ula3pc2n56', 3, 1, 'event', 1, '2015-01-15 03:51:28'),
(16, 'sdgecka98kionl8ig22fukn2h7', 0, 1, 'event', 1, '2015-01-15 03:52:50'),
(18, '5s7bmusf0u14ivnij9n3s3aba5', 0, 1, 'event', 1, '2015-01-15 03:57:22');

-- --------------------------------------------------------

--
-- Table structure for table `temp_cart_addition`
--

CREATE TABLE IF NOT EXISTS `temp_cart_addition` (
  `TCA_id` int(11) NOT NULL AUTO_INCREMENT,
  `TCA_TC_id` int(11) NOT NULL,
  `TCA_session_id` varchar(255) NOT NULL,
  `TCA_item_id` int(11) NOT NULL,
  `TCA_item_type` enum('include','type') NOT NULL,
  `TCA_item_per_price` decimal(10,2) NOT NULL,
  `TCA_item_total_price` decimal(10,2) NOT NULL,
  `TCA_item_quantity` int(11) NOT NULL,
  `TCA_item_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`TCA_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `temp_cart_addition`
--

INSERT INTO `temp_cart_addition` (`TCA_id`, `TCA_TC_id`, `TCA_session_id`, `TCA_item_id`, `TCA_item_type`, `TCA_item_per_price`, `TCA_item_total_price`, `TCA_item_quantity`, `TCA_item_updated`) VALUES
(1, 1, '98f6mfvtk7uei3jd195rpverg0', 2, 'type', '2000.00', '4000.00', 2, '2015-01-11 11:06:55'),
(2, 1, '98f6mfvtk7uei3jd195rpverg0', 3, 'include', '10.00', '10.00', 1, '2015-01-11 11:07:38'),
(6, 5, 'l2pm2969ofr7i5e3p5ktmr3tt1', 2, 'type', '2000.00', '2000.00', 1, '2015-01-11 13:33:33'),
(7, 5, 'l2pm2969ofr7i5e3p5ktmr3tt1', 3, 'type', '3000.00', '3000.00', 1, '2015-01-11 13:05:33'),
(8, 5, 'l2pm2969ofr7i5e3p5ktmr3tt1', 2, 'include', '15.00', '15.00', 1, '2015-01-11 13:05:41'),
(9, 5, 'l2pm2969ofr7i5e3p5ktmr3tt1', 3, 'include', '10.00', '10.00', 1, '2015-01-11 13:05:46'),
(10, 6, 's57mhb00h8fbogtdvpqc6dtcc6', 2, 'type', '2000.00', '4000.00', 2, '2015-01-12 04:35:03'),
(11, 7, 'h09vostc5ttub2u2uvqikaho47', 5, 'type', '1000.00', '5000.00', 5, '2015-01-13 03:46:09'),
(12, 8, 'h09vostc5ttub2u2uvqikaho47', 7, 'type', '200.00', '400.00', 2, '2015-01-13 08:24:29'),
(14, 10, 'pis7gmuuqpq6rrcenb4lq6iaf6', 10, 'type', '100.00', '100.00', 1, '2015-01-13 14:32:07'),
(16, 12, 'r28npqa2p2p04cln1cuncfpeb6', 1, 'type', '500.00', '1500.00', 3, '2015-01-14 11:35:35'),
(17, 12, 'r28npqa2p2p04cln1cuncfpeb6', 1, 'include', '10.00', '30.00', 3, '2015-01-14 11:35:37'),
(20, 15, 'gthiak4g9jc4ihr4ula3pc2n56', 1, 'type', '500.00', '1000.00', 2, '2015-01-15 03:51:28'),
(21, 16, 'sdgecka98kionl8ig22fukn2h7', 1, 'type', '500.00', '1000.00', 2, '2015-01-15 03:52:50'),
(22, 16, 'sdgecka98kionl8ig22fukn2h7', 1, 'include', '10.00', '20.00', 2, '2015-01-15 03:53:03'),
(24, 18, '5s7bmusf0u14ivnij9n3s3aba5', 1, 'type', '500.00', '500.00', 1, '2015-01-15 03:57:22');

-- --------------------------------------------------------

--
-- Table structure for table `temp_donation`
--

CREATE TABLE IF NOT EXISTS `temp_donation` (
  `temp_donation_id` int(11) NOT NULL AUTO_INCREMENT,
  `college_validation_id` int(11) DEFAULT NULL,
  `donation_amount` varchar(45) DEFAULT NULL,
  `student_number` varchar(45) DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `is_donate` varchar(45) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `token_no` varchar(255) NOT NULL,
  `event_id` int(11) DEFAULT NULL,
  `schedule_id` int(11) DEFAULT NULL,
  `delivery_method` varchar(255) DEFAULT NULL,
  `is_online` varchar(255) DEFAULT NULL,
  `college_id` int(11) DEFAULT NULL,
  `customer_address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `discrict_id` int(11) DEFAULT NULL,
  `zip_code` varchar(45) DEFAULT NULL,
  `val_id` varchar(255) DEFAULT NULL,
  `payment_status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`temp_donation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `temp_subscription`
--

CREATE TABLE IF NOT EXISTS `temp_subscription` (
  `temp_subscription_id` int(11) NOT NULL AUTO_INCREMENT,
  `college_validation_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `subscription_type` varchar(255) DEFAULT NULL,
  `subscription_amount` float DEFAULT NULL,
  `payment_status` varchar(45) DEFAULT NULL,
  `is_booking` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`temp_subscription_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

CREATE TABLE IF NOT EXISTS `ticket` (
  `ticket_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_total` int(10) unsigned DEFAULT NULL,
  `ticket_price` int(10) unsigned DEFAULT NULL,
  `discount_price` varchar(45) DEFAULT NULL,
  `ticket_available` int(10) unsigned DEFAULT NULL,
  `ticket_limit` int(10) unsigned DEFAULT NULL,
  `event_id` int(10) unsigned DEFAULT NULL,
  `ticket_type_id` int(10) unsigned DEFAULT NULL,
  `vendor_id` varchar(45) DEFAULT NULL,
  `total_vticket` varchar(45) DEFAULT NULL,
  `total_eticket` varchar(45) DEFAULT NULL,
  `total_pticket` varchar(45) DEFAULT NULL,
  `schedule_id` int(10) unsigned DEFAULT NULL,
  `schedule_total_ticket` int(10) DEFAULT NULL,
  PRIMARY KEY (`ticket_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_type`
--

CREATE TABLE IF NOT EXISTS `ticket_type` (
  `ticket_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_type_name` varchar(45) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `includes` varchar(45) DEFAULT NULL,
  `is_active` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ticket_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `ticket_type`
--

INSERT INTO `ticket_type` (`ticket_type_id`, `ticket_type_name`, `event_id`, `includes`, `is_active`) VALUES
(1, 'Platinum', 1, 'true', 'true'),
(2, 'Gold', 1, 'true', 'true'),
(3, 'Silver', 2, 'true', 'true'),
(4, 'High', 3, 'true', 'true'),
(5, 'Medium', 3, 'true', 'true'),
(6, 'a', 4, 'true', 'false'),
(7, 'Closed', 5, 'true', 'true'),
(8, 'bb', 40, 'false', 'true'),
(9, 'aaa', 42, 'true', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_type_wise_price`
--

CREATE TABLE IF NOT EXISTS `ticket_type_wise_price` (
  `ticket_id` int(11) NOT NULL AUTO_INCREMENT,
  `e_ticket` int(11) DEFAULT NULL,
  `p_ticket` int(11) DEFAULT NULL,
  `price` varchar(40) DEFAULT NULL,
  `ticket_type_id` int(11) DEFAULT NULL,
  `schedule_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `total_ticket` int(11) DEFAULT NULL,
  `discount` float DEFAULT NULL,
  PRIMARY KEY (`ticket_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `ticket_type_wise_price`
--

INSERT INTO `ticket_type_wise_price` (`ticket_id`, `e_ticket`, `p_ticket`, `price`, `ticket_type_id`, `schedule_id`, `event_id`, `total_ticket`, `discount`) VALUES
(1, NULL, NULL, '1000', 1, 1, 1, 2000, NULL),
(2, NULL, NULL, '2000', 2, 1, 1, 2000, NULL),
(3, NULL, NULL, '500', 3, 2, 2, 2000, NULL),
(4, NULL, NULL, '900', 4, 3, 3, 2000, NULL),
(5, NULL, NULL, '900', 5, 3, 3, 2000, NULL),
(6, NULL, NULL, '800', 4, 4, 3, 2000, NULL),
(7, NULL, NULL, '800', 5, 4, 3, 2000, NULL),
(8, NULL, NULL, '500', 7, 6, 5, 5000, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_review`
--

CREATE TABLE IF NOT EXISTS `user_review` (
  `user_review_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `session_id` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ratings` int(11) DEFAULT '0',
  `review` text CHARACTER SET utf8,
  `status` enum('draft','published','deleted') CHARACTER SET utf8 NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_review_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `user_review`
--

INSERT INTO `user_review` (`user_review_id`, `user_id`, `event_id`, `session_id`, `ratings`, `review`, `status`, `updated_date`) VALUES
(1, 6, 36, 'pcu9j64u0gkv583eld8ihpe3j3', 3, 'this is the first message', 'published', '2015-01-13 10:14:44'),
(2, 7, 36, 'pcu9j64u0gkv583eld8ihpe3j3', 1, 'fgdddddddddd', 'published', '2015-01-13 10:17:43'),
(3, 1, 36, 'pcu9j64u0gkv583eld8ihpe3j3', 4, 'adfssssssssssssssss', 'published', '2015-01-13 10:33:03'),
(4, 2, 38, 'pcu9j64u0gkv583eld8ihpe3j3', 2, 'dfasfasdfsdfdfddfdfdsfdsfdsfdsf', 'published', '2015-01-13 10:36:06'),
(5, 3, 38, 'pcu9j64u0gkv583eld8ihpe3j3', 5, 'khairul islamkdjfdfjdhfd jkodjfuden dfjfkdf kjfdio okdjfd ioidkd kodfokd kdkfjdk ', 'draft', '2015-01-13 11:24:35'),
(7, 4, 38, 'pcu9j64u0gkv583eld8ihpe3j3', 0, 'asasasasasas', 'draft', '2015-01-13 12:08:41'),
(9, 5, 38, 'pcu9j64u0gkv583eld8ihpe3j3', 0, 'qqqqqqqqqqqqqqqqqq', 'draft', '2015-01-13 13:51:06'),
(10, 6, 38, 'pis7gmuuqpq6rrcenb4lq6iaf6', 0, 'fddfdf', 'draft', '2015-01-13 14:49:55'),
(11, 3, 1, 'ch55a9gtsva58fn7aj6ga1dge5', 5, 'Hi ticketchai, your site is awesome looking.', 'draft', '2015-01-14 04:34:12'),
(14, 3, 4, 'r28npqa2p2p04cln1cuncfpeb6', 4, 'qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq', 'published', '2015-01-14 13:00:59');

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE IF NOT EXISTS `user_type` (
  `user_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_type_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `is_active` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`user_type_id`, `user_type_name`, `updated_date`, `updated_by`, `is_active`) VALUES
(1, 'Super Admin', '2014-10-08 08:10:22', NULL, 'true'),
(2, 'Member', '2014-10-08 08:10:31', NULL, 'true'),
(3, 'Admin', '2014-10-08 08:10:38', NULL, 'true'),
(4, 'Vendor', '2014-11-11 13:55:31', NULL, 'true'),
(5, 'Merchant', '2014-12-23 04:28:59', NULL, 'true');

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE IF NOT EXISTS `vendor` (
  `vendor_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vendor_name` varchar(255) DEFAULT NULL,
  `vendor_phone` varchar(255) DEFAULT NULL,
  `vendor_email` varchar(255) DEFAULT NULL,
  `vendor_address` varchar(255) DEFAULT NULL,
  `organization_id` int(10) unsigned DEFAULT NULL,
  `is_active` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`vendor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`vendor_id`, `vendor_name`, `vendor_phone`, `vendor_email`, `vendor_address`, `organization_id`, `is_active`) VALUES
(1, 'Vendor', '018-74584512', 'vendor@gmail.com', 'Banani, Dhaka', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `venue`
--

CREATE TABLE IF NOT EXISTS `venue` (
  `venue_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `venue_name` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `venue_address` varchar(255) DEFAULT NULL,
  `venue_attraction` varchar(255) DEFAULT NULL,
  `venue_total_seat` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`venue_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `venue`
--

INSERT INTO `venue` (`venue_id`, `venue_name`, `latitude`, `longitude`, `venue_address`, `venue_attraction`, `venue_total_seat`) VALUES
(1, 'ARMY STADIUM', '23.8055', '90.4041', 'Tongi Diversion Road, Dhaka', 'Bangladesh Army Stadium is a multi-use stadium in Dhaka, Bangladesh. It is currently used mostly for football matches and hosts the home matches of Bangladesh Army. The stadium holds 15,000 people.', 15000),
(2, 'JAMUNA FUTURE PARK', '23.8133', '90.4242', 'Ka-244, Pragati Avenue, Dhaka', 'Jamuna Future Park, commonly abbreviated as JFP, is a shopping mall located in Dhaka, Bangladesh.', 10000);

-- --------------------------------------------------------

--
-- Table structure for table `video_gallery`
--

CREATE TABLE IF NOT EXISTS `video_gallery` (
  `video_gallery_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `is_active` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`video_gallery_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `video_gallery`
--

INSERT INTO `video_gallery` (`video_gallery_id`, `event_id`, `link`, `is_active`) VALUES
(1, 4, 'http://www.facebook.com', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

CREATE TABLE IF NOT EXISTS `wishlists` (
  `WL_id` int(5) NOT NULL AUTO_INCREMENT,
  `WL_product_id` int(5) NOT NULL,
  `WL_product_type` enum('event','') COLLATE utf8_unicode_ci NOT NULL,
  `WL_user_id` int(11) NOT NULL,
  `WL_cteated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`WL_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='WL= wishlists ' AUTO_INCREMENT=10 ;

--
-- Dumping data for table `wishlists`
--

INSERT INTO `wishlists` (`WL_id`, `WL_product_id`, `WL_product_type`, `WL_user_id`, `WL_cteated`) VALUES
(1, 2, 'event', 38, '2015-01-07 13:37:09'),
(2, 2, 'event', 7, '2015-01-12 09:24:30'),
(3, 37, 'event', 6, '2015-01-13 08:32:29'),
(4, 36, 'event', 6, '2015-01-13 08:32:32'),
(5, 38, 'event', 6, '2015-01-13 08:32:38'),
(6, 1, 'event', 46, '2015-01-14 04:33:23'),
(7, 1, 'event', 2, '2015-01-14 07:21:57'),
(8, 1, 'event', 3, '2015-01-14 07:58:41'),
(9, 1, 'event', 4, '2015-01-14 08:47:55');

-- --------------------------------------------------------

--
-- Table structure for table `year`
--

CREATE TABLE IF NOT EXISTS `year` (
  `year_id` int(11) NOT NULL AUTO_INCREMENT,
  `year_name` varchar(45) DEFAULT NULL,
  `is_active` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`year_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `year`
--

INSERT INTO `year` (`year_id`, `year_name`, `is_active`) VALUES
(1, '1965', 'true'),
(2, '1966', 'true'),
(3, '1967', 'true'),
(4, '1968', 'true'),
(5, '1969', 'true'),
(6, '1970', 'true'),
(7, '1971', 'true'),
(8, '1972', 'true'),
(9, '1973', 'true'),
(10, '1974', 'true'),
(11, '1975', 'true'),
(12, '1976', 'true'),
(13, '1977', 'true'),
(14, '1978', 'true'),
(15, '1979', 'true'),
(16, '1980', 'true'),
(17, '1981', 'true'),
(18, '1982', 'true'),
(19, '1983', 'true'),
(20, '1984', 'true'),
(21, '1985', 'true'),
(22, '1986', 'true'),
(23, '1987', 'true'),
(24, '1988', 'true'),
(25, '1989', 'true'),
(26, '1990', 'true'),
(27, '1991', 'true'),
(28, '1992', 'true'),
(29, '1993', 'true'),
(30, '1994', 'true'),
(31, '1995', 'true'),
(32, '1996', 'true'),
(33, '1997', 'true'),
(34, '1998', 'true'),
(35, '1999', 'true'),
(36, '2000', 'true'),
(37, '2001', 'true'),
(38, '2002', 'true'),
(39, '2003', 'true'),
(40, '2004', 'true'),
(41, '2005', 'true'),
(42, '2006', 'true'),
(43, '2007', 'true'),
(44, '2008', 'true'),
(45, '2009', 'true'),
(46, '2010', 'true'),
(47, '2011', 'true'),
(48, '2012', 'true'),
(49, '2013', 'true'),
(50, '2014', 'true');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
