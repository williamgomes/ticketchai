<?php
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];

if ($verb == "GET") {

    $arr = array();
    $arr[0] = array("status_id" => utf8_encode(1), "status_name" => utf8_encode("Active"));
    $arr[1] = array("status_id" => utf8_encode(2), "status_name" => utf8_encode("In-Active"));
    $jsonArr = json_encode($arr);

    echo "{\"data\":" . $jsonArr . "}";
}
