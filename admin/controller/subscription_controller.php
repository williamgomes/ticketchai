<?php

include '../config/class.web.config.php';
$con = new Config();
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];

if ($verb == "GET") {
    $arr = array();
    $rs = "SELECT
	CASE subscription_type
WHEN 'is_anual' THEN
	'Annual Subscriber'
ELSE
	'Life Time Subscriber'
END AS type,
 subscription.*, CASE college_validation.subcription_delivery_method
WHEN 'online_payment' THEN
	'Online Payment'
ELSE
	CASE college_validation.subcription_delivery_method
WHEN 'home_delivery' THEN
	'Home Delivery'
ELSE
	'Pick From Office'
END
END AS method,

ifnull(college_validation.contact_number, (SELECT cs.phone FROM customer as cs WHERE cs.customer_id=customer.customer_id)) as phone,
 college_validation.student_name,
 college_validation.batch_id,
 college_validation.student_number,
 customer.address,
 customer.email_address

FROM
	subscription
LEFT JOIN college_validation ON college_validation.college_validation_id = subscription.college_validation_id
LEFT JOIN customer ON customer.customer_id = subscription.customer_id
ORDER BY subscription_id DESC";

    $jsonArr = $con->ReturnObjectByQuery($rs, "json");
    $count = count($jsonArr);
    if ($count >= 1) {
        echo "{\"data\":" . $jsonArr . "}";
    } else {
        echo "{\"data\":" . "[]" . "}";
    }
}
?>
