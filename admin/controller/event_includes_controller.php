<?php

include '../config/class.web.config.php';
$con = new Config();
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];

if ($verb == "GET") {
    $g_event_id = $_GET["event_id"];
    $query = "SELECT CONCAT('Date-',event_schedule.event_date,' Start-',event_schedule.event_schedule_start_time,' End-',event_schedule.event_schedule_end_time) as event_schedule_name,
event_includes.* FROM event_includes 
INNER JOIN event_schedule ON event_includes.event_schedule_id = event_schedule.event_schedule_id
WHERE event_includes.event_id='$g_event_id'";
    $jsonArr = $con->ReturnObjectByQuery($query, "json");
    $count = count(json_decode($jsonArr));
    if ($count >= 1) {
        echo "{\"data\":" . $jsonArr . "}";
    } else {
        echo "{\"data\":" . "[]" . "}";
    }
}


if ($verb == "POST") {
    extract($_POST);
    //array_push($_POST, "Error_Flag");
    unset($_POST["event_schedule_name"]);
    unset($_POST["event_id"]);
    //unset($_POST["event_venue_id"]);
    $errors = array();
    // $_POST["event_id"] = $_GET["event_id"];
    $result = $con->update("event_includes", $_POST, "event_includes_id", "", "array");
    if ($result["output"] == "error") {
        $errors = array("error" => "yes", "message" => $result["msg"]);
        echo json_encode($errors);
    } else if ($result["output"] == "success") {
        if ($result["result"]) {
            echo json_encode($result["result"]);
        } else {
            $errors = array("error" => "yes", "message" => "Update failed for Includes Name: " . $_POST["event_includes_id"]);
            echo json_encode($errors);
        }
    }
}



if ($verb == "PUT") {
    $request_vars = array();
    parse_str(file_get_contents('php://input'), $request_vars);

    array_shift($request_vars);
    $request_vars["event_id"] = $_GET["event_id"];
    $c_event_id = $_GET["event_id"];
//array_push($request_vars, $request_vars["event_id"]);
// $con->debug($request_vars);
//exit();
    $event_id = $request_vars["event_id"];
    $event_includes_schedule_id = $request_vars["event_schedule_id"];
    $event_includes_name = $request_vars["event_includes_name"];
    $event_includes_price = $request_vars["event_includes_price"];
    $event_includes_per_user_limit = $request_vars["event_includes_per_user_limit"];
    $total_qty = $request_vars["total_qty"];
    $is_active = $request_vars["is_active"];


    $CheckExists = $con->CheckExistsWithCondition("event_includes", " event_includes_name='$event_includes_name' AND event_id='$event_id'");

    if ($CheckExists == 0) {
        $insert_array = array(
            "event_id" => $c_event_id,
            "event_schedule_id" => $event_includes_schedule_id,
            "event_includes_name" => $event_includes_name,
            "event_includes_price" => $event_includes_price,
            "event_includes_per_user_limit" => $event_includes_per_user_limit,
            "total_qty" => $total_qty,
            "is_active" => $is_active
        );
        $result = $con->insert("event_includes", $insert_array, "", "", "array");
        if ($result["output"] == "error") {
            $errors = array("error" => "yes", "message" => $result["msg"]);
            echo json_encode($errors);
        } else if ($result["output"] == "success") {
            if ($result["result"]) {
                echo "" . $result["result"] . "";
            } else {
                $errors = array("error" => "yes", "message" => "Invalid Insertion Query");
                echo json_encode($errors);
            }
        }
    } else {
        $errors = array("error" => "yes", "message" => "Event Includes Name Already Exists For This Event");
        echo json_encode($errors);
    }
}
if ($verb == "DELETE") {

    $request_vars = array();
    parse_str(file_get_contents('php://input'), $request_vars);
    $errors = array();
    $event_includes_id = $request_vars["event_includes_id"];

    $delete_array = array("event_includes_id" => $event_includes_id);
//$rs = $con->delete($object, $object_array, $return_type)
    $rs = $con->delete("event_includes", $delete_array, "array");


    if ($rs["output"] == "success") {
        echo "" . $event_includes_id . "";
    } else {
        $errors = array("error" => "yes", "message" => "Invalid Delete Query!");
        echo json_encode($errors);
    }
}
?>