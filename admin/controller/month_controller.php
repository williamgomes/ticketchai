<?php
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];
if ($verb == "GET") {
    $arr = array();
    
    $arr[0] = array("month_id" => utf8_encode(1), "month_name" => utf8_encode("January"));
    $arr[1] = array("month_id" => utf8_encode(2), "month_name" => utf8_encode("February"));
    $arr[2] = array("month_id" => utf8_encode(3), "month_name" => utf8_encode("March"));
    $arr[3] = array("month_id" => utf8_encode(4), "month_name" => utf8_encode("April"));
    $arr[4] = array("month_id" => utf8_encode(5), "month_name" => utf8_encode("May"));
    $arr[5] = array("month_id" => utf8_encode(6), "month_name" => utf8_encode("June"));
    $arr[6] = array("month_id" => utf8_encode(7), "month_name" => utf8_encode("July"));
    $arr[7] = array("month_id" => utf8_encode(8), "month_name" => utf8_encode("August"));
    $arr[8] = array("month_id" => utf8_encode(9), "month_name" => utf8_encode("September"));
    $arr[9] = array("month_id" => utf8_encode(10), "month_name" => utf8_encode("October"));
    $arr[10] = array("month_id" => utf8_encode(11), "month_name" => utf8_encode("November"));
    $arr[11] = array("month_id" => utf8_encode(12), "month_name" => utf8_encode("December"));
    
    $jsonArr = json_encode($arr);
    echo "{\"data\":" . $jsonArr . "}";
}