<?php
include '../config/class.web.config.php';
$con = new Config();
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];

if ($verb == "GET") {
    $jsonArr = $con->SelectAll("pick_point", "", "", "json");
    $count = count(json_decode($jsonArr));
    if ($count >= 1) {
        echo "{\"data\":" . $jsonArr . "}";
    } else {
        echo "{\"data\":" . "[]" . "}";
    }
}
if ($verb == "POST") {
    extract($_POST);
    //array_push($_POST, "Error_Flag");
    $errors = array();
    $result= $con->update("pick_point", $_POST, "pick_point_name","", "array");
    if ($result["output"] == "error") {
        $errors = array("error" => "yes", "message" => $result["msg"]);
        echo json_encode($errors);
    } 
    else if($result["output"]=="success")
    {
        if ($result["result"]) {
            echo json_encode($result["result"]);
        } else {
            $errors = array("error" => "yes", "message" => "Update failed for Pick Point Name: " .$_POST["pick_point_name"] );
            echo json_encode($errors);
        }
    }
}
if ($verb == "PUT") {
    $request_vars = array();
    parse_str(file_get_contents('php://input'), $request_vars);
    array_shift($request_vars);
    //array_push($request_vars, "flag");

    $result = $con->insert("pick_point", $request_vars, "pick_point_name","","array");

    if ($result["output"] == "error") {
        $errors = array("error" => "yes", "message" => $result["msg"]);
        echo json_encode($errors);
    } else if ($result["output"] == "success") {
        if ($result["result"]) {
            echo "" . $result["result"] . "";
        } else {
            $errors = array("error" => "yes", "message" => "Invalid Insertion Query");
            echo json_encode($errors);
        }
    }
}
if ($verb == "DELETE") {

    $request_vars = array();
    parse_str(file_get_contents('php://input'), $request_vars);
    $errors = array();
    $pick_point_id = $request_vars["pick_point_id"];

    $delete_array = array("pick_point_id" => $pick_point_id);
    //$rs = $con->delete($object, $object_array, $return_type)
    $rs = $con->delete("pick_point", $delete_array,"array");
    

    if ($rs["output"] == "success") {
        echo "" . $pick_point_id . "";
    } else {
        $errors = array("error" => "yes", "message" => "Invalid Delete Query!");
        echo json_encode($errors);
    }
}
?>