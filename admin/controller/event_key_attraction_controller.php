<?php
include '../config/class.web.config.php';
$con = new Config();

header("Content-type: application/json");

$verb = $_SERVER["REQUEST_METHOD"];


if ($verb == "GET") {
    $g_event_id = $_GET["event_id"];
    $query = "SELECT * FROM event_key_attraction WHERE event_id = '$g_event_id'";
    $jsonArr = $con->ReturnObjectByQuery($query, "array");
    $returnarray = array();
    foreach ($jsonArr as $js){
        $temp_array = array();
        $temp_array["event_key_attraction_id"] = $js->event_key_attraction_id;
        $temp_array["event_id"] = $js->event_id;
        $temp_array["attraction_name"] = $js->attraction_name;
        $temp_array["image"] = $js->image;
        $temp_array["facebook"] = $js->facebook;
        $temp_array["twitter"] = $js->twitter;
        $temp_array["google_plus"] = $js->google_plus;
        $temp_array["details"] = html_entity_decode(html_entity_decode($js->details, ENT_COMPAT, 'UTF-8'));
        $temp_array["is_active"] = $js->is_active;
        
        array_push($returnarray, $temp_array);
    } 
    $json_return_array = json_encode($returnarray);
    
    
    $count = count(json_decode($json_return_array));
    if ($count >= 1) {
        echo "{\"data\":" . $json_return_array . "}";
    } else {
        echo "{\"data\":" . "[]" . "}";
    }
}


if ($verb == "PUT") {

    $request_vars = array();

    parse_str(file_get_contents('php://input'), $request_vars);

    array_shift($request_vars);

    //array_push($request_vars, "flag");

    $request_vars["event_id"] = $_GET["event_id"];
    $c_event_id = $_GET["event_id"];
    $event_id = $request_vars["event_id"];
    $attraction_name = $request_vars["attraction_name"];
    $image = $request_vars["image"];
    $facebook = $request_vars["facebook"];
    $twitter = $request_vars["twitter"];
    $google_plus = $request_vars["google_plus"];
    $details = htmlentities($request_vars["details"]);
    $is_active = $request_vars["is_active"];

    $insert_array = array(
        "event_id" => $event_id,
        "attraction_name" => $attraction_name,
        "details" => $details,
        "image" => $image,
        "facebook" => $facebook,
        "twitter" => $twitter,
        "google_plus" => $google_plus,
        "is_active" => $is_active
    );

    $result = $con->insert("event_key_attraction", $insert_array, "", "", "array");
    if ($result["output"] == "error") {

        $errors = array("error" => "yes", "message" => $result["msg"]);

        echo json_encode($errors);
    } else if ($result["output"] == "success") {

        if ($result["result"]) {

            echo "" . $result["result"] . "";
        } else {

            $errors = array("error" => "yes", "message" => "Invalid Insertion Query");

            echo json_encode($errors);
        }
    }
}

if ($verb == "POST") {

    extract($_POST);

    //array_push($_POST, "Error_Flag");

    $errors = array();

    $result = $con->update("event_key_attraction", $_POST, "event_key_attraction_id", "", "array");

    if ($result["output"] == "error") {

        $errors = array("error" => "yes", "message" => $result["msg"]);

        echo json_encode($errors);
    } else if ($result["output"] == "success") {

        if ($result["result"]) {

            echo json_encode($result["result"]);
        } else {

            $errors = array("error" => "yes", "message" => "Update failed for Event Key Attraction ID: " . $_POST["event_key_attraction_id"]);

            echo json_encode($errors);
        }
    }
}


if ($verb == "DELETE") {

    $request_vars = array();
    parse_str(file_get_contents('php://input'), $request_vars);
    $errors = array();
    $event_key_attraction_id= $request_vars["event_key_attraction_id"];
    $delete_array = array("event_key_attraction_id" => $event_key_attraction_id);
    $rs = $con->delete("event_key_attraction", $delete_array, "array");


    if ($rs["output"] == "success") {
        echo "" . $event_key_attraction_id . "";
    } else {
        $errors = array("error" => "yes", "message" => "Invalid Delete Query!");
        echo json_encode($errors);
    }
}

?>