<?php
include '../config/class.web.config.php';
$con = new Config();
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];

if ($verb == "GET") {
    $jsonArr = $con->SelectAll("venue", "", "", "json");
    $count = count(json_decode($jsonArr));
    if ($count >= 1) {
        echo "{\"data\":" . $jsonArr . "}";
    } else {
        echo "{\"data\":" . "[]" . "}";
    }
}
if ($verb == "DELETE") {

    $request_vars = array();
    parse_str(file_get_contents('php://input'), $request_vars);
    $errors = array();
    $venue_id = $request_vars["venue_id"];

    $delete_array = array("venue_id" => $venue_id);
    //$rs = $con->delete($object, $object_array, $return_type)
    $rs = $con->delete("venue", $delete_array,"array");
    

    if ($rs["output"] == "success") {
        echo "" . $venue_id . "";
    } else {
        $errors = array("error" => "yes", "message" => "Invalid Delete Query!");
        echo json_encode($errors);
    }
}
/*
select venue.venue_id as venue_id, venue.venue_name as venue_name,venue.venue_address as venue_address,
venue.venue_attraction as venue_attraction, venue.venue_googlemap as venue_googlemap,
venue.venue_total_seat as venue_total_seat, event.event_id as event_id, event.event_title as event_title from
venue inner join event on venue.event_id=event.event_id order by venue.venue_id ASC
*/
?>