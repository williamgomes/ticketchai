<?php
include '../config/class.web.config.php';
$con = new Config();
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];

if ($verb == "GET") {
    if (isset($_SESSION['user_type_id'])) {
        $user_id = $_SESSION['user_type_id'];
        //$query = "SELECT `event`.event_id, `event`.event_title, `event`.event_ticket_expair_date AS expiry_date,CASE `event`.featured WHEN 'true' THEN 'APPROVED' WHEN 'false' THEN 'PENDING...' ELSE 'NOT APPROVED' END as status, ( SELECT GROUP_CONCAT(category_name) FROM category AS ca INNER JOIN event_category AS ec ON ca.category_id = ec.category_id WHERE ec.event_id = `event`.event_id ) AS category_name, ( SELECT GROUP_CONCAT(ticket_name) FROM event_ticket_type AS tt INNER JOIN EVENT AS e ON tt.event_id = e.event_id WHERE e.event_id = `event`.event_id ) AS ticket_type, event_ticket_type.total_sold_ticket AS sold_ticket, event_ticket_type.ticket_qty AS total_ticket FROM `event` LEFT JOIN event_category ON `event`.event_id = event_category.event_id LEFT JOIN category ON event_category.category_id = category.category_id LEFT JOIN event_ticket_type ON EVENT .event_id = event_ticket_type.event_id WHERE `event`.event_created_by = '$user_id' GROUP BY `event`.event_id DESC";
        //$query = "SELECT `event`.event_id AS event_id, `event`.event_title AS event_title, `event`.event_ticket_expair_date AS expiry_date, event_category.event_category_id, event_category.category_id, category.category_id, category.category_name, event_ticket_type.event_ticket_type_id AS event_ticket_type_id, event_ticket_type.ticket_name AS ticket_type, event_ticket_type.ticket_qty AS total_ticket, event_ticket_type.total_sold_ticket AS sold_ticket FROM `event` LEFT JOIN event_category ON `event`.event_id = event_category.event_id LEFT JOIN category ON event_category.category_id = category.category_id LEFT JOIN event_ticket_type ON EVENT .event_id = event_ticket_type.event_id WHERE `event`.event_created_by = '$user_id' group by event.event_id";
        $query = "SELECT `event`.event_id, `event`.event_title, `event`.event_ticket_expair_date AS expiry_date, CASE WHEN `event`.featured = 'true' OR `event`.upcomming = 'true' THEN 'APPROVED' ELSE 'PENDING...' END AS `status`, ( SELECT GROUP_CONCAT(category_name) FROM category AS ca INNER JOIN event_category AS ec ON ca.category_id = ec.category_id WHERE ec.event_id = `event`.event_id ) AS category_name, ( SELECT GROUP_CONCAT(ticket_name) FROM event_ticket_type AS tt INNER JOIN event AS e ON tt.event_id = e.event_id WHERE e.event_id = `event`.event_id ) AS ticket_type, event_ticket_type.total_sold_ticket AS sold_ticket, event_ticket_type.ticket_qty AS total_ticket FROM `event` LEFT JOIN event_category ON `event`.event_id = event_category.event_id LEFT JOIN category ON event_category.category_id = category.category_id LEFT JOIN event_ticket_type ON event .event_id = event_ticket_type.event_id WHERE `event`.event_created_by = '$user_id' GROUP BY `event`.event_id DESC";
        $jsonArr = $con->ReturnObjectByQuery($query, "json");
        //$con->debug($jsonArr);
        $count = count(json_decode($jsonArr));
        if ($count >= 1) {
            echo "{\"data\":" . $jsonArr . "}";
        } else {
            echo "{\"data\":" . "[]" . "}";
        }
    }
}
