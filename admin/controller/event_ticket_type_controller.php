<?php

include '../config/class.web.config.php';
$con = new Config();
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];

if ($verb == "GET") {
    $g_event_id = $_GET["event_id"];
    $query = "SELECT CONCAT('Date-',event_schedule.event_date,' Start-',event_schedule.event_schedule_start_time,' End-',event_schedule.event_schedule_end_time) as event_schedule_name,
event_ticket_type.* FROM event_ticket_type 
INNER JOIN event_schedule ON event_ticket_type.event_schedule_id = event_schedule.event_schedule_id
WHERE event_ticket_type.event_id='$g_event_id' ORDER BY event_ticket_type_id DESC ";
    //$con->debug($query);
    $jsonArr = $con->ReturnObjectByQuery($query, "json");
    $count = count(json_decode($jsonArr));
    if ($count >= 1) {
        echo "{\"data\":" . $jsonArr . "}";
    } else {
        echo "{\"data\":" . "[]" . "}";
    }
}



if ($verb == "POST") {
    extract($_POST);

    unset($_POST["event_schedule_name"]);
    unset($_POST["event_id"]);

    $errors = array();
    $event_ticket_type_id = $_POST["event_ticket_type_id"];
    $event_ticket_schedule_id = $_POST["event_schedule_id"];
    $ticket_name = $_POST["ticket_name"];
    $ticket_qty = $_POST["ticket_qty"];
    $ticket_price = $_POST["ticket_price"];
    $per_user_limit = $_POST["per_user_limit"];
    $e_ticket = $_POST["e_ticket"];
    $p_ticket = $_POST["p_ticket"];
    $is_active = $_POST["is_active"];

    if ($e_ticket != "" && $p_ticket != "") {
        if ($e_ticket > $ticket_qty) {
            $errors = array("error" => "yes", "message" => "E-Ticket Is Higher Than This Schedule Total Ticket Quantity");
            echo json_encode($errors);
        } elseif ($p_ticket > $ticket_qty) {
            $errors = array("error" => "yes", "message" => "Printed Ticket Is Higher Than This Schedule Total Ticket Quantity");
            echo json_encode($errors);
        } elseif (($e_ticket + $p_ticket) > $ticket_qty) {
            $errors = array("error" => "yes", "message" => "This Schedule E-Ticket And Printed Ticket Exceeds Total Quantity ");
            echo json_encode($errors);
        } elseif (($e_ticket + $p_ticket) != $ticket_qty) {
            $errors = array("error" => "yes", "message" => "Distribute Total Ticket Properly");
            echo json_encode($errors);
        } else {
            $update_query = "UPDATE event_ticket_type SET";
            $update_query .=" event_ticket_type_id='" . mysql_real_escape_string($_POST["event_ticket_type_id"]) . "',";
            $update_query .=" event_schedule_id='" . mysql_real_escape_string($_POST["event_schedule_id"]) . "',";
            $update_query .=" ticket_name='" . mysql_real_escape_string($_POST["ticket_name"]) . "',";
            $update_query .=" ticket_qty='" . mysql_real_escape_string($_POST["ticket_qty"]) . "',";
            $update_query .=" ticket_price='" . mysql_real_escape_string($_POST["ticket_price"]) . "',";
            $update_query .=" per_user_limit='" . mysql_real_escape_string($_POST["per_user_limit"]) . "',";
            if ($_POST["e_ticket"] == 0) {
                $update_query .=" e_ticket=NULL,";
            } else {
                $update_query .=" e_ticket='" . mysql_real_escape_string($_POST["e_ticket"]) . "',";
            }

            if ($_POST["p_ticket"] == 0) {
                $update_query .=" p_ticket=NULL,";
            } else {
                $update_query .=" p_ticket='" . mysql_real_escape_string($_POST["p_ticket"]) . "',";
            }

            $update_query .=" is_active='" . mysql_real_escape_string($_POST["is_active"]) . "'";
            $update_query .=" WHERE event_ticket_type_id = '" . mysql_real_escape_string($_POST["event_ticket_type_id"]) . "'";
            
            $con->debug($update_query);
            $result = $con->QueryWithQueryString($update_query);
            $con->debug($result);
        }
    } else {
        $errors = array("error" => "yes", "message" => "Update Failed For Event Ticket Type");
        echo json_encode($errors);
    }
}


if ($verb == "PUT") {
    $request_vars = array();
    parse_str(file_get_contents('php://input'), $request_vars);

    array_shift($request_vars);
    $request_vars["event_id"] = $_GET["event_id"];
    $c_event_id = $_GET["event_id"];

    $event_id = $request_vars["event_id"];
    $event_ticket_schedule_id = $request_vars["event_schedule_id"];
    $ticket_name = $request_vars["ticket_name"];
    $ticket_qty = $request_vars["ticket_qty"];
    $ticket_price = $request_vars["ticket_price"];
    $per_user_limit = $request_vars["per_user_limit"];
    $e_ticket = $request_vars["e_ticket"];
    $p_ticket = $request_vars["p_ticket"];
    $is_active = $request_vars["is_active"];


    $CheckExists = $con->CheckExistsWithCondition("event_ticket_type", " ticket_name='$ticket_name' AND event_id='$event_id'");

    if ($CheckExists == 0) {
        if ($e_ticket != "" && $p_ticket != "" && $ticket_qty != "") {
            if ($e_ticket > $ticket_qty) {
                $errors = array("error" => "yes", "message" => "E-Ticket Is Higher Than This Schedule Total Ticket Quantity");
                echo json_encode($errors);
            } elseif ($p_ticket > $ticket_qty) {
                $errors = array("error" => "yes", "message" => "Printed Ticket Is Higher Than This Schedule Total Ticket Quantity");
                echo json_encode($errors);
            } elseif (($e_ticket + $p_ticket) > $ticket_qty) {
                $errors = array("error" => "yes", "message" => "This Schedule E-Ticket And Printed Ticket Exceeds Total Quantity ");
                echo json_encode($errors);
            } elseif (($e_ticket + $p_ticket) != $ticket_qty) {
                $errors = array("error" => "yes", "message" => "Distribute Total Ticket Properly");
                echo json_encode($errors);
            } else {

                $insert_query = "INSERT INTO event_ticket_type SET ";
                $insert_query .=" event_id='" . mysql_real_escape_string($request_vars["event_id"]) . "',";
                $insert_query .=" event_schedule_id='" . mysql_real_escape_string($request_vars["event_schedule_id"]) . "',";
                $insert_query .=" ticket_name='" . mysql_real_escape_string($request_vars["ticket_name"]) . "',";
                $insert_query .=" ticket_qty='" . mysql_real_escape_string($request_vars["ticket_qty"]) . "',";
                $insert_query .=" ticket_price='" . mysql_real_escape_string($request_vars["ticket_price"]) . "',";
                $insert_query .=" per_user_limit='" . mysql_real_escape_string($request_vars["per_user_limit"]) . "',";
                if ($request_vars["e_ticket"] == 0) {
                    $insert_query .=" e_ticket=NULL,";
                } else {
                    $insert_query .=" e_ticket='" . mysql_real_escape_string($request_vars["e_ticket"]) . "',";
                }

                if ($request_vars["p_ticket"] == 0) {
                    $insert_query .=" p_ticket=NULL,";
                } else {
                    $insert_query .=" p_ticket='" . mysql_real_escape_string($request_vars["p_ticket"]) . "',";
                }

                $insert_query .=" is_active='" . mysql_real_escape_string($request_vars["is_active"]) . "'";


                $result = $con->QueryWithQueryString($insert_query);


                if ($result["output"] == "error") {
                    $errors = array("error" => "yes", "message" => $result["msg"]);
                    echo json_encode($errors);
                } else if ($result["output"] == "success") {
                    if ($result["result"]) {
                        echo "" . $result["result"] . "";
                    } else {
                        $errors = array("error" => "yes", "message" => "Invalid Insertion Query");
                        echo json_encode($errors);
                    }
                }
            }
        }
    } else {
        $errors = array("error" => "yes", "message" => "Ticket Type Already Exists For This Event");
        echo json_encode($errors);
    }
}
if ($verb == "DELETE") {

    $request_vars = array();
    parse_str(file_get_contents('php://input'), $request_vars);
    $errors = array();
    $event_ticket_type_id = $request_vars["event_ticket_type_id"];

    $delete_array = array("event_ticket_type_id" => $event_ticket_type_id);
//$rs = $con->delete($object, $object_array, $return_type)
    $rs = $con->delete("event_ticket_type", $delete_array, "array");


    if ($rs["output"] == "success") {
        echo "" . $event_ticket_type_id . "";
    } else {
        $errors = array("error" => "yes", "message" => "Invalid Delete Query!");
        echo json_encode($errors);
    }
}
?>