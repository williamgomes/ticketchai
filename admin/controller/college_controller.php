<?php
include '../config/class.web.config.php';
$con = new Config();
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];

if ($verb == "GET") {
    $jsonArr = $con->SelectAll("college", "", "", "json");
    $count = count(json_decode($jsonArr));
    if ($count >= 1) {
        echo "{\"data\":" . $jsonArr . "}";
    } else {
        echo "{\"data\":" . "[]" . "}";
    }
}
if ($verb == "POST") {
    extract($_POST);
    //array_push($_POST, "Error_Flag");
    $errors = array();
    $result= $con->update("college", $_POST, "college_name","", "array");
    if ($result["output"] == "error") {
        $errors = array("error" => "yes", "message" => $result["msg"]);
        echo json_encode($errors);
    } 
    else if($result["output"]=="success")
    {
        if ($result["result"]) {
            echo json_encode($result["result"]);
        } else {
            $errors = array("error" => "yes", "message" => "Update failed for College Name: " .$_POST["college_name"] );
            echo json_encode($errors);
        }
    }
}
if ($verb == "PUT") {
    $request_vars = array();
    parse_str(file_get_contents('php://input'), $request_vars);
    array_shift($request_vars);
    //array_push($request_vars, "flag");

    $result = $con->insert("college", $request_vars, "college_name","","array");

    if ($result["output"] == "error") {
        $errors = array("error" => "yes", "message" => $result["msg"]);
        echo json_encode($errors);
    } else if ($result["output"] == "success") {
        if ($result["result"]) {
            echo "" . $result["result"] . "";
        } else {
            $errors = array("error" => "yes", "message" => "Invalid Insertion Query");
            echo json_encode($errors);
        }
    }
}
if ($verb == "DELETE") {

    $request_vars = array();
    parse_str(file_get_contents('php://input'), $request_vars);
    $errors = array();
    $college_id = $request_vars["college_id"];

    $delete_array = array("college_id" => $college_id);
    //$rs = $con->delete($object, $object_array, $return_type)
    $rs = $con->delete("college", $delete_array,"array");
    

    if ($rs["output"] == "success") {
        echo "" . $college_id . "";
    } else {
        $errors = array("error" => "yes", "message" => "Invalid Delete Query!");
        echo json_encode($errors);
    }
}
?>