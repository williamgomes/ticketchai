
<?php

include '../config/class.web.config.php';

$con = new Config();

header("Content-type: application/json");

$verb = $_SERVER["REQUEST_METHOD"];

if ($verb == "GET") {

    $arr = array();

    //$rs = "SELECT booking.booking_id,booking.event_id,booking.online_pay_method, booking.customer_id, booking.payment_status, booking.delivery_method, booking.delivery_address, booking.delivery_phone_number,customer.email, booking.anual_subsciption_cost, booking.enrollment_fee, booking.ticket_price, booking.total_amount, booking.tocken, ( SELECT CONCAT(first_name, ' ', last_name) FROM customer WHERE booking.customer_id = customer.customer_id ) AS customer_name FROM booking LEFT JOIN customer ON booking.customer_id = customer.customer_id ORDER BY booking.booking_id DESC";
    $rs = "SELECT booking.booking_id,booking.event_id,booking.schedule_id,booking.life_time_subsciption_cost, booking.customer_id, booking.payment_status, booking.delivery_method, booking.delivery_address, booking.delivery_phone_number, booking.anual_subsciption_cost, booking.enrollment_fee, booking.ticket_price, booking.total_amount, booking.tocken, customer.email, CASE online_pay_method WHEN 'home' THEN 'Home' ELSE CASE online_pay_method WHEN 'collect' THEN 'Venue' ELSE 'TicketChai Office' END END AS online_pay_method, ( SELECT CONCAT(first_name, ' ', last_name) FROM customer WHERE booking.customer_id = customer.customer_id ) AS customer_name FROM booking LEFT JOIN customer ON booking.customer_id = customer.customer_id WHERE booking.payment_status != 'delete' ORDER BY booking.booking_id DESC";
    $jsonArr = $con->ReturnObjectByQuery($rs, "json");

    $count = count($jsonArr);

    if ($count >= 1) {

        echo "{\"data\":" . $jsonArr . "}";

    } else {

        echo "{\"data\":" . "[]" . "}";

    }

}

?>