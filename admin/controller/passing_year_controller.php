<?php
include '../config/class.web.config.php';
$con = new Config();
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];

if ($verb == "GET") {
    $jsonArr = $con->SelectAll("year", "", "", "json");
    $count = count(json_decode($jsonArr));
    if ($count >= 1) {
        echo "{\"data\":" . $jsonArr . "}";
    } else {
        echo "{\"data\":" . "[]" . "}";
    }
}
if ($verb == "POST") {
    extract($_POST);
    //array_push($_POST, "Error_Flag");
    $errors = array();
    $result= $con->update("year", $_POST, "year_name","", "array");
    if ($result["output"] == "error") {
        $errors = array("error" => "yes", "message" => $result["msg"]);
        echo json_encode($errors);
    } 
    else if($result["output"]=="success")
    {
        if ($result["result"]) {
            echo json_encode($result["result"]);
        } else {
            $errors = array("error" => "yes", "message" => "Update failed for Year Name: " .$_POST["year_name"] );
            echo json_encode($errors);
        }
    }
}
if ($verb == "PUT") {
    $request_vars = array();
    parse_str(file_get_contents('php://input'), $request_vars);
    array_shift($request_vars);
    //array_push($request_vars, "flag");

    $result = $con->insert("year", $request_vars, "year_name","","array");

    if ($result["output"] == "error") {
        $errors = array("error" => "yes", "message" => $result["msg"]);
        echo json_encode($errors);
    } else if ($result["output"] == "success") {
        if ($result["result"]) {
            echo "" . $result["result"] . "";
        } else {
            $errors = array("error" => "yes", "message" => "Invalid Insertion Query");
            echo json_encode($errors);
        }
    }
}
if ($verb == "DELETE") {

    $request_vars = array();
    parse_str(file_get_contents('php://input'), $request_vars);
    $errors = array();
    $year_id = $request_vars["year_id"];

    $delete_array = array("year_id" => $year_id);
    //$rs = $con->delete($object, $object_array, $return_type)
    $rs = $con->delete("year", $delete_array,"array");
    

    if ($rs["output"] == "success") {
        echo "" . $year_id . "";
    } else {
        $errors = array("error" => "yes", "message" => "Invalid Delete Query!");
        echo json_encode($errors);
    }
}
?>

