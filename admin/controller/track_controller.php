<?php

include '../config/class.web.config.php';
$con = new Config();
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];
if ($verb == "GET") {
    $arr = array();
    $rs = "select concat(c.first_name,' ',c.last_name) as customer_full_name,
           c.*, 
	   b.*,
           IFNULL(b.delivery_address , c.address) AS booking_delivery_address,
     e.event_title,
     cv.batch_id,
     cv.student_number
from 
 booking as b,
 customer as c,
 event as e,
 college_validation as cv
WHERE 
   b.customer_id = c.customer_id
   AND e.event_id = b.event_id
   AND b.cadet_id = cv.college_validation_id
   ORDER BY booking_id DESC";
    $jsonArr = $con->ReturnObjectByQuery($rs, "json");
    $count = count($jsonArr);
    if ($count >= 1) {
        echo "{\"data\":" . $jsonArr . "}";
    } else {
        echo "{\"data\":" . "[]" . "}";
    }
}
?>