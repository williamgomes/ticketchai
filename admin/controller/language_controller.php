<?php
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];

if ($verb == "GET") {

    $arr = array();
    $arr[0] = array("language_id" => utf8_encode(1), "language_name" => utf8_encode("Bangla"));
    $arr[1] = array("language_id" => utf8_encode(2), "language_name" => utf8_encode("Hindi"));
    $arr[2] = array("language_id" => utf8_encode(3), "language_name" => utf8_encode("English"));
    $jsonArr = json_encode($arr);

    echo "{\"data\":" . $jsonArr . "}";
}
