<?php
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];

if ($verb == "GET") {

    $arr = array();
    $arr[0] = array("gender_id" => utf8_encode(1), "gender_name" => utf8_encode( "Male"));
    $arr[1] = array("gender_id" => utf8_encode(2), "gender_name" => utf8_encode("Female"));
    $arr[2] = array("gender_id" => utf8_encode(3), "gender_name" => utf8_encode( "Others"));
    $jsonArr = json_encode($arr);

    echo "{\"data\":" . $jsonArr . "}";
}
