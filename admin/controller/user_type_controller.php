<?php
include '../config/class.web.config.php';
$con = new Config();
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];

if ($verb == "GET") {
   
    $jsonArr = $con->SelectAll("user_type", "", "", "json");
    $count = count(json_decode($jsonArr));
    if ($count >= 1) {
        echo "{\"data\":" . $jsonArr . "}";
    } else {
        echo "{\"data\":" . "[]" . "}";
    }
}
if ($verb == "POST") {
    extract($_POST);
    //array_push($_POST, "Error_Flag");
    /* unset unnecessary element of array */
    unset($_POST["updated_date"]);
    unset($_POST["updated_by"]);
    /* unset unnecessary element of array */
    $errors = array();
    $result= $con->update("user_type", $_POST, "user_type_name","", "array");
    if ($result["output"] == "error") {
        $errors = array("error" => "yes", "message" => $result["msg"]);
        echo json_encode($errors);
    } 
    else if($result["output"]=="success")
    {
        if ($result["result"]) {
            echo json_encode($result["result"]);
        } else {
            $errors = array("error" => "yes", "message" => "Update failed for User Type Nmae: " .$_POST["user_type_name"]);
            echo json_encode($errors);
        }
    }
}
if ($verb == "PUT") {
    $request_vars = array();
    parse_str(file_get_contents('php://input'), $request_vars);
    array_shift($request_vars);
    
    /* unset unnecessary variable */
    unset($request_vars["updated_date"]);
    /* unset unnecessary variable */
    //array_push($request_vars, "flag");
    //$con->debug($request_vars);
    //exit();
   //$con->inser
    $result = $con->insert("user_type", $request_vars, "user_type_name","", "array");

    if ($result["output"] == "error") {
        $errors = array("error" => "yes", "message" => $result["msg"]);
        echo json_encode($errors);
    } else if ($result["output"] == "success") {
        if ($result["result"]) {
            echo "" . $result["result"] . "";
        } else {
            $errors = array("error" => "yes", "message" => "Invalid Insertion Query");
            echo json_encode($errors);
        }
    }
}
if ($verb == "DELETE") {

    $request_vars = array();
    parse_str(file_get_contents('php://input'), $request_vars);
    $errors = array();
    $user_type_id = $request_vars["user_type_id"];

    $delete_array = array("user_type_id" => $user_type_id);
    //$rs = $con->delete($object, $object_array, $return_type)
    $rs = $con->delete("user_type", $delete_array,"array");
    

    if ($rs["output"] == "success") {
        echo "" . $user_type_id . "";
    } else {
        $errors = array("error" => "yes", "message" => "Invalid Delete Query!");
        echo json_encode($errors);
    }
}
?>
