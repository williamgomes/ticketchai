<?php
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];
if ($verb == "GET") {
    $arr = array();
    $arr[0] = array("year_id" => utf8_encode(1), "year_name" => utf8_encode("2014"));
    $arr[1] = array("year_id" => utf8_encode(2), "year_name" => utf8_encode("2015"));
    $arr[2] = array("year_id" => utf8_encode(3), "year_name" => utf8_encode("2016"));
    $arr[3] = array("year_id" => utf8_encode(4), "year_name" => utf8_encode("2017"));
    $arr[4] = array("year_id" => utf8_encode(5), "year_name" => utf8_encode("2018"));
    $arr[5] = array("year_id" => utf8_encode(6), "year_name" => utf8_encode("2019"));
    $arr[6] = array("year_id" => utf8_encode(7), "year_name" => utf8_encode("2020"));
    $arr[7] = array("year_id" => utf8_encode(8), "year_name" => utf8_encode("2021"));
    $jsonArr = json_encode($arr);
    echo "{\"data\":" . $jsonArr . "}";
}