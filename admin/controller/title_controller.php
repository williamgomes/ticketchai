<?php
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];

if ($verb == "GET") {
    $arr = array();  
    $arr[0] = array("title_id" => utf8_encode(1), "title_name" => utf8_encode("Mr"));
    $arr[1] = array("title_id" => utf8_encode(2), "title_name" => utf8_encode("Miss"));
    $arr[2] = array("title_id" => utf8_encode(3), "title_name" => utf8_encode("Ms"));
    $arr[3] = array("title_id" => utf8_encode(4), "title_name" => utf8_encode("Mrs"));
    $jsonArr = json_encode($arr);
    echo "{\"data\":" . $jsonArr . "}";
}