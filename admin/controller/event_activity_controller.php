<?php

include '../config/class.web.config.php';
$con = new Config();

header("Content-type: application/json");

$verb = $_SERVER["REQUEST_METHOD"];


if ($verb == "GET") {
    $g_event_id = $_GET["event_id"];
    $query = "SELECT event_activity.*,activity.activity_id,activity.activity_name FROM event_activity  INNER JOIN activity ON event_activity.activity_id = activity.activity_id WHERE event_id = '$g_event_id'";
    $jsonArr = $con->ReturnObjectByQuery($query, "array");
    $returnarray = array();
    foreach ($jsonArr as $js) {
        $temp_array = array();
        $temp_array["activity_id"]=$js->activity_id;
        $temp_array["event_activity_id"] = $js->event_activity_id;
        $temp_array["event_id"] = $js->event_id;
        $temp_array["activity_name"] = $js->activity_name;
        $temp_array["event_activity_title"] = $js->event_activity_title;
        $temp_array["image"] = $js->image;
        $temp_array["event_activity_details"] = html_entity_decode(html_entity_decode($js->event_activity_details, ENT_COMPAT, 'UTF-8'));
        $temp_array["is_active"] = $js->is_active;

        array_push($returnarray, $temp_array);
    }
    $json_return_array = json_encode($returnarray);


    $count = count(json_decode($json_return_array));
    if ($count >= 1) {
        echo "{\"data\":" . $json_return_array . "}";
    } else {
        echo "{\"data\":" . "[]" . "}";
    }
}


if ($verb == "PUT") {

    $request_vars = array();

    parse_str(file_get_contents('php://input'), $request_vars);

    array_shift($request_vars);

    //array_push($request_vars, "flag");

    $request_vars["event_id"] = $_GET["event_id"];
    $c_event_id = $_GET["event_id"];
    $event_id = $request_vars["event_id"];
    $activity_id = $request_vars["activity_id"];
    $event_activity_title = $request_vars["event_activity_title"];
    $image = $request_vars["image"];
    $details = htmlentities($request_vars["event_activity_details"]);
    $is_active = $request_vars["is_active"];

    $insert_array = array(
        "event_id" => $event_id,
        "activity_id" => $activity_id,
        "event_activity_title" => $event_activity_title,
        "event_activity_details" => $details,
        "image" => $image,
        "is_active" => $is_active
    );

    $result = $con->insert("event_activity", $insert_array, "", "", "array");
    if ($result["output"] == "error") {

        $errors = array("error" => "yes", "message" => $result["msg"]);

        echo json_encode($errors);
    } else if ($result["output"] == "success") {

        if ($result["result"]) {

            echo "" . $result["result"] . "";
        } else {

            $errors = array("error" => "yes", "message" => "Invalid Insertion Query");

            echo json_encode($errors);
        }
    }
}

if ($verb == "POST") {

    extract($_POST);

    //array_push($_POST, "Error_Flag");
    unset($_POST["activity_name"]);
    unset($_POST["event_id"]);

    $errors = array();

    $event_activity_id = $_POST["event_activity_id"];
    $activity_id = $_POST["activity_id"];
    $event_activity_title = $_POST["event_activity_title"];
    $event_activity_details = $_POST["event_activity_details"];
    $image = $_POST["image"];
    $is_active = $_POST["is_active"];

    $update_event_activity = array(
        "event_activity_id" => $event_activity_id,
        "activity_id" => $activity_id,
        "event_activity_title" => $event_activity_title,
        "event_activity_details" => $event_activity_details,
        "image" => $image,
        "is_active" => $is_active
    );
    $result = $con->update("event_activity", $update_event_activity, "event_activity_id", "", "array");

    if ($result["output"] == "error") {

        $errors = array("error" => "yes", "message" => $result["msg"]);

        echo json_encode($errors);
    } else if ($result["output"] == "success") {

        if ($result["result"]) {

            echo json_encode($result["result"]);
        } else {

            $errors = array("error" => "yes", "message" => "Update failed for Event Activity ID: " . $_POST["event_activity_id"]);

            echo json_encode($errors);
        }
    }
}


if ($verb == "DELETE") {

    $request_vars = array();
    parse_str(file_get_contents('php://input'), $request_vars);
    $errors = array();
    $event_activity_id = $request_vars["event_activity_id"];
    $delete_array = array("event_activity_id" => $event_activity_id);
    $rs = $con->delete("event_activity", $delete_array, "array");


    if ($rs["output"] == "success") {
        echo "" . $event_activity_id . "";
    } else {
        $errors = array("error" => "yes", "message" => "Invalid Delete Query!");
        echo json_encode($errors);
    }
}
?>