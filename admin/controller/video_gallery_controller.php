<?php

include '../config/class.web.config.php';
$con = new Config();

header("Content-type: application/json");

$verb = $_SERVER["REQUEST_METHOD"];


if ($verb == "GET") {
    $g_event_id = $_GET["event_id"];
    $query = "SELECT * FROM video_gallery WHERE event_id = '$g_event_id'";
    $jsonArr = $con->ReturnObjectByQuery($query, "json");
    $count = count(json_decode($jsonArr));
    if ($count >= 1) {
        echo "{\"data\":" . $jsonArr . "}";
    } else {
        echo "{\"data\":" . "[]" . "}";
    }
}


if ($verb == "PUT") {

    $request_vars = array();

    parse_str(file_get_contents('php://input'), $request_vars);

    array_shift($request_vars);

    //array_push($request_vars, "flag");

    $request_vars["event_id"] = $_GET["event_id"];
    $c_event_id = $_GET["event_id"];
    $event_id = $request_vars["event_id"];
    //$link = $request_vars["link"];
    $is_active = $request_vars["is_active"];

    if (isset($request_vars["link"]) === true) {

        $url = $request_vars["link"];


        if (filter_var($url, FILTER_VALIDATE_URL) == true) {


            $insert_array = array(
                "event_id" => $event_id,
                "link" => $url,
                "is_active" => $is_active
            );
            $result = $con->insert("video_gallery", $insert_array, "", "", "array");
            if ($result["output"] == "error") {

                $errors = array("error" => "yes", "message" => $result["msg"]);

                echo json_encode($errors);
            } else if ($result["output"] == "success") {

                if ($result["result"]) {

                    echo "" . $result["result"] . "";
                } else {

                    $errors = array("error" => "yes", "message" => "Invalid Insertion Query");

                    echo json_encode($errors);
                }
            }
        } else {
            $errors = array("error" => "yes", "message" => "Invalid Video Link");
            echo json_encode($errors);
        }
    }
}

if ($verb == "POST") {

    extract($_POST);

    //array_push($_POST, "Error_Flag");
    unset($_POST["event_id"]);
    $errors = array();

    $video_gallery_id = $_POST["video_gallery_id"];
    $is_active = $_POST["is_active"];
    //$link = $_POST["link"];
    //------- Checking Update Video Link ---------------//
    if (isset($_POST["link"])) {

        $url = $_POST["link"];
        echo filter_var($url, FILTER_VALIDATE_URL);

        if (filter_var($url, FILTER_VALIDATE_URL) == true) {

            $update_array = array(
                "video_gallery_id" => $video_gallery_id,
                "link" => $url,
                "is_active" => $is_active
            );

            $result = $con->update("video_gallery", $update_array, "video_gallery_id", "", "array");

            if ($result["output"] == "error") {

                $errors = array("error" => "yes", "message" => $result["msg"]);

                echo json_encode($errors);
            } else if ($result["output"] == "success") {

                if ($result["result"]) {

                    echo json_encode($result["result"]);
                } else {

                    $errors = array("error" => "yes", "message" => "Update failed for Video Gallery ID: " . $_POST["video_gallery_id"]);

                    echo json_encode($errors);
                }
            }
        } else {
            $errors = array("error" => "yes", "message" => "Invalid Video Link");
            echo json_encode($errors);
        }
    } else {
        $errors = array("error" => "yes", "message" => "Invalid Video Link");
        echo json_encode($errors);
    }
}

if ($verb == "DELETE") {

    $request_vars = array();
    parse_str(file_get_contents('php://input'), $request_vars);
    $errors = array();
    $video_gallery_id = $request_vars["video_gallery_id"];
    $delete_array = array("video_gallery_id" => $video_gallery_id);
    $rs = $con->delete("video_gallery", $delete_array, "array");


    if ($rs["output"] == "success") {
        echo "" . $video_gallery_id . "";
    } else {
        $errors = array("error" => "yes", "message" => "Invalid Delete Query!");
        echo json_encode($errors);
    }
}
?>