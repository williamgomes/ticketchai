<?php

include '../config/class.web.config.php';
$con = new Config();
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];

if ($verb == "GET") {
    $queryString = "";
    if (isset($_GET["sCat"])) {
        if ($_GET["sCat"] == 1) {
            $queryString = "SELECT cat1.category_id,cat1.category_name,cat2.category_id as parent_category_id,cat2.category_name as parent_category_name,cat1.is_active
    FROM
    category as cat1 left join
    category as cat2 ON cat1.parent_category_id = cat2.category_id WHERE cat1.category_id NOT IN(2,4)";
        } else if ($_GET["sCat"] == 2) {
            $queryString = "SELECT cat1.category_id,cat1.category_name,cat2.category_id as parent_category_id,cat2.category_name as parent_category_name,cat1.is_active
    FROM
    category as cat1 left join
    category as cat2 ON cat1.parent_category_id = cat2.category_id WHERE cat1.category_id IN(2,4)";
        }
    } else {

        $queryString = "SELECT cat1.category_id,cat1.category_name,cat2.category_id as parent_category_id,cat2.category_name as parent_category_name,cat1.is_active
    FROM
    category as cat1 left join
    category as cat2 ON cat1.parent_category_id = cat2.category_id";
    }
//     $queryString = "SELECT cat1.category_id, cat1.category_name, cat1.parent_category_id,
//      (select category_name from category cat2 where cat2.category_id = cat1.parent_category_id group by cat2.parent_category_id) as `parent_category_name`
//      FROM category cat1" ;



    $jsonArr = $con->ReturnObjectByQuery($queryString, "json");

    // $jsonArr = $con->SelectAll("country", "", "", "json");
    $count = count(json_decode($jsonArr));
    if ($count >= 1) {
        echo "{\"data\":" . $jsonArr . "}";
    } else {
        echo "{\"data\":" . "[]" . "}";
    }
}
if ($verb == "POST") {
    extract($_POST);
    //parse_str(file_get_contents('php://input'), $request_vars);
    //array_push($_POST, "Error_Flag");
    //$con->debug($request_vars);
    //exit();
    //$con->debug($_POST);
    unset($_POST["parent_category_name"]);
    $category_name = $_POST["category_name"];
    $parent_category_id = $_POST["parent_category_id"];
    $errors = array();
    if (empty($parent_category_id)) {
        unset($_POST["parent_category_id"]);
        $Checkduplicate = $con->CheckExistsWithCondition("category", " category_name='$category_name'");
        if ($Checkduplicate >= 1) {
            $update_array = array("category_id" => $_POST["category_id"], "is_active" => $_POST["is_active"]);
            $result = $con->update("category", $update_array, "", "", "array");
            if ($result["output"] == "error") {
                $errors = array("error" => "yes", "message" => $result["msg"]);
                echo json_encode($errors);
            } else if ($result["output"] == "success") {
                if ($result["result"]) {
                    echo json_encode($result["result"]);
                } else {
                    $errors = array("error" => "yes", "message" => "Update failed for category combination");
                    echo json_encode($errors);
                }
            }
        } else {
            $update_array = array("category_id" => $_POST["category_id"], "category_name" => $_POST["category_name"], "is_active" => $_POST["is_active"]);
            $result = $con->update("category", $update_array, "", "", "array");
            if ($result["output"] == "error") {
                $errors = array("error" => "yes", "message" => $result["msg"]);
                echo json_encode($errors);
            } else if ($result["output"] == "success") {
                if ($result["result"]) {
                    echo json_encode($result["result"]);
                } else {
                    $errors = array("error" => "yes", "message" => "Update failed for category combination");
                    echo json_encode($errors);
                }
            }
        }
    } else {
        $parent_category_id = $_POST["parent_category_id"];
        $CheckCategoryWithId = $con->CheckExistsWithCondition("category", " category_name='$category_name' AND parent_category_id='$parent_category_id'");
        if ($CheckCategoryWithId >= 1) {
            $update_post_array = array("category_id" => $_POST["category_id"], "is_active" => $_POST["is_active"]);
            $result = $con->update("category", $update_post_array, "", "", "array");
            if ($result["output"] == "error") {
                $errors = array("error" => "yes", "message" => $result["msg"]);
                echo json_encode($errors);
            } else if ($result["output"] == "success") {
                if ($result["result"]) {
                    echo json_encode($result["result"]);
                } else {
                    $errors = array("error" => "yes", "message" => "Update failed for category combination");
                    echo json_encode($errors);
                }
            }
        } else {
            $xupdate_post_array = array("category_id" => $_POST["category_id"], "category_name" => $_POST["category_name"], "parent_category_id" => $_POST["parent_category_id"], "is_active" => $_POST["is_active"]);
            $xresult = $con->update("category", $xupdate_post_array, "", "", "array");
            if ($xresult["output"] == "error") {
                $errors = array("error" => "yes", "message" => $xresult["msg"]);
                echo json_encode($errors);
            } else if ($xresult["output"] == "success") {
                if ($xresult["result"]) {
                    echo json_encode($xresult["result"]);
                } else {
                    $errors = array("error" => "yes", "message" => "Update failed for category combination");
                    echo json_encode($errors);
                }
            }
        }
    }
}
if ($verb == "PUT") {
    $request_vars = array();
    parse_str(file_get_contents('php://input'), $request_vars);
    array_shift($request_vars);
    //array_push($request_vars, "flag");
    /*   unset unnecessary variable         */
    unset($request_vars["parent_category_name"]);
    /*   unset unnecessary variable         */
    $category_name = $request_vars["category_name"];

    $parent_category_id = $request_vars["parent_category_id"];
    $result = array();
    if (empty($parent_category_id)) {
        unset($request_vars["parent_category_id"]);
        $result = $con->insert("category", $request_vars, "", " category_name='$category_name'", "array");
    } else {
        $result = $con->insert("category", $request_vars, "", " category_name='$category_name' AND parent_category_id='$parent_category_id'", "array");
    }
    //$con->debug($parent_category_id);
    if ($result["output"] == "error") {
        $errors = array("error" => "yes", "message" => $result["msg"]);
        echo json_encode($errors);
    } else if ($result["output"] == "success") {
        if ($result["result"]) {
            echo "" . $result["result"] . "";
        } else {
            $errors = array("error" => "yes", "message" => "Invalid Insertion Query");
            echo json_encode($errors);
        }
    }
}
if ($verb == "DELETE") {

    $request_vars = array();
    parse_str(file_get_contents('php://input'), $request_vars);
    $errors = array();
    $country_id = $request_vars["country_id"];

    $delete_array = array("country_id" => $country_id);
    //$rs = $con->delete($object, $object_array, $return_type)
    $rs = $con->delete("country", $delete_array, "array");


    if ($rs["output"] == "success") {
        echo "" . $country_id . "";
    } else {
        $errors = array("error" => "yes", "message" => "Invalid Delete Query!");
        echo json_encode($errors);
    }
}
?>