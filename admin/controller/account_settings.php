<?php

include("../config/class.web.config.php");
$con = new Config();

$email = $_SESSION['admin_email_address'];
$user_id = $_SESSION['user_type_id'];

$admin_first_name = $_POST["admin_first_name"];
$admin_last_name = $_POST["admin_last_name"];
$admin_mobile_number = $_POST["admin_mobile_number"];
$admin_address = $_POST["admin_address"];

$update_array = array(
    "admin_email_address" => $email,
    "admin_first_name" => $admin_first_name,
    "admin_last_name" => $admin_last_name,
    "admin_mobile_number" => $admin_mobile_number,
    "admin_address" => $admin_address
);
$update_admin = $con->Update("admin", $update_array, "", "", "array");
//$con->debug($update_admin);
if ($update_admin["output"] == "success") {

    $return_array = array("output" => "success", "msg" => $update_admin["msg"]);
    echo json_encode($return_array);
    exit();
} else {
    $return_array = array("output" => "error", "Admin is not update properly");
    echo json_encode($return_array);
    exit();
}
//$update_admin = $con->Update("admin", array("admin_email_address" => $email), "", "", "array");
?>


