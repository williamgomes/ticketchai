<?php
include '../config/class.web.config.php';
$con = new Config();
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];

if ($verb == "GET") {
    
    $arr = array();    
    $table_array= array("vendor","organization");
    $field_array= array("vendor"=>"vendor_id,vendor_name,vendor_phone,vendor_email,vendor_address","organization"=>"organization_id,organization_name");
    $jsonArr = $con->one_many_relation($table_array, $field_array, "", "json");
    $count = count(json_decode($jsonArr));
     if ($count >= 1) {
        echo "{\"data\":" . $jsonArr . "}";
    } else {
        echo "{\"data\":" . "[]" . "}";
    }
}
?>