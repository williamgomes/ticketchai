<?php

include '../config/class.web.config.php';

$con = new Config();

header("Content-type: application/json");

$verb = $_SERVER["REQUEST_METHOD"];


if ($verb == "GET") {
    $g_event_id = $_GET["event_id"];
    $query = "SELECT * FROM event_faq WHERE event_id ='$g_event_id' order by faq_id DESC";
    $jsonArr = $con->ReturnObjectByQuery($query,"json");
   
    $count = count($jsonArr);
    if ($count >= 1) {
        echo "{\"data\":" . $jsonArr . "}";
    } else {
        echo "{\"data\":" . "[]" . "}";
    }
}

if ($verb == "POST") {
    extract($_POST);
    
    $errors = array();
   
    $result = $con->update("event_faq", $_POST, "faq_id", "", "array");
    if ($result["output"] == "error") {
        $errors = array("error" => "yes", "message" => $result["msg"]);
        echo json_encode($errors);
    } else if ($result["output"] == "success") {
        if ($result["result"]) {
            echo json_encode($result["result"]);
        } else {
            $errors = array("error" => "yes", "message" => "Update failed for FAQ ID: " . $_POST["faq_id"]);
            echo json_encode($errors);
        }
    }
}


if ($verb == "PUT") {
    $request_vars = array();
    parse_str(file_get_contents('php://input'), $request_vars);

    
    $request_vars["event_id"] = $_GET["event_id"];
    //array_push($request_vars, $request_vars["event_id"]);
    //$con->debug($request_vars);
    $c_event_id = $_GET["event_id"];
    $question = $request_vars["question"];
    $answer = $request_vars["answer"];
    $is_active = $request_vars["is_active"];
    
    $ques = htmlentities($question, ENT_COMPAT, 'UTF-8');
    $ans = htmlentities($answer, ENT_COMPAT, 'UTF-8');
   
    $CheckExists = $con->CheckExistsWithCondition("event_faq", "question='$question' AND event_id='$c_event_id'");

    if ($CheckExists == 0) {

        $insert_array = array(
            "event_id" => $c_event_id,
            "question" => $question,
            "answer" => $answer,
            "is_active" => $is_active
        );
        $result = $con->insert("event_faq", $insert_array, "", "", "array");

        if ($result["output"] == "error") {
            $errors = array("error" => "yes", "message" => $result["msg"]);
            echo json_encode($errors);
        } else if ($result["output"] == "success") {
            if ($result["result"]) {
                echo "" . $result["result"] . "";
                $errors = array("error" => "yes", "message" => "Faq saved Successfully");
        echo json_encode($errors);
            } else {
                $errors = array("error" => "yes", "message" => "Invalid Insertion Query");
                echo json_encode($errors);
            }
        }
    } else {
        $errors = array("error" => "yes", "message" => "Question Already Exists For This Event");
        echo json_encode($errors);
    }
}



if ($verb == "DELETE") {

    $request_vars = array();
    parse_str(file_get_contents('php://input'), $request_vars);
    $errors = array();
    $faq_id = $request_vars["faq_id"];

    $delete_array = array("faq_id" => $faq_id);

    $rs = $con->delete("event_faq", $delete_array, "array");


    if ($rs["output"] == "success") {
        echo "" . $faq_id . "";
    } else {
        $errors = array("error" => "yes", "message" => "Invalid Delete Query!");
        echo json_encode($errors);
    }
}
?>



