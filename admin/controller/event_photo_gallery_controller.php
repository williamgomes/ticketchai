<?php

include '../config/class.web.config.php';
$con = new Config();

header("Content-type: application/json");

$verb = $_SERVER["REQUEST_METHOD"];


if ($verb == "GET") {
    $g_event_id = $_GET["event_id"];
    $query = "SELECT * FROM photo_gallery WHERE event_id = '$g_event_id'";
    $jsonArr = $con->ReturnObjectByQuery($query, "json");
    $count = count(json_decode($jsonArr));
    if ($count >= 1) {
        echo "{\"data\":" . $jsonArr . "}";
    } else {
        echo "{\"data\":" . "[]" . "}";
    }
}


if ($verb == "PUT") {

    $request_vars = array();

    parse_str(file_get_contents('php://input'), $request_vars);

    array_shift($request_vars);

    //array_push($request_vars, "flag");

    $request_vars["event_id"] = $_GET["event_id"];
    $c_event_id = $_GET["event_id"];
    $event_id = $request_vars["event_id"];
    $photo_name = $request_vars["photo_name"];
    $is_active = $request_vars["is_active"];

    $insert_array = array(
        "event_id" => $event_id,
        "photo_name" => $photo_name,
        "is_active" => $is_active
    );

    $result = $con->insert("photo_gallery", $insert_array, "", "", "array");
    if ($result["output"] == "error") {

        $errors = array("error" => "yes", "message" => $result["msg"]);

        echo json_encode($errors);
    } else if ($result["output"] == "success") {

        if ($result["result"]) {

            echo "" . $result["result"] . "";
        } else {

            $errors = array("error" => "yes", "message" => "Invalid Insertion Query");

            echo json_encode($errors);
        }
    }
}

if ($verb == "POST") {

    extract($_POST);

    //array_push($_POST, "Error_Flag");

    $errors = array();

    $result = $con->update("photo_gallery", $_POST, "photo_gallery_id", "", "array");

    if ($result["output"] == "error") {

        $errors = array("error" => "yes", "message" => $result["msg"]);

        echo json_encode($errors);
    } else if ($result["output"] == "success") {

        if ($result["result"]) {

            echo json_encode($result["result"]);
        } else {

            $errors = array("error" => "yes", "message" => "Update failed for Photo Gallery ID: " . $_POST["photo_gallery_id"]);

            echo json_encode($errors);
        }
    }
}

if ($verb == "DELETE") {

    $request_vars = array();
    parse_str(file_get_contents('php://input'), $request_vars);
    $errors = array();
    $photo_gallery_id = $request_vars["photo_gallery_id"];
    $delete_array = array("photo_gallery_id" => $photo_gallery_id);
    $rs = $con->delete("photo_gallery", $delete_array, "array");


    if ($rs["output"] == "success") {
        echo "" . $photo_gallery_id . "";
    } else {
        $errors = array("error" => "yes", "message" => "Invalid Delete Query!");
        echo json_encode($errors);
    }
}
?>