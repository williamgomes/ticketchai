<?php 
include '../config/class.web.config.php';
$con = new Config();
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];

if ($verb == "GET") {

    $arr = array();
    $table_array= array("synopsis","event","language");
    $field_array= array("synopsis"=>"synopsis_id,synopsis_genre,synopsis_writer_name,synopsis_director_name,synopsis_duration,synopsis_release_date,synopsis_details","event"=>"event_id,event_title","language"=>"language_id,language_name");
    $jsonArr = $con->one_many_relation($table_array, $field_array, "", "json");
    $count = count(json_decode($jsonArr));
     if ($count >= 1) {
        echo "{\"data\":" . $jsonArr . "}";
    } else {
        echo "{\"data\":" . "[]" . "}";
    }
}
/*
select synopsis.synopsis_id as synopsis_id, synopsis.synopsis_genre as synopsis_genre, synopsis.synopsis_details as synopsis_details,
synopsis.synopsis_release_date as synopsis_release_date,synopsis.synopsis_writer_name as writer_name, synopsis.synopsis_director_name as
synopsis_director_name, synopsis.synopsis_duration as synopsis_duration, event.event_id as event_id, event.event_title as event_title,
language.language_id as language_id, language.language_name as language_name from synopsis inner join
event on synopsis.event_id = event.event_id inner join language on synopsis.language_id = language.language_id order by synopsis.synopsis_id DESC
*/
?>