<?php

include '../config/class.web.config.php';
$con = new Config();
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];
if ($verb == "GET") {
    $arr = array();
    $rs = "SELECT `event`.event_id AS event_id, `event`.event_title AS event_title, `event`.event_ticket_expair_date AS event_ticket_expair_date, `event`.event_full_size_image AS event_full_size_image, `event`.event_logo_image AS event_logo_image, `event`.featured AS featured, `event`.f_priority AS f_priority, `event`.upcomming AS upcomming, `event`.u_priority AS u_priority FROM `event` ORDER BY `event`.event_id DESC";

    $jsonArr = $con->ReturnObjectByQuery($rs, "json");
    $count = count($jsonArr);
    if ($count >= 1) {
        echo "{\"data\":" . $jsonArr . "}";
    } else {
        echo "{\"data\":" . "[]" . "}";
    }
}


if ($verb == "POST") {
    extract($_POST);
    $featured = $_POST["featured"];
    $f_priority = $_POST["f_priority"];
    $upcomming = $_POST["upcomming"];
    $u_priority = $_POST["u_priority"];

    $errors = array();

    if ($featured == "true" && $f_priority == "" && $upcomming == "true" && $u_priority == "") {
        $errors = array("error" => "yes", "message" => "Please Select One For " . $_POST["event_title"]);
        echo json_encode($errors);
    } elseif ($featured == "true" && $f_priority != "" && $upcomming == "true" && $u_priority == "") {
        $errors = array("error" => "yes", "message" => "Please Select One For " . $_POST["event_title"]);
        echo json_encode($errors);
    } elseif ($featured == "true" && $f_priority == "" && $upcomming != "true" && $u_priority != "") {
        $errors = array("error" => "yes", "message" => "Please Enter Featured Priority Or Select Upcoming For  " . $_POST["event_title"]);
        echo json_encode($errors);
    } elseif ($featured == "true" && $f_priority != "" && $upcomming == "true" && $u_priority != "") {
        $errors = array("error" => "yes", "message" => "Please Select One For  " . $_POST["event_title"]);
        echo json_encode($errors);
    } elseif ($featured == "true" && $f_priority == "" && $upcomming != "true" && $u_priority == "") {
        $errors = array("error" => "yes", "message" => "Please Enter Featured Priority For " . $_POST["event_title"]);
        echo json_encode($errors);
    } elseif ($featured == "true" && $f_priority != "" && $upcomming != "true" && $u_priority != "") {
        $errors = array("error" => "yes", "message" => "Please Select One For " . $_POST["event_title"]);
        echo json_encode($errors);
    } elseif ($featured != "true" && $f_priority != "" && $upcomming == "true" && $u_priority != "") {
        $errors = array("error" => "yes", "message" => "Please Enter Upcoming Priority For  " . $_POST["event_title"]);
        echo json_encode($errors);
    } elseif ($featured == "true" && $f_priority == "" && $upcomming == "true" && $u_priority != "") {
        $errors = array("error" => "yes", "message" => "Please Select One For " . $_POST["event_title"]);
        echo json_encode($errors);
    } elseif ($featured != "true" && $f_priority == "" && $upcomming == "true" && $u_priority == "") {
        $errors = array("error" => "yes", "message" => "Please Enter Upcoming Priority For  " . $_POST["event_title"]);
        echo json_encode($errors);
    } elseif ($featured != "true" && $f_priority == "" && $upcomming != "true" && $u_priority != "") {
        $errors = array("error" => "yes", "message" => "Please Select Upcoming For  " . $_POST["event_title"]);
        echo json_encode($errors);
    } elseif ($featured == "true" && $f_priority != "" && $upcomming != "true" && $u_priority == "") {
        //echo "111111";
        $getList = "SELECT featured, f_priority FROM event WHERE featured='$featured' AND f_priority='$f_priority' AND event_id NOT IN (" . $_POST["event_id"] . ")";
        $getListResult = $con->ReturnObjectByQuery($getList);

        if (count($getListResult) > 0) {
            $errors = array("error" => "yes", "message" => "Featured Priority Already Exist For " . $_POST["event_title"]);
            echo json_encode($errors);
        } else {
            //echo "OK1";
            $update_array = array(
                "event_id" => $_POST["event_id"],
                "event_title" => $_POST["event_title"],
                "featured" => $_POST["featured"],
                "f_priority" => $_POST["f_priority"]
            );
            $result = $con->update("event", $update_array, "event_title", "", "array");
            //echo "OK2";
        }
    } elseif ($featured != "true" && $f_priority == "" && $upcomming == "true" && $u_priority != "") {
        //echo "222222";
        $getListUp = "SELECT upcomming, u_priority FROM event WHERE upcomming ='$upcomming' AND u_priority='$u_priority' AND event_id NOT IN (" . $_POST["event_id"] . ")";
        $getListResultUp = $con->ReturnObjectByQuery($getListUp);
        if (count($getListResultUp) > 0) {
            $errors = array("error" => "yes", "message" => "Upcoming Priority Already Exist For " . $_POST["event_title"]);
            echo json_encode($errors);
        } else {
            //echo "OK3";
            $update_array = array(
                "event_id" => $_POST["event_id"],
                "event_title" => $_POST["event_title"],
                "upcomming" => $_POST["upcomming"],
                "u_priority" => $_POST["u_priority"]
            );
            $result = $con->update("event", $update_array, "event_title", "", "array");
            // echo "OK4";
        }
    }
}

if ($verb == "DELETE") {
    $request_vars = array();
    parse_str(file_get_contents('php://input'), $request_vars);
    $errors = array();
    $event_id = $request_vars["event_id"];
    $delete_array = array("event_id" => $event_id);

    $rs = $con->delete("event", $delete_array, "array");
    if ($rs["output"] == "success") {
        echo "" . $event_id . "";
    } else {
        $errors = array("error" => "yes", "message" => "Invalid Delete Query!");
        echo json_encode($errors);
    }
}
?>
