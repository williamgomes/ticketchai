<?php
include '../config/class.web.config.php';
$con= new Config();
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];

if ($verb == "GET") {
    
    $arr = array();    
    $table_array= array("city","country");
    $field_array= array("city"=>"city_id,city_name,is_active","country"=>"country_id,country_name");
    $jsonArr = $con->one_many_relation($table_array, $field_array, "", "json");
    $count = count(json_decode($jsonArr));
     if ($count >= 1) {
        echo "{\"data\":" . $jsonArr . "}";
    } else {
        echo "{\"data\":" . "[]" . "}";
    }
}
if ($verb == "POST") { 
     extract($_POST);
     //array_push($_POST, "Error_Flag");
     unset($_POST["country_name"]);
    
    $errors = array();
    $result= $con->update("city", $_POST, "city_name","", "array");
    if ($result["output"] == "error") {
        $errors = array("error" => "yes", "message" => $result["msg"]);
        echo json_encode($errors);
    } 
    else if($result["output"]=="success")
    {
        if ($result["result"]) {
            echo json_encode($result["result"]);
        } else {
            $errors = array("error" => "yes", "message" => "Update failed for City name: " .$_POST["city_name"] );
            echo json_encode($errors);
        }
    }
    
}
if ($verb == "PUT") {
     $request_vars = array();
    parse_str(file_get_contents('php://input'), $request_vars);
    array_shift($request_vars);
    //array_push($request_vars, "flag");
    unset($request_vars["country_name"]);
    $result = $con->insert("city", $request_vars, "city_name","", "array");

    if ($result["output"] == "error") {
        $errors = array("error" => "yes", "message" => $result["msg"]);
        echo json_encode($errors);
    } else if ($result["output"] == "success") {
        if ($result["result"]) {
            echo "" . $result["result"] . "";
        } else {
            $errors = array("error" => "yes", "message" => "Invalid Insertion Query");
            echo json_encode($errors);
        }
    }
    
}
if ($verb == "DELETE") {
 
   $request_vars = array();
    parse_str(file_get_contents('php://input'), $request_vars);
    $errors = array();
    $city_id = $request_vars["city_id"];

    $delete_array = array("city_id" => $city_id);
    //$rs = $con->delete($object, $object_array, $return_type)
    $rs = $con->delete("city", $delete_array,"array");
    

    if ($rs["output"] == "success") {
        echo "" . $city_id . "";
    } else {
        $errors = array("error" => "yes", "message" => "Invalid Delete Query!");
        echo json_encode($errors);
    }
}
?>