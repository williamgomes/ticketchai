<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 * @author Tariqul Islam(Sotware Engineer At Systechunimax)
 * @copyright (c) 2014, Tariqul Islam
 * 
 */
class controller {

    function Label($title, $text_align) {
        $label_render = "";

        if ($text_align == "left") {
            $label_render .='<label class="col-md-4 control-label" style="text-align: left !important;" for="' . $title . '">' . $title . '</label>';
        } else {
            $label_render .='<label class="col-md-4 control-label"  for="' . $title . '">' . $title . '</label>';
        }
        return $label_render;
    }

    function TextBox($name, $id, $style = '', $placeholder = '', $class = '', $value = '', $type = '') {
        $textbox = "";
        if (empty($type)) {
            $textbox = "<input type='text' name='$name' id='$id' style='$style' placeholder='$placeholder' class='$class' value='$value'/>";
        } else if ($type == 'kendo') {
            $textbox = '<input class="form-control k-textbox" style="' . $style . '" id="' . $id . '" name="' . $name . '" type="text" value="' . $value . '" placeholder="' . $placeholder . '" />';
        }
        return $textbox;
    }
     function TextArea($name,$id,$selected_value,$row,$col,$style='',$class=''){
       return  '<textarea cols="'.$col.'" style="" class="'.$class.'" rows="'.$row.'" name="'.$name.'" id="'.$id.'">'.$selected_value.'</textarea>';
     }
     function PasswordTextBox($name, $id, $style = '', $placeholder = '', $class = '', $value = '', $type = '') {
        $password = "";
        if (empty($type)) {
            $password = "<input type='password' name='$name' id='$id' style='$style' placeholder='$placeholder' class='$class' value='$value'/>";
        } else if ($type == 'kendo') {
            $password = '<input class="form-control k-textbox" style="' . $style . '" id="' . $id . '" name="' . $name . '" type="password" value="' . $value . '" placeholder="' . $placeholder . '" />';
        }
        return $password;
    }

    function DateTimePicker($name, $id, $value, $style = '', $class = '') {
        $datetime_render = '';
        $datetime_render .= '<input id="' . $id . '" name="' . $name . '" style="' . $style . '" class="' . $class . '" value="' . $value . '" />';
        $datetime_render .= '<script type="text/javascript">';
        $datetime_render .= '$(document).ready(function(){';
        $datetime_render .= '$("#' . $id . '").kendoDatePicker();';
        $datetime_render .= '});</script>';

        return $datetime_render;
    }

    function FileUpload($name, $id, $multi = '', $asyn = '', $save_controller = '', $delete_controller = '') {

        $file_upload_render = '';
        if (!empty($asyn)) {
            if (!empty($multi)) {
                $file_upload_render.= '<input type="file" id="' . $id . '" name="' . $name . '[]"/>';
                $file_upload_render .= '<script type="text/javascript">';
                $file_upload_render .= '$(document).ready(function() {';
                $file_upload_render .= '$("#' . $id . '").kendoUpload({';
                $file_upload_render .= 'multiple:true,';
                $file_upload_render .= 'async: {';
                $file_upload_render .= '  saveUrl: "' . $save_controller . '",';
                $file_upload_render .= 'removeUrl: "' . $delete_controller . '",';
                $file_upload_render .= 'autoUpload: true';
                $file_upload_render .= '}';
                $file_upload_render .= '  });';
                $file_upload_render .=' });';
                $file_upload_render .= '</script>  ';
            } else {

                $file_upload_render.= '<input type="file" id="' . $id . '" name="' . $name . '"/>';
                $file_upload_render .= '<script type="text/javascript">';
                $file_upload_render .= '$(document).ready(function() {';
                $file_upload_render .= '$("#' . $id . '").kendoUpload({';
                $file_upload_render .= 'multiple:false,';
                $file_upload_render .= 'async: {';
                $file_upload_render .= 'saveUrl: "' . $save_controller . '",';
                $file_upload_render .= 'removeUrl: "' . $delete_controller . '",';
                $file_upload_render .= 'autoUpload: true';
                $file_upload_render .= '}';
                $file_upload_render .= '  });';
                $file_upload_render .=' });';
                $file_upload_render .= '</script>  ';
            }
        } else {
            if (!empty($multi)) {
                $file_upload_render.= '<input type="file" id="' . $id . '" name="' . $name . '[]"/>';
                $file_upload_render .= ' <script type="text/javascript">';
                $file_upload_render .= '$(document).ready(function() {';
                $file_upload_render .= '$("#' . $id . '").kendoUpload({multiple: true});';
                $file_upload_render .= ' });';
                $file_upload_render .= '</script>';
                $file_upload_render .= '}';
            } else {
                $file_upload_render .= '<input type="file" id="' . $id . '" name="' . $name . '"/>';
                $file_upload_render .= ' <script type="text/javascript">';
                $file_upload_render .= '$(document).ready(function() {';
                $file_upload_render .= '$("#' . $id . '").kendoUpload({multiple: false});';
                $file_upload_render .= ' });';
                $file_upload_render .= '</script>';
            }
        }
        return $file_upload_render;
    }

    function AutoComplete($name, $id, $data_text_field, $controller_name, $value, $style = '', $class = '') {
        $auto_complete_render = "";
        $auto_complete_render .= '<input id="' . $id . '" name="' . $name . '" style="' . $style . '" class="' . $class . '" value="' . $value . '" />';
        $auto_complete_render .= '<script type="text/javascript">';
        $auto_complete_render .= '$(document).ready(function(){';
        $auto_complete_render.= '$("#' . $id . '").kendoAutoComplete({
                                    dataSource   : new kendo.data.DataSource({
                                        serverFiltering: true,
                                        transport      : {
                                            read: "../../controller/' . $controller_name . '"
                                        },
                                        schema         : {
                                            data: "data"
                                        }
                                    }),
                                    minLength    : 1,
                                    dataTextField: "' . $data_text_field . '",
                                    placeholder  : "Please select' . $name . '"
                                   });
    
                                        });</script>';
        return $auto_complete_render;
    }

    function NumericTextBox($name, $id, $selected_value, $min, $max, $step, $style = '', $class = '') {
        $numaric_textbox = '';
        //width: 100% !important;height:35px !important
        $numaric_textbox .='<input id="' . $id . '" style="' . $style . '" name="' . $name . '" type="number" value="' . $selected_value . '" min="' . $min . '" max="' . $max . '" step="' . $step . '" class="' . $class . '" />';
        $numaric_textbox .= '<script type="text/javascript">';
        $numaric_textbox .= ' $(document).ready(function() {';
        // create NumericTextBox from input HTML element
        $numaric_textbox .= ' $("#' . $id . '").kendoNumericTextBox();';
        $numaric_textbox .= '});';
        $numaric_textbox .= '</script>';
        return $numaric_textbox;
    }

    function SelectBox($name, $id, $DataSource = '', $data_value_field, $data_text_field, $selected_value, $cascade_from = '', $controller = '', $type = '', $style = '', $class = '') {

        $select_render = "";

        if (empty($type)) {
            $select_render .='<select id="' . $id . '" style="' . $style . '" class="' . $class . '" name="' . $name . '">';
            $select_render .='<option value="option_0" >--SELECT ONE--</option>';
            if (!empty($DataSource)) {
                if (count($DataSource) >= 1):
                    foreach ($DataSource as $d):
                        $select_render .= '<option value="' . $d->{"$data_value_field"} . '"  ';
                        if ($d->{"$data_value_field"} == $selected_value):
                            $select_render .="   selected='selected' ";
                        endif;
                        $select_render .=">";
                        $select_render .= $d->{"$data_text_field"} . "</option>";
                    endforeach;
                endif;
            }
        }
        else if ($type == "kendo") {
            if (empty($DataSource)) {
                $temp_name = explode("_", $name);
                $PlaceHolderName = $temp_name[0];
                $select_render .= '<input id="' . $id . '" style="' . $style . '"  name="' . $name . '" value="' . $selected_value . '"  />';

                $select_render .= '<script type="text/javascript">';
                $select_render .= '$(document).ready(function() {';
                $select_render .= 'var ' . $name . ' = $("#' . $name . '").kendoComboBox({';
                $select_render .= 'placeholder: "Select ' . $PlaceHolderName . '",';
                if (!empty($cascade_from)) {
                    $select_render .= 'cascadeFrom: "' . $cascade_from . '",';
                }
                $select_render .= 'dataTextField: "' . $data_text_field . '",';
                $select_render .= 'dataValueField: "' . $data_value_field . '",';
                $select_render .= 'dataSource: {';
                $select_render .= 'type: "json",';
                $select_render .= 'transport: {';
                $select_render .= 'read: {';
                $select_render .= 'url: "../../controller/' . $controller . '",';
                $select_render .= 'type: "GET"';
                $select_render .= '}';
                $select_render .= '},';
                $select_render .= 'schema: {';
                $select_render .= 'data: "data"';
                $select_render .= '}';
                $select_render .= '}';
                $select_render .= '}).data("kendoComboBox");';
                $select_render .= '});';
                $select_render .= '</script>';
            } else {
//                $select_render .='<select id="' . $id . '" style="' . $style . '" class="' . $class . '" name="' . $name . '">';
//                $select_render .='<option value="option_0" >--SELECT ONE--</option>';
//                if (!empty($DataSource)) {
//                    if (count($DataSource) >= 1):
//                        foreach ($DataSource as $d):
//                            $select_render .= '<option value="' . $d->{"$data_value_field"} . '"  ';
//                            if ($d->{"$data_value_field"} == $selected_value):
//                                $select_render .="   selected='selected' ";
//                            endif;
//                            $select_render .=">";
//                            $select_render .= $d->{"$data_text_field"} . "</option>";
//                        endforeach;
//                    endif;
//                    $select_render .= '<script type="text/javascript">
//                            $(document).ready(function() {
//                                $("#'.$id.'").kendoDropDownList();
//
//                            });
//                        </script>';
//                }
            }
        }

        return $select_render;
    }

    function DropDownList($name, $id, $data_value_field, $data_text_field, $selected_value, $cascade_from = '', $controller = '', $style = '', $class = '') {

        $select_render = "";
        $select_render .= '<input id="' . $id . '" style="' . $style . '"  name="' . $name . '" value="' . $selected_value . '"  />';

        $select_render .= '<script type="text/javascript">';
        $select_render .= '$(document).ready(function() {';
        $select_render .= 'var ' . $name . ' = $("#' . $name . '").kendoDropDownList({';
        $select_render .= 'placeholder: "Select ' . $name . '",';
        if (!empty($cascade_from)) {
            $select_render .= 'cascadeFrom: "' . $cascade_from . '",';
        }
        $select_render .= 'dataTextField: "' . $data_text_field . '",';
        $select_render .= 'dataValueField: "' . $data_value_field . '",';
        $select_render .= 'dataSource: {';
        $select_render .= 'type: "json",';
        $select_render .= 'transport: {';
        $select_render .= 'read: {';
        $select_render .= 'url: "../../controller/' . $controller . '",';
        $select_render .= 'type: "GET"';
        $select_render .= '}';
        $select_render .= '},';
        $select_render .= 'schema: {';
        $select_render .= 'data: "data"';
        $select_render .= '}';
        $select_render .= '}';
        $select_render .= '}).data("kendoComboBox");';
        $select_render .= '});';
        $select_render .= '</script>';


        return $select_render;
    }

    function CheckBox() {
        
    }

}
