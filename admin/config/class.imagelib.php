<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Image Scaling Creation
 */
class ImageScale {

    var $image;
    var $image_type;

    function load($filename) {
        $image_info = getimagesize($filename);
        $this->image_type = $image_info[2];
        if ($this->image_type == IMAGETYPE_JPEG) {
            $this->image = imagecreatefromjpeg($filename);
        } elseif ($this->image_type == IMAGETYPE_GIF) {
            $this->image = imagecreatefromgif($filename);
        } elseif ($this->image_type == IMAGETYPE_PNG) {
            $this->image = imagecreatefrompng($filename);
        }
    }

    function save($filename, $image_type = IMAGETYPE_JPEG, $compression = 75, $permissions = null) {
        if ($image_type == IMAGETYPE_JPEG) {
            imagejpeg($this->image, $filename, $compression);
        } elseif ($image_type == IMAGETYPE_GIF) {
            imagegif($this->image, $filename);
        } elseif ($image_type == IMAGETYPE_PNG) {
            imagepng($this->image, $filename);
        } if ($permissions != null) {
            chmod($filename, $permissions);
        }
    }

    function output($image_type = IMAGETYPE_JPEG) {
        if ($image_type == IMAGETYPE_JPEG) {
            imagejpeg($this->image);
        } elseif ($image_type == IMAGETYPE_GIF) {
            imagegif($this->image);
        } elseif ($image_type == IMAGETYPE_PNG) {
            imagepng($this->image);
        }
    }

    function getWidth() {
        return imagesx($this->image);
    }

    function getHeight() {
        return imagesy($this->image);
    }

    function resizeToHeight($height) {
        $ratio = $height / $this->getHeight();
        $width = $this->getWidth() * $ratio;
        $this->resize($width, $height);
    }

    function resizeToWidth($width) {
        $ratio = $width / $this->getWidth();
        $height = $this->getheight() * $ratio;
        $this->resize($width, $height);
    }

    function scale($scale) {
        $width = $this->getWidth() * $scale / 100;
        $height = $this->getheight() * $scale / 100;
        $this->resize($width, $height);
    }

    function resize($width, $height) {
        $new_image = imagecreatetruecolor($width, $height);
        imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
        $this->image = $new_image;
    }

}

/** imagescaling Class Usage * */
//------------------resize the hight and width ----//

/* * <?php include('ImageScale.php');
  $image = new SimpleImage();
  $image->load('picture.jpg');
  $image->resize(250, 400);
  $image->save('picture2.jpg'); ?> * */

//---- end resize the hight and width -------------//
//------------- resize the width of image ---------------//
/* * <?php include('ImageScale.php');
  $image = new ImageScale();
  $image->load('picture.jpg');
  $image->scale(50);
  $image->save('picture2.jpg'); ?> * */

//--------------- end resize the width of image -----------//

/** scale the image * */
/* * <?php include('ImageScale.php');
  $image = new ImageScale();
  $image->load('picture.jpg');
  $image->scale(50);
  $image->save('picture2.jpg'); ?>



  <?php include('SimpleImage.php'); $image = new SimpleImage(); $image->load('picture.jpg'); $image->resizeToHeight(500); $image->save('picture2.jpg'); $image->resizeToHeight(200); $image->save('picture3.jpg'); ?>
 *  */

//<?php
//if (isset($_POST['submit'])) {
//    include('ImageScale.php');
//    $image = new SimpleImage();
//    $image->load($_FILES['uploaded_image']['tmp_name']);
//    $image->resizeToWidth(150);
//    $image->output();
//} else {
     

//<form action="upload.php" method="post" enctype="multipart/form-data"> <input type="file" name="uploaded_image" />   <input type="submit" name="submit" value="Upload" />   </form>   <?php } 


/** End imagescaling class Usage **/