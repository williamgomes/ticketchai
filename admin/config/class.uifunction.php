<?php

class ui_function {

    function notification($msg='',$err='') {
        $render_notification = '';
        if (!empty($msg)):
            $render_notification .= '<div class = "alert alert-success">';
            $render_notification .= '<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>';
            $render_notification .= '<strong>Well done! </strong>';
            $render_notification .= $msg;
            $render_notification .='</div >';
        endif;
        if (!empty($err)):
            $render_notification .='<div class = "alert alert-danger fade in alert-dismissable">';
            $render_notification .= '<button class = "close" type = "button" aria-hidden="true" data-dismiss = "alert">×</button>';
            $render_notification .='<strong>Sorry! </strong>';
            $render_notification .=$err;
            $render_notification .= '</div>';
        endif;
        return $render_notification;
    }

}
