<?php
session_start();
include("class.database.php");
include ("class.imagelib.php");
include("class.controller.php");
include("class.uifunction.php");



/**
 * class use for configure the the database percestance object
 */
class Config extends database {

    function sendSMSAPI($destination, $message) {

//        $host = "121.241.242.114";
//        $port = "8080";
//        $strUserName = "unim-ticketchai";
//        $strPassword = "ticket12";
//        $strSender = "TicketChai";
//        $strMobile = urlencode($destination);
//        $strMessage = urlencode($message);
//        $strMessageType = 0;
//        $strDlr = 0;
//
//        try {
//            $live_url = "http://" . $host . ":" . $port . "/bulksms/bulksms?username=" . $strUserName . "&password=" . $strPassword . "&type=" . $strMessageType . "&dlr=" . $strDlr . "&destination=" . $strMobile . "&source=" . $strSender . "&message=" . $strMessage . "";
////$live_url="http://".$host."/websms/unimax?username=".$strUserName."&password=".$strPassword."&type=".$strMessageType."&dlr=".$strDlr."&destination=".$strMobile."&source=".$strSender."&message=".$strMessage."";
//            $parse_url = file($live_url);
//            return $parse_url[0];
//        } catch (Exception $e) {
//            return 'Message:' . $e->getMessage();
//        }
    }

   
    function redirect($link) {
        echo "<script>location.href='$link'</script>";
    }

    function notification($msg = '', $err = '') {
        $ui = new ui_function();
        return $ui->notification($msg, $err);
    }

    function Label($title, $text_align) {
        $con = new controller();
        return $con->Label($title, $text_align);
    }

    function PasswordTextBox($name, $id, $style = '', $placeholder = '', $class = '', $value = '', $type = '') {
        $con = new controller();
        return $con->PasswordTextBox($name, $id, $style, $placeholder, $class, $value, $type);
    }

    function TextArea($name, $id, $selected_value, $row, $col, $style = '', $class = '') {
        $con = new controller();
        return $con->TextArea($name, $id, $selected_value, $row, $col, $style, $class);
    }

    function TextBox($name, $id, $style = '', $placeholder = '', $class = '', $value = '', $type = '') {
        $con = new controller();
        return $con->TextBox($name, $id, $style, $placeholder, $class, $value, $type);
    }

    function SelectBox($name, $id, $DataSource, $data_value_field, $data_text_field, $selected_value, $cascade_from = '', $controller = '', $type = '', $style = '', $class = '') {
        $con = new controller();
        return $con->SelectBox($name, $id, $DataSource, $data_value_field, $data_text_field, $selected_value, $cascade_from, $controller, $type, $style, $class);
    }

    function DropDownList($name, $id, $data_value_field, $data_text_field, $selected_value, $cascade_from = '', $controller = '', $style = '', $class = '') {
        $con = new controller();
        return $con->DropDownList($name, $id, $data_value_field, $data_text_field, $selected_value, $cascade_from, $controller, $style, $class);
    }

    function NumericTextBox($name, $id, $selected_value, $min, $max, $step, $style = '', $class = '') {
        $con = new controller();
        return $con->NumericTextBox($name, $id, $selected_value, $min, $max, $step, $style, $class);
    }

    function FileUpload($name, $id, $multi = '', $asyn = '', $save_controller = '', $delete_controller = '') {
        $con = new controller();
        return $con->FileUpload($name, $id, $multi, $asyn, $save_controller, $delete_controller);
    }

    function debug($object) {
        echo "<pre>";
        print_r($object);
        echo "</pre>";
    }

    function MetaRefresh($time_limit, $url = '') {
        if (!empty($url)) {
            return '<meta http-equiv="refresh" content="' . $time_limit . ';url=' . $url . '">';
        } else {
            return '<meta http-equiv="refresh" content="' . $time_limit . '">';
        }
    }

    function DateTimePicker($name, $id, $value, $style = '', $class = '') {
        $con = new controller();
        return $con->DateTimePicker($name, $id, $value, $style, $class);
    }

    function AutoComplete($name, $id, $data_text_field, $controller_name, $value, $style = '', $class = '') {
        $con = new controller();

        return $con->AutoComplete($name, $id, $data_text_field, $controller_name, $value, $style, $class);
    }

    function baseUrl($suffix = '') {
        $protocol = strpos($_SERVER['SERVER_SIGNATURE'], '443') !== false ? 'https://' : 'http://';
        $web_root = $protocol . $_SERVER['HTTP_HOST'] . "/" . "ticketchai/admin/";
        $suffix = ltrim($suffix, '/');
        return $web_root . trim($suffix);
    }

    function siteUrl($suffix = '') {
        $protocol = strpos($_SERVER['SERVER_SIGNATURE'], '443') !== false ? 'https://' : 'http://';
        $web_root = $protocol . $_SERVER['HTTP_HOST'] . "/ticketchai";
        $suffix = ltrim($suffix, '/');
        return $web_root . trim($suffix);
    }

    function PHP_File_Upload($path, $filesFieldName, $alies = '', $file_extention_allowed = '', $file_size = '', $return_type = '') {
        $ext = explode('.', $_FILES["$filesFieldName"]["name"]);
        $extension = end($ext);
        if (!empty($file_extention_allowed)) {
            if (!in_array($extension, $file_extention_allowed)) {
                if ($return_type == "array") {
                    return array("output" => "error", "msg" => "Invalid File Extention");
                } else if ($return_type == "json") {
                    return json_encode(array("output" => "error", "msg" => "Invalid File Extention"));
                } else {
                    return "Invalid File Extention";
                }
            }
        }

        if (!empty($file_size)) {
            if (!is_int($file_size)) {
                if ($return_type == "array") {
                    return array("output" => "error", "msg" => "Invalid File Size");
                } else if ($return_type == "json") {
                    return json_encode(array("output" => "error", "msg" => "Invalid File Size"));
                } else {
                    return "Invalid File Size";
                }
            } else if ($_FILES["$filesFieldName"]["size"] > $file_size) {
                if ($return_type == "array") {
                    return array("output" => "error", "msg" => "Invalid File Size");
                } else if ($return_type == "json") {
                    return json_encode(array("output" => "error", "msg" => "Invalid File Size"));
                } else {
                    return "Invalid File Size";
                }
            }
        }

        $newname = '';
        if (!empty($alies)) {
            $newname = $alies . '_' . date("Y_m_d") . '_' . time() . "." . $extension;
        } else {
            $newname = $ext[0] . '_' . date("Y_m_d") . '_' . time() . "." . $extension;
        }
        $server_upload_path = $path . $newname;
        if (move_uploaded_file($_FILES["$filesFieldName"]["tmp_name"], $server_upload_path)) {
            if ($return_type == "array") {
                return array("output" => "success", "msg" => "File is Upload Successfully");
            } else if ($return_type == "json") {
                return json_encode(array("output" => "success", "msg" => "Invalid is Upload Successfully"));
            } else {
                return "File is Uplaod Successfully";
            }
        } else {
            if ($return_type == "array") {
                return array("output" => "error", "msg" => "File Upload Server Error");
            } else if ($return_type == "json") {
                return json_encode(array("output" => "error", "msg" => "FIle Upload Server Error"));
            } else {
                return "File Upload Server Error";
            }
        }
    }

    function authenticate() {

        if (!isset($_SESSION["admin_mobile_number"]) || (trim($_SESSION["admin_mobile_number"]) == '')) {
            session_write_close();
            return 1;
        } else {
            return 0;
        }
    }

    function logout() {
        session_unset();
        session_destroy();
        return 1;
    }

//    function login($object, $email = '', $username = '', $password = '') {
//        $count = 0;
//        $fields = '';
//        $con = $this->open();
//
//        if (!empty($email)) {
//            $fields .= $object . "_" . "email='" . mysqli_real_escape_string($con, $email) . "'  ";
//        }
//        if (!empty($username)) {
//            $fields .= " AND " . $object . "_" . "username='" . mysqli_real_escape_string($con, $username) . "' ";
//        }
//        if (!empty($password)) {
//            $fields .= " AND " . $object . "_" . "password='" . mysqli_real_escape_string($con, md5($password)) . "' ";
//        }
//        $query = "SELECT * FROM `$object` WHERE $fields";
//        $result = mysqli_query($con, $query);
//        if ($result) {
//            $count = mysqli_num_rows($result);
//            $this->close($con);
//            if ($count >= 1) {
//                return 1;
//            } else {
//                return 0;
//            }
//        } else {
//            $this->close($con);
//            return 0;
//        }
//    }

    function login($username, $password) {
        $count = 0;
        $fields = '';
        $con = $this->open();

//        $fields = "admin_email='" . mysqli_real_escape_string($con, $username) . "' and admin_password ='" . mysqli_real_escape_string($con, $password) . "'";
//        $query = "SELECT * FROM `admin` WHERE $fields";
        $condition = " admin_email_address='" . mysqli_real_escape_string($con, $username) . "' and admin_password='" . mysqli_real_escape_string($con, $password) . "'";
        $query = "SELECT * FROM `admin` WHERE $condition";

        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
            if ($count == 1) {
                return 1;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }

    function customer_login($email, $password) {
        $count = 0;
        $fields = '';
        $con = $this->open();

//        $fields = "admin_email='" . mysqli_real_escape_string($con, $username) . "' and admin_password ='" . mysqli_real_escape_string($con, $password) . "'";
//        $query = "SELECT * FROM `admin` WHERE $fields";
        $condition = " email='" . mysqli_real_escape_string($con, $email) . "' and password='" . mysqli_real_escape_string($con, $password) . "'";
        $query = "SELECT * FROM `customer` WHERE $condition";

        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
            if ($count == 1) {
                return 1;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }

    function ConvertTimeAMPM($time) {

        $time_array = explode(":", trim($time));
        $hours = '';
        $ampm = '';
        if ($time_array[0] > 12) {
            $hours = $time_array[0] - 12;
            if ($hours < 9) {
                $hours = $hours;
            }
            $ampm = ' PM';
        }if ($time_array[0] == 0) {
            $hours = "12";
            $ampm = ' AM';
        } else if ($time_array[0] < 12 && $time_array[0] > 0) {
            $hours = $time_array[0];
            if ($hours < 9) {
                $hours = $hours;
            }
            $ampm = ' AM';
        }
        $minutes = '';

        if ($time_array[1] < 9 && $time_array[1] > 0) {
            $minutes = "0" . $minutes;
        } else if ($time_array[1] == 0) {
            $minutes = "00";
        } else {
            $minutes = $time_array[1];
        }
        return $hours . ':' . $minutes . $ampm;
    }

    function SitebaseUrl($suffix = '') {
        $protocol = strpos($_SERVER['SERVER_SIGNATURE'], '443') !== false ? 'https://' : 'http://';
        $web_root = $protocol . $_SERVER['HTTP_HOST'] . "/" . "ticket_chai/site/html/";
        $suffix = ltrim($suffix, '/');
        return $web_root . trim($suffix);
    }

    function mail_attachment($filename, $path, $mailto, $replyto, $subject, $message) {
        $file = $path . $filename;
        $file_size = filesize($file);
        $handle = fopen($file, "r");
        $content = fread($handle, $file_size);
        fclose($handle);
        $content = chunk_split(base64_encode($content));
        $uid = md5(uniqid(time()));
        $name = basename($file);
        $from_mail = "rpac_erp@r-pac.com";
        $header = "From: RPAC ERP<" . $from_mail . ">\r\n";
        $header .= "Reply-To: " . $replyto . "\r\n";
//        $header .= "MIME-Version: 1.0\r\n";
//        $header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"\r\n\r\n";
//        $header .= "This is a multi-part message in MIME format.\r\n";
//        $header .= "--" . $uid . "\r\n";
        $header .= "Content-type:text/html\r\n";       //; charset=iso-8859-1\r\n";
        $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
        $header .= $message . "\r\n\r\n";
//        $header .= "--" . $uid . "\r\n";

        $header .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"\r\n"; // use different content types here
        $header .= "Content-Transfer-Encoding: base64\r\n";
        $header .= "Content-Disposition: attachment; filename=\"" . $filename . "\"\r\n\r\n";
        $header .= $content . "\r\n\r\n";

//        $header .= "--" . $uid . "--";

        if (mail($mailto, $subject, "asdasd", $header, '-f rpac_erp@r-pac.com')) {
            echo "mail send ... OK"; // or use booleans here
        } else {
            echo "mail not send";
        }
    }

    function sent_mail_without_attatchment($mailto, $replyto, $subject, $message) {

        $from_mail = "rpac_erp@r-pac.com";
        $header = "From: RPAC ERP<" . $from_mail . ">\r\n";
        $header .= "Reply-To: " . $replyto . "\r\n";
//        $header .= "MIME-Version: 1.0\r\n";
//        $header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"\r\n\r\n";
//        $header .= "This is a multi-part message in MIME format.\r\n";
//        $header .= "--" . $uid . "\r\n";
        $header .= "Content-type:text/html\r\n";       // ;charset=iso-8859-1\r\n"; 
        $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
        $header .= $message . "\r\n\r\n";
//        $header .= "--" . $uid . "\r\n";
        if (mail($mailto, $subject, "asdasd", $header, '-f rpac_erp@r-pac.com')) {
            echo "mail send ... OK"; // or use booleans here
        } else {
            echo "mail not send";
        }
    }
    
    
    function authenticateClient() {

        if (!isset($_SESSION["customer_id"]) || (trim($_SESSION["customer_id"]) == '')) {
            session_write_close();
            return 1;
        } else {
            return 0;
        }
    }
    
    /**
     * Check:admin_login, admin_id ,admin_email , admin_type, admin_hash,  admin_password, admin_name
     * @return bool 
     */
    function checkUserLogin() {
        $status = array();
        
        if(isset($_SESSION['customer_id'])){
            if ($_SESSION['customer_id'] > 0) {
                $status[] = 1;
            }

            if ($_SESSION['first_name'] != '') {
                $status[] = 2;
            }

            if ($_SESSION['last_name'] != '') {
                $status[] = 3;
            }

            if ($_SESSION['email'] != '') {
                $status[] = 4;
            }
            

            if (count($status) < 4 OR in_array(0, $status)) {
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }
    
    
    
    function validateInput($value = ''){
        $output = '';
        
        if($value != ""){
            $output = trim($value);
            $output = strip_tags($output);
            $output = mysqli_real_escape_string($this->open(), $output);
        }
        
        return $output;
    }

}
