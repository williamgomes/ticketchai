<?php

class database {

    public function close($con) {
        mysqli_close($con);
    }

    /**
     * open the mysql connection
     * @return mysql connection
     */
    public function open() {
        $con = mysqli_connect("localhost", "root", "", "ticketchai");
        // $con = mysqli_connect("localhost", "root", "", "ticketchai");
        return $con;
    }

    /**
     * if the object is exists
     * @param type $object
     * @param type $object_array
     * @return int
     */
    public function CheckExistsWithArray($object, $object_array) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        if (count($object_array) <= 1) {
            foreach ($object_array as $col => $val) {
                if ($count++ != 0)
                    $fields .= ', ';
                $col = mysqli_real_escape_string($con, $col);
                $val = mysqli_real_escape_string($con, $val);
                $fields .= "`$col` = '$val'";
            }
        }
        $query = "SELECT * FROM `$object` WHERE $fields";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
            if ($count >= 1) {
                return 1;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }

    public function CheckExistsWithQuery($queryString) {
        $count = 0;
        $fields = '';
        $con = $this->open();


        $result = mysqli_query($con, $queryString);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
            if ($count >= 1) {
                return 1;
            } else {
                return 0;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }

    public function CheckExistsWithCondition($object, $condition) {
        $count = 0;
        $fields = '';
        $con = $this->open();

        $query = "SELECT * FROM `$object` WHERE $condition";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
            if ($count >= 1) {
                return 1;
            } else {
                return 0;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }

    /**
     * 
     * @param type $object
     * @param type $object_array
     * @param type $unique_column
     * @return json/array
     * @example path 
     */
    public function Insert($object = '', $object_array = '', $unique_column = '', $condition = '', $return_type = '') {
        $count = 0;
        $fields = '';
        //$object_array = array_slice($object_array, 0, count($object_array) - 1);
        $con = $this->open();
        $exists_array;
        foreach ($object_array as $col => $val) {
            if (empty($val)) {
                $col_render = '';
                if (preg_match('#^[a-zA-Z1-9]+_[a-zA-Z1-9]+$#', $col)) {
                    $col_render = ucfirst(str_replace('_', ' ', $col));
                } else {
                    $col_render = ucfirst($col);
                }
                if ($return_type == "array") {
                    return array("output" => "error", "msg" => $col_render . " is empty");
                } else if ($return_type == "json") {
                    return json_encode(array("output" => "error", "msg" => $col_render . " is empty"));
                } else {
                    return $col_render . " is empty";
                }
            }
            if ($val == "option_0") {
                $col_render = '';
                if (preg_match('#^[a-zA-Z1-9]+_[a-zA-Z1-9]+$#', $col)) {

                    $col_render = ucfirst(str_replace('_', ' ', $col));
                } else {
                    $col_render = ucfirst($col);
                }
                if ($return_type == "array") {
                    return array("output" => "error", "msg" => $col_render . " is unselected");
                } else if ($return_type == "json") {
                    return json_encode(array("output" => "error", "msg" => $col_render . " is unselected"));
                } else {
                    return $col_render . " is unselected";
                }
                //return $col_render . " is unselected";
            }
            if ($val == "check_0") {
                $col_render = '';

                if (preg_match('#^[a-zA-Z1-9]+_[a-zA-Z1-9]+$#', $col)) {
                    $col_render = ucfirst(str_replace('_', ' ', $col));
                } else {
                    $col_render = ucfirst($col);
                }
                if ($return_type == "array") {
                    return array("output" => "error", "msg" => $col_render . " is unchecked");
                } else if ($return_type == "json") {
                    return json_encode(array("output" => "error", "msg" => $col_render . " is unchecked"));
                } else {
                    return $col_render . " is unchecked";
                }

                //return $col_render . " is unchecked";
            }
            if ($val == "radio_0") {
                $col_render = '';

                if (preg_match('#^[a-zA-Z1-9]+_[a-zA-Z1-9]+$#', $col)) {
                    $col_render = ucfirst(str_replace('_', ' ', $col));
                } else {
                    $col_render = ucfirst($col);
                }
                if ($return_type == "array") {
                    return array("output" => "error", "msg" => $col_render . " is checked");
                } else if ($return_type == "json") {
                    return json_encode(array("output" => "error", "msg" => $col_render . " is checked"));
                } else {
                    return $col_render . " is unchecked";
                }
            }
            if (!empty($unique_column)) {
                if ($col == $unique_column) {
                    $exists_array = array("$col" => $val);
                }
            }
            if ($count++ != 0)
                $fields .= ', ';
            $col = mysqli_real_escape_string($con, $col);
            $val = mysqli_real_escape_string($con, $val);

            $fields .= "`$col` = '$val'";
        }
        if (!empty($unique_column)) {
            if ($this->CheckExistsWithArray($object, $exists_array) == 1) {
                if (preg_match('#^[a-zA-Z1-9]+_[a-zA-Z1-9]+$#', $unique_column)) {
                    $unique_column = ucfirst(str_replace('_', ' ', $unique_column));
                }
                if ($return_type == "array") {
                    return array("output" => "error", "msg" => ucfirst(str_replace('_', ' ', $unique_column)) . " already exists");
                } else if ($return_type == "json") {
                    return json_encode(array("output" => "error", "msg" => ucfirst(str_replace('_', ' ', $unique_column)) . " already exists"));
                } else {
                    return ucfirst(str_replace('_', ' ', $unique_column))  . " already exists";
                }
            }
        }
        if (!empty($condition)) {
            if ($this->CheckExistsWithCondition($object, $condition) == 1) {

                if ($return_type == "array") {
                    return array("output" => "error", "msg" => "Record already exists");
                } else if ($return_type == "json") {
                    return json_encode(array("output" => "error", "msg" => "Record already exists"));
                } else {
                    return "Record" . " already exists";
                }
            }
        }

          $query = "INSERT INTO `$object` SET $fields";

        $result = mysqli_query($con, $query);
        if ($result) {

            $last_inserted_id = mysqli_insert_id($con);
            $primarykey = $this->PrimaryKey($object);
            $insertedObject = $this->SelectAll($object, "", array("$primarykey" => $last_inserted_id), "", "array");
            $this->close($con);
            if ($return_type == "array") {
                if (preg_match('#^[a-zA-Z1-9]+_[a-zA-Z1-9]+$#', $object)) {
                    $object = ucfirst(str_replace('_', ' ', $object));
                }
                return array("output" => "success", "msg" => ucfirst($object) . " Successfully saved", "id" => $last_inserted_id, "object" => $insertedObject, "result" => $result);
            } else if ($return_type == "json") {
                if (preg_match('#^[a-zA-Z1-9]+_[a-zA-Z1-9]+$#', $object)) {
                    $object = ucfirst(str_replace('_', ' ', $object));
                }
                return json_encode(array("output" => "success", "msg" => ucfirst($object) . " Successfully saved", "id" => $last_inserted_id, "object" => $insertedObject, "result" => $result));
            } else {
                if (preg_match('#^[a-zA-Z1-9]+_[a-zA-Z1-9]+$#', $object)) {
                    $object = ucfirst(str_replace('_', ' ', $object));
                }
                return array("output" => "success", "msg" => ucfirst($object) . " Successfully saved", "id" => $last_inserted_id, "object" => $insertedObject, "result" => $result);
            }
        } else {

            $exception = mysqli_error($con);
            $this->close($con);
            if ($return_type == "array") {
                return array("output" => "error", "msg" => $exception);
            } else if ($return_type == "json") {
                return json_encode(array("output" => "error", "msg" => $exception));
            } else {
                return $exception;
            }
        }
    }

    
    
    public function InsertClear($object = '', $object_array = '', $unique_column = '', $condition = '', $return_type = '') {
        $count = 0;
        $fields = '';
        //$object_array = array_slice($object_array, 0, count($object_array) - 1);
        $con = $this->open();
        $exists_array;
        foreach ($object_array as $col => $val) {
            
            if ($count++ != 0)
                $fields .= ', ';
            $col = mysqli_real_escape_string($con, $col);
            $val = mysqli_real_escape_string($con, $val);
            if($val == NULL OR $val == ""){
                $fields .= "`$col` = NULL";
            } else {
                $fields .= "`$col` = '$val'";
            }
            
        }
        if (!empty($unique_column)) {
            if ($this->CheckExistsWithArray($object, $exists_array) == 1) {
                if (preg_match('#^[a-zA-Z1-9]+_[a-zA-Z1-9]+$#', $unique_column)) {
                    $unique_column = ucfirst(str_replace('_', ' ', $unique_column));
                }
                if ($return_type == "array") {
                    return array("output" => "error", "msg" => ucfirst(str_replace('_', ' ', $unique_column)) . " already exists");
                } else if ($return_type == "json") {
                    return json_encode(array("output" => "error", "msg" => ucfirst(str_replace('_', ' ', $unique_column)) . " already exists"));
                } else {
                    return ucfirst(str_replace('_', ' ', $unique_column))  . " already exists";
                }
            }
        }
        if (!empty($condition)) {
            if ($this->CheckExistsWithCondition($object, $condition) == 1) {

                if ($return_type == "array") {
                    return array("output" => "error", "msg" => "Record already exists");
                } else if ($return_type == "json") {
                    return json_encode(array("output" => "error", "msg" => "Record already exists"));
                } else {
                    return "Record" . " already exists";
                }
            }
        }

          $query = "INSERT INTO `$object` SET $fields";

        $result = mysqli_query($con, $query);
        if ($result) {

            $last_inserted_id = mysqli_insert_id($con);
            $primarykey = $this->PrimaryKey($object);
            $insertedObject = $this->SelectAll($object, "", array("$primarykey" => $last_inserted_id), "", "array");
            $this->close($con);
            if ($return_type == "array") {
                if (preg_match('#^[a-zA-Z1-9]+_[a-zA-Z1-9]+$#', $object)) {
                    $object = ucfirst(str_replace('_', ' ', $object));
                }
                return array("output" => "success", "msg" => ucfirst($object) . " Successfully saved", "id" => $last_inserted_id, "object" => $insertedObject, "result" => $result);
            } else if ($return_type == "json") {
                if (preg_match('#^[a-zA-Z1-9]+_[a-zA-Z1-9]+$#', $object)) {
                    $object = ucfirst(str_replace('_', ' ', $object));
                }
                return json_encode(array("output" => "success", "msg" => ucfirst($object) . " Successfully saved", "id" => $last_inserted_id, "object" => $insertedObject, "result" => $result));
            } else {
                if (preg_match('#^[a-zA-Z1-9]+_[a-zA-Z1-9]+$#', $object)) {
                    $object = ucfirst(str_replace('_', ' ', $object));
                }
                return array("output" => "success", "msg" => ucfirst($object) . " Successfully saved", "id" => $last_inserted_id, "object" => $insertedObject, "result" => $result);
            }
        } else {

            $exception = mysqli_error($con);
            $this->close($con);
            if ($return_type == "array") {
                return array("output" => "error", "msg" => $exception);
            } else if ($return_type == "json") {
                return json_encode(array("output" => "error", "msg" => $exception));
            } else {
                return $exception;
            }
        }
    }

    
    
    
    
    /**
     * Update the object
     * @param type $object
     * @param type $object_array
     */
    
     
    public function Update($object, $object_array, $duplicate_check_column = '', $condition = '', $return_type = '') {
        $con_key_from_arr = array_keys($object_array);
        $key = $con_key_from_arr[0];
        $value = array_shift($object_array);
        $fields = array();
        //$object_array = array_slice($object_array, 0, count($object_array) - 1);
        if (!empty($duplicate_check_column)) {
            $obj_arr = array("$duplicate_check_column" => $object_array["$duplicate_check_column"]);
            $rs = $this->CheckExistsWithArray($object, $obj_arr);
            if ($rs == 1) {
                unset($object_array["$duplicate_check_column"]);
            }
        }
        $con = $this->open();
        foreach ($object_array as $field => $val) {
            if (empty($val)) {
                $col_render = '';
                if (preg_match('#^[a-zA-Z1-9]+_[a-zA-Z1-9]+$#', $field)) {
                    $col_render = ucfirst(str_replace('_', ' ', $field));
                } else {
                    $col_render = ucfirst($field);
                }
                if ($return_type == "array") {
                    return array("output" => "error", "msg" => $col_render . " is empty");
                } else if ($return_type == "json") {
                    return json_encode(array("output" => "error", "msg" => $col_render . " is empty"));
                } else {
                    return $col_render . " is empty";
                }
                //return $col_render . " is empty";
            }
            if ($val == "option_0") {
                $col_render = '';
                if (preg_match('#^[a-zA-Z1-9]+_[a-zA-Z1-9]+$#', $col)) {
                    $col_render = ucfirst(str_replace('_', ' ', $col));
                } else {
                    $col_render = ucfirst($field);
                }
                if ($return_type == "array") {
                    return array("output" => "error", "msg" => $col_render . " is unselected");
                } else if ($return_type == "json") {
                    return json_encode(array("output" => "error", "msg" => $col_render . " is unselected"));
                } else {
                    return $col_render . " is unselected";
                }
            }
            if ($val == "check_0") {
                $col_render = '';
                if (preg_match('#^[a-zA-Z1-9]+_[a-zA-Z1-9]+$#', $col)) {
                    $col_render = ucfirst(str_replace('_', ' ', $col));
                } else {
                    $col_render = ucfirst($field);
                }
                if ($return_type == "array") {
                    return array("output" => "error", "msg" => $col_render . " is unchecked");
                } else if ($return_type == "json") {
                    return json_encode(array("output" => "error", "msg" => $col_render . " is unchecked"));
                } else {
                    return $col_render . " is unchecked";
                }

                //return $col_render . " is unchecked";
            }
            if ($val == "radio_0") {
                $col_render = '';
                if (preg_match('#^[a-zA-Z1-9]+_[a-zA-Z1-9]+$#', $col)) {
                    $col_render = ucfirst(str_replace('_', ' ', $col));
                } else {
                    $col_render = ucfirst($field);
                }
                if ($return_type == "array") {
                    return array("output" => "error", "msg" => $col_render . " is unchecked");
                } else if ($return_type == "json") {
                    return json_encode(array("output" => "error", "msg" => $col_render . " is unchecked"));
                } else {
                    return $col_render . " is unchecked";
                }

                //return $col_render . " is unchecked";
            }

            $fields[] = "$field = '" . mysqli_real_escape_string($con, $val) . "'";
        }
        $query = "";

        if (!empty($condition)) {
            $query .= "UPDATE `$object` SET " . join(', ', $fields) . " WHERE  $condition";
        } else {
            $query = "UPDATE `$object` SET " . join(', ', $fields) . " WHERE $key = '$value'";
        }


        $result = mysqli_query($con, $query);
        if ($result) {
            $updateObject = $this->SelectAll($object, "", array("$key" => "$value"), "", "array");
            $this->close($con);
            if ($return_type == "array") {
                return array("output" => "success", "msg" => $object . " is update successfully", "object" => $updateObject, "result" => $result);
            } else if ($return_type == "json") {
                return json_encode(array("output" => "success", "msg" => $object . " is update successfully", "object" => $updateObject, "result" => $result));
            } else {
                return array("output" => "success", "msg" => $object . " is update successfully", "object" => $updateObject, "result" => $result);
            }

            //return ucfirst($object) . " is save in database";
        } else {
            $exception = mysqli_error($con);
            $this->close($con);
            if ($return_type == "array") {
                return array("output" => "error", "msg" => $exception);
            } else if ($return_type == "json") {
                return json_encode(array("output" => "error", "msg" => $exception));
            } else {
                return $exception;
            }
        }
    }

    /**
     * Delete the object from database
     * @param type $object
     * @param type $object_array
     * @return string|\Exception
     */
    public function Delete($object, $object_array = '', $return_type = '') {

        $query = '';
        if (!empty($object_array)) {
            $count = 0;
            $fields = '';
            $con = $this->open();
            if (count($object_array) <= 1) {
                foreach ($object_array as $col => $val) {
                    if ($count++ != 0)
                        $fields .= ', ';
                    $col = mysqli_real_escape_string($con, $col);
                    $val = mysqli_real_escape_string($con, $val);
                    $fields .= "`$col` = '$val'";
                }
            }
            $query = "Delete FROM `$object` WHERE $fields";
        }
        else {
            if ($return_type == "array") {
                return array("output" => "error", "msg" => $object . " not deleted because of selection");
            } else if ($return_type == "json") {
                return json_encode(array("output" => "error", "msg" => $object . " not deleted because of selection"));
            } else {
                return array("output" => "error", "msg" => $object . " not deleted because of selection");
            }
        }
        if (mysqli_query($con, $query)) {
            //$objects = $this->SelectAll($object,"","","array");
            $this->close($con);
            if ($return_type == "array") {
                return array("output" => "success", "msg" => $object . " is Delete successfully");
            } else if ($return_type == "json") {
                return json_encode(array("output" => "success", "msg" => $object . " is Delete successfully"));
            } else {
                return array("output" => "success", "msg" => $object . " is Delete successfully");
            }
            //return ucfirst($object) . " is Delete From database";
        } else {
            $exception = mysqli_error($con);
            $this->close($con);
            if ($return_type == "array") {
                return array("output" => "error", "msg" => $exception);
            } else if ($return_type == "json") {
                return json_encode(array("output" => "error", "msg" => $exception));
            } else {
                return $exception;
            }
        }
    }

    public function DeleteByCondition($object, $condition = '', $return_type = '') {
        $query = '';
        if (!empty($condition)) {
            $query = "Delete FROM `$object` WHERE $condition";
        }
        if (mysqli_query($con, $query)) {
            $objects = $this->SelectAll($object, "", "", "array");
            $this->close($con);
            if ($return_type == "array") {
                return array("output" => "success", "msg" => $object . " is Delete successfully", "object" => $objects);
            } else if ($return_type == "json") {
                return json_encode(array("output" => "success", "msg" => $object . " is Delete successfully", "object" => $objects));
            } else {
                return array("output" => "success", "msg" => $object . " is Delete successfully", "object" => $objects);
            }
        } else {
            $exception = mysqli_error($con);
            $this->close($con);
            if ($return_type == "array") {
                return array("output" => "error", "msg" => $exception);
            } else if ($return_type == "json") {
                return json_encode(array("output" => "error", "msg" => $exception));
            } else {
                return $exception;
            }
        }
    }

    /**
     * Using the single table to return the result as stdClass array
     * @param type $object
     * @param type $condition
     * @param type $object_array
     * @return stdClass array
     */
    public function SelectAll($object, $condition = '', $object_array = '', $return_type = '') {
        $con = $this->open();
        $query = "";
        if (!empty($condition)) {
            $query = "SELECT * FROM `$object` WHERE  $condition";
        } else if (!empty($object_array)) {
            if (count($object_array) <= 1) {
                $count = 0;
                $fields = '';
                foreach ($object_array as $col => $val) {
                    if ($count++ != 0)
                        $fields .= ', ';
                    $col = mysqli_real_escape_string($con, $col);
                    $val = mysqli_real_escape_string($con, $val);
                    $fields .= "`$col` = '$val'";
                }
            }
            $query = "SELECT * FROM `$object` WHERE $fields";
        }
        else {
            $query = "SELECT * FROM `$object`";
        }

        //echo $query;
        //exit();
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $objects = array();
            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                if ($return_type == "array") {
                    return $objects;
                } else if ($return_type == "json") {
                    $return_arr = array();
                    foreach ($objects as $obj) {
                        $push_arr = array();
                        foreach ($objects{0} as $key => $val) {
                            $push_arr["$key"] = utf8_encode($obj->{"$key"});
                        }
                        array_push($return_arr, $push_arr);
                    }
                    return json_encode($return_arr);
                } else {
                    return $objects;
                }
            }
        } else {
            $exception = mysqli_error($con);
            $this->close($con);
            if ($return_type == "array") {
                return array("output" => "error", "msg" => $exception);
            } else if ($return_type == "json") {
                return json_encode(array("output" => "error", "msg" => $exception));
            } else {
                return $exception;
            }
        }
    }

    public function PrimaryKey($object) {
        $open = $this->open();
        $r = mysqli_fetch_object(mysqli_query($open, "SHOW KEYS FROM `$object` WHERE Key_name = 'PRIMARY'"));
        $this->close($open);
        return $r->Column_name;
    }

    public function QueryWithQueryString($queryString) {
        $con = $this->open();
        $result = mysqli_query($con, $queryString);
        if ($result) {
            $this->close($con);
            return $result;
        } else {

            $exception = mysqli_error($con);
            $this->close($con);
            return $exception . " Exception";
        }
    }

    public function ReturnObjectByQuery($queryString, $return_type = '') {
        $con = $this->open();
        $result = mysqli_query($con, $queryString);
        if ($result) {
            $objects = array();
            while ($rows = $result->fetch_object()) {
                $objects[] = $rows;
            }
            $this->close($con);
            if (empty($return_type)) {
                return $objects;
            } elseif ($return_type == "array") {
                return $objects;
            } elseif ($return_type == "json") {
                $return_arr = array();
                foreach ($objects as $obj) {
                    $push_arr = array();
                    foreach ($objects{0} as $key => $val) {
                        $push_arr["$key"] = utf8_encode($obj->{"$key"});
                    }
                    array_push($return_arr, $push_arr);
                }
                return json_encode($return_arr);
            }
        } else {

            $exception = mysqli_error($con);
            $this->close($con);
            return $exception . " Exception";
        }
    }
    
    
     public function InsertObjectByQuery($queryString) {
        $con = $this->open();
        $result = mysqli_query($con, $queryString);
        $lastID = mysqli_insert_id($con);
        if ($result) {
            $lastID = mysqli_insert_id($con);
            return $lastID;
        } else {
            $exception = mysqli_error($con);
            $this->close($con);
            return $exception . " Exception";
        }
    }
    
 



    public function DatabaseColumnNames($database_name, $table_name, $column_name = '', $return_type = '') {
        if (!empty($column_name)) {
            $con = $this->open();
            $queryString = "SELECT COLUMN_NAME FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '$database_name' AND TABLE_NAME='$table_name' AND COLUMN_NAME='$column_name'";
            $result = mysqli_query($con, $queryString);
            if ($result) {
                $object = array();
                while ($rows = mysqli_fetch_assoc($result)) {
                    array_push($object, $rows["COLUMN_NAME"]);
                    //array_push($object, $rows);
                }
                $this->close($con);
                if ($return_type == '') {
                    return $object;
                } else if ($return_type == 'json') {
                    $return_arr = array();
                    foreach ($object as $key => $val) {
                        array_push($return_arr, utf8_encode($val));
                    }
                    return json_encode($return_arr);
                } else if ($return_type == 'array') {
                    return $object;
                } else {
                    return $object;
                }
            } else {

                $exception = mysqli_error($con);
                $this->close($con);
                return $exception . " Exception";
            }
        } else {
            $con = $this->open();
            $queryString = "SELECT COLUMN_NAME FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '$database_name' AND TABLE_NAME='$table_name'";
            $result = mysqli_query($con, $queryString);
            if ($result) {
                $object = array();
                while ($rows = mysqli_fetch_assoc($result)) {
                    array_push($object, $rows["COLUMN_NAME"]);
                    //array_push($object, $rows);
                }
                $this->close($con);
                if ($return_type == '') {
                    return $object;
                } else if ($return_type == 'json') {
                    $return_arr = array();
                    foreach ($object as $key => $val) {
                        array_push($return_arr, utf8_encode($val));
                    }
                    return json_encode($return_arr);
                } else if ($return_type == 'array') {
                    return $object;
                } else {
                    return $object;
                }
            } else {

                $exception = mysqli_error($con);
                $this->close($con);
                return $exception . " Exception";
            }
        }
    }

    public function one_many_relation_fatchAssoc($object_array, $fields_array, $condition = '', $return_type = '') {
        $query_string = "SELECT ";

        $fields = "";
        if (!empty($fields_array)) {
            if (is_array($fields_array)) {
                if (count($fields_array) >= 1) {
                    foreach ($fields_array as $key => $val) {
                        $myfields = explode(',', $val);
                        if (is_array($myfields)) {
                            if (count($myfields) >= 1) {
                                foreach ($myfields as $f) {
                                    if (empty($f)) {
                                        $fields.=$key . "." . "*" . ",";
                                        break;
                                    } else {
                                        $fields.=$key . "." . $f . ",";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $fields = substr($fields, 0, -1);
        $query_string .= " $fields FROM ";
        $tables = " ";
        $tables_conditions = " ";
        if (is_array($object_array)) {
            if (count($object_array) >= 1) {

                foreach ($object_array as $val) {
                    $tables .= "`" . $val . "`" . ",";
                    if ($val != $object_array[0])
                        $tables_conditions .="  " . $object_array[0] . "." . $this->PrimaryKey($val) . "=" . $val . "." . $this->PrimaryKey($val) . "  AND";
                }
            }
            $tables = substr($tables, 0, -1);
            $tables_conditions = substr($tables_conditions, 0, -4);
        }

        $query_string .= " $tables  WHERE  $tables_conditions";

        if (!empty($condition)) {
            $query_string .= " " . $condition;
        }

        //--------------return the result of selected table fields---------//
        //echo $query_string;

        $con = $this->open();
        $result = mysqli_query($con, $query_string);
        if ($result) {
            $count = mysqli_num_rows($result);
            $objects = array();
            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_assoc()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                if ($return_type == "array") {
                    return $objects;
                } else if ($return_type == "json") {
                    $return_arr = array();
                    foreach ($objects as $skey => $obj) {
                        $push_arr = array();

                        foreach ($objects{0} as $key => $val) {
                            $push_arr["$key"] = utf8_encode($objects["$skey"]["$key"]);
                        }
                        array_push($return_arr, $push_arr);

//                        echo "<pre>";
//                        print_r($push_arr);
//                        echo "</pre>";
                    }
                    return json_encode($return_arr);
                } else {
                    return $objects;
                }
            }
        } else {
            $exception = mysqli_error($con);
            $this->close($con);
            if ($return_type = 'array') {
                return array("output" => "error", "msg" => $exception);
            } else if ($return_type = 'json') {
                return json_encode(array("output" => "error", "msg" => $exception));
            } else {
                return $exception;
            }
        }

        //----------- end return the result of selected table fields-------//
    }

    public function one_many_relation($object_array, $fields_array, $condition = '', $return_type = '') {
        $query_string = "SELECT ";

        $fields = "";
        if (!empty($fields_array)) {
            if (is_array($fields_array)) {
                if (count($fields_array) >= 1) {
                    foreach ($fields_array as $key => $val) {
                        $myfields = explode(',', $val);
                        if (is_array($myfields)) {
                            if (count($myfields) >= 1) {
                                foreach ($myfields as $f) {
                                    if (empty($f)) {
                                        $fields.=$key . "." . "*" . ",";
                                        break;
                                    } else {
                                        $fields.=$key . "." . $f . ",";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $fields = substr($fields, 0, -1);
        $query_string .= " $fields FROM ";
        $tables = " ";
        $tables_conditions = " ";
        if (is_array($object_array)) {
            if (count($object_array) >= 1) {

                foreach ($object_array as $val) {
                    $tables .= "`" . $val . "`" . ",";
                    if ($val != $object_array[0])
                        $tables_conditions .="  " . $object_array[0] . "." . $this->PrimaryKey($val) . "=" . $val . "." . $this->PrimaryKey($val) . "  AND";
                }
            }
            $tables = substr($tables, 0, -1);
            $tables_conditions = substr($tables_conditions, 0, -4);
        }

        $query_string .= " $tables  WHERE  $tables_conditions";

        if (!empty($condition)) {
            $query_string .= " " . $condition;
        }


        //--------------return the result of selected table fields---------//
        //echo $query_string;
     
        $con = $this->open();
        $result = mysqli_query($con, $query_string);
        if ($result) {
            $count = mysqli_num_rows($result);
            $objects = array();
            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                if ($return_type == "array") {
                    return $objects;
                } else if ($return_type == "json") {
                    $return_arr = array();
                    foreach ($objects as $obj) {
                        $push_arr = array();
                        foreach ($objects{0} as $key => $val) {
                            $push_arr["$key"] = utf8_encode($obj->{"$key"});
                        }
                        array_push($return_arr, $push_arr);
                    }
                    return json_encode($return_arr);
                } else {
                    return $objects;
                }
            }
        } else {
            $exception = mysqli_error($con);
            $this->close($con);
            if ($return_type == 'array') {
                return array("output" => "error", "msg" => $exception);
            } else if ($return_type == 'json') {
                return json_encode(array("output" => "error", "msg" => $exception));
            } else {
                return $exception;
            }
        }

        //----------- end return the result of selected table fields-------//
    }

    /**
     * 
     * @param type $object_array
     * @param type $fields_array
     * @param type $condition
     * @param type $return_type
     * @return object/array
     * @example path $con = new database();
      $object_array = array("tbl_country", "tbl_city");
      $fields_array = array("tbl_country"=>"country_id,country_name","tbl_city"=>"city_id,city_name");

      $con->n_1_relation_experimintal($object_array, $fields_array, "", "array"));

      echo $con->n_1_relation_experimintal($object_array, $fields_array, "", "json");
     */
    public function one_one_or_many_relation_experimintal($object_array = '', $fields_array = '', $condition = '', $return_type = '') {
        $query_string = "SELECT ";
        $fields = "";
        $fields = "";
        if (!empty($fields_array)) {
            if (is_array($fields_array)) {
                if (count($fields_array) >= 1) {
                    foreach ($fields_array as $key => $val) {
                        $myfields = explode(',', $val);
                        if (is_array($myfields)) {
                            if (count($myfields) >= 1) {
                                foreach ($myfields as $f) {
                                    if (empty($f)) {
                                        $fields.=$key . "." . "*" . ",";
                                        break;
                                    } else {
                                        $fields.=$key . "." . $f . ",";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $fields = substr($fields, 0, -1);

        $query_string .= " $fields FROM  ";
        $tables = "";
        if (is_array($object_array)) {
            if (count($object_array) >= 1) {
                foreach ($object_array as $arr) {
                    $tables .= $arr . ",";
                }
            }
            $tables = substr($tables, 0, -1);
        }
        $query_string .= $tables . " ";
        $tables_conditions = "";
        if (is_array($object_array)) {
            if (count($object_array) >= 1) {
                foreach ($object_array as $arr) {

                    if (!$arr[0])
                        $tables_conditions .= $arr[0] . "." . $this->PrimaryKey($arr) . "=" . $arr . "." . $this->PrimaryKey($arr) . "  AND";
                }
            }
            $tables_conditions = substr($tables_conditions, 0, -4);
        }
        $query_string .= " $tables_conditions ";
        if (!empty($condition)) {
            $query_string .= " " . $condition;
        }


        //--------------return the result of selected table fields---------//
        $con = $this->open();
        $result = mysqli_query($con, $query_string);
        if ($result) {
            $count = mysqli_num_rows($result);
            $objects = array();
            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                if ($return_type == "array") {
                    return $objects;
                } else if ($return_type == "json") {
                    $return_arr = array();
                    foreach ($objects as $obj) {
                        $push_arr = array();
                        foreach ($objects{0} as $key => $val) {
                            $push_arr["$key"] = utf8_encode($obj->{"$key"});
                        }
                        array_push($return_arr, $push_arr);
                    }
                    return json_encode($return_arr);
                } else {
                    return $objects;
                }
            }
        } else {
            $exception = mysqli_error($con);
            $this->close($con);
            if ($return_type == "array") {
                return array("output" => "error", "msg" => $exception);
            } else if ($return_type == "json") {
                return json_encode(array("output" => "error", "msg" => $exception));
            } else {
                return $exception;
            }
        }

        //----------- end return the result of selected table fields-------//
    }

    /**
     * 
     * @param type $object_array
     * @param type $fields_array
     * @param type $condition
     * @param type $return_type
     * @return array/json
     * @example path ()
     */
    public function one_many_drildown_relation_experimintal($object_array = '', $fields_array = '', $condition = '', $return_type = '') {

        /** use for field to show * */
        $query_string = "SELECT ";
        $fields = "";
        if (!empty($fields_array)) {
            if (is_array($fields_array)) {
                if (count($fields_array) >= 1) {
                    foreach ($fields_array as $key => $val) {
                        $myfields = explode(',', $val);
                        if (is_array($myfields)) {
                            if (count($myfields) >= 1) {
                                foreach ($myfields as $f) {
                                    if (empty($f)) {
                                        $fields.=$key . "." . "*" . ",";
                                        break;
                                    } else {
                                        $fields.=$key . "." . $f . ",";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $fields = substr($fields, 0, -1);

        $query_string .= " $fields FROM  ";
        /** end use for field to show * */
        /** start table name to add in quey * */
        $table = "";
        if (!empty($object_array)) {
            if (is_array($object_array)) {
                if (count($object_array) >= 1) {
                    foreach ($object_array as $key => $val) {

                        $tables = explode(',', $val);

                        if (is_array($tables)) {
                            if (count($tables) >= 1) {
                                foreach ($tables as $f) {

                                    if ($f != $tables[0])
                                        $tables_conditions .= "  " . $tables[0] . "." . $this->PrimaryKey($arr) . "=" . $f . "." . $this->PrimaryKey($arr) . "  AND";
                                }
                            }
                        }
                    }
                }
            }
        }

        /** end table name to add in quey * */
        /** use for table condition */
        $table = array();
        $tables_conditions = " ";
        if (!empty($object_array)) {
            if (is_array($object_array)) {
                if (count($object_array) >= 1) {
                    foreach ($object_array as $key => $val) {

                        $tables = explode(',', $val);

                        if (is_array($tables)) {
                            if (count($tables) >= 1) {
                                foreach ($tables as $f) {

                                    array_push($table, $f);
                                    if ($f != $tables[0])
                                        $tables_conditions .= "  " . $tables[0] . "." . /*                                                 * $this->ObjectPrimaryKey($arr) */"id" . "=" . $f . "." . /*                                                 * $this->ObjectPrimaryKey($arr) */"id" . "  AND";
                                }
                            }
                        }
                    }
                }
            }
        }
        $tables_conditions = substr($tables_conditions, 0, -4);
        /** start table name to add in quey * */
        $unique_tables = array_unique($table, SORT_REGULAR);
        $TablesNames = "";
        if (is_array($unique_tables)) {
            if (count($unique_tables) >= 1) {
                foreach ($unique_tables as $arr) {
                    $TablesNames .= $arr . ",";
                }
            }
            $TablesNames = substr($TablesNames, 0, -1);
        }
        /** end table name to add in quey * */
        $query_string .= " $TablesNames WHERE $tables_conditions ";
        /** end use for table condition * */
        /** start condition * */
        if (!empty($condition)) {
            $query_string .= " " . $condition;
        }
        /** End condition * */
        //return $query_string;
        //--------------return the result of selected table fields---------//
        $con = $this->open();
        $result = mysqli_query($con, $query_string);
        if ($result) {
            $count = mysqli_num_rows($result);
            $objects = array();
            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                if ($return_type == "array") {
                    return $objects;
                } else if ($return_type == "json") {
                    $return_arr = array();
                    foreach ($objects as $obj) {
                        $push_arr = array();
                        foreach ($objects{0} as $key => $val) {
                            $push_arr["$key"] = utf8_encode($obj->{"$key"});
                        }
                        array_push($return_arr, $push_arr);
                    }
                    return json_encode($return_arr);
                } else {
                    return $objects;
                }
            }
        } else {
            $exception = mysqli_error($con);
            $this->close($con);
            return json_encode(array("error" => "yes", "msg" => $exception . " Exception"));
        }
        //----------- end return the result of selected table fields-------//
    }

}

//-- 386/3A akota sorok, khan monjil--//
//-- kilgong model
//$con = new database();
//echo "<pre>";
//print_r( $con->PrimaryKey("country"));
//echo "</pre>";
//$object_array = array("city", "country");
//$fields_array = array("city"=>"city_id,city_name","country"=>"country_id,country_name");
//echo $con->one_many_relation_fatchAssoc($object_array, $fields_array, "", "json");
//echo "<pre>";
//print_r($con->n_1_relation_experimintal($object_array, $fields_array, "", "array"));
//echo "</pre>";
//echo $con->DatabaseColumnNames("price_matrix", "country", "", "json");
//echo $con->one_many_relation($object_array, $fields_array, "", "json");
 //echo $con->ReturnObjectByQuery("SELECT * FROM employee", "json");
/** test for  select All function */

//echo "<pre>";
//print_r($con->SelectAll("employee_role", "", "", "array"));
//echo "</pre>";

//echo $con->SelectAll("role", "", "", "json");

/** t
 * Test for  select All function */


/** Start test for one_many relation */


/** End test for one_many relation */