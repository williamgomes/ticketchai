<?php

include("../../config/class.web.config.php");
$con = new Config();
$err = '';
$msg = '';
/* Start Authentication */
if ($con->authenticate() == 1) {
    $con->redirect("../../login.php");
}
/* End Authentication */
/*  Start Vendor Fields */
$vendor_id = "";
$vendor_name = "";
$vendor_phone = "";
$vendor_email = "";
$vendor_address = "";
$status_id = "";
$is_active = "";
$organization_id = "";
/*  End Vendor Fields */

if (isset($_POST["btnCreateVendor"])) {
    extract($_POST);
    $is_active = $_POST["status_id"];
    if ($_POST["vendor_name"] == "") {
        $err = "Vendor Name Required";
    } else {
        $insert_array = array(
            "vendor_name" => $vendor_name,
            "vendor_phone" => $vendor_phone,
            "vendor_email" => $vendor_email,
            "vendor_address" => $vendor_address,
            "organization_id" => $organization_id,
            "is_active" => $is_active
        );
//        $con->debug($insert_array);
//        exit();
        $result = $con->Insert("vendor", $insert_array, "", "", "array");
//        $con->debug($result);
//        exit();
        if ($result["output"] == "success") {
            $msg = "Vendor Saved Successfully";
        } else {
            $err = "WRONG";
        }
    }
}
?>
<?php include '../layout/header_script.php'; ?>
<body>
    <div class="mainwrapper">
        <?php include '../layout/header_login_info.php'; ?>
        <div class="leftpanel">
            <?php include '../layout/left_navigation.php'; ?>
        </div>
        <div class="rightpanel">
            <ul class="breadcrumbs">
                <li><a href="dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
                <li>Vendor Management <span class="separator"></span></li>
                <li>Vendor Registration</li>
            </ul>

            <div class="maincontent">
                <div class="maincontentinner">
                    <div class="row-fluid">
                        <script type="text/javascript" src="<?php echo $con->baseUrl("lib/jquery.maskedinput-1.3.1.min_.js"); ?>"></script>
                        <form method="post">
                            <div class="k-block">
                                <div class="maincontentinner">
                                    <div class="row-fluid">
                                        <?php echo(!empty($msg) ? $con->notification($msg, '') : $con->notification('', $err)); ?>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="span3">Organization Name:</div>
                                            <div class="span9"><?php echo $con->SelectBox("organization_id", "organization_id", "", "organization_id", "organization_name", $organization_id, "", "organization_controller.php", "kendo", "width:60%", ""); ?></div>
                                        </div>
                                        <div class="span6">
                                            <div class="span3">Vendor Name:</div>
                                            <div class="span9"><?php echo $con->TextBox("vendor_name", "vendor_name", "width:60%", "", "", $vendor_name, "kendo"); ?></div>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="span3">Mobile Number:</div>
                                            <div class="span9" style="margin-top: 4px !important;"><?php echo $con->TextBox("vendor_phone", "vendor_phone", "width:60%", "---.--------", "", $vendor_phone, "kendo"); ?></div>
                                            <script type="text/javascript">
                                                $(document).ready(function () {
                                                    $('#vendor_phone').mask('999-99999999');
                                                });
                                            </script>
                                        </div>
                                        <div class="span6">
                                            <div class="span3">Email Address:</div>
                                            <div class="span9" style="margin-top: 4px !important;"><?php echo $con->TextBox("vendor_email", "vendor_email", "width:60%", "e.g:someone@email.com", "", $vendor_email, "kendo"); ?></div>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="span3">Vendor Address:</div>
                                            <div class="span9"><?php echo $con->TextArea("vendor_address", "vendor_address", $vendor_address, "2", "29", "resize: none;border-style:none;border:1px solid #dbdbde;", ""); ?></div>
                                        </div>
                                        <div class="span6">
                                            <div class="span3">Status:</div>
                                            <div class="span9"><?php echo $con->SelectBox("status_id", "status_id", "", "status_id", "status_name", $is_active, "", "status_controller.php", "kendo", "width:60%;", ""); ?></div>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row-fluid">
                                        <div class="span12 pull-left">
                                            <input type="submit" value="Create Vendor"  name="btnCreateVendor" class="k-button btn-success"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>  
                    </div>
<!--                    <div style="padding-bottom:100px;"></div>               -->
                </div>
            </div>
            <?php include '../layout/footer.php'; ?>
        </div>
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('.tabbedwidget').tabs();
        });
        jQuery('#1').addClass('active');
    </script>
</body>
</html>