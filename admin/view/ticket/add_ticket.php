<?php
session_start();
include("../../config/class.web.config.php");
$con = new Config();
/* Start Authentication */
if ($con->authenticate() == 1) {
    $con->redirect("../../login.php");
}
/* End Authentication */
$err = '';
$msg = '';
/* Start Ticket Fields Initialization */
$event_id = "";
$ticket_type_id = "";
$ticket_total = "";
$ticket_price = "";
$discount_price = "";
$ticket_price = "";
$ticket_available = "";
$ticket_limit = "";
$vendor_id = "";
$total_vticket = "";
$total_eticket = "";
$total_pticket = "";
/* End Ticket Fields Initialization */
if (isset($_POST["btnCreateEventTicket"])) {
    extract($_POST);

    if ($_POST["vendor_id"] == "") {
        $vendor_id = 'NULL';
    }
    if ($_POST["total_vticket"] == "") {
        $total_vticket = 'NULL';
    }
    if ($_POST["total_eticket"] == "") {
        $total_eticket = 'NULL';
    }
    if ($_POST["total_pticket"] == "") {
        $total_pticket = 'NULL';
    }
    if ($_POST["event_id"] == "") {
        $err = "Select Event Title";
    } elseif ($_POST["ticket_type_id"] == "") {
        $err = "Select Ticket Type";
    } elseif ($_POST["ticket_type_id"] == 1) {
        if ($_POST["ticket_total"] == "") {
            $err = "Total Ticket";
        } elseif ($_POST["ticket_price"] == "") {
            $err = "Ticket Price";
        } elseif ($_POST["discount_price"] == "") {
            $err = "Discount Price";
        } else {
            $insert_array = array(
                "event_id" => $event_id,
                "ticket_type_id" => $ticket_type_id,
                "ticket_total" => $ticket_total,
                "ticket_price" => $ticket_price,
                "discount_price" => $discount_price,
                "vendor_id" => $vendor_id,
                "total_vticket" => $total_vticket,
                "total_eticket" => $total_eticket,
                "total_pticket" => $total_pticket
            );
//            $con->debug($insert_array);
//            exit();
            if (($con->CheckExistsWithArray("ticket", array("event_id" => $event_id)) >= 1)) {
                $err = "You are not permitted to create ticket for same event";
            } else {
                $result = $con->Insert("ticket", $insert_array, "", "", "array");
//            $con->debug($result);
//            exit();
                if ($result["output"] == "success") {
                    $msg = "Ticket Added Successfully.";
                } else {
                    $err = "ERROR";
                }
            }
        }
    } elseif ($_POST["ticket_type_id"] == 2) {
        if ($_POST["ticket_total"] == "") {
            $err = "Total Ticket";
        } elseif ($_POST["ticket_price"] == "") {
            $err = "Ticket Price";
        } elseif ($_POST["discount_price"] == "") {
            $err = "Discount Price";
        } elseif ($_POST["ticket_limit"] == "") {
            $err = "Enter Limit";
        } else {
            $insert_array = array(
                "event_id" => $event_id,
                "ticket_type_id" => $ticket_type_id,
                "ticket_total" => $ticket_total,
                "ticket_price" => $ticket_price,
                "discount_price" => $discount_price,
                "ticket_limit" => $ticket_limit,
                "vendor_id" => $vendor_id,
                "total_vticket" => $total_vticket,
                "total_eticket" => $total_eticket,
                "total_pticket" => $total_pticket
            );
            if (($con->CheckExistsWithArray("ticket", array("event_id" => $event_id)) >= 1)) {
                $err = "You are not permitted to create ticket for same event";
            } else {
                $result = $con->Insert("ticket", $insert_array, "", "", "array");
                if ($result["output"] == "success") {
                    $msg = "Ticket Added Successfully.";
                } else {
                    $err = "ERROR";
                }
            }
        }
    } else {
        $err = "Something went wrong with input.";
    }
}
?>
<?php include '../layout/header_script.php'; ?>
<body>
    <div class="mainwrapper">
        <?php include '../layout/header_login_info.php'; ?>
        <div class="leftpanel">
            <?php include '../layout/left_navigation.php'; ?>
        </div>
        <div class="rightpanel">
            <ul class="breadcrumbs">
                <li><a href="dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
                <li>Ticket Management <span class="separator"></span></li>
                <li>Add New Ticket</li>
            </ul>

            <div class="maincontent">
                <div class="maincontentinner">
                    <div class="row-fluid">
                        <form method="post">
                            <div class="k-block">
                                <div class="maincontentinner">
                                    <div class="row-fluid">
                                        <?php echo(!empty($msg) ? $con->notification($msg, '') : $con->notification('', $err)); ?>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="span3">Select Event Title :</div>
                                            <div class="span9"><?php echo $con->SelectBox("event_id", "event_id", "", "event_id", "event_title", $event_id, "", "event_controller.php", "kendo", "width:60%", ""); ?></div>
                                        </div>
                                        <div class="span6">
                                            <div class="span3">Select Ticket Type:</div>
                                            <div class="span9"><?php echo $con->SelectBox("ticket_type_id", "ticket_type_id", "", "ticket_type_id", "ticket_type_name", $ticket_type_id, "", "ticket_type_controller.php", "kendo", "width:60%", ""); ?></div>
                                        </div>
                                        <script type="text/javascript">
                                            $(document).ready(function () {
                                                $("#ticket_type_id").change(function () {
                                                    var ticket_type_id = $("#ticket_type_id").val();
                                                    if (ticket_type_id === "2") {
                                                        console.log(ticket_type_id);
                                                        $("#group_info").fadeIn("slow");
                                                    }
                                                    else
                                                    {
                                                        console.log(ticket_type_id);
                                                        $("#group_info").fadeOut("slow");
                                                    }
                                                });
                                            });
                                        </script>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="span3">Total Ticket:</div>
                                            <div class="span9"><?php echo $con->NumericTextBox("ticket_total", "ticket_total", "", "1", "10000", "", "width:60%", ""); ?></div>

                                        </div>
                                        <div class="span6">
                                            <div class="span3">Ticket Price:</div>
                                            <div class="span9"><?php echo $con->NumericTextBox("ticket_price", "ticket_price", "", "1", "10000", "", "width:60%", ""); ?></div>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="span3">Discount Price:</div>
                                            <div class="span9"><?php echo $con->NumericTextBox("discount_price", "discount_price", "", "1", "10000", "", "width:60%", ""); ?></div>
                                        </div>
                                        <div class="span6" id="group_info" style="display: none;">
                                            <div class="span3">Ticket Limit:</div>
                                            <div class="span9"><?php echo $con->NumericTextBox("ticket_limit", "ticket_limit", "", "1", "20", "", "width:60%", ""); ?></div>
                                        </div>

                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="span3">Is Vendor :</div>
                                            <div class="span9"><input type="checkbox" id="is_vendor"/>
                                                <script type="text/javascript">
                                                    $(document).ready(function () {
                                                        $("#is_vendor").change(function () {

                                                            if (this.checked) {
                                                                $("#vendor_info").fadeIn("slow");
                                                            }
                                                            else
                                                            {
                                                                $("#vendor_info").fadeOut("slow");
                                                            }
                                                        });
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="vendor_info" style="display: none;">
                                        <div class="row-fluid" >
                                            <div class="span6">
                                                <div class="span3">Select Vendor:</div>
                                                <div class="span9"><?php echo $con->SelectBox("vendor_id", "vendor_id", "", "vendor_id", "vendor_name", $vendor_id, "", "vendor_controller.php", "kendo", "width:60%", ""); ?></div>
                                            </div>
                                            <div class="span6">
                                                <div class="span3">Total Vendor Ticket:</div>
                                                <div class="span9"><?php echo $con->NumericTextBox("total_vticket", "total_vticket", "", "1", "10000", "", "width:60%", ""); ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="span3">Is E-Ticket :</div>
                                            <div class="span9"><input type="checkbox" id="is_eticket"/>
                                                <script type="text/javascript">
                                                    $(document).ready(function () {
                                                        $("#is_eticket").change(function () {

                                                            if (this.checked) {
                                                                $("#eticket_info").fadeIn("slow");
                                                            }
                                                            else
                                                            {
                                                                $("#eticket_info").fadeOut("slow");
                                                            }
                                                        });
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                        <div class="span6" id="eticket_info" style="display: none;">
                                            <div class="span3">Total E-Ticket:</div>
                                            <div class="span9"><?php echo $con->NumericTextBox("total_eticket", "total_eticket", "", "1", "10000", "", "width:60%", ""); ?></div>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="span3">Is Printed Ticket :</div>
                                            <div class="span9"><input type="checkbox" id="is_pticket"/>
                                                <script type="text/javascript">
                                                    $(document).ready(function () {
                                                        $("#is_pticket").change(function () {

                                                            if (this.checked) {
                                                                $("#pticket_info").fadeIn("slow");
                                                            }
                                                            else
                                                            {
                                                                $("#pticket_info").fadeOut("slow");
                                                            }
                                                        });
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                        <div class="span6" id="pticket_info" style="display: none;">
                                            <div class="span3">Total Printed Ticket:</div>
                                            <div class="span9"><?php echo $con->NumericTextBox("total_pticket", "total_pticket", "", "1", "10000", "", "width:60%", ""); ?></div>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row-fluid">
                                        <div class="span12 pull-left">
                                            <input type="submit" value="Create Event Ticket"  name="btnCreateEventTicket" class="k-button btn-success"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
<!--                    <div style="padding-bottom:100px;"></div>               -->
                </div>
            </div>
            <?php include '../layout/footer.php'; ?>
        </div>
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('.tabbedwidget').tabs();
        });
        jQuery('#1').addClass('active');
    </script>
</body>
</html>