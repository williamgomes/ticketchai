<?php
session_start();
include("../../config/class.web.config.php");
$con = new Config();
/* Start Authentication */
if ($con->authenticate() == 1) {
    $con->redirect("../../login.php");
}
/* End Authentication */
?>

<?php include '../layout/header_script.php'; ?>
<body>
    <div class="mainwrapper">
        <?php include '../layout/header_login_info.php'; ?>
        <div class="leftpanel">
            <?php include '../layout/left_navigation.php'; ?>
        </div>
        <div class="rightpanel">
            <ul class="breadcrumbs">
                <li><a href="dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
                <li>Synopsis Management <span class="separator"></span></li>
                <li>Synopsis List</li>
            </ul>
            <div class="maincontent">
                <div class="maincontentinner">
                    <div class="row-fluid">
                        <div class="k-grid  k-secondary" data-role="grid" style="">
                            <div class="k-toolbar k-grid-toolbar">
                                <a class="k-button k-button-icontext k-grid-add" href="add_synopsis.php">
                                    <span class="k-icon k-add"></span>
                                    Add New Synopsis
                                </a>
                            </div>
                        </div>
                        <div id="grid"></div>
                        <script type="text/javascript">
                            jQuery(document).ready(function () {
                                var dataSource = new kendo.data.DataSource({
                                    pageSize: 5,
                                    transport: {
                                        read: {
                                            url: "../../controller/synopsis_list_controller.php",
                                            type: "GET"
                                        }
                                    },
                                    autoSync: false,
                                    schema: {
                                        errors: function (e) {
                                            if (e.error === "yes")
                                            {
                                                var message = "";
                                                message += e.message;
                                                var window = jQuery("#kWindow");
                                                if (!window.data("kendoWindow")) {
                                                    window.kendoWindow({
                                                        title: "Error window",
                                                        modal: true,
                                                        height: 100,
                                                        width: 400
                                                    });
                                                }

                                                window.data("kendoWindow").center().open();
                                                window.html('<div style="margin-top:10px;text-align:center;"><span style="color:red;font-size:14px;">' + message + '</span></div>');
                                                this.cancelChanges();
                                            }
                                        },
                                        data: "data",
                                        total: "data.length",
                                        model: {
                                            id: "synopsis_id",
                                            fields: {
                                                synopsis_id: {editable: false, nullable: true},
                                                synopsis_genre: {type: "string"},
                                                synopsis_details: {type: "string"},
                                                synopsis_writer_name: {type: "string"},
                                                synopsis_director_name: {type: "string"},
                                                synopsis_release_date: {type: "string"},
                                                synopsis_duration: {type: "string"},
                                                event_id: {type: "number"},
                                                event_title: {type: "string"},
                                                language_id: {type: "number"},
                                                language_name: {type: "string"}
                                            }
                                        }
                                    }
                                });
                                jQuery("#grid").kendoGrid({
                                    dataSource: dataSource,
                                    filterable: true,
                                    pageable: {
                                        refresh: true,
                                        input: true,
                                        numeric: false,
                                        pageSizes: [5, 10, 20, 50]
                                    },
                                    sortable: true,
                                    groupable: true,
                                    resizable: true,
                                    //toolbar: [{name: "create", text: "Add New"}, "save", "cancel"],
                                    //toolbar: [{name: "create", text: "Add New Member"}],
                                    columns: [
                                        {field: "event_title", title: "Event Title"},
                                        {field: "synopsis_genre", title: "Genre"},
                                        {field: "language_name", title: "Language"},
                                        {field: "synopsis_writer_name", title: "Writer Name"},
                                        {field: "synopsis_director_name", title: "Director Name"},
                                        {field: "synopsis_release_date", title: "Release Date"},
                                        {field: "synopsis_duration", title: "Duration"},
                                        {field: "synopsis_details", title: "Details"},
                                        //{command: ["edit", "destroy"], title: "Action", width: "230px"}]
                                    ]
                                });
                            });</script>		


                        <div id="kWindow"></div>
                    </div>
<!--                    <div style="padding-bottom:100px;"></div>               -->
                </div>
            </div>
            <?php include '../layout/footer.php'; ?>
        </div>
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('.tabbedwidget').tabs();
        });
        jQuery('#1').addClass('active');
    </script>
</body>
</html>
