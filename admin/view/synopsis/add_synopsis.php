<?php
session_start();
include("../../config/class.web.config.php");
$con = new Config();
$err = '';
$msg = '';
/* Start Authentication */
if ($con->authenticate() == 1) {
    $con->redirect("../../login.php");
}
/* End Authentication */
/* Start Synopsis Field Initialization */
$event_id = "";
$language_id = "";
$synopsis_details = "";
$synopsis_director_name = "";
$synopsis_genre = "";
$synopsis_release_date = "";
$synopsis_writer_name = "";
$synopsis_duration = "";
/* Ends Event Field Initialization */
if (isset($_POST["btnCreateSynopsis"])) {
    extract($_POST);
    $rdate = date_create($synopsis_release_date);
    $release_date = date_format($rdate, 'Y-m-d');
    if ($_POST["synopsis_director_name"] == "") {
        $synopsis_director_name = 'NULL';
    }
    if ($_POST["synopsis_writer_name"] == "") {
        $synopsis_writer_name = 'NULL';
    }
    if ($_POST["synopsis_duration"] == "") {
        $synopsis_duration = 'NULL';
    }
    if ($_POST["synopsis_release_date"] == "") {
        $synopsis_release_date = 'NULL';
    }

    if ($_POST["event_id"] == "") {
        $err = "Select Event Title";
    } elseif ($_POST["language_id"] == "") {
        $err = "Select Language Name";
    } elseif ($_POST["synopsis_genre"] == "") {
        $err = "Genre Required";
    } elseif ($_POST["synopsis_details"] == "") {
        $err = "Synopsis Details Required";
    } else {
        $insert_array = array(
            "event_id" => $event_id,
            "language_id" => $language_id,
            "synopsis_genre" => $synopsis_genre,
            "synopsis_director_name" => $synopsis_director_name,
            "synopsis_writer_name" => $synopsis_writer_name,
            "synopsis_release_date" => $release_date,
            "synopsis_duration" => $synopsis_duration,
            "synopsis_details" => $synopsis_details
        );
        if (($con->CheckExistsWithArray("synopsis", array("event_id" => $event_id)) >= 1)) {
            $err = "Event Synopsis Already Exists.";
        } else {
            $result = $con->Insert("synopsis", $insert_array, "", "", "array");
//        $con->debug($result);
//        exit();
            if ($result["output"] == "success") {
                $msg = "Synopsis Added Successfully.";
            } else {
                $err = "ERROR";
            }
        }
    }
}
?>
<?php include '../layout/header_script.php'; ?>
<body>
    <div class="mainwrapper">
        <?php include '../layout/header_login_info.php'; ?>
        <div class="leftpanel">
            <?php include '../layout/left_navigation.php'; ?>
        </div>
        <div class="rightpanel">
            <ul class="breadcrumbs">
                <li><a href="dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
                <li>Synopsis Management <span class="separator"></span></li>
                <li>Add New Synopsis</li>
            </ul>

            <div class="maincontent">
                <div class="maincontentinner">
                    <div class="row-fluid">
                        <form method="post">
                            <div class="k-block">
                                <div class="maincontentinner">
                                    <div class="row-fluid">
                                        <?php echo(!empty($msg) ? $con->notification($msg, '') : $con->notification('', $err)); ?>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="span3">Select Event Title :</div>
                                            <div class="span9"><?php echo $con->SelectBox("event_id", "event_id", "", "event_id", "event_title", $event_id, "", "event_controller.php", "kendo", "width:60%", ""); ?></div>
                                        </div>
                                        <div class="span6">
                                            <div class="span3">Synopsis Language:</div>
                                            <div class="span9"><?php echo $con->SelectBox("language_id", "language_id", "", "language_id", "language_name", $language_id, "", "language_controller.php", "kendo", "width:60%", ""); ?></div>
                                        </div>
                                    </div>
                                    <div class="row-fluid" >
                                        <div class="span6">
                                            <div class="span3">Genre:</div>
                                            <div class="span9"><?php echo $con->TextBox("synopsis_genre", "synopsis_genre", "width:60%", "", "", $synopsis_genre, 'kendo'); ?></div>
                                        </div>
                                        <div class="span6">
                                            <div class="span3">Director Name:</div>
                                            <div class="span9"><?php echo $con->TextBox("synopsis_director_name", "synopsis_director_name", "width:60%", "", "", $synopsis_director_name, 'kendo'); ?></div>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="span3">Writer Name:</div>
                                            <div class="span9"><?php echo $con->TextBox("synopsis_writer_name", "synopsis_writer_name", "width:60%", "", "", $synopsis_writer_name, 'kendo'); ?></div>
                                        </div>
                                        <div class="span6">
                                            <div class="span3">Release Date:</div>
                                            <div class="span9"><input type="text" value="<?php echo $synopsis_release_date; ?>" id="synopsis_release_date" placeholder="" name="synopsis_release_date" type="text" style="width: 60%;"/></div>
                                        </div>
                                        <script>
                                            $(document).ready(function () {
                                                $("#synopsis_release_date").kendoDatePicker();
                                            });
                                        </script>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="span3">Duration:</div>
                                            <div class="span9"><?php echo $con->TextBox("synopsis_duration", "synopsis_duration", "width:60%", "", "", $synopsis_duration, 'kendo'); ?></div>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="span3">Synopsis Details</div>
                                        </div>
                                    </div>
                                    <div class="row-fluid">

                                        <div class="span12">
                                            <textarea id="editor" name="synopsis_details" rows="5" cols="40"></textarea>
                                        </div>
                                        <script>
                                            $(document).ready(function () {
                                                $("#editor").kendoEditor();
                                            });
                                        </script>
                                    </div>
                                    <hr />
                                    <div class="row-fluid">
                                        <div class="span12 pull-left">
                                            <input type="submit" value="Create Synopsis"  name="btnCreateSynopsis" class="k-button btn-success"/>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </form>

                    </div>
<!--                    <div style="padding-bottom:100px;"></div>               -->
                </div>
            </div>
            <?php include '../layout/footer.php'; ?>
        </div>
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('.tabbedwidget').tabs();
        });
        jQuery('#1').addClass('active');
    </script>
</body>
</html>