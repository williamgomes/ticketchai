<?php
include("../../config/class.web.config.php");

$con = new Config();

/* Start Authentication */

if ($con->authenticate() == 1) {

    $con->redirect("../../login.php");
}

/* End Authentication */
$err = '';
$msg = '';
$event_id = '';
$event_title = '';
$city_id = '';
$event_details_description = '';
$event_terms_and_conditions_description = '';
$event_ticket_expair_date = '';
$featured = '';
$upcomming = '';
$event_created_date = '';
$event_created_by = '';
$is_active = '';
$is_private = '';
$is_online_payable = '';
$is_home_delivery = '';
$user_id = $_SESSION['user_type_id'];


//--------------- Event General Information Tab Start ---------------//
if (isset($_POST["btnAddEvent"])) {
    extract($_POST);
    
    if(!$is_private OR $is_private != "yes"){
        $is_private = 'no';
    }
    
    if(!$is_online_payable OR $is_online_payable != "yes"){
        $is_online_payable = 'no';
    }
    
    if(!$is_home_delivery OR $is_home_delivery != "yes"){
        $is_home_delivery = 'no';
    }
    
    
   
    /*  ------------------ Start Event Logo Image ----------  */

    $targetfolder_logo = '';

    $filename_logo = '';

    $even_logo_image = '';

    if (isset($_FILES['event_logo_image']['name'])) {
        $targetfolder_logo = '../../uploads/event_logo_image/';
        $filename_logo = basename($_FILES['event_logo_image']['name']);
        $targetfolder_logo = $targetfolder_logo . $filename_logo;
        $event_logo_image = substr($targetfolder_logo, 6);
    }

    /*  ------------- End Event Logo Image ------------   */

    /*   Start Event Full Size Image   */

    $targetfolder_full = '';

    $filename_full = '';

    $event_full_size_image = '';

    if (isset($_FILES['event_full_size_image']['name'])) {

        $targetfolder_full = '../../uploads/event_full_size_image/';

        $filename_full = basename($_FILES['event_full_size_image']['name']);

        $targetfolder_full = $targetfolder_full . $filename_full;

        $event_full_size_image = substr($targetfolder_full, 6);
    }

    /*   End Event Full Size Image   */

    $is_active = $_POST["status_id"];

    if (empty($event_title)) {

        $err = "Event Title Required";
    } elseif (empty($city_id)) {

        $err = "Select Event City";
    } else if (empty($filename_logo)) {

        $err = "Select Event Logo Image";
    } elseif (empty($filename_full)) {

        $err = "Select Event Full Size Image";
    } else if (empty($event_ticket_expair_date)) {
        $err = "Enter Event Expire Date";
    } else {
        if($is_private != "yes"){
            $is_private = "no";
        }else{
            $is_private = "yes";
        }
        $insert_array = array(
            "event_title" => $event_title,
            "event_logo_image" => $filename_logo,
            "event_full_size_image" => $filename_full,
            "city_id" => $city_id,
            "is_active" => $is_active,
            "event_ticket_expair_date" => $event_ticket_expair_date,
            "featured" => 'false',
            "upcomming" => 'false',
            "event_created_by" => $user_id,
            "is_private"=> $is_private
        );

        //$con->debug($insert_array);

        if (($con->CheckExistsWithArray("event", array("event_title" => $event_title)) >= 1)) {

            $err = "You are not permitted to create event for same event title";
        } else {
            $result = $con->Insert("event", $insert_array, "", "", "array");
            if ($result["output"] == "success") {

                // $con->debug($result);

                if ($filename_logo != "") {

                    move_uploaded_file($_FILES['event_logo_image']['tmp_name'], $targetfolder_logo);
                }

                if ($filename_full != "") {

                    move_uploaded_file($_FILES['event_full_size_image']['tmp_name'], $targetfolder_full);
                }

                $_SESSION["tmp_event_id_1"] = $result["id"];

                $_SESSION["tab_1"] = 1;
            } else {

                $err = "ERROR";
            }
        }
    }
}
//--------------- Event General Information Tab End ---------------//
// ------------------ Cheking Event Details Tab Start ----------------//

if (isset($_POST["btnAddEventDetails"])) {

    extract($_POST);

    if (isset($_SESSION["tmp_event_id_1"])) {

        $d_event_id = $_SESSION["tmp_event_id_1"];

        $event_details_description = $_POST["event_details_description"];

        $up_object_array = array(
            "event_id" => $d_event_id,
            "event_details_description" => htmlentities($event_details_description, ENT_COMPAT, 'UTF-8')
        );

        $update_res = $con->Insert("event_details", $up_object_array, "", "", "array");

        if ($update_res["output"] == "success") {

            $_SESSION["tab_2"] = 2;

            unset($_SESSION["tab_1"]);
        } else {
            $_SESSION["tab_2"] = 2;

            unset($_SESSION["tab_1"]);
        }
    }
}

// ------------------ Checking Event Details Tab End ----------------//
// ---------------- Checking Event Terms And Conditions Tab Start -----------//

if (isset($_POST["btnAddTermsCondition"])) {

    extract($_POST);

    if (isset($_SESSION["tmp_event_id_1"])) {

        $t_event_id = $_SESSION["tmp_event_id_1"];

        $event_terms_and_conditions_description = $_POST["event_terms_and_conditions_description"];

        $event_update_terms_array = array(
            "event_id" => $t_event_id,
            "event_terms_and_conditions_description" => htmlentities($event_terms_and_conditions_description, ENT_COMPAT, 'UTF-8'));

        $update_res_t = $con->Insert("event_terms_and_conditions", $event_update_terms_array, "", "", "array");

        //$con->debug($update_res_t);

        if ($update_res_t["output"] == "success") {

            $_SESSION["tab_3"] = 3;

            unset($_SESSION["tab_2"]);
        } else {
            $_SESSION["tab_3"] = 3;

            unset($_SESSION["tab_2"]);
        }
    }
}
// ---------------- Checking Event Terms And Conditions Tab End-----------//
//--------------Cheking Event Category Start-------------------- //

if (isset($_POST["btnCreateCategory"])) {
    extract($_POST);
    if (isset($_SESSION["tmp_event_id_1"])) {
        $d_event_id = $_SESSION["tmp_event_id_1"];

        if (($con->CheckExistsWithArray("event_category", array("event_id" => $d_event_id)) >= 1)) {
            $_SESSION["tab_4"] = 4;

            unset($_SESSION["tab_3"]);
        } else {
            $err = "Please Select Category For This Event";
        }
    }
}
//--------------Cheking Event Category End-------------------- //
//--------------Cheking Event Venue Start -------------------- //
if (isset($_POST["btnCreateVenue"])) {
    extract($_POST);
    if (isset($_SESSION["tmp_event_id_1"])) {
        $d_event_id = $_SESSION["tmp_event_id_1"];

        if (($con->CheckExistsWithArray("event_venue", array("event_id" => $d_event_id)) >= 1)) {
            $_SESSION["tab_5"] = 5;

            unset($_SESSION["tab_4"]);
        } else {
            $err = "Please Select Venue For This Event";
        }
    }
}
//--------------Cheking Event Venue End -------------------- //
// --------------------- Checking Event Schedule Start ------------//
if (isset($_POST["btnCreateSchedule"])) {

    extract($_POST);

    $_SESSION["tab_6"] = 6;

    unset($_SESSION["tab_5"]);
}

// --------------------- Checking Event Schedule End ------------//
// --------------------- Checking Event Ticket Type Start ------------//
if (isset($_POST["btnCreateEventTicketType"])) {

    extract($_POST);

    $_SESSION["tab_7"] = 7;

    unset($_SESSION["tab_6"]);
}

// --------------------- Checking Event Ticket Type End ------------//
// ------------------Checking Event Includes Tab Start -------------------//

if (isset($_POST["btnCreateEventIncludes"])) {
    extract($_POST);

    $_SESSION["tab_8"] = 8;

    unset($_SESSION["tab_7"]);
}
// ------------------Checking Event Includes Tab End -------------------//
// ------------------ Checking Event Key Attraction Tab Start    ---------------------//

if (isset($_POST["btnCreateKeyAttraction"])) {

    extract($_POST);

    $_SESSION["tab_9"] = 9;

    unset($_SESSION["tab_8"]);
}

// ------------------ Checking Event Key Attraction Tab End    ---------------------//
// ------------------- Checking Event Photo Gallery Tab Start ------------------------------//

if (isset($_POST["btnCreatePhotoGallery"])) {

    extract($_POST);

    $_SESSION["tab_10"] = 10;

    unset($_SESSION["tab_9"]);
}

// ------------------- Checking Event Photo Gallery Tab End ------------------------------//
// ----------- Cheking Video Gallery Tab Start --------------------------------//
if (isset($_POST["btnCreateVideoGallery"])) {

    extract($_POST);

    $_SESSION["tab_11"] = 11;

    unset($_SESSION["tab_10"]);
}


// ----------- Cheking Video Gallery Tab End --------------------------------//
//---------------------- Checking Event Activity Tab Start -----------------------------//
if (isset($_POST["btnCreateActivity"])) {

    extract($_POST);

    $_SESSION["tab_12"] = 12;

    unset($_SESSION["tab_11"]);
}
//---------------------- Checking Event Activity Tab End -----------------------------//
//---------------------- Checking Event FAQ Tab Start -----------------------------//
if (isset($_POST["btnCreateFAQ"])) {

    extract($_POST);

    //$_SESSION["tab_13"] = 13;
    unset($_SESSION["tmp_event_id_1"]);

    unset($_SESSION["tab_12"]);
}
//---------------------- Checking Event FAQ Tab End -----------------------------//

if (empty($event_title)) {

    if (isset($_SESSION["tmp_event_id_1"])) {

        unset($_SESSION["tmp_event_id_1"]);
    }

    if (isset($_SESSION["tab_1"])) {

        unset($_SESSION["tab_1"]);
    }

    if (isset($_SESSION["tab_2"])) {

        unset($_SESSION["tab_2"]);
    }

    if (isset($_SESSION["tab_3"])) {

        unset($_SESSION["tab_3"]);
    }

    if (isset($_SESSION["tab_4"])) {

        unset($_SESSION["tab_4"]);
    }

    if (isset($_SESSION["tab_5"])) {

        unset($_SESSION["tab_5"]);
    }

    if (isset($_SESSION["tab_6"])) {

        unset($_SESSION["tab_6"]);
    }
    if (isset($_SESSION["tab_7"])) {

        unset($_SESSION["tab_7"]);
    }
    if (isset($_SESSION["tab_8"])) {

        unset($_SESSION["tab_8"]);
    }
    if (isset($_SESSION["tab_9"])) {

        unset($_SESSION["tab_9"]);
    }
    if (isset($_SESSION["tab_10"])) {

        unset($_SESSION["tab_10"]);
    }
    if (isset($_SESSION["tab_11"])) {

        unset($_SESSION["tab_11"]);
    }
    if (isset($_SESSION["tab_12"])) {

        unset($_SESSION["tab_12"]);
    }
}

//------------- Account Settings Tab checking Start Here --------------//
$email = $_SESSION['admin_email_address'];
$user_id = $_SESSION['user_type_id'];

$query_for_account_settings = "SELECT * FROM admin WHERE admin_email_address = '$email' AND user_type_id = '$user_id'";
$query_result = $con->ReturnObjectByQuery($query_for_account_settings, "array");

$admin_first_name = '';
$admin_last_name = '';
$admin_address = '';
$admin_first_name = $query_result{0}->admin_first_name;
$admin_last_name = $query_result{0}->admin_last_name;
$admin_mobile_number = $query_result{0}->admin_mobile_number;
$admin_address = $query_result{0}->admin_address;
$admin_password = '';
$admin_new_password = '';
$admin_retype_password = '';



//------------- Account Settings Tab checking End Here --------------//
?>
<?php include("../layout/header_script.php"); ?>

<?php include("../layout/menu_top.php"); ?>

<?php include("../layout/breadcrumbs.php"); ?>

<div class="row">

    <div class="col-md-12">

        <?php echo(!empty($msg) ? $con->notification($msg, '') : $con->notification('', $err)); ?>

    </div>

</div>

<br />
<form method="post">
    <div class="row">
        <div class="col-md-12">
            <div id="main_kendo_tab">
                <ul>
                    <li class="k-state-active">
                        Create New Event
                    </li>
                    <li>
                        My Events
                    </li>

                    <li>
                        Report
                    </li>
                    <li>
                        Account Settings
                    </li>
                    <li>
                        Contact Support Desk
                    </li>
                </ul>


                <!-- Create New Event Tab Start-->
                <div class="row">
                    <div class="col-md-12">
                        <div id="kendotab" style="margin-left: -22px;margin-top: 15px;margin-right: 7px">
                            <ul>
                                <li class="k-state-active">
                                    General Info
                                </li>
                                <li>
                                    Details
                                </li>
                                <li>
                                    Terms & Conditions
                                </li>
                                <li>
                                    Category
                                </li>
                                <li>
                                    Venue
                                </li>
                                <li>
                                    Schedule
                                </li>
                                <li>
                                    Ticket Type
                                </li>
                                <li>
                                    Includes
                                </li>

                                <li>
                                    Attraction
                                </li>
                                <li>
                                    Photo Gallery      
                                </li>
                                <li>
                                    Video Gallery
                                </li>
                                <li>
                                    Activity
                                </li>
                                <li>
                                    FAQ
                                </li>
                            </ul>

                            <div>

                                <div class="row">

                                    <div style="height: 15px;"></div>

                                    <div class="col-md-5">

                                        <div class="col-md-3">Event Title:</div>

                                        <div class="col-md-7"><?php echo $con->TextBox("event_title", "event_title", "", "", "", $event_title, "kendo"); ?></div>

                                    </div>

                                    <div class="col-md-5">

                                        <div class="col-md-3">City Name :</div>

                                        <div class="col-md-7"><?php echo $con->SelectBox("city_id", "city_id", "", "city_id", "city_name", $city_id, "", "city_controller.php", "kendo", "width:100%", ""); ?></div>

                                    </div>



                                </div>

                                <div class="row">

                                    <div style="height: 15px;"></div>

                                    <div class="col-md-5">

                                        <div class="col-md-3">Event Logo Image:</div>

                                        <div class="col-md-7" ><?php echo $con->FileUpload("event_logo_image", "event_logo_image", "", "", "", ""); ?></div>

                                    </div>

                                    <div class="col-md-5">

                                        <div class="col-md-3">Event Full Size Image:</div>

                                        <div class="col-md-7"><?php echo $con->FileUpload("event_full_size_image", "event_full_size_image", "", "", "", ""); ?></div>

                                    </div>

                                </div>

                                <div style="height: 15px;"></div>

                                <div class="row">


                                    <div class="col-md-5">
                                        <div class="col-md-3">Expire Date :</div>
                                        <div class="col-md-7"><input style="width: 100% !important;" id="event_ticket_expair_date" name="event_ticket_expair_date" value="<?php echo $event_ticket_expair_date; ?>" /></div> 
                                    </div>
                                    <script>
                                        $(document).ready(function () {
                                            $("#event_ticket_expair_date").kendoDatePicker();
                                        });</script>


                                    <div class="col-md-5">

                                        <div class="col-md-3">Event Status:</div>

                                        <div class="col-md-7"><?php echo $con->SelectBox("status_id", "status_id", "", "status_id", "status_name", $is_active, "", "status_controller.php", "kendo", "width:100%", ""); ?></div>

                                    </div>

                                </div>
                                <div style="height: 15px;"></div>
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="col-md-3">Is Private</div>
                                        <div class="col-md-7"><input type="checkbox" name="is_private" value="yes" <?php if ($is_private == 'yes'){ echo 'checked="checked"';} ?>/></div> 
                                    </div>
                                </div>
                                <div style="height: 15px;"></div>
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="col-md-3">Is Online Payable</div>
                                        <div class="col-md-7"><input type="checkbox" name="is_online_payable" value="yes" <?php if ($is_online_payable == 'yes'){ echo 'checked="checked"';} ?>/></div> 
                                    </div>
                                </div>
                                <div style="height: 15px;"></div>
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="col-md-3">Is Home Delivery</div>
                                        <div class="col-md-7"><input type="checkbox" name="is_home_delivery" value="yes" <?php if ($is_home_delivery == 'yes'){ echo 'checked="checked"';} ?>/></div> 
                                    </div>
                                </div>
                                
                                

                                <div style="height: 15px;"></div>
                                <div class="row">
                                    <div class="col-md-5">

                                        <input style="margin-left: 15px;" type="submit" name="btnAddEvent" class="btn btn-success" value="Next"  />

                                    </div>
                                </div>

                            </div>

                            <!-- Event Details Tab Start Here -->

                            <div>

                                <div class="row">

                                    <div style="height: 15px;"></div>

                                    <div class="col-md-5">Event Details</div>

                                </div>



                                <div class="row">

                                    <div class="col-md-11">

                                        <textarea id="event_details_description" name="event_details_description" rows="3" cols="30"><?php echo html_entity_decode($event_details_description, ENT_QUOTES | ENT_IGNORE, "UTF-8"); ?></textarea>

                                        <script>

                                            $(document).ready(function () {

                                                $("#event_details_description").kendoEditor({
                                                });
                                            });</script>

                                    </div>

                                </div>

                                <div class="row">
                                    <br/>

                                    <div class="col-md-11">
                                        <input type="submit" name="btnAddEventDetails" class="btn btn-success" value="Next"  />
                                    </div>
                                </div>
                            </div>
                            <!-- Event Details Tab End Here -->


                            <!-- Event Terms And Condition Tab Start Here -->
                            <div>
                                <div class="row">
                                    <div style="height: 20px"></div>
                                    <div class="col-md-5">Terms & Condition</div>
                                    <div style="height: 20px"></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-11">
                                        <textarea id="event_terms_and_conditions_description" name="event_terms_and_conditions_description" rows="3" cols="30"><?php echo html_entity_decode($event_terms_and_conditions_description, ENT_QUOTES | ENT_IGNORE, "UTF-8"); ?></textarea>
                                        <script>
                                            $(document).ready(function () {

                                                $("#event_terms_and_conditions_description").kendoEditor({
                                                });
                                            });</script>
                                    </div>
                                </div>
                                <div class="row">
                                    <div style="height: 15px;"></div>
                                    <div class="col-md-11">
                                        <input type="submit" name="btnAddTermsCondition" value="Next" class="btn btn-success" />
                                    </div>
                                </div>
                            </div>

                            <!-- Event Terms And Condition Tab End Here -->


                            <!-- Category Tab Start Here -->
                            <div>

                                <div id="grid"></div>
                                <script type="text/javascript">
                                    jQuery(document).ready(function () {
                                        var dataSource = new kendo.data.DataSource({
                                            pageSize: 10,
                                            transport: {
                                                read: {
                                                    url: "../../controller/event_category_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "GET"

                                                },
                                                update: {
                                                    url: "../../controller/event_category_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "POST",
                                                    complete: function (e) {

                                                        jQuery("#grid").data("kendoGrid").dataSource.read();
                                                    }

                                                },
                                                destroy: {
                                                    url: "../../controller/event_category_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "DELETE"

                                                },
                                                create: {
                                                    url: "../../controller/event_category_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "PUT",
                                                    complete: function (e) {

                                                        jQuery("#grid").data("kendoGrid").dataSource.read();
                                                    }

                                                }

                                            },
                                            autoSync: false,
                                            schema: {
                                                errors: function (e) {

                                                    if (e.error === "yes")

                                                    {

                                                        var message = "";
                                                        message += e.message;
                                                        var window = jQuery("#kWindow");
                                                        if (!window.data("kendoWindow")) {

                                                            window.kendoWindow({
                                                                title: "Error window",
                                                                modal: true,
                                                                height: 100,
                                                                width: 400

                                                            });
                                                        }

                                                        window.data("kendoWindow").center().open();
                                                        window.html('<div style="margin-top:10px;text-align:center;"><span style="color:red;font-size:14px;">' + message + '</span></div>');
                                                        this.cancelChanges();
                                                    }

                                                },
                                                data: "data",
                                                total: "data.length",
                                                model: {
                                                    id: "event_category_id",
                                                    fields: {
                                                        event_category_id: {editable: false, nullable: true},
                                                        category_id: {type: "number"},
                                                        category_name: {type: "string", validation: {required: true}},
                                                        is_active: {type: "boolean"}
                                                    }

                                                }

                                            }

                                        });
                                        jQuery("#grid").kendoGrid({
                                            dataSource: dataSource,
                                            filterable: true,
                                            pageable: {
                                                refresh: true,
                                                input: true,
                                                numeric: false,
                                                pageSizes: [10, 20, 50]

                                            },
                                            sortable: true,
                                            groupable: true,
                                            resizable: true,
                                            toolbar: [{name: "create", text: "Add Category"}],
                                            columns: [
                                                {field: "category_id",
                                                    title: "Category Name",
                                                    editor: CategoryDropDownEditor,
                                                    template: "#=category_name#",
                                                    filterable: {
                                                        ui: CategoryFilter,
                                                        extra: false,
                                                        operators: {
                                                            string: {
                                                                eq: "Is equal to",
                                                                neq: "Is not equal to"

                                                            }

                                                        }

                                                    }

                                                },
                                                {field: "is_active", title: "Active?", template: "#= is_active ? 'Yes' : 'No' #", width: "10%"},
                                                {command: ["edit", "destroy"], title: "Action", width: "230px"}],
                                            editable: "inline"

                                        });
                                    });</script>		
                                <script type="text/javascript">
                                    function CategoryFilter(element) {

                                        element.kendoDropDownList({
                                            autoBind: false,
                                            dataTextField: "category_name",
                                            dataValueField: "category_id",
                                            dataSource: {
                                                transport: {
                                                    read: {
                                                        url: "../../controller/category_controller.php",
                                                        type: "GET"

                                                    }

                                                },
                                                schema: {
                                                    data: "data"

                                                }

                                            },
                                            optionLabel: "Select Category"

                                        });
                                    }

                                    function CategoryDropDownEditor(container, options) {

                                        jQuery('<input required data-text-field="category_name" data-value-field="category_id" data-bind="value:' + options.field + '"/>')

                                                .appendTo(container)

                                                .kendoDropDownList({
                                                    autoBind: false,
                                                    dataTextField: "category_name",
                                                    dataValueField: "category_id",
                                                    dataSource: {
                                                        transport: {
                                                            read: {
                                                                url: "../../controller/category_controller.php",
                                                                type: "GET"

                                                            }

                                                        },
                                                        schema: {
                                                            data: "data"

                                                        }

                                                    },
                                                    optionLabel: "Select Category"

                                                });
                                    }
                                </script>
                                <div id="kWindow"></div>

                                <div class="clearfix"></div>

                                <br/>

                                <div>

                                    <input type="submit" class="btn btn-success" name="btnCreateCategory" value="Next" />

                                </div>

                            </div>
                            <!--Event Category Tab End Here -->


                            <!-- Event Venue Tab Start Here -->

                            <div>

                                <div id="venue"></div>
                                <script type="text/javascript">
                                    jQuery(document).ready(function () {
                                        var dataSource = new kendo.data.DataSource({
                                            pageSize: 10,
                                            transport: {
                                                read: {
                                                    url: "../../controller/event_venue_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "GET"

                                                },
                                                update: {
                                                    url: "../../controller/event_venue_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "POST",
                                                    complete: function (e) {

                                                        jQuery("#venue").data("kendoGrid").dataSource.read();
                                                    }

                                                },
                                                destroy: {
                                                    url: "../../controller/event_venue_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "DELETE"

                                                },
                                                create: {
                                                    url: "../../controller/event_venue_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "PUT",
                                                    complete: function (e) {

                                                        jQuery("#venue").data("kendoGrid").dataSource.read();
                                                    }

                                                }

                                            },
                                            autoSync: false,
                                            schema: {
                                                errors: function (e) {

                                                    if (e.error === "yes")

                                                    {

                                                        var message = "";
                                                        message += e.message;
                                                        var window = jQuery("#kWindow");
                                                        if (!window.data("kendoWindow")) {

                                                            window.kendoWindow({
                                                                title: "Error window",
                                                                modal: true,
                                                                height: 100,
                                                                width: 400

                                                            });
                                                        }

                                                        window.data("kendoWindow").center().open();
                                                        window.html('<div style="margin-top:10px;text-align:center;"><span style="color:red;font-size:14px;">' + message + '</span></div>');
                                                        this.cancelChanges();
                                                    }

                                                },
                                                data: "data",
                                                total: "data.length",
                                                model: {
                                                    id: "event_venue_id",
                                                    fields: {
                                                        event_venue_id: {editable: false, nullable: true},
                                                        venue_id: {type: "number"},
                                                        venue_name: {type: "string", validation: {required: true}},
                                                        is_active: {type: "boolean"}
                                                    }

                                                }

                                            }

                                        });
                                        jQuery("#venue").kendoGrid({
                                            dataSource: dataSource,
                                            filterable: true,
                                            pageable: {
                                                refresh: true,
                                                input: true,
                                                numeric: false,
                                                pageSizes: [10, 20, 50]

                                            },
                                            sortable: true,
                                            groupable: true,
                                            resizable: true,
                                            toolbar: [{name: "create", text: "Add Venue"}],
                                            columns: [
                                                {field: "venue_id",
                                                    title: "Venue Name",
                                                    editor: VenueDropDownEditor,
                                                    template: "#=venue_name#",
                                                    filterable: {
                                                        ui: VenueFilter,
                                                        extra: false,
                                                        operators: {
                                                            string: {
                                                                eq: "Is equal to",
                                                                neq: "Is not equal to"

                                                            }

                                                        }

                                                    }

                                                },
                                                {field: "is_active", title: "Active?", template: "#= is_active ? 'Yes' : 'No' #", width: "10%"},
                                                {command: ["edit", "destroy"], title: "Action", width: "230px"}],
                                            editable: "inline"

                                        });
                                    });</script>		
                                <script type="text/javascript">
                                    function VenueFilter(element) {

                                        element.kendoDropDownList({
                                            autoBind: false,
                                            dataTextField: "venue_name",
                                            dataValueField: "venue_id",
                                            dataSource: {
                                                transport: {
                                                    read: {
                                                        url: "../../controller/venue_list_controller.php",
                                                        type: "GET"

                                                    }

                                                },
                                                schema: {
                                                    data: "data"

                                                }

                                            },
                                            optionLabel: "Select Venue"

                                        });
                                    }

                                    function VenueDropDownEditor(container, options) {

                                        jQuery('<input required data-text-field="venue_name" data-value-field="venue_id" data-bind="value:' + options.field + '"/>')

                                                .appendTo(container)

                                                .kendoDropDownList({
                                                    autoBind: false,
                                                    dataTextField: "venue_name",
                                                    dataValueField: "venue_id",
                                                    dataSource: {
                                                        transport: {
                                                            read: {
                                                                url: "../../controller/venue_list_controller.php",
                                                                type: "GET"

                                                            }

                                                        },
                                                        schema: {
                                                            data: "data"

                                                        }

                                                    },
                                                    optionLabel: "Select Venue"

                                                });
                                    }
                                </script>
                                <div id="kWindow"></div>

                                <div class="clearfix"></div>

                                <br/>

                                <div>

                                    <input type="submit" class="btn btn-success" name="btnCreateVenue" value="Next" />

                                </div>

                            </div>
                            <!-- Event Venue Tab End Here -->

                            <!-- Event Schedule Tab Start Here -->

                            <div>
                                <div id="schedule"></div>

                                <script type="text/javascript">

                                    jQuery(document).ready(function () {

                                        var dataSource = new kendo.data.DataSource({
                                            pageSize: 10,
                                            transport: {
                                                read: {
                                                    url: "../../controller/schedule_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "GET"

                                                },
                                                update: {
                                                    url: "../../controller/schedule_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "POST",
                                                    complete: function (e) {

                                                        jQuery("#schedule").data("kendoGrid").dataSource.read();
                                                        jQuery(".k-pager-refresh").trigger('click');
                                                    }

                                                },
                                                destroy: {
                                                    url: "../../controller/schedule_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "DELETE"

                                                },
                                                create: {
                                                    url: "../../controller/schedule_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "PUT",
                                                    complete: function (e) {

                                                        jQuery("#schedule").data("kendoGrid").dataSource.read();
                                                        jQuery(".k-pager-refresh").trigger('click');
                                                    }

                                                },
                                            },
                                            autoSync: false,
                                            schema: {
                                                errors: function (e) {

                                                    if (e.error === "yes")

                                                    {

                                                        var message = "";
                                                        message += e.message;
                                                        var window = jQuery("#kWindowSchedule");
                                                        if (!window.data("kendoWindow")) {

                                                            window.kendoWindow({
                                                                title: "",
                                                                modal: true,
                                                                height: 120,
                                                                width: 400

                                                            });
                                                        }

                                                        window.data("kendoWindow").center().open();
                                                        window.html('<br/><br/><center><P style="color:red">' + message + '</p></center>');
                                                        this.cancelChanges();
                                                    }

                                                },
                                                data: "data",
                                                total: "data.length",
                                                model: {
                                                    id: "event_schedule_id",
                                                    fields: {
                                                        event_schdule_id: {editable: false, nullable: true},
                                                        venue_id: {type: "number"},
                                                        venue_name: {type: "string", validation: {required: true}},
                                                        event_date: {editable: true, nullable: true},
                                                        event_schedule_start_time: {type: "string", validation: true},
                                                        event_schedule_end_time: {type: "string", validation: true},
                                                        event_id: {type: "string"},
                                                        is_active: {type: "boolean"}

                                                    }
                                                }
                                            }

                                        });
                                        jQuery("#schedule").kendoGrid({
                                            dataSource: dataSource,
                                            filterable: true,
                                            pageable: {
                                                refresh: true,
                                                input: true,
                                                numeric: false,
                                                pageSizes: [10, 20, 50]

                                            },
                                            sortable: true,
                                            groupable: true,
                                            toolbar: [{name: "create", text: "Add Schedule"}],
                                            columns: [
                                                {field: "venue_id",
                                                    title: "Venue Name",
                                                    editor: ScheduleVenueDropDownEditor,
                                                    template: "#=venue_name#",
                                                    filterable: {
                                                        ui: ScheduleVenueFilter,
                                                        extra: false,
                                                        operators: {
                                                            string: {
                                                                eq: "Is equal to",
                                                                neq: "Is not equal to"

                                                            }

                                                        }

                                                    }

                                                },
                                                {field: "event_date", title: "Schedule Date", editor: dateTimeEditor, format: "{0:yyyy-MM-dd}"},
                                                {field: "event_schedule_start_time", title: "Start - Time", editor: timeEditor, format: "{0:hh:mm tt}"},
                                                {field: "event_schedule_end_time", title: "End - Time", editor: timeEditor, format: "{0:hh:mm tt}"},
                                                {field: "is_active", title: "Active?", template: "#= is_active ? 'Yes' : 'No' #", width: "10%"},
                                                {command: ["edit", "destroy"], title: "Action", width: "230px"}],
                                            editable: "inline"

                                        });
                                    });</script> 

                                <script type="text/javascript">

                                    function ScheduleVenueFilter(element) {

                                        element.kendoDropDownList({
                                            autoBind: false,
                                            dataTextField: "venue_name",
                                            dataValueField: "venue_id",
                                            dataSource: {
                                                transport: {
                                                    read: {
                                                        url: "../../controller/schedule_venue_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                        type: "GET"

                                                    }

                                                },
                                                schema: {
                                                    data: "data"

                                                }

                                            },
                                            optionLabel: "Select Venue"

                                        });
                                    }

                                    function ScheduleVenueDropDownEditor(container, options) {

                                        jQuery('<input required data-text-field="venue_name" data-value-field="venue_id" data-bind="value:' + options.field + '"/>')

                                                .appendTo(container)

                                                .kendoDropDownList({
                                                    autoBind: false,
                                                    dataTextField: "venue_name",
                                                    dataValueField: "venue_id",
                                                    dataSource: {
                                                        transport: {
                                                            read: {
                                                                url: "../../controller/schedule_venue_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                                type: "GET"

                                                            }

                                                        },
                                                        schema: {
                                                            data: "data"

                                                        }

                                                    },
                                                    optionLabel: "Select Venue"

                                                });
                                    }

                                    function timeEditor(container, options) {

                                        $('<input data-text-field="' + options.field + '" data-value-field="' + options.field + '" data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')

                                                .appendTo(container)

                                                .kendoTimePicker({
                                                    format: "hh:mm tt"

                                                });
                                    }



                                    function dateTimeEditor(container, options) {

                                        $('<input data-text-field="' + options.field + '" data-value-field="' + options.field + '" data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')

                                                .appendTo(container)

                                                .kendoDatePicker({
                                                    format: "yyyy-MM-dd"

                                                });
                                    }
                                </script>

                                <div id="kWindowSchedule"></div>
                                <div class="clearfix"></div>

                                <br/>

                                <div>

                                    <input type="submit" class="btn btn-success" name="btnCreateSchedule" value="Next" />

                                </div>
                            </div>
                            <!-- Event Schedule Tab End Here -->
                            <!-- Event Ticket Type Tab Start Here -->

                            <div>
                                <div id="event_ticket_type"></div>
                                <script type="text/javascript">
                                    jQuery(document).ready(function () {

                                        var dataSource = new kendo.data.DataSource({
                                            pageSize: 10,
                                            transport: {
                                                read: {
                                                    url: "../../controller/event_ticket_type_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "GET"

                                                },
                                                update: {
                                                    url: "../../controller/event_ticket_type_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "POST",
                                                    complete: function (e) {

                                                        jQuery("#event_ticket_type").data("kendoGrid").dataSource.read();
                                                    }

                                                },
                                                destroy: {
                                                    url: "../../controller/event_ticket_type_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "DELETE"

                                                },
                                                create: {
                                                    url: "../../controller/event_ticket_type_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "PUT",
                                                    complete: function (e) {

                                                        jQuery("#event_ticket_type").data("kendoGrid").dataSource.read();
                                                    }

                                                }

                                            },
                                            autoSync: false,
                                            schema: {
                                                errors: function (e) {

                                                    if (e.error === "yes")

                                                    {

                                                        var message = "";
                                                        message += e.message;
                                                        var window = jQuery("#kWindow");
                                                        if (!window.data("kendoWindow")) {

                                                            window.kendoWindow({
                                                                title: "Error window",
                                                                modal: true,
                                                                height: 100,
                                                                width: 400

                                                            });
                                                        }

                                                        window.data("kendoWindow").center().open();
                                                        window.html('<div style="margin-top:10px;text-align:center;"><span style="color:red;font-size:14px;">' + message + '</span></div>');
                                                        this.cancelChanges();
                                                    }

                                                },
                                                data: "data",
                                                total: "data.length",
                                                model: {
                                                    id: "event_ticket_type_id",
                                                    fields: {
                                                        event_ticket_type_id: {editable: false, nullable: true},
                                                        event_id: {type: "number"},
                                                        event_schedule_id: {type: "number"},
                                                        event_schedule_name: {type: "string"},
                                                        ticket_name: {type: "string", validation: {required: true}},
                                                        ticket_qty: {type: "number", validation: {required: true, min: 1}},
                                                        ticket_price: {type: "number", validation: {required: true, min: 1}},
                                                        per_user_limit: {type: "number", validation: {required: true, min: 1}},
                                                        e_ticket: {type: "number", validation: {min: 0}},
                                                        p_ticket: {type: "number", validation: {min: 0}},
                                                        is_active: {type: "boolean"}
                                                    }
                                                }

                                            }

                                        });
                                        jQuery("#event_ticket_type").kendoGrid({
                                            dataSource: dataSource,
                                            filterable: true,
                                            pageable: {
                                                refresh: true,
                                                input: true,
                                                numeric: false,
                                                pageSizes: [10, 20, 50]

                                            },
                                            sortable: true,
                                            groupable: true,
                                            resizable: true,
                                            toolbar: [{name: "create", text: "Add Ticket Type"}],
                                            columns: [
                                                {field: "event_schedule_id",
                                                    title: "Event Schedule",
                                                    width: "220px",
                                                    editor: EventScheduleTicketTypeDropDownEditor,
                                                    template: "#=event_schedule_name#",
                                                    filterable: {
                                                        ui: EventScheduleTicketTypeFilter,
                                                        extra: false,
                                                        operators: {
                                                            string: {
                                                                eq: "Is equal to",
                                                                neq: "Is not equal to"

                                                            }

                                                        }

                                                    }

                                                },
                                                {field: "ticket_name", title: "Ticket Name", width: "150px"},
                                                {field: "ticket_qty", title: "Ticket QTY", width: "100px"},
                                                {field: "ticket_price", title: "Ticket Price", width: "100px"},
                                                {field: "per_user_limit", title: "Per User Limit", width: "120px"},
                                                {field: "e_ticket", title: "E-Ticket", width: "100px"},
                                                {field: "p_ticket", title: "P-Ticket", width: "100px"},
                                                {field: "is_active", title: "Active?", template: "#= is_active ? 'Yes' : 'No' #", width: "80px"},
                                                {command: ["edit", "destroy"], title: "Action", width: "200px"}],
                                            editable: "inline"

                                        });
                                    });</script>

                                <script type="text/javascript">

                                    function EventScheduleTicketTypeFilter(element) {

                                        element.kendoDropDownList({
                                            autoBind: false,
                                            dataTextField: "event_schedule_name",
                                            dataValueField: "event_schedule_id",
                                            dataSource: {
                                                transport: {
                                                    read: {
                                                        url: "../../controller/ticket_type_schedule_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                        type: "GET"

                                                    }

                                                },
                                                schema: {
                                                    data: "data"

                                                }

                                            },
                                            optionLabel: "Select Schedule"

                                        });
                                    }

                                    function EventScheduleTicketTypeDropDownEditor(container, options) {

                                        jQuery('<input required data-text-field="event_schedule_name" data-value-field="event_schedule_id" data-bind="value:' + options.field + '"/>')

                                                .appendTo(container)

                                                .kendoDropDownList({
                                                    autoBind: false,
                                                    dataTextField: "event_schedule_name",
                                                    dataValueField: "event_schedule_id",
                                                    dataSource: {
                                                        transport: {
                                                            read: {
                                                                url: "../../controller/ticket_type_schedule_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                                type: "GET"

                                                            }

                                                        },
                                                        schema: {
                                                            data: "data"

                                                        }

                                                    },
                                                    optionLabel: "Select Schedule"

                                                });
                                    }
                                </script>

                                <div id="kWindow"></div>

                                <div class="clearfix"></div>
                                <br/>

                                <div>

                                    <input type="submit" class="btn btn-success" name="btnCreateEventTicketType" value="Next" />

                                </div>
                            </div>
                            <!-- Event Ticket Type Tab End Here -->

                            <!-- Event Includes Tab Start Here -->
                            <div>
                                <div id="event_includes"></div>
                                <script type="text/javascript">
                                    jQuery(document).ready(function () {

                                        var dataSource = new kendo.data.DataSource({
                                            pageSize: 10,
                                            transport: {
                                                read: {
                                                    url: "../../controller/event_includes_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "GET"

                                                },
                                                update: {
                                                    url: "../../controller/event_includes_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "POST",
                                                    complete: function (e) {

                                                        jQuery("#event_includes").data("kendoGrid").dataSource.read();
                                                    }

                                                },
                                                destroy: {
                                                    url: "../../controller/event_includes_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "DELETE"

                                                },
                                                create: {
                                                    url: "../../controller/event_includes_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "PUT",
                                                    complete: function (e) {

                                                        jQuery("#event_includes").data("kendoGrid").dataSource.read();
                                                    }

                                                }

                                            },
                                            autoSync: false,
                                            schema: {
                                                errors: function (e) {

                                                    if (e.error === "yes")

                                                    {

                                                        var message = "";
                                                        message += e.message;
                                                        var window = jQuery("#kWindow");
                                                        if (!window.data("kendoWindow")) {

                                                            window.kendoWindow({
                                                                title: "Error window",
                                                                modal: true,
                                                                height: 100,
                                                                width: 400

                                                            });
                                                        }

                                                        window.data("kendoWindow").center().open();
                                                        window.html('<div style="margin-top:10px;text-align:center;"><span style="color:red;font-size:14px;">' + message + '</span></div>');
                                                        this.cancelChanges();
                                                    }

                                                },
                                                data: "data",
                                                total: "data.length",
                                                model: {
                                                    id: "event_includes_id",
                                                    fields: {
                                                        event_includes_id: {editable: false, nullable: true},
                                                        event_id: {type: "number"},
                                                        event_schedule_id: {type: "number"},
                                                        event_schedule_name: {type: "string"},
                                                        event_includes_name: {type: "string"},
                                                        event_includes_price: {type: "number", validation: {required: true, min: 1}},
                                                        event_includes_per_user_limit: {type: "number", validation: {required: true, min: 1}},
                                                        total_qty: {type: "number", validation: {required: true, min: 1}},
                                                        is_active: {type: "boolean"}
                                                    }
                                                }

                                            }

                                        });
                                        jQuery("#event_includes").kendoGrid({
                                            dataSource: dataSource,
                                            filterable: true,
                                            pageable: {
                                                refresh: true,
                                                input: true,
                                                numeric: false,
                                                pageSizes: [10, 20, 50]

                                            },
                                            sortable: true,
                                            groupable: true,
                                            resizable: true,
                                            toolbar: [{name: "create", text: "Add Event Includes"}],
                                            columns: [
                                                {field: "event_schedule_id",
                                                    title: "Event Schedule",
                                                    width: "220px",
                                                    editor: EventIncludesScheduleDropDownEditor,
                                                    template: "#=event_schedule_name#",
                                                    filterable: {
                                                        ui: EventIncludesScheduleFilter,
                                                        extra: false,
                                                        operators: {
                                                            string: {
                                                                eq: "Is equal to",
                                                                neq: "Is not equal to"

                                                            }

                                                        }

                                                    }

                                                },
                                                {field: "event_includes_name", title: "Includes Name", width: "150px"},
                                                {field: "event_includes_price", title: "Price", width: "100px"},
                                                {field: "event_includes_per_user_limit", title: "Per User Limit", width: "120px"},
                                                {field: "total_qty", title: "Total QTY", width: "120px"},
                                                {field: "is_active", title: "Active?", template: "#= is_active ? 'Yes' : 'No' #", width: "80px"},
                                                {command: ["edit", "destroy"], title: "Action", width: "200px"}],
                                            editable: "inline"

                                        });
                                    });</script>

                                <script type="text/javascript">

                                    function EventIncludesScheduleFilter(element) {

                                        element.kendoDropDownList({
                                            autoBind: false,
                                            dataTextField: "event_schedule_name",
                                            dataValueField: "event_schedule_id",
                                            dataSource: {
                                                transport: {
                                                    read: {
                                                        url: "../../controller/ticket_type_schedule_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                        type: "GET"

                                                    }

                                                },
                                                schema: {
                                                    data: "data"

                                                }

                                            },
                                            optionLabel: "Select Schedule"

                                        });
                                    }

                                    function EventIncludesScheduleDropDownEditor(container, options) {

                                        jQuery('<input required data-text-field="event_schedule_name" data-value-field="event_schedule_id" data-bind="value:' + options.field + '"/>')

                                                .appendTo(container)

                                                .kendoDropDownList({
                                                    autoBind: false,
                                                    dataTextField: "event_schedule_name",
                                                    dataValueField: "event_schedule_id",
                                                    dataSource: {
                                                        transport: {
                                                            read: {
                                                                url: "../../controller/ticket_type_schedule_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                                type: "GET"

                                                            }

                                                        },
                                                        schema: {
                                                            data: "data"

                                                        }

                                                    },
                                                    optionLabel: "Select Schedule"

                                                });
                                    }
                                </script>

                                <div id="kWindow"></div>

                                <div class="clearfix"></div>
                                <br/>

                                <div>

                                    <input type="submit" class="btn btn-success" name="btnCreateEventIncludes" value="Next" />

                                </div>
                            </div>




                            <!-- Event Includes Tab End Here -->







                            <!-- Event Key Attraction Tab Start Here -->
                            <div>
                                <div id="attraction"></div>
                                <script type="text/javascript">


                                    jQuery(document).ready(function () {
                                        var dataSource = new kendo.data.DataSource({
                                            pageSize: 10,
                                            transport: {
                                                read: {
                                                    url: "../../controller/event_key_attraction_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "GET"
                                                },
                                                update: {
                                                    url: "../../controller/event_key_attraction_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "POST",
                                                    complete: function (e) {
                                                        jQuery("#attraction").data("kendoGrid").dataSource.read();
                                                    }
                                                },
                                                destroy: {
                                                    url: "../../controller/event_key_attraction_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "DELETE"
                                                },
                                                create: {
                                                    url: "../../controller/event_key_attraction_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "PUT",
                                                    complete: function (e) {
                                                        jQuery("#attraction").data("kendoGrid").dataSource.read();
                                                    }
                                                },
                                            },
                                            autoSync: false,
                                            schema: {
                                                data: "data",
                                                total: "data.length",
                                                model: {
                                                    id: "event_key_attraction_id",
                                                    fields: {
                                                        event_key_attraction_id: {type: "string"},
                                                        attraction_name: {type: "string"},
                                                        details: {type: "string"},
                                                        image: {type: "string"},
                                                        facebook: {type: "string"},
                                                        twitter: {type: "string"},
                                                        google_plus: {type: "string"},
                                                        is_active: {type: "boolean"}


                                                    }
                                                }
                                            }
                                        });
                                        jQuery("#attraction").kendoGrid({
                                            dataSource: dataSource,
                                            filterable: true,
                                            pageable: {
                                                refresh: true,
                                                input: true,
                                                numeric: false,
                                                pageSizes: true,
                                                pageSizes: [10, 20, 50],
                                            },
                                            sortable: true,
                                            groupable: true,
                                            resizable: true,
                                            toolbar: [{name: "create", text: "Add Key Attraction"}],
                                            columns: [
                                                {field: "attraction_name", title: "Name", width: "150px"},
                                                {field: "details",
                                                    title: "Attraction Details", width: "400px",
                                                    editor: textAreaEditor
                                                },
                                                {field: "image",
                                                    title: "Attraction Image", width: "160px",
                                                    editor: fileEditor,
                                                    template: "<img src='<?php echo $con->baseUrl("uploads/event_key_attraction/") ?>#=image#'   height='100' width='140'/>"
                                                },
                                                {field: "facebook", title: "Facebook", width: "150px"},
                                                {field: "twitter", title: "Twitter", width: "150px"},
                                                {field: "google_plus", title: "Google Plus", width: "150px"},
                                                {field: "is_active", title: "Active?", template: "#= is_active ? 'Yes' : 'No' #", width: "80px"},
                                                {command: ["edit", "destroy"], title: "Action", width: "200px"}],
                                            editable: "inline"
                                        });
                                    });
                                    function fileEditor(container, options) {
                                        $('<input type="file" id="image" name="image" data-role="upload" />')
                                                .appendTo(container)
                                                .kendoUpload({
                                                    multiple: true,
                                                    async: {
                                                        saveUrl: "../../controller/event_key_attraction_save.php",
                                                        autoUpload: true
                                                    },
                                                    upload: function (e) {
                                                        e.data = {event_key_attraction_id: options.model.event_key_attraction_id};
                                                    },
                                                    success: function (e) {
                                                        console.log(e.response);
                                                        options.model.set("image", e.response.image);
                                                    }
                                                });
                                    }

                                    function textAreaEditor(container, options) {
                                        $('<textarea id="details" name="details" rows="3" cols="30">' + options.field + '</textarea>')
                                                .appendTo(container)
                                                .kendoEditor({
                                                    tools: [
                                                        "bold",
                                                        "italic",
                                                        "underline",
                                                        "strikethrough",
                                                        "justifyLeft",
                                                        "justifyCenter",
                                                        "justifyRight",
                                                        "justifyFull",
                                                        "insertUnorderedList",
                                                        "insertOrderedList",
                                                        "indent",
                                                        "outdent",
                                                        "createLink",
                                                        "unlink",
                                                        "insertImage",
                                                        "insertFile",
                                                        "subscript",
                                                        "superscript",
                                                        "createTable",
                                                        "addRowAbove",
                                                        "addRowBelow",
                                                        "addColumnLeft",
                                                        "addColumnRight",
                                                        "deleteRow",
                                                        "deleteColumn",
                                                        "viewHtml",
                                                        "formatting",
                                                        "cleanFormatting",
                                                        "fontName",
                                                        "fontSize",
                                                        "foreColor",
                                                        "backColor"
                                                    ]
                                                });
                                    }

                                </script>

                                <div id="kWindow"></div>
                                <div class="clearfix"></div>
                                <br/>
                                <div>
                                    <input type="submit" class="btn btn-success" name="btnCreateKeyAttraction" value="Next" />
                                </div>
                            </div>
                            <!-- Event Attraction Tab End Here -->

                            <!-- Event Photo Gallery Tab Start Here -->
                            <div>
                                <div id="photo"></div>
                                <script type="text/javascript">


                                    jQuery(document).ready(function () {
                                        var dataSource = new kendo.data.DataSource({
                                            pageSize: 10,
                                            transport: {
                                                read: {
                                                    url: "../../controller/event_photo_gallery_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "GET"
                                                },
                                                update: {
                                                    url: "../../controller/event_photo_gallery_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "POST",
                                                    complete: function (e) {
                                                        jQuery("#photo").data("kendoGrid").dataSource.read();
                                                    }
                                                },
                                                destroy: {
                                                    url: "../../controller/event_photo_gallery_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "DELETE"
                                                },
                                                create: {
                                                    url: "../../controller/event_photo_gallery_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "PUT",
                                                    complete: function (e) {
                                                        jQuery("#photo").data("kendoGrid").dataSource.read();
                                                    }
                                                },
                                            },
                                            autoSync: false,
                                            schema: {
                                                data: "data",
                                                total: "data.length",
                                                model: {
                                                    id: "photo_gallery_id",
                                                    fields: {
                                                        photo_gallery_id: {type: "string"},
                                                        photo_name: {type: "string"},
                                                        is_active: {type: "boolean"}


                                                    }
                                                }
                                            }
                                        });
                                        jQuery("#photo").kendoGrid({
                                            dataSource: dataSource,
                                            filterable: true,
                                            pageable: {
                                                refresh: true,
                                                input: true,
                                                numeric: false,
                                                pageSizes: true,
                                                pageSizes: [10, 20, 50],
                                            },
                                            sortable: true,
                                            groupable: true,
                                            toolbar: [{name: "create", text: "Add Photo"}],
                                            columns: [
                                                {field: "photo_name",
                                                    title: "Photo Name", width: "170px",
                                                    editor: photoEditor,
                                                    template: "<img src='<?php echo $con->baseUrl("uploads/event_photo_gallery/") ?>#=photo_name#'   height='100' width='200'/>"
                                                },
                                                {field: "is_active", title: "Active?", template: "#= is_active ? 'Yes' : 'No' #", width: "8%"},
                                                {command: ["edit", "destroy"], title: "Action", width: "20%"}],
                                            editable: "inline"
                                        });
                                    });
                                    function photoEditor(container, options) {
                                        $('<input type="file" id="photo_name" name="photo_name" data-role="upload" />')
                                                .appendTo(container)
                                                .kendoUpload({
                                                    multiple: true,
                                                    async: {
                                                        saveUrl: "../../controller/event_photo_gallery_save.php",
                                                        autoUpload: true
                                                    },
                                                    upload: function (e) {
                                                        e.data = {photo_gallery_id: options.model.photo_gallery_id};
                                                    },
                                                    success: function (e) {
                                                        console.log(e.response);
                                                        options.model.set("photo_name", e.response.image);
                                                    }
                                                });
                                    }

                                </script>

                                <div id="kWindow"></div>
                                <div class="clearfix"></div>
                                <br/>
                                <div>
                                    <input type="submit" class="btn btn-success" name="btnCreatePhotoGallery" value="Next" />
                                </div> 
                            </div>
                            <!-- Event Photo Gallery Tab End Here -->

                            <!-- Event Video Gallery Tab Start Here -->

                            <div>
                                <div id="videogallery"></div>
                                <script type="text/javascript">

                                    jQuery(document).ready(function () {

                                        var dataSource = new kendo.data.DataSource({
                                            pageSize: 10,
                                            transport: {
                                                read: {
                                                    url: "../../controller/video_gallery_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "GET"

                                                },
                                                update: {
                                                    url: "../../controller/video_gallery_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "POST",
                                                    complete: function (e) {

                                                        jQuery("#videogallery").data("kendoGrid").dataSource.read();
                                                    }

                                                },
                                                destroy: {
                                                    url: "../../controller/video_gallery_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "DELETE"

                                                },
                                                create: {
                                                    url: "../../controller/video_gallery_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "PUT",
                                                    complete: function (e) {

                                                        jQuery("#videogallery").data("kendoGrid").dataSource.read();
                                                    }

                                                }

                                            },
                                            autoSync: false,
                                            schema: {
                                                errors: function (e) {

                                                    if (e.error === "yes")

                                                    {

                                                        var message = "";
                                                        message += e.message;
                                                        var window = jQuery("#kWindow");
                                                        if (!window.data("kendoWindow")) {

                                                            window.kendoWindow({
                                                                title: "Error window",
                                                                modal: true,
                                                                height: 100,
                                                                width: 400

                                                            });
                                                        }

                                                        window.data("kendoWindow").center().open();
                                                        window.html('<div style="margin-top:10px;text-align:center;"><span style="color:red;font-size:14px;">' + message + '</span></div>');
                                                        this.cancelChanges();
                                                    }

                                                },
                                                data: "data",
                                                total: "data.length",
                                                model: {
                                                    id: "video_gallery_id",
                                                    fields: {
                                                        video_gallery_id: {editable: false, nullable: true},
                                                        link: {type: "string", validation: {required: true}},
                                                        is_active: {type: "boolean"}

                                                    }

                                                }

                                            }

                                        });
                                        jQuery("#videogallery").kendoGrid({
                                            dataSource: dataSource,
                                            filterable: true,
                                            pageable: {
                                                refresh: true,
                                                input: true,
                                                numeric: false,
                                                pageSizes: [10, 20, 50]

                                            },
                                            sortable: true,
                                            groupable: true,
                                            resizable: true,
                                            toolbar: [{name: "create", text: "Add Video Link"}],
                                            columns: [
                                                {field: "link", title: "Link / URL", id: "link"},
                                                {field: "is_active", title: "Active?", template: "#= is_active ? 'Yes' : 'No' #", width: "10%"},
                                                {command: ["edit", "destroy"], title: "Action", width: "200px"}],
                                            editable: "inline"

                                        });
                                    });</script>
                                <div id="kWindow"></div>
                                <div class="clearfix"></div>
                                <br/>
                                <div>
                                    <input type="submit" class="btn btn-success" name="btnCreateVideoGallery" value="Next" />
                                </div> 
                            </div>
                            <!-- Event Video Gallery Tab End Here -->

                            <!-- Event Activity Tab Start Here -->
                            <div>
                                <div id="activity"></div>
                                <script type="text/javascript">


                                    jQuery(document).ready(function () {
                                        var dataSource = new kendo.data.DataSource({
                                            pageSize: 5,
                                            transport: {
                                                read: {
                                                    url: "../../controller/event_activity_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "GET"
                                                },
                                                update: {
                                                    url: "../../controller/event_activity_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "POST",
                                                    complete: function (e) {
                                                        jQuery("#activity").data("kendoGrid").dataSource.read();
                                                    }
                                                },
                                                destroy: {
                                                    url: "../../controller/event_activity_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "DELETE"
                                                },
                                                create: {
                                                    url: "../../controller/event_activity_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "PUT",
                                                    complete: function (e) {
                                                        jQuery("#activity").data("kendoGrid").dataSource.read();
                                                    }
                                                },
                                            },
                                            autoSync: false,
                                            schema: {
                                                data: "data",
                                                total: "data.length",
                                                model: {
                                                    id: "event_activity_id",
                                                    fields: {
                                                        event_activity_id: {type: "number", editable: false},
                                                        activity_id: {type: "number"},
                                                        activity_name: {type: "string"},
                                                        event_activity_title: {type: "string", validation: {required: true}},
                                                        event_activity_details: {type: "string"},
                                                        image: {type: "string"},
                                                        is_active: {type: "boolean"}


                                                    }
                                                }
                                            }
                                        });
                                        jQuery("#activity").kendoGrid({
                                            dataSource: dataSource,
                                            filterable: true,
                                            pageable: {
                                                refresh: true,
                                                input: true,
                                                numeric: false,
                                                pageSizes: true,
                                                pageSizes: [10, 20, 50],
                                            },
                                            sortable: true,
                                            groupable: true,
                                            toolbar: [{name: "create", text: "Add Activity"}],
                                            columns: [
                                                {field: "activity_id",
                                                    title: "Activity Name",
                                                    width: "130px",
                                                    editor: ActivityNameDropDownEditor,
                                                    template: "#=activity_name#",
                                                    filterable: {
                                                        ui: ActivityNameFilter,
                                                        extra: false,
                                                        operators: {
                                                            string: {
                                                                eq: "Is equal to",
                                                                neq: "Is not equal to"

                                                            }

                                                        }

                                                    }

                                                },
                                                {field: "event_activity_title", title: "Title", width: "200px"},
                                                {field: "event_activity_details",
                                                    title: "Activity Details", width: "550px",
                                                    editor: activitytextAreaEditor
                                                },
                                                {field: "image",
                                                    title: "Activity Image", width: "330px",
                                                    editor: activityfileEditor,
                                                    template: "<img src='<?php echo $con->baseUrl("uploads/event_activity_image/") ?>#=image#'   height='100' width='300'/>"
                                                },
                                                {field: "is_active", title: "Active?", template: "#= is_active ? 'Yes' : 'No' #", width: "80px"},
                                                {command: ["edit", "destroy"], title: "Action", width: "200px"}],
                                            editable: "inline"
                                        });
                                    });
                                    function activityfileEditor(container, options) {
                                        $('<input type="file" id="image" name="image" data-role="upload" />')
                                                .appendTo(container)
                                                .kendoUpload({
                                                    multiple: true,
                                                    async: {
                                                        saveUrl: "../../controller/event_activity_image_save.php",
                                                        autoUpload: true
                                                    },
                                                    upload: function (e) {
                                                        e.data = {event_activity_id: options.model.event_activity_id};
                                                    },
                                                    success: function (e) {
                                                        console.log(e.response);
                                                        options.model.set("image", e.response.image);
                                                    }
                                                });
                                    }

                                    function activitytextAreaEditor(container, options) {
                                        $('<textarea id="event_activity_details" name="event_activity_details" rows="3" cols="30">' + options.field + '</textarea>')
                                                .appendTo(container)
                                                .kendoEditor({
                                                    tools: [
                                                        "bold",
                                                        "italic",
                                                        "underline",
                                                        "strikethrough",
                                                        "justifyLeft",
                                                        "justifyCenter",
                                                        "justifyRight",
                                                        "justifyFull",
                                                        "insertUnorderedList",
                                                        "insertOrderedList",
                                                        "indent",
                                                        "outdent",
                                                        "createLink",
                                                        "unlink",
                                                        "insertImage",
                                                        "insertFile",
                                                        "subscript",
                                                        "superscript",
                                                        "createTable",
                                                        "addRowAbove",
                                                        "addRowBelow",
                                                        "addColumnLeft",
                                                        "addColumnRight",
                                                        "deleteRow",
                                                        "deleteColumn",
                                                        "viewHtml",
                                                        "formatting",
                                                        "cleanFormatting",
                                                        "fontName",
                                                        "fontSize",
                                                        "foreColor",
                                                        "backColor"
                                                    ]
                                                });
                                    }

                                </script>
                                <script type="text/javascript">

                                    function ActivityNameFilter(element) {

                                        element.kendoDropDownList({
                                            autoBind: false,
                                            dataTextField: "activity_name",
                                            dataValueField: "activity_id",
                                            dataSource: {
                                                transport: {
                                                    read: {
                                                        url: "../../controller/activity_controller.php",
                                                        type: "GET"

                                                    }

                                                },
                                                schema: {
                                                    data: "data"

                                                }

                                            },
                                            optionLabel: "Select Activity"

                                        });
                                    }

                                    function ActivityNameDropDownEditor(container, options) {

                                        jQuery('<input required data-text-field="activity_name" data-value-field="activity_id" data-bind="value:' + options.field + '"/>')

                                                .appendTo(container)

                                                .kendoDropDownList({
                                                    autoBind: false,
                                                    dataTextField: "activity_name",
                                                    dataValueField: "activity_id",
                                                    dataSource: {
                                                        transport: {
                                                            read: {
                                                                url: "../../controller/activity_controller.php",
                                                                type: "GET"

                                                            }

                                                        },
                                                        schema: {
                                                            data: "data"

                                                        }

                                                    },
                                                    optionLabel: "Select Activity"

                                                });
                                    }
                                </script>
                                <div id="kWindow"></div>
                                <div class="clearfix"></div>
                                <br/>
                                <div>
                                    <input type="submit" class="btn btn-success" name="btnCreateActivity" value="Next" />
                                </div>
                            </div>
                            <!-- Event Attraction Tab End Here -->

                            <!-- Event FAQ Tab Start Here -->
                            <div>
                                <div id="faq"></div>
                                <script type="text/javascript">

                                    jQuery(document).ready(function () {

                                        var dataSource = new kendo.data.DataSource({
                                            pageSize: 10,
                                            transport: {
                                                read: {
                                                    url: "../../controller/faq_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "GET"

                                                },
                                                update: {
                                                    url: "../../controller/faq_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "POST",
                                                    complete: function (e) {

                                                        jQuery("#faq").data("kendoGrid").dataSource.read();
                                                    }

                                                },
                                                destroy: {
                                                    url: "../../controller/faq_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "DELETE"

                                                },
                                                create: {
                                                    url: "../../controller/faq_controller.php?event_id=<?php echo (isset($_SESSION["tmp_event_id_1"]) ? $_SESSION["tmp_event_id_1"] : ""); ?>",
                                                    type: "PUT",
                                                    complete: function (e) {

                                                        jQuery("#faq").data("kendoGrid").dataSource.read();
                                                    }

                                                }

                                            },
                                            autoSync: false,
                                            schema: {
                                                errors: function (e) {

                                                    if (e.error === "yes")

                                                    {

                                                        var message = "";
                                                        message += e.message;
                                                        var window = jQuery("#kWindow");
                                                        if (!window.data("kendoWindow")) {

                                                            window.kendoWindow({
                                                                title: "Error window",
                                                                modal: true,
                                                                height: 100,
                                                                width: 400

                                                            });
                                                        }

                                                        window.data("kendoWindow").center().open();
                                                        window.html('<div style="margin-top:10px;text-align:center;"><span style="color:red;font-size:14px;">' + message + '</span></div>');
                                                        this.cancelChanges();
                                                    }

                                                },
                                                data: "data",
                                                total: "data.length",
                                                model: {
                                                    id: "faq_id",
                                                    fields: {
                                                        faq_id: {editable: false, nullable: true},
                                                        question: {type: "string", validation: {required: true}},
                                                        answer: {type: "string"},
                                                        is_active: {type: "boolean"}
                                                    }

                                                }

                                            }

                                        });
                                        jQuery("#faq").kendoGrid({
                                            dataSource: dataSource,
                                            filterable: true,
                                            pageable: {
                                                refresh: true,
                                                input: true,
                                                numeric: false,
                                                pageSizes: [10, 20, 50]

                                            },
                                            sortable: true,
                                            groupable: true,
                                            resizable: true,
                                            toolbar: [
                                                {name: "create", text: "Add FAQ"}
                                            ],
                                            columns: [
                                                {field: "question",
                                                    title: "Question?", width: "250px",
                                                    editor: questiontextAreaEditor
                                                },
                                                {field: "answer",
                                                    title: "Answer", width: "250px",
                                                    editor: answertextAreaEditor
                                                },
                                                {field: "is_active", title: "Active?", template: "#= is_active ? 'Yes' : 'No' #", width: "10%"},
                                                {command: ["edit", "destroy"], title: "Action", width: "130px"}
                                            ],
                                            editable: "inline"

                                        });
                                    });
                                    function answertextAreaEditor(container, options) {
                                        $('<textarea id="answer" name="answer" rows="3" cols="30">' + options.field + '</textarea>')
                                                .appendTo(container)
                                                .kendoEditor({
                                                    tools: [
                                                        "bold",
                                                        "italic",
                                                        "underline",
                                                        "strikethrough",
                                                        "justifyLeft",
                                                        "justifyCenter",
                                                        "justifyRight",
                                                        "justifyFull",
                                                        "insertUnorderedList",
                                                        "insertOrderedList",
                                                        "indent",
                                                        "outdent",
                                                        "createLink",
                                                        "unlink",
                                                        "insertImage",
                                                        "insertFile",
                                                        "subscript",
                                                        "superscript",
                                                        "createTable",
                                                        "addRowAbove",
                                                        "addRowBelow",
                                                        "addColumnLeft",
                                                        "addColumnRight",
                                                        "deleteRow",
                                                        "deleteColumn",
                                                        "viewHtml",
                                                        "formatting",
                                                        "cleanFormatting",
                                                        "fontName",
                                                        "fontSize",
                                                        "foreColor",
                                                        "backColor"
                                                    ]
                                                });
                                    }

                                    function questiontextAreaEditor(container, options) {
                                        $('<textarea id="question" name="question" rows="3" cols="30">' + options.field + '</textarea>')
                                                .appendTo(container)
                                                .kendoEditor({
                                                    tools: [
                                                        "bold",
                                                        "italic",
                                                        "underline",
                                                        "strikethrough",
                                                        "justifyLeft",
                                                        "justifyCenter",
                                                        "justifyRight",
                                                        "justifyFull",
                                                        "insertUnorderedList",
                                                        "insertOrderedList",
                                                        "indent",
                                                        "outdent",
                                                        "createLink",
                                                        "unlink",
                                                        "insertImage",
                                                        "insertFile",
                                                        "subscript",
                                                        "superscript",
                                                        "createTable",
                                                        "addRowAbove",
                                                        "addRowBelow",
                                                        "addColumnLeft",
                                                        "addColumnRight",
                                                        "deleteRow",
                                                        "deleteColumn",
                                                        "viewHtml",
                                                        "formatting",
                                                        "cleanFormatting",
                                                        "fontName",
                                                        "fontSize",
                                                        "foreColor",
                                                        "backColor"
                                                    ]
                                                });
                                    }
                                </script>		
                                <div id="kWindow"></div>
                                <div class="clearfix"></div>

                                <br/>

                                <div>

                                    <input type="submit" class="btn btn-success" name="btnCreateFAQ" value="Next" />

                                </div>
                            </div>
                            <!-- Event FAQ Tab End Here -->

                        </div>
                    </div>
                    <script type="text/javascript">

                        $(document).ready(function () {

                            var kendotab = $("#kendotab").kendoTabStrip({
                                animation: {
                                    open: {
                                        effects: "fadeIn"

                                    }

                                }

                            }).data("kendoTabStrip");
                            kendotab.enable(kendotab.tabGroup.children("li:eq(1)"), false);
                            kendotab.enable(kendotab.tabGroup.children("li:eq(2)"), false);
                            kendotab.enable(kendotab.tabGroup.children("li:eq(3)"), false);
                            kendotab.enable(kendotab.tabGroup.children("li:eq(4)"), false);
                            kendotab.enable(kendotab.tabGroup.children("li:eq(5)"), false);
                            kendotab.enable(kendotab.tabGroup.children("li:eq(6)"), false);
                            kendotab.enable(kendotab.tabGroup.children("li:eq(7)"), false);
                            kendotab.enable(kendotab.tabGroup.children("li:eq(8)"), false);
                            kendotab.enable(kendotab.tabGroup.children("li:eq(9)"), false);
                            kendotab.enable(kendotab.tabGroup.children("li:eq(10)"), false);
                            kendotab.enable(kendotab.tabGroup.children("li:eq(11)"), false);
                            kendotab.enable(kendotab.tabGroup.children("li:eq(12)"), false);
<?php if (isset($_SESSION["tab_1"])): ?>

                                kendotab.select(1);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(1)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(2)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(3)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(4)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(5)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(6)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(7)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(8)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(9)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(10)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(11)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(12)"), false);
<?php endif; ?>

<?php if (isset($_SESSION["tab_2"])): ?>

                                kendotab.select(2);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(1)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(2)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(3)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(4)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(5)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(6)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(7)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(8)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(9)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(10)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(11)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(12)"), false);
<?php endif; ?>

<?php if (isset($_SESSION["tab_3"])): ?>

                                kendotab.select(3);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(1)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(2)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(3)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(4)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(5)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(6)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(7)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(8)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(9)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(10)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(11)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(12)"), false);
<?php endif; ?>

<?php if (isset($_SESSION["tab_4"])): ?>

                                kendotab.select(4);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(1)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(2)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(3)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(4)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(5)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(6)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(7)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(8)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(9)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(10)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(11)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(12)"), false);
<?php endif; ?>

<?php if (isset($_SESSION["tab_5"])): ?>

                                kendotab.select(5);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(1)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(2)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(3)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(4)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(5)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(6)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(7)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(8)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(9)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(10)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(11)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(12)"), false);
<?php endif; ?>

<?php if (isset($_SESSION["tab_6"])): ?>

                                kendotab.select(6);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(1)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(2)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(3)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(4)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(5)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(6)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(7)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(8)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(9)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(10)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(11)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(12)"), false);
<?php endif; ?>

<?php if (isset($_SESSION["tab_7"])): ?>

                                kendotab.select(7);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(1)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(2)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(3)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(4)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(5)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(6)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(7)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(8)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(9)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(10)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(11)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(12)"), false);
<?php endif; ?>
<?php if (isset($_SESSION["tab_8"])): ?>

                                kendotab.select(8);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(1)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(2)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(3)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(4)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(5)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(6)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(7)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(8)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(9)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(10)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(11)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(12)"), false);
<?php endif; ?>
<?php if (isset($_SESSION["tab_9"])): ?>

                                kendotab.select(9);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(1)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(2)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(3)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(4)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(5)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(6)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(7)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(8)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(9)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(10)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(11)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(12)"), false);
<?php endif; ?>

<?php if (isset($_SESSION["tab_10"])): ?>

                                kendotab.select(10);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(1)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(2)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(3)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(4)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(5)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(6)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(7)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(8)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(9)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(10)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(11)"), false);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(12)"), false);
<?php endif; ?>


<?php if (isset($_SESSION["tab_11"])): ?>

                                kendotab.select(11);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(1)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(2)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(3)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(4)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(5)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(6)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(7)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(8)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(9)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(10)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(11)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(12)"), false);
<?php endif; ?>
<?php if (isset($_SESSION["tab_12"])): ?>

                                kendotab.select(12);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(1)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(2)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(3)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(4)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(5)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(6)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(7)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(8)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(9)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(10)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(11)"), true);
                                kendotab.enable(kendotab.tabGroup.children("li:eq(12)"), true);
<?php endif; ?>

                        });</script>
                </div>
                <!-- Create New Event Tab End-->

                <!-- My Event Tab -->
                <div>
                    <div id="my_event" style="margin-top: 15px;"></div>
                    <script id="edit_event_button" type="text/x-kendo-template">
                        <a class="k-button k-button-icontext k-grid-edit" href="edit_event.php?event_id=#= event_id#"><span class="k-icon k-edit"></span>Edit Event</a>
                    </script>
                    <script type="text/javascript">
                        jQuery(document).ready(function () {
                            var dataSource = new kendo.data.DataSource({
                                pageSize: 10,
                                transport: {
                                    read: {
                                        url: "../../controller/my_event_list.php",
                                        type: "GET"
                                    },
                                    update: {
                                        url: "../../controller/my_event_list.php",
                                        type: "POST",
                                        complete: function (e) {
                                            jQuery("#my_event").data("kendoGrid").dataSource.read();
                                        }
                                    },
                                    destroy: {
                                        url: "../../controller/my_event_list.php",
                                        type: "DELETE"
                                    },
                                    create: {
                                        url: "../../controller/my_event_list.php",
                                        type: "PUT",
                                        complete: function (e) {
                                            jQuery("#my_event").data("kendoGrid").dataSource.read();
                                        }
                                    }
                                },
                                autoSync: false,
                                schema: {
                                    errors: function (e) {
                                        if (e.error === "yes")
                                        {
                                            var message = "";
                                            message += e.message;
                                            var window = jQuery("#kWindow");
                                            if (!window.data("kendoWindow")) {
                                                window.kendoWindow({
                                                    title: "Error window",
                                                    modal: true,
                                                    height: 100,
                                                    width: 400
                                                });
                                            }
                                            window.data("kendoWindow").center().open();
                                            window.html('<div style="margin-top:10px;text-align:center;"><span style="color:red;font-size:14px;">' + message + '</span></div>');
                                            this.cancelChanges();
                                        }
                                    },
                                    data: "data",
                                    total: "data.length",
                                    model: {
                                        id: "event_id",
                                        fields: {
                                            event_id: {editable: false, nullable: true},
                                            event_title: {type: "string"},
                                            category_name: {type: "string", validation: {required: true}},
                                            ticket_type: {type: "string"},
                                            total_ticket: {type: "number"},
                                            sold_ticket: {type: "number"},
                                            expiry_date: {type: "string"},
                                            status: {type: "string"}
                                        }
                                    }
                                }
                            });
                            jQuery("#my_event").kendoGrid({
                                dataSource: dataSource,
                                filterable: true,
                                pageable: {
                                    refresh: true,
                                    input: true,
                                    numeric: false,
                                    pageSizes: [10, 20, 50]

                                },
                                sortable: true,
                                groupable: true,
                                resizable: true,
                                //toolbar: [{name: "create", text: "Add Category"}],
                                columns: [
                                    {field: "event_id", title: "Event ID"},
                                    {field: "event_title", title: "Event Title"},
                                    {field: "category_name", title: "Category"},
                                    {field: "ticket_type", title: "Ticket Type"},
                                    {field: "total_ticket", title: "Total Ticket"},
                                    {field: "sold_ticket", title: "Sold Ticket"},
                                    {field: "expiry_date", title: "Expiry Date"},
                                    {field: "status", title: "Status"},
                                    {title: "Action", template: kendo.template(jQuery("#edit_event_button").html())}

                                ]

                            });
                        });</script>		
                    <div id="kWindow"></div>
                </div>
                <!-- My Event Tab End Here -->

                <!-- Report Tab Start Here -->
                <div class="row">
                    OK
                </div>
                <!-- Report Tab End Here -->
                <!-- Account Settings Tab Start Here -->

                <script type="text/javascript" src="<?php echo $con->baseUrl("lib/jquery.maskedinput-1.3.1.min_.js"); ?>"></script>
                <div class="row">
                    <div id="account_settings_kendo_tab" style="margin-left: -10px;margin-top: 15px;margin-right: -10px">
                        <ul>
                            <li class="k-state-active">
                                General Information Change
                            </li>
                            <li>
                                Password Change
                            </li>
                        </ul>
                        <!-- General Information Tab Start Here -->
                        <div class="k-block">

                            <div class="maincontentinner">
                                <div class="row">

                                    <div class="col-md-10">

                                        <?php echo(!empty($msg) ? $con->notification($msg, '') : $con->notification('', $err)); ?>

                                    </div>

                                </div>

                                <br />
                                <div name="account_settings" id="account_settings">
                                    <div class="row">

                                        <div class="col-md-5">

                                            <div class="col-md-3">First Name:</div>

                                            <div class="col-md-7"><?php echo $con->TextBox("admin_first_name", "admin_first_name", "width:100%", "First Name", "", $admin_first_name, "kendo"); ?></div>

                                        </div>

                                        <div class="col-md-5">

                                            <div class="col-md-3">Last Name:</div>

                                            <div class="col-md-7"><?php echo $con->TextBox("admin_last_name", "admin_last_name", "width:100%", "Last Name", "", $admin_last_name, "kendo"); ?></div>

                                        </div>

                                    </div>
                                    <br/>
                                    <div class="row">

                                        <div class="col-md-5">

                                            <div class="col-md-3">Mobile Number:</div>

                                            <div class="col-md-7"><?php echo $con->TextBox("admin_mobile_number", "admin_mobile_number", "width:100%", "---.--------", "", $admin_mobile_number, "kendo"); ?></div>
                                            <script type="text/javascript">

                        $(document).ready(function () {

                            $('#admin_mobile_number').mask('999-99999999');
                        });

                                            </script>
                                        </div>

                                        <div class="col-md-5">

                                            <div class="col-md-3">Address</div>

                                            <div class="col-md-7"><?php echo $con->TextArea("admin_address", "admin_address", $admin_address, "4", "34", "resize: none;border-style:none;border:1px solid;", ""); ?></div>

                                        </div>

                                    </div>

                                    <br/>
                                    <div class="row" >

                                        <div class="col-md-6" style="float: right; margin-right: -298px;">
                                            <a href="javascript:void(0);" style="text-decoration: none;" id="account_change" class="k-button btn-success">Change Account</a>

                                        </div>

                                    </div>

                                </div>
                                <br/>

                            </div>

                        </div>
                        <script type="text/javascript">

                            $(document).ready(function () {
                                $("#account_change").click(function () {

                                    var admin_first_name = $("#admin_first_name").val();
                                    var admin_last_name = $("#admin_last_name").val();
                                    var admin_mobile_number = $("#admin_mobile_number").val();
                                    var admin_address = $("#admin_address").val();
                                    if (admin_first_name === '') {
                                        $(".error").html('');
                                        $("#admin_first_name").after('<span class="error" style="color:red;">Please Enter First Name</span>');
                                    } else if (admin_last_name === '') {
                                        $(".error").html('');
                                        $("#admin_last_name").after('<span class="error" style="color:red;">Please Enter Last Name</span>');
                                    } else if (admin_mobile_number === '') {
                                        $(".error").html('');
                                        $("#admin_mobile_number").after('<span class="error" style="color:red;">Please Enter Mobile Number</span>');
                                    } else {

                                        $.ajax({
                                            type: "POST",
                                            url: "../../controller/account_settings.php",
                                            data: {
                                                admin_first_name: admin_first_name,
                                                admin_last_name: admin_last_name,
                                                admin_mobile_number: admin_mobile_number,
                                                admin_address: admin_address
                                            },
                                            dataType: "json",
                                            success: function (response) {
                                                console.log(response);
                                                if (response.output !== "error") {
                                                    alert("Your Account Information Changed Successfully");
                                                    window.location = "index.php";
                                                } else {
                                                    alert(response.msg);
                                                }
                                            }
                                        });
                                    }

                                });
                            });</script>
                        <!-- General Information Tab End Here -->

                        <!-- Password Change Settings Tab Start Here -->
                        <div name="password_change_tab" id="password_change_tab">
                            <div class="row" style="margin-top: 14px;">

                                <div class="col-md-5">

                                    <div class="col-md-3">New Password:</div>
                                    <div class="col-md-7"><?php echo $con->PasswordTextBox("new_password", "new_password", "width:100%", "********", "", "", "kendo"); ?></div>

                                </div>

                                <div class="col-md-5">

                                    <div class="col-md-3">Confirm Password:</div>

                                    <div class="col-md-7"><?php echo $con->PasswordTextBox("confirm_password", "confirm_password", "width:100%", "********", "", "", "kendo"); ?></div>

                                </div>

                            </div>
                            <br/>
                            <div class="row" >

                                <div class="col-md-6" style="float: right; margin-right: -287px;">
                                    <a href="javascript:void(0);" style="text-decoration: none;" id="password_change" class="k-button btn-success">Change Password</a>

                                </div>

                            </div>
                        </div>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                $("#password_change").click(function () {

                                    var new_password = $("#new_password").val();
                                    var confirm_password = $("#confirm_password").val();
                                    if (new_password === '') {
                                        $(".error").html('');
                                        $("#new_password").after('<span class="error" style="color:red;">Please Enter New Password</span>');
                                    } else if (confirm_password === '') {
                                        $(".error").html('');
                                        $("#confirm_password").after('<span class="erroir" style="color:red;">Please Enter Confirm Password</span>');
                                    } else if (new_password !== confirm_password) {
                                        $(".error").html('');
                                        $("#confirm_password").after('<span class="error" style="color:red;">Password Do Not Match</span>');
                                    } else {
                                        $.ajax({
                                            type: "POST",
                                            url: "../../controller/password_change.php",
                                            data: {new_password: new_password},
                                            dataType: "json",
                                            success: function (response) {
                                                console.log(response);
                                                if (response.output !== "error") {
                                                    alert("Password Changed Successfully");
                                                    window.location = "index.php";
                                                } else {
                                                    alert(response.msg);
                                                }
                                            }
                                        });
                                    }
                                });
                            });

                        </script>
                        <!-- Password Change Settings Tab End Here -->
                    </div>
                </div>
                <!-- Account Settings Tab end Here -->


                <!-- Contact Support Desk Tab Start Here-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <h4>Contact Information</h4>
                            <ul>
                                <li>Razzak Plaza (8th Floor),1 New Eskaton Road,Moghbazar Circle, Dhaka-1217 </li>
                                <li>Phone: +8801971842538,+8804478009569</li>
                                <li>Website:  www.ticketchai.com</li>
                                <li>Email: support@ticketchai.com</li>
                            </ul>
                        </div>
                        <div class="col-md-6" style="margin-left: 703px;margin-top: -82px;">
                            <img src="../../assets/images/img_1.png"/>
                        </div>
                    </div>
                </div>
                <!-- Contact Support Desk Tab End Here-->
            </div>

        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                var kendotab = $("#main_kendo_tab").kendoTabStrip({
                    animation: {
                        open: {
                            effects: "fadeIn"

                        }

                    }

                }).data("kendoTabStrip");
            });

        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                var kendotab = $("#account_settings_kendo_tab").kendoTabStrip({
                    animation: {
                        open: {
                            effects: "fadeIn"

                        }

                    }

                }).data("kendoTabStrip");
            });
        </script>

    </div>

</form>


<?php include("../layout/copyright.php"); ?>