<?php
include("../../config/class.web.config.php");
$con = new Config();

//------------- Result for dinamic features ------------------//
$query ="SELECT f.feature_name,f.feature_id FROM event_feature as ef 
INNER JOIN event as es ON es.event_id = ef.event_id
INNER JOIN  feature as f ON f.feature_id = ef.feature_id";

$features =$con->ReturnObjectByQuery($query, "array");
$con->debug($features);
//----------- Result for dinamic features ---------------------//
?>
<?php include("../layout/header_script.php"); ?>
<?php include("../layout/menu_top.php"); ?>
<?php include("../layout/breadcrumbs.php"); ?>
<?php include("../layout/left_navigation.php"); ?>
<div class="row">
    <?php echo(!empty($msg) ? $con->notification($msg, '') : $con->notification('', $err)); ?>
</div>
<div class="row">
    <div id="kendotab">
        <ul>
          
            <li class="k-state-active">
                Paris
            </li>
            <li>
                New York
            </li>
            <li>
                London
            </li>
            <li>
                Moscow
            </li>
            <li>
                Sydney
            </li>
        </ul>
        <div>
            <div class="weather">
                <h2>17<span>&ordm;C</span></h2>
                <p>Rainy weather in Paris.</p>
            </div>
            <span class="rainy">&nbsp;</span>
        </div>
        <div>
            <div class="weather">
                <h2>29<span>&ordm;C</span></h2>
                <p>Sunny weather in New York.</p>
            </div>
            <span class="sunny">&nbsp;</span>
        </div>
        <div>
            <div class="weather">
                <h2>21<span>&ordm;C</span></h2>
                <p>Sunny weather in London.</p>
            </div>
            <span class="sunny">&nbsp;</span>
        </div>
        <div>
            <div class="weather">
                <h2>16<span>&ordm;C</span></h2>
                <p>Cloudy weather in Moscow.</p>
            </div>
            <span class="cloudy">&nbsp;</span>
        </div>
        <div>
            <div class="weather">
                <h2>17<span>&ordm;C</span></h2>
                <p>Rainy weather in Sydney.</p>
            </div>
            <span class="rainy">&nbsp;</span>
        </div>
    </div> 

    <script type="text/javascript">
        $(document).ready(function () {
            $("#kendotab").kendoTabStrip({
                animation: {
                    open: {
                        effects: "fadeIn"
                    }
                }
            });
        });
    </script>
</div>

<?php include("../layout/footer.php"); ?>
<?php include("../layout/copyright.php"); ?>
