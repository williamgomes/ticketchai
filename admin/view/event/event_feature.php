<?php
session_start();
include("../../config/class.web.config.php");
$con = new Config();
if ($con->authenticate() == 1) {
    $con->redirect("../../login.php");
}
/* End Authentication */
?>
<?php include("../layout/header_script.php"); ?>
<?php include("../layout/menu_top.php"); ?>
<?php include("../layout/breadcrumbs.php"); ?>
<div class="row">
    <div class="col-md-12">
        <div style="margin-right: 15px; margin-left: 15px;" id="grid"></div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        var dataSource = new kendo.data.DataSource({
            pageSize: 5,
            transport: {
                read: {
                    url: "../../controller/event_feature_controller.php",
                    type: "GET"
                },
                update: {
                    url: "../../controller/event_feature_controller.php",
                    type: "POST",
                    complete: function (e) {
                        jQuery("#grid").data("kendoGrid").dataSource.read();
                    }
                },
                destroy: {
                    url: "../../controller/event_feature_controller.php",
                    type: "DELETE"
                },
                create: {
                    url: "../../controller/event_feature_controller.php",
                    type: "PUT",
                    complete: function (e) {
                        jQuery("#grid").data("kendoGrid").dataSource.read();
                    }
                },
            },
            autoSync: false,
            schema: {
                errors: function (e) {
                    //alert(e.error);
                    if (e.error === "yes")
                    {
                        var message = "";
                        message += e.message;
                        var window = jQuery("#kWindow");
                        if (!window.data("kendoWindow")) {
                            window.kendoWindow({
                                title: "Error window",
                                modal: true,
                                height: 100,
                                width: 400
                            });
                        }

                        window.data("kendoWindow").center().open();
                        window.html('<div style="margin-top:10px;text-align:center;"><span style="color:red;font-size:14px;">' + message + '</span></div>');
                        //var grid = $("#RBOGrid").data("kendoGrid");
                        this.cancelChanges();
                    }
                },
                data: "data",
                total: "data.length",
                model: {
                    id: "event_feature_id",
                    fields: {
                        event_feature_id: {editable: false, nullable: true},
                        event_id: {type: "number"},
                        event_title: {type: "string", validation: {required: true}},
                        feature_id: {type: "number"},
                        feature_name: {type: "string", validation: {required: true}},
                        is_active: {type: "boolean"}
                    }
                }
            }
        });
        jQuery("#grid").kendoGrid({
            dataSource: dataSource,
            filterable: true,
            pageable: {
                refresh: true,
                input: true,
                numeric: false,
                pageSizes: true,
                pageSizes: [5, 10, 20, 30, 50],
            },
            sortable: true,
            groupable: true,
            toolbar: [{name: "create", text: "Event Feature Mapping"}],
            columns: [
                {field: "event_id",
                    title: "Event Title",
                    editor: EventDropDownEditor,
                    template: "#=event_title#",
                    filterable: {
                        ui: EventFilter,
                        extra: false,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to"
                            }
                        }
                    }
                },
                {field: "feature_id",
                    title: "Feature Name",
                    editor: FeatureDropDownEditor,
                    template: "#=feature_name#",
                    filterable: {
                        ui: FeatureFilter,
                        extra: false,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to"
                            }
                        }
                    }
                },
                {field: "is_active", title: "Active?", template: "#= is_active ? 'Yes' : 'No' #", width: "10%"},
                {command: ["edit", "destroy"], title: "Action", width: "180px"}],
            editable: "inline"
        });
    });
</script>				

<script type="text/javascript">
    function EventFilter(element) {
        element.kendoComboBox({
            autoBind: false,
            dataTextField: "event_title",
            dataValueField: "event_id",
            dataSource: {
                transport: {
                    read: {
                        url: "../../controller/event_controller.php",
                        type: "GET"
                    }
                },
                schema: {
                    data: "data"
                }
            },
            optionLabel: "Select Event"
        });
    }
    function FeatureFilter(element) {
        element.kendoComboBox({
            autoBind: false,
            dataTextField: "feature_name",
            dataValueField: "feature_id",
            dataSource: {
                transport: {
                    read: {
                        url: "../../controller/feature_controller.php",
                        type: "GET"
                    }
                },
                schema: {
                    data: "data"
                }
            },
            optionLabel: "Select Feature"
        });
    }

    function EventDropDownEditor(container, options) {
        jQuery('<input required data-text-field="event_title" data-value-field="event_id" data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoComboBox({
                    autoBind: false,
                    dataTextField: "event_title",
                    dataValueField: "event_id",
                    dataSource: {
                        transport: {
                            read: {
                                url: "../../controller/event_controller.php",
                                type: "GET"
                            }
                        },
                        schema: {
                            data: "data"
                        }
                    },
                    optionLabel: "Select Event"
                });
    }

    function FeatureDropDownEditor(container, options) {
        jQuery('<input required data-text-field="feature_name" data-value-field="feature_id" data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoComboBox({
                    autoBind: false,
                    dataTextField: "feature_name",
                    dataValueField: "feature_id",
                    dataSource: {
                        transport: {
                            read: {
                                url: "../../controller/feature_controller.php",
                                type: "GET"
                            }
                        },
                        schema: {
                            data: "data"
                        }
                    },
                    optionLabel: "Select Feature"
                });
    }
</script>
<div id="kWindow"></div>


<?php //include("../layout/footer.php");  ?>
<?php include("../layout/copyright.php"); ?>
