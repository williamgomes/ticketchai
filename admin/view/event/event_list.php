<?php
include("../../config/class.web.config.php");
$con = new Config();
/* Start Authentication */
if ($con->authenticate() == 1) {
    $con->redirect("../../login.php");
}
/* End Authentication */
?>

<?php include("../layout/header_script.php"); ?>
<?php include("../layout/menu_top.php"); ?>
<?php include("../layout/breadcrumbs.php"); ?>

<div class="row">
    <div class="col-md-12">
        <div style="margin-right: 15px; margin-left: 15px;" id="grid"></div>
    </div>
</div>
<script type="text/javascript">


</script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        var dataSource = new kendo.data.DataSource({
            pageSize: 5,
            transport: {
                read: {
                    url: "../../controller/event_controller.php",
                    type: "GET"
                },
                update: {
                    url: "../../controller/event_controller.php",
                    type: "POST",
                    complete: function (e) {

                        jQuery("#grid").data("kendoGrid").dataSource.read();

                    }

                },
                destroy: {
                    url: "../../controller/event_controller.php",
                    type: "DELETE"

                }
            },
            autoSync: false,
            schema: {
                errors: function (e) {
                    if (e.error === "yes")
                    {
                        var message = "";
                        message += e.message;
                        var window = jQuery("#kWindow");
                        if (!window.data("kendoWindow")) {
                            window.kendoWindow({
                                title: "Error window",
                                modal: true,
                                height: 100,
                                width: 400
                            });
                        }

                        window.data("kendoWindow").center().open();
                        window.html('<div style="margin-top:10px;text-align:center;"><span style="color:red;font-size:14px;">' + message + '</span></div>');
                        this.cancelChanges();
                    }
                },
                data: "data",
                total: "data.length",
                model: {
                    id: "event_id",
                    fields: {
                        event_id: {editable: false, nullable: true},
                        event_title: {editable: false,validation: {required: true}},
                        event_ticket_expair_date: {editable: false,type: "string"},
                        event_logo_image: {editable: false,type: "string"},
                        event_full_size_image: {editable: false,type: "string"},
                        featured: {type: "boolean"},
                        f_priority: {type: "number"},
                        upcomming: {type: "boolean"},
                        u_priority: {type: "number"}
                    }
                }
            }
        });
        jQuery("#grid").kendoGrid({
            dataSource: dataSource,
            //filterable: true,
            pageable: {
                refresh: true,
                input: true,
                numeric: false,
                pageSizes: [5, 10, 20, 50]
            },
            sortable: true,
            groupable: true,
            resizable: true,
            scrollable: true,
            toolbar: [{text: "Event List"}],
            columns: [
                {field: "event_title", title: "Event Title",width:"200px"},
                {field: "event_ticket_expair_date", title: "Expire Date",width:"120px"},
                {field: "event_logo_image",
                    title: "Logo Image",
                    width:"200px",
                    template: "<img src='<?php echo $con->baseUrl("uploads/event_logo_image/") ?>#=event_logo_image#' height='50' width='200'/>"
                },
                {field: "event_full_size_image",
                    title: "Full Image",
                    width:"200px",
                    template: "<img src='<?php echo $con->baseUrl("uploads/event_full_size_image/") ?>#=event_full_size_image#' height='50' width='200'/>"
                },
                {field: "featured", title: "Is Featured?", template: "#= featured ? 'True' : 'False' #", width: "100px"},
                {field: "f_priority", title: "F Priority",width: "100px", validation: {min: 0, required: true}},
                {field: "upcomming", title: "Is Upcomming?",width: "100px", template: "#= upcomming ? 'True' : 'False' #", width: "100px"},
                {field: "u_priority", title: "U Priority",width: "100px", validation: {min: 0, required: true, format: "n"}},
                {command: ["edit", "destroy"], title: "Action", width: "200px"}],
            editable: "inline"

        });
    });
</script>

<div id="kWindow"></div>

<?php include("../layout/copyright.php"); ?>

