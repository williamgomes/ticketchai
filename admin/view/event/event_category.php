
<?php

session_start();

include("../../config/class.web.config.php");

$con = new Config();

if ($con->authenticate() == 1) {

    $con->redirect("../../login.php");

}

/* End Authentication */

?>

<?php include("../layout/header_script.php"); ?>

<?php include("../layout/menu_top.php"); ?>

<?php include("../layout/breadcrumbs.php"); ?>

<div class="row">

    <div class="col-md-12">

        <div style="margin-right: 15px; margin-left: 15px;" id="grid"></div>

    </div>

</div>

<script type="text/javascript">

    jQuery(document).ready(function () {

        var dataSource = new kendo.data.DataSource({

            pageSize: 5,

            transport: {

                read: {

                    url: "../../controller/event_category_controller.php",

                    type: "GET"

                },

                update: {

                    url: "../../controller/event_category_controller.php",

                    type: "POST",

                    complete: function (e) {

                        jQuery("#grid").data("kendoGrid").dataSource.read();

                    }

                },

                destroy: {

                    url: "../../controller/event_category_controller.php",

                    type: "DELETE"

                },

                create: {

                    url: "../../controller/event_category_controller.php",

                    type: "PUT",

                    complete: function (e) {

                        jQuery("#grid").data("kendoGrid").dataSource.read();

                    }

                },

            },

            autoSync: false,

            schema: {

                errors: function (e) {

                    //alert(e.error);

                    if (e.error === "yes")

                    {

                        var message = "";

                        message += e.message;

                        var window = jQuery("#kWindow");

                        if (!window.data("kendoWindow")) {

                            window.kendoWindow({

                                title: "Error window",

                                modal: true,

                                height: 100,

                                width: 400

                            });

                        }



                        window.data("kendoWindow").center().open();

                        window.html('<div style="margin-top:10px;text-align:center;"><span style="color:red;font-size:14px;">' + message + '</span></div>');

                        //var grid = $("#RBOGrid").data("kendoGrid");

                        this.cancelChanges();

                    }

                },

                data: "data",

                total: "data.length",

                model: {

                    id: "event_category_id",

                    fields: {

                        event_category_id: {editable: false, nullable: true},

                        event_id: {type: "number"},

                        event_title: {type: "string", validation: {required: true}},

                        category_id: {type: "number"},

                        category_name: {type: "string", validation: {required: true}}

                        //is_active: {type: "boolean"}

                    }

                }

            }

        });

        jQuery("#grid").kendoGrid({

            dataSource: dataSource,

            filterable: true,

            pageable: {

                refresh: true,

                input: true,

                numeric: false,

                pageSizes: true,

                pageSizes: [5, 10, 20, 30, 50],

            },

            sortable: true,

            groupable: true,

            toolbar: [{name: "create", text: "Add Event Category"}],

            columns: [

                {field: "event_id",

                    title: "Event Title",

                    editor: EventDropDownEditor,

                    template: "#=event_title#",

                    filterable: {

                        ui: EventFilter,

                        extra: false,

                        operators: {

                            string: {

                                eq: "Is equal to",

                                neq: "Is not equal to"

                            }

                        }

                    }

                },

                {field: "category_id",

                    title: "Category Name",

                    editor: CategoryDropDownEditor,

                    template: "#=category_name#",

                    filterable: {

                        ui: CategoryFilter,

                        extra: false,

                        operators: {

                            string: {

                                eq: "Is equal to",

                                neq: "Is not equal to"

                            }

                        }

                    }

                },

               // {field: "is_active", title: "Active?", template: "#= is_active ? 'Yes' : 'No' #", width: "10%"},

                {command: ["edit", "destroy"], title: "Action", width: "180px"}],

            editable: "popup"

        });

    });

</script>				



<script type="text/javascript">

    function EventFilter(element) {

        element.kendoDropDownList({

            autoBind: false,

            dataTextField: "event_title",

            dataValueField: "event_id",

            dataSource: {

                transport: {

                    read: {

                        url: "../../controller/event_controller.php",

                        type: "GET"

                    }

                },

                schema: {

                    data: "data"

                }

            },

            optionLabel: "Select Event"

        });

    }

    function CategoryFilter(element) {

        element.kendoDropDownList({

            autoBind: false,

            dataTextField: "category_name",

            dataValueField: "category_id",

            dataSource: {

                transport: {

                    read: {

                        url: "../../controller/category_controller.php",

                        type: "GET"

                    }

                },

                schema: {

                    data: "data"

                }

            },

            optionLabel: "Select Category"

        });

    }



    function EventDropDownEditor(container, options) {

        jQuery('<input required data-text-field="event_title" data-value-field="event_id" data-bind="value:' + options.field + '"/>')

                .appendTo(container)

                .kendoDropDownList({

                    autoBind: false,

                    dataTextField: "event_title",

                    dataValueField: "event_id",

                    dataSource: {

                        transport: {

                            read: {

                                url: "../../controller/event_controller.php",

                                type: "GET"

                            }

                        },

                        schema: {

                            data: "data"

                        }

                    },

                    optionLabel: "Select Event"

                });

    }



    function CategoryDropDownEditor(container, options) {

        jQuery('<input required data-text-field="category_name" data-value-field="category_id" data-bind="value:' + options.field + '"/>')

                .appendTo(container)

                .kendoDropDownList({

                    autoBind: false,

                    dataTextField: "category_name",

                    dataValueField: "category_id",

                    dataSource: {

                        transport: {

                            read: {

                                url: "../../controller/category_controller.php",

                                type: "GET"

                            }

                        },

                        schema: {

                            data: "data"

                        }

                    },

                    optionLabel: "Select Category"

                });

    }

</script>

<div id="kWindow"></div>


<?php include("../layout/copyright.php"); ?>

