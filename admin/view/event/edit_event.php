<?php
include '../../config/class.web.config.php';
$con = new Config();
$event_id = $_GET["event_id"];
/* Start Authentication */
if ($con->authenticate() == 1) {
    $con->redirect("../../login.php");
}
/* End Authentication */
$err = '';
$msg = '';
$user_id = $_SESSION['user_type_id'];



if (isset($_POST["btnUpdateEvent"])) {

    extract($_POST);
    if (!$is_private OR $is_private != "yes") {
        $is_private = 'no';
    }

    if (!$is_online_payable OR $is_online_payable != "yes") {
        $is_online_payable = 'no';
    }

    if (!$is_home_delivery OR $is_home_delivery != "yes") {
        $is_home_delivery = 'no';
    }

    /*  ------------------ Start Event Logo Image ----------  */

    $targetfolder_logo = '';

    $filename_logo = '';

    $even_logo_image = '';

    if (isset($_FILES['event_logo_image']['name'])) {

        $targetfolder_logo = '../../uploads/event_logo_image/';

        $filename_logo = basename($_FILES['event_logo_image']['name']);

        $targetfolder_logo = $targetfolder_logo . $filename_logo;

        $event_logo_image = substr($targetfolder_logo, 6);
    }

    /*  ------------- End Event Logo Image ------------   */

    /*   Start Event Full Size Image   */

    $targetfolder_full = '';

    $filename_full = '';

    $event_full_size_image = '';

    if (isset($_FILES['event_full_size_image']['name'])) {

        $targetfolder_full = '../../uploads/event_full_size_image/';

        $filename_full = basename($_FILES['event_full_size_image']['name']);

        $targetfolder_full = $targetfolder_full . $filename_full;

        $event_full_size_image = substr($targetfolder_full, 6);
    }

    /*   End Event Full Size Image   */

    $is_active = $_POST["status_id"];

    if (empty($event_title)) {

        $err = "Event Title Required";
    } elseif (empty($city_id)) {

        $err = "Select Event City";
    } else if (empty($event_ticket_expair_date)) {
        $err = "Enter Event Expire Date";
    } else {
        $update_array = array(
            "event_id" => $event_id,
            "event_title" => $event_title,
            "city_id" => $city_id,
            "is_active" => $is_active,
            "event_ticket_expair_date" => $event_ticket_expair_date,
            "event_created_by" => $user_id,
            "is_private" => $is_private,
            "is_online_payable" => $is_online_payable,
            "is_home_delivery" => $is_home_delivery
        );

        if (isset($_FILES['event_logo_image']['name'])) {
            $update_array['event_logo_image'] = $filename_logo;
            move_uploaded_file($_FILES['event_logo_image']['tmp_name'], $targetfolder_logo);
        }

        if (isset($_FILES['event_full_size_image']['name'])) {
            $update_array['event_full_size_image'] = $filename_full;
            move_uploaded_file($_FILES['event_full_size_image']['tmp_name'], $targetfolder_full);
        }

        $result = $con->Update("event", $update_array, "", "", "array");
        //$con->debug($result);
        if ($result["output"] == "error") {
            $err = "ERROR";
        }
    }
}
// ----------- Update General Information End Here ------------//
//------------- Update Event Details Tab Start Here ------------//
if (isset($_POST["btnUpdateEventDetails"])) {

    extract($_POST);

    if (isset($_GET["event_id"])) {

        $event_id = $_GET["event_id"];

        $event_details_description = $_POST["event_details_description"];

        $up_object_array = array(
            "event_id" => $event_id,
            "event_details_description" => htmlentities($event_details_description, ENT_COMPAT, 'UTF-8')
        );

        $update_res = $con->Update("event_details", $up_object_array, "", "", "array");


        if ($update_res["output"] == "success") {

            $con->redirect("index.php");
        } else {

            $con->redirect("index.php");
        }
    }
}
//------------- Update Event Details Tab End Here ------------//
//------------- Update Event Terms And Conditions Tab Start Here ------------//
if (isset($_POST["btnUpdateTermsCondition"])) {

    extract($_POST);

    if (isset($_GET["event_id"])) {

        $event_id = $_GET["event_id"];

        $event_terms_and_conditions_description = $_POST["event_terms_and_conditions_description"];

        $up_object_array = array("event_id" => $event_id,
            "event_terms_and_conditions_description" => htmlentities($event_terms_and_conditions_description, ENT_COMPAT, 'UTF-8'));

        $update_res = $con->Update("event_terms_and_conditions", $up_object_array, "", "", "array");


        if ($update_res["output"] == "success") {

            $con->redirect("index.php");
        } else {

            $con->redirect("index.php");
        }
    }
}

//------------- Update Event Terms And Conditions Tab End Here ------------//
//------------- Update Event FAQ Tab Start Here ------------//
if (isset($_POST["btnUpdateFAQ"])) {
    $con->redirect("index.php");
}
//------------- Update Event FAQ Tab End Here ------------//
//------- Data Display in General Information Start----------------//
$query_for_general_info = "SELECT `event`.*, city.city_id, city.city_name FROM EVENT LEFT JOIN city ON `event`.city_id = city.city_id WHERE `event`.event_id='$event_id'";
$result_general_info = $con->ReturnObjectByQuery($query_for_general_info, "array");
//$con->debug($result_general_info);
$event_title = $result_general_info{0}->event_title;
$event_ticket_expair_date = $result_general_info{0}->event_ticket_expair_date;
$city_id = $result_general_info{0}->city_id;
$event_logo_image = $result_general_info{0}->event_logo_image;
$event_full_size_image = $result_general_info{0}->event_full_size_image;
$is_active = $result_general_info{0}->is_active;
$is_private = $result_general_info{0}->is_private;
$is_online_payable = $result_general_info{0}->is_online_payable;
$is_home_delivery = $result_general_info{0}->is_home_delivery;

//------- Data Display in General Information End----------------//
// ----------Data Display For Event Details ---------------//

$query_for_event_details = "SELECT * FROM event_details WHERE event_id = $event_id";
$result_event_details = $con->ReturnObjectByQuery($query_for_event_details, "array");
$event_details_description = $result_event_details{0}->event_details_description;

// ----------Data Display For Event Details ---------------//
// ----------Data Display For Event Terms And conditions ---------------//

$query_for_event_terms_and_conditions = "SELECT * FROM event_terms_and_conditions WHERE event_id = $event_id";
$result_event_terms_and_conditions = $con->ReturnObjectByQuery($query_for_event_terms_and_conditions, "array");
$event_terms_and_conditions_description = $result_event_terms_and_conditions{0}->event_terms_and_conditions_description;

// ----------Data Display For Event Details ---------------//
// ----------- Update General Information Start Here ------------//
?>
<?php include("../layout/header_script.php"); ?>

<?php include("../layout/menu_top.php"); ?>

<?php include("../layout/breadcrumbs.php"); ?>


<!-- Edit Event Tab Start-->
<div class="row">

    <div class="col-md-12">

<?php echo(!empty($msg) ? $con->notification($msg, '') : $con->notification('', $err)); ?>

    </div>

</div>

<br />
<form method="post">
    <div class="row">
        <div class="col-md-12">
            <div id="kendotab_edit">
                <ul>

                    <li class="k-state-active">
                        General Info
                    </li>
                    <li>
                        Details
                    </li>
                    <li>
                        Terms & Conditions
                    </li>
                    <li>
                        Category
                    </li>
                    <li>
                        Venue
                    </li>
                    <li>
                        Schedule
                    </li>
                    <li>
                        Ticket Type
                    </li>
                    <li>
                        Includes
                    </li>
                    <li>
                        Attraction
                    </li>
                    <li>
                        Photo Gallery      
                    </li>
                    <li>
                        Video Gallery
                    </li>
                    <li>
                        Activity
                    </li>
                    <li>
                        FAQ
                    </li>
                </ul>

                <!-- Event General Information Tab Update Start Here -->
                <div>
                    <div class="row">
                        <div style="height: 15px;"></div>
                        <div class="col-md-5">
                            <div class="col-md-3">Event Title:</div>
                            <div class="col-md-7"><?php echo $con->TextBox("event_title", "event_title", "", "", "", $event_title, "kendo"); ?></div>
                        </div>
                        <div class="col-md-5">
                            <div class="col-md-3">City Name :</div>
                            <div class="col-md-7"><?php echo $con->SelectBox("city_id", "city_id", "", "city_id", "city_name", $city_id, "", "city_controller.php", "kendo", "width:100%", ""); ?></div>
                        </div>
                    </div>
                    <div class="row">
                        <div style="height: 15px;"></div>
                        <div class="col-md-5">
                            <div class="col-md-3">Event Logo Image:</div>
                            <img style="margin-left: 16px;" src="../../uploads/event_logo_image/<?php echo $event_logo_image; ?>" height="70px" width="90px" />
                            <div class="col-md-7" style="margin-left: 152px;"><?php echo $con->FileUpload("event_logo_image", "event_logo_image", "", "", "", ""); ?></div>
                        </div>
                        <div class="col-md-5">
                            <div class="col-md-3">Event Full Size Image:</div>
                            <img style="margin-left: 16px;" src="../../uploads/event_full_size_image/<?php echo $event_full_size_image; ?>" height="70px" width="90px" />
                            <div class="col-md-7" style="margin-left: 152px;"><?php echo $con->FileUpload("event_full_size_image", "event_full_size_image", "", "", "", ""); ?></div>
                        </div>
                    </div>
                    <div style="height: 15px;"></div>
                    <div class="row">
                        <div class="col-md-5">
                            <div class="col-md-3">Expire Date :</div>
                            <div class="col-md-7"><input style="width: 100% !important;" id="event_ticket_expair_date" name="event_ticket_expair_date" value="<?php echo $event_ticket_expair_date; ?>" /></div> 
                        </div>
                        <script>
                            $(document).ready(function () {
                                $("#event_ticket_expair_date").kendoDatePicker();
                            });</script>
                        <div class="col-md-5">
                            <div class="col-md-3">Event Status:</div>
                            <div class="col-md-7"><?php echo $con->SelectBox("status_id", "status_id", "", "status_id", "status_name", $is_active, "", "status_controller.php", "kendo", "width:100%", ""); ?></div>
                        </div>
                    </div>

                    <div style="height: 15px;"></div>
                    <div class="row">
                        <div class="col-md-5">
                            <div class="col-md-3">Is Private</div>
                            <div class="col-md-7"><input type="checkbox" name="is_private" value="yes" <?php
if ($is_private == 'yes') {
    echo 'checked="checked"';
}
?>/></div> 
                        </div>

                    </div>
                    <div style="height: 15px;"></div>
                    <div class="row">
                        <div class="col-md-5">
                            <div class="col-md-3">Is Online Payable</div>
                            <div class="col-md-7"><input type="checkbox" name="is_online_payable" value="yes" <?php
if ($is_online_payable == 'yes') {
    echo 'checked="checked"';
}
?>/></div> 
                        </div>

                    </div>
                    <div style="height: 15px;"></div>
                    <div class="row">
                        <div class="col-md-5">
                            <div class="col-md-3">Is Home Delivery</div>
                            <div class="col-md-7"><input type="checkbox" name="is_home_delivery" value="yes" <?php
if ($is_home_delivery == 'yes') {
    echo 'checked="checked"';
}
?>/></div> 
                        </div>

                    </div>


                    <div style="height: 15px;"></div>
                    <div class="row">
                        <div class="col-md-5">
                            <input style="margin-left: 15px;" type="submit" name="btnUpdateEvent" class="btn btn-success" value="Update"  />
                        </div>
                    </div>
                </div>

                <!-- Event General Information Update Tab End Here -->
                <!-- Event Details Update Tab Start Here  -->
                <div>

                    <div class="row">

                        <div style="height: 15px;"></div>

                        <div class="col-md-5">Event Details</div>

                    </div>



                    <div class="row">

                        <div class="col-md-11">

                            <textarea id="event_details_description" name="event_details_description" rows="3" cols="30"><?php echo html_entity_decode($event_details_description, ENT_QUOTES | ENT_IGNORE, "UTF-8"); ?></textarea>

                            <script>

                                $(document).ready(function () {

                                    $("#event_details_description").kendoEditor({
                                    });
                                });</script>

                        </div>

                    </div>

                    <div class="row">
                        <br/>

                        <div class="col-md-11">
                            <input type="submit" name="btnUpdateEventDetails" class="btn btn-success" value="Update Details"  />
                        </div>
                    </div>
                </div>
                <!-- Event Details Tab End Here -->

                <!-- Event Terms And Condition Tab Start Here -->
                <div>
                    <div class="row">
                        <div style="height: 20px"></div>
                        <div class="col-md-5">Terms & Condition</div>
                        <div style="height: 20px"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-11">
                            <textarea id="event_terms_and_conditions_description" name="event_terms_and_conditions_description" rows="3" cols="30"><?php echo html_entity_decode($event_terms_and_conditions_description, ENT_QUOTES | ENT_IGNORE, "UTF-8"); ?></textarea>
                            <script>
                                $(document).ready(function () {

                                    $("#event_terms_and_conditions_description").kendoEditor({
                                        tools: [
                                            "bold",
                                            "italic",
                                            "underline",
                                            "strikethrough",
                                            "justifyLeft",
                                            "justifyCenter",
                                            "justifyRight",
                                            "justifyFull",
                                            "insertUnorderedList",
                                            "insertOrderedList",
                                            "indent",
                                            "outdent",
                                            "createLink",
                                            "unlink",
                                            "insertImage",
                                            "insertFile",
                                            "subscript",
                                            "superscript",
                                            "createTable",
                                            "addRowAbove",
                                            "addRowBelow",
                                            "addColumnLeft",
                                            "addColumnRight",
                                            "deleteRow",
                                            "deleteColumn",
                                            "viewHtml",
                                            "formatting",
                                            "cleanFormatting",
                                            "fontName",
                                            "fontSize",
                                            "foreColor",
                                            "backColor"
                                        ]
                                    });
                                });</script>
                        </div>
                    </div>
                    <div class="row">
                        <div style="height: 15px;"></div>
                        <div class="col-md-11">
                            <input type="submit" name="btnUpdateTermsCondition" value="Update Terms And Conditions" class="btn btn-success" />
                        </div>
                    </div>
                </div>

                <!-- Event Terms And Condition Tab End Here -->
                <!-- Category Tab Start Here -->
                <div>

                    <div id="grid"></div>
                    <script type="text/javascript">
                        jQuery(document).ready(function () {
                            var dataSource = new kendo.data.DataSource({
                                pageSize: 10,
                                transport: {
                                    read: {
                                        url: "../../controller/event_category_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "GET"

                                    },
                                    update: {
                                        url: "../../controller/event_category_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "POST",
                                        complete: function (e) {

                                            jQuery("#grid").data("kendoGrid").dataSource.read();
                                        }

                                    },
                                    destroy: {
                                        url: "../../controller/event_category_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "DELETE"

                                    },
                                    create: {
                                        url: "../../controller/event_category_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "PUT",
                                        complete: function (e) {

                                            jQuery("#grid").data("kendoGrid").dataSource.read();
                                        }

                                    }

                                },
                                autoSync: false,
                                schema: {
                                    errors: function (e) {

                                        if (e.error === "yes")

                                        {

                                            var message = "";
                                            message += e.message;
                                            var window = jQuery("#kWindow");
                                            if (!window.data("kendoWindow")) {

                                                window.kendoWindow({
                                                    title: "Error window",
                                                    modal: true,
                                                    height: 100,
                                                    width: 400

                                                });
                                            }

                                            window.data("kendoWindow").center().open();
                                            window.html('<div style="margin-top:10px;text-align:center;"><span style="color:red;font-size:14px;">' + message + '</span></div>');
                                            this.cancelChanges();
                                        }

                                    },
                                    data: "data",
                                    total: "data.length",
                                    model: {
                                        id: "event_category_id",
                                        fields: {
                                            event_category_id: {editable: false, nullable: true},
                                            category_id: {type: "number"},
                                            category_name: {type: "string", validation: {required: true}},
                                            is_active: {type: "boolean"}
                                        }

                                    }

                                }

                            });
                            jQuery("#grid").kendoGrid({
                                dataSource: dataSource,
                                filterable: true,
                                pageable: {
                                    refresh: true,
                                    input: true,
                                    numeric: false,
                                    pageSizes: [10, 20, 50]

                                },
                                sortable: true,
                                groupable: true,
                                resizable: true,
                                toolbar: [{name: "create", text: "Add Category"}],
                                columns: [
                                    {field: "category_id",
                                        title: "Category Name",
                                        editor: CategoryDropDownEditor,
                                        template: "#=category_name#",
                                        filterable: {
                                            ui: CategoryFilter,
                                            extra: false,
                                            operators: {
                                                string: {
                                                    eq: "Is equal to",
                                                    neq: "Is not equal to"

                                                }

                                            }

                                        }

                                    },
                                    {field: "is_active", title: "Active?", template: "#= is_active ? 'Yes' : 'No' #", width: "10%"},
                                    {command: ["edit", "destroy"], title: "Action", width: "230px"}],
                                editable: "inline"

                            });
                        });</script>		
                    <script type="text/javascript">
                        function CategoryFilter(element) {

                            element.kendoDropDownList({
                                autoBind: false,
                                dataTextField: "category_name",
                                dataValueField: "category_id",
                                dataSource: {
                                    transport: {
                                        read: {
                                            url: "../../controller/category_controller.php",
                                            type: "GET"

                                        }

                                    },
                                    schema: {
                                        data: "data"

                                    }

                                },
                                optionLabel: "Select Category"

                            });
                        }

                        function CategoryDropDownEditor(container, options) {

                            jQuery('<input required data-text-field="category_name" data-value-field="category_id" data-bind="value:' + options.field + '"/>')

                                    .appendTo(container)

                                    .kendoDropDownList({
                                        autoBind: false,
                                        dataTextField: "category_name",
                                        dataValueField: "category_id",
                                        dataSource: {
                                            transport: {
                                                read: {
                                                    url: "../../controller/category_controller.php",
                                                    type: "GET"

                                                }

                                            },
                                            schema: {
                                                data: "data"

                                            }

                                        },
                                        optionLabel: "Select Category"

                                    });
                        }
                    </script>
                    <div id="kWindow"></div>

                    <div class="clearfix"></div>

                </div>
                <!--Event Category Tab End Here -->


                <!-- Event Venue Tab Start Here -->

                <div>

                    <div id="venue"></div>
                    <script type="text/javascript">
                        jQuery(document).ready(function () {
                            var dataSource = new kendo.data.DataSource({
                                pageSize: 10,
                                transport: {
                                    read: {
                                        url: "../../controller/event_venue_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "GET"

                                    },
                                    update: {
                                        url: "../../controller/event_venue_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "POST",
                                        complete: function (e) {

                                            jQuery("#venue").data("kendoGrid").dataSource.read();
                                        }

                                    },
                                    destroy: {
                                        url: "../../controller/event_venue_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "DELETE"

                                    },
                                    create: {
                                        url: "../../controller/event_venue_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "PUT",
                                        complete: function (e) {

                                            jQuery("#venue").data("kendoGrid").dataSource.read();
                                        }

                                    }

                                },
                                autoSync: false,
                                schema: {
                                    errors: function (e) {

                                        if (e.error === "yes")

                                        {

                                            var message = "";
                                            message += e.message;
                                            var window = jQuery("#kWindow");
                                            if (!window.data("kendoWindow")) {

                                                window.kendoWindow({
                                                    title: "Error window",
                                                    modal: true,
                                                    height: 100,
                                                    width: 400

                                                });
                                            }

                                            window.data("kendoWindow").center().open();
                                            window.html('<div style="margin-top:10px;text-align:center;"><span style="color:red;font-size:14px;">' + message + '</span></div>');
                                            this.cancelChanges();
                                        }

                                    },
                                    data: "data",
                                    total: "data.length",
                                    model: {
                                        id: "event_venue_id",
                                        fields: {
                                            event_venue_id: {editable: false, nullable: true},
                                            venue_id: {type: "number"},
                                            venue_name: {type: "string", validation: {required: true}},
                                            is_active: {type: "boolean"}
                                        }

                                    }

                                }

                            });
                            jQuery("#venue").kendoGrid({
                                dataSource: dataSource,
                                filterable: true,
                                pageable: {
                                    refresh: true,
                                    input: true,
                                    numeric: false,
                                    pageSizes: [10, 20, 50]

                                },
                                sortable: true,
                                groupable: true,
                                resizable: true,
                                toolbar: [{name: "create", text: "Add Venue"}],
                                columns: [
                                    {field: "venue_id",
                                        title: "Venue Name",
                                        editor: VenueDropDownEditor,
                                        template: "#=venue_name#",
                                        filterable: {
                                            ui: VenueFilter,
                                            extra: false,
                                            operators: {
                                                string: {
                                                    eq: "Is equal to",
                                                    neq: "Is not equal to"

                                                }

                                            }

                                        }

                                    },
                                    {field: "is_active", title: "Active?", template: "#= is_active ? 'Yes' : 'No' #", width: "10%"},
                                    {command: ["edit", "destroy"], title: "Action", width: "230px"}],
                                editable: "inline"

                            });
                        });</script>		
                    <script type="text/javascript">
                        function VenueFilter(element) {

                            element.kendoDropDownList({
                                autoBind: false,
                                dataTextField: "venue_name",
                                dataValueField: "venue_id",
                                dataSource: {
                                    transport: {
                                        read: {
                                            url: "../../controller/venue_list_controller.php",
                                            type: "GET"

                                        }

                                    },
                                    schema: {
                                        data: "data"

                                    }

                                },
                                optionLabel: "Select Venue"

                            });
                        }

                        function VenueDropDownEditor(container, options) {

                            jQuery('<input required data-text-field="venue_name" data-value-field="venue_id" data-bind="value:' + options.field + '"/>')

                                    .appendTo(container)

                                    .kendoDropDownList({
                                        autoBind: false,
                                        dataTextField: "venue_name",
                                        dataValueField: "venue_id",
                                        dataSource: {
                                            transport: {
                                                read: {
                                                    url: "../../controller/venue_list_controller.php",
                                                    type: "GET"

                                                }

                                            },
                                            schema: {
                                                data: "data"

                                            }

                                        },
                                        optionLabel: "Select Venue"

                                    });
                        }
                    </script>
                    <div id="kWindow"></div>

                    <div class="clearfix"></div>

                </div>
                <!-- Event Venue Tab End Here -->

                <!-- Event Schedule Tab Start Here -->

                <div>
                    <div id="schedule"></div>

                    <script type="text/javascript">

                        jQuery(document).ready(function () {

                            var dataSource = new kendo.data.DataSource({
                                pageSize: 10,
                                transport: {
                                    read: {
                                        url: "../../controller/schedule_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "GET"

                                    },
                                    update: {
                                        url: "../../controller/schedule_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "POST",
                                        complete: function (e) {

                                            jQuery("#schedule").data("kendoGrid").dataSource.read();
                                            jQuery(".k-pager-refresh").trigger('click');
                                        }

                                    },
                                    destroy: {
                                        url: "../../controller/schedule_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "DELETE"

                                    },
                                    create: {
                                        url: "../../controller/schedule_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "PUT",
                                        complete: function (e) {

                                            jQuery("#schedule").data("kendoGrid").dataSource.read();
                                            jQuery(".k-pager-refresh").trigger('click');
                                        }

                                    },
                                },
                                autoSync: false,
                                schema: {
                                    errors: function (e) {

                                        if (e.error === "yes")

                                        {

                                            var message = "";
                                            message += e.message;
                                            var window = jQuery("#kWindowSchedule");
                                            if (!window.data("kendoWindow")) {

                                                window.kendoWindow({
                                                    title: "",
                                                    modal: true,
                                                    height: 120,
                                                    width: 400

                                                });
                                            }

                                            window.data("kendoWindow").center().open();
                                            window.html('<br/><br/><center><P style="color:red">' + message + '</p></center>');
                                            this.cancelChanges();
                                        }

                                    },
                                    data: "data",
                                    total: "data.length",
                                    model: {
                                        id: "event_schedule_id",
                                        fields: {
                                            event_schdule_id: {editable: false, nullable: true},
                                            venue_id: {type: "number"},
                                            venue_name: {type: "string", validation: {required: true}},
                                            event_date: {editable: true, nullable: true},
                                            event_schedule_start_time: {type: "string", validation: true},
                                            event_schedule_end_time: {type: "string", validation: true},
                                            event_id: {type: "string"},
                                            is_active: {type: "boolean"}

                                        }
                                    }
                                }

                            });
                            jQuery("#schedule").kendoGrid({
                                dataSource: dataSource,
                                filterable: true,
                                pageable: {
                                    refresh: true,
                                    input: true,
                                    numeric: false,
                                    pageSizes: [10, 20, 50]

                                },
                                sortable: true,
                                groupable: true,
                                toolbar: [{name: "create", text: "Add Schedule"}],
                                columns: [
                                    {field: "venue_id",
                                        title: "Venue Name",
                                        editor: ScheduleVenueDropDownEditor,
                                        template: "#=venue_name#",
                                        filterable: {
                                            ui: ScheduleVenueFilter,
                                            extra: false,
                                            operators: {
                                                string: {
                                                    eq: "Is equal to",
                                                    neq: "Is not equal to"

                                                }

                                            }

                                        }

                                    },
                                    {field: "event_date", title: "Date - Time", editor: dateTimeEditor, format: "{0:yyyy-MM-dd}"},
                                    {field: "event_schedule_start_time", title: "Start - Time", editor: timeEditor, format: "{0:hh:mm tt}"},
                                    {field: "event_schedule_end_time", title: "End - Time", editor: timeEditor, format: "{0:hh:mm tt}"},
                                    {field: "is_active", title: "Active?", template: "#= is_active ? 'Yes' : 'No' #", width: "10%"},
                                    {command: ["edit", "destroy"], title: "Action", width: "230px"}],
                                editable: "inline"

                            });
                        });</script> 

                    <script type="text/javascript">

                        function ScheduleVenueFilter(element) {

                            element.kendoDropDownList({
                                autoBind: false,
                                dataTextField: "venue_name",
                                dataValueField: "venue_id",
                                dataSource: {
                                    transport: {
                                        read: {
                                            url: "../../controller/schedule_venue_controller.php?event_id=<?php echo $event_id; ?>",
                                            type: "GET"

                                        }

                                    },
                                    schema: {
                                        data: "data"

                                    }

                                },
                                optionLabel: "Select Venue"

                            });
                        }

                        function ScheduleVenueDropDownEditor(container, options) {

                            jQuery('<input required data-text-field="venue_name" data-value-field="venue_id" data-bind="value:' + options.field + '"/>')

                                    .appendTo(container)

                                    .kendoDropDownList({
                                        autoBind: false,
                                        dataTextField: "venue_name",
                                        dataValueField: "venue_id",
                                        dataSource: {
                                            transport: {
                                                read: {
                                                    url: "../../controller/schedule_venue_controller.php?event_id=<?php echo $event_id; ?>",
                                                    type: "GET"

                                                }

                                            },
                                            schema: {
                                                data: "data"

                                            }

                                        },
                                        optionLabel: "Select Venue"

                                    });
                        }

                        function timeEditor(container, options) {

                            $('<input data-text-field="' + options.field + '" data-value-field="' + options.field + '" data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')

                                    .appendTo(container)

                                    .kendoTimePicker({
                                        format: "hh:mm tt"

                                    });
                        }



                        function dateTimeEditor(container, options) {

                            $('<input data-text-field="' + options.field + '" data-value-field="' + options.field + '" data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')

                                    .appendTo(container)

                                    .kendoDatePicker({
                                        format: "yyyy-MM-dd"

                                    });
                        }
                    </script>

                    <div id="kWindowSchedule"></div>
                    <div class="clearfix"></div>

                </div>
                <!-- Event Schedule Tab End Here -->
                <!-- Event Ticket Type Tab Start Here -->

                <div>
                    <div id="event_ticket_type"></div>
                    <script type="text/javascript">
                        jQuery(document).ready(function () {

                            var dataSource = new kendo.data.DataSource({
                                pageSize: 10,
                                transport: {
                                    read: {
                                        url: "../../controller/event_ticket_type_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "GET"

                                    },
                                    update: {
                                        url: "../../controller/event_ticket_type_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "POST",
                                        complete: function (e) {

                                            jQuery("#event_ticket_type").data("kendoGrid").dataSource.read();
                                        }

                                    },
                                    destroy: {
                                        url: "../../controller/event_ticket_type_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "DELETE"

                                    },
                                    create: {
                                        url: "../../controller/event_ticket_type_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "PUT",
                                        complete: function (e) {

                                            jQuery("#event_ticket_type").data("kendoGrid").dataSource.read();
                                        }

                                    }

                                },
                                autoSync: false,
                                schema: {
                                    errors: function (e) {

                                        if (e.error === "yes")

                                        {

                                            var message = "";
                                            message += e.message;
                                            var window = jQuery("#kWindow");
                                            if (!window.data("kendoWindow")) {

                                                window.kendoWindow({
                                                    title: "Error window",
                                                    modal: true,
                                                    height: 100,
                                                    width: 400

                                                });
                                            }

                                            window.data("kendoWindow").center().open();
                                            window.html('<div style="margin-top:10px;text-align:center;"><span style="color:red;font-size:14px;">' + message + '</span></div>');
                                            this.cancelChanges();
                                        }

                                    },
                                    data: "data",
                                    total: "data.length",
                                    model: {
                                        id: "event_ticket_type_id",
                                        fields: {
                                            event_ticket_type_id: {editable: false, nullable: true},
                                            event_id: {type: "number"},
                                            event_schedule_id: {type: "number"},
                                            event_schedule_name: {type: "string"},
                                            ticket_name: {type: "string", validation: {required: true}},
                                            ticket_qty: {type: "number", validation: {required: true, min: 1}},
                                            ticket_price: {type: "number", validation: {required: true, min: 1}},
                                            per_user_limit: {type: "number", validation: {required: true, min: 1}},
                                            e_ticket: {type: "number", validation: {min: -1}},
                                            p_ticket: {type: "number", validation: {min: -1}},
                                            is_active: {type: "boolean"}
                                        }
                                    }

                                }

                            });
                            jQuery("#event_ticket_type").kendoGrid({
                                dataSource: dataSource,
                                filterable: true,
                                pageable: {
                                    refresh: true,
                                    input: true,
                                    numeric: false,
                                    pageSizes: [10, 20, 50]

                                },
                                sortable: true,
                                groupable: true,
                                resizable: true,
                                toolbar: [{name: "create", text: "Add Ticket Type"}],
                                columns: [
                                    {field: "event_schedule_id",
                                        title: "Event Schedule",
                                        width: "220px",
                                        editor: EventScheduleTicketTypeDropDownEditor,
                                        template: "#=event_schedule_name#",
                                        filterable: {
                                            ui: EventScheduleTicketTypeFilter,
                                            extra: false,
                                            operators: {
                                                string: {
                                                    eq: "Is equal to",
                                                    neq: "Is not equal to"

                                                }

                                            }

                                        }

                                    },
                                    {field: "ticket_name", title: "Ticket Name", width: "150px"},
                                    {field: "ticket_qty", title: "Ticket QTY", width: "100px"},
                                    {field: "ticket_price", title: "Ticket Price", width: "100px"},
                                    {field: "per_user_limit", title: "Per User Limit", width: "120px"},
                                    {field: "e_ticket", title: "E-Ticket", width: "100px"},
                                    {field: "p_ticket", title: "P-Ticket", width: "100px"},
                                    {field: "is_active", title: "Active?", template: "#= is_active ? 'Yes' : 'No' #", width: "80px"},
                                    {command: ["edit", "destroy"], title: "Action", width: "200px"}],
                                editable: "inline"

                            });
                        });</script>

                    <script type="text/javascript">

                        function EventScheduleTicketTypeFilter(element) {

                            element.kendoDropDownList({
                                autoBind: false,
                                dataTextField: "event_schedule_name",
                                dataValueField: "event_schedule_id",
                                dataSource: {
                                    transport: {
                                        read: {
                                            url: "../../controller/ticket_type_schedule_controller.php?event_id=<?php echo $event_id; ?>",
                                            type: "GET"

                                        }

                                    },
                                    schema: {
                                        data: "data"

                                    }

                                },
                                optionLabel: "Select Schedule"

                            });
                        }

                        function EventScheduleTicketTypeDropDownEditor(container, options) {

                            jQuery('<input required data-text-field="event_schedule_name" data-value-field="event_schedule_id" data-bind="value:' + options.field + '"/>')

                                    .appendTo(container)

                                    .kendoDropDownList({
                                        autoBind: false,
                                        dataTextField: "event_schedule_name",
                                        dataValueField: "event_schedule_id",
                                        dataSource: {
                                            transport: {
                                                read: {
                                                    url: "../../controller/ticket_type_schedule_controller.php?event_id=<?php echo $event_id; ?>",
                                                    type: "GET"

                                                }

                                            },
                                            schema: {
                                                data: "data"

                                            }

                                        },
                                        optionLabel: "Select Schedule"

                                    });
                        }
                    </script>

                    <div id="kWindow"></div>

                    <div class="clearfix"></div>

                </div>
                <!-- Event Ticket Type Tab End Here -->

                <!-- Event Includes Tab Start Here -->
                <div>
                    <div id="event_includes"></div>
                    <script type="text/javascript">
                        jQuery(document).ready(function () {

                            var dataSource = new kendo.data.DataSource({
                                pageSize: 10,
                                transport: {
                                    read: {
                                        url: "../../controller/event_includes_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "GET"

                                    },
                                    update: {
                                        url: "../../controller/event_includes_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "POST",
                                        complete: function (e) {

                                            jQuery("#event_includes").data("kendoGrid").dataSource.read();
                                        }

                                    },
                                    destroy: {
                                        url: "../../controller/event_includes_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "DELETE"

                                    },
                                    create: {
                                        url: "../../controller/event_includes_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "PUT",
                                        complete: function (e) {

                                            jQuery("#event_includes").data("kendoGrid").dataSource.read();
                                        }

                                    }

                                },
                                autoSync: false,
                                schema: {
                                    errors: function (e) {

                                        if (e.error === "yes")

                                        {

                                            var message = "";
                                            message += e.message;
                                            var window = jQuery("#kWindow");
                                            if (!window.data("kendoWindow")) {

                                                window.kendoWindow({
                                                    title: "Error window",
                                                    modal: true,
                                                    height: 100,
                                                    width: 400

                                                });
                                            }

                                            window.data("kendoWindow").center().open();
                                            window.html('<div style="margin-top:10px;text-align:center;"><span style="color:red;font-size:14px;">' + message + '</span></div>');
                                            this.cancelChanges();
                                        }

                                    },
                                    data: "data",
                                    total: "data.length",
                                    model: {
                                        id: "event_includes_id",
                                        fields: {
                                            event_includes_id: {editable: false, nullable: true},
                                            event_id: {type: "number"},
                                            event_schedule_id: {type: "number"},
                                            event_schedule_name: {type: "string"},
                                            event_includes_name: {type: "string"},
                                            event_includes_price: {type: "number", validation: {required: true, min: 1}},
                                            event_includes_per_user_limit: {type: "number", validation: {required: true, min: 1}},
                                            total_qty: {type: "number", validation: {required: true, min: 1}},
                                            is_active: {type: "boolean"}
                                        }
                                    }

                                }

                            });
                            jQuery("#event_includes").kendoGrid({
                                dataSource: dataSource,
                                filterable: true,
                                pageable: {
                                    refresh: true,
                                    input: true,
                                    numeric: false,
                                    pageSizes: [10, 20, 50]

                                },
                                sortable: true,
                                groupable: true,
                                resizable: true,
                                toolbar: [{name: "create", text: "Add Event Includes"}],
                                columns: [
                                    {field: "event_schedule_id",
                                        title: "Event Schedule",
                                        width: "220px",
                                        editor: EventIncludesScheduleDropDownEditor,
                                        template: "#=event_schedule_name#",
                                        filterable: {
                                            ui: EventIncludesScheduleFilter,
                                            extra: false,
                                            operators: {
                                                string: {
                                                    eq: "Is equal to",
                                                    neq: "Is not equal to"

                                                }

                                            }

                                        }

                                    },
                                    {field: "event_includes_name", title: "Includes Name", width: "150px"},
                                    {field: "event_includes_price", title: "Price", width: "100px"},
                                    {field: "event_includes_per_user_limit", title: "Per User Limit", width: "120px"},
                                    {field: "total_qty", title: "Total QTY", width: "120px"},
                                    {field: "is_active", title: "Active?", template: "#= is_active ? 'Yes' : 'No' #", width: "80px"},
                                    {command: ["edit", "destroy"], title: "Action", width: "200px"}],
                                editable: "inline"

                            });
                        });</script>

                    <script type="text/javascript">

                        function EventIncludesScheduleFilter(element) {

                            element.kendoDropDownList({
                                autoBind: false,
                                dataTextField: "event_schedule_name",
                                dataValueField: "event_schedule_id",
                                dataSource: {
                                    transport: {
                                        read: {
                                            url: "../../controller/ticket_type_schedule_controller.php?event_id=<?php echo $event_id; ?>",
                                            type: "GET"

                                        }

                                    },
                                    schema: {
                                        data: "data"

                                    }

                                },
                                optionLabel: "Select Schedule"

                            });
                        }

                        function EventIncludesScheduleDropDownEditor(container, options) {

                            jQuery('<input required data-text-field="event_schedule_name" data-value-field="event_schedule_id" data-bind="value:' + options.field + '"/>')

                                    .appendTo(container)

                                    .kendoDropDownList({
                                        autoBind: false,
                                        dataTextField: "event_schedule_name",
                                        dataValueField: "event_schedule_id",
                                        dataSource: {
                                            transport: {
                                                read: {
                                                    url: "../../controller/ticket_type_schedule_controller.php?event_id=<?php echo $event_id; ?>",
                                                    type: "GET"

                                                }

                                            },
                                            schema: {
                                                data: "data"

                                            }

                                        },
                                        optionLabel: "Select Schedule"

                                    });
                        }
                    </script>

                    <div id="kWindow"></div>

                    <div class="clearfix"></div>

                </div>

                <!-- Event Includes Tab End Here -->


                <!-- Event Key Attraction Tab Start Here -->
                <div>
                    <div id="attraction"></div>
                    <script type="text/javascript">


                        jQuery(document).ready(function () {
                            var dataSource = new kendo.data.DataSource({
                                pageSize: 10,
                                transport: {
                                    read: {
                                        url: "../../controller/event_key_attraction_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "GET"
                                    },
                                    update: {
                                        url: "../../controller/event_key_attraction_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "POST",
                                        complete: function (e) {
                                            jQuery("#attraction").data("kendoGrid").dataSource.read();
                                        }
                                    },
                                    destroy: {
                                        url: "../../controller/event_key_attraction_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "DELETE"
                                    },
                                    create: {
                                        url: "../../controller/event_key_attraction_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "PUT",
                                        complete: function (e) {
                                            jQuery("#attraction").data("kendoGrid").dataSource.read();
                                        }
                                    },
                                },
                                autoSync: false,
                                schema: {
                                    data: "data",
                                    total: "data.length",
                                    model: {
                                        id: "event_key_attraction_id",
                                        fields: {
                                            event_key_attraction_id: {type: "string"},
                                            attraction_name: {type: "string"},
                                            details: {type: "string"},
                                            image: {type: "string"},
                                            facebook: {type: "string"},
                                            twitter: {type: "string"},
                                            google_plus: {type: "string"},
                                            is_active: {type: "boolean"}


                                        }
                                    }
                                }
                            });
                            jQuery("#attraction").kendoGrid({
                                dataSource: dataSource,
                                filterable: true,
                                pageable: {
                                    refresh: true,
                                    input: true,
                                    numeric: false,
                                    pageSizes: true,
                                    pageSizes: [10, 20, 50],
                                },
                                sortable: true,
                                groupable: true,
                                toolbar: [{name: "create", text: "Add Key Attraction"}],
                                columns: [
                                    {field: "attraction_name", title: "Name", width: "150px"},
                                    {field: "details",
                                        title: "Attraction Details", width: "400px",
                                        editor: textAreaEditor
                                    },
                                    {field: "image",
                                        title: "Attraction Image", width: "160px",
                                        editor: fileEditor,
                                        template: "<img src='<?php echo $con->baseUrl("uploads/event_key_attraction/") ?>#=image#'   height='100' width='140'/>"
                                    },
                                    {field: "facebook", title: "Facebook", width: "150px"},
                                    {field: "twitter", title: "Twitter", width: "150px"},
                                    {field: "google_plus", title: "Google Plus", width: "150px"},
                                    {field: "is_active", title: "Active?", template: "#= is_active ? 'Yes' : 'No' #", width: "80px"},
                                    {command: ["edit", "destroy"], title: "Action", width: "200px"}],
                                editable: "inline"
                            });
                        });
                        function fileEditor(container, options) {
                            $('<input type="file" id="image" name="image" data-role="upload" />')
                                    .appendTo(container)
                                    .kendoUpload({
                                        multiple: true,
                                        async: {
                                            saveUrl: "../../controller/event_key_attraction_save.php",
                                            autoUpload: true
                                        },
                                        upload: function (e) {
                                            e.data = {event_key_attraction_id: options.model.event_key_attraction_id};
                                        },
                                        success: function (e) {
                                            console.log(e.response);
                                            options.model.set("image", e.response.image);
                                        }
                                    });
                        }

                        function textAreaEditor(container, options) {
                            $('<textarea id="details" name="details" rows="3" cols="30">' + options.field + '</textarea>')
                                    .appendTo(container)
                                    .kendoEditor({
                                        tools: [
                                            "bold",
                                            "italic",
                                            "underline",
                                            "strikethrough",
                                            "justifyLeft",
                                            "justifyCenter",
                                            "justifyRight",
                                            "justifyFull",
                                            "insertUnorderedList",
                                            "insertOrderedList",
                                            "indent",
                                            "outdent",
                                            "createLink",
                                            "unlink",
                                            "insertImage",
                                            "insertFile",
                                            "subscript",
                                            "superscript",
                                            "createTable",
                                            "addRowAbove",
                                            "addRowBelow",
                                            "addColumnLeft",
                                            "addColumnRight",
                                            "deleteRow",
                                            "deleteColumn",
                                            "viewHtml",
                                            "formatting",
                                            "cleanFormatting",
                                            "fontName",
                                            "fontSize",
                                            "foreColor",
                                            "backColor"
                                        ]
                                    });
                        }

                    </script>

                    <div id="kWindow"></div>
                    <div class="clearfix"></div>

                </div>
                <!-- Event Attraction Tab End Here -->

                <!-- Event Photo Gallery Tab Start Here -->
                <div>
                    <div id="photo"></div>
                    <script type="text/javascript">


                        jQuery(document).ready(function () {
                            var dataSource = new kendo.data.DataSource({
                                pageSize: 10,
                                transport: {
                                    read: {
                                        url: "../../controller/event_photo_gallery_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "GET"
                                    },
                                    update: {
                                        url: "../../controller/event_photo_gallery_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "POST",
                                        complete: function (e) {
                                            jQuery("#photo").data("kendoGrid").dataSource.read();
                                        }
                                    },
                                    destroy: {
                                        url: "../../controller/event_photo_gallery_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "DELETE"
                                    },
                                    create: {
                                        url: "../../controller/event_photo_gallery_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "PUT",
                                        complete: function (e) {
                                            jQuery("#photo").data("kendoGrid").dataSource.read();
                                        }
                                    },
                                },
                                autoSync: false,
                                schema: {
                                    data: "data",
                                    total: "data.length",
                                    model: {
                                        id: "photo_gallery_id",
                                        fields: {
                                            photo_gallery_id: {type: "string"},
                                            photo_name: {type: "string"},
                                            is_active: {type: "boolean"}


                                        }
                                    }
                                }
                            });
                            jQuery("#photo").kendoGrid({
                                dataSource: dataSource,
                                filterable: true,
                                pageable: {
                                    refresh: true,
                                    input: true,
                                    numeric: false,
                                    pageSizes: true,
                                    pageSizes: [10, 20, 50],
                                },
                                sortable: true,
                                groupable: true,
                                toolbar: [{name: "create", text: "Add Photo"}],
                                columns: [
                                    {field: "photo_name",
                                        title: "Photo Name", width: "170px",
                                        editor: photoEditor,
                                        template: "<img src='<?php echo $con->baseUrl("uploads/event_photo_gallery/") ?>#=photo_name#'   height='100' width='200'/>"
                                    },
                                    {field: "is_active", title: "Active?", template: "#= is_active ? 'Yes' : 'No' #", width: "8%"},
                                    {command: ["edit", "destroy"], title: "Action", width: "20%"}],
                                editable: "inline"
                            });
                        });
                        function photoEditor(container, options) {
                            $('<input type="file" id="photo_name" name="photo_name" data-role="upload" />')
                                    .appendTo(container)
                                    .kendoUpload({
                                        multiple: true,
                                        async: {
                                            saveUrl: "../../controller/event_photo_gallery_save.php",
                                            autoUpload: true
                                        },
                                        upload: function (e) {
                                            e.data = {photo_gallery_id: options.model.photo_gallery_id};
                                        },
                                        success: function (e) {
                                            console.log(e.response);
                                            options.model.set("photo_name", e.response.image);
                                        }
                                    });
                        }

                    </script>

                    <div id="kWindow"></div>
                    <div class="clearfix"></div>

                </div>
                <!-- Event Photo Gallery Tab End Here -->

                <!-- Event Video Gallery Tab Start Here -->

                <div>
                    <div id="videogallery"></div>
                    <script type="text/javascript">

                        jQuery(document).ready(function () {

                            var dataSource = new kendo.data.DataSource({
                                pageSize: 10,
                                transport: {
                                    read: {
                                        url: "../../controller/video_gallery_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "GET"

                                    },
                                    update: {
                                        url: "../../controller/video_gallery_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "POST",
                                        complete: function (e) {

                                            jQuery("#videogallery").data("kendoGrid").dataSource.read();
                                        }

                                    },
                                    destroy: {
                                        url: "../../controller/video_gallery_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "DELETE"

                                    },
                                    create: {
                                        url: "../../controller/video_gallery_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "PUT",
                                        complete: function (e) {

                                            jQuery("#videogallery").data("kendoGrid").dataSource.read();
                                        }

                                    }

                                },
                                autoSync: false,
                                schema: {
                                    errors: function (e) {

                                        if (e.error === "yes")

                                        {

                                            var message = "";
                                            message += e.message;
                                            var window = jQuery("#kWindow");
                                            if (!window.data("kendoWindow")) {

                                                window.kendoWindow({
                                                    title: "Error window",
                                                    modal: true,
                                                    height: 100,
                                                    width: 400

                                                });
                                            }

                                            window.data("kendoWindow").center().open();
                                            window.html('<div style="margin-top:10px;text-align:center;"><span style="color:red;font-size:14px;">' + message + '</span></div>');
                                            this.cancelChanges();
                                        }

                                    },
                                    data: "data",
                                    total: "data.length",
                                    model: {
                                        id: "video_gallery_id",
                                        fields: {
                                            video_gallery_id: {editable: false, nullable: true},
                                            link: {type: "string", validation: {required: true}},
                                            is_active: {type: "boolean"}

                                        }

                                    }

                                }

                            });
                            jQuery("#videogallery").kendoGrid({
                                dataSource: dataSource,
                                filterable: true,
                                pageable: {
                                    refresh: true,
                                    input: true,
                                    numeric: false,
                                    pageSizes: [10, 20, 50]

                                },
                                sortable: true,
                                groupable: true,
                                resizable: true,
                                toolbar: [{name: "create", text: "Add Video Link"}],
                                columns: [
                                    {field: "link", title: "Link / URL", id: "link"},
                                    {field: "is_active", title: "Active?", template: "#= is_active ? 'Yes' : 'No' #", width: "10%"},
                                    {command: ["edit", "destroy"], title: "Action", width: "200px"}],
                                editable: "inline"

                            });
                        });</script>
                    <div id="kWindow"></div>
                    <div class="clearfix"></div>

                </div>
                <!-- Event Video Gallery Tab End Here -->

                <!-- Event Activity Tab Start Here -->
                <div>
                    <div id="activity"></div>
                    <script type="text/javascript">


                        jQuery(document).ready(function () {
                            var dataSource = new kendo.data.DataSource({
                                pageSize: 10,
                                transport: {
                                    read: {
                                        url: "../../controller/event_activity_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "GET"
                                    },
                                    update: {
                                        url: "../../controller/event_activity_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "POST",
                                        complete: function (e) {
                                            jQuery("#activity").data("kendoGrid").dataSource.read();
                                        }
                                    },
                                    destroy: {
                                        url: "../../controller/event_activity_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "DELETE"
                                    },
                                    create: {
                                        url: "../../controller/event_activity_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "PUT",
                                        complete: function (e) {
                                            jQuery("#activity").data("kendoGrid").dataSource.read();
                                        }
                                    },
                                },
                                autoSync: false,
                                schema: {
                                    data: "data",
                                    total: "data.length",
                                    model: {
                                        id: "event_activity_id",
                                        fields: {
                                            event_activity_id: {type: "number", editable: false},
                                            activity_id: {type: "number"},
                                            activity_name: {type: "string"},
                                            event_activity_title: {type: "string", validation: {required: true}},
                                            event_activity_details: {type: "string"},
                                            image: {type: "string"},
                                            is_active: {type: "boolean"}


                                        }
                                    }
                                }
                            });
                            jQuery("#activity").kendoGrid({
                                dataSource: dataSource,
                                filterable: true,
                                pageable: {
                                    refresh: true,
                                    input: true,
                                    numeric: false,
                                    pageSizes: true,
                                    pageSizes: [10, 20, 50],
                                },
                                sortable: true,
                                groupable: true,
                                toolbar: [{name: "create", text: "Add Activity"}],
                                columns: [
                                    {field: "activity_id",
                                        title: "Activity Name",
                                        width: "130px",
                                        editor: ActivityNameDropDownEditor,
                                        template: "#=activity_name#",
                                        filterable: {
                                            ui: ActivityNameFilter,
                                            extra: false,
                                            operators: {
                                                string: {
                                                    eq: "Is equal to",
                                                    neq: "Is not equal to"

                                                }

                                            }

                                        }

                                    },
                                    {field: "event_activity_title", title: "Title", width: "200px"},
                                    {field: "event_activity_details",
                                        title: "Activity Details", width: "550px",
                                        editor: activitytextAreaEditor
                                    },
                                    {field: "image",
                                        title: "Activity Image", width: "330px",
                                        editor: activityfileEditor,
                                        template: "<img src='<?php echo $con->baseUrl("uploads/event_activity_image/") ?>#=image#'   height='100' width='300'/>"
                                    },
                                    {field: "is_active", title: "Active?", template: "#= is_active ? 'Yes' : 'No' #", width: "80px"},
                                    {command: ["edit", "destroy"], title: "Action", width: "200px"}],
                                editable: "inline"
                            });
                        });
                        function activityfileEditor(container, options) {
                            $('<input type="file" id="image" name="image" data-role="upload" />')
                                    .appendTo(container)
                                    .kendoUpload({
                                        multiple: true,
                                        async: {
                                            saveUrl: "../../controller/event_activity_image_save.php",
                                            autoUpload: true
                                        },
                                        upload: function (e) {
                                            e.data = {event_activity_id: options.model.event_activity_id};
                                        },
                                        success: function (e) {
                                            console.log(e.response);
                                            options.model.set("image", e.response.image);
                                        }
                                    });
                        }

                        function activitytextAreaEditor(container, options) {
                            $('<textarea id="event_activity_details" name="event_activity_details" rows="3" cols="30">' + options.field + '</textarea>')
                                    .appendTo(container)
                                    .kendoEditor({
                                        tools: [
                                            "bold",
                                            "italic",
                                            "underline",
                                            "strikethrough",
                                            "justifyLeft",
                                            "justifyCenter",
                                            "justifyRight",
                                            "justifyFull",
                                            "insertUnorderedList",
                                            "insertOrderedList",
                                            "indent",
                                            "outdent",
                                            "createLink",
                                            "unlink",
                                            "insertImage",
                                            "insertFile",
                                            "subscript",
                                            "superscript",
                                            "createTable",
                                            "addRowAbove",
                                            "addRowBelow",
                                            "addColumnLeft",
                                            "addColumnRight",
                                            "deleteRow",
                                            "deleteColumn",
                                            "viewHtml",
                                            "formatting",
                                            "cleanFormatting",
                                            "fontName",
                                            "fontSize",
                                            "foreColor",
                                            "backColor"
                                        ]
                                    });
                        }

                    </script>
                    <script type="text/javascript">

                        function ActivityNameFilter(element) {

                            element.kendoDropDownList({
                                autoBind: false,
                                dataTextField: "activity_name",
                                dataValueField: "activity_id",
                                dataSource: {
                                    transport: {
                                        read: {
                                            url: "../../controller/activity_controller.php",
                                            type: "GET"

                                        }

                                    },
                                    schema: {
                                        data: "data"

                                    }

                                },
                                optionLabel: "Select Activity"

                            });
                        }

                        function ActivityNameDropDownEditor(container, options) {

                            jQuery('<input required data-text-field="activity_name" data-value-field="activity_id" data-bind="value:' + options.field + '"/>')

                                    .appendTo(container)

                                    .kendoDropDownList({
                                        autoBind: false,
                                        dataTextField: "activity_name",
                                        dataValueField: "activity_id",
                                        dataSource: {
                                            transport: {
                                                read: {
                                                    url: "../../controller/activity_controller.php",
                                                    type: "GET"

                                                }

                                            },
                                            schema: {
                                                data: "data"

                                            }

                                        },
                                        optionLabel: "Select Activity"

                                    });
                        }
                    </script>
                    <div id="kWindow"></div>
                    <div class="clearfix"></div>

                </div>
                <!-- Event Attraction Tab End Here -->

                <!-- Event FAQ Tab Start Here -->
                <div>
                    <div id="faq"></div>
                    <script type="text/javascript">

                        jQuery(document).ready(function () {

                            var dataSource = new kendo.data.DataSource({
                                pageSize: 10,
                                transport: {
                                    read: {
                                        url: "../../controller/faq_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "GET"

                                    },
                                    update: {
                                        url: "../../controller/faq_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "POST",
                                        complete: function (e) {

                                            jQuery("#faq").data("kendoGrid").dataSource.read();
                                        }

                                    },
                                    destroy: {
                                        url: "../../controller/faq_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "DELETE"

                                    },
                                    create: {
                                        url: "../../controller/faq_controller.php?event_id=<?php echo $event_id; ?>",
                                        type: "PUT",
                                        complete: function (e) {

                                            jQuery("#faq").data("kendoGrid").dataSource.read();
                                        }

                                    }

                                },
                                autoSync: false,
                                schema: {
                                    errors: function (e) {

                                        if (e.error === "yes")

                                        {

                                            var message = "";
                                            message += e.message;
                                            var window = jQuery("#kWindow");
                                            if (!window.data("kendoWindow")) {

                                                window.kendoWindow({
                                                    title: "Error window",
                                                    modal: true,
                                                    height: 100,
                                                    width: 400

                                                });
                                            }

                                            window.data("kendoWindow").center().open();
                                            window.html('<div style="margin-top:10px;text-align:center;"><span style="color:red;font-size:14px;">' + message + '</span></div>');
                                            this.cancelChanges();
                                        }

                                    },
                                    data: "data",
                                    total: "data.length",
                                    model: {
                                        id: "faq_id",
                                        fields: {
                                            faq_id: {editable: false, nullable: true},
                                            question: {type: "string", validation: {required: true}},
                                            answer: {type: "string"},
                                            is_active: {type: "boolean"}
                                        }

                                    }

                                }

                            });
                            jQuery("#faq").kendoGrid({
                                dataSource: dataSource,
                                filterable: true,
                                pageable: {
                                    refresh: true,
                                    input: true,
                                    numeric: false,
                                    pageSizes: [10, 20, 50]

                                },
                                sortable: true,
                                groupable: true,
                                resizable: true,
                                toolbar: [
                                    {name: "create", text: "Add FAQ"}
                                ],
                                columns: [
                                    {field: "question",
                                        title: "Question?", width: "250px",
                                        editor: questiontextAreaEditor
                                    },
                                    {field: "answer",
                                        title: "Answer", width: "250px",
                                        editor: answertextAreaEditor
                                    },
                                    {field: "is_active", title: "Active?", template: "#= is_active ? 'Yes' : 'No' #", width: "10%"},
                                    {command: ["edit", "destroy"], title: "Action", width: "130px"}
                                ],
                                editable: "inline"

                            });
                        });
                        function answertextAreaEditor(container, options) {
                            $('<textarea id="answer" name="answer" rows="3" cols="30">' + options.field + '</textarea>')
                                    .appendTo(container)
                                    .kendoEditor({
                                        tools: [
                                            "bold",
                                            "italic",
                                            "underline",
                                            "strikethrough",
                                            "justifyLeft",
                                            "justifyCenter",
                                            "justifyRight",
                                            "justifyFull",
                                            "insertUnorderedList",
                                            "insertOrderedList",
                                            "indent",
                                            "outdent",
                                            "createLink",
                                            "unlink",
                                            "insertImage",
                                            "insertFile",
                                            "subscript",
                                            "superscript",
                                            "createTable",
                                            "addRowAbove",
                                            "addRowBelow",
                                            "addColumnLeft",
                                            "addColumnRight",
                                            "deleteRow",
                                            "deleteColumn",
                                            "viewHtml",
                                            "formatting",
                                            "cleanFormatting",
                                            "fontName",
                                            "fontSize",
                                            "foreColor",
                                            "backColor"
                                        ]
                                    });
                        }

                        function questiontextAreaEditor(container, options) {
                            $('<textarea id="question" name="question" rows="3" cols="30">' + options.field + '</textarea>')
                                    .appendTo(container)
                                    .kendoEditor({
                                        tools: [
                                            "bold",
                                            "italic",
                                            "underline",
                                            "strikethrough",
                                            "justifyLeft",
                                            "justifyCenter",
                                            "justifyRight",
                                            "justifyFull",
                                            "insertUnorderedList",
                                            "insertOrderedList",
                                            "indent",
                                            "outdent",
                                            "createLink",
                                            "unlink",
                                            "insertImage",
                                            "insertFile",
                                            "subscript",
                                            "superscript",
                                            "createTable",
                                            "addRowAbove",
                                            "addRowBelow",
                                            "addColumnLeft",
                                            "addColumnRight",
                                            "deleteRow",
                                            "deleteColumn",
                                            "viewHtml",
                                            "formatting",
                                            "cleanFormatting",
                                            "fontName",
                                            "fontSize",
                                            "foreColor",
                                            "backColor"
                                        ]
                                    });
                        }
                    </script>		
                    <div id="kWindow"></div>
                    <div class="clearfix"></div>

                    <br/>

                    <div>

                        <input type="submit" class="btn btn-success" name="btnUpdateFAQ" value="Go Event Page" />

                    </div>
                </div>
                <!-- Event FAQ Tab End Here -->

            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                var kendotab_edit = $("#kendotab_edit").kendoTabStrip({
                    animation: {
                        open: {
                            effects: "fadeIn"

                        }

                    }

                }).data("kendoTabStrip");
            });

        </script>
    </div>
</form>
<!-- Edit Event Tab End-->

<?php include("../layout/copyright.php"); ?>

