<?php
session_start();
include("../../config/class.web.config.php");
//error_reporting(0);
$con = new Config();
$err = '';
$msg = '';
/* Start Authentication */
if ($con->authenticate() == 1) {
    $con->redirect("../../login.php");
}
$event_id='';
/* End Event Field Initialization */
if (isset($_GET["event_id"])) {
 $event_id = base64_decode($_GET["event_id"]);
//    $event_schedules = "";
//    if (is_numeric($event_id)) {
//
//        $queryString = "SELECT es.event_schedule_id + 1 as event_schedule_id,es.event_schedule_start_time,es.event_schedule_end_time,dates.date as event_date,es.is_active,es.event_id FROM event_schedule as es
//RIGHT OUTER JOIN (SELECT `date` from dates where date between (SELECT event_start_date FROM event WHERE event.event_id='$event_id') AND (SELECT event_end_date FROM event WHERE event.event_id='$event_id')) as dates ON dates.date = es.event_date
//Left OUTER JOIN event e ON es.event_id = e.event_id WHERE es.event_id is NULL OR es.event_id='$event_id'";
//        $event_schedules = $con->ReturnObjectByQuery($queryString, "array");
//        $con->debug($event_schedules);
//    }
    
    
}

if(isset($_POST["btnCreateSchedule"])){
    extract($_POST);
    $schedules = $_POST;
    //$con->debug($schedules);
    unset($schedules["event_id"]);
    unset($schedules["btnCreateSchedule"]);
    //$con->debug($schedules);
    $total_count = count($schedules)/3;
    //$con->debug($total_count);
    for($i=1; $i<=$total_count;$i++){

       $temp_schedule_date_key = "schedule_date_".$i;
       $temp_schedule_start_time_key ="schedule_start_time_".$i;
       $temp_schedule_end_time_key ="schedule_end_time_".$i;
       
       $temp_event_id = $_POST["event_id"];
       $temp_schedule_date = $_POST["$temp_schedule_date_key"];
       $temp_schedule_start_time= $_POST["$temp_schedule_start_time_key"];
       $temp_schedule_end_time = $_POST["$temp_schedule_end_time_key"];
       
       $temp_schedule_array = array("event_date"=>$temp_schedule_date,"event_schedule_start_time"=>$temp_schedule_start_time,"event_schedule_end_time"=>$temp_schedule_end_time,"event_id"=>$event_id);
       $temp_result = $con->Insert("event_schedule", $temp_schedule_array, "", "", "array");
       //$con->debug($temp_result);
    }
    $con->redirect("ticket.php?event_id=".base64_encode($_POST["event_id"]));
}
?>
<?php include '../layout/header_script.php'; ?>
<body>
    <div class="mainwrapper">
        <?php include '../layout/header_login_info.php'; ?>
        <div class="leftpanel">
            <?php include '../layout/left_navigation.php'; ?>
        </div>
        <div class="rightpanel">
            <ul class="breadcrumbs">
                <li><a href="dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
                <li>Event Management <span class="separator"></span></li>
                <li>Add New Event <span class="separator"></span></li>
                <li>Add New Event Schedule</li>
            </ul>
            <div class="maincontent">
                <div class="maincontentinner">
                    <div class="row-fluid">
                        <form method="post">
                            <div class="k-block"> 
                                <div class="maincontentinner">
                                    <div class="row-fluid">
                                        <?php echo(!empty($msg) ? $con->notification($msg, '') : $con->notification('', $err)); ?>
                                    </div>
                                    <div class="row-fluid">
                                        <input type="button" id="btnAddSchedule" value="Add Event Schedule" class="btn btn-success"/>
                                    </div>
                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            var count = 0;
                                            $("#btnAddSchedule").click(function () {
                                                count++;
                                                console.log(count);
                                                var htmlString = '<div class="row-fluid">';

                                                htmlString += '<div class="span3">';
                                                htmlString += 'Schedule Date &nbsp;:<input type="text" name="schedule_date_' + count + '" id="schedule_date_' + count + '" value="">';
                                                htmlString += '<script type="text/javascript">';
                                                htmlString += '$("#schedule_date_' + count + '").kendoDatePicker({ });';
                                                htmlString += '</scr' + 'ipt>';
                                                htmlString += '</div>';

                                                htmlString += '<div class="span3">';
                                                htmlString += 'Schedule Start Time &nbsp;<input type="text" name="schedule_start_time_' + count + '" id="schedule_start_time_' + count + '" value="">';
                                                htmlString += '<script type="text/javascript">';
                                                htmlString += '$("#schedule_start_time_' + count + '").kendoTimePicker({ });';
                                                htmlString += '</scr' + 'ipt>';
                                                htmlString += '</div>';

                                                htmlString += '<div class="span3">';
                                                htmlString += 'Schedule End Time &nbsp;<input type="text" name="schedule_end_time_' + count + '" id="schedule_end_time_' + count + '" value="">';
                                                htmlString += '<script type="text/javascript">';
                                                htmlString += '$("#schedule_end_time_' + count + '").kendoTimePicker({ });';
                                                htmlString += '</scr' + 'ipt>';
                                                htmlString += '</div>';

                                                htmlString += '</div>';
                                                
                                                
                                                $("#schedule_div").append(htmlString);

                                            });
                                        });</script>
                                 
                                       
                                        <div id="schedule_div">

                                        </div>
                                    <hr />
                                    <div class="row-fluid">
                                        <div class="span12 pull-left">
                                            <input type="submit" value="Create Event"  name="btnCreateSchedule" class="k-button btn-success"/>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </form>

                    </div>
                    <!--                    <div style="padding-bottom:100px;"></div>               -->
                </div>
            </div>
            <?php include '../layout/footer.php'; ?>
        </div>
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('.tabbedwidget').tabs();
        });
        jQuery('#1').addClass('active');
    </script>
</body>
</html>
