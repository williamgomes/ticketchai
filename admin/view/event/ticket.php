<?php
session_start();
include("../../config/class.web.config.php");
$con = new Config();
/* Start Authentication */
if ($con->authenticate() == 1) {
    $con->redirect("../../login.php");
}
/* End Authentication */
$err = '';
$msg = '';
/* Start Ticket Fields Initialization */
$event_id = "";
$ticket_type_id = "";
$ticket_total = "";
$ticket_price = "";
$discount_price = "";
$ticket_price = "";
$ticket_available = "";
$ticket_limit = "";
$vendor_id = "";
$total_vticket = "";
$total_eticket = "";
$total_pticket = "";

$schedules = array();

if (isset($_GET["event_id"])) {
    $event_id = base64_decode($_GET["event_id"]);
    /* Start Ticket Information */
    $schedules = $con->SelectAll("event_schedule", "", array("event_id" => $event_id), "array");
    /*  End Ticket Information */
}



/* End Ticket Fields Initialization */
if (isset($_POST["btnCreateEventTicket"])) {
    extract($_POST);

    //$con->debug($_POST);`````````````````````````````````````````````````````````````````````````````````````````````````
    if ($_POST["ticket_total"] == "") {
        $err = "Total Ticket";
    } elseif ($_POST["ticket_price"] == "") {
        $err = "Ticket Price";
    } elseif ($_POST["discount_price"] == "") {
        $err = "Discount Price";
    } else {
        $schedule_event_id = base64_decode($_GET["event_id"]);
        $insert_schedules = $con->SelectAll("event_schedule", "", array("event_id" => $schedule_event_id), "array");

        foreach ($insert_schedules as $is) {
            $checkExistance = $con->CheckExistsWithCondition("ticket", " event_id='$is->event_id' AND schedule_id='$is->event_schedule_id'");
            if ($checkExistance == 1) {
                $schedule_total_ticket_key = "total_ticket_".$is->event_schedule_id;
                $schedule_total_ticket = $_POST["$schedule_total_ticket_key"];
                
                $total_e_ticket_key = "total_e_ticket_".$is->event_schedule_id;
                $total_e_ticket = $_POST["$total_e_ticket_key"];
                
                $total_p_ticket_key = "total_p_ticket_".$is->event_schedule_id;
                $total_p_ticket =$_POST["$total_p_ticket_key"];
                
                
                $update_ticket_array = array(
                    "event_id"=>$schedule_event_id,
                    "discount_price" => $_POST["discount_price"],
                    "ticket_price" => $_POST["ticket_price"],
                    "ticket_total" => $_POST["ticket_total"],
                    "schedule_total_ticket"=>$schedule_total_ticket,
                    "total_eticket"=>$total_e_ticket,
                    "total_pticket"=>$total_p_ticket
                );
                $result = $con->Update("ticket", $update_ticket_array, "",  " event_id='$schedule_event_id' AND schedule_id='$is->event_schedule_id'", "array");
                $con->debug($result);
            } else {
                $schedule_total_ticket_key = "total_ticket_".$is->event_schedule_id;
                $schedule_total_ticket = $_POST["$schedule_total_ticket_key"];
                
                $total_e_ticket_key = "total_e_ticket_".$is->event_schedule_id;
                $total_e_ticket = $_POST["$total_e_ticket_key"];
                
                $total_p_ticket_key = "total_p_ticket_".$is->event_schedule_id;
                $total_p_ticket =$_POST["$total_p_ticket_key"];
                
                
                $insert_ticket_array = array(
                    "event_id" => $schedule_event_id,
                    "schedule_id" => $is->event_schedule_id,
                    "discount_price" => $_POST["discount_price"],
                    "ticket_price" => $_POST["ticket_price"],
                    "ticket_total" => $_POST["ticket_total"],
                    "schedule_total_ticket"=>$schedule_total_ticket,
                    "total_eticket"=>$total_e_ticket,
                    "total_pticket"=>$total_p_ticket
                );
                $result = $con->Insert("ticket", $insert_ticket_array, "", "", "array");
                $con->debug($result);
                
            }
        }
    }
}
?>
<?php include '../layout/header_script.php'; ?>
<body>
    <div class="mainwrapper">
<?php include '../layout/header_login_info.php'; ?>
        <div class="leftpanel">
<?php include '../layout/left_navigation.php'; ?>
        </div>
        <div class="rightpanel">
            <ul class="breadcrumbs">
                <li><a href="dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
                <li>Ticket Management <span class="separator"></span></li>
                <li>Add New Ticket</li>
            </ul>

            <div class="maincontent">
                <div class="maincontentinner">
                    <div class="row-fluid">
                        <form method="post">
                            <div class="k-block">
                                <div class="maincontentinner">
                                    <div class="row-fluid">
<?php echo(!empty($msg) ? $con->notification($msg, '') : $con->notification('', $err)); ?>
                                    </div>
                                    <div class="row-fluid">


                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="span3">Total Ticket:</div>
                                            <div class="span9"><?php echo $con->NumericTextBox("ticket_total", "ticket_total", "", "1", "10000", "", "width:60%", ""); ?></div>

                                        </div>
                                        <div class="span6">
                                            <div class="span3">Ticket Price:</div>
                                            <div class="span9"><?php echo $con->NumericTextBox("ticket_price", "ticket_price", "", "1", "10000", "", "width:60%", ""); ?></div>
                                        </div>

                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="span3">Discount Price:</div>
                                            <div class="span9"><?php echo $con->NumericTextBox("discount_price", "discount_price", "", "1", "10000", "", "width:60%", ""); ?></div>
                                        </div>
                                        <!--                                    <div class="span6" id="group_info" style="display: none;">
                                                                                    <div class="span3">Ticket Limit:</div>
                                                                                    <div class="span9"><?php //echo $con->NumericTextBox("ticket_limit", "ticket_limit", "", "1", "20", "", "width:60%", "");   ?></div>
                                                                                </div>-->

                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <!--                                            <div class="span3">Is Vendor :</div>
                                                                                        <div class="span9"><input type="checkbox" id="is_vendor"/>
                                                                                            <script type="text/javascript">
                                                                                                $(document).ready(function () {
                                                                                                    $("#is_vendor").change(function () {
                                            
                                                                                                        if (this.checked) {
                                                                                                            $("#vendor_info").fadeIn("slow");
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            $("#vendor_info").fadeOut("slow");
                                                                                                        }
                                                                                                    });
                                                                                                });
                                                                                            </script>
                                                                                        </div>-->
                                        </div>
                                    </div>
                                    <div id="vendor_info" style="display: none;">
                                        <!--                                        <div class="row-fluid" >
                                                                                    <div class="span6">
                                                                                        <div class="span3">Select Vendor:</div>
                                                                                        <div class="span9"><?php //echo $con->SelectBox("vendor_id", "vendor_id", "", "vendor_id", "vendor_name", $vendor_id, "", "vendor_controller.php", "kendo", "width:60%", "");   ?></div>
                                                                                    </div>
                                                                                    <div class="span6">
                                                                                        <div class="span3">Total Vendor Ticket:</div>
                                                                                        <div class="span9"><?php //echo $con->NumericTextBox("total_vticket", "total_vticket", "", "1", "10000", "", "width:60%", "");   ?></div>
                                                                                    </div>
                                                                                </div>-->
                                    </div>

<?php if (count($schedules)): ?>
    <?php foreach ($schedules as $se): ?>
                                            <div style="margin: 5px; padding: 5px; border: 1px solid silver;">
                                                <div class="row-fluid">
                                                    <div class="span4">
                                                        <input type="hidden" name="event_schedule_id_<?php echo $se->event_schedule_id; ?>" value="<?php echo $se->event_schedule_id; ?>" />
                                                        <div>Schedule Date: <?php echo $se->event_date; ?></div>
                                                        <div>Start Time: <?php echo $se->event_schedule_start_time; ?></div>
                                                        <div>End Time: <?php echo $se->event_schedule_end_time; ?></div>
                                                    </div>
                                                    <div class="span4">
                                                        <div class="span3">
                                                            Total Ticket :
                                                        </div>
                                                        <div class="span9">
        <?php $total_ticket_key = "total_ticket_" . $se->event_schedule_id; ?>
        <?php echo $con->NumericTextBox("$total_ticket_key", "$total_ticket_key", "", "1", "999999", "", "width:60%", ""); ?>
                                                        </div>
                                                    </div>
                                                    <div class="span4">
                                                        <div class="span3">E-Ticket</div>
                                                            <?php $total_e_ticket_key = "total_e_ticket_" . $se->event_schedule_id; ?>
                                                        <div class="span9"><?php echo $con->NumericTextBox("$total_e_ticket_key", "$total_e_ticket_key", "", "1", "999999", "", "width:60%", ""); ?></div>
                                                    </div>
                                                    <div class="span4">
                                                        <div class="span3">P-Ticket</div>
                                                        <?php $total_p_ticket_key = "total_p_ticket_" . $se->event_schedule_id; ?>
                                                        <div class="span9"><?php echo $con->NumericTextBox("$total_p_ticket_key", "$total_p_ticket_key", "", "1", "999999", "", "width:60%", ""); ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                                    <?php endforeach; ?>
<?php endif; ?>



                                    <hr />
                                    <div class="row-fluid">
                                        <div class="span12 pull-left">
                                            <input type="submit" value="Create Event Ticket"  name="btnCreateEventTicket" class="k-button btn-success"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--                    <div style="padding-bottom:100px;"></div>               -->
                </div>
            </div>
<?php include '../layout/footer.php'; ?>
        </div>
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('.tabbedwidget').tabs();
        });
        jQuery('#1').addClass('active');
    </script>
</body>
</html>