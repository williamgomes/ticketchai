<?php
include("../../config/class.web.config.php");
include("../../lib/mpdf/mpdf.php");
$con = new Config();
$report_query = "SELECT CASE delivery_method WHEN 'online_payment' THEN 'Online Payment' ELSE CASE delivery_method WHEN 'home_delivery' THEN 'Cash On Delivery' ELSE 'Pick From Office' END END AS method, sum(ifnull(total_amount, 0)) AS TotalAmount, count(ifnull(customer_id, 0)) AS TotalPerson FROM booking WHERE payment_status = 'paid' GROUP BY delivery_method";
$rs = $con->ReturnObjectByQuery($report_query, "array");
//$con->debug($rs);
/* Start Authentication */
if ($con->authenticate() == 1) {
    $con->redirect("../../login.php");
}

/* End Authentication */
?>
<?php include("../layout/header_script.php"); ?>
<?php include("../layout/menu_top.php"); ?>
<?php include("../layout/breadcrumbs.php"); ?>

<div class="tab-v1 margin-bottom-60" style="padding-left: 14px; padding-right: 14px; margin-top: -1px;">
    <ul class="nav nav-tabs nav-justified margin-bottom-20">
        <li class="active"><a href="#book-2" data-toggle="tab">Booking History</a></li>
    </ul>
    <div class="tab-content" style="">
        <div class="tab-pane active" id="book-2">
            <div class="panel panel-grey margin-bottom-50" style="border-style: none;">
                <div class="panel-body" style="border: 2px solid silver; margin-top: -20px;">
                    <div class="row">
                        <div class="col-md-12">
                            <div style="margin-right: 5px; margin-left: 5px;" id="grid"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      

    </div>


</div>

<?php if ($_SESSION["user_type_id"] == 5): ?>
    <script id="command-template" type="text/x-kendo-template">
        
    </script>
<?php else: ?>
    <script id="command-template" type="text/x-kendo-template">
        # if(delivery_method == "online_payment") { #
        # } else {#
        # if(payment_status == "paid") { #
        <a style="font-size:11px;" class="k-button k-grid-even" href="confirm_payment.php?booking_id=#= booking_id #&payment_status=#= payment_status #">Cancel Payment</a>
        # } else { #
        <a style="font-size:11px;" class="k-button k-grid-odd" href="confirm_payment.php?booking_id=#= booking_id #&payment_status=#= payment_status #">Confirm Payment</a>
        # } #
        # } #
    </script>
<?php endif; ?>
<script id="download" type="text/x-kendo-template">
    <a style="font-size:11px;" class="k-button k-grid-even" href="download.php?booking_id=#= booking_id #&event_id=#= event_id #&schedule_id =#= schedule_id #">Download</a>
</script>
<?php if ($_SESSION["user_type_id"] == 5): ?>
    <script id="delete_booking" type="text/x-kendo-template">
        
    </script>
<?php else: ?>
<script id="delete_booking" type="text/x-kendo-template">
        # if(payment_status == "paid") { #
        <a style="font-size:11px;" class="k-button k-grid-even">Done</a>
        # } else { #
        <a style="font-size:11px;" class="k-button k-grid-odd" href="booking_delete.php?booking_id=#= booking_id #&payment_status=#= payment_status #">Delete</a>
       # } #
</script>
<?php endif; ?>
<script id="command-delivery-status" type="text/x-kendo-template">
    # if(delivery_method=="home_delivery"){#
    <a style="font-size:11px; text-decoration:none; width:120px;" class="k-button k-grid-even" href="delivery_method_confirm.php?booking_id=#= booking_id #&delivery_method=#= delivery_method #">Change Delivery Method</a>
    # } else { #
    <a style="font-size:11px; text-decoration:none; width:120px;" class="k-button k-grid-even" href="javascript:void(0);">Change Delivery Method</a>
    # } #
</script>
<script id="command-delivery-method" type="text/x-kendo-template">
    # if(delivery_method=="online_payment"){#
    Online Payment
    # } else if (delivery_method=="home_delivery"){#
    Cash On Delivery
    # } else {
    # Pick From Office #
    } #
</script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        var dataSource = new kendo.data.DataSource({
            pageSize: 20,
            transport: {
                read: {
                    url: "../../controller/booking_controller.php",
                    type: "GET"
                }
            },
            autoSync: false,
            schema: {
                data: "data",
                total: "data.length",
                model: {
                    id: "booking_id",
                    fields: {
                        booking_id: {type: "number"},
                        tocken: {type: "string"},
                        event_id: {type: "number"},
                        schedule_id: {type: "number"},
                        customer_name: {type: "string"},
                        delivery_phone_number: {type: "string"},
                        email: {type: "string"},
                        delivery_address: {type: "string"},
                        anual_subsciption_cost: {type: "string"},
                        enrollment_fee: {type: "string"},
                        life_time_subsciption_cost: {type: "number"},
                        ticket_price: {type: "string"},
                        total_amount: {type: "string"},
                        delivery_method: {type: "string"},
                        online_pay_method: {type: "string"},
                        payment_status: {type: "string"}
                       
                    }

                }
            }

        });
        jQuery("#grid").kendoGrid({
            dataSource: dataSource,
            filterable: true,
            pageable: {
                refresh: true,
                input: true,
                numeric: false,
                pageSizes: [20, 50, 100]
            },
            sortable: true,
            groupable: true,
            scrollable: true,
            columns: [
                {field: "tocken", title: "Ticket Number.", width: "140px"},
                {field: "booking_id", title: "Transaction ID", width: "120px"},
                {field: "customer_name", title: "Customer Name", width: "140px"},
                {field: "delivery_phone_number", title: "Mobile Number", width: "130px"},
                {field: "email", title: "Email Address", width: "200px"},
                {field: "delivery_address", title: "Delivery Address", width: "160px"},
                {field: "anual_subsciption_cost", title: "Anual Subscription", width: "150px"},
                {field: "enrollment_fee", title: "Enrollment Fee", width: "170px"},
                {field: "life_time_subsciption_cost", title: "Lifetime Amount", width: "170px"},
                {field: "ticket_price", title: "Ticket Price", width: "170px"},
                {field: "total_amount", title: "Total Amount", width: "170px"},
                {
                    title: "Payment Method", width: "150px",
                    template: kendo.template($("#command-delivery-method").html())
                },
                {field: "online_pay_method", title: "Delivery Method", width: "170px"},
                {field: "payment_status", title: "Payment Status", width: "140px"},
                
                {
                    title: "Ticket Download", width: "130px",
                    template: kendo.template($("#download").html())
                },
                {
                    title: "Payment Confirmation", width: "150px",
                    template: kendo.template($("#command-template").html())
                },
                {
                    title: "Delete Booking", width: "160px",
                    template: kendo.template($("#delete_booking").html())
                }
            ]
        });

    });</script>	
<div id="kWindow"></div>

<!-- Subscription Grid -->
<script id="donation_confirm" type="text/x-kendo-template">
        # if(method != "Online Payment"){ #
        # if(payment_status == "paid") { #
        <a style="font-size:11px;" class="k-button k-grid-even" href="donation_payment.php?donation_id=#= donation_id #&payment_status=#= payment_status #">Cancel Payment</a>
        # } else { #
        <a style="font-size:11px;" class="k-button k-grid-odd" href="donation_payment.php?donation_id=#= donation_id #&payment_status=#= payment_status #">Confirm Payment</a>
        # } #
        # } else {#

        # } #
</script>
<script id="donation_print" type="text/x-kendo-template">
    <a style="font-size:11px;" class="k-button k-grid-even" href="donation_download.php?donation_id=#= donation_id #">Download</a>
</script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        var dataSource = new kendo.data.DataSource({
            pageSize: 20,
            transport: {
                read: {
                    url: "../../controller/donation_controller.php",
                    type: "GET"
                }
            },
            autoSync: false,
            schema: {
                data: "data",
                total: "data.length",
                model: {
                    id: "donation_id",
                    fields: {
                        donation_id: {type: "number"},
                        token_no: {type: "string"},
                        customer_name: {type: "string"},
                        email: {type: "string"},
                        batch_name: {type: "string"},
                        program_name: {type: "string"},
                        amount: {type: "string"},
                        method: {type: "string"},
                        payment_status: {type: "string"}
                    }

                }
            }

        });
        jQuery("#grid-donation").kendoGrid({
            dataSource: dataSource,
            filterable: true,
            pageable: {
                refresh: true,
                input: true,
                numeric: false,
                pageSizes: [20, 50, 100]
            },
            sortable: true,
            groupable: true,
            scrollable: true,
            columns: [
                {field: "token_no", title: "S-Ticket Number", width: "150px"},
                {field: "customer_name", title: "Customer Name", width: "150px"},
                {field: "email", title: "Email Address", width: "200px"},
                {field: "batch_name", title: "Batch", width: "100px"},
                {field: "program_name", title: "Program", width: "100px"},
                {field: "amount", title: "Amount", width: "100px"},
                {field: "method", title: "Delivery Method", width: "150px"},
                {field: "payment_status", title: "Payment Status", width: "150px"},
                {
                    title: "Confirm Donation", width: "160px",
                    template: kendo.template($("#donation_confirm").html())
                },
                {
                    title: "Action", width: "120px",
                    template: kendo.template($("#donation_print").html())
                }
            ]
        });

    });</script>	
<div id="kWindow"></div>
<!-- Subscription Grid -->


<!-- Report Summary Start Here -->
<br/>
<div class="row">
    <div class="heading heading-v2 margin-bottom-40">
        <h2>Report Summary</h2>
    </div>
</div>
<div class="tab-v1 margin-bottom-60" style="padding-left: 14px; padding-right: 14px; margin-top: -20px;">
    <ul class="nav nav-tabs nav-justified margin-bottom-20">
        <li class="active"><a href="#tab-1" data-toggle="tab">Delivery Method Summary</a></li>
        
        <li><a href="#tab-3" data-toggle="tab">Subscription Summary</a></li>
    </ul>
    <div class="tab-content" style="">
        <div class="tab-pane active" id="tab-1">

            <div class="panel panel-grey margin-bottom-40" style="border-style: none;">
                <div class="panel-body" style="border: 1px solid silver; margin-top: -30px;">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Delivery Method</th>
                                <th>Total Person</th>
                                <th>Total Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (count($rs) > 0): ?>
                                <?php foreach ($rs as $data): ?>
                                    <tr>
                                        <td><?php echo $data->method; ?></td>
                                        <td><?php echo $data->TotalPerson; ?></td>
                                        <td><?php echo $data->TotalAmount; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div> 

            </div>
        </div>
       

        <div class="tab-pane" id="tab-3">
            <div class="panel panel-grey margin-bottom-40" style="margin-top: -30px;">
                <div class="panel-body" >

                    <?php
                    $life_time_sql = "SELECT SUM(life_time_subsciption_cost) AS total_life_time, COUNT(booking_id) AS total_count FROM booking WHERE payment_status = 'paid' AND life_time_subsciption_cost > 0";
                    $life_time = $con->ReturnObjectByQuery($life_time_sql, "array");
                    //$con->debug($life_time);
                    //$annual_sql = "SELECT SUM(anual_subsciption_cost) AS total_annual, COUNT(booking_id) AS total_annual_count FROM booking WHERE payment_status = 'paid' AND anual_subsciption_cost > 0";
                    //$annual = $con->ReturnObjectByQuery($annual_sql, "array");
                    //$con->debug($annual);
                    ?>

                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Subscription Type</th>
                                <th>No of Subscriptions.</th>
                                <th>Total Amount (BDT)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Life Time</td>
                                <td>
                                    <?php
                                    if ($life_time{0}->total_count > 0) {
                                        echo $life_time{0}->total_count;
                                    } else {
                                        echo '<i style="color:gray;">No subscription yet</i>';
                                    }
                                    ?>
                                </td>

                                <td>
                                    <?php echo $life_time{0}->total_life_time; ?>
                                </td>
                            </tr>
<!--                            <tr>
                                <td>Annual</td>
                                <td>
                                    <?php
                                    //if ($annual{0}->total_annual_count > 0) {
                                     //   echo $annual{0}->total_annual_count;
                                  //  } else {
                                    //    echo '<i style="color:gray;">No subscription yet</i>';
                                    //}
                                    ?>
                                </td>
                                <td>
                                    <?php //echo $annual{0}->total_annual; ?>
                                </td>
                            </tr>-->
                        </tbody>
                    </table>

                </div> 

            </div>
        </div>


    </div>
</div>
<!-- Report Summary End Here -->
<?php include("../layout/copyright.php"); ?>