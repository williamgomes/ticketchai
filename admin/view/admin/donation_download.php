<?php
include("../../config/class.web.config.php");
include("../../lib/mpdf/mpdf.php");
$con = new Config();
if (isset($_GET["donation_id"])) {
    $donation_id = $_GET["donation_id"];
}
//Set page mergins
$mpdf = new mPDF('c', 'A4', '', '', 15, 15, 15, 15, 16, 13);
$mpdf->SetDisplayMode('fullpage');
$stylesheet = file_get_contents('style.css');
$mpdf->WriteHTML($stylesheet, 1);
$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
$url = "http://www.ticketchai.org/apec/admin/view/admin/donation_print.php?&donation_id=$donation_id";
$html = file_get_contents($url);
$mpdf->WriteHTML($html, 2);
$mpdf->Output('e_ticket.pdf', 'D');
exit;

