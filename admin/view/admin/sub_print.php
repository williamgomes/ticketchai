<?php
session_start();
include("../../config/class.web.config.php");
$con = new Config();
error_reporting(0);

if (isset($_GET["sub_id"])) {
    $sub_id = $_GET["sub_id"];
}
//Fetch donation data$subscriptions = "SELECT
$subscriptions = "SELECT
	sub.*,
        sub.payment_status as ps,
        c.*,  
        cv.*,
        college.college_name
        FROM
              subscription sub
        LEFT JOIN college_validation AS cv ON cv.college_validation_id = sub.college_validation_id
        LEFT JOIN customer as c ON c.customer_id = sub.customer_id
        LEFT JOIN college ON college.college_id = cv.college_id
        WHERE
	sub.subscription_id = '$sub_id'";
$sub_output = $con->ReturnObjectByQuery($subscriptions, "array");
?>
<!DOCTYPE html> 
<html>
    <head>
        <script type="text/javascript" src="admin/assets/js/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="jquery-barcode.js"></script>
        <link href="style.css" rel="stylesheet">
    </head>
    <body>
        <div class="top_text">
            Please print and bring this document with you, if further necessary.
        </div>
        <br />

        <?php foreach ($sub_output as $sub): ?>

            <table class="main">
                <tr>
                    <td width="139" style="height:50px;" bgcolor="#FFFFFF"><h3>Document</h3></td>
                    <td colspan="9" bgcolor="#FFFFFF"><h3><strong>Subscription Receipt</strong></h1></td>
                    
                </tr>
                <tr>
                    <td colspan="10" bgcolor="#FFFFFF"><h3>Thank You For Your Subscription!</h3></td>
                </tr>
                <tr>
                    <td bgcolor="#FFFFFF"><h6>DATE</h6></td>
                    <td colspan="4" bgcolor="#FFFFFF"> </td>
                    <td  bgcolor="#FFFFFF"><h6>TIME</h6></td>
                    <td colspan="4" bgcolor="#FFFFFF">
                        <h6>                                                
                        </h6>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#FFFFFF"><h6>Delivery Method:</h6></td>
                    <td colspan="4" bgcolor="#FFFFFF">
                        <h4>
                            <?php
                            if ($sub->subcription_delivery_method == "pick_from_point") {
                                echo "Pick From Office";
                            } else if ($sub->subcription_delivery_method == "home_delivery") {
                                echo "Home Delivery";
                            } else {
                               echo "Online Payment";
                            }
                            ?>
                        </h4>
                    </td>
                    <td bgcolor="#FFFFFF"><h6>CATAGORY</h6></td>
                    <td colspan="4" bgcolor="#FFFFFF"><h6>
                            <?php
                            if ($sub->subscription_type == 'is_lifetime') {
                                echo "Life Time";
                            } else {
                                echo "Annual Subscription";
                            }
                            ?>
                        </h6></td>
                </tr>


                <tr>
                    <td colspan="5" valign="top" bgcolor="#FFFFFF" style="font-size: 11px;">
                        <p style="text-align: center; font-size: 14px; font-weight: bold; margin-top: 4px;">Owner's Info</p>
                        <p> Name: <?php echo $sub->first_name; ?>&nbsp;<?php echo $sub->last_name; ?></p>
                        <p> Address:  <?php echo $sub->address; ?> </p>                       
                        <p> Email: <?php echo $sub->email_address; ?></p>
                        <p> Cadet NO :<?php echo $sub->student_number; ?></p>
                        <p> Batch NO :<?php echo $sub->batch_id; ?></p>
                        <p> College name:<?php echo $sub->college_name; ?>
                        <p> Phone Number:<?php echo $sub->mobile_number; ?></p>
                        <?php if ($sub->delivery_method == "home_delivery"): ?>
                            <?php if ($sub->delivery_city != ''): ?>
                                <p>Delivery City:<?php echo $sub->delivery_city; ?></p>
                            <?php elseif ($sub->district_id != ''): ?>

                                <p>Delivery District:<?php echo $sub->delivery_city; ?></p>
                            <?php endif; ?>
                            <p>Delivery Post Code:<?php echo $sub->zip_code; ?></p>
                        <?php endif; ?>
                    </td>
                    <td colspan="6">
                        <table style="width:100%;">

                            <tr>
                                <td style="border-style: none; width: 50%;">Subscription Charge:</td>
                                <td style="border-style: none; text-align: right; width: 50%;">
                                    <?php echo $sub->subscription_amount; ?>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2" style="border-style: none;"><hr style="border-style: dotted;" />

                                </td>
                            </tr>

                            <tr>
                                <td style="border-style: none; font-weight: bold;">Grand Total:</td>
                                <td style="border-style: none; font-weight: bold; font-size: 16px; text-align: right">
                                    <?php echo $sub->subscription_amount; ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>



                <tr>
                    <td style="border-top-style: none;" colspan="10" bgcolor="#FFFFFF">
                        <table style="width: 100%; margin-left: -8px;">
                            <tr>
                                <td style="border-style: none; width: 30%"> <h5>Hot Line Number: +8801971-842538, +88 04478009569 </h5></td>
                                <td style="border-style: none; width: 50%"><img src="ticketchai_logo.png" width="100" height="50" alt=""/></td>
                     
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </td>
    </tr>

    <?php
endforeach;
?>
</tbody>
</table>

</html>