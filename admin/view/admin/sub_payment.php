<?php

include ('../../config/class.web.config.php');
$con = new Config();
extract($_POST);
if (isset($_GET["subscription_id"])) {
    $subscription_id = $_GET["subscription_id"];
}
if (isset($_GET["payment_status"])) {
    $payment_status = $_GET["payment_status"];
}

if ($payment_status == "paid") {
    $update_array = array("subscription_id" => $subscription_id, "payment_status" => "unpaid");
}else {
    $update_array = array("subscription_id" => $subscription_id, "payment_status" => "paid");
}
$result = $con->update("subscription", $update_array, '', '', "array");
if ($result["output"] == "success") {
    //echo "Payment status is updated successfully";
    $con->redirect("index.php");
} else {
    //echo "Payment status is not updated!";
}
?>
