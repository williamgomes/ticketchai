<?php

include ('../../config/class.web.config.php');
$con = new Config();
extract($_POST);
if (isset($_GET["donation_id"])) {
    $donation_id = $_GET["donation_id"];
}
if (isset($_GET["payment_status"])) {
    $payment_status = $_GET["payment_status"];
}

if ($payment_status == "paid") {
    $update_array = array("donation_id" => $donation_id, "payment_status" => "unpaid");
}else {
    $update_array = array("donation_id" => $donation_id, "payment_status" => "paid");
}
$result = $con->update("donation", $update_array, '', '', "array");
if ($result["output"] == "success") {
    //echo "Payment status is updated successfully";
    $con->redirect("index.php");
} else {
    //echo "Payment status is not updated!";
}
?>
