<?php
include("../../config/class.web.config.php");
include("../../lib/mpdf/mpdf.php");
$con = new Config();

$rs = $con->ReturnObjectByQuery("select case delivery_method when 'online_payment' then 'Online Payment' else case delivery_method when 'home_delivery' then 'Home Delivery' else 'Pick From Office' end  end as method, sum(ifnull(spouseQty,0)) as Spouse,
count(Self) as Self, sum(ifnull(kidsUpQty,0)) as Kids12Up,
sum(ifnull(KidesUn12Qty,0)) as Kids12Down,  sum(ifnull(guestQty,0)) as Guest,
sum(ifnull(maidQty,0)) as Maid,
(sum(ifnull(spouseQty,0)) + count(Self) + sum(ifnull(kidsUpQty,0)) + sum(ifnull(KidesUn12Qty,0)) + sum(ifnull(guestQty,0)) +
sum(ifnull(maidQty,0)) +  sum(ifnull(driverQty,0))) as TotalPerson,
sum(ifnull(driverQty,0)) as Driver,  sum(ifnull(total_amount,0)) as TotalAmount
from booking where payment_status='paid' group by delivery_method", "array");
//$con->debug($rs);
//$donation = $con->ReturnObjectByQuery("select count(donation_id) as NoOfDonation,sum(donation_amount) as TotalDonation from donation where payment_status='paid'", "array");
$donation = $con->ReturnObjectByQuery("SELECT donation.*, batch.batch_name, customer.* FROM donation LEFT JOIN batch ON donation.batch_id = batch.batch_id LEFT JOIN customer ON donation.customer_id = customer.customer_id", "array");
//$con->debug($donation);
/* Start Authentication */
if ($con->authenticate() == 1) {
    $con->redirect("../../login.php");
}

/* End Authentication */
?>
<?php include("../layout/header_script.php"); ?>
<?php include("../layout/menu_top.php"); ?>
<?php include("../layout/breadcrumbs.php"); ?>

<div class="tab-v1 margin-bottom-60" style="padding-left: 14px; padding-right: 14px; margin-top: -1px;">
    <ul class="nav nav-tabs nav-justified margin-bottom-20">
        <li class="active"><a href="#book-2" data-toggle="tab">Booking History</a></li>
        <li><a href="#don-2" data-toggle="tab">Donation History</a></li>
<!--        <li><a href="#sub-2" data-toggle="tab">Subscription History</a></li>-->
    </ul>
    <div class="tab-content" style="">
        <div class="tab-pane active" id="book-2">
            <div class="panel panel-grey margin-bottom-50" style="border-style: none;">
                <div class="panel-body" style="border: 2px solid silver; margin-top: -20px;">
                    <div class="row">
                        <div class="col-md-12">
                            <div style="margin-right: 5px; margin-left: 5px;" id="grid"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="don-2">
            <div class="panel panel-grey margin-bottom-50" style="border-style: none;">
                <div class="panel-body" style="border: 2px solid silver; margin-top: -20px;">
                    <div class="row">
                        <div class="col-md-12">
                            <div style="margin-right: 5px; margin-left: 5px;" id="grid-donation"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="sub-2">
            <div class="panel panel-grey margin-bottom-50" style="border-style: none;">
                <div class="panel-body" style="border: 2px solid silver; margin-top: -20px;">
                    <div class="row">
                        <div class="col-md-12">
                            <div style="margin-right: 5px; margin-left: 5px;" id="grid-sub"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


</div>

<?php if ($_SESSION["user_type_id"] == 4): ?>
    <script id="command-template" type="text/x-kendo-template">

    </script>
<?php else: ?>
    <script id="command-template" type="text/x-kendo-template">
        # if(delivery_method == "online_payment") { #
        # } else {#
        # if(payment_status == "paid") { #
        <a style="font-size:11px;" class="k-button k-grid-even" href="confirm_payment.php?booking_id=#= booking_id #&payment_status=#= payment_status #">Cancel Payment</a>
        # } else { #
        <a style="font-size:11px;" class="k-button k-grid-odd" href="confirm_payment.php?booking_id=#= booking_id #&payment_status=#= payment_status #">Confirm Payment</a>
        # } #
        # } #
    </script>
<?php endif; ?>
<script id="download" type="text/x-kendo-template">
    <a style="font-size:11px;" class="k-button k-grid-even" href="download.php?booking_id=#= booking_id #&event_id=#= event_id #&schedule_id =#= schedule_id #">Download</a>
</script>
<script id="command-delivery-status" type="text/x-kendo-template">
    # if(delivery_method=="home_delivery"){#
    <a style="font-size:11px; text-decoration:none; width:120px;" class="k-button k-grid-even" href="delivery_method_confirm.php?booking_id=#= booking_id #&delivery_method=#= delivery_method #">Change Delivery Method</a>
    # } else { #
    <a style="font-size:11px; text-decoration:none; width:120px;" class="k-button k-grid-even" href="javascript:void(0);">Change Delivery Method</a>
    # } #
</script>
<script id="command-delivery-method" type="text/x-kendo-template">
    # if(delivery_method=="online_payment"){#
    Online Payment
    # } else if (delivery_method=="home_delivery"){#
    Home Delivery
    # } else {
    # Pick From Office #
    } #
</script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        var dataSource = new kendo.data.DataSource({
            pageSize: 5,
            transport: {
                read: {
                    url: "../../controller/track_controller.php",
                    type: "GET"
                }
            },
            autoSync: false,
            schema: {
                data: "data",
                total: "data.length",
                model: {
                    id: "booking_id",
                    fields: {
                        tocken: {type: "string"},
                        booking_id: {type: "number"},
                        event_id: {type: "number"},
                        schedule_id: {type: "number"},
                        // EventTitle: {type: "string"},
                        customer_full_name: {type: "string"},
                        mobile_number: {type: "string"},
                        email_address: {type: "string"},
                        // cadet_id: {type: "string"},
                        spouse: {type: "string"},
                        kidesUp12: {type: "string"},
                        kidesUn12: {type: "string"},
                        guest: {type: "string"},
                        driver: {type: "string"},
                        maid: {type: "string"},
                        booking_delivery_address: {type: "string"},
                        total_amount: {type: "string"},
                        delivery_method: {type: "string"},
                        //  delivery_cost: {type: "string"},
                        payment_status: {type: "string"},
                        //donation: {type: "string"},
                        anual_subsciption_cost: {type: "string"},
                        life_time_subsciption_cost: {type: "string"},
                        self: {type: "string"},
                        batch_id: {type: "string"},
                        student_number: {type: "string"}
                    }

                }
            }

        });
        jQuery("#grid").kendoGrid({
            dataSource: dataSource,
            filterable: true,
            pageable: {
                refresh: true,
                input: true,
                numeric: false,
                pageSizes: [5, 10, 20, 50]
            },
            sortable: true,
            groupable: true,
            scrollable: true,
            columns: [
                {field: "tocken", title: "Ticket Number.", width: "140px"},
                {field: "booking_id", title: "Transaction ID", width: "120px"},
                {field: "customer_full_name", title: "Customer Name", width: "140px"},
                {field: "mobile_number", title: "Mobile Number", width: "130px"},
                {field: "email_address", title: "Email Address", width: "200px"},
                {field: "student_number", title: "Cadet No.", width: "100px"},
                {field: "batch_id", title: "Batch ID", width: "100px"},
                {field: "booking_delivery_address", title: "Address", width: "160px"},
                {field: "self", title: "Self", width: "160px"},
                {field: "spouse", title: "Spouse", width: "90px"},
                {field: "kidesUp12", title: "Kids +12", width: "90px"},
                {field: "kidesUn12", title: "Kids -12", width: "90px"},
                {field: "guest", title: "Guest", width: "90px"},
                {field: "driver", title: "Driver", width: "90px"},
                {field: "maid", title: "Maid", width: "90px"},
                {field: "anual_subsciption_cost", title: "Anual Subscription", width: "150px"},
                {field: "life_time_subsciption_cost", title: "Life Time Subscription", width: "170px"},
                {field: "total_amount", title: "Total Cost", width: "120px"},
                {
                    title: "Delivery Method", width: "150px",
                    template: kendo.template($("#command-delivery-method").html())
                },
//                {field: "delivery_cost", title: "Delivery Cost", width: "120px"},
                {field: "payment_status", title: "Payment Status", width: "140px"},
//                {field: "donation", title: "Donation", width: "100px"},


                {
                    title: "Payment Confirmation", width: "150px",
                    template: kendo.template($("#command-template").html())
                },
                {
                    title: "Ticket Download", width: "130px",
                    template: kendo.template($("#download").html())
                },
                {
                    title: "Delivery Method Change", width: "160px",
                    template: kendo.template($("#command-delivery-status").html())
                }
            ]
        });

    });</script>	
<div id="kWindow"></div>

<!-- Donation Grid -->
<?php if ($_SESSION["user_type_id"] == 4): ?>
    <script id="donation_confirm" type="text/x-kendo-template">

    </script>
<?php else: ?>
    <script id="donation_confirm" type="text/x-kendo-template">
        # if(method != "Online Payment"){ #
        # if(payment_status == "paid") { #
        <a style="font-size:11px;" class="k-button k-grid-even" href="donation_payment.php?donation_id=#= donation_id #&payment_status=#= payment_status #">Cancel Payment</a>
        # } else { #
        <a style="font-size:11px;" class="k-button k-grid-odd" href="donation_payment.php?donation_id=#= donation_id #&payment_status=#= payment_status #">Confirm Payment</a>
        # } #
        # } else {#

        # } #
    </script>
<?php endif; ?>
<script id="donation_print" type="text/x-kendo-template">
    <a style="font-size:11px;" class="k-button k-grid-even" href="donation_download.php?donation_id=#= donation_id #">Download</a>
</script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        var dataSource = new kendo.data.DataSource({
            pageSize: 5,
            transport: {
                read: {
                    url: "../../controller/donation_controller.php",
                    type: "GET"
                }
            },
            autoSync: false,
            schema: {
                data: "data",
                total: "data.length",
                model: {
                    id: "donation_id",
                    fields: {
                        token_no: {type: "string"},
                        donation_id: {type: "number"},
                        student_number: {type: "number"},
                        college_id: {type: "number"},
                        batch_id: {type: "number"},
                        donation_amount: {type: "string"},
                        method: {type: "string"},
                        payment_status: {type: "string"}
                    }

                }
            }

        });
        jQuery("#grid-donation").kendoGrid({
            dataSource: dataSource,
            filterable: true,
            pageable: {
                refresh: true,
                input: true,
                numeric: false,
                pageSizes: [5, 10, 20, 50]
            },
            sortable: true,
            groupable: true,
            scrollable: true,
            columns: [
                {field: "token_no", title: "D-Ticket Number", width: "90px"},
                {field: "student_number", title: "Cadet No", width: "70px"},
                {field: "batch_id", title: "Batch Name", width: "70px"},
                {field: "donation_amount", title: "Donation Amount", width: "100px"},
                {field: "method", title: "Delivery Method", width: "100px"},
                {field: "payment_status", title: "Payment Status", width: "90px"},
                {
                    title: "Donation Confirmation", width: "120px",
                    template: kendo.template($("#donation_confirm").html())
                },
                {
                    title: "Action", width: "120px",
                    template: kendo.template($("#donation_print").html())
                }
            ]
        });

    });</script>	
<div id="kWindow"></div>
<!-- Donation Grid -->


<!-- Subscription Summary -->
<script id="sub_confirm" type="text/x-kendo-template">
    # if(method != "Online Payment"){ # 
    # if(payment_status == "paid") { #
    <a style="font-size:11px;" class="k-button k-grid-even" href="sub_payment.php?subscription_id=#= subscription_id #&payment_status=#= payment_status #">Cancel Payment</a>
    # } else { #
    <a style="font-size:11px;" class="k-button k-grid-odd" href="sub_payment.php?subscription_id=#= subscription_id #&payment_status=#= payment_status #">Confirm Payment</a>
    # } #
    # } else {#

    # } #
</script>
<script id="#sub_print" type="text/x-kendo-template">
    <a style="font-size:11px;" class="k-button k-grid-odd" href="sub_download.php?subscription_id=#= subscription_id">
    Download
    </a>
</script>
<script id="sub_print" type="text/x-kendo-template">
    <a style="font-size:11px;" class="k-button k-grid-even" href="sub_download.php?sub_id=#= subscription_id #">Download</a>
</script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        var dataSource = new kendo.data.DataSource({
            pageSize: 5,
            transport: {
                read: {
                    url: "../../controller/subscription_controller.php",
                    type: "GET"
                }
            },
            autoSync: false,
            schema: {
                data: "data",
                total: "data.length",
                model: {
                    id: "subscription_id",
                    fields: {
                        subscription_id: {type: "number"},
                        student_number: {type: "number"},
                        student_name: {type: "string"},
                        batch_id: {type: "string"},
                        phone: {type: "string"},
                        email_address: {type: "string"},
                        address: {type: "string"},
                        type: {type: "string"},
                        subscription_amount: {type: "string"},
                        method: {type: "string"},
                        payment_status: {type: "string"},
                        college_validation_id: {type: "number"}
                    }

                }
            }

        });
        jQuery("#grid-sub").kendoGrid({
            dataSource: dataSource,
            filterable: true,
            pageable: {
                refresh: true,
                input: true,
                numeric: false,
                pageSizes: [5, 10, 20, 50]
            },
            sortable: true,
            groupable: true,
            scrollable: true,
            // toolbar: [{name: "create", text: "Add User Type"}],
            columns: [
                {field: "student_name", title: "Student Name", width: "120px"},
                {field: "student_number", title: "Cadet Number", width: "130px"},
                {field: "college_validation_id", title: "Transaction ID", width: "130px"},
                {field: "batch_id", title: "Batch Name", width: "100px"},
                {field: "phone", title: "Phone Number", width: "130px"},
                {field: "email_address", title: "Email Address", width: "130px"},
                {field: "address", title: "Address", width: "200px"},
                {field: "type", title: "Subscription Type", width: "120px"},
                {field: "subscription_amount", title: "Subscription Amount", width: "140px"},
                {field: "method", title: "Delivery Method", width: "120px"},
                {field: "payment_status", title: "Payment Status", width: "120px"},
                {
                    title: "Subscription Confirmation", width: "150px",
                    template: kendo.template($("#sub_confirm").html())
                },
                {
                    title: "Action", width: "150px",
                    template: kendo.template($("#sub_print").html())
                }
            ]
        });

    });</script>	
<div id="kWindow"></div>
<!-- Subscription Summary -->


<br/>
<div class="row">
    <div class="heading heading-v2 margin-bottom-40">
        <h2>Report Summary</h2>
    </div>
</div>
<div class="tab-v1 margin-bottom-60" style="padding-left: 14px; padding-right: 14px; margin-top: -20px;">
    <ul class="nav nav-tabs nav-justified margin-bottom-20">
        <li class="active"><a href="#tab-1" data-toggle="tab">Delivery Method Summary</a></li>
        <li><a href="#tab-2" data-toggle="tab">Donation Summary</a></li>
        <li><a href="#tab-3" data-toggle="tab">Subscription Summary</a></li>
    </ul>
    <div class="tab-content" style="">
        <div class="tab-pane active" id="tab-1">

            <div class="panel panel-grey margin-bottom-40" style="border-style: none;">
                <div class="panel-body" style="border: 1px solid silver; margin-top: -30px;">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Delivery Method</th>
                                <th>Spouse</th>
                                <th>Self</th>
                                <th>Kids 12+</th>
                                <th>Kids 12-</th>
                                <th>Guest</th>
                                <th>Maid</th>
                                <th>Driver</th>
                                <th>Total Person</th>
                                <th>Total Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (count($rs) > 0): ?>
                                <?php foreach ($rs as $data): ?>
                                    <tr>
                                        <td><?php echo $data->method; ?></td>
                                        <td><?php echo $data->Spouse; ?></td>
                                        <td><?php echo $data->Self; ?></td>
                                        <td><?php echo $data->Kids12Up; ?></td>
                                        <td><?php echo $data->Kids12Down; ?></td>
                                        <td><?php echo $data->Guest; ?></td>
                                        <td><?php echo $data->Maid; ?></td>
                                        <td><?php echo $data->Driver; ?></td>
                                        <td><?php echo $data->TotalPerson; ?></td>
                                        <td><?php echo $data->TotalAmount; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div> 

            </div>
        </div>
        <div class="tab-pane" id="tab-2">
            <div class="panel panel-grey margin-bottom-40" style="margin-top: -30px;">
                <div class="panel-body" >
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No of Donation</th>
                                <th>Total Amount (BDT)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (count($donation) > 0): ?>
                                <?php foreach ($donation as $data): ?>
                                    <tr>
                                        <td><?php echo $data->NoOfDonation; ?></td>
                                        <td><?php echo $data->TotalDonation; ?></td>

                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>



                        </tbody>
                    </table>
                </div> 

            </div>
        </div>


        <div class="tab-pane" id="tab-3">
            <div class="panel panel-grey margin-bottom-40" style="margin-top: -30px;">
                <div class="panel-body" >

                    <?php
                    $query_one = "SELECT SUM(subscription_amount) as total_life_time FROM subscription where subscription_type='is_life_time' AND payment_status='paid'";
                    $query_two = "SELECT SUM(subscription_amount) as total_annual FROM subscription where subscription_type='is_anual' AND payment_status='paid'";
                    $query_no_of_annual = "SELECT count(subscription_id) as no_of_anual FROM subscription where subscription_type='is_anual' AND payment_status='paid'";
                    $query_no_of_life_time = "SELECT count(subscription_id) as no_of_life_time FROM subscription where subscription_type='is_life_time' AND payment_status='paid'";

                    $result_life_time = $con->ReturnObjectByQuery($query_one);
                    $result_annual = $con->ReturnObjectByQuery($query_two);
                    $result_4 = $con->ReturnObjectByQuery($query_no_of_annual);
                    $result_5 = $con->ReturnObjectByQuery($query_no_of_life_time);
                    ?>

                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Subscription Type</th>
                                <th>No of Subscriptions.</th>
                                <th>Total Amount (BDT)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Life Time</td>
                                <td>
                                    <?php
                                    if ($result_5{0}->no_of_life_time > 0) {
                                        echo $result_5{0}->no_of_life_time;
                                    } else {
                                        echo '<i style="color:gray;">No subscription yet</i>';
                                    }
                                    ?>
                                </td>

                                <td>
                                    <?php echo $result_life_time{0}->total_life_time; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Annual</td>
                                <td>
                                    <?php
                                    if ($result_4{0}->no_of_anual > 0) {
                                        echo $result_4{0}->no_of_anual;
                                    } else {
                                        echo '<i style="color:gray;">No subscription yet</i>';
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php echo $result_annual{0}->total_annual; ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div> 

            </div>
        </div>


    </div>
</div>
<?php include("../layout/copyright.php"); ?>