<?php
session_start();
include("../../config/class.web.config.php");
$con = new Config();
error_reporting(0);
if (isset($_GET["donation_id"])) {
    $donation_id = $_GET["donation_id"];
}
//Fetch donation data
$donations = $con->SelectAll("donation", "donation_id='$donation_id'");

?>
<!DOCTYPE html> 
<html>
    <head>
        <script type="text/javascript" src="admin/assets/js/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="jquery-barcode.js"></script>
        <link href="style.css" rel="stylesheet">
    </head>
    <body>
        <div class="top_text">
            Please print and bring this ticket with you.
        </div>
        <br />

        <?php foreach ($donations as $don): ?>
            <?php $customers = $con->SelectAll("customer", "customer_id='$don->customer_id'"); ?>
            <?php $events = $con->SelectAll("event", "event_id='$don->event_id'"); ?>
            <?php $college = $con->SelectAll("college", "college_id='$don->college_id'"); ?>
            <?php $dis = $con->SelectAll("district", "dis_state_id='$don->district_id'"); ?>
            <?php $college_val = $con->SelectAll("college_validation", "college_validation_id='$don->college_validation_id'"); ?>
            <table class="main">
                <tr>
                    <td width="139" style="height:50px;" bgcolor="#FFFFFF"><h3>Document</h3></td>
                    <td colspan="5" bgcolor="#FFFFFF"><h3><strong>Donation Reciept</strong></h1></td>
                    <td colspan="4" rowspan="2" align="center" valign="middle" bgcolor="#FFFFFF">
                        <?php if ($events{0}->event_logo_image != ''): ?>
                            <img src="<?php echo "../../uploads/event_logo_image/" . $events{0}->event_logo_image; ?>" class="event_logo" alt=""/>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" bgcolor="#FFFFFF"><h3>Thank You For Your Contribution!</h3></td>
                </tr>


                <tr>
                    <td bgcolor="#FFFFFF"><h6>DATE</h6></td>
                    <td colspan="4" bgcolor="#FFFFFF"> </td>
                    <td  bgcolor="#FFFFFF"><h6>TIME</h6></td>
                    <td colspan="4" bgcolor="#FFFFFF">
                        <h6>                                                
                        </h6>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#FFFFFF"><h6>Delivery Method:</h6></td>
                    <td colspan="4" bgcolor="#FFFFFF">
                        <h4>
                            <?php
                            if ($don->delivery_method == "pick_from_venue") {
                                echo "Pick From Office";
                            } else if ($don->delivery_method == "home_delivery") {
                                echo "Home Delivery";
                            } else {
                               echo  "Online Payment";
                            }
                            ?>
                        </h4>
                    </td>
                    <td bgcolor="#FFFFFF"><h6>CATAGORY</h6></td>
                    <td colspan="4" bgcolor="#FFFFFF"><h6><?php echo $events{0}->category_name; ?></h6></td>
                </tr>


                <tr>
                    <td colspan="5" valign="top" bgcolor="#FFFFFF" style="font-size: 11px;">
                        <p style="text-align: center; font-size: 14px; font-weight: bold; margin-top: 4px;">Owner's Info</p>
                        <p> Name: <?php echo $customers{0}->first_name; ?>&nbsp;<?php echo $customers{0}->last_name; ?></p>
                        <p> Address:  <?php echo $customers{0}->address; ?> </p>                       
                        <p> Email: <?php echo $customers{0}->email_address; ?></p>
                        <p> Cadet NO :<?php echo $don->student_number; ?></p>
                        <p> Batch NO :<?php echo $college_val{0}->batch_id; ?></p>
                        <p> College name:<?php echo $college{0}->college_name; ?>
                        <p> Phone Number:<?php echo $customers{0}->mobile_number; ?></p>
                        <?php if ($don->delivery_method == "home_delivery"): ?>
                            <?php if ($don->delivery_city != ''): ?>
                                <p>Delivery City:<?php echo $don->delivery_city; ?></p>
                            <?php elseif ($don->district_id != ''): ?>

                                <p>Delivery District:<?php echo $dis{0}->delivery_city; ?></p>
                            <?php endif; ?>
                            <p>Delivery Post Code:<?php echo $don->zip_code; ?></p>
                        <?php endif; ?>
                    </td>
                    <td colspan="6">
                        <table style="width:100%;">

                            <tr>
                                <td style="border-style: none; width: 50%;">Donated Amount:</td>
                                <td style="border-style: none; text-align: right; width: 50%;">
                                    <?php echo $don->donation_amount; ?>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2" style="border-style: none;"><hr style="border-style: dotted;" />

                                </td>
                            </tr>

                            <tr>
                                <td style="border-style: none; font-weight: bold;">Grand Total:</td>
                                <td style="border-style: none; font-weight: bold; font-size: 16px; text-align: right">
                                    <?php echo $don->donation_amount; ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td height="43" colspan="10" align="center" bgcolor="#FFFFFF" style="border-bottom-color: white; padding-bottom: 0px; padding-top: 0px;">Ticket Chai E-Ticket NO:

                    </td>
                </tr>
                <tr>
                    <td colspan="10" align="center" bgcolor="#FFFFFF" style="padding-top: 0px; border-top-color: white;" >
                        <?php if ($don->token_no != ''): ?>
                        <div class="barcodecell"><barcode code="<?php echo $don->token_no; ?>" type="C39" height="1.5" text="1"/></div>
                        <div style="text-align:center; font-size: 10px;">
                            <?php echo $don->token_no;?>
                        </div> 
                        <?php endif;?>
                    </td>
                </tr>
                <?php if ($don->delivery_method == "pick_from_venue"): ?>
                    <tr>
                        <td colspan="10" align="center" bgcolor="#FFFFFF" style=" font-size:10px;">
                    <center><h6>PICK POINTS</h6></center><br />
                    <?php
                    if ($don->delivery_method == "pick_from_venue") {
                        $picks = $con->SelectAll("pick_point", "", "", "array");
                        foreach ($picks as $p) {
                            echo $p->pick_point_name . ",";
                        }
                    }
                    ?>
                </td>
            </tr>
        <?php endif; ?>

        <tr>
            <td colspan="10" align="center" bgcolor="#FFFFFF" style="border-bottom-style: none;">TICKETING PARTNER</td>
        </tr>
        <tr>
            <td style="border-top-style: none;" colspan="10" bgcolor="#FFFFFF">
                <table style="width: 100%; margin-left: -8px;">
                    <tr>
                        <td style="border-style: none; width: 33%"> <h5>Hot Line Number: +8801971-842538, +88 04478009569 </h5></td>
                        <td style="border-style: none;  width: 33%"><img src="ticketchai_logo.png" width="100" height="50" alt=""/></td>
                        <td style="border-style: none; width: 33%; text-align: right;"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
<?php endforeach; ?>
</html>