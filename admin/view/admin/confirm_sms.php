<?php
include ('../../config/class.web.config.php');
$con = new Config();
extract($_POST);
$booking_id = '';
$sms_status = '';
if (isset($_GET["booking_id"])) {
    $booking_id = $_GET["booking_id"];
}

if (isset($_GET["sms_status"])) {
    $sms_status = $_GET["sms_status"];
}

if ($sms_status == "NOT CONFIRMED") {
    $results = $con->CheckExistsWithCondition("sms_traking", " booking_id='$booking_id'");
    if ($results == 1) {

        $update_array = array("booking_id" => $booking_id, "status" => "CONFIRMED");
        $result = $con->Update("sms_traking", $update_array, "", "", "array");
        if ($result["output"] == "success") {
            $con->redirect("index.php");
        }
    } else {
        $insert_array = array("booking_id" => $booking_id, "status" => "CONFIRMED");
        $result2 = $con->Insert("sms_traking", $insert_array, "", "", "array");
        if ($result2["output"] == "success") {
            $con->redirect("index.php");
        }
    }
} else {
    $results = $con->CheckExistsWithCondition("sms_traking", " booking_id='$booking_id'");
    if ($results == 1) {

        $update_array = array("booking_id" => $booking_id, "status" => "NOT CONFIRMED");
        $result = $con->Update("sms_traking", $update_array, "", "", "array");
        if ($result["output"] == "success") {
            $con->redirect("index.php");
        }
    } else {
        $insert_array = array("booking_id" => $booking_id, "status" => "NOT CONFIRMED");
        $result2 = $con->Insert("sms_traking", $insert_array, "", "", "array");
        if ($result2["output"] == "success") {
            $con->redirect("index.php");
        }
    }
}
