<?php
include ('../../config/class.web.config.php');
$con = new Config();
extract($_POST);
$booking_id = '';
$delivery_method = '';
if (isset($_GET["booking_id"])) {
    $booking_id = $_GET["booking_id"];
}

if (isset($_GET["delivery_method"])) {
    $delivery_method = $_GET["delivery_method"];
}

if ($delivery_method == "HOME DELIVERY") {
   
        $update_array = array("booking_id" => $booking_id, "delivery_method" => "pick_from_venue","delivery_cost"=>'0.00');
        $result = $con->Update("booking", $update_array, "", "", "array");
        if ($result["output"] == "success") {
            $con->redirect("index.php");
        }
}