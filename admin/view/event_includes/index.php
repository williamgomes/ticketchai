<?php
session_start();
include("../../config/class.web.config.php");
$con = new Config();
if ($con->authenticate() == 1) {
    $con->redirect("../../login.php");
}
/* -- End Authentication  -- */
?>
<?php include("../layout/header_script.php"); ?>
<?php include("../layout/menu_top.php"); ?>
<?php include("../layout/breadcrumbs.php"); ?>
<div id="grid"></div>

<script type="text/javascript">
    jQuery(document).ready(function () {
        var dataSource = new kendo.data.DataSource({
            pageSize: 5,
            transport: {
                read: {
                    url: "../../controller/event_includesController.php?id=31",
                    type: "GET"
                },
                update: {
                    url: "../../controller/event_includesController.php?id=31",
                    type: "POST",
                    complete: function (e) {
                        jQuery("#grid").data("kendoGrid").dataSource.read();
                    }
                },
                destroy: {
                    url: "../../controller/event_includesController.php?id=31",
                    type: "DELETE"
                },
                create: {
                    url: "../../controller/event_includesController.php?id=31",
                    type: "PUT",
                    complete: function (e) {
                        jQuery("#grid").data("kendoGrid").dataSource.read();
                    }
                }
            },
            autoSync: false,
            schema: {
                errors: function (e) {
                    if (e.error === "yes")
                    {
                        var message = "";
                        message += e.message;
                        var window = jQuery("#kWindow");
                        if (!window.data("kendoWindow")) {
                            window.kendoWindow({
                                title: "Error window",
                                modal: true,
                                height: 100,
                                width: 400
                            });
                        }
                        window.data("kendoWindow").center().open();
                        window.html('<div style="margin-top:10px;text-align:center;"><span style="color:red;font-size:14px;">' + message + '</span></div>');
                        this.cancelChanges();
                    }
                },
                data: "data",
                total: "data.length",
                model: {
                    id: "event_includes_id",
                    fields: {
                        event_includes_id: {editable: false, nullable: true},
                        event_id: {type: "string", validation: {required: true}},
                        event_includes_name: {type: "string", validation: {required: "Nationality is required"}},
                        event_includes_image: {type: "string", validation: {required: "Language is required"}},
                        event_includes_price: {type: "string", validation: {required: "Price is required"}},
                        is_active: {type: "boolean"}
                    }
                }
            }
        });
        jQuery("#grid").kendoGrid({
            dataSource: dataSource,
            filterable: true,
            pageable: {
                refresh: true,
                input: true,
                numeric: false,
                pageSizes: [5, 10, 20, 50]
            },
            sortable: true,
            groupable: true,
            resizable: true,
            toolbar: [{name: "create", text: "Add Event Includes"}],
            columns: [
                {field: "event_includes_name", title: "Event Name"},
                {field: "event_includes_image", title: "Event Includes Logo"},
                {field: "event_includes_price", title: "Event Includes Price",
                    editor: function (container, options) {
                        $('<input  maxlength="10"  name="' + options.field + '"/>')
                                .appendTo(container)
                                .kendoNumericTextBox({
                                    min: 0,
                                    max: 9999999999,
                                    format: "#",
                                    decimals: 0
                                });
                    }
                },
                {field: "is_active", title: "Active?", template: "#= is_active ? 'Yes' : 'No' #", width: "10%"},
                {command: ["edit", "destroy"], title: "Action", width: "230px"}],
            editable: "inline"
        });
    });</script>		
<div id="kWindow"></div>


<?php include("../layout/copyright.php"); ?>
